//
//  AppDelegate.m
//  YDERP
//
//  Created by 何英健 on 2021/7/19.
//

#import "AppDelegate.h"
#import "EETabBarController.h"
#import "YDLoginVC.h"
#import "YDWelcomViewController.h"

@interface AppDelegate ()

@end

@implementation AppDelegate

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    if (self.window == nil) {
        self.window = [[UIWindow alloc] initWithFrame:[UIScreen mainScreen].bounds];
        self.window.backgroundColor = [UIColor whiteColor];
        if (@available(iOS 13.0, *)) {
            self.window.overrideUserInterfaceStyle = UIUserInterfaceStyleLight;
        }
    }

    [self.window makeKeyAndVisible];
    
    // 增加启动页时间
    [NSThread sleepForTimeInterval:0.5];
    
    [self setupEnvironment];
    
    [self setupAPP];
    
    [self setupUI];
    
    // 判断是否弹出欢迎页
    [self checkShowWelcome];
    
    return YES;
}

// 环境切换
- (void)setupEnvironment
{
//    NSString *environment = GetUserDefaultWithKey(STORE_ENVIRONMENT);
//
//    if ([environment isEqualToString:@"Test"]) {
//        self.type = YDEnvironmentTypeTest;
//
//    } else if ([environment isEqualToString:@"Prepare"]) {
//        self.type = YDEnvironmentTypePrepare;
//
//    } else {
//        self.type = YDEnvironmentTypeRelease;
//    }
    
    self.type = YDEnvironmentTypeRelease;
}

- (void)setType:(EEEnvironmentType)type
{
    _type = type;
    
    switch (type) {
        case YDEnvironmentTypeTest:
        {
            self.mainUrl = @"https://rfgmcerp.test.rfgmc.com";
        }
            break;
            
        case YDEnvironmentTypePrepare:
        {
            self.mainUrl = @"https://rfgmcerp-pre.rfgmc.com";
        }
            break;
            
        case YDEnvironmentTypeRelease:
        {
            self.mainUrl = @"https://rfgmcerp.rfgmc.com";
        }
            break;
            
        default:
            break;
    }
}

// 加载配置
- (void)setupAPP
{

}

// 界面设置
- (void)setupUI
{
    // 去除默认黑线
    [[UINavigationBar appearance] setBackgroundImage:[UIImage imageWithColor:[UIColor whiteColor]] forBarPosition:UIBarPositionAny barMetrics:UIBarMetricsDefault];
    [[UINavigationBar appearance] setShadowImage:[[UIImage alloc] init]];
    
    [SVProgressHUD setDefaultStyle:SVProgressHUDStyleDark];
    [SVProgressHUD setDefaultMaskType:SVProgressHUDMaskTypeClear];
    [SVProgressHUD setDefaultAnimationType:SVProgressHUDAnimationTypeNative];
    [SVProgressHUD setMinimumDismissTimeInterval:0.5];
    [SVProgressHUD setMaximumDismissTimeInterval:1.5];
}

- (void)checkShowWelcome
{
    NSString *lastVersion = GetUserDefaultWithKey(STORE_LAST_VERSION);
    NSDictionary *infoDic = [[NSBundle mainBundle] infoDictionary];
    NSString *currentVersion = infoDic[@"CFBundleShortVersionString"];
    if ([lastVersion isEqualToString:currentVersion]) {
        [self initRootVC];
        
    } else {
        SetUserDefaultKeyWithObject(STORE_LAST_VERSION, currentVersion);
        UserDefaultSynchronize;
        
        YDWelcomViewController *vc = [[YDWelcomViewController alloc] initWithNibName:@"YDWelcomViewController" bundle:nil];
        self.window.rootViewController = vc;
    }
}

- (void)initRootVC
{
    if (UserModel.loginToken.length) {
        self.window.rootViewController = [[EETabBarController alloc] init];
        
    } else {
        YDLoginVC *loginVC = [[YDLoginVC alloc] init];
        UINavigationController *nav = [[UINavigationController alloc] initWithRootViewController:loginVC];
        self.window.rootViewController = nav;
    }
}

+ (AppDelegate *)shareAppDelegate
{
    return (AppDelegate *)[[UIApplication sharedApplication] delegate];
}

- (UIViewController *)getCurrentUIVC
{
    UIViewController *superVC = [self getCurrentVC];
    
    if ([superVC isKindOfClass:[UITabBarController class]]) {
        
        UIViewController  *tabSelectVC = ((UITabBarController*)superVC).selectedViewController;
        
        if ([tabSelectVC isKindOfClass:[UINavigationController class]]) {
            
            return ((UINavigationController*)tabSelectVC).viewControllers.lastObject;
        }
        return tabSelectVC;
    }else
        if ([superVC isKindOfClass:[UINavigationController class]]) {
            
            return ((UINavigationController*)superVC).viewControllers.lastObject;
        }
    return superVC;
}

- (UIViewController *)getCurrentVC
{
    UIViewController *result = nil;
    UIWindow *window = KeyWindow;
    if (window.windowLevel != UIWindowLevelNormal) {
        NSArray *windows = [[UIApplication sharedApplication] windows];
        for(UIWindow * tmpWin in windows) {
            if (tmpWin.windowLevel == UIWindowLevelNormal) {
                window = tmpWin;
                break;
            }
        }
    }
    
    UIView *frontView = [[window subviews] objectAtIndex:0];
    id nextResponder = [frontView nextResponder];
    
    if ([nextResponder isKindOfClass:[UIViewController class]])
        result = nextResponder;
    else
        result = window.rootViewController;
    
    return result;
}

@end

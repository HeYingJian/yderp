//
//  AppDelegate.h
//  YDERP
//
//  Created by 何英健 on 2021/7/19.
//

#import <UIKit/UIKit.h>

typedef enum : NSUInteger {
    YDEnvironmentTypeTest, //**< 测试版  */
    YDEnvironmentTypePrepare,  //**< 预生产版  */
    YDEnvironmentTypeRelease //**< 正式版  */
} EEEnvironmentType;

@interface AppDelegate : UIResponder <UIApplicationDelegate>

// 环境
@property (assign, nonatomic) EEEnvironmentType type;
@property (nonatomic, copy) NSString *mainUrl;

@property (nonatomic, strong) UIWindow *window;

@property (nonatomic, strong) UINavigationController *currentNav;

+ (AppDelegate *)shareAppDelegate;

- (UIViewController *)getCurrentUIVC;

- (void)initRootVC;

@end


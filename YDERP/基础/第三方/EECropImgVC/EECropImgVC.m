//
//  EECropImgVC.m
//  EEducation
//
//  Created by 何英健 on 2020/11/1.
//

#import "EECropImgVC.h"
#import "TKImageView.h"

@interface EECropImgVC ()

@property (nonatomic, weak) IBOutlet TKImageView *tkImageView;

@end

@implementation EECropImgVC

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self initUI];
}

- (void)initUI
{
    [self setUpTKImageView];
    
//    // 禁用当前返回手势
//    id target = self.navigationController.interactivePopGestureRecognizer.delegate;
//    UIPanGestureRecognizer *pan = [[UIPanGestureRecognizer alloc] initWithTarget:target action:nil];
//    [self.view addGestureRecognizer:pan];
}

- (void)setUpTKImageView
{
    self.tkImageView.backgroundColor = [UIColor blackColor];
    self.tkImageView.toCropImage = self.originImage;
    self.tkImageView.showMidLines = YES;
    self.tkImageView.needScaleCrop = YES;
    self.tkImageView.showCrossLines = NO;
    self.tkImageView.cornerBorderInImage = NO;
    self.tkImageView.cropAreaCornerWidth = 44;
    self.tkImageView.cropAreaCornerHeight = 44;
    self.tkImageView.minSpace = 30;
    self.tkImageView.cropAreaCornerLineColor = [UIColor whiteColor];
    self.tkImageView.cropAreaBorderLineColor = [UIColor whiteColor];
    self.tkImageView.cropAreaCornerLineWidth = 6;
    self.tkImageView.cropAreaBorderLineWidth = 4;
    self.tkImageView.cropAreaMidLineWidth = 20;
    self.tkImageView.cropAreaMidLineHeight = 6;
    self.tkImageView.cropAreaMidLineColor = [UIColor whiteColor];
    self.tkImageView.cropAreaCrossLineColor = [UIColor whiteColor];
    self.tkImageView.cropAreaCrossLineWidth = 4;
    self.tkImageView.initialScaleFactor = .8f;
    self.tkImageView.cropAspectRatio = 1;
}

- (IBAction)tapCancelBtn:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)tapConfirmBtn:(id)sender
{
    if (self.cropImageSucceedBlock) {
        self.cropImageSucceedBlock(self.tkImageView.currentCroppedImage);
    }
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [self.navigationController setNavigationBarHidden:YES animated:NO];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    
    [self.navigationController setNavigationBarHidden:NO animated:NO];
}

@end

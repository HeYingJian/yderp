//
//  EECropImgVC.h
//  EEducation
//
//  Created by 何英健 on 2020/11/1.
//

#import <UIKit/UIKit.h>

typedef void(^BlockCropImageSucceed)(UIImage *image);

@interface EECropImgVC : UIViewController

@property (nonatomic, strong) UIImage *originImage;

@property (nonatomic, copy) BlockCropImageSucceed cropImageSucceedBlock;

@end

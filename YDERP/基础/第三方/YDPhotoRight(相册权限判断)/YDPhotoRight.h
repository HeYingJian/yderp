//
//  YDPhotoRight.h
//  YiDing
//
//  Created by 何英健 on 2019/7/9.
//  Copyright © 2019 com.rfchina.yiding. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface YDPhotoRight : NSObject

// 检查相机权限
+ (BOOL)checkHaveCameraRight;

// 检查相册权限(不是相机，相机会自己弹出来)
+ (BOOL)checkHavePhotoLibraryRight;

@end


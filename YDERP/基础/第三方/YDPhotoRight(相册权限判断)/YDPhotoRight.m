//
//  YDPhotoRight.m
//  YiDing
//
//  Created by 何英健 on 2019/7/9.
//  Copyright © 2019 com.rfchina.yiding. All rights reserved.
//

#import "YDPhotoRight.h"
#import <AVFoundation/AVCaptureDevice.h>
#import <AVFoundation/AVMediaFormat.h>
#import <Photos/PHPhotoLibrary.h>

@implementation YDPhotoRight

+ (BOOL)checkHaveCameraRight
{
    AVAuthorizationStatus status = [AVCaptureDevice authorizationStatusForMediaType:AVMediaTypeVideo];
    if (status == AVAuthorizationStatusRestricted || status == AVAuthorizationStatusDenied) {
        // 提示拍照权限未开启
        [YDPhotoRight createNoticeViewWithType:0];
        
        return NO;
    }

    return YES;
}

+ (BOOL)checkHavePhotoLibraryRight
{
    //相册权限判断
    __block BOOL result = NO;
    PHAuthorizationStatus status = [PHPhotoLibrary authorizationStatus];
    if (status == PHAuthorizationStatusDenied) {
        //相册权限未开启
        [YDPhotoRight createNoticeViewWithType:1];
        result = NO;
        
    } else if (status == PHAuthorizationStatusNotDetermined) {
        //相册进行授权
        /* * * 第一次安装应用时直接进行这个判断进行授权 * * */
        
        //创建信号
        dispatch_semaphore_t sem = dispatch_semaphore_create(0);
        [PHPhotoLibrary requestAuthorization:^(PHAuthorizationStatus status){
            //授权后直接打开照片库
            if (status == PHAuthorizationStatusAuthorized){
                result = YES;
            }
            
            if (@available(iOS 14, *)) {
                if (status == PHAuthorizationStatusLimited) {
                    result = YES;
                }
            }
            
            //信号量加1
            dispatch_semaphore_signal(sem);
            
        }];
        //如果信号量大于0则执行后面代码
        dispatch_semaphore_wait(sem, DISPATCH_TIME_FOREVER);
        return result;
      
    } else if (status == PHAuthorizationStatusAuthorized) {
        result = YES;
        
    } else if (@available(iOS 14, *)) {
        if (status == PHAuthorizationStatusLimited) {
            return YES;
        }
    }
    
    return result;
}

// 创建提示框 type：0-相机 1-相册
+ (UIView *)createNoticeViewWithType:(NSInteger)type
{
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT)];
    view.backgroundColor = [UIColor clearColor];
    
    UIView *bgView = [[UIView alloc] initWithFrame:view.bounds];
    bgView.backgroundColor = [UIColor blackColor];
    bgView.alpha = 0.7;
    [view addSubview:bgView];
    
    UIView *containerView = [[UIView alloc] init];
    [containerView setSize:CGSizeMake(310, 195)];
    [containerView setOrigin:CGPointMake((SCREEN_WIDTH - containerView.width) / 2, (SCREEN_HEIGHT - containerView.height) / 2)];
    containerView.backgroundColor = [UIColor whiteColor];
    containerView.layer.cornerRadius = 15;
    [view addSubview:containerView];
    
    UILabel *titleLabel = [[UILabel alloc] init];
    titleLabel.font = [UIFont systemFontOfSize:18 weight:UIFontWeightMedium];
    titleLabel.textColor = ColorFromRGB(0x222222);
    NSString *title;
    if (type == 0) {
        title = @"“研才教育”想访问您的相机";
        
    } else {
        title = @"“研才教育”想访问您的相册";
    }
    titleLabel.text = title;
    [titleLabel sizeToFit];
    [titleLabel setOrigin:CGPointMake((containerView.width - titleLabel.width) / 2, 25)];
    [containerView addSubview:titleLabel];
    
    UILabel *contentLabel = [[UILabel alloc] init];
    contentLabel.font = [UIFont systemFontOfSize:14 weight:UIFontWeightRegular];
    contentLabel.textColor = ColorFromRGB(0x555555);
    NSString *content;
    if (type == 0) {
        content = @"请进入iPhone的“设置-隐私-相机”选项, 允许圆猿记账访问您的相机";
        
    } else {
        content = @"请进入iPhone的“设置-隐私-相册”选项, 允许圆猿记账访问您的手机相册";
    }
    contentLabel.text = content;
    contentLabel.numberOfLines = 2;
    [contentLabel setSize:CGSizeMake(252, 45)];
    [contentLabel setOrigin:CGPointMake((containerView.width - contentLabel.width) / 2, CGRectGetMaxY(titleLabel.frame) + 15)];
    [containerView addSubview:contentLabel];
    
    UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
    [btn setSize:CGSizeMake(185, 45)];
    [btn setOrigin:CGPointMake((containerView.width - btn.width) / 2, containerView.height - 21 - btn.height)];
    btn.backgroundColor = ColorFromRGB(0xFE3A48);
    btn.layer.cornerRadius = btn.height / 2;
    [btn setTitle:@"确定" forState:UIControlStateNormal];
    [btn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [btn addTapBlock:^(UIButton *btn) {
        [view removeFromSuperview];
    }];
    [containerView addSubview:btn];
    
    [[UIApplication sharedApplication].keyWindow addSubview:view];
    
    return view;
}

@end

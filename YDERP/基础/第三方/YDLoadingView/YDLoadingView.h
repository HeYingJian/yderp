//
//  YDLoadingView.h
//  PregnancyHelper
//
//  Created by 何英健 on 19/4/26.
//  Copyright (c) 2019年 YiDing. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface YDLoadingView : UIView

//开始动画
- (void)startLoading;

//停止动画
- (void)stopLoading;

/**
 *  在view上添加一个loadingView
 *
 *  @param superview superview
 */
+ (instancetype)showToSuperview:(UIView *)superview;

/**
 *  隐藏view中的所有YDLoadingView类型
 *
 *  @param superview superview
 */
+ (void)hideToSuperview:(UIView *)superview;


@end

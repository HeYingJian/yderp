//
//  YDLoadingView.m
//  PregnancyHelper
//
//  Created by 何英健 on 19/4/26.
//  Copyright (c) 2019年 YiDing. All rights reserved.
//

#import "YDLoadingView.h"
#import "YDActivityIndicatorView.h"

#define PH_LOADING_VIEW_HEIGHT    75.0f
#define ANIMATION_VIEW_WIDTH      25.0f

@interface YDLoadingView()

@property(strong, nonatomic) UIView *loadingView;
@property(strong ,nonatomic) YDActivityIndicatorView *anmationView;

@end

@implementation YDLoadingView

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        [self setupSubView];
    }
    return self;
}

// 创建加载页面
- (void)setupSubView
{
    self.userInteractionEnabled = NO;
    self.backgroundColor = [UIColor clearColor];
    [self setAutoresizingMask:UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight];
    
    self.loadingView = [[UIView alloc] initWithFrame:CGRectMake(0, (self.bounds.size.height - PH_LOADING_VIEW_HEIGHT) * 0.5, SCREEN_WIDTH, PH_LOADING_VIEW_HEIGHT)];
    self.loadingView.backgroundColor = [UIColor clearColor];
    [self.loadingView setAutoresizingMask:UIViewAutoresizingFlexibleTopMargin | UIViewAutoresizingFlexibleBottomMargin];
    [self addSubview:self.loadingView];
    
    UIImageView *backgroundLogo = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"亿订转圈logo"]];
    CGFloat logoBorder = 58;
    backgroundLogo.frame = CGRectMake((SCREEN_WIDTH - logoBorder) / 2, 0, logoBorder, logoBorder);
    [self.loadingView addSubview:backgroundLogo];
    
    self.anmationView = [[YDActivityIndicatorView alloc] init];
    [self.anmationView setX:(SCREEN_WIDTH - ANIMATION_VIEW_WIDTH) / 2];
    [self.anmationView setY:(backgroundLogo.height - ANIMATION_VIEW_WIDTH) / 2 - 3];
    [self.anmationView setSize:CGSizeMake(ANIMATION_VIEW_WIDTH, ANIMATION_VIEW_WIDTH)];
    [self.loadingView addSubview:self.anmationView];
}

// 启动加载动画
- (void)startLoading
{
    self.loadingView.hidden = NO;
    [self.anmationView startLoading];
}

// 停止加载动画
- (void)stopLoading
{
    self.loadingView.hidden = YES;
    [self.anmationView endLoading];
}

+ (instancetype)showToSuperview:(UIView *)superview
{
    BOOL superViewNil = NO;
    if (!superview) {
        superview = [UIApplication sharedApplication].keyWindow;
        
        superViewNil = YES;
    }
    
    YDLoadingView *loadingView = [[self alloc] init];
    if (superViewNil) {
        loadingView.frame = CGRectMake(0, TopHeight, superview.width, superview.height - TopHeight);
    } else {
        loadingView.frame = CGRectMake(0, 0, superview.width, superview.height);
    }

    [superview addSubview:loadingView];
    [superview bringSubviewToFront:loadingView];
    
    loadingView.userInteractionEnabled = YES;
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:loadingView action:@selector(tap)];
    [loadingView addGestureRecognizer:tap];
    
    [loadingView startLoading];
    return loadingView;
}

- (void)tap
{
    // 阻止点击穿透
}

+ (void)hideToSuperview:(UIView *)superview;
{
    if (!superview) {
        superview = [UIApplication sharedApplication].keyWindow;
    }
    
    for (UIView *subview in superview.subviews) {
        if ([subview isKindOfClass:[YDLoadingView class]]) {
            [(YDLoadingView *)subview stopLoading];
            
            for (UIGestureRecognizer *sub in subview.gestureRecognizers) {
                [(YDLoadingView *)subview removeGestureRecognizer:sub];
            }
            
            [subview removeFromSuperview];
        }
    }
}

@end

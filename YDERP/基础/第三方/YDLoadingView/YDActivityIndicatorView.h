//
//  YDActivityIndicatorView.h
//  PregnancyHelper
//
//  Created by dengzl on 2017/11/29.
//  Copyright © 2017年 ShengCheng. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface YDActivityIndicatorView : UIView

@property (nonatomic,strong) UIImageView *imageView;

@property (nonatomic,readonly,assign) BOOL isLoading;

- (void)startLoading;

- (void)endLoading;

@end

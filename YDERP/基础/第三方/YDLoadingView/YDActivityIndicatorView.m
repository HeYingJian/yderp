//
//  YDActivityIndicatorView.m
//  PregnancyHelper
//
//  Created by dengzl on 2017/11/29.
//  Copyright © 2017年 ShengCheng. All rights reserved.
//

#import "YDActivityIndicatorView.h"

@implementation YDActivityIndicatorView

- (id)initWithFrame:(CGRect)frame{
    if (self = [super initWithFrame:frame]) {
        _imageView = [[UIImageView alloc]initWithFrame:self.bounds];
        [_imageView setImage:[UIImage imageNamed:@"转圈"]];
        [self addSubview:_imageView];
        [_imageView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.edges.mas_equalTo(UIEdgeInsetsZero);
        }];
    }
    return self;
}

- (void)startLoading{
    if (_isLoading) {
        return;
    }
    
    CABasicAnimation *animation = [CABasicAnimation animationWithKeyPath:@"transform.rotation.z"];
    animation.removedOnCompletion = NO;
    animation.fillMode = kCAFillModeForwards;
    animation.fromValue = @(0);
    animation.toValue = @(M_PI * 2);
    animation.duration = 1.f;
    animation.repeatCount = INT_MAX;
    
    [self.layer addAnimation:animation forKey:@"rotate"];
    
    _isLoading = YES;
    self.hidden = NO;
}

- (void)endLoading{
    [self.layer removeAnimationForKey:@"rotate"];
    _isLoading = NO;
}

@end

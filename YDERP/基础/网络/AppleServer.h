//
//  AppleServer.h
//  YiDing
//
//  Created by Hang Baoan on 2018/12/26.
//  Copyright © 2018 com.rfchina.yiding. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AppleServerResponse.h"

//POST
typedef void(^AppleServerPostCompleteBlock)(BOOL done, id _Nullable object, NSString* _Nullable error);

//GET
typedef void(^AppleServerGetCompleteBlock)(BOOL done, id _Nullable object, NSString* _Nullable error);

//CompleteHandler
typedef void(^AppleServerCompleteHandler)(BOOL done, NSString* _Nullable error);

NS_ASSUME_NONNULL_BEGIN

@interface AppleServer : NSObject

+ (AppleServer *)shared;
// 设置请求头信息
+ (void)setUpRequest:(NSMutableURLRequest *)request;

//POST Request
- (void)postWithPath:(NSString*)path Paramters:(NSDictionary*)paramters CompleteBlock: (AppleServerPostCompleteBlock)complete;

//GET Request
- (void)getWithPath:(NSString*)path CompleteBlock:(AppleServerPostCompleteBlock)complete;

//HTTP POST Request
- (void)httpPostWithPath:(NSString*)path Paramters:(NSDictionary*)paramters CompleteBlock: (AppleServerPostCompleteBlock)complete;

//JSON GET FULL URL
- (void)getWithUrl:(NSString*)url Paramters:(NSDictionary*)paramters Callback: (AppleServerGetCompleteBlock)callback;

// post请求(application/x-www-form-urlencoded方式)
- (void)postUrlencodedWithPath:(NSString *)path Paramters:(NSDictionary *)paramters CompleteBlock:(AppleServerPostCompleteBlock)complete;

@end

NS_ASSUME_NONNULL_END

//
//  YDBaseVM.h
//  terasure
//
//  Created by 何英健 on 2019/11/15.
//  Copyright © 2019 何英健. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef enum : NSUInteger {
    BaseVMRefreshTypeNoMore, //下一页没有数据了
    BaseVMRefreshTypeNormal, //下一页还有数据
} BaseVMRefreshType;

typedef void (^BlockLoadDataDone) (void);
typedef void (^BlockLoadPageDataDone) (BaseVMRefreshType type);
typedef void (^BlockLoadDataFaild) (NSInteger code, NSString *error);

@interface YDBaseVM : NSObject

@end

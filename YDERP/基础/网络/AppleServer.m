//
//  AppleServer.m
//  YiDing
//
//  Created by Hang Baoan on 2018/12/26.
//  Copyright © 2018 com.rfchina.yiding. All rights reserved.
//

#import "AppleServer.h"
#import <AFNetworking.h>
#import <OpenUDID.h>

@implementation AppleServer {
    AFHTTPSessionManager* _manager;
    NSDate* loginDate;
}

+ (AppleServer *)shared {
    static dispatch_once_t once;
    static id instance;
    dispatch_once(&once, ^{
        instance = [[self alloc]init];
    });
    return instance;
}

- (instancetype)init
{
    self = [super init];
    if (self) {
        NSURLSessionConfiguration *configuration = [NSURLSessionConfiguration defaultSessionConfiguration];
        _manager = [[AFHTTPSessionManager alloc] initWithSessionConfiguration:configuration];
        _manager.responseSerializer.acceptableContentTypes = [NSSet setWithArray:@[@"text/plain", @"application/json"]];
    }
    return self;
}

- (void)postWithPath:(NSString*)path Paramters:(NSDictionary*)paramters CompleteBlock: (AppleServerPostCompleteBlock)complete
{
    NSError *error;
    
    NSMutableURLRequest *request = [[AFJSONRequestSerializer serializer] requestWithMethod:@"POST" URLString:path parameters:paramters error:&error];
    [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [AppleServer setUpRequest:request];
    
    _manager.requestSerializer = [AFJSONRequestSerializer serializer];
    NSURLSessionDataTask *dataTask = [_manager dataTaskWithRequest:request uploadProgress:nil downloadProgress:nil completionHandler:^(NSURLResponse * _Nonnull response, id  _Nullable responseObject, NSError * _Nullable error) {
        
        //主线程回调
        if (error == nil) {
            complete(YES, responseObject, nil);
            
        } else {
            complete(NO, nil, error.localizedDescription);
        }
    }];
    
    dispatch_queue_t queue = dispatch_queue_create("com.terasure.makePostRequestQueue", NULL);
    dispatch_async(queue, ^{
        [dataTask resume];
    });
}

- (void)getWithPath:(NSString *)path CompleteBlock:(AppleServerPostCompleteBlock)complete
{
    NSError* error;

    NSMutableURLRequest *request = [[AFJSONRequestSerializer serializer] requestWithMethod:@"GET" URLString:path parameters:nil error:&error];
    [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [AppleServer setUpRequest:request];
    
    _manager.requestSerializer = [AFJSONRequestSerializer serializer];
    NSURLSessionDataTask *dataTask = [_manager dataTaskWithRequest:request uploadProgress:nil downloadProgress:nil completionHandler:^(NSURLResponse * _Nonnull response, id  _Nullable responseObject, NSError * _Nullable error) {
        
        //主线程回调
        if (error == nil) {
            complete(YES, responseObject, nil);
        }else{
            complete(NO, nil, error.localizedDescription);
        }
    }];
    
    dispatch_queue_t queue = dispatch_queue_create("com.terasure.makeGetRequestQueue", NULL);
    dispatch_async(queue, ^{
        [dataTask resume];
    });
}

- (void)httpPostWithPath:(NSString*)path Paramters:(NSDictionary*)paramters CompleteBlock: (AppleServerPostCompleteBlock)complete
{
    NSError *error;

    NSMutableURLRequest *request = [[AFHTTPRequestSerializer serializer] requestWithMethod:@"POST" URLString:path parameters:paramters error:&error];
    [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [AppleServer setUpRequest:request];
    
    _manager.requestSerializer = [AFHTTPRequestSerializer serializer];
    NSURLSessionDataTask *dataTask = [_manager dataTaskWithRequest:request uploadProgress:nil downloadProgress:nil completionHandler:^(NSURLResponse * _Nonnull response, id  _Nullable responseObject, NSError * _Nullable error) {
        
        //主线程回调
        if (error == nil) {
            complete(YES, responseObject, nil);
            
        } else {
            complete(NO, nil, error.localizedDescription);
        }
    }];
    
    dispatch_queue_t queue = dispatch_queue_create("com.terasure.makePostRequestQueue", NULL);
    dispatch_async(queue, ^{
        [dataTask resume];
    });
}

- (void)getWithUrl:(NSString *)url Paramters:(NSDictionary *)paramters Callback: (AppleServerGetCompleteBlock)callback
{
    NSError *error;
    
    NSMutableURLRequest *request = [[AFJSONRequestSerializer serializer] requestWithMethod:@"GET" URLString:url parameters:paramters error:&error];
    [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [AppleServer setUpRequest:request];
    
    _manager.requestSerializer = [AFJSONRequestSerializer serializer];
    NSURLSessionDataTask *dataTask = [_manager dataTaskWithRequest:request uploadProgress:nil downloadProgress:nil completionHandler:^(NSURLResponse * _Nonnull response, id  _Nullable responseObject, NSError * _Nullable error) {

        //主线程回调
        if (error == nil) {
            callback(YES, responseObject, nil);
        }else{
            callback(NO, nil, error.localizedDescription);
        }
    }];
    
    dispatch_queue_t queue = dispatch_queue_create("com.terasure.makeGetRequestQueue", NULL);
    dispatch_async(queue, ^{
        [dataTask resume];
    });
}

- (void)postUrlencodedWithPath:(NSString *)path Paramters:(NSDictionary *)paramters CompleteBlock:(AppleServerPostCompleteBlock)complete
{
    AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc] initWithSessionConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
    [self setupUrlencodedRequest:manager.requestSerializer];

    [manager POST:path parameters:paramters headers:nil progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        complete(YES, responseObject, nil);
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        complete(NO, nil, error.localizedDescription);
    }];
}

+ (void)setUpRequest:(NSMutableURLRequest *)request
{
    request.timeoutInterval = 15;
    
    if (UserModel.loginToken.length) {
        [request setValue:UserModel.loginToken forHTTPHeaderField:@"x-access-token"];
    }
    if (UserModel.shopId > 0) {
        [request setValue:[NSString stringWithFormat:@"%ld", UserModel.shopId] forHTTPHeaderField:@"shopId"];
    }
    [request setValue:@"ios" forHTTPHeaderField:@"appType"];
    [request setValue:[OpenUDID value] forHTTPHeaderField:@"deviceId"];
    NSString *version = [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleShortVersionString"];
    [request setValue:version forHTTPHeaderField:@"version"];
}

- (void)setupUrlencodedRequest:(AFHTTPRequestSerializer *)serializer
{
    [serializer setValue:@"application/x-www-form-urlencoded; charset=UTF-8" forHTTPHeaderField:@"Content-Type"];
    serializer = [AFHTTPRequestSerializer serializer];
    serializer.timeoutInterval = 15;
    if (UserModel.loginToken.length) {
        [serializer setValue:UserModel.loginToken forHTTPHeaderField:@"x-access-token"];
    }
    if (UserModel.shopId > 0) {
        [serializer setValue:[NSString stringWithFormat:@"%ld", UserModel.shopId] forHTTPHeaderField:@"shopId"];
    }
    [serializer setValue:@"ios" forHTTPHeaderField:@"appType"];
    [serializer setValue:[OpenUDID value] forHTTPHeaderField:@"deviceId"];
    NSString *version = [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleShortVersionString"];
    [serializer setValue:version forHTTPHeaderField:@"version"];
}

@end

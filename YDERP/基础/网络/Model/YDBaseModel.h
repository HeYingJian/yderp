//
//  YDBaseModel.h
//  terasure
//
//  Created by 何英健 on 2019/11/14.
//  Copyright © 2019 何英健. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface YDBaseModel : NSObject

@property (nonatomic, assign) BOOL success;

@property (nonatomic, assign) NSInteger code;

@property (nonatomic, copy) NSString *message;

@property (nonatomic, copy) NSString *detailMessage;

@end


//
//Created by ESJsonFormatForMac on 19/01/23.
//

#import <Foundation/Foundation.h>

@interface AppleServerResponse : NSObject

@property (nonatomic, assign) BOOL success;

@property (nonatomic, copy) NSString *code;

@property (nonatomic, copy) NSString *message;

@property (nonatomic, copy) NSString *detailMessage;

@end

//
//  YDModifyMessagePopView.m
//  YDERP
//
//  Created by 何英健 on 2021/8/17.
//

#import "YDModifyMessagePopView.h"

@interface YDModifyMessagePopView () <UITextViewDelegate>

@property (nonatomic, copy) NSString *placeholder;
@property (nonatomic, copy) BlockModifyMessageComplete completion;
@property (nonatomic, copy) BlockLoadDataSucceed tapCloseBlock;

@property (nonatomic, weak) IBOutlet UIView *mainView;
@property (nonatomic, weak) IBOutlet UILabel *titleLabel;
@property (nonatomic, weak) IBOutlet UITextView *textView;
@property (nonatomic, weak) IBOutlet UILabel *placeholderLabel;
@property (nonatomic, weak) IBOutlet UIButton *confirmBtn;

@property (nonatomic, weak) IBOutlet NSLayoutConstraint *mainViewTop;

@end

@implementation YDModifyMessagePopView

+ (void)showWithTitle:(NSString *)title content:(NSString *)content placeholder:(NSString *)placeholder completion:(BlockModifyMessageComplete)completion tapCloseBlock:(BlockLoadDataSucceed)tapCloseBlock
{
    YDModifyMessagePopView *view = [[NSBundle mainBundle] loadNibNamed:@"YDModifyMessagePopView" owner:nil options:nil][0];
    view.frame = CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT);
    view.placeholder = placeholder;
    view.completion = completion;
    view.tapCloseBlock = tapCloseBlock;
    
    view.titleLabel.text = title;
    view.placeholderLabel.text = placeholder;
    view.placeholderLabel.hidden = (content.length)? YES : NO;
    view.textView.text = content;
    
    [view initUI];
    [view addNotification];
    [view.textView becomeFirstResponder];

    [KeyWindow addSubview:view];
}

- (void)initUI
{
    self.mainViewTop.constant = SCREEN_HEIGHT;
    [self layoutIfNeeded];
    
    // 添加上半部分圆角
    [self.mainView addRoundedCorners:UIRectCornerTopLeft|UIRectCornerTopRight withRadii:CGSizeMake(20, 20) viewRect:CGRectMake(0, 0, SCREEN_WIDTH, 200)];
    
    self.textView.contentInset = UIEdgeInsetsMake(4, 4, -4, -1);
}

- (void)hide
{
    [self.textView resignFirstResponder];
    
    [UIView animateWithDuration:0.3 animations:^{
        self.mainViewTop.constant = SCREEN_HEIGHT;
        [self layoutIfNeeded];
        
    } completion:^(BOOL finished) {
        [self removeFromSuperview];
    }];
}

- (IBAction)tapCloseBtn:(id)sender
{
    [self hide];
    
    if (self.tapCloseBlock) {
        self.tapCloseBlock();
    }
}

- (IBAction)tapConfirmBtn:(id)sender
{
    [self save];
}

- (void)save
{
    self.confirmBtn.userInteractionEnabled = NO;
    if (self.completion) {
        YDWeakSelf
        self.completion(self.textView.text, ^(BOOL succeed) {
            if (succeed) {
                [weakSelf hide];
                
            } else {
                weakSelf.confirmBtn.userInteractionEnabled = YES;
            }
        });
    }
}

#pragma mark - UITextViewDelegate

- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text
{
    if ([text isEqualToString:@"\n"]) {
        if (!textView.text.length) {
            [MBProgressHUD showToastMessage:@"内容不能为空"];
            return NO;
        }
        
        [self save];
        
        return NO;
    }
    
    return YES;
}

- (void)textViewDidChange:(UITextView *)textView
{
    self.placeholderLabel.hidden = (textView.text.length)? YES : NO;
}

#pragma mark - 键盘高度变化

- (void)addNotification
{
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardShow:) name:UIKeyboardWillShowNotification object:nil];
}

// 弹出键盘改变控制器view
- (void)keyboardShow:(NSNotification *)noti
{
    CGFloat keyboardHeight = [noti.userInfo[UIKeyboardFrameEndUserInfoKey] CGRectValue].size.height;
    [UIView animateWithDuration:0.0f animations:^{
        self.mainViewTop.constant = SCREEN_HEIGHT - self.mainView.height - keyboardHeight;
        [self layoutIfNeeded];
    }];
}

@end

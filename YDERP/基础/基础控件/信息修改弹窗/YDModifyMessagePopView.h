//
//  YDModifyMessagePopView.h
//  YDERP
//
//  Created by 何英健 on 2021/8/17.
//

#import <UIKit/UIKit.h>

typedef void(^BlockModifyMessageStatus)(BOOL succeed);
typedef void(^BlockModifyMessageComplete)(NSString *string, BlockModifyMessageStatus statusBlock);

@interface YDModifyMessagePopView : UIView

/**
 初始化
 @param title           标题
 @param content         预设内容
 @param placeholder     默认文案
 @param completion      点击保存的回调，返回值表示是否保存成功，可以隐藏
 @param tapCloseBlock   点击关闭的回调
 */
+ (void)showWithTitle:(NSString *)title content:(NSString *)content placeholder:(NSString *)placeholder completion:(BlockModifyMessageComplete)completion tapCloseBlock:(BlockLoadDataSucceed)tapCloseBlock;

@end

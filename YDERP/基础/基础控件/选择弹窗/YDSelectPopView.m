//
//  YDSelectPopView.m
//  YDERP
//
//  Created by 何英健 on 2021/8/7.
//

#import "YDSelectPopView.h"

@interface YDSelectPopView ()

@property (nonatomic, weak) IBOutlet UILabel *titleLabel;
@property (nonatomic, weak) IBOutlet UILabel *contentLabel;
@property (nonatomic, weak) IBOutlet UIButton *leftBtn;
@property (nonatomic, weak) IBOutlet UIButton *rightBtn;

@end

@implementation YDSelectPopView

+ (YDSelectPopView *)show
{
    YDSelectPopView *view = [[NSBundle mainBundle] loadNibNamed:@"YDSelectPopView" owner:nil options:nil][0];
    view.frame = CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT);
    [view initUI];
    [KeyWindow addSubview:view];
    
    return view;
}

- (void)hide
{
    [self removeFromSuperview];
}

- (void)initUI
{
    self.leftBtn.layer.borderColor = ColorFromRGB(0xDBDDE0).CGColor;
    self.leftBtn.layer.borderWidth = 1;
}

- (void)setTitle:(NSString *)title
{
    _title = title;
    self.titleLabel.text = title;
}

- (void)setContent:(NSString *)content
{
    _content = content;
    self.contentLabel.text = content;
}

- (void)setLeftBtnText:(NSString *)leftBtnText
{
    _leftBtnText = leftBtnText;
    [self.leftBtn setTitle:leftBtnText forState:UIControlStateNormal];
}

- (void)setRightBtnText:(NSString *)rightBtnText
{
    _rightBtnText = rightBtnText;
    [self.rightBtn setTitle:rightBtnText forState:UIControlStateNormal];
}

- (IBAction)tapLeftBtn:(id)sender
{
    if (self.tapLeftBtnBlock) {
        self.tapLeftBtnBlock();
    }
    
    [self hide];
}

- (IBAction)tapRightBtn:(id)sender
{
    if (self.tapRightBtnBlock) {
        self.tapRightBtnBlock();
    }
    
    [self hide];
}

@end

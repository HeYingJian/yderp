//
//  YDNormalSelectPopView.h
//  YDERP
//
//  Created by 何英健 on 2021/10/18.
//

#import <UIKit/UIKit.h>

typedef void(^BlockNormalSelectPopTapBtn)(void);

@interface YDNormalSelectPopView : UIView

@property (nonatomic, weak) IBOutlet YDVerticalAlignmentLabel *contentLabel;

@property (nonatomic, copy) NSString *content;
@property (nonatomic, copy) NSString *leftBtnText;
@property (nonatomic, copy) NSString *rightBtnText;
@property (nonatomic, copy) BlockNormalSelectPopTapBtn tapLeftBtnBlock;
@property (nonatomic, copy) BlockNormalSelectPopTapBtn tapRightBtnBlock;

+ (YDNormalSelectPopView *)show;

@end

//
//  YDSelectPopView.h
//  YDERP
//
//  Created by 何英健 on 2021/8/7.
//

#import <UIKit/UIKit.h>

typedef void(^BlockSelectPopTapBtn)(void);

@interface YDSelectPopView : UIView

@property (nonatomic, copy) NSString *title;
@property (nonatomic, copy) NSString *content;
@property (nonatomic, copy) NSString *leftBtnText;
@property (nonatomic, copy) NSString *rightBtnText;
@property (nonatomic, copy) BlockSelectPopTapBtn tapLeftBtnBlock;
@property (nonatomic, copy) BlockSelectPopTapBtn tapRightBtnBlock;

+ (YDSelectPopView *)show;

@end


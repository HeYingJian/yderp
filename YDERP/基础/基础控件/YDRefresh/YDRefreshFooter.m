//
//  YDRefreshFooter.m
//  YiDing
//
//  Created by 何英健 on 2019/10/18.
//  Copyright © 2019 com.rfchina.yiding. All rights reserved.
//

#import "YDRefreshFooter.h"

@implementation YDRefreshFooter

+ (instancetype)footerWithRefreshingTarget:(id)target refreshingAction:(SEL)action
{
    YDRefreshFooter *footer = [super footerWithRefreshingTarget:target refreshingAction:action];
    footer.height = 55;
    footer.stateLabel.textColor = ColorFromRGB(0xCCCCCC);
    footer.backgroundColor = ColorFromRGB(0xF8F8F8);
    
    return footer;
}   

@end

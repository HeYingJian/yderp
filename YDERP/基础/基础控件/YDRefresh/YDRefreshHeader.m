//
//  YDRefreshHeader.m
//  YiDing
//
//  Created by 何英健 on 2019/5/17.
//  Copyright © 2019年 YiDing. All rights reserved.
//

#import "YDRefreshHeader.h"

@interface YDRefreshHeader ()

@end

@implementation YDRefreshHeader

#pragma mark - 懒加载

- (UILabel *)msgLabel{
    if (!_msgLabel) {
        _msgLabel = [[UILabel alloc] init];
        _msgLabel.textColor = ColorFromRGB(0x999999);
        _msgLabel.font = [UIFont systemFontOfSize:12.0];
        _msgLabel.textAlignment = NSTextAlignmentCenter;
        [self addSubview:_msgLabel];
    }
    return _msgLabel;
}

- (UIImageView *)arrowImageView
{
    if (!_arrowImageView) {
        _arrowImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"MJ图标.gif"]];
        _arrowImageView.contentMode = UIViewContentModeScaleAspectFit;
        [self addSubview:_arrowImageView];
    }
    return _arrowImageView;
}

- (YYAnimatedImageView *)circleIndicatorView
{
    if (!_circleIndicatorView) {
        _circleIndicatorView = [[YYAnimatedImageView alloc] initWithFrame:CGRectZero];
        YYImage *img = [YYImage imageNamed:@"MJ图标.gif"];
        [_circleIndicatorView setImage:img];
        _circleIndicatorView.hidden = YES;
        [self addSubview:_circleIndicatorView];
    }
    return _circleIndicatorView;
}

- (void)setUseWhiteIcon:(BOOL)useWhiteIcon
{
    _useWhiteIcon = useWhiteIcon;
    
    if (useWhiteIcon) {
        YYImage *img = [YYImage imageNamed:@"白色MJ图标.gif"];
        [self.arrowImageView setImage:img];
        [self.circleIndicatorView setImage:img];
    }
}

+ (instancetype)headerWithRefreshingTarget:(id)target refreshingAction:(SEL)action
{
    YDRefreshHeader *header = [super headerWithRefreshingTarget:target refreshingAction:action];
    if (IS_PhoneXAll) {
        header.height = 54;
        
    } else {
        header.height = 34;
    }
    header.automaticallyChangeAlpha = YES;
    header.backgroundColor = ColorFromRGB(0xF8F8F8);
    
    return header;
}

- (void)placeSubviews
{
    [super placeSubviews];
    
    [self.msgLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self.mas_bottom).offset(-13);
        make.centerX.mas_equalTo(self).offset(20);
//        make.center.mas_equalTo(self);
    }];
    [self.arrowImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(self.msgLabel.mas_left).offset(-7);
        make.centerY.equalTo(self.msgLabel);
        make.width.mas_equalTo(33);
        make.height.mas_equalTo(33);
    }];
    [self.circleIndicatorView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.height.mas_equalTo(33);
        make.centerY.equalTo(self.arrowImageView);
        make.right.equalTo(self.msgLabel.mas_left).offset(-10);
    }];
}

- (void)setPullingPercent:(CGFloat)pullingPercent
{
    [super setPullingPercent:pullingPercent];
    self.arrowImageView.hidden = NO;
    self.circleIndicatorView.hidden = YES ;
}

- (void)setState:(MJRefreshState)state
{
    MJRefreshCheckState
    
    // 根据状态做事情
    if (state == MJRefreshStateRefreshing) {
        self.arrowImageView.hidden = YES;
        self.circleIndicatorView.hidden = NO;
        self.msgLabel.text = @"正在加载数据…";
        
        if (self.statesRefreshingBlock) {
            self.statesRefreshingBlock();
        }
        
    } else if (state == MJRefreshStateIdle) {
        self.arrowImageView.hidden = YES;
        self.circleIndicatorView.hidden = NO;
        self.msgLabel.text = @"松开即刷新";
        
        if (self.statesNormalBlock) {
            self.statesNormalBlock();
        }
        
    } else if (state == MJRefreshStatePulling) {
        self.arrowImageView.hidden = NO;
        self.circleIndicatorView.hidden = YES;
        self.msgLabel.text = @"松开即刷新";
    }
}

@end

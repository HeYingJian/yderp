//
//  YDRefreshHeader.h
//  YiDing
//
//  Created by 何英健 on 2019/5/17.
//  Copyright © 2019年 YiDing. All rights reserved.
//

#import <MJRefresh/MJRefresh.h>
#import "YDActivityIndicatorView.h"

typedef void(^BlockStatesRefreshing)(void);
typedef void(^BlockStatesNormal)(void);

@interface YDRefreshHeader : MJRefreshHeader

@property (nonatomic, assign) BOOL useWhiteIcon;

@property (nonatomic, copy) BlockStatesNormal statesNormalBlock;
@property (nonatomic, copy) BlockStatesRefreshing statesRefreshingBlock;

@property (nonatomic, strong) UILabel *msgLabel;

/** 圈圈*/
@property (nonatomic, strong) YYAnimatedImageView *circleIndicatorView;

/** 箭头*/
@property (nonatomic, strong) UIImageView *arrowImageView;

@end

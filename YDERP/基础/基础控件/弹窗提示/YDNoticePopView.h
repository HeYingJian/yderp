//
//  YDNoticePopView.h
//  YDERP
//
//  Created by 何英健 on 2021/9/10.
//

#import <UIKit/UIKit.h>

@interface YDNoticePopView : UIView

+ (void)showWithTitle:(NSString *)title completion:(BlockLoadDataDone)completion;

@end

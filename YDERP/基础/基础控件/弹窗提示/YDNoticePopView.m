//
//  YDNoticePopView.m
//  YDERP
//
//  Created by 何英健 on 2021/9/10.
//

#import "YDNoticePopView.h"

@interface YDNoticePopView ()

@property (nonatomic, copy) BlockLoadDataDone completion;

@property (nonatomic, weak) IBOutlet UILabel *titleLabel;

@end

@implementation YDNoticePopView

+ (void)showWithTitle:(NSString *)title completion:(BlockLoadDataDone)completion
{
    YDNoticePopView *view = [[NSBundle mainBundle] loadNibNamed:@"YDNoticePopView" owner:nil options:nil][0];
    view.frame = CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT);
    view.completion = completion;
    view.titleLabel.text = title;

    [KeyWindow addSubview:view];
}

- (IBAction)tapConfirmBtn:(id)sender
{
    if (self.completion) {
        self.completion();
    }
    
    [self removeFromSuperview];
}

@end

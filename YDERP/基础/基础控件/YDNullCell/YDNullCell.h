//
//  YDNullCell.h
//  YDERP
//
//  Created by 何英健 on 2021/7/21.
//

#import <UIKit/UIKit.h>

@interface YDNullCell : UITableViewCell

+ (instancetype)cellWithTableView:(UITableView *)tableView;

+ (CGFloat)getHeightWithTableView:(UITableView *)tableView;

@property (nonatomic, copy) NSString *content;
@property (nonatomic, assign) CGFloat originY;

@end


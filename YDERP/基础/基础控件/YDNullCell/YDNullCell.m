//
//  YDNullCell.m
//  YDERP
//
//  Created by 何英健 on 2021/7/21.
//

#import "YDNullCell.h"

@interface YDNullCell ()

@property (nonatomic, weak) IBOutlet UILabel *contentLabel;

@property (nonatomic, weak) IBOutlet NSLayoutConstraint *contentTop;

@end

@implementation YDNullCell

+ (instancetype)cellWithTableView:(UITableView *)tableView
{
    static NSString *reuseID = @"nullCell";
    YDNullCell *cell = [tableView dequeueReusableCellWithIdentifier:reuseID];
    if (!cell) {
        cell = [[NSBundle mainBundle] loadNibNamed:@"YDNullCell" owner:nil options:nil][0];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        cell.contentTop.constant = (tableView.height - 113) / 2 - 110;
    }
    
    return cell;
}

+ (CGFloat)getHeightWithTableView:(UITableView *)tableView
{
    return tableView.height;
}

- (void)setContent:(NSString *)content
{
    _content = content;
    
    self.contentLabel.text = content;
}

- (void)setOriginY:(CGFloat)originY
{
    _originY = originY;
    
    self.contentTop.constant = originY;
    [self layoutIfNeeded];
}

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end

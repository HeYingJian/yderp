//
//  YDWelcomViewController.m
//  YiDing
//
//  Created by Hang Baoan on 2018/12/12.
//  Copyright © 2018 com.rfchina.yiding. All rights reserved.
//

#import "YDWelcomViewController.h"

@interface YDWelcomViewController () <UIScrollViewDelegate>

@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;
@property (strong, nonatomic) NSArray<NSString*> *imageNames;
@property (weak, nonatomic) IBOutlet UIButton *enterHomePageButton;

@end

@implementation YDWelcomViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.imageNames = [self prepareForImageNames];
    [self prepareForScrollView];
}

- (void)prepareForScrollView
{
    for (int i = 0; i < self.imageNames.count; i++) {
        CGRect rect = CGRectMake(i * SCREEN_WIDTH, 0, SCREEN_WIDTH, SCREEN_HEIGHT);
        
        UIImageView* imageView = [[UIImageView alloc]initWithFrame:rect];
        imageView.image = [UIImage imageNamed:self.imageNames[i]];
        imageView.contentMode = UIViewContentModeScaleToFill;
        imageView.clipsToBounds = YES;
        [self.scrollView addSubview:imageView];
    }
    
    self.scrollView.contentSize = CGSizeMake(SCREEN_WIDTH * self.imageNames.count, SCREEN_HEIGHT);
    self.scrollView.delegate = self;
}

- (NSArray *)prepareForImageNames
{
    NSArray *fiveSizeArr = @[@"启动页面1-640x960",@"启动页面2-640x960",@"启动页面3-640x960"];
    // iphone6屏幕
    NSArray *fourSizeArr = @[@"启动页面1-750x1334",@"启动页面2-750x1334",@"启动页面3-750x1334"];
    // iphoneX屏幕
    NSArray *fiveFiveSizeArr = @[@"启动页面1-1125x2436",@"启动页面2-1125x2436",@"启动页面3-1125x2436"];
    // iphone6P屏幕
    NSArray *xSizeArr = @[@"启动页面1-1242x2208",@"启动页面2-1242x2208",@"启动页面3-1242x2208"];
    
    NSInteger widthInter = (NSInteger)SCREEN_WIDTH;
    int scale = (SCREEN_WIDTH / SCREEN_HEIGHT) * 100;
    
    if (scale == 46) {
        return fiveFiveSizeArr;
    }

    switch (widthInter) {
        case fourSizeiphone:
            return fourSizeArr;
        case fiveSizeiphone:
            return fiveSizeArr;
        case fiveFiveSizeiphone:
            return fiveFiveSizeArr;
            
        default:
            return xSizeArr;
    }
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    int page = (int) scrollView.contentOffset.x / SCREEN_WIDTH;
    
    if (page == 2) {
        self.enterHomePageButton.hidden = NO;
        
    } else {
        self.enterHomePageButton.hidden = YES;
    }
}

- (IBAction)enterHomePageAction:(UIButton *)sender
{
    [kAppDelegate initRootVC];
}

@end

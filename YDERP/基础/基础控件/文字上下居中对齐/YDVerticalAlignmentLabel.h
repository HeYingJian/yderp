//
//  YDVerticalAlignmentLabel.h
//  YDERP
//
//  Created by 何英健 on 2021/8/13.
//

#import <UIKit/UIKit.h>

typedef enum {
    VerticalAlignmentTop = 0, // default
    VerticalAlignmentMiddle,
    VerticalAlignmentBottom,
} VerticalAlignment;

@interface YDVerticalAlignmentLabel : UILabel
{
@private
VerticalAlignment _verticalAlignment;
}
 
@property (nonatomic) VerticalAlignment verticalAlignment;

@end

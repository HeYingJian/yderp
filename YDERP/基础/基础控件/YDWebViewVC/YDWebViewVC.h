//
//  YDWebViewVC.h
//  terasure
//
//  Created by 何英健 on 2019/11/15.
//  Copyright © 2019 何英健. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface YDWebViewVC : UIViewController

// isHideNav-是否隐藏头部(头部由h5自行搞定)
+ (void)showWebViewWithUrl:(NSString *)url isHideNav:(BOOL)isHideNav;

+ (void)showWebViewWithUrl:(NSString *)url isHideNav:(BOOL)isHideNav nav:(UINavigationController *)nav;

@end

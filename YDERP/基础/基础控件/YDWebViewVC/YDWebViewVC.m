//
//  YDWebViewVC.m
//  terasure
//
//  Created by 何英健 on 2019/11/15.
//  Copyright © 2019 何英健. All rights reserved.
//

#import "YDWebViewVC.h"
#import "WebViewJavascriptBridge.h"
#import "YDAddProductVC.h"
#import "YDLoginVC.h"
#import "YDOrderDetailVC.h"

@interface YDWebViewVC () <WKUIDelegate, WKNavigationDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate>

// 是否隐藏头部(头部由h5自行搞定)
@property (nonatomic, assign) BOOL isHideNav;

@property (nonatomic, copy) NSString *url;

@property (nonatomic, strong) WKWebView *webView;

@property (nonatomic, strong) UIProgressView *progressView;

@property (nonatomic, strong) WebViewJavascriptBridge *bridge;

// 保存回调方法
@property (nonatomic, strong) NSDictionary *responDict;

// 顶部状态栏颜色 0:白色 1:黑色
@property (nonatomic, assign) NSInteger statusBarType;

// 是否需要刷新网页
@property (nonatomic, assign) BOOL isNeedRefresh;

@end

@implementation YDWebViewVC

#pragma mark - 懒加载

- (WKWebView *)webView
{
    if (!_webView) {
        //创建网页配置对象
        WKWebViewConfiguration *config = [[WKWebViewConfiguration alloc] init];
        config.allowsInlineMediaPlayback = YES;
        
        _webView = [[WKWebView alloc] initWithFrame:CGRectMake(0, TopHeight, SCREEN_WIDTH, SCREEN_HEIGHT - TopHeight) configuration:config];
        // UI代理
        _webView.UIDelegate = self;
         // 导航代理
        _webView.navigationDelegate = self;
         // 是否允许手势左滑返回上一级, 类似导航控制的左滑返回
        _webView.allowsBackForwardNavigationGestures = YES;
        
        NSString *appVersion = [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleShortVersionString"];
        NSString *customUserAgent = [NSString stringWithFormat:@"iOS/ERP/%@", appVersion];
        [_webView setCustomUserAgent:customUserAgent];
        
        NSString *userLoginToken = UserModel.loginToken;
        
        if (userLoginToken.length) {
            NSString *cookieScriptDocument = [NSString stringWithFormat:@"document.cookie = '%@=%@';", @"token", userLoginToken];
            WKUserScript *cookieScript = [[WKUserScript alloc] initWithSource: cookieScriptDocument injectionTime:WKUserScriptInjectionTimeAtDocumentStart forMainFrameOnly:NO];
            [config.userContentController addUserScript:cookieScript];
        }
        
        [_webView addObserver:self forKeyPath:@"title" options:NSKeyValueObservingOptionNew context:nil];
        
        [self.view addSubview:_webView];
        
        [_webView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.leading.mas_equalTo(0);
            make.bottom.mas_equalTo(0);
            make.top.mas_equalTo(0);
            make.trailing.mas_equalTo(0);
        }];
        
        [WebViewJavascriptBridge enableLogging];
        
        //初始化WebViewJavascriptBridge设置代理
        self.bridge = [WebViewJavascriptBridge bridgeForWebView:_webView];
        [self.bridge setWebViewDelegate:self];
        
        //js调用oc的方法先注册
        [self jsCallOC];
    }
    
    return _webView;
}

- (UIProgressView *)progressView
{
    if (!_progressView) {
        _progressView = [[UIProgressView alloc] initWithProgressViewStyle:UIProgressViewStyleDefault];
        _progressView.tintColor = [UIColor colorWithHexString:@"0485d1"];
        _progressView.trackTintColor = [UIColor clearColor];
        _progressView.frame = CGRectMake(0, 0, self.view.bounds.size.width, 3.0);
        if (self.isHideNav) {
            _progressView.hidden = YES;
        }
        [_webView addSubview:_progressView];
    }
    
    return _progressView;
}

#pragma mark - 对外方法

+ (void)showWebViewWithUrl:(NSString *)url isHideNav:(BOOL)isHideNav
{
    [YDWebViewVC showWebViewWithUrl:url isHideNav:isHideNav nav:nil];
}

+ (void)showWebViewWithUrl:(NSString *)url isHideNav:(BOOL)isHideNav nav:(UINavigationController *)nav
{
    YDWebViewVC *webVC = [[YDWebViewVC alloc] init];
    webVC.url = url;
    webVC.isHideNav = isHideNav;
    webVC.hidesBottomBarWhenPushed = YES;
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:url]];
    if (UserModel.loginToken.length) {
        NSString *loginTokenStr = UserModel.loginToken;
        [request setValue:loginTokenStr forHTTPHeaderField:@"token"];
    }
    [webVC.webView loadRequest:request];
    
    if (nav) {
        kAppDelegate.currentNav = nav;
        [nav pushViewController:webVC animated:YES];
        
    } else {
        kAppDelegate.currentNav = nil;
        [[UINavigationController rootNavigationController] pushViewController:webVC animated:YES];
    }
}

#pragma mark - 页面加载

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self initUI];
    
    [self addObserver];
}

- (void)initUI
{
    self.statusBarType = 1;
    [self setNeedsStatusBarAppearanceUpdate];
    
    if (@available(iOS 11.0, *)) {
        self.webView.scrollView.contentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentNever;

    } else {
        self.automaticallyAdjustsScrollViewInsets = false;
    }
    
    [self.navigationItem addDefaultBackButton:self actionMethod:@selector(back)];
//    // 重新指定代理
//    self.navigationController.interactivePopGestureRecognizer.delegate = (id)self;
}

- (void)back
{
    WKBackForwardList *list = self.webView.backForwardList;
    
    if (list.backList.count != 0 ) {
        [self.webView goBack];
        
    } else {
        [self.navigationController popViewControllerAnimated:YES];
    }
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    self.navigationController.navigationBar.translucent = self.isHideNav;
    [self.navigationController setNavigationBarHidden:self.isHideNav animated:NO];
    
    if (self.isNeedRefresh) {
        self.isNeedRefresh = NO;
        [self.webView reload];
    }
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];

    if (!self.isHideNav) {
        self.navigationController.navigationBar.translucent = !self.isHideNav;
        [self.navigationController setNavigationBarHidden:!self.isHideNav animated:YES];
    }
}

- (void)addObserver
{
    //添加监测网页加载进度的观察者
    [self.webView addObserver:self forKeyPath:@"estimatedProgress" options:0 context:nil];
    
    //添加监测网页标题title的观察者
    [self.webView addObserver:self forKeyPath:@"title" options:NSKeyValueObservingOptionNew context:nil];
}

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    
    // 清除所有缓存
    [[WKWebsiteDataStore defaultDataStore] removeDataOfTypes:[WKWebsiteDataStore allWebsiteDataTypes] modifiedSince:[NSDate dateWithTimeIntervalSince1970:0] completionHandler:^{
    }];
}

// kvo 监听进度 必须实现此方法
- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary<NSKeyValueChangeKey,id> *)change context:(void *)context
{
    if ([keyPath isEqualToString:NSStringFromSelector(@selector(estimatedProgress))]
        && object == _webView) {
       NSLog(@"网页加载进度 = %f",_webView.estimatedProgress);
        self.progressView.progress = _webView.estimatedProgress;
        
        if (_webView.estimatedProgress >= 1.0f) {
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.3 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                self.progressView.progress = 0;
            });
        }
        
    } else if([keyPath isEqualToString:@"title"] && object == _webView) {
        self.navigationItem.title = _webView.title;
        
    } else {
        [super observeValueForKeyPath:keyPath ofObject:object change:change context:context];
    }
}

// 页面开始加载时调用
- (void)webView:(WKWebView *)webView didStartProvisionalNavigation:(WKNavigation *)navigation
{
    
}

// 页面加载失败时调用
- (void)webView:(WKWebView *)webView didFailProvisionalNavigation:(null_unspecified WKNavigation *)navigation withError:(NSError *)error
{
    [self.progressView setProgress:0.0f animated:NO];
}

//提交发生错误时调用
- (void)webView:(WKWebView *)webView didFailNavigation:(WKNavigation *)navigation withError:(NSError *)error
{
    [self.progressView setProgress:0.0f animated:NO];
}

// 根据WebView对于即将跳转的HTTP请求头信息和相关信息来决定是否跳转
- (void)webView:(WKWebView *)webView decidePolicyForNavigationAction:(WKNavigationAction *)navigationAction decisionHandler:(void (^)(WKNavigationActionPolicy))decisionHandler
{
    NSString * urlStr = navigationAction.request.URL.absoluteString;
    NSLog(@"发送跳转请求：%@",urlStr);
    
    decisionHandler(WKNavigationActionPolicyAllow);
}

// 根据客户端受到的服务器响应头以及response相关信息来决定是否可以跳转
- (void)webView:(WKWebView *)webView decidePolicyForNavigationResponse:(WKNavigationResponse *)navigationResponse decisionHandler:(void (^)(WKNavigationResponsePolicy))decisionHandler
{
    NSString * urlStr = navigationResponse.response.URL.absoluteString;
    NSLog(@"当前跳转地址：%@",urlStr);
    //允许跳转
    decisionHandler(WKNavigationResponsePolicyAllow);
    //不允许跳转
    //decisionHandler(WKNavigationResponsePolicyCancel);
}

// 需要响应身份验证时调用 同样在block中需要传入用户身份凭证
- (void)webView:(WKWebView *)webView didReceiveAuthenticationChallenge:(NSURLAuthenticationChallenge *)challenge completionHandler:(void (^)(NSURLSessionAuthChallengeDisposition disposition, NSURLCredential * _Nullable credential))completionHandler
{
    //用户身份信息
    NSURLCredential * newCred = [[NSURLCredential alloc] initWithUser:@"user123" password:@"123" persistence:NSURLCredentialPersistenceNone];
    //为 challenge 的发送方提供 credential
    [challenge.sender useCredential:newCred forAuthenticationChallenge:challenge];
    completionHandler(NSURLSessionAuthChallengeUseCredential,newCred);
}

/**
     *  web界面中有弹出警告框时调用
     *
     *  @param webView           实现该代理的webview
     *  @param message           警告框中的内容
     *  @param completionHandler 警告框消失调用
     */
- (void)webView:(WKWebView *)webView runJavaScriptAlertPanelWithMessage:(NSString *)message initiatedByFrame:(WKFrameInfo *)frame completionHandler:(void (^)(void))completionHandler
{
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"HTML的弹出框" message:message?:@"" preferredStyle:UIAlertControllerStyleAlert];
    [alertController addAction:([UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        completionHandler();
    }])];
    [self presentViewController:alertController animated:YES completion:nil];
}

// JavaScript调用confirm方法后回调的方法 confirm是js中的确定框，需要在block中把用户选择的情况传递进去
- (void)webView:(WKWebView *)webView runJavaScriptConfirmPanelWithMessage:(NSString *)message initiatedByFrame:(WKFrameInfo *)frame completionHandler:(void (^)(BOOL))completionHandler
{
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"" message:message?:@"" preferredStyle:UIAlertControllerStyleAlert];
    [alertController addAction:([UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
        completionHandler(NO);
    }])];
    [alertController addAction:([UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        completionHandler(YES);
    }])];
    [self presentViewController:alertController animated:YES completion:nil];
}

// JavaScript调用prompt方法后回调的方法 prompt是js中的输入框 需要在block中把用户输入的信息传入
- (void)webView:(WKWebView *)webView runJavaScriptTextInputPanelWithPrompt:(NSString *)prompt defaultText:(NSString *)defaultText initiatedByFrame:(WKFrameInfo *)frame completionHandler:(void (^)(NSString * _Nullable))completionHandler
{
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:prompt message:@"" preferredStyle:UIAlertControllerStyleAlert];
    [alertController addTextFieldWithConfigurationHandler:^(UITextField * _Nonnull textField) {
        textField.text = defaultText;
    }];
    [alertController addAction:([UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        completionHandler(alertController.textFields[0].text?:@"");
    }])];
    [self presentViewController:alertController animated:YES completion:nil];
}

// 页面是弹出窗口 _blank 处理
- (WKWebView *)webView:(WKWebView *)webView createWebViewWithConfiguration:(WKWebViewConfiguration *)configuration forNavigationAction:(WKNavigationAction *)navigationAction windowFeatures:(WKWindowFeatures *)windowFeatures
{
    if (!navigationAction.targetFrame.isMainFrame) {
        [webView loadRequest:navigationAction.request];
    }
    return nil;
}

- (BOOL)gestureRecognizerShouldBegin:(UIGestureRecognizer *)gestureRecognizer {
    if (self.navigationController.viewControllers.count == 1)
    {
        // 关闭主界面的右滑返回
        return NO;
    } else {
        return YES;
    }
}

#pragma mark - OC与JS相互调用

// 等待js调用oc 先注册这个方法
- (void)jsCallOC
{
    YDWeakSelf
    
    // 跳到首页
    [_bridge registerHandler:@"gotoHome" handler:^(id data, WVJBResponseCallback responseCallback) {
        [weakSelf showHome];
    }];
    
    // 获取设备信息
    [_bridge registerHandler:@"getDeviceInfo" handler:^(id data, WVJBResponseCallback responseCallback) {
        [weakSelf getDeviceInfoWithCallBack:responseCallback];
    }];
    
    // 返回客户首页
    [_bridge registerHandler:@"gotoCustomerIndex" handler:^(id data, WVJBResponseCallback responseCallback) {
        [weakSelf gotoCustomerIndex];
    }];
    
    // 跳到登录
    [_bridge registerHandler:@"gotoLogin" handler:^(id data, WVJBResponseCallback responseCallback) {
        [weakSelf showLoginWithDict:(NSDictionary *)data];
    }];
    
    // 跳到订单详情
    [_bridge registerHandler:@"gotoOrderDetail" handler:^(id data, WVJBResponseCallback responseCallback) {
        [weakSelf showOrderDetailWithDict:(NSDictionary *)data];
    }];
    
    // 跳到编辑商品
    [_bridge registerHandler:@"gotoEditProduct" handler:^(id data, WVJBResponseCallback responseCallback) {
        [weakSelf showEditProductWithDict:(NSDictionary *)data];
    }];
    
    // 选择图片
    [_bridge registerHandler:@"selectImage" handler:^(id data, WVJBResponseCallback responseCallback) {
        [weakSelf selectImageWithDict:(NSDictionary *)data];
    }];
   
    // 返回我的页面
    [_bridge registerHandler:@"gotoMine" handler:^(id data, WVJBResponseCallback responseCallback) {
        [weakSelf gotoMine];
    }];
    
    // 网页关闭
    [_bridge registerHandler:@"closeWebView" handler:^(id data, WVJBResponseCallback responseCallback) {
        [weakSelf closeWebView];
    }];
    
    // 修改系统UI
    [_bridge registerHandler:@"changeSystemUI" handler:^(id data, WVJBResponseCallback responseCallback) {
        [weakSelf changeSystemUIWithDict:(NSDictionary *)data];
    }];
    
    // 获取用户信息
    [_bridge registerHandler:@"getUser" handler:^(id data, WVJBResponseCallback responseCallback) {
        [weakSelf getUserInfoWithCallBack:responseCallback];
    }];
    
//    [_bridge registerHandler:@"模版" handler:^(id data, WVJBResponseCallback responseCallback) {
//        NSLog(@"js调用oc成功了参数为%@", data);
//        responseCallback(@"js调用oc后回调给js的参数");
//    }];
}

// oc调用js方法
- (void)registerOCCallJS
{
    //oc调js不带参数不带回调
    [_bridge callHandler:@"OCCallJS"];
    //oc调js带参数不带回调
    //    [_bridge callHandler:@"OCCallJSParameter" data:@"带参数不带回调"];
    //oc调js带参数带回调
    //    [_bridge callHandler:@"OCCallJSParameterBlock" data:@"带参数带回调" responseCallback:^(id response) {
    //        NSLog(@"testJavascriptHandler responded: %@", response);
    //    }];
}

#pragma mark - js方法

- (void)showHome
{
    if (kAppDelegate.currentNav) {
        [kAppDelegate.currentNav popToRootViewControllerAnimated:YES];
        
    } else {
        [[UINavigationController rootNavigationController] popToRootViewControllerAnimated:YES];
    }
    
    [UINavigationController rootNavigationController].tabBarController.selectedIndex = 0;
}

- (void)getDeviceInfoWithCallBack:(WVJBResponseCallback)callBack
{
    NSMutableDictionary *result = [NSMutableDictionary dictionary];
    [result setObject:[NSString stringWithFormat:@"%.0f", StatusBarHeight] forKey:@"statusBarHeight"];
    
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:result options:NSJSONWritingPrettyPrinted error:nil];
    NSString *str = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    callBack(str);
}

- (void)showLoginWithDict:(NSDictionary *)dict
{
    YDLoginVC *loginVC = [[YDLoginVC alloc] init];
    UINavigationController *nav = [[UINavigationController alloc] initWithRootViewController:loginVC];
    kAppDelegate.window.rootViewController = nav;
}

- (void)showOrderDetailWithDict:(NSDictionary *)dict
{
    YDOrderDetailVC *vc = [[YDOrderDetailVC alloc] init];
    vc.orderID = [dict objectForKey:@"orderId"];
    
    if (kAppDelegate.currentNav) {
        [kAppDelegate.currentNav pushViewController:vc animated:YES];
        
    } else {
        [[UINavigationController rootNavigationController] pushViewController:vc animated:YES];
    }
}

- (void)showEditProductWithDict:(NSDictionary *)dict
{
    YDAddProductVC *vc = [[YDAddProductVC alloc] init];
    vc.type = YDProductTypeEdit;
    vc.ID = [[dict objectForKey:@"id"] integerValue];
    YDWeakSelf
    vc.editSucceedBlock = ^{
        weakSelf.isNeedRefresh = YES;
    };
    
    if (kAppDelegate.currentNav) {
        [kAppDelegate.currentNav pushViewController:vc animated:YES];
        
    } else {
        [[UINavigationController rootNavigationController] pushViewController:vc animated:YES];
    }
}

- (void)selectImageWithDict:(NSDictionary *)dict
{
    self.responDict = dict;
    [self showPhotoAlbum];
}

- (void)gotoMine
{
    if (kAppDelegate.currentNav) {
        [kAppDelegate.currentNav popToRootViewControllerAnimated:YES];
        
    } else {
        [[UINavigationController rootNavigationController] popToRootViewControllerAnimated:YES];
    }
    
    [UINavigationController rootNavigationController].tabBarController.selectedIndex = 4;
}

- (void)gotoCustomerIndex
{
    if (kAppDelegate.currentNav) {
        [kAppDelegate.currentNav popToRootViewControllerAnimated:YES];
        
    } else {
        [[UINavigationController rootNavigationController] popToRootViewControllerAnimated:YES];
    }
    
    [UINavigationController rootNavigationController].tabBarController.selectedIndex = 3;
}

- (void)closeWebView
{
    if (kAppDelegate.currentNav) {
        [kAppDelegate.currentNav popViewControllerAnimated:YES];
        
    } else {
        [[UINavigationController rootNavigationController] popViewControllerAnimated:YES];
    }
}

- (void)changeSystemUIWithDict:(NSDictionary *)dict
{
    BOOL color = [[dict objectForKey:@"statusBarTextColor"] boolValue];
    if (color) {
        // 修改状态栏文字颜色为白色
        self.statusBarType = 0;
        
    } else {
        // 修改状态栏文字颜色为黑色
        self.statusBarType = 1;
    }
    [self setNeedsStatusBarAppearanceUpdate];
}

- (void)getUserInfoWithCallBack:(WVJBResponseCallback)callBack
{
    NSMutableDictionary *result = [NSMutableDictionary dictionary];
    if (UserModel.userIsLogin) {
        [result setObject:[NSString stringWithFormat:@"%ld", UserModel.ID] forKey:@"id"];
        [result setObject:UserModel.userName forKey:@"userName"];
        [result setObject:UserModel.name forKey:@"name"];
        [result setObject:UserModel.headImg forKey:@"headImg"];
        [result setObject:UserModel.phone forKey:@"phone"];
        [result setObject:UserModel.roleCode forKey:@"roleCode"];
        [result setObject:[NSString stringWithFormat:@"%ld", UserModel.bossId] forKey:@"bossId"];
        [result setObject:[NSString stringWithFormat:@"%ld", UserModel.shopId] forKey:@"shopId"];
        [result setObject:UserModel.shopName forKey:@"shopName"];
        [result setObject:UserModel.loginToken forKey:@"token"];
        [result setObject:UserModel.roleName forKey:@"roleName"];
        [result setObject:[NSString stringWithFormat:@"%ld", UserModel.roleId] forKey:@"roleId"];
    }
    
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:result options:NSJSONWritingPrettyPrinted error:nil];
    NSString *str = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    callBack(str);
}

- (UIStatusBarStyle)preferredStatusBarStyle
{
    if (self.statusBarType == 0) {
        return UIStatusBarStyleLightContent;
    }

    return UIStatusBarStyleDefault;
}

#pragma mark - 上传图片

// 显示相册选择
- (void)showPhotoAlbum
{
    if (![YDPhotoRight checkHavePhotoLibraryRight]) {
        return;
    }
    
    NSInteger photoNum = 1;
    if ([self.responDict containsObjectForKey:@"photoNum"]) {
        photoNum = [[self.responDict objectForKey:@"photoNum"] integerValue];
    }
    
    YDWeakSelf
    TZImagePickerController *imagePickerVc = [[TZImagePickerController alloc] initWithMaxImagesCount:photoNum delegate:nil];
    imagePickerVc.allowPickingOriginalPhoto = NO;
    imagePickerVc.showSelectedIndex = YES;
    imagePickerVc.naviTitleColor = ColorFromRGB(0x0095F7);
    imagePickerVc.barItemTextColor = ColorFromRGB(0x0095F7);
    imagePickerVc.allowPickingVideo = NO;
    imagePickerVc.modalPresentationStyle = UIModalPresentationFullScreen;
    [imagePickerVc setDidFinishPickingPhotosHandle:^(NSArray<UIImage *> *photos, NSArray *assets, BOOL isSelectOriginalPhoto) {
        if (photos.count == 1) {
            EECropImgVC *vc = [[EECropImgVC alloc] init];
            vc.originImage = photos[0];
            YDWeakSelf
            vc.cropImageSucceedBlock = ^(UIImage *image) {
                [weakSelf uploadImgArray:[NSArray arrayWithObject:image]];
            };
            [self.navigationController pushViewController:vc animated:YES];
            
        } else {
            [weakSelf uploadImgArray:photos];
        }
    }];
    
    if (kAppDelegate.currentNav) {
        [kAppDelegate.currentNav presentViewController:imagePickerVc animated:YES completion:nil];
        
    } else {
        [[UINavigationController rootNavigationController] presentViewController:imagePickerVc animated:YES completion:nil];
    }
}

// 上传图片
- (void)uploadImgArray:(NSArray<UIImage *> *)imgArray
{
    [YDLoadingView showToSuperview:self.view];
    
    NSString *groupStr;
    if ([self.responDict containsObjectForKey:@"group"]) {
        groupStr = [self.responDict objectForKey:@"group"];
    }
    
    YDWeakSelf
    [YDFunction uploadImageArray:imgArray type:1 group:-1 groupStr:groupStr parameters:nil sucess:^(NSMutableArray<NSString *> *urlArray) {
        [YDLoadingView hideToSuperview:weakSelf.view];
        
        NSString *successStr = [self.responDict objectForKey:@"success"];
        if (successStr.length) {
            NSData *jsonData = [NSJSONSerialization dataWithJSONObject:urlArray options:NSJSONWritingPrettyPrinted error:nil];
            NSString *str = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
            [weakSelf.bridge callHandler:successStr data:str];
        }
        
    } failure:^(NSString *msg) {
        [YDLoadingView hideToSuperview:weakSelf.view];
        [MBProgressHUD showToastMessage:msg];
        
        NSString *failStr = [self.responDict objectForKey:@"fail"];
        if (failStr.length) {
            [weakSelf.bridge callHandler:failStr];
        }
    }];
}

@end

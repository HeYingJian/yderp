//
//  YDNavigationController.m
//  YDERP
//
//  Created by 何英健 on 2021/9/6.
//

#import "YDNavigationController.h"

@interface YDNavigationController () <UIGestureRecognizerDelegate>

@end

@implementation YDNavigationController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    //  这句很核心 稍后讲解
    id target = self.interactivePopGestureRecognizer.delegate;
    //  这句很核心 稍后讲解
    SEL handler = NSSelectorFromString(@"handleNavigationTransition:");
    //  获取添加系统边缘触发手势的View
    UIView *targetView = self.interactivePopGestureRecognizer.view;
    
    //  创建pan手势 作用范围是全屏
    UIPanGestureRecognizer * fullScreenGes = [[UIPanGestureRecognizer alloc]initWithTarget:target action:handler];
    fullScreenGes.delegate = self;
    [targetView addGestureRecognizer:fullScreenGes];
    
    // 关闭边缘触发手势 防止和原有边缘手势冲突
    [self.interactivePopGestureRecognizer setEnabled:NO];
}

//  防止导航控制器只有一个rootViewcontroller时触发手势
- (BOOL)gestureRecognizerShouldBegin:(UIGestureRecognizer *)gestureRecognizer
{
    if ([gestureRecognizer isKindOfClass:[UIPanGestureRecognizer class]]) {
        //解决与左滑手势冲突
        CGPoint translation = [(UIPanGestureRecognizer *)gestureRecognizer translationInView:gestureRecognizer.view];
        if (translation.x <= 0) {
            return NO;
        }
        // 过滤执行过渡动画时的手势处理
       if ([[self valueForKey:@"_isTransitioning"] boolValue]) {
            return NO;
       }

        return self.childViewControllers.count == 1 ? NO : YES;
    }
    
    return YES;
}

@end

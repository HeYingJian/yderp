//
//  EETabBarController.m
//  EEducation
//
//  Created by 何英健 on 2020/10/21.
//

#import "EETabBarController.h"
#import "YDHomeVC.h"
#import "YDProductVC.h"
#import "YDMakeListVC.h"
#import "YDCustomerVC.h"
#import "YDMineVC.h"
#import "YDNavigationController.h"

@interface EETabBarController () <UITabBarControllerDelegate>

@end

@implementation EETabBarController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self addSubViewControllers];
}

- (void)addSubViewControllers
{
    self.delegate = self;
    
    YDHomeVC *vc1 = [[YDHomeVC alloc] init];
    YDProductVC *vc2 = [[YDProductVC alloc] init];
    UIViewController *vc3 = [[UIViewController alloc] init];
    YDCustomerVC *vc4 = [[YDCustomerVC alloc] init];
    YDMineVC *vc5 = [[YDMineVC alloc] init];

    YDNavigationController *nav1 = [[YDNavigationController alloc] initWithRootViewController:vc1];
    YDNavigationController *nav2 = [[YDNavigationController alloc] initWithRootViewController:vc2];
    YDNavigationController *nav3 = [[YDNavigationController alloc] initWithRootViewController:vc3];
    YDNavigationController *nav4 = [[YDNavigationController alloc] initWithRootViewController:vc4];
    YDNavigationController *nav5 = [[YDNavigationController alloc] initWithRootViewController:vc5];
    self.viewControllers = @[nav1,nav2,nav3,nav4,nav5];

    nav1.tabBarItem = [[UITabBarItem alloc] initWithTitle:@"首页" image:[[UIImage imageNamed:@"首页_未选中"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal] selectedImage:[[UIImage imageNamed:@"首页_选中"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal]];
    nav1.tabBarItem.tag = 1;
    
    nav2.tabBarItem = [[UITabBarItem alloc] initWithTitle:@"商品" image:[[UIImage imageNamed:@"商品_未选中"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal] selectedImage:[[UIImage imageNamed:@"商品_选中"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal]];
    nav2.tabBarItem.tag = 2;
    
    nav3.tabBarItem = [[UITabBarItem alloc] initWithTitle:@"" image:[[UIImage imageNamed:@"开单主按钮"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal] selectedImage:[[UIImage imageNamed:@"开单主按钮"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal]];
    nav3.tabBarItem.tag = 3;
//    nav3.tabBarItem.imageInsets = UIEdgeInsetsMake(5, 0, -5, 0);
    
    nav4.tabBarItem = [[UITabBarItem alloc] initWithTitle:@"客户" image:[[UIImage imageNamed:@"客户_未选中"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal] selectedImage:[[UIImage imageNamed:@"客户_选中"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal]];
    nav4.tabBarItem.tag = 4;
    
    nav5.tabBarItem = [[UITabBarItem alloc] initWithTitle:@"我的" image:[[UIImage imageNamed:@"我的_未选中"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal] selectedImage:[[UIImage imageNamed:@"我的_选中"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal]];
    nav5.tabBarItem.tag = 5;

    for (UIBarItem *item in self.tabBar.items) {
        [item setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:
         [UIFont systemFontOfSize:10.0 weight:UIFontWeightMedium], NSFontAttributeName, COLOR_MAIN_BULE, NSForegroundColorAttributeName, nil]
         forState:UIControlStateSelected];
    }

    [[UITabBar appearance] setUnselectedItemTintColor:RGBCOLOR(215, 217, 224)];
    [[UITabBar appearance] setTranslucent:NO];
}

- (BOOL)tabBarController:(UITabBarController *)tabBarController shouldSelectViewController:(UIViewController *)viewController
{
    // 如果点击了中间的controller
    if (viewController.tabBarItem.tag == 3) {
        if (UserModel.feePlanExpire == 0) {
            if ([YDLimit limitOrder]) {
                YDMakeListVC *vc = [[YDMakeListVC alloc] init];
                
                YDNavigationController *nav = [[YDNavigationController alloc] initWithRootViewController:vc];
                nav.modalPresentationStyle = UIModalPresentationFullScreen;
                [self presentViewController:nav animated:YES completion:nil];
            }
            
        } else {
            [YDAccountPackagePopView showWithType:YDAccountPopType2];
        }

        // 不允许选择这个页面页面
        return NO;
        
    } else if (viewController.tabBarItem.tag == 2) {
        if (![YDLimit limitProduct]) {
            return NO;
        }
        
    } else if (viewController.tabBarItem.tag == 4) {
        if (![YDLimit limitCustomer]) {
            return NO;
        }
    }
        
    return YES;
}

- (nullable UIViewController *)childViewControllerForStatusBarStyle
{
    UINavigationController *nav = (UINavigationController *)self.selectedViewController;
    return nav.topViewController;
}

@end

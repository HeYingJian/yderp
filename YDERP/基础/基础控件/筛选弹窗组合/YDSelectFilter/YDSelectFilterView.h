//
//  YDSelectFilterView.h
//  YDERP
//
//  Created by 何英健 on 2021/7/26.
//

#import <UIKit/UIKit.h>

typedef enum : NSUInteger {
    YDSelectFilterTypeProduct, // 商品管理页的筛选(数据请求项+状态项)
    YDSelectFilterTypeCustomer, // 客户管理页的筛选(状态项+有无欠款项+有无欠货项)
    YDSelectFilterTypeOrder  // 订单列表的删选(日期+显示+按订单欠款+按订单欠货)
} YDSelectFilterType;

typedef void(^BlockSelectedCompletion)(NSMutableArray *array, NSInteger status, NSInteger moneyOwe, NSInteger goodsOwe, NSString *startTime, NSString *endTime, NSInteger orderStatus, NSInteger arrears, NSInteger dueGoods);
typedef void(^BlockFilerShow)(void);

@interface YDSelectFilterView : UIView

// 初始化
+ (YDSelectFilterView *)createWithType:(YDSelectFilterType)type selectedCompletion:(BlockSelectedCompletion)completion;
// 显示筛选
- (void)showWithBlock:(BlockFilerShow)block;
// 清除筛选数据，下次进入会重新请求（针对商品）
- (void)clearData;

@end


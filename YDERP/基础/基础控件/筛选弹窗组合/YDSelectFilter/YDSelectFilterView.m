//
//  YDSelectFilterView.m
//  YDERP
//
//  Created by 何英健 on 2021/7/26.
//

#import "YDSelectFilterView.h"
#import "YDSearcherPropListModel.h"

typedef void(^BlockLoadFilterDataSucceed)(void);

@interface YDSelectFilterView ()

@property (nonatomic, assign) YDSelectFilterType type;

@property (nonatomic, strong) NSMutableArray<YDSearcherPropList *> *data;

// 记录订单筛选开始时间
@property (nonatomic, copy) NSString *orderStartTime;
@property (nonatomic, strong) UIButton *startTimebtn;
// 记录订单筛选结束时间
@property (nonatomic, copy) NSString *orderEndTime;
@property (nonatomic, strong) UIButton *endTimebtn;

@property (nonatomic, copy) BlockLoadFilterDataSucceed loadDataCompletion;
@property (nonatomic, copy) BlockSelectedCompletion selectedCompletion;

// 存储当前所有按钮的数据
@property (nonatomic, strong) NSMutableArray<NSMutableArray *> *btnArray;

@property (nonatomic, strong) UIView *BGView;
@property (nonatomic, strong) UIView *mainView;
@property (nonatomic, strong) UIScrollView *scrollView;

@property (nonatomic, strong) UIDatePicker *datePicker;

@end

@implementation YDSelectFilterView

#pragma mark - 懒加载

- (NSMutableArray<NSMutableArray *> *)btnArray
{
    if (!_btnArray) {
        _btnArray = [NSMutableArray array];
    }
    
    return _btnArray;
}

- (UIView *)BGView
{
    if (!_BGView) {
        _BGView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT)];
        _BGView.backgroundColor = [UIColor blackColor];
        _BGView.alpha = 0.7;
        
        UITapGestureRecognizer *tapBG = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapConfirmBtn)];
        [_BGView addGestureRecognizer:tapBG];
        
        [self insertSubview:_BGView atIndex:0];
    }
    
    return _BGView;
}

- (UIView *)mainView
{
    if (!_mainView) {
        _mainView = [[UIView alloc] initWithFrame:CGRectMake(SCREEN_WIDTH, 0, SCREEN_WIDTH - 75, SCREEN_HEIGHT)];
        _mainView.backgroundColor = [UIColor whiteColor];
        
        UILabel *label = [[UILabel alloc] init];
        label.text = @"筛选";
        label.font = [UIFont systemFontOfSize:19 weight:UIFontWeightSemibold];
        label.textColor = ColorFromRGB(0x222222);
        [label sizeToFit];
        CGFloat labelOriginY = 25;
        if (IS_PhoneXAll) {
            labelOriginY += 30;
        }
        [label setOrigin:CGPointMake((_mainView.width - label.width) / 2, labelOriginY)];
        [_mainView addSubview:label];
        
        UIButton *resetBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        CGFloat btnW = (SCREEN_WIDTH - 75 - 24 * 2 - 11) / 2;
        CGFloat originY = SCREEN_HEIGHT - 45 - 10;
        if (IS_PhoneXAll) {
            originY -= 10;
        }
        [resetBtn setFrame:CGRectMake(24, originY, btnW, 45)];
        [resetBtn setTitle:@"重置" forState:UIControlStateNormal];
        [resetBtn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        resetBtn.titleLabel.font = [UIFont systemFontOfSize:17];
        [resetBtn setBackgroundColor:[UIColor whiteColor]];
        resetBtn.layer.borderColor = ColorFromRGB(0xE2E2E2).CGColor;
        resetBtn.layer.borderWidth = 1.5;
        resetBtn.layer.cornerRadius = resetBtn.height / 2;
        [resetBtn addTarget:self action:@selector(tapResetBtn) forControlEvents:UIControlEventTouchUpInside];
        [_mainView addSubview:resetBtn];
        
        UIButton *confirmBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        [confirmBtn setFrame:CGRectMake(CGRectGetMaxX(resetBtn.frame) + 11, resetBtn.y, resetBtn.width, resetBtn.height)];
        [confirmBtn setTitle:@"确定" forState:UIControlStateNormal];
        confirmBtn.titleLabel.font = [UIFont systemFontOfSize:17];
        [confirmBtn setBackgroundColor:ColorFromRGB(0x3A4BF0)];
        confirmBtn.layer.cornerRadius = confirmBtn.height / 2;
        [confirmBtn addTarget:self action:@selector(tapConfirmBtn) forControlEvents:UIControlEventTouchUpInside];
        [_mainView addSubview:confirmBtn];
        
        CGFloat scrollViewOriginY = CGRectGetMaxY(label.frame) + 10;
        self.scrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(15, scrollViewOriginY, _mainView.width - 15 * 2, _mainView.height - resetBtn.height - 10 - scrollViewOriginY)];
        [_mainView addSubview:_scrollView];
        
        [self insertSubview:_mainView aboveSubview:self.BGView];
    }
    
    return _mainView;
}

- (UIDatePicker *)datePicker
{
    if (!_datePicker) {
        _datePicker = [[UIDatePicker alloc] initWithFrame:CGRectMake(0, 280, SCREEN_WIDTH, 200)];
        _datePicker.datePickerMode = UIDatePickerModeDate;
        _datePicker.locale = [[NSLocale alloc] initWithLocaleIdentifier:@"zh_CN"];
        if (@available(iOS 13.4, *)) {
            _datePicker.preferredDatePickerStyle = UIDatePickerStyleWheels;
        }
    }
    return _datePicker;
}

#pragma mark - 对外方法

+ (YDSelectFilterView *)createWithType:(YDSelectFilterType)type selectedCompletion:(BlockSelectedCompletion)completion
{
    YDSelectFilterView *view = [[YDSelectFilterView alloc] init];
    view.type = type;
    view.selectedCompletion = completion;
    view.frame = CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT);
    view.backgroundColor = [UIColor clearColor];

    [view BGView];
    [view mainView];
    
    if (type == YDSelectFilterTypeProduct) {
        // 预加载筛选数据
        [view loadData];
        
    } else if (type == YDSelectFilterTypeCustomer) {
        // 不需要请求
        view.data = [NSMutableArray array];
        [view addStatusData];
        [view addOweData];
        [view refreshUI];
        
    } else if (type == YDSelectFilterTypeOrder) {
        // 不需要请求
        view.data = [NSMutableArray array];
        [view addOrderStatusData];
        [view addOrderArrearsData];
        [view addOrderDueGoodsData];
        [view refreshUI];
    }

    return view;
}

- (void)showWithBlock:(BlockFilerShow)block
{
    if (self.data) {
        [self showFilterViewWithBlock:block];
        
    } else {
        YDWeakSelf
        self.loadDataCompletion = ^{
            [YDLoadingView hideToSuperview:nil];
            [weakSelf showFilterViewWithBlock:block];
        };
        
        [YDLoadingView showToSuperview:nil];
        [self loadData];
    }
}

// 显示筛选界面
- (void)showFilterViewWithBlock:(BlockFilerShow)block
{
    if (block) {
        block();
    }
    
    [KeyWindow addSubview:self];
    
    [UIView animateWithDuration:0.3f animations:^{
        [self.mainView setX:75];
    }];
}

- (void)hideFilterView
{
    [UIView animateWithDuration:0.3f animations:^{
        [self.mainView setX:SCREEN_WIDTH];
        
    } completion:^(BOOL finished) {
        [self removeFromSuperview];
    }];
}

#pragma mark - 数据来源

- (void)loadData
{
    NSString *path = URL_SEARCH_PROPERTY;
    
    NSMutableDictionary *param = [NSMutableDictionary dictionary];

    YDWeakSelf
    AppleServerPostCompleteBlock postHandleBlock = ^(BOOL done, id _Nullable object, NSString * _Nullable error) {
        //接口调用成功
        if (done) {
            YDSearcherPropListModel *resp = [YDSearcherPropListModel modelWithJSON:object];
            
            if (resp.code == 200) {
                weakSelf.data = resp.data;
                
                [weakSelf addStatusData];
                [weakSelf refreshUI];
                
                if (self.loadDataCompletion) {
                    self.loadDataCompletion();
                }
            
            } else {
                NSString *msg;
                if (resp.detailMessage.length) {
                    msg = resp.detailMessage;
                    
                } else if (resp.message.length) {
                    msg = resp.message;
                    
                } else {
                    msg = @"加载失败";
                }
                [MBProgressHUD showToastMessage:msg];
            }
            
        } else {
            [MBProgressHUD showToastMessage:@"网络不给力"];
        }
    };
    
    [AppleServer.shared getWithUrl:path Paramters:param Callback:postHandleBlock];
}

// 添加状态选择项
- (void)addStatusData
{
    YDSearcherPropList *propData = [[YDSearcherPropList alloc] init];
    propData.propName = @"状态";
    [self.data addObject:propData];

    YDSearcherPropValueList *subData1 = [[YDSearcherPropValueList alloc] init];
    subData1.propValue = @"全部";
    YDSearcherPropValueList *subData2 = [[YDSearcherPropValueList alloc] init];
    subData2.propValue = @"启用";
    YDSearcherPropValueList *subData3 = [[YDSearcherPropValueList alloc] init];
    subData3.propValue = @"停用";
    propData.propValueList = [NSMutableArray arrayWithObjects:subData1, subData2, subData3, nil];
}

// 添加欠款与欠货项(客户管理筛选专用)
- (void)addOweData
{
    // 有无欠款
    YDSearcherPropList *moneyOweData = [[YDSearcherPropList alloc] init];
    moneyOweData.propName = @"有无欠款";
    [self.data addObject:moneyOweData];

    YDSearcherPropValueList *moneyData1 = [[YDSearcherPropValueList alloc] init];
    moneyData1.propValue = @"全部";
    YDSearcherPropValueList *moneyData2 = [[YDSearcherPropValueList alloc] init];
    moneyData2.propValue = @"无欠款";
    YDSearcherPropValueList *moneyData3 = [[YDSearcherPropValueList alloc] init];
    moneyData3.propValue = @"有欠款";
    moneyOweData.propValueList = [NSMutableArray arrayWithObjects:moneyData1, moneyData2, moneyData3, nil];
    
    // 有无欠货
    YDSearcherPropList *goodsOweData = [[YDSearcherPropList alloc] init];
    goodsOweData.propName = @"有无欠该客户货";
    [self.data addObject:goodsOweData];
    
    YDSearcherPropValueList *goodsData1 = [[YDSearcherPropValueList alloc] init];
    goodsData1.propValue = @"全部";
    YDSearcherPropValueList *goodsData2 = [[YDSearcherPropValueList alloc] init];
    goodsData2.propValue = @"无欠货";
    YDSearcherPropValueList *goodsData3 = [[YDSearcherPropValueList alloc] init];
    goodsData3.propValue = @"有欠货";
    goodsOweData.propValueList = [NSMutableArray arrayWithObjects:goodsData1, goodsData2, goodsData3, nil];
}

// 添加订单状态选择
- (void)addOrderStatusData
{
    YDSearcherPropList *propData = [[YDSearcherPropList alloc] init];
    propData.propName = @"显示";
    [self.data addObject:propData];

    YDSearcherPropValueList *subData1 = [[YDSearcherPropValueList alloc] init];
    subData1.propValue = @"全部";
    YDSearcherPropValueList *subData2 = [[YDSearcherPropValueList alloc] init];
    subData2.propValue = @"待配货";
    YDSearcherPropValueList *subData3 = [[YDSearcherPropValueList alloc] init];
    subData3.propValue = @"部分配货";
    YDSearcherPropValueList *subData4 = [[YDSearcherPropValueList alloc] init];
    subData4.propValue = @"已配齐";
    propData.propValueList = [NSMutableArray arrayWithObjects:subData1, subData2, subData3, subData4, nil];
}

// 添加订单是否欠款选择
- (void)addOrderArrearsData
{
    YDSearcherPropList *propData = [[YDSearcherPropList alloc] init];
    propData.propName = @"按订单欠款";
    [self.data addObject:propData];

    YDSearcherPropValueList *subData1 = [[YDSearcherPropValueList alloc] init];
    subData1.propValue = @"全部";
    YDSearcherPropValueList *subData2 = [[YDSearcherPropValueList alloc] init];
    subData2.propValue = @"有欠款";
    YDSearcherPropValueList *subData3 = [[YDSearcherPropValueList alloc] init];
    subData3.propValue = @"无欠款";
    propData.propValueList = [NSMutableArray arrayWithObjects:subData1, subData2, subData3, nil];
}

// 添加订单是否欠货选择
- (void)addOrderDueGoodsData
{
    YDSearcherPropList *propData = [[YDSearcherPropList alloc] init];
    propData.propName = @"按订单欠货";
    [self.data addObject:propData];

    YDSearcherPropValueList *subData1 = [[YDSearcherPropValueList alloc] init];
    subData1.propValue = @"全部";
    YDSearcherPropValueList *subData2 = [[YDSearcherPropValueList alloc] init];
    subData2.propValue = @"有欠货";
    YDSearcherPropValueList *subData3 = [[YDSearcherPropValueList alloc] init];
    subData3.propValue = @"无欠货";
    propData.propValueList = [NSMutableArray arrayWithObjects:subData1, subData2, subData3, nil];
}

- (void)clearData
{
    if (self.type == YDSelectFilterTypeProduct) {
        self.data = nil;
        
    } else {
        self.data = [NSMutableArray array];
    }
}

#pragma mark - 页面构造

- (void)refreshUI
{
    [self.scrollView removeAllSubviews];
    [self.btnArray removeAllObjects];
    self.orderStartTime = nil;
    self.orderEndTime = nil;
    
    CGFloat originY = 0;
    if (self.type == YDSelectFilterTypeOrder) {
        // 订单筛选第一项是时间选择
        UIView *subView = [self createTimeSelectView];
        [subView setY:originY];
        originY = CGRectGetMaxY(subView.frame) + 25;
    }

    // 其他选择项
    for (int i = 0; i < self.data.count; i++) {
        UIView *subView = [self createSubViewWithIndex:i];
        if (i == 0) {
            [subView setY:originY];
            
        } else {
            [subView setY:originY + 25];
        }
        
        if (i == self.data.count - 1) {
            [self.scrollView setContentSize:CGSizeMake(self.scrollView.width, CGRectGetMaxY(subView.frame) + 10)];
            
        } else {
            originY = CGRectGetMaxY(subView.frame);
        }
    }
}

// 创建时间选择
- (UIView *)createTimeSelectView
{
    UIView *view = [[UIView alloc] init];
    [view setWidth:self.scrollView.width];
    [view setHeight:67];
    [self.scrollView addSubview:view];
    
    UILabel *title = [[UILabel alloc] init];
    title.font = [UIFont systemFontOfSize:17 weight:UIFontWeightSemibold];
    title.textColor = ColorFromRGB(0x222222);
    title.text = @"日期";
    [title sizeToFit];
    [title setY:0];
    [view addSubview:title];
    
    self.startTimebtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [_startTimebtn setWidth:(self.mainView.width - 15 * 2 - 20) / 2];
    [_startTimebtn setHeight:32];
    [_startTimebtn setY:35];
    [_startTimebtn setX:0];
    [_startTimebtn setTitle:@"开始日期" forState:UIControlStateNormal];
    _startTimebtn.layer.cornerRadius = _startTimebtn.height / 2;
    [_startTimebtn setBackgroundColor:ColorFromRGB(0xF4F4F4)];
    [_startTimebtn setTitleColor:ColorFromRGB(0xC1C1C6) forState:UIControlStateNormal];
    [_startTimebtn setTitleColor:ColorFromRGB(0x222222) forState:UIControlStateSelected];
    _startTimebtn.titleLabel.font = FONT_NUMBER(12);
    [_startTimebtn setTarget:self action:@selector(tapStartTimeBtn) forControlEvents:UIControlEventTouchUpInside];
    [view addSubview:_startTimebtn];
    
    UIView *lineView = [[UIView alloc] initWithFrame:CGRectMake(CGRectGetMaxX(_startTimebtn.frame) + 7, _startTimebtn.y + _startTimebtn.height / 2, 6, 1)];
    lineView.backgroundColor = ColorFromRGB(0x222222);
    [view addSubview:lineView];
    
    self.endTimebtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [_endTimebtn setFrame:_startTimebtn.frame];
    [_endTimebtn setX:CGRectGetMaxX(_startTimebtn.frame) + 20];
    [_endTimebtn setTitle:@"结束日期" forState:UIControlStateNormal];
    _endTimebtn.layer.cornerRadius = _endTimebtn.height / 2;
    [_endTimebtn setBackgroundColor:ColorFromRGB(0xF4F4F4)];
    [_endTimebtn setTitleColor:ColorFromRGB(0xC1C1C6) forState:UIControlStateNormal];
    [_endTimebtn setTitleColor:ColorFromRGB(0x222222) forState:UIControlStateSelected];
    _endTimebtn.titleLabel.font = FONT_NUMBER(12);
    [_endTimebtn setTarget:self action:@selector(tapEndTimeBtn) forControlEvents:UIControlEventTouchUpInside];
    [view addSubview:_endTimebtn];
    
    return view;
}

// 点击选择订单开始时间
- (void)tapStartTimeBtn
{
    YDWeakSelf
    [self showDatePickerWithTimeType:0 block:^(NSDateComponents *dateComponents) {
        NSString *str = [NSString stringWithFormat:@"%ld-%ld-%ld", dateComponents.year, dateComponents.month, dateComponents.day];
        weakSelf.orderStartTime = str;
        [weakSelf.startTimebtn setTitle:str forState:UIControlStateNormal];
        weakSelf.startTimebtn.selected = YES;
    }];
}

// 点击选择订单结束时间
- (void)tapEndTimeBtn
{
    YDWeakSelf
    [self showDatePickerWithTimeType:1 block:^(NSDateComponents *dateComponents) {
        NSString *str = [NSString stringWithFormat:@"%ld-%ld-%ld", dateComponents.year, dateComponents.month, dateComponents.day];
        weakSelf.orderEndTime = str;
        [weakSelf.endTimebtn setTitle:str forState:UIControlStateNormal];
        weakSelf.endTimebtn.selected = YES;
    }];
}

- (void)showDatePickerWithTimeType:(NSInteger)timeType block:(void(^)(NSDateComponents *dateComponents))block
{
    PGDatePickManager *datePickManager = [[PGDatePickManager alloc] init];
    datePickManager.isShadeBackground = true;
    PGDatePicker *datePicker = datePickManager.datePicker;
    datePicker.datePickerType = PGDatePickerTypeVertical;
    datePicker.isHiddenMiddleText = false;
    datePicker.datePickerMode = PGDatePickerModeDate;
    datePicker.selectedDate = block;
    
    NSDateFormatter *format = [[NSDateFormatter alloc] init];
    format.dateFormat = @"yyyy-MM-dd";
    
    if (timeType == 0) {
        // 开始时间
        if (self.orderStartTime.length) {
            [datePicker setDate:[format dateFromString:self.orderStartTime]];
        }
        
        if (self.orderEndTime.length) {
            datePicker.maximumDate = [format dateFromString:self.orderEndTime];
        }
        
    } else {
        // 结束时间
        if (self.orderEndTime.length) {
            [datePicker setDate:[format dateFromString:self.orderEndTime]];
        }
        
        datePicker.maximumDate = [NSDate date];
        
        if (self.orderStartTime.length) {
            datePicker.minimumDate = [format dateFromString:self.orderStartTime];
        }
    }
    [[UINavigationController rootNavigationController] presentViewController:datePickManager animated:false completion:nil];
}

- (UIView *)createSubViewWithIndex:(NSInteger)index
{
    YDSearcherPropList *propData = self.data[index];
    
    UIView *view = [[UIView alloc] init];
    [view setWidth:self.scrollView.width];
    
    UILabel *title = [[UILabel alloc] init];
    title.font = [UIFont systemFontOfSize:17 weight:UIFontWeightSemibold];
    title.textColor = ColorFromRGB(0x222222);
    title.text = propData.propName;
    [title sizeToFit];
    [title setY:0];
    [view addSubview:title];
    
    // 保存创建的按钮
    NSMutableArray *array = [NSMutableArray array];
    [self.btnArray addObject:array];
    
    for (int i = 0; i < propData.propValueList.count; i++) {
        UIButton *btn = [self createBtnWithPropData:propData index:i];
        [view addSubview:btn];
        [array addObject:btn];

        if (i == propData.propValueList.count - 1) {
            [view setHeight:CGRectGetMaxY(btn.frame)];
        }
        
        // 按钮选择初始化
        BOOL btnSelected = NO;
        if (self.type == YDSelectFilterTypeProduct || self.type == YDSelectFilterTypeCustomer) {
            if ([propData.propName isEqualToString:@"状态"] && i == 1) {
                // "状态"选择"启用"
                btnSelected = YES;
            }
            
            if (self.type == YDSelectFilterTypeCustomer) {
                if ([propData.propName isEqualToString:@"有无欠款"] && i == 0) {
                    // "有无欠款"选择"全部"
                    btnSelected = YES;
                    
                } else if ([propData.propName isEqualToString:@"有无欠该客户货"] && i == 0) {
                    // "有无欠该客户货"选择"全部"
                    btnSelected = YES;
                }
            }
            
        } else if (self.type == YDSelectFilterTypeOrder) {
            if ([propData.propName isEqualToString:@"显示"] && i == 0) {
                // "显示"选择"全部"
                btnSelected = YES;
                
            } else if ([propData.propName isEqualToString:@"按订单欠款"] && i == 0) {
                // "按订单欠款"选择"全部"
                btnSelected = YES;
                
            } else if ([propData.propName isEqualToString:@"按订单欠货"] && i == 0) {
                // "按订单欠货"选择"全部"
                btnSelected = YES;
            }
        }
        
        [self setBtnStatus:btn selected:btnSelected];
    }
    
    [self.scrollView addSubview:view];
    
    return view;
}

- (UIButton *)createBtnWithPropData:(YDSearcherPropList *)propData index:(NSInteger)index
{
    YDSearcherPropValueList *subData = propData.propValueList[index];
    
    UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
    [btn setWidth:(self.mainView.width - 15 * 4) / 3];
    [btn setHeight:32];
    [btn setY:35 + index / 3 * (32 + 15)];
    [btn setX:index%3 * (15 + btn.width)];
    NSString *str = subData.propValue;
    if (str.length > 5) {
        str = [NSString stringWithFormat:@"%@…", [str substringToIndex:5]];
    }
    [btn setTitle:str forState:UIControlStateNormal];
    btn.layer.borderColor = ColorFromRGB(0xFF5630).CGColor;
    btn.layer.cornerRadius = btn.height / 2;
    [btn setTitleColor:ColorFromRGB(0x222222) forState:UIControlStateNormal];
    [btn setTitleColor:ColorFromRGB(0xFF5630) forState:UIControlStateSelected];
    [btn setTarget:self action:@selector(tapSelectBtn:) forControlEvents:UIControlEventTouchUpInside];
    
    return btn;
}

// 改变按钮选择状态
- (void)setBtnStatus:(UIButton *)btn selected:(BOOL)selected
{
    if (selected) {
        btn.titleLabel.font = [UIFont systemFontOfSize:12.0f weight:UIFontWeightMedium];
        btn.backgroundColor = ColorFromRGB(0xFFF6F2);
        btn.layer.borderWidth = 1;
        
    } else {
        btn.titleLabel.font = [UIFont systemFontOfSize:12.0f weight:UIFontWeightMedium];
        btn.backgroundColor = ColorFromRGB(0xF4F4F4);
        btn.layer.borderWidth = 0;
    }
    
    btn.selected = selected;
}

- (void)tapSelectBtn:(UIButton *)btn
{
    for (int i = 0; i < self.btnArray.count; i++) {
        if (btn.selected) {
            // 商品管理的最后一项，状态栏必选一个
            if (self.type == YDSelectFilterTypeProduct && i == self.btnArray.count - 1) {
                // 必须选择一个,不能取消选择
                return;
            }
            
            // 客户管理所有项都是必选一个
            if (self.type == YDSelectFilterTypeCustomer || self.type == YDSelectFilterTypeOrder) {
                return;
            }
        }
        
        NSMutableArray *subArray = self.btnArray[i];
        
        if ([subArray containsObject:btn]) {
            for (UIButton *subBtn in subArray) {
                if (subBtn == btn) {
                    [self setBtnStatus:btn selected:!btn.selected];
                    
                } else {
                    [self setBtnStatus:subBtn selected:NO];
                }
            }
        }
    }
}

#pragma mark - 点击事件

- (void)tapResetBtn
{
    [self refreshUI];
}

- (void)tapConfirmBtn
{
    [self hideFilterView];

    if (self.selectedCompletion) {
        NSMutableArray *array = [NSMutableArray array];
        NSInteger status = -1;
        NSInteger moneyOwe = -1;
        NSInteger goodsOwe = -1;
        NSInteger orderStatus = -1;
        NSInteger arrears = -1;
        NSInteger dueGoods = -1;
        
        for (int i = 0; i < self.btnArray.count; i++) {
            NSMutableArray *subArray = self.btnArray[i];
            
            for (int m = 0; m < subArray.count; m++) {
                UIButton *btn = subArray[m];
                
                if (btn.selected) {
                    if (self.type == YDSelectFilterTypeProduct) {
                        if (i == self.btnArray.count - 1) {
                            // 状态栏
                            status = [self getStatusWithBtnIndex:m];
                            
                        } else {
                            NSString *propNameId = self.data[i].propNameId;
                            NSString *propValueId = self.data[i].propValueList[m].propValueId;
                            NSMutableDictionary *dict = [NSMutableDictionary dictionary];
                            [dict setObject:propNameId forKey:@"propNameId"];
                            [dict setObject:propValueId forKey:@"propValueId"];
                            [array addObject:dict];
                        }
                        
                    } else if (self.type == YDSelectFilterTypeCustomer) {
                        if (i == 0) {
                            // 状态栏
                            status = [self getStatusWithBtnIndex:m];
                            
                        } else if (i == 1) {
                            // 有无欠款
                            moneyOwe = [self getMoneyOweWithBtnIndex:m];
                            
                        } else if (i == 2) {
                            // 有无欠货
                            goodsOwe = [self getGoodsOweWithBtnIndex:m];
                        }
                        
                    } else if (self.type == YDSelectFilterTypeOrder) {
                        if (i == 0) {
                            // 显示
                            orderStatus = [self getOrderStatusWithBtnIndex:m];
                            
                        } else if (i == 1) {
                            // 按订单欠款
                            arrears = [self getArrearsWithBtnIndex:m];
                            
                        } else if (i == 2) {
                            // 按订单欠货
                            dueGoods = [self getDueGoodsWithBtnIndex:m];
                        }
                    }
                }
            }
        }
        self.selectedCompletion(array, status, moneyOwe, goodsOwe, self.orderStartTime, self.orderEndTime, orderStatus, arrears, dueGoods);
    }
}

#pragma mark - 筛选结果判断

// 获取“状态”结果
- (NSInteger)getStatusWithBtnIndex:(NSInteger)index
{
    switch (index) {
        case 1:
            // 启用
            return 1;
        case 2:
            // 停用
            return 0;
        default:
            // 查全部
            return -1;
    }
}

// 获取“有无欠款”结果
- (NSInteger)getMoneyOweWithBtnIndex:(NSInteger)index
{
    switch (index) {
        case 1:
            // 无欠款
            return 0;
        case 2:
            // 有欠款
            return 1;
        default:
            // 查全部
            return -1;
    }
}

// 获取“有无欠货”结果
- (NSInteger)getGoodsOweWithBtnIndex:(NSInteger)index
{
    switch (index) {
        case 1:
            // 无欠货
            return 0;
        case 2:
            // 有欠货
            return 1;
        default:
            // 查全部
            return -1;
    }
}

// 获取“显示”结果
- (NSInteger)getOrderStatusWithBtnIndex:(NSInteger)index
{
    switch (index) {
        case 1:
            // 待配货
            return 10;
        case 2:
            // 部分配货
            return 20;
        case 3:
            // 已配齐
            return 30;
        default:
            // 查全部
            return -1;
    }
}

// 获取“按订单欠款”结果
- (NSInteger)getArrearsWithBtnIndex:(NSInteger)index
{
    switch (index) {
        case 1:
            // 有欠款
            return 1;
        case 2:
            // 无欠款
            return 0;
        default:
            // 查全部
            return -1;
    }
}

// 获取“按订单欠货”结果
- (NSInteger)getDueGoodsWithBtnIndex:(NSInteger)index
{
    switch (index) {
        case 1:
            // 有欠货
            return 1;
        case 2:
            // 无欠货
            return 0;
        default:
            // 查全部
            return -1;
    }
}

@end

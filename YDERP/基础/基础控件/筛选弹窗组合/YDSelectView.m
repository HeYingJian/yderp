//
//  YDSelectView.m
//  YDERP
//
//  Created by 何英健 on 2021/7/26.
//

#import "YDSelectView.h"

@interface YDSelectView ()

@property (nonatomic, assign) YDSelectFilterType type;

@property (nonatomic, copy) BlockSelectViewTapRefresh refreshBlock;
@property (nonatomic, copy) BlockSelectViewShowFilter showFilterBlock;

@property (nonatomic, strong) YDSelectFilterView *filterView;

@property (nonatomic, weak) IBOutlet UIView *selectView1;
@property (nonatomic, weak) IBOutlet UIView *selectView2;
@property (nonatomic, weak) IBOutlet UIView *selectView3;
@property (nonatomic, weak) IBOutlet UIView *selectView4;

@property (nonatomic, strong) NSMutableArray *titleLabelArray;
@property (nonatomic, weak) IBOutlet UILabel *titleLabel1;
@property (nonatomic, weak) IBOutlet UILabel *titleLabel2;
@property (nonatomic, weak) IBOutlet UILabel *titleLabel3;
@property (nonatomic, weak) IBOutlet UILabel *titleLabel4;

@property (nonatomic, strong) NSMutableArray *imgViewArray;
@property (nonatomic, weak) IBOutlet UIImageView *imgView1;
@property (nonatomic, weak) IBOutlet UIImageView *imgView2;
@property (nonatomic, weak) IBOutlet UIImageView *imgView3;
@property (nonatomic, weak) IBOutlet UIImageView *imgView4;

@property (nonatomic, weak) IBOutlet NSLayoutConstraint *selectViewW1;
@property (nonatomic, weak) IBOutlet NSLayoutConstraint *selectViewW2;
@property (nonatomic, weak) IBOutlet NSLayoutConstraint *selectViewW3;
@property (nonatomic, weak) IBOutlet NSLayoutConstraint *selectViewW4;

@property (nonatomic, weak) IBOutlet NSLayoutConstraint *imgViewLeading1;
@property (nonatomic, weak) IBOutlet NSLayoutConstraint *imgViewLeading2;
@property (nonatomic, weak) IBOutlet NSLayoutConstraint *imgViewLeading3;

@end

@implementation YDSelectView

#pragma mark - 懒加载

- (NSMutableArray *)titleLabelArray
{
    if (!_titleLabelArray) {
        _titleLabelArray = [NSMutableArray array];
        [_titleLabelArray addObject:self.titleLabel1];
        [_titleLabelArray addObject:self.titleLabel2];
        [_titleLabelArray addObject:self.titleLabel3];
        [_titleLabelArray addObject:self.titleLabel4];
    }
    
    return _titleLabelArray;
}

- (NSMutableArray *)imgViewArray
{
    if (!_imgViewArray) {
        _imgViewArray = [NSMutableArray array];
        [_imgViewArray addObject:self.imgView1];
        [_imgViewArray addObject:self.imgView2];
        [_imgViewArray addObject:self.imgView3];
        [_imgViewArray addObject:self.imgView4];
    }
    
    return _imgViewArray;
}

- (YDSelectFilterView *)filterView
{
    if (!_filterView) {
        YDWeakSelf
        _filterView = [YDSelectFilterView createWithType:self.type selectedCompletion:^(NSMutableArray *array, NSInteger status, NSInteger moneyOwe, NSInteger goodsOwe, NSString *startTime, NSString *endTime, NSInteger orderStatus, NSInteger arrears, NSInteger dueGoods) {
            weakSelf.selectedResultArray = array;
            weakSelf.selectedResultStatus = status;
            weakSelf.selectedResultMoneyOwe = moneyOwe;
            weakSelf.selectedResultGoodsOwe = goodsOwe;
            weakSelf.selectedResultStartTime = startTime;
            weakSelf.selectedResultEndTime = endTime;
            weakSelf.selectedResultOrderStatus = orderStatus;
            weakSelf.selectedResultArrears = arrears;
            weakSelf.selectedResultDueGoods = dueGoods;
            
            if (weakSelf.refreshBlock) {
                weakSelf.refreshBlock();
            }
            
            // “筛选”中是否有选中默认值以外的东西
            BOOL isSelected = NO;
            if (array.count || moneyOwe != -1 || goodsOwe != -1 || startTime.length || endTime.length || orderStatus != -1 || arrears != -1 || dueGoods != -1) {
                isSelected = YES;
            }
            
            if (weakSelf.type == YDSelectFilterTypeProduct || weakSelf.type == YDSelectFilterTypeCustomer) {
                if (status != 1) {
                    isSelected = YES;
                }
                
            } else {
                if (status != -1) {
                    isSelected = YES;
                }
            }
            
            // 有选中东西的话，改变筛选的颜色
            YDSelectViewModel *model = [self.modelArray lastObject];
            model.isSelected = isSelected;
            [weakSelf refreshUI];
        }];
    }
    
    return _filterView;
}

#pragma mark - 类方法

+ (YDSelectView *)createViewWithType:(YDSelectFilterType)type modelArray:(NSMutableArray<YDSelectViewModel *> *)modelArray showFilterBlock:(BlockSelectViewShowFilter)showFilterBlock refreshBlock:(BlockSelectViewTapRefresh)refreshBlock
{
    YDSelectView *view = [[NSBundle mainBundle] loadNibNamed:@"YDSelectView" owner:nil options:nil][0];
    view.frame = CGRectMake(0, 0, SCREEN_WIDTH, 50);
    view.type = type;
    
    [view initUI];
    [view addNotification];
    [view filterView];
    
    view.modelArray = modelArray;
    view.refreshBlock = refreshBlock;
    view.showFilterBlock = showFilterBlock;
    
    if (type == YDSelectFilterTypeProduct || type == YDSelectFilterTypeCustomer) {
        // 默认选中启用
        view.selectedResultStatus = 1;
        
    } else {
        view.selectedResultStatus = -1;
    }
    // 默认选中全部(-1去到接口请求时，就不会传这个参数)
    view.selectedResultMoneyOwe = -1;
    view.selectedResultGoodsOwe = -1;
    view.selectedResultOrderStatus = -1;
    view.selectedResultArrears = -1;
    view.selectedResultDueGoods = -1;
    
    [view setSelectedWithIndex:0];

    return view;
}

- (void)initUI
{
    [self addGesture];
}

- (void)addGesture
{
    UITapGestureRecognizer *tap1 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapSelectView1)];
    [self.selectView1 addGestureRecognizer:tap1];
    
    UITapGestureRecognizer *tap2 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapSelectView2)];
    [self.selectView2 addGestureRecognizer:tap2];
    
    UITapGestureRecognizer *tap3 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapSelectView3)];
    [self.selectView3 addGestureRecognizer:tap3];
    
    UITapGestureRecognizer *tap4 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapSelectView4)];
    [self.selectView4 addGestureRecognizer:tap4];
}

- (void)addNotification
{
    if (self.type == YDSelectFilterTypeProduct) {
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(clearData) name:REFRESH_PRODUCT_FILTER object:nil];
    }
}

- (void)clearData
{
    [self.filterView clearData];
}

- (void)setModelArray:(NSMutableArray<YDSelectViewModel *> *)modelArray
{
    _modelArray = modelArray;
    
    CGFloat totalWidth = 0;
    for (YDSelectViewModel *sub in modelArray) {
        totalWidth += sub.tagWidth;
    }
    
    YDSelectViewModel *model = [[YDSelectViewModel alloc] init];
    model.title = @"筛选";
    model.tagWidth = SCREEN_WIDTH - totalWidth;
    model.type = 0;
    [model createImgViewModelWithSelectedImg:@"筛选图标选中" unselectedImg:@"筛选图标未选中"];
    [_modelArray addObject:model];
    
    for (int i = 0; i < modelArray.count; i++) {
        YDSelectViewModel *model = modelArray[i];
        
        switch (i) {
            case 0:
            {
                self.selectViewW1.constant = model.tagWidth;
                self.titleLabel1.text = model.title;
                if (!model.imgViewArray.count) {
                    self.imgViewLeading1.constant = 0;
                }
            }
                break;
                
            case 1:
            {
                self.selectViewW2.constant = model.tagWidth;
                self.titleLabel2.text = model.title;
                if (!model.imgViewArray.count) {
                    self.imgViewLeading2.constant = 0;
                }
            }
                break;
                
            case 2:
            {
                self.selectViewW3.constant = model.tagWidth;
                self.titleLabel3.text = model.title;
                if (!model.imgViewArray.count) {
                    self.imgViewLeading3.constant = 0;
                }
            }
                break;
                
            case 3:
            {
                self.selectViewW4.constant = SCREEN_WIDTH - model.tagWidth * 3;
                self.titleLabel4.text = model.title;
            }
                break;
                
            default:
                break;
        }
    }
    
    [self layoutIfNeeded];
}

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

#pragma mark - 点击选择tab

- (void)tapSelectView1
{
    [self setSelectedWithIndex:0];
}

- (void)tapSelectView2
{
    [self setSelectedWithIndex:1];
}

- (void)tapSelectView3
{
    [self setSelectedWithIndex:2];
}

- (void)tapSelectView4
{
    [self setSelectedWithIndex:3];
}

// 选中当前index，其它项(筛选以外)均变成未选中，因为筛选与其他项可以共存；如果当前项本身选中，则变为下一个选中状态(例如本身为选中倒序，则变为选中正序)
- (void)setSelectedWithIndex:(NSInteger)index
{
    if (index < self.modelArray.count - 1) {
        // 点击筛选以外的项
        for (int i = 0; i < self.modelArray.count - 1; i++) {
            YDSelectViewModel *model = self.modelArray[i];
            if (index == i) {
                if (model.isSelected) {
                    // 本身已经选中，则查看是否还有下一个选中状态
                    if (model.imgViewArray.count > 1) {
                        if (model.type == model.imgViewArray.count - 1) {
                            model.type = 0;
                            
                        } else {
                            model.type++;
                        }
                    }
                    
                } else {
                    model.isSelected = YES;
                }
                
            } else {
                model.isSelected = NO;
            }
        }
        
        [self refreshUI];
        
        if (self.refreshBlock) {
            self.refreshBlock();
        }
        
    } else {
        // 点击筛选
        [self showFilterView];
    }
}

// 根据modelArray数据，刷新为对应样式
- (void)refreshUI
{
    for (int i = 0; i < self.modelArray.count; i++) {
        YDSelectViewModel *model = self.modelArray[i];
        
        UILabel *titleLabel = self.titleLabelArray[i];
        if (model.isSelected) {
            titleLabel.textColor = COLOR_MAIN_BULE;
            
        } else {
            titleLabel.textColor = ColorFromRGB(0x222222);
        }
        
        UIImageView *imgView = self.imgViewArray[i];
        
        if (model.imgViewArray.count) {
            NSString *imgName;
            YDSelectImgViewModel *imgViewModel = model.imgViewArray[model.type];
            
            if (model.isSelected) {
                imgName = imgViewModel.selectedImg;
                
            } else {
                imgName = imgViewModel.unselectedImg;
            }
            [imgView setImage:[UIImage imageNamed:imgName]];
            
        } else {
            [imgView setImage:nil];
        }
    }
}

// 弹出筛选弹窗
- (void)showFilterView
{
    [self.filterView showWithBlock:self.showFilterBlock];
}

@end

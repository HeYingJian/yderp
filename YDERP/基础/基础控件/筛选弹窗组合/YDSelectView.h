//
//  YDSelectView.h
//  YDERP
//
//  Created by 何英健 on 2021/7/26.
//

#import <UIKit/UIKit.h>
#import "YDSelectFilterView.h"
#import "YDSelectViewModel.h"

typedef void(^BlockSelectViewTapRefresh)(void);
typedef void(^BlockSelectViewShowFilter)(void);

@interface YDSelectView : UIView

/**
 创建方法
 @param type 筛选类型
 @param modelArray 数据模型
 @param showFilterBlock 显示筛选
 @param refreshBlock 通知外部刷新数据
 */
+ (YDSelectView *)createViewWithType:(YDSelectFilterType)type modelArray:(NSMutableArray<YDSelectViewModel *> *)modelArray showFilterBlock:(BlockSelectViewShowFilter)showFilterBlock refreshBlock:(BlockSelectViewTapRefresh)refreshBlock;

// 用于外部查看
@property (nonatomic, strong) NSMutableArray<YDSelectViewModel *> *modelArray;

// 筛选结果数组
@property (nonatomic, strong) NSMutableArray *selectedResultArray;

#pragma mark - 商品搜索&客户搜索专用

// 筛选结果中的“状态”
@property (nonatomic, assign) NSInteger selectedResultStatus;

#pragma mark - 客户搜索专用

// 筛选结果中的“有无欠款”
@property (nonatomic, assign) NSInteger selectedResultMoneyOwe;
// 筛选结果中的“有无欠货”
@property (nonatomic, assign) NSInteger selectedResultGoodsOwe;

#pragma mark - 订单搜索专用

// 筛选结果中的“日期-开始”
@property (nonatomic, copy) NSString *selectedResultStartTime;
// 筛选结果中的“日期-结束”
@property (nonatomic, copy) NSString *selectedResultEndTime;
// 筛选结果中的“显示”(待配货/部分配货/已配齐)
@property (nonatomic, assign) NSInteger selectedResultOrderStatus;
// 筛选结果中的“按订单欠款”
@property (nonatomic, assign) NSInteger selectedResultArrears;
// 筛选结果中的“按订单欠货”
@property (nonatomic, assign) NSInteger selectedResultDueGoods;

@end

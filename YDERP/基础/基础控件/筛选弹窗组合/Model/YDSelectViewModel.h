//
//  YDSelectViewModel.h
//  YDERP
//
//  Created by 何英健 on 2021/7/26.
//

#import <Foundation/Foundation.h>

@interface YDSelectImgViewModel : NSObject

// 选中时候图片
@property (nonatomic, copy) NSString *selectedImg;
// 未选中时候图片
@property (nonatomic, copy) NSString *unselectedImg;

@end

@interface YDSelectViewModel : NSObject

// 当前是否选中
@property (nonatomic, assign) BOOL isSelected;
// 当前的排序样式(例如倒序，顺序)
@property (nonatomic, assign) NSInteger type;

// 宽度
@property (nonatomic, assign) CGFloat tagWidth;
// 名称
@property (nonatomic, copy) NSString *title;
// 状态图片
@property (nonatomic, strong) NSMutableArray<YDSelectImgViewModel *> *imgViewArray;

// 创建图片模型
- (void)createImgViewModelWithSelectedImg:(NSString *)selectedImg unselectedImg:(NSString *)unselectedImg;

@end

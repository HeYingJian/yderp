//
//  YDSelectViewModel.m
//  YDERP
//
//  Created by 何英健 on 2021/7/26.
//

#import "YDSelectViewModel.h"

@implementation YDSelectImgViewModel

@end

@implementation YDSelectViewModel

- (NSMutableArray<YDSelectImgViewModel *> *)imgViewArray
{
    if (!_imgViewArray) {
        _imgViewArray = [NSMutableArray array];
    }
    
    return _imgViewArray;
}

- (void)createImgViewModelWithSelectedImg:(NSString *)selectedImg unselectedImg:(NSString *)unselectedImg
{
    YDSelectImgViewModel *imgModel = [[YDSelectImgViewModel alloc] init];
    imgModel.selectedImg = selectedImg;
    imgModel.unselectedImg = unselectedImg;
    [self.imgViewArray addObject:imgModel];
}

@end

//
//  UIView+Extension.m
//  terasure
//
//  Created by 何英健 on 2019/11/19.
//  Copyright © 2019 何英健. All rights reserved.
//

#import "UIView+Extension.h"

@implementation UIView (Extension)

#pragma mark - 设置部分圆角

- (void)addRoundedCorners:(UIRectCorner)corners
                withRadii:(CGSize)radii
{
    UIBezierPath* rounded = [UIBezierPath bezierPathWithRoundedRect:self.bounds byRoundingCorners:corners cornerRadii:radii];
    CAShapeLayer* shape = [[CAShapeLayer alloc] init];
    [shape setPath:rounded.CGPath];
    
    self.layer.mask = shape;
}

- (void)addRoundedCorners:(UIRectCorner)corners
                withRadii:(CGSize)radii
                 viewRect:(CGRect)rect
{
    UIBezierPath* rounded = [UIBezierPath bezierPathWithRoundedRect:rect byRoundingCorners:corners cornerRadii:radii];
    CAShapeLayer* shape = [[CAShapeLayer alloc] init];
    [shape setPath:rounded.CGPath];
    
    self.layer.mask = shape;
}

- (void)appleSetGradientColorWith:(UIColor *)color1 Color:(UIColor *)color2
{
    [self appleSetGradientColorWith:color1 Color:color2 index:-1];
}

- (void)appleSetGradientColorWith:(UIColor *)color1 Color:(UIColor *)color2 index:(int)index
{
    CAGradientLayer *gradient = [CAGradientLayer layer];
//    gradient.startPoint = CGPointMake(0, 0);
//    gradient.endPoint = CGPointMake(1, 1);

    gradient.frame = self.bounds;
    gradient.colors = [NSArray arrayWithObjects:
                       (id)color1.CGColor,
                       (id)color2.CGColor, nil];
    
    if (index == -1) {
        [self.layer addSublayer:gradient];
        
    } else {
        [self.layer insertSublayer:gradient atIndex:index];
    }
}

- (void)appleSetGradientColorWith:(UIColor *)color1 Color:(UIColor *)color2 Size:(CGSize)size
{
    CAGradientLayer *gradient = [CAGradientLayer layer];
    gradient.startPoint = CGPointMake(0, 0);
    gradient.endPoint = CGPointMake(1, 1);
    
    CGRect rect = CGRectMake(0, 0, size.width, size.height);
    gradient.frame = rect;
    gradient.colors = [NSArray arrayWithObjects:
                       (id)color1.CGColor,
                       (id)color2.CGColor, nil];

    [self.layer insertSublayer:gradient atIndex:0];
}

+ (UIImage *)changeViewToPhoto:(UIView *)view
{
    // 下面方法，第一个参数表示区域大小。第二个参数表示是否是非透明的。如果需要显示半透明效果，需要传NO，否则传YES。第三个参数就是屏幕密度了
    UIGraphicsBeginImageContextWithOptions(view.bounds.size, NO, 1);
    [view.layer renderInContext:UIGraphicsGetCurrentContext()];
    UIImage *viewImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();

    return viewImage;
}

+ (void)savePhoto:(UIImage *)image
{
    UIImageWriteToSavedPhotosAlbum(image, nil, nil, nil);
}

@end

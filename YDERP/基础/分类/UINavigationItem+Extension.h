//
//  UINavigationItem+Extension.h
//  terasure
//
//  Created by 何英健 on 2020/1/13.
//  Copyright © 2020 何英健. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface UINavigationItem (Extension)

// 添加返回按钮
- (void)addDefaultBackButton:(id)viewController;
- (void)addDefaultBackButton:(id)viewController actionMethod:(SEL)actionMethod;
- (void)addDefaultWhiteBackButton:(id)viewController actionMethod:(SEL)actionMethod;

@end

//
//  UIView+Extension.h
//  terasure
//
//  Created by 何英健 on 2019/11/19.
//  Copyright © 2019 何英健. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface UIView (Extension)

#pragma mark - 设置部分圆角
/**
 *  设置部分圆角(绝对布局)
 *
 *  @param corners 需要设置为圆角的角 UIRectCornerTopLeft | UIRectCornerTopRight | UIRectCornerBottomLeft | UIRectCornerBottomRight | UIRectCornerAllCorners
 *  @param radii   需要设置的圆角大小 例如 CGSizeMake(20.0f, 20.0f)
 */
- (void)addRoundedCorners:(UIRectCorner)corners
                withRadii:(CGSize)radii;
/**
 *  设置部分圆角(相对布局)
 *
 *  @param corners 需要设置为圆角的角 UIRectCornerTopLeft | UIRectCornerTopRight | UIRectCornerBottomLeft | UIRectCornerBottomRight | UIRectCornerAllCorners
 *  @param radii   需要设置的圆角大小 例如 CGSizeMake(20.0f, 20.0f)
 *  @param rect    需要设置的圆角view的rect
 */
- (void)addRoundedCorners:(UIRectCorner)corners
                withRadii:(CGSize)radii
                 viewRect:(CGRect)rect;

// 设置渐变色
- (void)appleSetGradientColorWith:(UIColor*)color1 Color:(UIColor*)color2;
// 设置渐变色
- (void)appleSetGradientColorWith:(UIColor *)color1 Color:(UIColor *)color2 index:(int)index;
// 设置渐变色
- (void)appleSetGradientColorWith:(UIColor *)color1 Color:(UIColor *)color2 Size:(CGSize)size;

// uiview转图片
+ (UIImage *)changeViewToPhoto:(UIView *)view;
// 保存图片到相册
+ (void)savePhoto:(UIImage *)image;

@end

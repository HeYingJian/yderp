//
//  UIControl+Extension.m
//  EEducation
//
//  Created by oymuzi on 2020/12/31.
//

#import "UIControl+Extension.h"

static void * UIBUTTON_INTERVAL_KEY = &UIBUTTON_INTERVAL_KEY;
static void * UIBUTTON_IGNORE_KEY = &UIBUTTON_IGNORE_KEY;

@implementation UIControl (EE_EventInterval)

+ (void)load{
    Method originMethod = class_getInstanceMethod(self, @selector(sendAction:to:forEvent:));
    Method eeMethod = class_getInstanceMethod(self, @selector(ee_sendAction:to:forEvent:));
    method_exchangeImplementations(originMethod, eeMethod);
}

- (void)ee_sendAction:(SEL)action to:(id)target forEvent:(UIEvent *)event{
    if (self.ee_ignoreEvent) return;
    if (self.ee_eventInterval > 0){
        self.ee_ignoreEvent = YES;
        [self performSelector:@selector(setEe_ignoreEvent:) withObject:@(NO) afterDelay:self.ee_eventInterval];
    }
    [self ee_sendAction:action to:target forEvent:event];
}

- (void)setEe_ignoreEvent:(BOOL)ee_ignoreEvent{
    objc_setAssociatedObject(self, UIBUTTON_IGNORE_KEY, @(ee_ignoreEvent), OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}

- (BOOL)ee_ignoreEvent{
    return [objc_getAssociatedObject(self, UIBUTTON_IGNORE_KEY) boolValue];
}

- (void)setEe_eventInterval:(NSTimeInterval)ee_eventInterval{
    objc_setAssociatedObject(self, UIBUTTON_INTERVAL_KEY, @(ee_eventInterval), OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}

- (NSTimeInterval)ee_eventInterval{
    return [objc_getAssociatedObject(self, UIBUTTON_INTERVAL_KEY) doubleValue];
}

@end

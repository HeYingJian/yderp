//
//  UIImageView+AppleImageView.m
//  YiDing
//
//  Created by HuangBaoan on 2019/1/18.
//  Copyright © 2019 com.rfchina.yiding. All rights reserved.
//

#import "UIImageView+AppleImageView.h"

@implementation UIImageView (AppleImageView)

- (void)appleSetImageWithUrl:(NSString *)url
{
    [self appleSetImageWithUrl:url placeholderImage:nil];
}

- (void)appleSetImageWithUrl:(NSString *)url placeholderImage:(nullable NSString *)placeholderImage
{
    //网页链接是空的
    if (url == nil || [url isEqualToString:@""] || [url isEqualToString:@"null"] || [url isEqualToString:@"NULL"]) {return;}
    
    //判断URL是否有效
    if (![url hasPrefix:@"http"] && ![url hasPrefix:@"https"]) {return;}
    
    //样式
    NSString* ossString = [NSString stringWithFormat:@"%@?x-oss-process=style/%@", url, [self getStyleStringFromFrameWidth]];
    
    //中文处理
    NSString *imageString = [ossString stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]];
    
    //加载图片
    NSURL* imageUrl = [NSURL URLWithString:imageString];
    
    UIImage *defaultImg;
    if (placeholderImage.length) {
        defaultImg = [UIImage imageNamed:placeholderImage];
    }
    [self sd_setImageWithURL:imageUrl placeholderImage:defaultImg];
}

- (void)appleSetImageWithUrl:(NSString *)url SprcifyOssImageStyle:(NSString* )style HasHD: (BOOL)hasHD {
    
    [self appleSetImageWithUrl:url SprcifyOssImageStyle:style HasHD:hasHD placeholderImage:nil completion:nil];
}

- (void)appleSetImageWithUrl:(NSString *)url SprcifyOssImageStyle:(NSString* )style HasHD: (BOOL)hasHD placeholderImage:(nullable NSString *)placeholderImage completion:(nullable SDExternalCompletionBlock)completion {
    
    //网页链接是空的
    if (url == nil || [url isEqualToString:@""] || [url isEqualToString:@"null"] || [url isEqualToString:@"NULL"]) {
        [self setImage:[UIImage imageNamed:placeholderImage]];
        return;
    }
    
    //判断URL是否有效
    if (![url hasPrefix:@"http"] && ![url hasPrefix:@"https"]) {return;}
    
    
    NSMutableString* hdStyle = [[NSMutableString alloc] initWithString:style];
    
    if ([UIScreen mainScreen].scale == 3 && hasHD) {
        [hdStyle appendString:@"_hd"];
    }
    
    //样式
    NSString* ossString = [NSString stringWithFormat:@"%@?x-oss-process=style/%@", url, hdStyle];
    
    //中文处理
    NSString *imageString = [ossString stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]];
    
    //加载图片
    NSURL* imageUrl = [NSURL URLWithString:imageString];

    UIImage *defaultImg;
    if (placeholderImage.length) {
        defaultImg = [UIImage imageNamed:placeholderImage];
    }
    [self sd_setImageWithURL:imageUrl placeholderImage:defaultImg completed:completion];
}

- (NSString* )getStyleStringFromFrameWidth {
    
    CGFloat pixleWith = self.bounds.size.width * [UIScreen mainScreen].scale;
    
    if (pixleWith <= 200) {
        return @"productPic_thumbnail";
    }
    
    if (pixleWith <= 320) {
        return @"productPic_list";
    }
    
    if (pixleWith <= 480) {
        return @"productPic_list_hd";
    }
    
    //取用全尺寸图时，将分辨率降低至1080p以下
    pixleWith -= pixleWith - 162;
    
    if (pixleWith <= 640) {
        return @"productPic_full";
    }
    
    if (pixleWith <= 800) {
        return @"productDetails_list";
    }
    
    if (pixleWith <= 960) {
        return @"productPic_full_hd";
    }
    
    if (pixleWith <= 1200) {
        return @"productDetails_list_hd";
    }
    
    if (pixleWith <= 1600) {
        return @"productPic_enlarge";
    }
    
    return @"productPic_full";
}

// 生成一张纯色的图片
+ (UIImage *)imageWithColor:(UIColor *)color size:(CGSize)size
{
    CGRect rect = CGRectMake(0.0f, 0.0f, size.width, size.height);
    
    UIGraphicsBeginImageContext(rect.size);
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGContextSetFillColorWithColor(context, [color CGColor]);
    CGContextFillRect(context, rect);
    UIImage *theImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return theImage;
}

// 把指定image绘制成圆形
+ (UIImage *)createCircleImageWithImage:(UIImage *)image
{
    // 开始图形上下文，NO代表透明
    UIGraphicsBeginImageContextWithOptions(image.size, NO, 0.0);
    // 获得图形上下文
    CGContextRef ctx = UIGraphicsGetCurrentContext();
    // 设置一个范围
    CGRect rect = CGRectMake(0, 0, image.size.width, image.size.height);
    // 根据一个rect创建一个椭圆
    CGContextAddEllipseInRect(ctx, rect);
    // 裁剪
    CGContextClip(ctx);
    // 将原照片画到图形上下文
    [image drawInRect:rect];
    // 从上下文上获取剪裁后的照片
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
    // 关闭上下文
    UIGraphicsEndImageContext();
    
    return newImage;
}

@end

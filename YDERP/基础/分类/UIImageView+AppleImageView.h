//
//  UIImageView+AppleImageView.h
//  YiDing
//
//  Created by HuangBaoan on 2019/1/18.
//  Copyright © 2019 com.rfchina.yiding. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SDWebImageManager.h"

typedef enum : NSInteger {
    AppleOssImageStyleProductPicList,
    AppleOssImageStyleProductPicFull,
    AppleOssImageStyleProductPicEnlarge,
    AppleOssImageStyleProductPicThumbnail,
    AppleOssImageStyleTopBannerH5,
    AppleOssImageStyleCommonBannerFull,
    AppleOssImageStyleCommonPicThumbnail,
    AppleOssImageStyleShowBannerList,
    AppleOssImageStyleProductDetailsList,
} AppleOssImageStyle;

NS_ASSUME_NONNULL_BEGIN

@interface UIImageView (AppleImageView)

- (void)appleSetImageWithUrl: (NSString* )url;
- (void)appleSetImageWithUrl:(NSString *)url placeholderImage:(nullable NSString *)placeholderImage;
- (void)appleSetImageWithUrl:(NSString *)url SprcifyOssImageStyle:(NSString* )style HasHD: (BOOL)hasHD;
- (void)appleSetImageWithUrl:(NSString *)url SprcifyOssImageStyle:(NSString* )style HasHD: (BOOL)hasHD placeholderImage:(nullable NSString *)placeholderImage completion:(nullable SDExternalCompletionBlock)completion;

// 生成一张纯色的图片
+ (UIImage *)imageWithColor:(UIColor *)color size:(CGSize)size;

// 把指定image绘制成圆形
+ (UIImage *)createCircleImageWithImage:(UIImage *)image;

@end

NS_ASSUME_NONNULL_END

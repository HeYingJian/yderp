//
//  UIButton+XYButton.h
//  亿订
//
//  Created by cheng on 2017/6/1.
//  Copyright © 2018年 cheng. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIButton (XYButton)

@property(nonatomic ,copy)void(^block)(UIButton*);

-(void)addTapBlock:(void(^)(UIButton*btn))block;

@end

@interface UIButton (ExpandActionSize)

/// 扩大点击范围响应事件，size数值须为正数，负数时将会减小点击范围
@property (nonatomic, assign) CGSize expandResponseSize;

@end

//
//  UIViewController+Extension.m
//  EEducation
//
//  Created by oymuzi on 2020/12/1.
//

#import "UIViewController+Extension.h"

@import ObjectiveC;

@implementation UIViewController (Extension)


@end


static NSString * const overlayPrefix = @"SFOverlay_";

NS_INLINE NSArray<CALayer *> * EEFindOverlayLayersIn(UIViewController *parentVC, UIViewController *subVC){
    NSString *prefixKey = [NSString stringWithFormat:@"%@%@_%@_", overlayPrefix, NSStringFromClass(parentVC.class), NSStringFromClass(subVC.class)];
    __block NSMutableArray *layers = [NSMutableArray array];
    [parentVC.view.layer.sublayers enumerateObjectsUsingBlock:^(__kindof CALayer * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        if (obj.name && [obj.name hasPrefix:prefixKey]) {
            [layers addObject:obj];
        }
    }];
    return layers;
}

NS_INLINE NSString * EEOverlayNameFromParentViewController(UIViewController *parentVC, UIViewController *subVC){
    NSArray<CALayer *> *layers = EEFindOverlayLayersIn(parentVC, subVC);
    if (layers.count == 0){
        NSString *name = [NSString stringWithFormat:@"%@%@_%@_0", overlayPrefix, NSStringFromClass(parentVC.class), NSStringFromClass(subVC.class)];
        return name;
    }
    NSString *name = [NSString stringWithFormat:@"%@%@_%@_%@", overlayPrefix, NSStringFromClass(parentVC.class), NSStringFromClass(subVC.class), @(layers.count-1)];
    return name;
}

NS_INLINE CALayer * EEFindOverlayLayerInParentVC(UIViewController *parentVC, UIViewController *subVC){
    NSArray * layers = EEFindOverlayLayersIn(parentVC, subVC);
    return layers.lastObject;
}


//void EEAddSubViewController(UIViewController *vc, UIViewController *toParent, BOOL isFullScreen, BOOL isAnimated, BOOL isNeedOverlay){
//    UIViewController *container = toParent;
//    if (isFullScreen) {
//        if (toParent.navigationController) {
//            container = toParent.navigationController;
//        } else if (toParent.tabBarController) {
//            container = toParent.tabBarController;
//        }
//    }
//    CALayer *layer;
//    if (isNeedOverlay) {
//        NSString *name = EEOverlayNameFromParentViewController(container, vc);
//        layer = [CALayer layer];
//        layer.frame = isFullScreen ? [UIScreen mainScreen].bounds : toParent.view.frame;
//        layer.name = name;
//        [container.view.layer addSublayer:layer];
//    }
//    [vc willMoveToParentViewController:container];
//    [container addChildViewController:vc];
//    [container.view addSubview:vc.view];
//    [vc didMoveToParentViewController:container];
//    CGRect beforeRect = [UIScreen mainScreen].bounds;
//    beforeRect.origin.y += beforeRect.size.height;
//    vc.view.frame = beforeRect;
//    CGRect rect = CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, [UIScreen mainScreen].bounds.size.height);
//    [UIView animateWithDuration:0.25 animations:^{
//        vc.view.frame = rect;
//        if (isNeedOverlay) {
//            layer.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0.3].CGColor;
//        }
//    }];
//}
//
//void EERemoveSubViewController(UIViewController *subViewController, UIViewController *fromParent, BOOL fullScreenAddedBefore, BOOL animation, BOOL wasAddedOverlay){
//    UIViewController *container = fromParent;
//    if (fullScreenAddedBefore) {
//        if (fromParent.navigationController) {
//            container = fromParent.navigationController;
//        } else if (fromParent.tabBarController) {
//            container = fromParent.tabBarController;
//        }
//    }
//    CALayer *layer;
//    if (wasAddedOverlay) {
//        layer = EEFindOverlayLayerInParentVC(container, subViewController);
//    }
//    void(^removeOverlayLayerBlock)(void) = ^(void) {
//        [subViewController willMoveToParentViewController:nil];
//        [subViewController.view removeFromSuperview];
//        [subViewController removeFromParentViewController];
//        [subViewController didMoveToParentViewController:nil];
//        [layer removeFromSuperlayer];
//    };
//    CGRect rect = [UIScreen mainScreen].bounds;
//    rect.origin.y += rect.size.height;
//    if (animation) {
//        [UIView animateWithDuration:0.25 animations:^{
//            subViewController.view.frame = rect;
//            if (wasAddedOverlay) {
//                layer.backgroundColor = [UIColor clearColor].CGColor;
//            }
//        } completion:^(BOOL finished) {
//            removeOverlayLayerBlock();
//        }];
//    } else {
//        removeOverlayLayerBlock();
//    }
//}

void EEPresentViewController(UIViewController *vc, UIViewController *toParent, BOOL isAnimated){
    void(^presentViewControllerBlock)(void) = ^(void) {
        vc.modalPresentationStyle = UIModalPresentationCustom;
        [toParent presentViewController:vc animated:isAnimated completion:^{
            vc.view.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0.3];
        }];
    };
    if (toParent.presentedViewController) {
        [toParent dismissViewControllerAnimated:isAnimated completion:^{
            presentViewControllerBlock();
        }];
        return;
    }
    presentViewControllerBlock();
}

void EEDismissViewController(UIViewController *vc, UIViewController *fromParent, BOOL isAnimated){
    vc.view.backgroundColor = [UIColor clearColor];
    [vc dismissViewControllerAnimated:isAnimated completion:nil];
}


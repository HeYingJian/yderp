//
//  UINavigationController+Extension.m
//  YiDing
//
//  Created by 何英健 on 2019/4/9.
//  Copyright © 2019 com.rfchina.yiding. All rights reserved.
//

#import "UINavigationController+Extension.h"

@implementation UINavigationController (Extension)

+ (UINavigationController *)rootNavigationController
{
    UIViewController *rootViewController = [KeyWindow rootViewController];
    if ([rootViewController isKindOfClass:[UITabBarController class]]) {
        UITabBarController *tabBarVC = (UITabBarController *)rootViewController;
        UINavigationController *navigationController = (UINavigationController *)tabBarVC.selectedViewController;
        return navigationController;
    }else if ([rootViewController isKindOfClass:[UINavigationController class]]) {
        return (UINavigationController *)rootViewController;
    }else {
        return rootViewController.navigationController;
    }
}

- (nullable UIViewController *)childViewControllerForStatusBarStyle
{
    return self.topViewController;
}

@end

//
//  NSDate+Extension.m
//  terasure
//
//  Created by 何英健 on 2019/11/18.
//  Copyright © 2019 何英健. All rights reserved.
//

#import "NSDate+Extension.h"

@implementation NSDate (Extension)

+ (NSString *)getTimestampNow
{
    NSDate* dat = [NSDate dateWithTimeIntervalSinceNow:0];
    NSTimeInterval a = [dat timeIntervalSince1970];
    NSString *timeString = [NSString stringWithFormat:@"%0.f", a];
    
    return timeString;
}

@end

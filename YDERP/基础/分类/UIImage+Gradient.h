//
//  UIImage+Gradient.h
//  testLayer
//
//  Created by tb on 17/3/17.
//  Copyright © 2017年 com.tb. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef NS_ENUM(NSInteger, GradientType) {
    GradientFromTopToBottom = 1,            //从上到下
    GradientFromLeftToRight,                //从做到右
    GradientFromLeftTopToRightBottom,       //从上到下
    GradientFromLeftBottomToRightTop        //从上到下
};

@interface UIImage (Gradient)

/**
 *  根据给定的颜色，生成渐变色的图片
 *  @param imageSize        要生成的图片的大小
 *  @param colorArr         渐变颜色的数组
 *  @param percents          渐变颜色的占比数组
 *  @param gradientType     渐变色的类型
 */
- (UIImage *)createImageWithSize:(CGSize)imageSize gradientColors:(NSArray *)colorArr percentage:(NSArray *)percents gradientType:(GradientType)gradientType;

// 压缩图片到指定大小,返回的是压缩后的图片
+ (UIImage *)compressImageToImage:(UIImage *)image maxLength:(NSUInteger)maxLength;
// 压缩图片到指定大小,返回的是压缩后的图片数据
+ (NSData *)compressImageToData:(UIImage *)image maxLength:(NSUInteger)maxLength;

/**
 * 从图片中按指定的位置大小截取图片的一部分
 * UIImage image 原始的图片
 * CGRect rect 要截取的区域
*/
+ (UIImage *)imageFromImage:(UIImage *)image inRect:(CGRect)rect;
/**
 * 根据给定的size的宽高比自动缩放原图片、自动判断截取位置,进行图片截取
 * UIImage image 原始的图片
 * CGSize size 截取图片的size
 */
+ (UIImage *)clipImage:(UIImage *)image;

// 缩放到指定大小
- (UIImage *)scaleToSize:(CGSize)size;

@end

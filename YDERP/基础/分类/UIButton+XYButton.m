//
//  UIButton+XYButton.m
//  亿订
//
//  Created by cheng on 2017/6/1.
//  Copyright © 2018年 cheng. All rights reserved.
//

#import "UIButton+XYButton.h"

@implementation UIButton (XYButton)

-(void)setBlock:(void(^)(UIButton*))block

{
    
    objc_setAssociatedObject(self,@selector(block), block,OBJC_ASSOCIATION_COPY_NONATOMIC);
    
    [self addTarget:self action:@selector(click:)forControlEvents:UIControlEventTouchUpInside];
    
}

-(void(^)(UIButton*))block

{
    
    return objc_getAssociatedObject(self,@selector(block));
    
}

-(void)addTapBlock:(void(^)(UIButton*))block

{
    
    self.block= block;
    
    [self addTarget:self action:@selector(click:)forControlEvents:UIControlEventTouchUpInside];
    
}

-(void)click:(UIButton*)btn

{
    
    if(self.block) {
        
        self.block(btn);
        
    }
    
}
@end


static void *expandResponseKey = &expandResponseKey;

@implementation UIButton (ExpandActionSize)

- (CGSize)expandResponseSize{
    CGSize size = CGSizeFromString(objc_getAssociatedObject(self, expandResponseKey));
    return size;
}

- (void)setExpandResponseSize:(CGSize)expandResponseSize{
    objc_setAssociatedObject(self, expandResponseKey, NSStringFromCGSize(expandResponseSize), OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}

- (BOOL)pointInside:(CGPoint)point withEvent:(UIEvent *)event{
    if (CGSizeEqualToSize(self.expandResponseSize, CGSizeZero)) {
        return [super pointInside:point withEvent:event];
    } else {
        CGRect bounds = self.bounds;
        bounds = CGRectInset(bounds, -self.expandResponseSize.width, -self.expandResponseSize.height);
        return CGRectContainsPoint(bounds, point);
    }
}


@end

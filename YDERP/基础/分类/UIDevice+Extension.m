//
//  UIDevice+Extension.m
//  EEducation
//
//  Created by oymuzi on 2020/11/30.
//

#import "UIDevice+Extension.h"

@implementation UIDevice (Extension)

+ (BOOL)isBangPhoneMode {
    BOOL isBangDevice = NO;
    
    if (@available(iOS 11.0, *)) {
        UIWindow *keyWindow = [UIApplication sharedApplication].keyWindow;
        if (keyWindow == nil) {
            keyWindow = [UIApplication sharedApplication].delegate.window;
        }
        if (keyWindow == nil) {
            keyWindow = [UIApplication sharedApplication].windows.firstObject;
        }
        
        if (keyWindow == nil) return isBangDevice;
        
        return keyWindow.safeAreaInsets.bottom > 0;
        
    } else {
        return isBangDevice;
    }
}

+ (UIEdgeInsets)safeAreaInsets{
    if([self isBangPhoneMode]){
        UIInterfaceOrientation orient = [UIApplication sharedApplication].statusBarOrientation;
        if(orient == UIInterfaceOrientationPortrait){ // home button on the bottom
            if([UIApplication sharedApplication].statusBarHidden){
                return UIEdgeInsetsMake(44.0,0,34.0,0);
            }else{
                return UIEdgeInsetsMake(24.0,0,34.0,0);
            }
        }
        if(orient == UIInterfaceOrientationPortraitUpsideDown){ // home button on the top
            if([UIApplication sharedApplication].statusBarHidden){
                return UIEdgeInsetsMake(34.0,0,44,0);
            }else{
                return UIEdgeInsetsMake(34.0,0,24,0);
            }
        }

        ////横屏
        if(orient == UIInterfaceOrientationLandscapeLeft){ // home button on the right
            return UIEdgeInsetsMake(0,44.0,21.0,44.0);
        }
        if(orient == UIInterfaceOrientationLandscapeRight) { // home button on the left
            return UIEdgeInsetsMake(0,44.0,21.0,44.0);
        }

        return UIEdgeInsetsZero;
    }else{
        return UIEdgeInsetsZero;
    }
}


+ (UIEdgeInsets)safeAreaInsets2{
    if([self isBangPhoneMode]){
        UIInterfaceOrientation orient = [UIApplication sharedApplication].statusBarOrientation;
        if(orient == UIInterfaceOrientationPortrait){ // home button on the bottom
            return UIEdgeInsetsMake(44.0,0,34.0,0);
        }
        if(orient == UIInterfaceOrientationPortraitUpsideDown){ // home button on the top
            return UIEdgeInsetsMake(34.0,0,44,0);
        }

        ////横屏
        if(orient == UIInterfaceOrientationLandscapeLeft){ // home button on the right
            return UIEdgeInsetsMake(0,44.0,21.0,44.0);
        }
        if(orient == UIInterfaceOrientationLandscapeRight) { // home button on the left
            return UIEdgeInsetsMake(0,44.0,21.0,44.0);
        }

        return UIEdgeInsetsZero;
    }else{
        return UIEdgeInsetsZero;
    }
}

@end

//
//  NSDate+Extension.h
//  terasure
//
//  Created by 何英健 on 2019/11/18.
//  Copyright © 2019 何英健. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSDate (Extension)

// 获取当前时间戳
+ (NSString *)getTimestampNow;

@end

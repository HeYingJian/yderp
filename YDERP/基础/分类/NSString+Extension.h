//
//  NSString+Extension.h
//  terasure
//
//  Created by 何英健 on 2019/11/17.
//  Copyright © 2019 何英健. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (Extension)

// 计算字符串宽高
- (CGSize)sizeWith:(CGSize)maxsize fontSize:(CGFloat)fontSize;
- (CGSize)sizeWith:(CGSize)maxsize font:(UIFont *)font;

// 去除emoji
- (NSString *)removeEmoji;

// 判断是否包含emoji
- (BOOL)containsEmoji;

// 将阿拉伯数字转换为中文数字
+ (NSString *)translationArabicNum:(NSInteger)num;

// 时间字符串转时间
+ (NSTimeInterval)getTimeStampWithString:(NSString *)str format:(NSString *)forma;
// 按照对应格式获取时间字符串
+ (NSString *)timeStringWith:(long long)timestamp format:(NSString *)format;
+ (NSString *)getTimeStringFormatFromTime:(NSInteger)time;

// 获取当前时间戳(秒为单位)
+ (NSInteger)getCurrentTime;
// 判断时间戳是否今天(秒为单位)
+ (BOOL)isToday:(NSInteger)time;
// 判断两个时间戳是否同一天(秒为单位)
+ (BOOL)isSameDate:(NSInteger)time1 time2:(NSInteger)time2;

- (NSString *)ymdStringWithComponent: (NSString *)component;

+ (BOOL)isEmpty: (NSString *)string;

// 价格格式化（带钱符号）
+ (NSString*)applePrefixPriceWith:(CGFloat)price;
// 价格格式化（不带钱符号，1.00显示为1）
+ (NSString *)appleStandardPriceWith:(CGFloat)price;
+ (BOOL)intWithFloat:(CGFloat)floatTemp;

// 当前字符串是否纯数字
- (BOOL)isNumbers;

// 返回当前手机型号
+ (NSString *)getCurrentDevice;
// 返回当前手机运营商
+ (NSString *)getCarrierName;
+ (NSString *)getNetworkStatus;

// 计算字符串内字节数（中文2 英文1 空格1）
- (NSUInteger)charactorNumber;

@end


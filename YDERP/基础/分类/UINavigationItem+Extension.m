//
//  UINavigationItem+Extension.m
//  terasure
//
//  Created by 何英健 on 2020/1/13.
//  Copyright © 2020 何英健. All rights reserved.
//

#import "UINavigationItem+Extension.h"

@implementation UINavigationItem (Extension)

- (void)addDefaultBackButton:(id)viewController
{
    class_addMethod([viewController class], @selector(tapUCEntrance), (IMP)tapUCEntrance, "v@:@");
    
    [self addCustomBackButton:viewController actionMethod:@selector(tapUCEntrance) imageName:@"返回按钮黑色"];
}

- (void)addDefaultBackButton:(id)viewController actionMethod:(SEL)actionMethod
{
    [self addCustomBackButton:viewController actionMethod:actionMethod imageName:@"返回按钮黑色"];
}

- (void)addDefaultWhiteBackButton:(id)viewController actionMethod:(SEL)actionMethod
{
    [self addCustomBackButton:viewController actionMethod:actionMethod imageName:@"返回_白色"];
}

- (void)addCustomBackButton:(id)viewController actionMethod:(SEL)actionMethod imageName:(NSString *)imageName
{
    UIView *containerView = [[UIView alloc] init];
    containerView.frame = CGRectMake(0, 0, 20, 20);
    
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    button.frame = CGRectMake(0, 0, 11, 20);
    [button setBackgroundImage:[UIImage imageNamed:imageName] forState:UIControlStateNormal];
    [button setBackgroundImage:[UIImage imageNamed:imageName] forState:UIControlStateHighlighted];
    [button addTarget:viewController action:actionMethod forControlEvents:UIControlEventTouchUpInside];
//    button.ee_eventInterval = 0.5;
    button.expandResponseSize = CGSizeMake(15, 15);
    [containerView addSubview:button];
    
    UIBarButtonItem *leftButtonItem = [[UIBarButtonItem alloc] initWithCustomView:containerView];
    self.leftBarButtonItem = leftButtonItem;
}

void tapUCEntrance(id self, SEL _cmd)
{
    [[UINavigationController rootNavigationController] popViewControllerAnimated:YES];
}

- (void)tapUCEntrance
{
    //只是为了去除警告
}

@end

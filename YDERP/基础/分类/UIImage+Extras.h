//
//  UIImage+Extras.h
//  terasure
//
//  Created by 何英健 on 2019/11/13.
//  Copyright © 2019 何英健. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface UIImage (Extras)

/**
 *  拉伸图片中间，使周边不变形
 *
 *  @return 拉伸后的图片
 */
- (instancetype)resizeImage;

//生成一张纯色的图片
+ (UIImage *)imageWithColor:(UIColor *)color;

@end


//
//  UITextField+Extension.h
//  YiDing
//
//  Created by 何英健 on 2019/4/28.
//  Copyright © 2019 com.rfchina.yiding. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface UITextField (Extension)

- (void)addToolSenderWithBlock:(void(^)(void))block;

// 当前光标位置
- (NSRange)selectedRange;
// 设置光标位置
- (void)setSelectedRange:(NSRange)range;

@end

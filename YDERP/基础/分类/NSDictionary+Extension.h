//
//  NSDictionary+Extension.h
//  EEducation
//
//  Created by oymuzi on 2020/12/6.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface NSDictionary (Extension)

+ (NSDictionary *)ee_identityPropertyMapWithModel:(Class)modelClass;

@end

NS_ASSUME_NONNULL_END

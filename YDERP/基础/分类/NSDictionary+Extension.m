//
//  NSDictionary+Extension.m
//  EEducation
//
//  Created by oymuzi on 2020/12/6.
//

#import "NSDictionary+Extension.h"

@implementation NSDictionary (Extension)

+ (NSDictionary *)ee_identityPropertyMapWithModel:(Class)modelClass {
    NSMutableArray *array = [NSMutableArray array];
    unsigned int outCount, i;
    objc_property_t *properties = class_copyPropertyList(modelClass, &outCount);
    for (i = 0; i < outCount; i++) {
        objc_property_t property = properties[i];
        //NSString *type = [NSString stringWithCString:property_getAttributes(property) encoding:NSUTF8StringEncoding];
        NSString *name = [NSString stringWithCString:property_getName(property) encoding:NSUTF8StringEncoding];
        [array addObject:name];
    }
    free(properties);
    return [NSDictionary dictionaryWithObjects:array forKeys:array];
}


@end

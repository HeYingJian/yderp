//
//  UIDevice+Extension.h
//  EEducation
//
//  Created by oymuzi on 2020/11/30.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface UIDevice (Extension)

+ (UIEdgeInsets)safeAreaInsets;
+ (UIEdgeInsets)safeAreaInsets2;

@end

NS_ASSUME_NONNULL_END

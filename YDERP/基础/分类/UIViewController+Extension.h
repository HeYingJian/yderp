//
//  UIViewController+Extension.h
//  EEducation
//
//  Created by oymuzi on 2020/12/1.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface UIViewController (Extension)


@end

//void EEAddSubViewController(UIViewController *vc, UIViewController *toParent, BOOL isFullScreen, BOOL isAnimated, BOOL isNeedOverlay);
//void EERemoveSubViewController(UIViewController *subViewController, UIViewController *fromParent, BOOL fullScreenAddedBefore, BOOL animation, BOOL wasAddedOverlay);

void EEPresentViewController(UIViewController *vc, UIViewController *toParent, BOOL isAnimated);
void EEDismissViewController(UIViewController *vc, UIViewController *fromParent, BOOL isAnimated);



NS_ASSUME_NONNULL_END

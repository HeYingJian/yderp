//
//  UINavigationController+Extension.h
//  YiDing
//
//  Created by 何英健 on 2019/4/9.
//  Copyright © 2019 com.rfchina.yiding. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface UINavigationController (Extension)

// 获取当前nav
+ (UINavigationController *)rootNavigationController;

@end

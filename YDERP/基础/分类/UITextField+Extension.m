//
//  UITextField+Extension.m
//  YiDing
//
//  Created by 何英健 on 2019/4/28.
//  Copyright © 2019 com.rfchina.yiding. All rights reserved.
//

#import "UITextField+Extension.h"

static const char kBlockActionKey;

@implementation UITextField (Extension)

- (void)addToolSenderWithBlock:(void(^)(void))block
{
    UIToolbar * topView = [[UIToolbar alloc]initWithFrame:CGRectMake(0, 0, kScreenWidth, 40)];
    [topView setBarStyle:UIBarStyleDefault];
    UIBarButtonItem * btnSpace = [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:self action:nil];
    UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
    btn.frame = CGRectMake(2, 5, 50, 25);
    [btn addTarget:self action:@selector(resignFirstResponderAction) forControlEvents:UIControlEventTouchUpInside];
    [btn setTitle:@"完成" forState:UIControlStateNormal];
    [btn setTitleColor:COLOR_MAIN_BULE forState:UIControlStateNormal];
    UIBarButtonItem *doneBtn = [[UIBarButtonItem alloc] initWithCustomView:btn];
    NSArray * buttonsArray = [NSArray arrayWithObjects:btnSpace,doneBtn,nil];
    [topView setItems:buttonsArray];
    
    if (block && !objc_getAssociatedObject(self, &kBlockActionKey)) {
        objc_setAssociatedObject(self, &kBlockActionKey, block, OBJC_ASSOCIATION_COPY);
    }
    self.inputAccessoryView = topView;
}

- (NSRange)selectedRange
{
    UITextPosition* beginning = self.beginningOfDocument;
    UITextRange* selectedRange = self.selectedTextRange;
    UITextPosition* selectionStart = selectedRange.start;
    UITextPosition* selectionEnd = selectedRange.end;

    const NSInteger location = [self offsetFromPosition:beginning toPosition:selectionStart];
    const NSInteger length = [self offsetFromPosition:selectionStart toPosition:selectionEnd];

    return NSMakeRange(location, length);
}

- (void)setSelectedRange:(NSRange)range
{
    UITextPosition* beginning = self.beginningOfDocument;
    UITextPosition* startPosition = [self positionFromPosition:beginning offset:range.location];

    UITextPosition* endPosition = [self positionFromPosition:beginning offset:range.location + range.length];

    UITextRange* selectionRange = [self textRangeFromPosition:startPosition toPosition:endPosition];

    [self setSelectedTextRange:selectionRange];
}

- (void)resignFirstResponderAction
{
    [self resignFirstResponder];
    
    void(^block)(void) = objc_getAssociatedObject(self, &kBlockActionKey);
    
    if (block) {
        block();
    }
}

@end

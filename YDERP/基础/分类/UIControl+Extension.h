//
//  UIControl+Extension.h
//  EEducation
//
//  Created by oymuzi on 2020/12/31.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface UIControl (EE_EventInterval)

@property (nonatomic, assign) NSTimeInterval ee_eventInterval;
@property (nonatomic, assign) BOOL ee_ignoreEvent;

@end

NS_ASSUME_NONNULL_END

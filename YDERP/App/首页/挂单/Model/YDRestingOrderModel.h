//
//  YDRestingOrderModel.h
//  YDERP
//
//  Created by 何英健 on 2021/8/18.
//

#import "YDBaseModel.h"

@interface YDRestingOrderData : NSObject

// 订单ID
@property (nonatomic, copy) NSString *orderId;
// 订单号
@property (nonatomic, copy) NSString *orderNo;
// 客户姓名
@property (nonatomic, copy) NSString *clientName;
// 总数量
@property (nonatomic, assign) NSInteger totalNum;
// 应付金额
@property (nonatomic, assign) CGFloat totalAmount;
// 开单时间
@property (nonatomic, copy) NSString *orderTime;
// 客户地址
@property (nonatomic, copy) NSString *clientAddress;

@end

@interface YDRestingOrderList : NSObject

@property (nonatomic, assign) NSInteger total;
@property (nonatomic, strong) NSMutableArray<YDRestingOrderData *> *list;

@end

@interface YDRestingOrderModel : YDBaseModel

@property (nonatomic, strong) YDRestingOrderList *data;

@end

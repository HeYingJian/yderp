//
//  YDRestingOrderModel.m
//  YDERP
//
//  Created by 何英健 on 2021/8/18.
//

#import "YDRestingOrderModel.h"

@implementation YDRestingOrderData

@end

@implementation YDRestingOrderList

+ (NSDictionary<NSString *,id> *)modelContainerPropertyGenericClass{
    return @{@"list" : [YDRestingOrderData class]};
}

@end

@implementation YDRestingOrderModel

@end

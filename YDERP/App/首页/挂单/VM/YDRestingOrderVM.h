//
//  YDRestingOrderVM.h
//  YDERP
//
//  Created by 何英健 on 2021/8/18.
//

#import "YDBaseVM.h"
#import "YDRestingOrderModel.h"

@interface YDRestingOrderVM : YDBaseVM

@property (nonatomic, assign) NSInteger total;
@property (nonatomic, strong) NSMutableArray<YDRestingOrderData *> *dataArray;

- (void)loadFirstPageData;
- (void)loadNextPageData;
// 重新加载所有数据到当前页码
- (void)refreshDataToCurrentPage;
@property (nonatomic, copy) BlockLoadPageDataDone loadDataDone;
@property (nonatomic, copy) BlockLoadDataFaild loadDataFaild;

@end

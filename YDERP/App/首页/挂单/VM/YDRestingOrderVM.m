//
//  YDRestingOrderVM.m
//  YDERP
//
//  Created by 何英健 on 2021/8/18.
//

#import "YDRestingOrderVM.h"

#define RESTING_ORDER_MAXPAGE 10

@interface YDRestingOrderVM ()

@property (nonatomic, assign) NSInteger page;

@end

@implementation YDRestingOrderVM

- (NSMutableArray<YDRestingOrderData *> *)dataArray
{
    if (!_dataArray) {
        _dataArray = [NSMutableArray array];
    }
    
    return _dataArray;
}

- (void)loadFirstPageData
{
    self.page = 0;
    self.dataArray = nil;
    
    [self loadNextPageData];
}

- (void)loadNextPageData
{
    [self loadDataWithMaxPage:RESTING_ORDER_MAXPAGE completion:nil];
}

- (void)loadDataWithMaxPage:(NSInteger)maxPage completion:(void (^)(void))completion
{
    //请求路径
    NSString *path = URL_RESTING_ORDER;
    
    //参数
    NSMutableDictionary *param = [NSMutableDictionary dictionary];
    [param setObject:[NSString stringWithFormat:@"%ld", self.page + 1] forKey:@"pageNum"];
    [param setObject:[NSString stringWithFormat:@"%ld", maxPage] forKey:@"pageSize"];
    
    YDWeakSelf
    AppleServerPostCompleteBlock postHandleBlock = ^(BOOL done, id _Nullable object, NSString * _Nullable error) {
        [YDLoadingView hideToSuperview:nil];
        
        if (done) {
            YDRestingOrderModel *resp = [YDRestingOrderModel modelWithJSON:object];
            
            if (resp.code == 200) {
                if (resp.data.list.count > 0) {
                    weakSelf.page++;
                    weakSelf.total = resp.data.total;
                    [weakSelf.dataArray addObjectsFromArray:resp.data.list];
                    
                    if (weakSelf.loadDataDone) {
                        if (resp.data.list.count < maxPage) {
                            weakSelf.loadDataDone(BaseVMRefreshTypeNoMore);
                            
                        } else {
                            weakSelf.loadDataDone(BaseVMRefreshTypeNormal);
                        }
                    }
                    
                    if (completion) {
                        completion();
                    }
                    
                } else {
                    if (weakSelf.loadDataDone) {
                        weakSelf.loadDataDone(BaseVMRefreshTypeNoMore);
                    }
                }
            
            } else {
                if (weakSelf.loadDataFaild) {
                    NSString *msg;
                    if (resp.detailMessage.length) {
                        msg = resp.detailMessage;
                        
                    } else if (resp.message.length) {
                        msg = resp.message;
                        
                    } else {
                        msg = @"网络失败";
                    }
                    weakSelf.loadDataFaild(resp.code, msg);
                }
            }
            
        } else {
            if (weakSelf.loadDataFaild) {
                weakSelf.loadDataFaild(0, @"网络不给力");
            }
        }
    };
    
    [AppleServer.shared getWithUrl:path Paramters:param Callback:postHandleBlock];
}

- (void)refreshDataToCurrentPage
{
    NSInteger currentPage = 0;
    NSInteger maxPage = 0;
    do {
        currentPage++;
        maxPage += RESTING_ORDER_MAXPAGE;
    } while (maxPage < self.dataArray.count);
    
    self.page = 0;
    self.dataArray = nil;
    
    YDWeakSelf
    [self loadDataWithMaxPage:maxPage completion:^{
        weakSelf.page = currentPage;
    }];
}

@end

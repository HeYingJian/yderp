//
//  YDRestingOrderVC.m
//  YDERP
//
//  Created by 何英健 on 2021/8/18.
//

#import "YDRestingOrderVC.h"
#import "YDRestingOrderVM.h"
#import "YDRestingOrderCell.h"
#import "YDMakeListVC.h"

@interface YDRestingOrderVC () <UITableViewDelegate, UITableViewDataSource>

@property (nonatomic, strong) YDRestingOrderVM *vm;

@property (nonatomic, weak) IBOutlet UITableView *tableView;

@property (nonatomic, weak) IBOutlet UILabel *countLabel;

@property (nonatomic, assign) BOOL isFirstTimeLoading;

@property (nonatomic, weak) IBOutlet NSLayoutConstraint *noticeViewH;

@end

@implementation YDRestingOrderVC

#pragma mark - 懒加载

- (YDRestingOrderVM *)vm
{
    if (!_vm) {
        _vm = [[YDRestingOrderVM alloc] init];
    }
    
    return _vm;
}

#pragma mark - 页面加载

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.isFirstTimeLoading = YES;
    
    [self initUI];
    
    [self addNotification];
    
    [self refreshData];
}

- (void)initUI
{
    self.title = @"待开单列表";
    [self.navigationItem addDefaultBackButton:self];
    
    self.noticeViewH.constant = 0;
    
    [self setupTableView];
}

- (void)setupTableView
{
    self.tableView.hidden = YES;
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    self.tableView.showsVerticalScrollIndicator = NO;
    self.tableView.showsHorizontalScrollIndicator = NO;
    
    if (@available(iOS 11.0, *)) {
        self.tableView.contentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentNever;
        
    } else {
        self.automaticallyAdjustsScrollViewInsets = NO;
    }
    
    self.tableView.mj_header = [YDRefreshHeader headerWithRefreshingTarget:self refreshingAction:@selector(refreshData)];
    
    self.tableView.mj_footer = [YDRefreshFooter footerWithRefreshingTarget:self refreshingAction:@selector(loadMore)];
}

- (void)addNotification
{
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(refreshDataToCurrentPage) name:REFRESH_RESTING_ORDER object:nil];
}

- (void)refreshData
{
    if (self.isFirstTimeLoading) {
        [YDLoadingView showToSuperview:nil];
    }
    
    YDWeakSelf
    if (!self.vm.loadDataDone) {
        self.vm.loadDataDone = ^(BaseVMRefreshType type) {
            weakSelf.isFirstTimeLoading = NO;
            weakSelf.tableView.hidden = NO;
            [YDLoadingView hideToSuperview:weakSelf.view];
            [weakSelf.tableView.mj_header endRefreshing];
            
            weakSelf.countLabel.text = [NSString stringWithFormat:@"%ld条", (long)weakSelf.vm.total];
            if (weakSelf.vm.dataArray.count) {
                weakSelf.noticeViewH.constant = 40;
                
            } else {
                weakSelf.noticeViewH.constant = 0;
            }
            
            if (type == BaseVMRefreshTypeNoMore) {
                [weakSelf.tableView.mj_footer endRefreshingWithNoMoreData];

            } else {
                [weakSelf.tableView.mj_footer endRefreshing];
            }
            
            [weakSelf.tableView reloadData];
        };
    }
    
    if (!self.vm.loadDataFaild) {
        self.vm.loadDataFaild = ^(NSInteger code, NSString *error) {
            weakSelf.isFirstTimeLoading = NO;
            weakSelf.tableView.hidden = NO;
            [YDLoadingView hideToSuperview:weakSelf.view];
            [weakSelf.tableView.mj_header endRefreshing];
            [weakSelf.tableView.mj_footer endRefreshing];
            
            [MBProgressHUD showToastMessage:error];
        };
    }
    
    [self.vm loadFirstPageData];
}

- (void)loadMore
{
    [self.vm loadNextPageData];
}

- (void)refreshDataToCurrentPage
{
    self.tableView.contentOffset = CGPointMake(0, 0);
    
    [YDLoadingView showToSuperview:self.view];
    [self.vm refreshDataToCurrentPage];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    self.navigationController.navigationBar.translucent = YES;
    [self.navigationController setNavigationBarHidden:NO animated:NO];
}

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

#pragma mark - UITableView代理

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    if (self.vm.dataArray.count) {
        return self.vm.dataArray.count;
        
    } else {
        return 1;
    }
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (self.vm.dataArray.count) {
        YDRestingOrderData *data = self.vm.dataArray[indexPath.section];
        return [YDRestingOrderCell getHeightWithData:data];
        
    } else {
        return [YDNullCell getHeightWithTableView:tableView];
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (self.vm.dataArray.count) {
        YDRestingOrderCell *cell = [YDRestingOrderCell cellWithTableView:tableView];
        YDRestingOrderData *data = self.vm.dataArray[indexPath.section];
        cell.data = data;
        YDWeakSelf
        cell.deleteBlock = ^(YDRestingOrderData *data) {
            [weakSelf.vm.dataArray removeObject:data];
            [weakSelf.tableView reloadData];
        };
        
        return cell;
        
    } else {
        YDNullCell *cell = [YDNullCell cellWithTableView:tableView];
        cell.content = @"暂无待开单数据！";
        
        return cell;
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 0.01;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    return [[UIView alloc] init];
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    return 10;
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section
{
    return [[UIView alloc] init];
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (self.vm.dataArray.count) {
        YDRestingOrderData *data = self.vm.dataArray[indexPath.section];
        
        [YDLoadingView showToSuperview:self.view];
        
        YDWeakSelf
        // 请求订单详情数据
        [YDFunction getOrderDetailWithType:0 orderID:data.orderId completion:^(YDOrderDetailData *data) {
            [YDLoadingView hideToSuperview:weakSelf.view];
            
            // 进入开单页面
            YDMakeListVC *vc = [[YDMakeListVC alloc] init];
            vc.data = data;
            vc.refreshBlock = ^{
                [weakSelf refreshData];
            };
            
            UINavigationController *nav = [[UINavigationController alloc] initWithRootViewController:vc];
            nav.modalPresentationStyle = UIModalPresentationFullScreen;
            [self presentViewController:nav animated:YES completion:nil];
        
        } refreshBlock:^(NSString *msg) {
            [YDLoadingView hideToSuperview:weakSelf.view];
            [YDNoticePopView showWithTitle:msg completion:^{
                [weakSelf refreshData];
            }];
            
        } faildBlock:^(NSString *msg) {
            [YDLoadingView hideToSuperview:weakSelf.view];
            [MBProgressHUD showToastMessage:msg];
        }];
    }
}

@end

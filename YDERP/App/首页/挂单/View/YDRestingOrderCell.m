//
//  YDRestingOrderCell.m
//  YDERP
//
//  Created by 何英健 on 2021/8/18.
//

#import "YDRestingOrderCell.h"

@interface YDRestingOrderCell ()

@property (nonatomic, weak) IBOutlet UILabel *nameLabel;
@property (nonatomic, weak) IBOutlet UILabel *orderNoLabel;
@property (nonatomic, weak) IBOutlet UILabel *quantityLabel;
@property (nonatomic, weak) IBOutlet UILabel *priceLabel;
@property (nonatomic, weak) IBOutlet UILabel *addressLabel;
@property (nonatomic, weak) IBOutlet UILabel *timeLabel;

@property (nonatomic, weak) IBOutlet NSLayoutConstraint *addressViewH;
@property (nonatomic, weak) IBOutlet NSLayoutConstraint *addressViewBottim;

@end

@implementation YDRestingOrderCell

+ (instancetype)cellWithTableView:(UITableView *)tableView
{
    static NSString *reuseID = @"restingOrderCell";
    YDRestingOrderCell *cell = [tableView dequeueReusableCellWithIdentifier:reuseID];
    if (!cell) {
        cell = [[NSBundle mainBundle] loadNibNamed:@"YDRestingOrderCell" owner:nil options:nil][0];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    
    return cell;
}

+ (CGFloat)getHeightWithData:(YDRestingOrderData *)data
{
    CGFloat addressH = 0;
    if (data.clientAddress.length) {
        addressH = 32;
    }
    
    return 164 + addressH;
}

- (void)setData:(YDRestingOrderData *)data
{
    _data = data;
    
    NSString *name =  @"临时客户";
    if (data.clientName.length) {
        name = data.clientName;
    }
    if (name.length > 5) {
        name = [NSString stringWithFormat:@"%@…", [name substringToIndex:5]];
    }
    self.nameLabel.text = name;
    
    self.orderNoLabel.text = [NSString stringWithFormat:@"订单%@", data.orderNo];
    self.quantityLabel.text = [NSString stringWithFormat:@"%ld", (long)data.totalNum];
    self.priceLabel.text = [NSString applePrefixPriceWith:data.totalAmount];
    self.timeLabel.text = data.orderTime;
    
    if (data.clientAddress.length) {
        self.addressViewH.constant = 17;
        self.addressViewBottim.constant = 15;
        self.addressLabel.text = data.clientAddress;
        
    } else {
        self.addressViewH.constant = 0;
        self.addressViewBottim.constant = 0;
    }
}

- (IBAction)tapDeleteBtn:(id)sender
{
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"删除后将不可恢复，确认删除么？" message:nil preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *confirm = [UIAlertAction actionWithTitle:@"确定" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        [self deleteOrder];
    }];
    UIAlertAction* cancel = [UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleCancel handler:nil];

    [alert addAction:cancel];
    [alert addAction:confirm];

    [[UINavigationController rootNavigationController] presentViewController:alert animated:YES completion:nil];
}

- (void)deleteOrder
{
    [YDLoadingView showToSuperview:nil];
    
    NSString *path = URL_DELETE_ORDER;
    path = [NSString stringWithFormat:@"%@/%@", path, self.data.orderId];
    NSMutableDictionary *param = [NSMutableDictionary dictionary];

    AppleServerPostCompleteBlock postHandleBlock = ^(BOOL done, id _Nullable object, NSString * _Nullable error) {
        [YDLoadingView hideToSuperview:nil];
        
        if (done) {
            YDBaseModel *resp = [YDBaseModel modelWithJSON:object];
            
            if (resp.code == 200) {
                [MBProgressHUD showToastMessage:@"删除成功"];
                
                if (self.deleteBlock) {
                    self.deleteBlock(self.data);
                }
            
            } else {
                NSString *msg;
                if (resp.detailMessage.length) {
                    msg = resp.detailMessage;
                    
                } else if (resp.message.length) {
                    msg = resp.message;
                    
                } else {
                    msg = @"网络失败";
                }
                [MBProgressHUD showToastMessage:msg];
            }
            
        } else {
            [MBProgressHUD showToastMessage:@"网络不给力"];
        }
    };
    
    //使用AppleServer发送POST请求
    [AppleServer.shared postWithPath:path Paramters:param CompleteBlock:postHandleBlock];
}

- (void)awakeFromNib
{
    [super awakeFromNib];
    
    self.quantityLabel.font = FONT_NUMBER(15);
    self.priceLabel.font = FONT_NUMBER(15);
    self.timeLabel.font = FONT_NUMBER(12);
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end

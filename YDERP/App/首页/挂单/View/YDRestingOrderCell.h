//
//  YDRestingOrderCell.h
//  YDERP
//
//  Created by 何英健 on 2021/8/18.
//

#import <UIKit/UIKit.h>
#import "YDRestingOrderModel.h"

typedef void(^BlockRestingOrderDelete)(YDRestingOrderData *data);

@interface YDRestingOrderCell : UITableViewCell

+ (instancetype)cellWithTableView:(UITableView *)tableView;

+ (CGFloat)getHeightWithData:(YDRestingOrderData *)data;

@property (nonatomic, strong) YDRestingOrderData *data;

@property (nonatomic, copy) BlockRestingOrderDelete deleteBlock;

@end

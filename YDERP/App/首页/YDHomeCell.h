//
//  YDHomeCell.h
//  YDERP
//
//  Created by 何英健 on 2021/7/21.
//

#import <UIKit/UIKit.h>
#import "YDHomeModel.h"

typedef enum : NSUInteger {
    YDHomeTypeToday = 0, // 显示今天
    YDHomeTypeMonth // 显示本月
} YDHomeType;

typedef void(^BlockTapChangeStatusBtn)(void);

@interface YDHomeCell : UITableViewCell

+ (instancetype)cellWithTableView:(UITableView *)tableView;

+ (CGFloat)getHeight;

@property (nonatomic, strong) YDHomeData *data;
// 显示类型
@property (nonatomic, assign) YDHomeType type;
@property (nonatomic, copy) BlockTapChangeStatusBtn changeStatusBlock;

@end

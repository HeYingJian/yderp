//
//  YDDeliveryDetailEditPhotoCell.h
//  YDERP
//
//  Created by 何英健 on 2021/10/21.
//

#import <UIKit/UIKit.h>
#import "YDDeliveryDetailModel.h"

typedef void(^BlockyDeliveryEditRefresh)(void);

@interface YDDeliveryDetailEditPhotoCell : UITableViewCell

+ (instancetype)cellWithTableView:(UITableView *)tableView;

+ (CGFloat)getHeightWithData:(YDDeliveryDetailData *)data;

@property (nonatomic, strong) YDDeliveryDetailData *data;

@property (nonatomic, weak) UIViewController *rootVC;

@property (nonatomic, copy) BlockyDeliveryEditRefresh editRefreshBlock;

@end

//
//  YDDeliveryDetailEditPhotoCell.m
//  YDERP
//
//  Created by 何英健 on 2021/10/21.
//

#import "YDDeliveryDetailEditPhotoCell.h"
#import "YDModifyMessagePopView.h"
#import "YDAddProductColCell.h"
#import "YDOrderDetailVC.h"

#define DELIVERY_PHOTO_MAXCOUNT 5

@interface YDDeliveryDetailEditPhotoCell () <UITextFieldDelegate, UICollectionViewDelegate, UICollectionViewDataSource, WDImageBrowserDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate>

@property (nonatomic, weak) IBOutlet UITextField *remarkTextField;

@property (nonatomic, weak) IBOutlet UICollectionView *collectionView;

@property (nonatomic, weak) IBOutlet UIView *expressNameView;
@property (nonatomic, weak) IBOutlet UILabel *expressNameLabel;
@property (nonatomic, weak) IBOutlet UIView *expressNoView;
@property (nonatomic, weak) IBOutlet UILabel *expressNoLabel;
@property (nonatomic, weak) IBOutlet UIView *orderView;

@end

@implementation YDDeliveryDetailEditPhotoCell

+ (instancetype)cellWithTableView:(UITableView *)tableView
{
    static NSString *reuseID = @"deliveryEditPhotoCell";
    YDDeliveryDetailEditPhotoCell *cell = [tableView dequeueReusableCellWithIdentifier:reuseID];
    if (!cell) {
        cell = [[NSBundle mainBundle] loadNibNamed:@"YDDeliveryDetailEditPhotoCell" owner:nil options:nil][0];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        
        cell.collectionView.showsHorizontalScrollIndicator = NO;
        
        UICollectionViewFlowLayout *layout = [[UICollectionViewFlowLayout alloc] init];
        // cell宽度
        CGFloat cellWidth = [YDDeliveryDetailEditPhotoCell getCollectionViewCellBorder];
        // cell与cell之间间距
        layout.minimumLineSpacing = 9;
        layout.itemSize = CGSizeMake(cellWidth, cellWidth);
        cell.collectionView.collectionViewLayout = layout;
        
        [cell.collectionView registerClass:[YDAddProductColCell class] forCellWithReuseIdentifier:@"AddProductColCell"];
    }
    
    return cell;
}

+ (CGFloat)getHeightWithData:(YDDeliveryDetailData *)data
{
    NSInteger rowNum = (data.medias.count + 1) / 3;
    if (rowNum * 3 < data.medias.count + 1) {
        rowNum++;
    }
    
    CGFloat cellH = [YDDeliveryDetailEditPhotoCell getCollectionViewCellBorder];
    CGFloat colViewH = rowNum * cellH + (rowNum - 1) * 9;
    
    return colViewH + 396;
}

+ (CGFloat)getCollectionViewCellBorder
{
    return (SCREEN_WIDTH - 17 * 2 - 9 * 2) / 3 - 1;
}

- (void)setData:(YDDeliveryDetailData *)data
{
    _data = data;
    
    self.remarkTextField.text = data.remark;
    self.expressNameLabel.text = data.expressName;
    self.expressNoLabel.text = data.expressNo;
}

// 修改物流公司名
- (void)tapExpressNameView
{
    YDWeakSelf
    [YDModifyMessagePopView showWithTitle:@"请输入快递物流商" content:self.data.expressName placeholder:@"" completion:^(NSString *string, BlockModifyMessageStatus statusBlock) {
        
        weakSelf.data.expressName = string;
        
        if (weakSelf.editRefreshBlock) {
            weakSelf.editRefreshBlock();
        }
        
        statusBlock(YES);
        
    } tapCloseBlock:nil];
}

// 修改物流单号
- (void)tapExpressNoView
{
    YDWeakSelf
    [YDModifyMessagePopView showWithTitle:@"请输入快递单号" content:self.data.expressNo placeholder:@"" completion:^(NSString *string, BlockModifyMessageStatus statusBlock) {
        
        weakSelf.data.expressNo = string;
        
        if (weakSelf.editRefreshBlock) {
            weakSelf.editRefreshBlock();
        }
        
        statusBlock(YES);
        
    } tapCloseBlock:nil];
}

// 跳转关联订单
- (void)tapOrderView
{
    YDOrderDetailVC *vc = [[YDOrderDetailVC alloc] init];
    vc.orderID = [NSString stringWithFormat:@"%ld", (long)self.data.orderId];
    [self.rootVC.navigationController pushViewController:vc animated:YES];
}

- (void)awakeFromNib
{
    [super awakeFromNib];
    
    NSMutableAttributedString *str = [[NSMutableAttributedString alloc] initWithString:@"请填写备注信息" attributes:
         @{NSForegroundColorAttributeName:ColorFromRGB(0xB7B7B7),
           NSFontAttributeName:[UIFont systemFontOfSize:15]}
         ];
    self.remarkTextField.attributedPlaceholder = str;
    
    UITapGestureRecognizer *tap1 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapExpressNameView)];
    [self.expressNameView addGestureRecognizer:tap1];
    
    UITapGestureRecognizer *tap2 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapExpressNoView)];
    [self.expressNoView addGestureRecognizer:tap2];
    
    UITapGestureRecognizer *tap3 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapOrderView)];
    [self.orderView addGestureRecognizer:tap3];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

#pragma mark - UICollectionView 代理

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    if (self.data.medias.count >= DELIVERY_PHOTO_MAXCOUNT) {
        return self.data.medias.count;
        
    } else {
        return self.data.medias.count + 1;
    }
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    YDAddProductColCell *cell = [YDAddProductColCell cellWithTableView:collectionView indexPath:indexPath];
    if (indexPath.row != 0 || self.data.medias.count >= DELIVERY_PHOTO_MAXCOUNT) {
        NSInteger photoIndex = 0;
        if (self.data.medias.count >= DELIVERY_PHOTO_MAXCOUNT) {
            photoIndex = indexPath.row;
            
        } else {
            photoIndex = indexPath.row - 1;
        }
        cell.picUrl = self.data.medias[photoIndex].url;
        
        YDWeakSelf
        cell.tapDeleteBtnBlock = ^(NSString *url) {
            for (YDSupplyDetailDataMedia *sub in weakSelf.data.medias) {
                if ([sub.url isEqualToString:url]) {
                    [weakSelf.data.medias removeObject:sub];
                    
                    if (weakSelf.editRefreshBlock) {
                        weakSelf.editRefreshBlock();
                    }
                    break;
                }
            }
        };
        
        cell.showDeleteBtn = YES;
        
    } else {
        cell.showDeleteBtn = NO;
    }
    
    return cell;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row == 0 && self.data.medias.count < DELIVERY_PHOTO_MAXCOUNT) {
        [self addPhotos];
        
    } else {
        UICollectionViewCell *cell = [self.collectionView cellForItemAtIndexPath:indexPath];
        
        NSInteger tapIndex;
        if (self.data.medias.count < DELIVERY_PHOTO_MAXCOUNT) {
            tapIndex = indexPath.row - 1;
            
        } else {
            tapIndex = indexPath.row;
        }
        
        NSMutableArray *photoArray = [NSMutableArray array];
        for (YDSearcherPicList *sub in self.data.medias) {
            if (sub.url.length) {
                [photoArray addObject:sub.url];
            }
        }
        
        // 浏览图片相册
        WDImageBrowser *browser = [[WDImageBrowser alloc] init];
        [browser setupWithDelegate:self tappedIndex:tapIndex imageUrls:photoArray originView:cell];
        [browser showBrowser];
    }
}

#pragma mark - 上传图片

- (void)addPhotos
{
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:nil message:nil preferredStyle:UIAlertControllerStyleActionSheet];
    
    // 需要添加此项设置，否则ipad会崩溃
    alert.popoverPresentationController.sourceView = self.rootVC.view;
    alert.popoverPresentationController.sourceRect = CGRectMake(0, SCREEN_HEIGHT - 70, SCREEN_WIDTH, 70);
    
    //相机
    UIAlertAction *camera = [UIAlertAction actionWithTitle:@"拍照" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        [self showCarmera];
    }];
    
    //相册
    UIAlertAction* library = [UIAlertAction actionWithTitle:@"我的相册" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        [self showPhotoAlbum];
    }];
    
    //取消
    UIAlertAction* cancel = [UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleCancel handler:nil];
    
    [alert addAction:camera];
    [alert addAction:library];
    [alert addAction:cancel];
    
    [self.rootVC presentViewController:alert animated:YES completion:nil];
}

- (void)showCarmera
{
    if (![YDPhotoRight checkHaveCameraRight]) {
        return;
    }
    
    YDImagePickerController *ipvc = [[YDImagePickerController alloc] init];
    ipvc.delegate = self;
    ipvc.sourceType = UIImagePickerControllerSourceTypeCamera;
    ipvc.modalPresentationStyle = UIModalPresentationFullScreen;

    [self.rootVC showViewController:ipvc sender:nil];
}

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary<UIImagePickerControllerInfoKey,id> *)info
{
    UIImage *image = [info objectForKey:UIImagePickerControllerOriginalImage];

    if (image) {
        [self uploadImgArray:[NSArray arrayWithObject:image]];
    }
    
    [picker dismissViewControllerAnimated:YES completion:nil];
}

// 上传图片
- (void)uploadImgArray:(NSArray<UIImage *> *)imgArray
{
    [YDLoadingView showToSuperview:self.rootVC.view];
    
    YDWeakSelf
    [YDFunction uploadImageArray:imgArray type:1 group:1 groupStr:nil parameters:nil sucess:^(NSMutableArray<NSString *> *urlArray) {
        [YDLoadingView hideToSuperview:weakSelf.rootVC.view];
        
        for (NSString *sub in urlArray) {
            YDSupplyDetailDataMedia *media = [[YDSupplyDetailDataMedia alloc] init];
            media.url = sub;
            [weakSelf.data.medias addObject:media];
        }
        
        if (weakSelf.editRefreshBlock) {
            weakSelf.editRefreshBlock();
        }
        
    } failure:^(NSString *msg) {
        [YDLoadingView hideToSuperview:weakSelf.rootVC.view];
        [MBProgressHUD showToastMessage:msg];
    }];
}

// 显示相册选择
- (void)showPhotoAlbum
{
    if (![YDPhotoRight checkHavePhotoLibraryRight]) {
        return;
    }
    
    YDWeakSelf
    TZImagePickerController *imagePickerVc = [[TZImagePickerController alloc] initWithMaxImagesCount:DELIVERY_PHOTO_MAXCOUNT - self.data.medias.count delegate:nil];
    imagePickerVc.allowPickingOriginalPhoto = NO;
    imagePickerVc.showSelectedIndex = YES;
    imagePickerVc.naviTitleColor = ColorFromRGB(0x0095F7);
    imagePickerVc.barItemTextColor = ColorFromRGB(0x0095F7);
    imagePickerVc.allowPickingVideo = NO;
    imagePickerVc.modalPresentationStyle = UIModalPresentationFullScreen;
    [imagePickerVc setDidFinishPickingPhotosHandle:^(NSArray<UIImage *> *photos, NSArray *assets, BOOL isSelectOriginalPhoto) {
        [weakSelf uploadImgArray:photos];
    }];
    [self.rootVC presentViewController:imagePickerVc animated:YES completion:nil];
}

#pragma mark - UITextFieldDelegate

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    YDWeakSelf
    [YDModifyMessagePopView showWithTitle:@"请输入备注信息" content:self.data.remark placeholder:@"最多可填写200字备注" completion:^(NSString *string, BlockModifyMessageStatus statusBlock) {
        if (string.length > 200) {
            [MBProgressHUD showToastMessage:@"最多可填写200字"];
            statusBlock(NO);
            return;
        }
        
        weakSelf.data.remark = string;
        
        if (weakSelf.editRefreshBlock) {
            weakSelf.editRefreshBlock();
        }
        
        statusBlock(YES);
        
    } tapCloseBlock:nil];
    
    return NO;
}

@end

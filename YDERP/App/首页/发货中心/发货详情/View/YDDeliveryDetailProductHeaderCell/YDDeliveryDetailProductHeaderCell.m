//
//  YDDeliveryDetailProductHeaderCell.m
//  YDERP
//
//  Created by 何英健 on 2021/10/20.
//

#import "YDDeliveryDetailProductHeaderCell.h"

@interface YDDeliveryDetailProductHeaderCell ()

@property (nonatomic, weak) IBOutlet UILabel *label;

@end

@implementation YDDeliveryDetailProductHeaderCell

+ (instancetype)cellWithTableView:(UITableView *)tableView
{
    static NSString *reuseID = @"productHeaderCell";
    YDDeliveryDetailProductHeaderCell *cell = [tableView dequeueReusableCellWithIdentifier:reuseID];
    if (!cell) {
        cell = [[NSBundle mainBundle] loadNibNamed:@"YDDeliveryDetailProductHeaderCell" owner:nil options:nil][0];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    
    return cell;
}

+ (CGFloat)getHeight
{
    return 50;
}

- (void)setData:(YDDeliveryDetailData *)data
{
    _data = data;
    
    switch (self.type) {
        case YDDeliveryDetailType0:
        {
            self.label.text = [NSString stringWithFormat:@"待发货共%ld件", (long)data.deliveryNum];
        }
            break;
        
        case YDDeliveryDetailType1:
        {
            self.label.text = [NSString stringWithFormat:@"已发货共%ld件", (long)data.deliveryNum];
        }
            break;
            
        case YDDeliveryDetailType2:
        {
            self.label.text = [NSString stringWithFormat:@"驳回重配共%ld件", (long)data.deliveryNum];
        }
            break;
            
        default:
            break;
    }
}

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end

//
//  YDDeliveryDetailProductHeaderCell.h
//  YDERP
//
//  Created by 何英健 on 2021/10/20.
//

#import <UIKit/UIKit.h>
#import "YDDeliveryDetailModel.h"
#import "YDDeliveryDetailVC.h"

@interface YDDeliveryDetailProductHeaderCell : UITableViewCell

+ (instancetype)cellWithTableView:(UITableView *)tableView;

+ (CGFloat)getHeight;

@property (nonatomic, assign) YDDeliveryDetailType type;

@property (nonatomic, strong) YDDeliveryDetailData *data;

@end

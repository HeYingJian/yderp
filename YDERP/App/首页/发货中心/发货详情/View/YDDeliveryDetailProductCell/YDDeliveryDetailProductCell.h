//
//  YDDeliveryDetailProductCell.h
//  YDERP
//
//  Created by 何英健 on 2021/10/20.
//

#import <UIKit/UIKit.h>
#import "YDDeliveryDetailModel.h"

@interface YDDeliveryDetailProductCell : UITableViewCell

+ (instancetype)cellWithTableView:(UITableView *)tableView;

+ (CGFloat)getHeightWithData:(YDDeliveryDetailDataItem *)data;

@property (nonatomic, strong) YDDeliveryDetailDataItem *data;

@end


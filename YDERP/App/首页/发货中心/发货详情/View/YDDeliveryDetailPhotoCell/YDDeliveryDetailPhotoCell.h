//
//  YDDeliveryDetailPhotoCell.h
//  YDERP
//
//  Created by 何英健 on 2021/10/20.
//

#import <UIKit/UIKit.h>
#import "YDDeliveryDetailModel.h"

@interface YDDeliveryDetailPhotoCell : UITableViewCell

+ (instancetype)cellWithTableView:(UITableView *)tableView;

+ (CGFloat)getHeightWithData:(YDDeliveryDetailData *)data;

@property (nonatomic, strong) YDDeliveryDetailData *data;

@property (nonatomic, weak) UIViewController *rootVC;

@end

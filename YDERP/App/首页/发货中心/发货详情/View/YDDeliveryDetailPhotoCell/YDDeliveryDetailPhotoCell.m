//
//  YDDeliveryDetailPhotoCell.m
//  YDERP
//
//  Created by 何英健 on 2021/10/20.
//

#import "YDDeliveryDetailPhotoCell.h"
#import "YDAddProductColCell.h"
#import "YDOrderDetailVC.h"

#define DELIVERY_PHOTO_MAXCOUNT 5

@interface YDDeliveryDetailPhotoCell () <UICollectionViewDelegate, UICollectionViewDataSource, WDImageBrowserDelegate>

@property (nonatomic, weak) IBOutlet UILabel *remarkLabel;
@property (nonatomic, weak) IBOutlet UILabel *expressNameLabel;
@property (nonatomic, weak) IBOutlet UILabel *expressNoLabel;

@property (nonatomic, weak) IBOutlet UICollectionView *collectionView;

@property (nonatomic, weak) IBOutlet UIView *orderView;

@end

@implementation YDDeliveryDetailPhotoCell

+ (instancetype)cellWithTableView:(UITableView *)tableView
{
    static NSString *reuseID = @"PhotoCell";
    YDDeliveryDetailPhotoCell *cell = [tableView dequeueReusableCellWithIdentifier:reuseID];
    if (!cell) {
        cell = [[NSBundle mainBundle] loadNibNamed:@"YDDeliveryDetailPhotoCell" owner:nil options:nil][0];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        
        cell.collectionView.showsHorizontalScrollIndicator = NO;
        
        UICollectionViewFlowLayout *layout = [[UICollectionViewFlowLayout alloc] init];
        // cell宽度
        CGFloat cellWidth = [YDDeliveryDetailPhotoCell getCollectionViewCellBorder];
        // cell与cell之间间距
        layout.minimumLineSpacing = 9;
        layout.itemSize = CGSizeMake(cellWidth, cellWidth);
        cell.collectionView.collectionViewLayout = layout;
        
        [cell.collectionView registerClass:[YDAddProductColCell class] forCellWithReuseIdentifier:@"AddProductColCell"];
    }
    
    return cell;
}

+ (CGFloat)getHeightWithData:(YDSupplyDetailData *)data
{
    CGFloat photoH = 0;
    if (data.medias.count) {
        NSInteger rowNum = (data.medias.count) / 3;
        if (rowNum * 3 < data.medias.count) {
            rowNum++;
        }
        
        CGFloat cellH = [YDDeliveryDetailPhotoCell getCollectionViewCellBorder];
        CGFloat colViewH = rowNum * cellH + (rowNum - 1) * 9;
        photoH = 68 + colViewH;
    }
    
    return 65 + photoH + 124 + 140;
}

- (void)setData:(YDDeliveryDetailData *)data
{
    _data = data;
    
    NSString *remark = @"--";
    if (data.remark.length) {
        remark = data.remark;
    }
    self.remarkLabel.text = remark;
    
    NSString *expressName = @"--";
    if (data.expressName.length) {
        expressName = data.expressName;
    }
    self.expressNameLabel.text = expressName;
    
    NSString *expressNo = @"--";
    if (data.expressNo.length) {
        expressNo = data.expressNo;
    }
    self.expressNoLabel.text = expressNo;
}

+ (CGFloat)getCollectionViewCellBorder
{
    return (SCREEN_WIDTH - 17 * 2 - 9 * 2) / 3 - 1;
}

// 跳转关联订单
- (void)tapOrderView
{
    YDOrderDetailVC *vc = [[YDOrderDetailVC alloc] init];
    vc.orderID = [NSString stringWithFormat:@"%ld", (long)self.data.orderId];
    [self.rootVC.navigationController pushViewController:vc animated:YES];
}

- (void)awakeFromNib
{
    [super awakeFromNib];
    
    UITapGestureRecognizer *tap1 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapOrderView)];
    [self.orderView addGestureRecognizer:tap1];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

#pragma mark - UICollectionView 代理

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return self.data.medias.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    YDAddProductColCell *cell = [YDAddProductColCell cellWithTableView:collectionView indexPath:indexPath];
    cell.showDeleteBtn = NO;
    cell.picUrl = self.data.medias[indexPath.row].url;
    
    return cell;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    UICollectionViewCell *cell = [self.collectionView cellForItemAtIndexPath:indexPath];

    NSMutableArray *photoArray = [NSMutableArray array];
    for (YDSupplyDetailDataMedia *sub in self.data.medias) {
        if (sub.url.length) {
            [photoArray addObject:sub.url];
        }
    }
    
    // 浏览图片相册
    WDImageBrowser *browser = [[WDImageBrowser alloc] init];
    [browser setupWithDelegate:self tappedIndex:indexPath.row imageUrls:photoArray originView:cell];
    [browser showBrowser];
}

@end

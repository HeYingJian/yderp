//
//  YDDeliveryDetailVC.h
//  YDERP
//
//  Created by 何英健 on 2021/10/20.
//

#import <UIKit/UIKit.h>

typedef enum : NSUInteger {
    YDDeliveryDetailType0, // 待发货
    YDDeliveryDetailType1, // 已发货
    YDDeliveryDetailType2, // 驳回重配
} YDDeliveryDetailType;

@interface YDDeliveryDetailVC : UIViewController

@property (nonatomic, copy) NSString *ID;

@end

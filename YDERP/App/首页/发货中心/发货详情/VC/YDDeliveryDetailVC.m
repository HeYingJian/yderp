//
//  YDDeliveryDetailVC.m
//  YDERP
//
//  Created by 何英健 on 2021/10/20.
//

#import "YDDeliveryDetailVC.h"
#import "YDDeliveryDetailModel.h"
#import "YDSupplyDetailClientInfoCell.h"
#import "YDDeliveryDetailProductHeaderCell.h"
#import "YDDeliveryDetailProductCell.h"
#import "YDDeliveryDetailPhotoCell.h"
#import "YDDeliveryDetailEditPhotoCell.h"

@interface YDDeliveryDetailVC () <UITableViewDelegate, UITableViewDataSource>

@property (nonatomic, assign) YDDeliveryDetailType type;

@property (nonatomic, strong) YDDeliveryDetailData *data;

@property (nonatomic, weak) IBOutlet UITableView *tableView;

@property (nonatomic, weak) IBOutlet UIButton *confirmBtn;

@property (nonatomic, weak) IBOutlet NSLayoutConstraint *bottomH;

@end

@implementation YDDeliveryDetailVC

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self initUI];

    [self refreshData];
}

- (void)initUI
{
    self.title = @"发货详情";
    [self.navigationItem addDefaultBackButton:self];
    
    [self setupTableView];
}

- (void)setupTableView
{
    self.tableView.hidden = YES;
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    self.tableView.showsVerticalScrollIndicator = NO;
    self.tableView.showsHorizontalScrollIndicator = NO;
    
    if (@available(iOS 11.0, *)) {
        self.tableView.contentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentNever;
        
    } else {
        self.automaticallyAdjustsScrollViewInsets = NO;
    }
}

- (void)refreshUI
{
    if (self.type == YDDeliveryDetailType0 || self.type == YDDeliveryDetailType1) {
        UIButton *rightBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        NSString *title;
        if (self.type == YDDeliveryDetailType0) {
            title = @"驳回重配";
            
        } else {
            title = @"撤回发货";
        }
        [rightBtn setTitle:title forState:UIControlStateNormal];
        [rightBtn setTitleColor:ColorFromRGB(0x222222) forState:UIControlStateNormal];
        rightBtn.titleLabel.font = [UIFont systemFontOfSize:13.0f weight:UIFontWeightRegular];
        [rightBtn addTarget:self action:@selector(tapRightBtn) forControlEvents:UIControlEventTouchUpInside];
        UIBarButtonItem *item = [[UIBarButtonItem alloc] initWithCustomView:rightBtn];
        self.navigationItem.rightBarButtonItem = item;
    }
    
    if (self.type == YDDeliveryDetailType0 || self.type == YDDeliveryDetailType2) {
        self.bottomH.constant = 65;
        
        if (self.type == YDDeliveryDetailType0) {
            [self.confirmBtn setTitle:@"确认发货" forState:UIControlStateNormal];
            
        } else {
            [self.confirmBtn setTitle:@"确定" forState:UIControlStateNormal];
        }
        
    } else {
        self.bottomH.constant = 0;
    }
}

- (void)tapRightBtn
{
    if (self.type == YDDeliveryDetailType0) {
        // 驳回重配
        YDNormalSelectPopView *view = [YDNormalSelectPopView show];
        view.contentLabel.font = [UIFont systemFontOfSize:17];
        view.content = @"确定驳回此发货单进行重新配货吗？";
        view.leftBtnText = @"取消";
        view.rightBtnText = @"确定";
        YDWeakSelf
        view.tapRightBtnBlock = ^{
            [weakSelf redelivey];
        };
        
    } else {
        // 撤回发货
        YDSelectPopView *view = [YDSelectPopView show];
        view.title = @"温馨提示";
        view.content = @"撤回发货后单据不能恢复！";
        view.leftBtnText = @"取消";
        view.rightBtnText = @"确定";
        YDWeakSelf
        view.tapRightBtnBlock = ^{
            [weakSelf withdraw];
        };
    }
}

- (IBAction)tapConfirmBtn:(id)sender
{
    if (self.type == YDDeliveryDetailType0) {
        // 待发货状态下，确认发货
        [self confirmDelivery];
        
    } else if (self.type == YDDeliveryDetailType2) {
        // 驳回重配状态，返回发货列表
        [self.navigationController popViewControllerAnimated:YES];
    }
}

#pragma mark - 接口请求

- (void)refreshData
{
    [YDLoadingView showToSuperview:nil];
    
    YDWeakSelf
    NSString *path = URL_DELIVERY_DETAIL;
    
    NSMutableDictionary *param = [NSMutableDictionary dictionary];
    [param setObject:self.ID forKey:@"id"];

    AppleServerPostCompleteBlock postHandleBlock = ^(BOOL done, id _Nullable object, NSString * _Nullable error) {
        [YDLoadingView hideToSuperview:nil];
        weakSelf.tableView.hidden = NO;
        
        if (done) {
            YDDeliveryDetailModel *resp = [YDDeliveryDetailModel modelWithJSON:object];
            
            if (resp.code == 200) {
                weakSelf.data = resp.data;
                
                switch (weakSelf.data.status) {
                    // 待发货
                    case 10:
                        weakSelf.type = YDDeliveryDetailType0;
                        break;
                        
                    // 已发货
                    case 20:
                        weakSelf.type = YDDeliveryDetailType1;
                        break;
                        
                    // 驳回重配
                    case 30:
                        weakSelf.type = YDDeliveryDetailType2;
                        break;
                        
                    default:
                        break;
                }
                
                [weakSelf refreshUI];
                [weakSelf.tableView reloadData];
            
            } else {
                NSString *msg;
                if (resp.detailMessage.length) {
                    msg = resp.detailMessage;
                    
                } else if (resp.message.length) {
                    msg = resp.message;
                    
                } else {
                    msg = @"网络失败";
                }
                [MBProgressHUD showToastMessage:msg];
            }
            
        } else {
            [MBProgressHUD showToastMessage:@"网络不给力"];
        }
    };
    
    //使用AppleServer发送POST请求
    [AppleServer.shared postWithPath:path Paramters:param CompleteBlock:postHandleBlock];
}

// 确认发货
- (void)confirmDelivery
{
    [YDLoadingView showToSuperview:self.view];
    
    YDWeakSelf
    NSString *path = URL_DELIVERY_DETAIL_SAVE;
    
    NSMutableDictionary *param = [NSMutableDictionary dictionary];
    [param setObject:self.ID forKey:@"id"];
    NSString *remark = @"";
    if (self.data.remark.length) {
        remark = self.data.remark;
    }
    [param setObject:remark forKey:@"remark"];
    
    NSMutableArray *photoArray = [NSMutableArray array];
    for (YDSupplyDetailDataMedia *sub in self.data.medias) {
        if (sub.url.length) {
            [photoArray addObject:sub.url];
        }
    }
    [param setObject:photoArray forKey:@"picUrlList"];
    
    NSString *expressName = @"";
    if (self.data.expressName.length) {
        expressName = self.data.expressName;
    }
    [param setObject:expressName forKey:@"expressName"];
    
    NSString *expressNo = @"";
    if (self.data.expressNo.length) {
        expressNo = self.data.expressNo;
    }
    [param setObject:expressNo forKey:@"expressNo"];

    AppleServerPostCompleteBlock postHandleBlock = ^(BOOL done, id _Nullable object, NSString * _Nullable error) {
        [YDLoadingView hideToSuperview:weakSelf.view];
        
        if (done) {
            YDBaseModel *resp = [YDBaseModel modelWithJSON:object];
            
            if (resp.code == 200) {
                [MBProgressHUD showToastMessage:@"发货成功"];
 
                // 通知订单列表刷新
                PostNotification(REFRESH_DELIVERY_LIST, nil);
                
                dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1.0f * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                    [weakSelf.navigationController popViewControllerAnimated:YES];
                });
                
            } else {
                NSString *msg;
                if (resp.detailMessage.length) {
                    msg = resp.detailMessage;
                    
                } else if (resp.message.length) {
                    msg = resp.message;
                    
                } else {
                    msg = @"网络失败";
                }
                [MBProgressHUD showToastMessage:msg];
            }
            
        } else {
            [MBProgressHUD showToastMessage:@"网络不给力"];
        }
    };
    
    //使用AppleServer发送POST请求
    [AppleServer.shared postWithPath:path Paramters:param CompleteBlock:postHandleBlock];
}

// 驳回重配
- (void)redelivey
{
    [YDLoadingView showToSuperview:self.view];
    
    YDWeakSelf
    NSString *path = URL_DELIVERY_DETAIL_REJECT;
    
    NSMutableDictionary *param = [NSMutableDictionary dictionary];
    [param setObject:self.ID forKey:@"id"];

    AppleServerPostCompleteBlock postHandleBlock = ^(BOOL done, id _Nullable object, NSString * _Nullable error) {
        [YDLoadingView hideToSuperview:weakSelf.view];
        
        if (done) {
            YDBaseModel *resp = [YDBaseModel modelWithJSON:object];
            
            if (resp.code == 200) {
                [MBProgressHUD showToastMessage:@"驳回成功"];
 
                // 通知订单列表刷新
                PostNotification(REFRESH_DELIVERY_LIST, nil);
                
                dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1.0f * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                    [weakSelf.navigationController popViewControllerAnimated:YES];
                });
                
            } else {
                NSString *msg;
                if (resp.detailMessage.length) {
                    msg = resp.detailMessage;
                    
                } else if (resp.message.length) {
                    msg = resp.message;
                    
                } else {
                    msg = @"网络失败";
                }
                [MBProgressHUD showToastMessage:msg];
            }
            
        } else {
            [MBProgressHUD showToastMessage:@"网络不给力"];
        }
    };
    
    //使用AppleServer发送POST请求
    [AppleServer.shared postWithPath:path Paramters:param CompleteBlock:postHandleBlock];
}

// 撤回发货
- (void)withdraw
{
    [YDLoadingView showToSuperview:self.view];
    
    YDWeakSelf
    NSString *path = URL_DELIVERY_DETAIL_WITHDRAW;
    
    NSMutableDictionary *param = [NSMutableDictionary dictionary];
    [param setObject:self.ID forKey:@"id"];

    AppleServerPostCompleteBlock postHandleBlock = ^(BOOL done, id _Nullable object, NSString * _Nullable error) {
        [YDLoadingView hideToSuperview:weakSelf.view];
        
        if (done) {
            YDBaseModel *resp = [YDBaseModel modelWithJSON:object];
            
            if (resp.code == 200) {
                [MBProgressHUD showToastMessage:@"撤销成功"];
 
                // 通知订单列表刷新
                PostNotification(REFRESH_DELIVERY_LIST, nil);
                
                dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1.0f * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                    [weakSelf.navigationController popViewControllerAnimated:YES];
                });
                
            } else {
                NSString *msg;
                if (resp.detailMessage.length) {
                    msg = resp.detailMessage;
                    
                } else if (resp.message.length) {
                    msg = resp.message;
                    
                } else {
                    msg = @"网络失败";
                }
                [MBProgressHUD showToastMessage:msg];
            }
            
        } else {
            [MBProgressHUD showToastMessage:@"网络不给力"];
        }
    };
    
    //使用AppleServer发送POST请求
    [AppleServer.shared postWithPath:path Paramters:param CompleteBlock:postHandleBlock];
}

#pragma mark - UITableView代理

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 4;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (section == 2) {
        return self.data.itemList.count;
    }
    
    return 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == 0) {
        return [YDSupplyDetailClientInfoCell getHeight];
        
    } else if (indexPath.section == 1) {
        return [YDDeliveryDetailProductHeaderCell getHeight];
        
    } else if (indexPath.section == 2) {
        YDDeliveryDetailDataItem *data = self.data.itemList[indexPath.row];
        return [YDDeliveryDetailProductCell getHeightWithData:data];
        
    } else if (indexPath.section == 3) {
        if (self.type == YDDeliveryDetailType0) {
            return [YDDeliveryDetailEditPhotoCell getHeightWithData:self.data];
            
        } else {
            return [YDDeliveryDetailPhotoCell getHeightWithData:self.data];
        }
    }
    
    return 0.001;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == 0) {
        YDSupplyDetailClientInfoCell *cell = [YDSupplyDetailClientInfoCell cellWithTableView:tableView];
        cell.deliveryData = self.data;

        return cell;
        
    } else if (indexPath.section == 1) {
        YDDeliveryDetailProductHeaderCell *cell = [YDDeliveryDetailProductHeaderCell cellWithTableView:tableView];
        cell.type = self.type;
        cell.data = self.data;
        
        return cell;
        
    } else if (indexPath.section == 2) {
        YDDeliveryDetailProductCell *cell = [YDDeliveryDetailProductCell cellWithTableView:tableView];
        YDDeliveryDetailDataItem *data = self.data.itemList[indexPath.row];
        cell.data = data;

        return cell;
        
    } else if (indexPath.section == 3) {
        if (self.type == YDDeliveryDetailType0) {
            YDDeliveryDetailEditPhotoCell *cell = [YDDeliveryDetailEditPhotoCell cellWithTableView:tableView];
            cell.data = self.data;
            cell.rootVC = self;
            YDWeakSelf
            cell.editRefreshBlock = ^{
                [weakSelf.tableView reloadData];
            };
            
            return cell;
            
        } else {
            YDDeliveryDetailPhotoCell *cell = [YDDeliveryDetailPhotoCell cellWithTableView:tableView];
            cell.data = self.data;
            cell.rootVC = self;
            
            return cell;
        }
    }
    
    return [[UITableViewCell alloc] init];
}

@end

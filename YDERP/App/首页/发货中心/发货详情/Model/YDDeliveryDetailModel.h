//
//  YDDeliveryDetailModel.h
//  YDERP
//
//  Created by 何英健 on 2021/10/20.
//

#import "YDBaseModel.h"
#import "YDSupplyDetailModel.h"

@interface YDDeliveryDetailDataItem : NSObject

// 商品id
@property (nonatomic, copy) NSString *itemId;
// 编号
@property (nonatomic, copy) NSString *code;
// 名称
@property (nonatomic, copy) NSString *name;
// 商品图url
@property (nonatomic, copy) NSString *picUrl;
// 发货数量
@property (nonatomic, assign) NSInteger deliveryNum;
// sku
@property (nonatomic, strong) NSMutableArray<YDSupplyDetailDataItemSku *> *skuList;

@end

@interface YDDeliveryDetailData : NSObject

// 客户
@property (nonatomic, copy) NSString *clientName;
// 客户配货地址
@property (nonatomic, copy) NSString *address;
// 订单号
@property (nonatomic, copy) NSString *orderNo;
// 销售单id
@property (nonatomic, assign) NSInteger orderId;
// 配货单id
@property (nonatomic, assign) NSInteger allocOrderId;
// 店铺id
@property (nonatomic, assign) NSInteger shopId;
// 客户ID
@property (nonatomic, assign) NSInteger clientId;
// 客户地址
@property (nonatomic, copy) NSString *clientAddress;
// 快递物流商
@property (nonatomic, copy) NSString *expressName;
// 快递单号
@property (nonatomic, copy) NSString *expressNo;
// 订单状态  10: 待发货 20: 已发货 30 驳回重配
@property (nonatomic, assign) NSInteger status;
// 发货数量
@property (nonatomic, assign) NSInteger deliveryNum;
// 发货员id，指向us_user.id
@property (nonatomic, assign) NSInteger deliveryUserId;
// 订单时间
@property (nonatomic, copy) NSString *orderTime;
// 备注
@property (nonatomic, copy) NSString *remark;
// 版本
@property (nonatomic, copy) NSString *version;
// 租户ID
@property (nonatomic, assign) NSInteger tenantId;
// 创建人
@property (nonatomic, copy) NSString *createPerson;
// 创建时间
@property (nonatomic, copy) NSString *createTime;
// 修改人
@property (nonatomic, copy) NSString *updatePerson;
// 最近一次修改时间
@property (nonatomic, copy) NSString *updateTime;
// 逻辑删除标志
@property (nonatomic, assign) NSInteger dr;
// 多媒体信息
@property (nonatomic, strong) NSMutableArray<YDSupplyDetailDataMedia *> *medias;
// 配货商品详情
@property (nonatomic, strong) NSMutableArray<YDDeliveryDetailDataItem *> *itemList;

@end

@interface YDDeliveryDetailModel : YDBaseModel

@property (nonatomic, strong) YDDeliveryDetailData *data;

@end

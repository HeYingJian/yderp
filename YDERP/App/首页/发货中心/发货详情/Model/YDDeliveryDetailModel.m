//
//  YDDeliveryDetailModel.m
//  YDERP
//
//  Created by 何英健 on 2021/10/20.
//

#import "YDDeliveryDetailModel.h"

@implementation YDDeliveryDetailDataItem

+ (NSDictionary<NSString *,id> *)modelContainerPropertyGenericClass{
    return @{@"skuList" : [YDSupplyDetailDataItemSku class]};
}

@end

@implementation YDDeliveryDetailData

- (NSMutableArray<YDSupplyDetailDataMedia *> *)medias
{
    if (!_medias) {
        _medias = [NSMutableArray array];
    }
    
    return _medias;
}

+ (NSDictionary<NSString *,id> *)modelCustomPropertyMapper
{
    return @{@"ID":@"id"};
}

+ (NSDictionary<NSString *,id> *)modelContainerPropertyGenericClass{
    return @{@"medias" : [YDSupplyDetailDataMedia class],
             @"itemList" : [YDDeliveryDetailDataItem class]
            };
}

@end

@implementation YDDeliveryDetailModel

@end

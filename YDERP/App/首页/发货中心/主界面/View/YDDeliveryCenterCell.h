//
//  YDDeliveryCenterCell.h
//  YDERP
//
//  Created by 何英健 on 2021/10/20.
//

#import <UIKit/UIKit.h>

@interface YDDeliveryCenterCell : UITableViewCell

+ (instancetype)cellWithTableView:(UITableView *)tableView index:(NSInteger)index;

+ (CGFloat)getHeightWithIndex:(NSInteger)index;

@property (nonatomic, strong) YDSearcherDeliveryData *data;

@end

//
//  YDDeliveryCenterCell.m
//  YDERP
//
//  Created by 何英健 on 2021/10/20.
//

#import "YDDeliveryCenterCell.h"

@interface YDDeliveryCenterCell ()

@property (nonatomic, weak) IBOutlet UILabel *clientLabel;
@property (nonatomic, weak) IBOutlet UILabel *orderNoLabel;
@property (nonatomic, weak) IBOutlet UIImageView *statusBGImgView;
@property (nonatomic, weak) IBOutlet UILabel *statusLabel;
@property (nonatomic, weak) IBOutlet UILabel *numLabel;
@property (nonatomic, weak) IBOutlet UILabel *staffLabel;
@property (nonatomic, weak) IBOutlet UILabel *timeLabel;

@property (nonatomic, weak) IBOutlet NSLayoutConstraint *viewTop;
@property (nonatomic, weak) IBOutlet NSLayoutConstraint *statusBGImgViewW;

@end

@implementation YDDeliveryCenterCell

+ (instancetype)cellWithTableView:(UITableView *)tableView index:(NSInteger)index
{
    static NSString *reuseID = @"deliveryCenterCell";
    YDDeliveryCenterCell *cell = [tableView dequeueReusableCellWithIdentifier:reuseID];
    if (!cell) {
        cell = [[NSBundle mainBundle] loadNibNamed:@"YDDeliveryCenterCell" owner:nil options:nil][0];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    
    if (index == 0) {
        cell.viewTop.constant = 0;
        
    } else {
        cell.viewTop.constant = 10;
    }
    
    return cell;
}

+ (CGFloat)getHeightWithIndex:(NSInteger)index
{
    if (index == 0) {
        return 130;
        
    } else {
        return 140;
    }
}

- (void)setData:(YDSearcherDeliveryData *)data
{
    _data = data;
    
    NSString *clientName = @"临时客户";
    if (data.clientName.length) {
        clientName = data.clientName;
    }
    self.clientLabel.text = clientName;
    self.orderNoLabel.text = data.orderNo;
    
    NSString *bgImgName;
    NSString *statusName;
    UIColor *statusColor;
    switch (data.status) {
        // 待发货
        case 10:
        {
            bgImgName = @"发货中心待发货";
            statusName = @"待发货";
            statusColor = ColorFromRGB(0xFF8A30);
            self.statusBGImgViewW.constant = 46;
        }
            break;
        // 已发货
        case 20:
        {
            bgImgName = @"发货中心已发货";
            statusName = @"已发货";
            statusColor = ColorFromRGB(0x6195FE);
            self.statusBGImgViewW.constant = 46;
        }
            break;
        // 驳回重配
        case 30:
        {
            bgImgName = @"发货中心驳回重配";
            statusName = @"驳回重配";
            statusColor = ColorFromRGB(0xFF5630);
            self.statusBGImgViewW.constant = 55;
        }
            break;
            
        default:
            break;
    }
    [self.statusBGImgView setImage:[UIImage imageNamed:bgImgName]];
    self.statusLabel.text = statusName;
    self.statusLabel.textColor = statusColor;
    
    self.numLabel.text = [NSString stringWithFormat:@"%ld件", data.deliveryNum];
    if (data.deliveryUserName.length) {
        self.staffLabel.text = [NSString stringWithFormat:@"开单人-%@", data.deliveryUserName];
        
    } else {
        self.staffLabel.text = nil;
    }
    self.timeLabel.text = data.orderTime;
}

- (void)awakeFromNib {
    [super awakeFromNib];
    
    self.numLabel.font = FONT_NUMBER(15);
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end

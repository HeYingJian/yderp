//
//  YDDeliveryCenterVC.m
//  YDERP
//
//  Created by 何英健 on 2021/10/19.
//

#import "YDDeliveryCenterVC.h"
#import "YDSupplySelectView.h"
#import "YDSearcher.h"
#import "YDSupplyCenterCell.h"
#import "YDDeliveryCenterCell.h"
#import "YDDeliveryDetailVC.h"

@interface YDDeliveryCenterVC () <UITextFieldDelegate, UITableViewDelegate, UITableViewDataSource>

// 搜索器
@property (nonatomic, strong) YDSearcher *searcher;
// 数据源
@property (nonatomic, strong) NSMutableArray<NSMutableArray *> *dataArray;

@property (nonatomic, weak) IBOutlet UITextField *searchTextField;

@property (nonatomic, weak) IBOutlet UIView *tagBGView;
@property (nonatomic, strong) YDSupplySelectView *tagView;

@property (nonatomic, weak) IBOutlet UITableView *tableView;

@end

@implementation YDDeliveryCenterVC

- (YDSearcher *)searcher
{
    if (!_searcher) {
        _searcher = [YDSearcher createWithType:YDSearchTypeDelivery];
        _searcher.pageSize = 10;
    }
    
    return _searcher;
}

- (NSMutableArray<NSMutableArray *> *)dataArray
{
    if (!_dataArray) {
        _dataArray = [NSMutableArray array];
    }
    
    return _dataArray;
}

#pragma mark - 页面加载

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self initUI];
    
    [self addNotification];
}

- (void)initUI
{
    self.title = @"发货中心";
    [self.navigationItem addDefaultBackButton:self];
    
    [self setupTagView];
    [self setupTableView];
}

- (void)setupTableView
{
    self.tableView.hidden = YES;
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    self.tableView.showsVerticalScrollIndicator = NO;
    self.tableView.showsHorizontalScrollIndicator = NO;
    
    if (@available(iOS 11.0, *)) {
        self.tableView.contentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentNever;
        
    } else {
        self.automaticallyAdjustsScrollViewInsets = NO;
    }

    self.tableView.mj_header = [YDRefreshHeader headerWithRefreshingTarget:self refreshingAction:@selector(pullingRefresh)];
    
    self.tableView.mj_footer = [YDRefreshFooter footerWithRefreshingTarget:self refreshingAction:@selector(loadMore)];
}

- (void)addNotification
{
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(receiveNotiToRefreshData) name:REFRESH_DELIVERY_LIST object:nil];
}

- (void)receiveNotiToRefreshData
{
    [self refreshData:NO];
}

- (IBAction)tapSearchBtn:(id)sender
{
    [self refreshData:YES];
}

- (void)dismissKeybord
{
    [self.searchTextField resignFirstResponder];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [self.navigationController setNavigationBarHidden:NO animated:YES];
}

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

#pragma mark - 数据请求

- (void)pullingRefresh
{
    [self refreshData:NO];
}

- (void)refreshData:(BOOL)showLoading
{
    [self dismissKeybord];
    
    if (showLoading) {
        [YDLoadingView showToSuperview:self.view];
    }
    
    YDWeakSelf
    if (!self.searcher.completion) {
        self.searcher.completion = ^(BaseVMRefreshType type) {
            weakSelf.tableView.hidden = NO;
            [YDLoadingView hideToSuperview:weakSelf.view];
            [weakSelf.tableView.mj_header endRefreshing];

            if (type == BaseVMRefreshTypeNoMore) {
                [weakSelf.tableView.mj_footer endRefreshingWithNoMoreData];
                
            } else {
                [weakSelf.tableView.mj_footer endRefreshing];
            }

            [weakSelf setupDataArray];
            [weakSelf.tableView reloadData];
        };
    }
    
    if (!self.searcher.faildBlock) {
        self.searcher.faildBlock = ^(NSInteger code, NSString *error) {
            weakSelf.tableView.hidden = NO;
            [YDLoadingView hideToSuperview:weakSelf.view];
            [weakSelf.tableView.mj_header endRefreshing];
            [weakSelf.tableView.mj_footer endRefreshing];
            [MBProgressHUD showToastMessage:error];
        };
    }
    
    [self setSearchCondiction];
    [self.searcher refreshData];
}

- (void)setupDataArray
{
    self.dataArray = nil;
    for (YDSearcherDeliveryData *newData in self.searcher.deliveryDataArray) {
        NSMutableArray *oldArray = [self.dataArray lastObject];
        YDSearcherDeliveryData *oldData = [oldArray lastObject];
        BOOL isSameDay = [self isSameDay:oldData.orderTime date2:newData.orderTime];
        if (isSameDay) {
            [oldArray addObject:newData];
            
        } else {
            NSMutableArray *newArray = [NSMutableArray array];
            [newArray addObject:newData];
            [self.dataArray addObject:newArray];
        }
    }
}

- (BOOL)isSameDay:(NSString *)date1 date2:(NSString *)date2
{
    NSArray *dateArray1 = [date1 componentsSeparatedByString:@" "];
    NSArray *dateArray2 = [date2 componentsSeparatedByString:@" "];
    if (dateArray1.count > 1 && dateArray2.count > 1) {
        NSString *str1 = dateArray1[0];
        NSString *str2 = dateArray2[0];
        if ([str1 isEqualToString:str2]) {
            return YES;
        }
    }
    
    return NO;
}

- (void)loadMore
{
    [self.searcher loadMoreData];
}

#pragma mark - 搜索条件

- (void)setupTagView
{
    NSMutableArray *modelArray = [NSMutableArray array];
    
    for (int i = 0; i < 1; i++) {
        YDSelectViewModel *model = [[YDSelectViewModel alloc] init];
        
        switch (i) {
            case 0:
            {
                model.title = @"日期";
                model.type = 0;
                [model createImgViewModelWithSelectedImg:@"筛选下箭头选中" unselectedImg:@"筛选下箭头未选中"];
                [model createImgViewModelWithSelectedImg:@"筛选上箭头选中" unselectedImg:@"筛选上箭头未选中"];
            }
                break;

            default:
                break;
        }
        [modelArray addObject:model];
    }
    
    YDWeakSelf
    self.tagView = [YDSupplySelectView createViewWithType:YDSelectViewTypeDelivery modelArray:modelArray showFilterBlock:^{
        [weakSelf dismissKeybord];
        
    } refreshBlock:^{
        [weakSelf refreshData:YES];
    }];
    [self.tagBGView addSubview:self.tagView];
}

// 设置搜索模型，设置对应搜索条件
- (void)setSearchCondiction
{
    self.searcher.keyword = self.searchTextField.text;
    
    for (int i = 0; i < self.tagView.modelArray.count; i++) {
        YDSelectViewModel *model = self.tagView.modelArray[i];
        
        if (model.isSelected) {
            switch (i) {
                case 0:
                {
                    if (model.type == 0) {
                        self.searcher.sortType = 11;
                        
                    } else {
                        self.searcher.sortType = 10;
                    }
                }
                    break;
                    
                default:
                    break;
            }
        }
    }
    
    if (self.tagView) {
        self.searcher.createStartTime = self.tagView.selectedResultStartTime;
        self.searcher.createEndTime = self.tagView.selectedResultEndTime;
        self.searcher.status = self.tagView.selectedResultStatus;
    }
}

#pragma mark - UITableView代理

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    NSInteger count = self.dataArray.count;
    if (count) {
        return count;
        
    } else {
        return 1;
    }
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    NSInteger count = self.dataArray.count;
    if (count) {
        return self.dataArray[section].count;
        
    } else {
        return 1;
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (self.dataArray.count) {
        return [YDDeliveryCenterCell getHeightWithIndex:indexPath.row];
        
    } else {
        return [YDNullCell getHeightWithTableView:tableView];
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (self.dataArray.count) {
        YDDeliveryCenterCell *cell = [YDDeliveryCenterCell cellWithTableView:tableView index:indexPath.row];
        cell.data = self.dataArray[indexPath.section][indexPath.row];
        
        return cell;
        
    } else {
        YDNullCell *cell = [YDNullCell cellWithTableView:tableView];
        cell.content = @"暂无数据";
        return cell;
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    if (self.dataArray.count) {
        return 40;
    }
    return 0.001;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UIView *view = [[UIView alloc] init];
    
    if (self.dataArray.count) {
        YDSearcherDeliveryData *data = self.dataArray[section][0];
        UILabel *label = [[UILabel alloc] init];
        label.font = FONT_NUMBER(13);
        label.textColor = ColorFromRGB(0x999999);
        NSArray *dateArray = [data.orderTime componentsSeparatedByString:@" "];
        if (dateArray.count) {
            label.text = dateArray[0];
        }
        [label sizeToFit];
        [label setOrigin:CGPointMake(17, (40 - label.height) / 2)];
        [view addSubview:label];
    }
    
    return view;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    return 0.001;
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section
{
    return [[UIView alloc] init];
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (self.dataArray.count) {
        YDDeliveryDetailVC *vc = [[YDDeliveryDetailVC alloc] init];
        YDSearcherDeliveryData *data = self.dataArray[indexPath.section][indexPath.row];
        vc.ID = data.ID;
        [self.navigationController pushViewController:vc animated:YES];
    }
}

#pragma mark - UITextFieldDelegate

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    
    [self refreshData:YES];

    return YES;
}

@end

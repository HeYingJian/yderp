//
//  YDHomeCell.m
//  YDERP
//
//  Created by 何英健 on 2021/7/21.
//

#import "YDHomeCell.h"
#import "YDSearchVC.h"
#import "YDCollectMoneySettingVC.h"
#import "YDRestingOrderVC.h"
#import "YDOrderListVC.h"
#import "YDSupplyCenterVC.h"
#import "YDDeliveryCenterVC.h"

@interface YDHomeCell ()

@property (nonatomic, weak) IBOutlet NSLayoutConstraint *BGViewH;

@property (nonatomic, weak) IBOutlet UIImageView *avatarImgView;
@property (nonatomic, weak) IBOutlet UILabel *nameLabel;
@property (nonatomic, weak) IBOutlet UIView *searchView;
@property (nonatomic, weak) IBOutlet UILabel *titleLabel;
@property (nonatomic, weak) IBOutlet UILabel *moneyLabel;

@property (nonatomic, weak) IBOutlet UILabel *todayTitleLabel;
@property (nonatomic, weak) IBOutlet UILabel *todayMarkLabel;
@property (nonatomic, weak) IBOutlet UILabel *oweQuantityLabel;
@property (nonatomic, weak) IBOutlet UILabel *oweMoneyLabel;

@property (nonatomic, weak) IBOutlet UIView *orderManageView;
@property (nonatomic, weak) IBOutlet UIView *restingOrderView;
@property (nonatomic, weak) IBOutlet UIView *productSettingView;
@property (nonatomic, weak) IBOutlet UIView *stockManageView;
@property (nonatomic, weak) IBOutlet UIView *supplyCenterView;
@property (nonatomic, weak) IBOutlet UIView *despatchingCenterView;
@property (nonatomic, weak) IBOutlet UIView *collectionSettingView;

@property (nonatomic, weak) IBOutlet UIButton *restingRedBtn;
@property (nonatomic, weak) IBOutlet UIButton *supplyRedBtn;

@property (nonatomic, weak) IBOutlet NSLayoutConstraint *nameLabelW;
@property (nonatomic, weak) IBOutlet NSLayoutConstraint *searchViewLeading;
@property (nonatomic, weak) IBOutlet NSLayoutConstraint *restingRedBtnW;
@property (nonatomic, weak) IBOutlet NSLayoutConstraint *supplyRedBtnW;

@end

@implementation YDHomeCell

+ (instancetype)cellWithTableView:(UITableView *)tableView
{
    static NSString *reuseID = @"homeCell";
    YDHomeCell *cell = [tableView dequeueReusableCellWithIdentifier:reuseID];
    if (!cell) {
        cell = [[NSBundle mainBundle] loadNibNamed:@"YDHomeCell" owner:nil options:nil][0];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    
    [cell setupData];
    
    return cell;
}

+ (CGFloat)getHeight
{
    return 600;
}

- (void)awakeFromNib
{
    [super awakeFromNib];
    
    if (IS_PhoneXAll) {
        self.BGViewH.constant = 340;
    }
    
    self.moneyLabel.font = FONT_NUMBER(31);
    self.todayMarkLabel.font = FONT_NUMBER(21);
    self.oweQuantityLabel.font = FONT_NUMBER(21);
    self.oweMoneyLabel.font = FONT_NUMBER(21);
    self.restingRedBtn.titleLabel.font = FONT_NUMBER(10);
    self.supplyRedBtn.titleLabel.font = FONT_NUMBER(10);
    
    [self addGesture];
}

- (void)addGesture
{
    UITapGestureRecognizer *tap1 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(showSearch)];
    [self.searchView addGestureRecognizer:tap1];
    
    UITapGestureRecognizer *tap2 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(showOrderManage)];
    [self.orderManageView addGestureRecognizer:tap2];
    
    UITapGestureRecognizer *tap3 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(showRestingOrder)];
    [self.restingOrderView addGestureRecognizer:tap3];
    
    UITapGestureRecognizer *tap4 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(showProductSetting)];
    [self.productSettingView addGestureRecognizer:tap4];
    
    UITapGestureRecognizer *tap5 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(showStockManage)];
    [self.stockManageView addGestureRecognizer:tap5];
    
    UITapGestureRecognizer *tap6 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(showSupplyCenter)];
    [self.supplyCenterView addGestureRecognizer:tap6];
    
    UITapGestureRecognizer *tap7 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(showDespatchingCenter)];
    [self.despatchingCenterView addGestureRecognizer:tap7];
    
    UITapGestureRecognizer *tap8 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(showCollectionSetting)];
    [self.collectionSettingView addGestureRecognizer:tap8];
}

- (void)setupData
{
    [self.avatarImgView appleSetImageWithUrl:UserModel.headImg SprcifyOssImageStyle:@"head_pic" HasHD:NO placeholderImage:@"默认头像" completion:nil];
    self.nameLabel.text = UserModel.shopName;
    CGFloat nameW = [UserModel.shopName sizeWith:CGSizeMake(MAXFLOAT, 16) fontSize:13.0f].width + 1;
    CGFloat maxW = SCREEN_WIDTH - 300;
    if (nameW > maxW) {
        nameW = maxW + 36;
        self.searchViewLeading.constant = 10;
        
    } else {
        self.searchViewLeading.constant = 46;
    }
    self.nameLabelW.constant = nameW;
}

- (void)setData:(YDHomeData *)data
{
    _data = data;
    
    self.moneyLabel.text = [NSString applePrefixPriceWith:data.saleAmount];
    self.todayMarkLabel.text = [NSString stringWithFormat:@"%ld", (long)data.orderNum];
    self.oweQuantityLabel.text = [NSString stringWithFormat:@"%ld", (long)data.dueNum];
    self.oweMoneyLabel.text = [NSString stringWithFormat:@"%.2f", data.dueAmount];
    
    if (data.hangupOrderNum > 0) {
        NSString *str ;
        if (data.hangupOrderNum > 99) {
            str = @"99+";
            
        } else {
            str = [NSString stringWithFormat:@"%ld", (long)data.hangupOrderNum];
        }

        self.restingRedBtn.hidden = NO;
        [self.restingRedBtn setTitle:str forState:UIControlStateNormal];
        self.restingRedBtnW.constant = [str sizeWith:CGSizeMake(MAXFLOAT, 12) font:FONT_NUMBER(10)].width + 9;
        
    } else {
        self.restingRedBtn.hidden = YES;
    }
    
    if (data.allocOrderNum > 0) {
        NSString *str ;
        if (data.allocOrderNum > 99) {
            str = @"99+";
            
        } else {
            str = [NSString stringWithFormat:@"%ld", (long)data.allocOrderNum];
        }

        self.supplyRedBtn.hidden = NO;
        [self.supplyRedBtn setTitle:str forState:UIControlStateNormal];
        self.supplyRedBtnW.constant = [str sizeWith:CGSizeMake(MAXFLOAT, 12) font:FONT_NUMBER(10)].width + 9;
        
    } else {
        self.supplyRedBtn.hidden = YES;
    }
}

- (void)setType:(YDHomeType)type
{
    _type = type;
    
    if (type == YDHomeTypeToday) {
        self.titleLabel.text = @"今日销售额 (元)";
        self.todayTitleLabel.text = @"今日开单 (笔)";
        
    } else {
        self.titleLabel.text = @"本月销售额 (元)";
        self.todayTitleLabel.text = @"本月开单 (笔)";
    }
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

#pragma mark - 页面跳转

// 进入搜索页
- (void)showSearch
{
    YDSearchVC *vc = [[YDSearchVC alloc] init];
    vc.hidesBottomBarWhenPushed = YES;
    [[UINavigationController rootNavigationController] pushViewController:vc animated:NO];
}

- (IBAction)tapChangeStatusBtn:(id)sender
{
    if (self.changeStatusBlock) {
        self.changeStatusBlock();
    }
}

// 进入客服页
- (IBAction)tapCustomerBtn:(id)sender
{
    [YDFunction showCustomer];
}

// 进入订单管理
- (void)showOrderManage
{
    if ([YDLimit limitOrder]) {
        YDOrderListVC *vc = [[YDOrderListVC alloc] init];
        vc.hidesBottomBarWhenPushed = YES;
        [[UINavigationController rootNavigationController] pushViewController:vc animated:YES];
    }
}

// 进入挂单
- (void)showRestingOrder
{
    if ([YDLimit limitOrder]) {
        YDRestingOrderVC *vc = [[YDRestingOrderVC alloc] init];
        vc.hidesBottomBarWhenPushed = YES;
        [[UINavigationController rootNavigationController] pushViewController:vc animated:YES];
    }
}

// 进入商品设置
- (void)showProductSetting
{
    if ([YDLimit limitProduct]) {
        [YDWebViewVC showWebViewWithUrl:WEBURL_PRODUCT_SETTING isHideNav:YES];
    }
}

// 进入库存管理
- (void)showStockManage
{
    if ([YDLimit limitStock]) {
        [YDWebViewVC showWebViewWithUrl:WEBURL_STOCK_MANAGE isHideNav:YES];
    }
}

// 进入配货中心
- (void)showSupplyCenter
{
    if ([YDLimit limitAllocation]) {
//        [YDWebViewVC showWebViewWithUrl:WEBURL_SUPPLY_CENTER isHideNav:YES];
        
        YDSupplyCenterVC *vc = [[YDSupplyCenterVC alloc] init];
        vc.hidesBottomBarWhenPushed = YES;
        [[UINavigationController rootNavigationController] pushViewController:vc animated:YES];
    }
}

// 进入发货中心
- (void)showDespatchingCenter
{
    if ([YDLimit limitDelivery]) {
//        [YDWebViewVC showWebViewWithUrl:WEBURL_DESPATCHING_CENTER isHideNav:YES];
        
        YDDeliveryCenterVC *vc = [[YDDeliveryCenterVC alloc] init];
        vc.hidesBottomBarWhenPushed = YES;
        [[UINavigationController rootNavigationController] pushViewController:vc animated:YES];
    }
}

// 进入收款设置
- (void)showCollectionSetting
{
    if ([UserModel.roleCode isEqualToString:@"BOSS"]) {
        YDCollectMoneySettingVC *vc = [[YDCollectMoneySettingVC alloc] init];
        vc.hidesBottomBarWhenPushed = YES;
        [[UINavigationController rootNavigationController] pushViewController:vc animated:YES];
        
    } else {
        [MBProgressHUD showToastMessage:@"没有收款设置的权限"];
    }
}

@end

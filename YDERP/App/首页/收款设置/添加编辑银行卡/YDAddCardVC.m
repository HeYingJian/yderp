//
//  YDAddCardVC.m
//  YDERP
//
//  Created by 何英健 on 2021/8/9.
//

#import "YDAddCardVC.h"
#import "YDCollectMoneyVM.h"

@interface YDAddCardVC () <UITextFieldDelegate>

@property (nonatomic, weak) IBOutlet UILabel *titleLabel;

@property (nonatomic, weak) IBOutlet UITextField *nameTextField;
@property (nonatomic, weak) IBOutlet UITextField *cardNumTextField;
@property (nonatomic, weak) IBOutlet UITextField *bankTextField;

@property (nonatomic, weak) IBOutlet UIButton *nameClearBtn;
@property (nonatomic, weak) IBOutlet UIButton *cardNumClearBtn;
@property (nonatomic, weak) IBOutlet UIButton *bankClearBtn;

@end

@implementation YDAddCardVC

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self initUI];
}

- (void)initUI
{
    self.title = @"绑定银行卡";
    [self.navigationItem addDefaultBackButton:self];
    
    self.cardNumTextField.font = FONT_NUMBER(17);
    
    NSMutableAttributedString *str1 = [[NSMutableAttributedString alloc] initWithString:@"请输入银行预留姓名" attributes:
         @{NSForegroundColorAttributeName:ColorFromRGB(0xCCCCCC),
           NSFontAttributeName:[UIFont systemFontOfSize:16]}
         ];
    self.nameTextField.attributedPlaceholder = str1;
    
    NSMutableAttributedString *str2 = [[NSMutableAttributedString alloc] initWithString:@"请输入店主本人银行卡号" attributes:
         @{NSForegroundColorAttributeName:ColorFromRGB(0xCCCCCC),
           NSFontAttributeName:[UIFont systemFontOfSize:16]}
         ];
    self.cardNumTextField.attributedPlaceholder = str2;
    
    NSMutableAttributedString *str3 = [[NSMutableAttributedString alloc] initWithString:@"请输入开户行" attributes:
         @{NSForegroundColorAttributeName:ColorFromRGB(0xCCCCCC),
           NSFontAttributeName:[UIFont systemFontOfSize:16]}
         ];
    self.bankTextField.attributedPlaceholder = str3;
    
    [self.cardNumTextField addToolSenderWithBlock:nil];
    
    if (self.data) {
        self.titleLabel.text = @"编辑银行卡";
        
        self.nameTextField.text = self.data.cardName;
        self.cardNumTextField.text = [self addSpaceToString:self.data.cardNo];
        self.bankTextField.text = self.data.cardBank;
    }
}

- (void)dismissKeybord
{
    [self.nameTextField resignFirstResponder];
    [self.cardNumTextField resignFirstResponder];
    [self.bankTextField resignFirstResponder];
}

- (IBAction)tapConfirmBtn:(id)sender
{
    [self dismissKeybord];
    
    if (!self.nameTextField.text.length) {
        [MBProgressHUD showToastMessage:@"请填写持卡人再提交"];
        return;
    }
    
    if (!self.cardNumTextField.text.length) {
        [MBProgressHUD showToastMessage:@"请填写银行卡号再提交"];
        return;
    }
    
    if (!self.bankTextField.text.length) {
        [MBProgressHUD showToastMessage:@"请填写开户行再提交"];
        return;
    }
    
    YDWeakSelf
    if (self.data) {
        // 编辑
        [YDCollectMoneyVM editCardPaymentWithID:self.data.ID name:self.nameTextField.text cardNum:self.cardNumTextField.text bank:self.bankTextField.text completion:^{
            [weakSelf changeCardSucceed];
        }];
        
    } else {
        // 新增
        [YDCollectMoneyVM addCardPaymentWithName:self.nameTextField.text cardNum:self.cardNumTextField.text bank:self.bankTextField.text completion:^{
            [weakSelf changeCardSucceed];
        }];
    }
}

- (void)changeCardSucceed
{
    [MBProgressHUD showToastMessage:@"设置成功"];
    
    if (self.addNewCardSucceedBlock) {
        self.addNewCardSucceedBlock();
    }
    
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)tapNameClearBtn:(id)sender
{
    self.nameTextField.text = nil;
    [self.nameTextField becomeFirstResponder];
    self.nameClearBtn.hidden = YES;
}

- (IBAction)tapCardNumClearBtn:(id)sender
{
    self.cardNumTextField.text = nil;
    [self.cardNumTextField becomeFirstResponder];
    self.cardNumClearBtn.hidden = YES;
}

- (IBAction)tapBankClearBtn:(id)sender
{
    self.bankTextField.text = nil;
    [self.bankTextField becomeFirstResponder];
    self.bankClearBtn.hidden = YES;
}

#pragma mark - UITextFieldDelegate

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];

    return YES;
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    NSInteger length = textField.text.length + string.length - range.length;
    
    if (textField == self.nameTextField) {
        self.nameClearBtn.hidden = length? NO : YES;
        
    } else if (textField == self.bankTextField) {
        self.bankClearBtn.hidden = length? NO : YES;
        
    } else if (textField == self.cardNumTextField) {
        // 4位分隔银行卡卡号
        NSString *text = [textField text];
        
        NSCharacterSet *characterSet = [NSCharacterSet characterSetWithCharactersInString:@"0123456789\b"];
        string = [string stringByReplacingOccurrencesOfString:@" " withString:@""];
        if ([string rangeOfCharacterFromSet:[characterSet invertedSet]].location != NSNotFound) {
            return NO;
        }
        
        if (string.length == 0 && [[textField.text substringWithRange:range] isEqualToString:@" "]) {
            text = [text stringByReplacingCharactersInRange:NSMakeRange(range.location - 1, range.length + 1) withString:string];
            [textField setSelectedRange:NSMakeRange([textField selectedRange].location - 1, 0)];
            
        } else {
            text = [text stringByReplacingCharactersInRange:range withString:string];
        }
        text = [text stringByReplacingOccurrencesOfString:@" " withString:@""];

        NSString *newString = [self addSpaceToString:text];
        newString = [newString stringByTrimmingCharactersInSet:[characterSet invertedSet]];
        
        if ([newString stringByReplacingOccurrencesOfString:@" " withString:@""].length >= 21) {
            return NO;
        }
        
        NSRange oldRange = [textField selectedRange];
        NSInteger oldStrLength = textField.text.length;
        
        [textField setText:newString];
        
        self.cardNumClearBtn.hidden = (newString.length)? NO : YES;
        
        if (string.length == 0) {
            // 删除
            NSInteger newLocation;
            if (range.length == 1) {
                newLocation = oldRange.location - range.length;
            } else {
                newLocation = oldRange.location;
            }
            if (newLocation > newString.length) {
                newLocation = newString.length;
            }
            [textField setSelectedRange:NSMakeRange(newLocation, 0)];
            
        } else {
            // 添加
            [textField setSelectedRange:NSMakeRange(oldRange.location + newString.length - oldStrLength, 0)];
        }

        return NO;
    }
    
    return YES;
}

// 添加空格，银行卡格式
- (NSString *)addSpaceToString:(NSString *)text
{
    NSString *newString = @"";
    while (text.length > 0) {
        NSString *subString = [text substringToIndex:MIN(text.length, 4)];
        newString = [newString stringByAppendingString:subString];
        if (subString.length == 4) {
            newString = [newString stringByAppendingString:@" "];
        }
        text = [text substringFromIndex:MIN(text.length, 4)];
    }
    
    return newString;
}

@end

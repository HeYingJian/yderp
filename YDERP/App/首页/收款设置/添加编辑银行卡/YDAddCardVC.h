//
//  YDAddCardVC.h
//  YDERP
//
//  Created by 何英健 on 2021/8/9.
//

#import <UIKit/UIKit.h>
#import "YDCollectMoneyModel.h"

typedef void(^BlockAddNewCardSucceed)(void);

@interface YDAddCardVC : UIViewController

@property (nonatomic, strong) YDCollectMoneyData *data;

@property (nonatomic, copy) BlockAddNewCardSucceed addNewCardSucceedBlock;

@end

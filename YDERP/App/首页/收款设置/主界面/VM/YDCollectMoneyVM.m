//
//  YDCollectMoneyVM.m
//  YDERP
//
//  Created by 何英健 on 2021/8/10.
//

#import "YDCollectMoneyVM.h"

@implementation YDCollectMoneyVM

+ (void)getCollectMoneySettingWithCompletion:(BlockGetCollectMoneySetting)completion
{
    [YDLoadingView showToSuperview:nil];
    
    NSString *path = URL_PAYMENT_SETTING;
    NSMutableDictionary *param = [NSMutableDictionary dictionary];

    AppleServerPostCompleteBlock postHandleBlock = ^(BOOL done, id _Nullable object, NSString * _Nullable error) {
        [YDLoadingView hideToSuperview:nil];
        
        if (done) {
            YDCollectMoneyModel *resp = [YDCollectMoneyModel modelWithJSON:object];
            
            if (resp.code == 200) {
                if (completion) {
                    completion(resp.data);
                }
            
            } else {
                NSString *msg;
                if (resp.detailMessage.length) {
                    msg = resp.detailMessage;
                    
                } else if (resp.message.length) {
                    msg = resp.message;
                    
                } else {
                    msg = @"网络失败";
                }
                [MBProgressHUD showToastMessage:msg];
            }
            
        } else {
            [MBProgressHUD showToastMessage:@"网络不给力"];
        }
    };
    
    [AppleServer.shared getWithUrl:path Paramters:param Callback:postHandleBlock];
}

+ (void)addCardPaymentWithName:(NSString *)name cardNum:(NSString *)cardNum bank:(NSString *)bank completion:(BlockLoadDataDone)completion
{
    [YDCollectMoneyVM uploadPaymentInfoWithID:nil type:@"card" imgUrl:nil cardName:name cardNo:cardNum cardBank:bank completion:completion];
}

+ (void)editCardPaymentWithID:(NSInteger)ID name:(NSString *)name cardNum:(NSString *)cardNum bank:(NSString *)bank completion:(BlockLoadDataDone)completion
{
    NSString *IDStr = [NSString stringWithFormat:@"%ld", ID];
    
    [YDCollectMoneyVM uploadPaymentInfoWithID:IDStr type:@"card" imgUrl:nil cardName:name cardNo:cardNum cardBank:bank completion:completion];
}

/**
 添加新的收款
 @param ID          对应id
 @param type        收款类型，wechat-微信, alipay-支付宝， other-其它收款码, card-银行卡
 @param imgUrl      二维码图片
 @param cardName    银行卡用户名
 @param cardNo      银行卡号
 @param cardBank    银行卡开户行
 */
+ (void)uploadPaymentInfoWithID:(NSString *)ID type:(NSString *)type imgUrl:(NSString *)imgUrl cardName:(NSString *)cardName cardNo:(NSString *)cardNo cardBank:(NSString *)cardBank completion:(BlockLoadDataDone)completion
{
    [YDLoadingView showToSuperview:nil];
    
    NSString *path = URL_PAYMENT_SAVE;
    NSMutableDictionary *param = [NSMutableDictionary dictionary];
    [param setObject:type forKey:@"type"];
    if (ID.length) {
        [param setObject:ID forKey:@"id"];
    }
    if (imgUrl) {
        [param setObject:imgUrl forKey:@"imgUrl"];
    }
    if (cardName.length) {
        [param setObject:cardName forKey:@"cardName"];
    }
    if (cardNo.length) {
        NSString *realCardNo = [cardNo stringByReplacingOccurrencesOfString:@" " withString:@""];
        [param setObject:realCardNo forKey:@"cardNo"];
    }
    if (cardBank.length) {
        [param setObject:cardBank forKey:@"cardBank"];
    }

    AppleServerPostCompleteBlock postHandleBlock = ^(BOOL done, id _Nullable object, NSString * _Nullable error) {
        [YDLoadingView hideToSuperview:nil];
        
        if (done) {
            YDBaseModel *resp = [YDBaseModel modelWithJSON:object];
            
            if (resp.code == 200) {
                if (completion) {
                    completion();
                }
            
            } else {
                NSString *msg;
                if (resp.detailMessage.length) {
                    msg = resp.detailMessage;
                    
                } else if (resp.message.length) {
                    msg = resp.message;
                    
                } else {
                    msg = @"网络失败";
                }
                [MBProgressHUD showToastMessage:msg];
            }
            
        } else {
            [MBProgressHUD showToastMessage:@"网络不给力"];
        }
    };
    
    //使用AppleServer发送POST请求
    [AppleServer.shared postWithPath:path Paramters:param CompleteBlock:postHandleBlock];
}

+ (void)deleteCardPaymentWithID:(NSInteger)ID completion:(BlockLoadDataDone)completion
{
    [YDLoadingView showToSuperview:nil];
    
    NSString *path = URL_PAYMENT_DEL;
    path = [NSString stringWithFormat:@"%@/%ld", path, ID];
    NSMutableDictionary *param = [NSMutableDictionary dictionary];

    AppleServerPostCompleteBlock postHandleBlock = ^(BOOL done, id _Nullable object, NSString * _Nullable error) {
        [YDLoadingView hideToSuperview:nil];
        
        if (done) {
            YDBaseModel *resp = [YDBaseModel modelWithJSON:object];
            
            if (resp.code == 200) {
                if (completion) {
                    completion();
                }
            
            } else {
                NSString *msg;
                if (resp.detailMessage.length) {
                    msg = resp.detailMessage;
                    
                } else if (resp.message.length) {
                    msg = resp.message;
                    
                } else {
                    msg = @"网络失败";
                }
                [MBProgressHUD showToastMessage:msg];
            }
            
        } else {
            [MBProgressHUD showToastMessage:@"网络不给力"];
        }
    };
    
    //使用AppleServer发送POST请求
    [AppleServer.shared postWithPath:path Paramters:param CompleteBlock:postHandleBlock];
}

+ (void)editQRCodePaymentWithID:(NSInteger)ID type:(NSString *)type codeImgUrl:(NSString *)codeImgUrl completion:(BlockLoadDataDone)completion
{
    NSString *IDStr = [NSString stringWithFormat:@"%ld", ID];
    
    [YDCollectMoneyVM uploadPaymentInfoWithID:IDStr type:type imgUrl:codeImgUrl cardName:nil cardNo:nil cardBank:nil completion:completion];
}

@end

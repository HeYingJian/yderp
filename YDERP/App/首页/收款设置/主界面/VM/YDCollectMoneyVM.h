//
//  YDCollectMoneyVM.h
//  YDERP
//
//  Created by 何英健 on 2021/8/10.
//

#import "YDBaseVM.h"
#import "YDCollectMoneyModel.h"

typedef void(^BlockGetCollectMoneySetting)(NSMutableArray<YDCollectMoneyData *> *data);

@interface YDCollectMoneyVM : YDBaseVM

/**
 请求收款设置数据
 */
+ (void)getCollectMoneySettingWithCompletion:(BlockGetCollectMoneySetting)completion;

/**
 添加新的银行卡收款
 @param name     姓名
 @param cardNum  卡号
 @param bank     开户行
 */
+ (void)addCardPaymentWithName:(NSString *)name cardNum:(NSString *)cardNum bank:(NSString *)bank completion:(BlockLoadDataDone)completion;

/**
 编辑银行卡收款
 @param ID  对应id
 */
+ (void)editCardPaymentWithID:(NSInteger)ID name:(NSString *)name cardNum:(NSString *)cardNum bank:(NSString *)bank completion:(BlockLoadDataDone)completion;

/**
 删除银行卡收款
 @param ID  对应id
 */
+ (void)deleteCardPaymentWithID:(NSInteger)ID completion:(BlockLoadDataDone)completion;

/**
 编辑二维码收款
 @param ID          对应id
 @param type        收款类型，wechat-微信, alipay-支付宝， other-其它收款码
 @param codeImgUrl  二维码图片
 */
+ (void)editQRCodePaymentWithID:(NSInteger)ID type:(NSString *)type codeImgUrl:(NSString *)codeImgUrl completion:(BlockLoadDataDone)completion;

@end


//
//  YDCollectMoneyCodeCell.m
//  YDERP
//
//  Created by 何英健 on 2021/8/9.
//

#import "YDCollectMoneyCodeCell.h"

@interface YDCollectMoneyCodeCell ()

@property (nonatomic, weak) IBOutlet UILabel *titleLabel;
@property (nonatomic, weak) IBOutlet UILabel *statusLabel;
@property (nonatomic, weak) IBOutlet UIView *lineView;

@end

@implementation YDCollectMoneyCodeCell

+ (instancetype)cellWithTableView:(UITableView *)tableView index:(NSInteger)index
{
    static NSString *reuseID = @"collectMoneyCodeCell";
    YDCollectMoneyCodeCell *cell = [tableView dequeueReusableCellWithIdentifier:reuseID];
    if (!cell) {
        cell = [[NSBundle mainBundle] loadNibNamed:@"YDCollectMoneyCodeCell" owner:nil options:nil][0];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    
    cell.lineView.hidden = (index == 0)? YES : NO;
    
    switch (index) {
        case 0:
            cell.titleLabel.text = @"微信";
            break;
        case 1:
            cell.titleLabel.text = @"支付宝";
            break;
        case 2:
            cell.titleLabel.text = @"收款码";
            break;
            
        default:
            break;
    }
    
    return cell;
}

+ (CGFloat)getHeight
{
    return 65;
}

- (void)setData:(YDCollectMoneyData *)data
{
    _data = data;
    
    if (data.imgUrl.length) {
        self.statusLabel.text = @"已设置";
        self.statusLabel.textColor = ColorFromRGB(0x555555);
        
    } else {
        self.statusLabel.text = @"未设置";
        self.statusLabel.textColor = ColorFromRGB(0xCCCCCC);
    }
}

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end

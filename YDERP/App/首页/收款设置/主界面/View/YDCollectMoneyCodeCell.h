//
//  YDCollectMoneyCodeCell.h
//  YDERP
//
//  Created by 何英健 on 2021/8/9.
//

#import <UIKit/UIKit.h>
#import "YDCollectMoneyModel.h"

@interface YDCollectMoneyCodeCell : UITableViewCell

+ (instancetype)cellWithTableView:(UITableView *)tableView index:(NSInteger)index;

+ (CGFloat)getHeight;

@property (nonatomic, strong) YDCollectMoneyData *data;

@end

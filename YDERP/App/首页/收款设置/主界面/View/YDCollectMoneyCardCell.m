//
//  YDCollectMoneyCardCell.m
//  YDERP
//
//  Created by 何英健 on 2021/8/9.
//

#import "YDCollectMoneyCardCell.h"
#import "YDCollectMoneyVM.h"
#import "YDAddCardVC.h"

@interface YDCollectMoneyCardCell ()

@property (nonatomic, weak) IBOutlet UILabel *bankLabel;
@property (nonatomic, weak) IBOutlet UILabel *cardNoLabel;

@end

@implementation YDCollectMoneyCardCell

+ (instancetype)cellWithTableView:(UITableView *)tableView
{
    static NSString *reuseID = @"collectMoneyCardCell";
    YDCollectMoneyCardCell *cell = [tableView dequeueReusableCellWithIdentifier:reuseID];
    if (!cell) {
        cell = [[NSBundle mainBundle] loadNibNamed:@"YDCollectMoneyCardCell" owner:nil options:nil][0];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    
    return cell;
}

+ (CGFloat)getHeight
{
    return 115;
}

- (void)setData:(YDCollectMoneyData *)data
{
    _data = data;
    
    self.bankLabel.text = data.cardBank;
    
    NSString *realCardNo = [data.cardNo stringByReplacingOccurrencesOfString:@" " withString:@""];
    NSInteger lastNum = realCardNo.length%4;
    NSInteger count = realCardNo.length/4;
    if (lastNum == 0) {
        count--;
    }
    NSString *lastStr = [realCardNo substringFromIndex:count * 4];
    
    NSString *text = @"";
    for (int i = 0; i < count; i++) {
        if (text.length) {
            text = [NSString stringWithFormat:@"%@     ****", text];
            
        } else {
            text = @"****";
        }
    }
    
    if (text.length) {
        text = [NSString stringWithFormat:@"%@     %@", text, lastStr];
        
    } else {
        text = lastStr;
    }
    
    self.cardNoLabel.text = text;
}

- (IBAction)tapDeleteBtn:(id)sender
{
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"收款账户将被删除哦！" message:nil preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *confirm = [UIAlertAction actionWithTitle:@"确定" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        YDWeakSelf
        [YDCollectMoneyVM deleteCardPaymentWithID:self.data.ID completion:^{
            if (weakSelf.changeCardBlock) {
                weakSelf.changeCardBlock();
            }
        }];
    }];
    UIAlertAction* cancel = [UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleCancel handler:nil];

    [alert addAction:cancel];
    [alert addAction:confirm];

    [[UINavigationController rootNavigationController] presentViewController:alert animated:YES completion:nil];
}

- (IBAction)tapEditBtn:(id)sender
{
    YDAddCardVC *vc = [[YDAddCardVC alloc] init];
    vc.data = self.data;
    [[UINavigationController rootNavigationController] pushViewController:vc animated:YES];
}

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end

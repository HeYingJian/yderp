//
//  YDCollectMoneyCardCell.h
//  YDERP
//
//  Created by 何英健 on 2021/8/9.
//

#import <UIKit/UIKit.h>
#import "YDCollectMoneyModel.h"

typedef void(^BlockChangeCard)(void);

@interface YDCollectMoneyCardCell : UITableViewCell

+ (instancetype)cellWithTableView:(UITableView *)tableView;

+ (CGFloat)getHeight;

@property (nonatomic, strong) YDCollectMoneyData *data;

@property (nonatomic, copy) BlockChangeCard changeCardBlock;

@end

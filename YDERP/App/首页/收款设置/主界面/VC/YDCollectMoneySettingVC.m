//
//  YDCollectMoneySettingVC.m
//  YDERP
//
//  Created by 何英健 on 2021/8/9.
//

#import "YDCollectMoneySettingVC.h"
#import "YDCollectMoneyCodeCell.h"
#import "YDCollectMoneyCardCell.h"
#import "YDCollectMoneyModel.h"
#import "YDAddCardVC.h"
#import "YDCollectMoneyVM.h"
#import "YDPaymentCodeSettingVC.h"

@interface YDCollectMoneySettingVC () <UITableViewDelegate, UITableViewDataSource>

// 微信收款
@property (nonatomic, strong) YDCollectMoneyData *wechatData;
// 支付宝收款
@property (nonatomic, strong) YDCollectMoneyData *alipayData;
// 其他收款
@property (nonatomic, strong) YDCollectMoneyData *otherData;
// 银行卡
@property (nonatomic, strong) NSMutableArray<YDCollectMoneyData *> *cardDataArray;

@property (nonatomic, weak) IBOutlet UITableView *tableView;

@end

@implementation YDCollectMoneySettingVC

- (NSMutableArray<YDCollectMoneyData *> *)cardDataArray
{
    if (!_cardDataArray) {
        _cardDataArray = [NSMutableArray array];
    }
    
    return _cardDataArray;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self initUI];
}

- (void)initUI
{
    self.title = @"收款设置管理";
    [self.navigationItem addDefaultBackButton:self];
    
    [self setupTableView];
}

- (void)setupTableView
{
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    self.tableView.showsVerticalScrollIndicator = NO;
    self.tableView.showsHorizontalScrollIndicator = NO;
    
    if (@available(iOS 11.0, *)) {
        self.tableView.contentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentNever;
        
    } else {
        self.automaticallyAdjustsScrollViewInsets = NO;
    }
}

- (void)refreshData
{
    YDWeakSelf
    [YDCollectMoneyVM getCollectMoneySettingWithCompletion:^(NSMutableArray<YDCollectMoneyData *> *data) {
        [weakSelf.cardDataArray removeAllObjects];
        
        for (YDCollectMoneyData *sub in data) {
            if ([sub.type isEqualToString:@"wechat"]) {
                weakSelf.wechatData = sub;
                
            } else if ([sub.type isEqualToString:@"alipay"]) {
                weakSelf.alipayData = sub;
            
            } else if ([sub.type isEqualToString:@"other"]) {
                weakSelf.otherData = sub;
                
            } else if ([sub.type isEqualToString:@"card"]) {
                [weakSelf.cardDataArray addObject:sub];
            }
        }
        
        [weakSelf.tableView reloadData];
    }];
}

- (IBAction)tapAddCardBtn:(id)sender
{
    YDAddCardVC *vc = [[YDAddCardVC alloc] init];
    YDWeakSelf
    vc.addNewCardSucceedBlock = ^{
        [weakSelf refreshData];
        [weakSelf.tableView scrollToTopAnimated:YES];
    };
    [self.navigationController pushViewController:vc animated:YES];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    self.navigationController.navigationBar.translucent = YES;
    [self.navigationController setNavigationBarHidden:NO animated:NO];
    
    [self refreshData];
}

#pragma mark - UITableView代理

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 2;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    switch (section) {
        case 0:
            return 3;
            
        case 1:
        {
            if (self.cardDataArray.count) {
                return self.cardDataArray.count;
                
            } else {
                return 1;
            }
        }
   
        default:
            break;
    }
    
    return 0;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    switch (indexPath.section) {
        case 0:
            return [YDCollectMoneyCodeCell getHeight];
            
        case 1:
        {
            if (self.cardDataArray.count) {
                return [YDCollectMoneyCardCell getHeight];
                
            } else {
                return 155;
            }
        }
   
        default:
            break;
    }
    
    return 0.01;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    switch (indexPath.section) {
        case 0:
        {
            YDCollectMoneyCodeCell *cell = [YDCollectMoneyCodeCell cellWithTableView:tableView index:indexPath.row];
            if (indexPath.row == 0) {
                cell.data = self.wechatData;
                
            } else if (indexPath.row == 1) {
                cell.data = self.alipayData;
                
            } else if (indexPath.row == 2) {
                cell.data = self.otherData;
            }
            
            return cell;
        }
            
        case 1:
        {
            if (self.cardDataArray.count) {
                YDCollectMoneyCardCell *cell = [YDCollectMoneyCardCell cellWithTableView:tableView];
                cell.data = self.cardDataArray[indexPath.row];
                YDWeakSelf
                cell.changeCardBlock = ^{
                    [weakSelf refreshData];
                };
                
                return cell;
                
            } else {
                YDNullCell *cell = [YDNullCell cellWithTableView:tableView];
                cell.content = @"暂无数据";
                cell.originY = 36;
                
                return cell;
            }
        }
   
        default:
            break;
    }
    
    return [[UITableViewCell alloc] init];
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 40;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UIView *view = [[UIView alloc] init];
    UILabel *label = [[UILabel alloc] init];
    label.font = [UIFont systemFontOfSize:13 weight:UIFontWeightMedium];
    label.textColor = ColorFromRGB(0x999999);
    if (section == 0) {
        label.text = @"收款码设置";
    } else {
        label.text = @"银行卡账号设置";
    }
    [label sizeToFit];
    [label setOrigin:CGPointMake(24, 13)];
    [view addSubview:label];
    
    return view;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    if (section == 1) {
        return 80;
    }
    return 0.01;
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section
{
    return [[UIView alloc] init];
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == 0) {
        YDPaymentCodeSettingVC *vc = [[YDPaymentCodeSettingVC alloc] init];
        switch (indexPath.row) {
            case 0:
            {
                vc.type = YDPaymentCodeTypeWechat;
                vc.data = self.wechatData;
            }
                break;
            case 1:
            {
                vc.type = YDPaymentCodeTypeAli;
                vc.data = self.alipayData;
            }
                break;
            default:
            {
                vc.type = YDPaymentCodeTypeOther;
                vc.data = self.otherData;
            }
                break;
        }
        YDWeakSelf
        vc.setQRCodeSucceedBlock = ^{
            [weakSelf refreshData];
        };
        [self.navigationController pushViewController:vc animated:YES]; 
    }
}

@end

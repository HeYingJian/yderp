//
//  YDCollectMoneyModel.h
//  YDERP
//
//  Created by 何英健 on 2021/8/9.
//

#import "YDBaseModel.h"

@interface YDCollectMoneyData : NSObject

@property (nonatomic, assign) NSInteger ID;
// 收款类型，wechat-微信, alipay-支付宝， other-其它收款码, card-银行卡
@property (nonatomic, copy) NSString *type;
// 图片url
@property (nonatomic, copy) NSString *imgUrl;
// 持卡人姓名
@property (nonatomic, copy) NSString *cardName;
// 卡号
@property (nonatomic, copy) NSString *cardNo;
// 卡所属银行
@property (nonatomic, copy) NSString *cardBank;

@end

@interface YDCollectMoneyModel : YDBaseModel

@property (nonatomic, strong) NSMutableArray<YDCollectMoneyData *> *data;

@end

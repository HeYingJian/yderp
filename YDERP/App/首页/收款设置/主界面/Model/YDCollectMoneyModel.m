//
//  YDCollectMoneyModel.m
//  YDERP
//
//  Created by 何英健 on 2021/8/9.
//

#import "YDCollectMoneyModel.h"

@implementation YDCollectMoneyData

+ (NSDictionary<NSString *,id> *)modelCustomPropertyMapper
{
    return @{@"ID":@"id"};
}

@end

@implementation YDCollectMoneyModel

+ (NSDictionary<NSString *,id> *)modelContainerPropertyGenericClass{
    return @{@"data" : [YDCollectMoneyData class]};
}

@end

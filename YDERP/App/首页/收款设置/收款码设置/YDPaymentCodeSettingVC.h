//
//  YDPaymentCodeSettingVC.h
//  YDERP
//
//  Created by 何英健 on 2021/8/10.
//

#import <UIKit/UIKit.h>
#import "YDCollectMoneyModel.h"

typedef enum : NSUInteger {
    YDPaymentCodeTypeWechat,
    YDPaymentCodeTypeAli,
    YDPaymentCodeTypeOther,
} YDPaymentCodeType;

typedef void(^BlockSetQRCodeSucceed)(void);

@interface YDPaymentCodeSettingVC : UIViewController

@property (nonatomic, assign) YDPaymentCodeType type;

@property (nonatomic, strong) YDCollectMoneyData *data;

// 二维码发生变化(新增/删除/置换)
@property (nonatomic, copy) BlockSetQRCodeSucceed setQRCodeSucceedBlock;

@end

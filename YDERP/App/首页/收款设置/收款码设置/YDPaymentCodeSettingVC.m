//
//  YDPaymentCodeSettingVC.m
//  YDERP
//
//  Created by 何英健 on 2021/8/10.
//

#import "YDPaymentCodeSettingVC.h"
#import "YDCollectMoneyVM.h"

@interface YDPaymentCodeSettingVC ()

@property (nonatomic, weak) IBOutlet UILabel *titleLabel;
@property (nonatomic, weak) IBOutlet UIImageView *moneyImgView;
@property (nonatomic, weak) IBOutlet UILabel *nameLabel;
@property (nonatomic, weak) IBOutlet UIButton *moreBtn;

@property (nonatomic, weak) IBOutlet UIView *noCodeBGView;
@property (nonatomic, weak) IBOutlet UIView *existCodeBGView;
@property (nonatomic, weak) IBOutlet UILabel *contentLabel;
@property (nonatomic, weak) IBOutlet UIImageView *QRCodeImgView;

@end

@implementation YDPaymentCodeSettingVC

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self initUI];
}

- (void)initUI
{
    if (self.type == YDPaymentCodeTypeWechat) {
        self.view.backgroundColor = ColorFromRGB(0x4CAD67);
        self.titleLabel.text = @"微信收款码设置";
        self.nameLabel.text = @"微信二维码收款";
        self.contentLabel.text = @"微信扫一扫二维码，向我付钱";
        
    } else if (self.type == YDPaymentCodeTypeAli) {
        self.view.backgroundColor = ColorFromRGB(0x3476FE);
        self.titleLabel.text = @"支付宝收款码设置";
        self.nameLabel.text = @"支付宝二维码收款";
        self.contentLabel.text = @"支付宝扫一扫二维码，向我付钱";
        
    } else {
        self.view.backgroundColor = ColorFromRGB(0xEAB533);
        self.titleLabel.text = @"收款码设置";
        self.nameLabel.text = @"二维码收款";
        self.contentLabel.text = @"微信扫一扫二维码，向我付钱";
    }
    
    [self refreshUI];
}

- (void)refreshUI
{
    NSString *moneyImg;
    if (self.data.imgUrl.length) {
        self.moreBtn.hidden = NO;
        self.noCodeBGView.hidden = YES;
        self.existCodeBGView.hidden = NO;
        
        [self.QRCodeImgView appleSetImageWithUrl:self.data.imgUrl SprcifyOssImageStyle:@"products_list" HasHD:NO placeholderImage:@"默认商品方形图" completion:nil];
        
        if (self.type == YDPaymentCodeTypeWechat) {
            moneyImg = @"收款码绿色钱图标";
            self.nameLabel.textColor = ColorFromRGB(0x4CAD67);
            
        } else if (self.type == YDPaymentCodeTypeAli) {
            moneyImg = @"收款码蓝色钱图标";
            self.nameLabel.textColor = ColorFromRGB(0x3476FE);
            
        } else {
            moneyImg = @"收款码黄色钱图标";
            self.nameLabel.textColor = ColorFromRGB(0xEAB533);
        }
        
    } else {
        moneyImg = @"收款码灰色钱图标";
        self.nameLabel.textColor = ColorFromRGB(0xD5D5D5);
        self.moreBtn.hidden = YES;
        self.noCodeBGView.hidden = NO;
        self.existCodeBGView.hidden = YES;
    }
    [self.moneyImgView setImage:[UIImage imageNamed:moneyImg]];
}

- (IBAction)tapBackBtn:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
    [self.navigationController setNavigationBarHidden:NO animated:YES];
}

- (IBAction)tapMoreBtn:(id)sender
{
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:nil message:nil preferredStyle:UIAlertControllerStyleActionSheet];
    
    // 需要添加此项设置，否则ipad会崩溃
    alert.popoverPresentationController.sourceView = self.view;
    alert.popoverPresentationController.sourceRect = CGRectMake(0, SCREEN_HEIGHT - 70, SCREEN_WIDTH, 70);
    
    //相机
    UIAlertAction *camera = [UIAlertAction actionWithTitle:@"更换收款码" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        [self showPhotoAlbum];
    }];
    
    //相册
    UIAlertAction* library = [UIAlertAction actionWithTitle:@"删除收款码" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        [self deleteQRCode];
    }];
    
    //取消
    UIAlertAction* cancel = [UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleCancel handler:nil];
    
    [alert addAction:camera];
    [alert addAction:library];
    [alert addAction:cancel];
    
    [[UINavigationController rootNavigationController] presentViewController:alert animated:YES completion:nil];
}

- (void)deleteQRCode
{
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"收款码将会被删掉哦！" message:nil preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *confirm = [UIAlertAction actionWithTitle:@"确定" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        YDWeakSelf
        [weakSelf uploadQRCodeImg:@""];
    }];
    UIAlertAction* cancel = [UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleCancel handler:nil];

    [alert addAction:cancel];
    [alert addAction:confirm];

    [[UINavigationController rootNavigationController] presentViewController:alert animated:YES completion:nil];
}

- (IBAction)tapAddCodeBtn:(id)sender
{
    [self showPhotoAlbum];
}

- (IBAction)tapChangeCodeBtn:(id)sender
{
    [self showPhotoAlbum];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];

    [self.navigationController setNavigationBarHidden:YES animated:NO];
}

#pragma mark - 上传图片

// 显示相册选择
- (void)showPhotoAlbum
{
    if (![YDPhotoRight checkHavePhotoLibraryRight]) {
        return;
    }
    
    YDWeakSelf
    TZImagePickerController *imagePickerVc = [[TZImagePickerController alloc] initWithMaxImagesCount:1 delegate:nil];
    imagePickerVc.allowPickingOriginalPhoto = NO;
    imagePickerVc.showSelectedIndex = YES;
    imagePickerVc.naviTitleColor = ColorFromRGB(0x0095F7);
    imagePickerVc.barItemTextColor = ColorFromRGB(0x0095F7);
    imagePickerVc.allowPickingVideo = NO;
    imagePickerVc.modalPresentationStyle = UIModalPresentationFullScreen;
    [imagePickerVc setDidFinishPickingPhotosHandle:^(NSArray<UIImage *> *photos, NSArray *assets, BOOL isSelectOriginalPhoto) {
        if (photos.count) {
            [weakSelf showCropVCWithImg:photos[0]];
        }
    }];
    [[UINavigationController rootNavigationController] presentViewController:imagePickerVc animated:YES completion:nil];
}

// 显示裁剪页面
- (void)showCropVCWithImg:(UIImage *)image
{
    EECropImgVC *vc = [[EECropImgVC alloc] init];
    vc.originImage = image;
    YDWeakSelf
    vc.cropImageSucceedBlock = ^(UIImage *image) {
        [weakSelf uploadImgArray:[NSArray arrayWithObject:image]];
    };
    [self.navigationController pushViewController:vc animated:YES];
}

// 上传图片
- (void)uploadImgArray:(NSArray<UIImage *> *)imgArray
{
    [YDLoadingView showToSuperview:self.view];
    
    YDWeakSelf
    [YDFunction uploadImageArray:imgArray type:1 group:1 groupStr:nil parameters:nil sucess:^(NSMutableArray<NSString *> *urlArray) {
        if (urlArray.count) {
            [weakSelf uploadQRCodeImg:urlArray[0]];
        }
        
    } failure:^(NSString *msg) {
        [YDLoadingView hideToSuperview:weakSelf.view];
        [MBProgressHUD showToastMessage:msg];
    }];
}

- (void)uploadQRCodeImg:(NSString *)imgUrl
{
    NSString *uploadType;
    if (self.type == YDPaymentCodeTypeWechat) {
        uploadType = @"wechat";
        
    } else if (self.type == YDPaymentCodeTypeAli) {
        uploadType = @"alipay";
        
    } else if (self.type == YDPaymentCodeTypeOther) {
        uploadType = @"other";
    }
    
    YDWeakSelf
    [YDCollectMoneyVM editQRCodePaymentWithID:self.data.ID type:uploadType codeImgUrl:imgUrl completion:^{
        [YDLoadingView hideToSuperview:weakSelf.view];
        
        weakSelf.data.imgUrl = imgUrl;
        [weakSelf refreshUI];
        
        if (weakSelf.setQRCodeSucceedBlock) {
            weakSelf.setQRCodeSucceedBlock();
        }
    }];
}

@end

//
//  YDSupplyCenterVC.m
//  YDERP
//
//  Created by 何英健 on 2021/10/11.
//

#import "YDSupplyCenterVC.h"
#import "YDSupplySelectView.h"
#import "YDSearcher.h"
#import "YDSupplyCenterCell.h"
#import "YDSupplyDetailVC.h"

@interface YDSupplyCenterVC () <UITextFieldDelegate, UITableViewDelegate, UITableViewDataSource>

// 0-未配齐 1-已配齐
@property (nonatomic, assign) NSInteger type;

// 搜索器
@property (nonatomic, strong) YDSearcher *searcher;

@property (nonatomic, weak) IBOutlet UITextField *searchTextField;

@property (nonatomic, weak) IBOutlet UIButton *unsupplyBtn;
@property (nonatomic, weak) IBOutlet UIButton *supplyBtn;
@property (nonatomic, weak) IBOutlet UIView *unsupplyTagView;
@property (nonatomic, weak) IBOutlet UIView *supplyTagView;

@property (nonatomic, weak) IBOutlet UIView *tagBGView;
@property (nonatomic, strong) YDSupplySelectView *tagView;

@property (nonatomic, weak) IBOutlet UITableView *tableView;

@end

@implementation YDSupplyCenterVC

- (YDSearcher *)searcher
{
    if (!_searcher) {
        _searcher = [YDSearcher createWithType:YDSearchTypeSupply];
        _searcher.pageSize = 10;
    }
    
    return _searcher;
}

#pragma mark - 页面加载

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self initUI];
    
    [self addNotification];
}

- (void)initUI
{
    self.title = @"配货中心";
    [self.navigationItem addDefaultBackButton:self];
    
    [self setTagType:0];
    [self setupTagView];
    [self setupTableView];
}

- (void)setupTableView
{
    self.tableView.hidden = YES;
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    self.tableView.showsVerticalScrollIndicator = NO;
    self.tableView.showsHorizontalScrollIndicator = NO;
    
    if (@available(iOS 11.0, *)) {
        self.tableView.contentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentNever;
        
    } else {
        self.automaticallyAdjustsScrollViewInsets = NO;
    }

    self.tableView.mj_header = [YDRefreshHeader headerWithRefreshingTarget:self refreshingAction:@selector(pullingRefresh)];
    
    self.tableView.mj_footer = [YDRefreshFooter footerWithRefreshingTarget:self refreshingAction:@selector(loadMore)];
}

- (void)addNotification
{
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(receiveNotiToRefreshData:) name:REFRESH_SUPPLY_CENTER object:nil];
}

- (void)receiveNotiToRefreshData:(NSNotification *)notification
{
    NSMutableDictionary *dict = notification.object;
    // 10：未配齐；30：已配齐
    NSInteger tapStatus = [[dict objectForKey:@"tapStatus"] integerValue];
    if (tapStatus == 10) {
        [self refreshDataWithTagType:0];
        
    } else {
        [self refreshDataWithTagType:1];
    }
}

- (IBAction)tapSearchBtn:(id)sender
{
    [self refreshData:YES];
}

- (void)dismissKeybord
{
    [self.searchTextField resignFirstResponder];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [self.navigationController setNavigationBarHidden:NO animated:YES];
}

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

#pragma mark - 数据请求

- (void)pullingRefresh
{
    [self refreshData:NO];
}

- (void)refreshData:(BOOL)showLoading
{
    [self dismissKeybord];
    
    if (showLoading) {
        [YDLoadingView showToSuperview:self.view];
    }
    
    YDWeakSelf
    if (!self.searcher.completion) {
        self.searcher.completion = ^(BaseVMRefreshType type) {
            weakSelf.tableView.hidden = NO;
            [YDLoadingView hideToSuperview:weakSelf.view];
            [weakSelf.tableView.mj_header endRefreshing];

            if (type == BaseVMRefreshTypeNoMore) {
                [weakSelf.tableView.mj_footer endRefreshingWithNoMoreData];
                
            } else {
                [weakSelf.tableView.mj_footer endRefreshing];
            }
            
            if (weakSelf.searcher.supplyDataArray.count) {
                UIView *tableHeaderView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, 40)];
                UILabel *totalLabel = [[UILabel alloc] init];
                totalLabel.font = [UIFont systemFontOfSize:13 weight:UIFontWeightMedium];
                totalLabel.textColor = ColorFromRGB(0x999999);
                totalLabel.text = [NSString stringWithFormat:@"配货单总数：%ld", weakSelf.searcher.total];
                [totalLabel sizeToFit];
                [totalLabel setOrigin:CGPointMake(24, (tableHeaderView.height - totalLabel.height) / 2)];
                [tableHeaderView addSubview:totalLabel];
                weakSelf.tableView.tableHeaderView = tableHeaderView;
                
            } else {
                weakSelf.tableView.tableHeaderView = nil;
            }
            
            [weakSelf.tableView reloadData];
        };
    }
    
    if (!self.searcher.faildBlock) {
        self.searcher.faildBlock = ^(NSInteger code, NSString *error) {
            weakSelf.tableView.hidden = NO;
            [YDLoadingView hideToSuperview:weakSelf.view];
            [weakSelf.tableView.mj_header endRefreshing];
            [weakSelf.tableView.mj_footer endRefreshing];
            [MBProgressHUD showToastMessage:error];
        };
    }
    
    [self setSearchCondiction];
    [self.searcher refreshData];
}

- (void)loadMore
{
    [self.searcher loadMoreData];
}

#pragma mark - 点击事件

- (IBAction)tapUnsupplyBtn:(id)sender
{
    [self refreshDataWithTagType:0];
}

- (IBAction)tapSupplyBtn:(id)sender
{
    [self refreshDataWithTagType:1];
}

- (void)refreshDataWithTagType:(NSInteger)tagType
{
    [self setTagType:tagType];
    [self refreshData:YES];
}

// 切换tag状态 type:0-未配齐 1-已配齐
- (void)setTagType:(NSInteger)type
{
    self.type = type;
    
    if (type == 0) {
        [self.unsupplyBtn setTitleColor:ColorFromRGB(0x3A4BF0) forState:UIControlStateNormal];
        self.unsupplyBtn.titleLabel.font = [UIFont systemFontOfSize:15.0f weight:UIFontWeightMedium];
        self.unsupplyTagView.hidden = NO;
        
        [self.supplyBtn setTitleColor:ColorFromRGB(0x222222) forState:UIControlStateNormal];
        self.supplyBtn.titleLabel.font = [UIFont systemFontOfSize:13.0f weight:UIFontWeightMedium];
        self.supplyTagView.hidden = YES;
        
    } else {
        [self.supplyBtn setTitleColor:ColorFromRGB(0x3A4BF0) forState:UIControlStateNormal];
        self.supplyBtn.titleLabel.font = [UIFont systemFontOfSize:15.0f weight:UIFontWeightMedium];
        self.supplyTagView.hidden = NO;
        
        [self.unsupplyBtn setTitleColor:ColorFromRGB(0x222222) forState:UIControlStateNormal];
        self.unsupplyBtn.titleLabel.font = [UIFont systemFontOfSize:13.0f weight:UIFontWeightMedium];
        self.unsupplyTagView.hidden = YES;
    }
}

#pragma mark - 搜索条件

- (void)setupTagView
{
    NSMutableArray *modelArray = [NSMutableArray array];
    
    for (int i = 0; i < 2; i++) {
        YDSelectViewModel *model = [[YDSelectViewModel alloc] init];
        
        switch (i) {
            case 0:
            {
                model.title = @"日期";
                model.type = 0;
                [model createImgViewModelWithSelectedImg:@"筛选上箭头选中" unselectedImg:@"筛选上箭头未选中"];
                [model createImgViewModelWithSelectedImg:@"筛选下箭头选中" unselectedImg:@"筛选下箭头未选中"];
            }
                break;
                
            case 1:
            {
                model.title = @"欠货数";
                model.type = 0;
                [model createImgViewModelWithSelectedImg:@"筛选下箭头选中" unselectedImg:@"筛选下箭头未选中"];
                [model createImgViewModelWithSelectedImg:@"筛选上箭头选中" unselectedImg:@"筛选上箭头未选中"];
            }
                break;

            default:
                break;
        }
        [modelArray addObject:model];
    }
    
    YDWeakSelf
    self.tagView = [YDSupplySelectView createViewWithType:YDSelectViewTypeSupply modelArray:modelArray showFilterBlock:^{
        [weakSelf dismissKeybord];
        
    } refreshBlock:^{
        [weakSelf refreshData:YES];
    }];
    [self.tagBGView addSubview:self.tagView];
}

// 设置搜索模型，设置对应搜索条件
- (void)setSearchCondiction
{
    if (self.type == 0) {
        self.searcher.tabStatus = 10;
        
    } else {
        self.searcher.tabStatus = 30;
    }
    
    self.searcher.keyword = self.searchTextField.text;
    
    for (int i = 0; i < self.tagView.modelArray.count; i++) {
        YDSelectViewModel *model = self.tagView.modelArray[i];
        
        if (model.isSelected) {
            switch (i) {
                case 0:
                {
                    if (model.type == 0) {
                        self.searcher.sortType = 10;
                        
                    } else {
                        self.searcher.sortType = 11;
                    }
                }
                    break;
                    
                case 1:
                {
                    if (model.type == 0) {
                        self.searcher.sortType = 21;
                        
                    } else {
                        self.searcher.sortType = 20;
                    }
                }
                    break;
                    
                default:
                    break;
            }
        }
    }
    
    if (self.tagView) {
        self.searcher.createStartTime = self.tagView.selectedResultStartTime;
        self.searcher.createEndTime = self.tagView.selectedResultEndTime;
        self.searcher.status = self.tagView.selectedResultStatus;
    }
}

#pragma mark - UITableView代理

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    NSInteger count = self.searcher.supplyDataArray.count;
    if (count) {
        return count;
        
    } else {
        return 1;
    }
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (self.searcher.supplyDataArray.count) {
        YDSearcherSupplyData *data = self.searcher.supplyDataArray[indexPath.section];
        return [YDSupplyCenterCell getHeightWithType:self.type data:data];
        
    } else {
        return [YDNullCell getHeightWithTableView:tableView];
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (self.searcher.supplyDataArray.count) {
        YDSupplyCenterCell *cell = [YDSupplyCenterCell cellWithTableView:tableView];
        cell.type = self.type;
        cell.data = self.searcher.supplyDataArray[indexPath.section];
        YDWeakSelf
        cell.tapBlock = ^{
            [weakSelf.tableView reloadData];
        };
        
        return cell;
        
    } else {
        YDNullCell *cell = [YDNullCell cellWithTableView:tableView];
        cell.content = @"暂无数据";
        return cell;
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    if (section != 0) {
        return 10;
    }
    return 0.001;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    return [[UIView alloc] init];
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (self.searcher.supplyDataArray.count) {
        // 打开配货详情
        YDSupplyDetailVC *vc = [[YDSupplyDetailVC alloc] init];
        YDSearcherSupplyData *data = self.searcher.supplyDataArray[indexPath.section];
        vc.ID = [NSString stringWithFormat:@"%ld", (long)data.ID];
        vc.lastController = self;
        [self.navigationController pushViewController:vc animated:YES];
    }
}

#pragma mark - UITextFieldDelegate

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    
    [self refreshData:YES];

    return YES;
}

@end

//
//  YDSupplySelectFilterView.h
//  YDERP
//
//  Created by 何英健 on 2021/10/11.
//

#import <UIKit/UIKit.h>

typedef enum : NSUInteger {
    YDSelectViewTypeSupply, // 配货中心
    YDSelectViewTypeDelivery // 发货中心
} YDSelectViewType;

typedef void(^BlockSelectedCompletion)(NSInteger status, NSString *startTime, NSString *endTime);
typedef void(^BlockFilerShow)(void);

@interface YDSupplySelectFilterView : UIView

// 初始化
+ (YDSupplySelectFilterView *)createWithType:(YDSelectViewType)type completion:(BlockSelectedCompletion)completion;
// 显示筛选
- (void)showWithBlock:(BlockFilerShow)block;

@end

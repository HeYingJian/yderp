//
//  YDSupplySelectFilterView.m
//  YDERP
//
//  Created by 何英健 on 2021/10/11.
//

#import "YDSupplySelectFilterView.h"

@interface YDSupplySelectFilterView ()

@property (nonatomic, assign) YDSelectViewType type;
@property (nonatomic, strong) NSMutableArray<YDSearcherPropList *> *data;

// 记录订单筛选开始时间
@property (nonatomic, copy) NSString *orderStartTime;
@property (nonatomic, strong) UIButton *startTimebtn;
// 记录订单筛选结束时间
@property (nonatomic, copy) NSString *orderEndTime;
@property (nonatomic, strong) UIButton *endTimebtn;

@property (nonatomic, copy) BlockSelectedCompletion selectedCompletion;

// 存储当前所有按钮的数据
@property (nonatomic, strong) NSMutableArray<NSMutableArray *> *btnArray;

@property (nonatomic, strong) UIView *BGView;
@property (nonatomic, strong) UIView *mainView;
@property (nonatomic, strong) UIScrollView *scrollView;

@property (nonatomic, strong) UIDatePicker *datePicker;

@end

@implementation YDSupplySelectFilterView

#pragma mark - 懒加载

- (NSMutableArray<NSMutableArray *> *)btnArray
{
    if (!_btnArray) {
        _btnArray = [NSMutableArray array];
    }
    
    return _btnArray;
}

- (UIView *)BGView
{
    if (!_BGView) {
        _BGView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT)];
        _BGView.backgroundColor = [UIColor blackColor];
        _BGView.alpha = 0.7;
        
        UITapGestureRecognizer *tapBG = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapConfirmBtn)];
        [_BGView addGestureRecognizer:tapBG];
        
        [self insertSubview:_BGView atIndex:0];
    }
    
    return _BGView;
}

- (UIView *)mainView
{
    if (!_mainView) {
        _mainView = [[UIView alloc] initWithFrame:CGRectMake(SCREEN_WIDTH, 0, SCREEN_WIDTH - 75, SCREEN_HEIGHT)];
        _mainView.backgroundColor = [UIColor whiteColor];
        
        UILabel *label = [[UILabel alloc] init];
        label.text = @"筛选";
        label.font = [UIFont systemFontOfSize:19 weight:UIFontWeightSemibold];
        label.textColor = ColorFromRGB(0x222222);
        [label sizeToFit];
        CGFloat labelOriginY = 25;
        if (IS_PhoneXAll) {
            labelOriginY += 30;
        }
        [label setOrigin:CGPointMake((_mainView.width - label.width) / 2, labelOriginY)];
        [_mainView addSubview:label];
        
        UIButton *resetBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        CGFloat btnW = (SCREEN_WIDTH - 75 - 24 * 2 - 11) / 2;
        CGFloat originY = SCREEN_HEIGHT - 45 - 10;
        if (IS_PhoneXAll) {
            originY -= 10;
        }
        [resetBtn setFrame:CGRectMake(24, originY, btnW, 45)];
        [resetBtn setTitle:@"重置" forState:UIControlStateNormal];
        [resetBtn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        resetBtn.titleLabel.font = [UIFont systemFontOfSize:17];
        [resetBtn setBackgroundColor:[UIColor whiteColor]];
        resetBtn.layer.borderColor = ColorFromRGB(0xE2E2E2).CGColor;
        resetBtn.layer.borderWidth = 1.5;
        resetBtn.layer.cornerRadius = resetBtn.height / 2;
        [resetBtn addTarget:self action:@selector(tapResetBtn) forControlEvents:UIControlEventTouchUpInside];
        [_mainView addSubview:resetBtn];
        
        UIButton *confirmBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        [confirmBtn setFrame:CGRectMake(CGRectGetMaxX(resetBtn.frame) + 11, resetBtn.y, resetBtn.width, resetBtn.height)];
        [confirmBtn setTitle:@"确定" forState:UIControlStateNormal];
        confirmBtn.titleLabel.font = [UIFont systemFontOfSize:17];
        [confirmBtn setBackgroundColor:ColorFromRGB(0x3A4BF0)];
        confirmBtn.layer.cornerRadius = confirmBtn.height / 2;
        [confirmBtn addTarget:self action:@selector(tapConfirmBtn) forControlEvents:UIControlEventTouchUpInside];
        [_mainView addSubview:confirmBtn];
        
        CGFloat scrollViewOriginY = CGRectGetMaxY(label.frame) + 10;
        self.scrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(15, scrollViewOriginY, _mainView.width - 15 * 2, _mainView.height - resetBtn.height - 10 - scrollViewOriginY)];
        [_mainView addSubview:_scrollView];
        
        [self insertSubview:_mainView aboveSubview:self.BGView];
    }
    
    return _mainView;
}

- (UIDatePicker *)datePicker
{
    if (!_datePicker) {
        _datePicker = [[UIDatePicker alloc] initWithFrame:CGRectMake(0, 280, SCREEN_WIDTH, 200)];
        _datePicker.datePickerMode = UIDatePickerModeDate;
        _datePicker.locale = [[NSLocale alloc] initWithLocaleIdentifier:@"zh_CN"];
        if (@available(iOS 13.4, *)) {
            _datePicker.preferredDatePickerStyle = UIDatePickerStyleWheels;
        }
    }
    return _datePicker;
}

#pragma mark - 对外方法

+ (YDSupplySelectFilterView *)createWithType:(YDSelectViewType)type completion:(BlockSelectedCompletion)completion
{
    YDSupplySelectFilterView *view = [[YDSupplySelectFilterView alloc] init];
    view.type = type;
    view.selectedCompletion = completion;
    view.frame = CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT);
    view.backgroundColor = [UIColor clearColor];

    [view BGView];
    [view mainView];
    
    view.data = [NSMutableArray array];
    if (type == YDSelectViewTypeSupply) {
        [view addSupplyStatusData];
        
    } else if (type == YDSelectViewTypeDelivery) {
        [view addDeliveryStatusData];
    }
    
    [view refreshUI];

    return view;
}

- (void)showWithBlock:(BlockFilerShow)block
{
    [self showFilterViewWithBlock:block];
}

// 显示筛选界面
- (void)showFilterViewWithBlock:(BlockFilerShow)block
{
    if (block) {
        block();
    }
    
    [KeyWindow addSubview:self];
    
    [UIView animateWithDuration:0.3f animations:^{
        [self.mainView setX:75];
    }];
}

- (void)hideFilterView
{
    [UIView animateWithDuration:0.3f animations:^{
        [self.mainView setX:SCREEN_WIDTH];
        
    } completion:^(BOOL finished) {
        [self removeFromSuperview];
    }];
}

#pragma mark - 数据来源

// 添加配货状态选择
- (void)addSupplyStatusData
{
    YDSearcherPropList *propData = [[YDSearcherPropList alloc] init];
    propData.propName = @"状态";
    [self.data addObject:propData];

    YDSearcherPropValueList *subData1 = [[YDSearcherPropValueList alloc] init];
    subData1.propValue = @"部分配货";
    YDSearcherPropValueList *subData2 = [[YDSearcherPropValueList alloc] init];
    subData2.propValue = @"待配货";
    YDSearcherPropValueList *subData3 = [[YDSearcherPropValueList alloc] init];
    subData3.propValue = @"重配";
    propData.propValueList = [NSMutableArray arrayWithObjects:subData1, subData2, subData3, nil];
}

// 添加发货状态选择
- (void)addDeliveryStatusData
{
    YDSearcherPropList *propData = [[YDSearcherPropList alloc] init];
    propData.propName = @"状态";
    [self.data addObject:propData];

    YDSearcherPropValueList *subData1 = [[YDSearcherPropValueList alloc] init];
    subData1.propValue = @"全部";
    YDSearcherPropValueList *subData2 = [[YDSearcherPropValueList alloc] init];
    subData2.propValue = @"待发货";
    YDSearcherPropValueList *subData3 = [[YDSearcherPropValueList alloc] init];
    subData3.propValue = @"已发货";
    YDSearcherPropValueList *subData4 = [[YDSearcherPropValueList alloc] init];
    subData4.propValue = @"驳回重配";
    propData.propValueList = [NSMutableArray arrayWithObjects:subData1, subData2, subData3, subData4, nil];
}

#pragma mark - 页面构造

- (void)refreshUI
{
    [self.scrollView removeAllSubviews];
    [self.btnArray removeAllObjects];
    self.orderStartTime = nil;
    self.orderEndTime = nil;
    
    CGFloat originY = 0;
    // 筛选第一项是时间选择
    UIView *subView = [self createTimeSelectView];
    [subView setY:originY];
    originY = CGRectGetMaxY(subView.frame) + 25;

    // 其他选择项
    for (int i = 0; i < self.data.count; i++) {
        UIView *subView = [self createSubViewWithIndex:i];
        if (i == 0) {
            [subView setY:originY];
            
        } else {
            [subView setY:originY + 25];
        }
        
        if (i == self.data.count - 1) {
            [self.scrollView setContentSize:CGSizeMake(self.scrollView.width, CGRectGetMaxY(subView.frame) + 10)];
            
        } else {
            originY = CGRectGetMaxY(subView.frame);
        }
    }
}

// 创建时间选择
- (UIView *)createTimeSelectView
{
    UIView *view = [[UIView alloc] init];
    [view setWidth:self.scrollView.width];
    [view setHeight:67];
    [self.scrollView addSubview:view];
    
    UILabel *title = [[UILabel alloc] init];
    title.font = [UIFont systemFontOfSize:17 weight:UIFontWeightSemibold];
    title.textColor = ColorFromRGB(0x222222);
    title.text = @"日期";
    [title sizeToFit];
    [title setY:0];
    [view addSubview:title];
    
    self.startTimebtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [_startTimebtn setWidth:(self.mainView.width - 15 * 2 - 20) / 2];
    [_startTimebtn setHeight:32];
    [_startTimebtn setY:35];
    [_startTimebtn setX:0];
    [_startTimebtn setTitle:@"开始日期" forState:UIControlStateNormal];
    _startTimebtn.layer.cornerRadius = _startTimebtn.height / 2;
    [_startTimebtn setBackgroundColor:ColorFromRGB(0xF4F4F4)];
    [_startTimebtn setTitleColor:ColorFromRGB(0xC1C1C6) forState:UIControlStateNormal];
    [_startTimebtn setTitleColor:ColorFromRGB(0x222222) forState:UIControlStateSelected];
    _startTimebtn.titleLabel.font = FONT_NUMBER(12);
    [_startTimebtn setTarget:self action:@selector(tapStartTimeBtn) forControlEvents:UIControlEventTouchUpInside];
    [view addSubview:_startTimebtn];
    
    UIView *lineView = [[UIView alloc] initWithFrame:CGRectMake(CGRectGetMaxX(_startTimebtn.frame) + 7, _startTimebtn.y + _startTimebtn.height / 2, 6, 1)];
    lineView.backgroundColor = ColorFromRGB(0x222222);
    [view addSubview:lineView];
    
    self.endTimebtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [_endTimebtn setFrame:_startTimebtn.frame];
    [_endTimebtn setX:CGRectGetMaxX(_startTimebtn.frame) + 20];
    [_endTimebtn setTitle:@"结束日期" forState:UIControlStateNormal];
    _endTimebtn.layer.cornerRadius = _endTimebtn.height / 2;
    [_endTimebtn setBackgroundColor:ColorFromRGB(0xF4F4F4)];
    [_endTimebtn setTitleColor:ColorFromRGB(0xC1C1C6) forState:UIControlStateNormal];
    [_endTimebtn setTitleColor:ColorFromRGB(0x222222) forState:UIControlStateSelected];
    _endTimebtn.titleLabel.font = FONT_NUMBER(12);
    [_endTimebtn setTarget:self action:@selector(tapEndTimeBtn) forControlEvents:UIControlEventTouchUpInside];
    [view addSubview:_endTimebtn];
    
    return view;
}

// 点击选择订单开始时间
- (void)tapStartTimeBtn
{
    YDWeakSelf
    [self showDatePickerWithTimeType:0 block:^(NSDateComponents *dateComponents) {
        NSString *str = [NSString stringWithFormat:@"%ld-%ld-%ld", (long)dateComponents.year, (long)dateComponents.month, (long)dateComponents.day];
        weakSelf.orderStartTime = str;
        [weakSelf.startTimebtn setTitle:str forState:UIControlStateNormal];
        weakSelf.startTimebtn.selected = YES;
    }];
}

// 点击选择订单结束时间
- (void)tapEndTimeBtn
{
    YDWeakSelf
    [self showDatePickerWithTimeType:1 block:^(NSDateComponents *dateComponents) {
        NSString *str = [NSString stringWithFormat:@"%ld-%ld-%ld", (long)dateComponents.year, (long)dateComponents.month, (long)dateComponents.day];
        weakSelf.orderEndTime = str;
        [weakSelf.endTimebtn setTitle:str forState:UIControlStateNormal];
        weakSelf.endTimebtn.selected = YES;
    }];
}

- (void)showDatePickerWithTimeType:(NSInteger)timeType block:(void(^)(NSDateComponents *dateComponents))block
{
    PGDatePickManager *datePickManager = [[PGDatePickManager alloc] init];
    datePickManager.isShadeBackground = true;
    PGDatePicker *datePicker = datePickManager.datePicker;
    datePicker.datePickerType = PGDatePickerTypeVertical;
    datePicker.isHiddenMiddleText = false;
    datePicker.datePickerMode = PGDatePickerModeDate;
    datePicker.selectedDate = block;
    
    NSDateFormatter *format = [[NSDateFormatter alloc] init];
    format.dateFormat = @"yyyy-MM-dd";
    
    if (timeType == 0) {
        // 开始时间
        if (self.orderStartTime.length) {
            [datePicker setDate:[format dateFromString:self.orderStartTime]];
        }
        
        if (self.orderEndTime.length) {
            datePicker.maximumDate = [format dateFromString:self.orderEndTime];
        }
        
    } else {
        // 结束时间
        if (self.orderEndTime.length) {
            [datePicker setDate:[format dateFromString:self.orderEndTime]];
        }
        
        datePicker.maximumDate = [NSDate date];
        
        if (self.orderStartTime.length) {
            datePicker.minimumDate = [format dateFromString:self.orderStartTime];
        }
    }
    [[UINavigationController rootNavigationController] presentViewController:datePickManager animated:false completion:nil];
}

- (UIView *)createSubViewWithIndex:(NSInteger)index
{
    YDSearcherPropList *propData = self.data[index];
    
    UIView *view = [[UIView alloc] init];
    [view setWidth:self.scrollView.width];
    
    UILabel *title = [[UILabel alloc] init];
    title.font = [UIFont systemFontOfSize:17 weight:UIFontWeightSemibold];
    title.textColor = ColorFromRGB(0x222222);
    title.text = propData.propName;
    [title sizeToFit];
    [title setY:0];
    [view addSubview:title];
    
    // 保存创建的按钮
    NSMutableArray *array = [NSMutableArray array];
    [self.btnArray addObject:array];
    
    for (int i = 0; i < propData.propValueList.count; i++) {
        UIButton *btn = [self createBtnWithPropData:propData index:i];
        [view addSubview:btn];
        [array addObject:btn];

        if (i == propData.propValueList.count - 1) {
            [view setHeight:CGRectGetMaxY(btn.frame)];
        }
        
        // 按钮选择初始化
        BOOL btnSelected = NO;
        if (self.type == YDSelectViewTypeDelivery) {
            if ([propData.propName isEqualToString:@"状态"] && i == 0) {
                // "状态"选择"全部"
                btnSelected = YES;
            }
        }
        
        [self setBtnStatus:btn selected:btnSelected];
    }
    
    [self.scrollView addSubview:view];
    
    return view;
}

- (UIButton *)createBtnWithPropData:(YDSearcherPropList *)propData index:(NSInteger)index
{
    YDSearcherPropValueList *subData = propData.propValueList[index];
    
    UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
    [btn setWidth:(self.mainView.width - 15 * 4) / 3];
    [btn setHeight:32];
    [btn setY:35 + index / 3 * (32 + 15)];
    [btn setX:index%3 * (15 + btn.width)];
    NSString *str = subData.propValue;
    if (str.length > 5) {
        str = [NSString stringWithFormat:@"%@…", [str substringToIndex:5]];
    }
    [btn setTitle:str forState:UIControlStateNormal];
    btn.layer.borderColor = ColorFromRGB(0xFF5630).CGColor;
    btn.layer.cornerRadius = btn.height / 2;
    [btn setTitleColor:ColorFromRGB(0x222222) forState:UIControlStateNormal];
    [btn setTitleColor:ColorFromRGB(0xFF5630) forState:UIControlStateSelected];
    [btn setTarget:self action:@selector(tapSelectBtn:) forControlEvents:UIControlEventTouchUpInside];
    
    return btn;
}

// 改变按钮选择状态
- (void)setBtnStatus:(UIButton *)btn selected:(BOOL)selected
{
    if (selected) {
        btn.titleLabel.font = [UIFont systemFontOfSize:12.0f weight:UIFontWeightMedium];
        btn.backgroundColor = ColorFromRGB(0xFFF6F2);
        btn.layer.borderWidth = 1;
        
    } else {
        btn.titleLabel.font = [UIFont systemFontOfSize:12.0f weight:UIFontWeightMedium];
        btn.backgroundColor = ColorFromRGB(0xF4F4F4);
        btn.layer.borderWidth = 0;
    }
    
    btn.selected = selected;
}

- (void)tapSelectBtn:(UIButton *)btn
{
    for (int i = 0; i < self.btnArray.count; i++) {
        NSMutableArray *subArray = self.btnArray[i];
        
        if (btn.selected) {
            // 发货筛选的最后一项，状态栏必选一个
            if (self.type == YDSelectViewTypeDelivery && i == self.btnArray.count - 1) {
                // 必须选择一个,不能取消选择
                return;
            }
        }
        
        if ([subArray containsObject:btn]) {
            for (UIButton *subBtn in subArray) {
                if (subBtn == btn) {
                    [self setBtnStatus:btn selected:!btn.selected];
                    
                } else {
                    [self setBtnStatus:subBtn selected:NO];
                }
            }
        }
    }
}

#pragma mark - 点击事件

- (void)tapResetBtn
{
    [self refreshUI];
}

- (void)tapConfirmBtn
{
    [self hideFilterView];

    if (self.selectedCompletion) {
        NSInteger status = -1;
        
        for (int i = 0; i < self.btnArray.count; i++) {
            NSMutableArray *subArray = self.btnArray[i];
            
            for (int m = 0; m < subArray.count; m++) {
                UIButton *btn = subArray[m];
                
                if (btn.selected) {
                    // 状态栏
                    status = [self getStatusWithBtnIndex:m];
                }
            }
        }
        self.selectedCompletion(status, self.orderStartTime, self.orderEndTime);
    }
}

#pragma mark - 筛选结果判断

// 获取“状态”结果
- (NSInteger)getStatusWithBtnIndex:(NSInteger)index
{
    if (self.type == YDSelectViewTypeSupply) {
        switch (index) {
            case 0:
                // 部分配货
                return 20;
            case 1:
                // 待配货
                return 10;
            case 2:
                // 重配
                return 19;
            default:
                // 查全部
                return -1;
        }
        
    } else if (self.type == YDSelectViewTypeDelivery) {
        switch (index) {
            case 1:
                // 待发货
                return 10;
            case 2:
                // 已发货
                return 20;
            case 3:
                // 驳回重配
                return 30;
            default:
                // 全部
                return -1;
        }
    }

    return -1;
}

@end

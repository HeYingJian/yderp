//
//  YDSupplySelectView.h
//  YDERP
//
//  Created by 何英健 on 2021/10/11.
//

#import <UIKit/UIKit.h>
#import "YDSupplySelectFilterView.h"
#import "YDSelectViewModel.h"

typedef void(^BlockSelectViewTapRefresh)(void);
typedef void(^BlockSelectViewShowFilter)(void);

@interface YDSupplySelectView : UIView

/**
 创建方法
 @param type 类型
 @param modelArray 数据模型
 @param showFilterBlock 显示筛选
 @param refreshBlock 通知外部刷新数据
 */
+ (YDSupplySelectView *)createViewWithType:(YDSelectViewType)type modelArray:(NSMutableArray<YDSelectViewModel *> *)modelArray showFilterBlock:(BlockSelectViewShowFilter)showFilterBlock refreshBlock:(BlockSelectViewTapRefresh)refreshBlock;

// 用于外部查看
@property (nonatomic, strong) NSMutableArray<YDSelectViewModel *> *modelArray;

// 筛选结果数组
@property (nonatomic, strong) NSMutableArray *selectedResultArray;

#pragma mark - 搜索条件

// 筛选结果中的“日期-开始”
@property (nonatomic, copy) NSString *selectedResultStartTime;
// 筛选结果中的“日期-结束”
@property (nonatomic, copy) NSString *selectedResultEndTime;
/**
 筛选结果中的“状态”
 配货中心:不选/部分配货/待配货/重配
 发货中心:不选/待发货/已发货/驳回重配
 */
@property (nonatomic, assign) NSInteger selectedResultStatus;

@end

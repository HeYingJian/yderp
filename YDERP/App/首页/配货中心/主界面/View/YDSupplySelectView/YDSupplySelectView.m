//
//  YDSupplySelectView.m
//  YDERP
//
//  Created by 何英健 on 2021/10/11.
//

#import "YDSupplySelectView.h"

@interface YDSupplySelectView ()

@property (nonatomic, assign) YDSelectViewType type;
@property (nonatomic, copy) BlockSelectViewTapRefresh refreshBlock;
@property (nonatomic, copy) BlockSelectViewShowFilter showFilterBlock;

@property (nonatomic, strong) YDSupplySelectFilterView *filterView;

@property (nonatomic, weak) IBOutlet UIView *selectView1;
@property (nonatomic, weak) IBOutlet UIView *selectView2;
@property (nonatomic, weak) IBOutlet UIView *selectView3;

@property (nonatomic, strong) NSMutableArray *titleLabelArray;
@property (nonatomic, weak) IBOutlet UILabel *titleLabel1;
@property (nonatomic, weak) IBOutlet UILabel *titleLabel2;
@property (nonatomic, weak) IBOutlet UILabel *titleLabel3;

@property (nonatomic, strong) NSMutableArray *imgViewArray;
@property (nonatomic, weak) IBOutlet UIImageView *imgView1;
@property (nonatomic, weak) IBOutlet UIImageView *imgView2;
@property (nonatomic, weak) IBOutlet UIImageView *imgView3;

@property (nonatomic, weak) IBOutlet NSLayoutConstraint *selectViewW1;
@property (nonatomic, weak) IBOutlet NSLayoutConstraint *selectViewW2;

@end

@implementation YDSupplySelectView

#pragma mark - 懒加载

- (NSMutableArray *)titleLabelArray
{
    if (!_titleLabelArray) {
        _titleLabelArray = [NSMutableArray array];
    }
    
    return _titleLabelArray;
}

- (NSMutableArray *)imgViewArray
{
    if (!_imgViewArray) {
        _imgViewArray = [NSMutableArray array];
    }
    
    return _imgViewArray;
}

- (YDSupplySelectFilterView *)filterView
{
    if (!_filterView) {
        YDWeakSelf
        _filterView = [YDSupplySelectFilterView createWithType:self.type completion:^(NSInteger status, NSString *startTime, NSString *endTime) {
            weakSelf.selectedResultStatus = status;
            weakSelf.selectedResultStartTime = startTime;
            weakSelf.selectedResultEndTime = endTime;
            
            if (weakSelf.refreshBlock) {
                weakSelf.refreshBlock();
            }
            
            // “筛选”中是否有选中默认值以外的东西
            BOOL isSelected = NO;
            if (status != -1 || startTime.length || endTime.length) {
                isSelected = YES;
            }
            
            // 有选中东西的话，改变筛选的颜色
            YDSelectViewModel *model = [self.modelArray lastObject];
            model.isSelected = isSelected;
            [weakSelf refreshUI];
        }];
    }
    
    return _filterView;
}

#pragma mark - 类方法

+ (YDSupplySelectView *)createViewWithType:(YDSelectViewType)type modelArray:(NSMutableArray<YDSelectViewModel *> *)modelArray showFilterBlock:(BlockSelectViewShowFilter)showFilterBlock refreshBlock:(BlockSelectViewTapRefresh)refreshBlock
{
    YDSupplySelectView *view = [[NSBundle mainBundle] loadNibNamed:@"YDSupplySelectView" owner:nil options:nil][0];
    view.frame = CGRectMake(0, 0, SCREEN_WIDTH, 50);
    view.type = type;
    view.modelArray = modelArray;
    view.refreshBlock = refreshBlock;
    view.showFilterBlock = showFilterBlock;
    
    [view initUI];
    [view filterView];
    
    // 默认选中全部(-1去到接口请求时，就不会传这个参数)
    view.selectedResultStatus = -1;
    
    [view setSelectedWithIndex:0];

    return view;
}

- (void)initUI
{
    [self addGesture];
}

- (void)addGesture
{
    UITapGestureRecognizer *tap1 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapSelectView1)];
    [self.selectView1 addGestureRecognizer:tap1];
    
    UITapGestureRecognizer *tap2 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapSelectView2)];
    [self.selectView2 addGestureRecognizer:tap2];
    
    UITapGestureRecognizer *tap3 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapSelectView3)];
    [self.selectView3 addGestureRecognizer:tap3];
}

- (void)setModelArray:(NSMutableArray<YDSelectViewModel *> *)modelArray
{
    _modelArray = modelArray;
    
    self.imgViewArray = nil;
    self.titleLabelArray = nil;
    
    if (_modelArray.count == 1) {
        self.selectViewW1.constant = SCREEN_WIDTH / 2;
        self.selectViewW2.constant = 0;
        
        [self.imgViewArray addObject:self.imgView1];
        [self.imgViewArray addObject:self.imgView3];
        [self.titleLabelArray addObject:self.titleLabel1];
        [self.titleLabelArray addObject:self.titleLabel3];
        
    } else if (_modelArray.count == 2) {
        self.selectViewW1.constant = SCREEN_WIDTH / 3;
        self.selectViewW2.constant = SCREEN_WIDTH / 3;
        
        [self.imgViewArray addObject:self.imgView1];
        [self.imgViewArray addObject:self.imgView2];
        [self.imgViewArray addObject:self.imgView3];
        [self.titleLabelArray addObject:self.titleLabel1];
        [self.titleLabelArray addObject:self.titleLabel2];
        [self.titleLabelArray addObject:self.titleLabel3];
    }
    [self layoutIfNeeded];
    
    YDSelectViewModel *model = [[YDSelectViewModel alloc] init];
    model.type = 0;
    [model createImgViewModelWithSelectedImg:@"筛选图标选中" unselectedImg:@"筛选图标未选中"];
    [_modelArray addObject:model];
}

#pragma mark - 点击选择tab

- (void)tapSelectView1
{
    [self setSelectedWithIndex:0];
}

- (void)tapSelectView2
{
    [self setSelectedWithIndex:1];
}

- (void)tapSelectView3
{
    [self setSelectedWithIndex:2];
}

// 选中当前index，其它项(筛选以外)均变成未选中，因为筛选与其他项可以共存；如果当前项本身选中，则变为下一个选中状态(例如本身为选中倒序，则变为选中正序)
- (void)setSelectedWithIndex:(NSInteger)index
{
    if (index < self.modelArray.count - 1) {
        // 点击筛选以外的项
        for (int i = 0; i < self.modelArray.count - 1; i++) {
            YDSelectViewModel *model = self.modelArray[i];
            if (index == i) {
                if (model.isSelected) {
                    // 本身已经选中，则查看是否还有下一个选中状态
                    if (model.imgViewArray.count > 1) {
                        if (model.type == model.imgViewArray.count - 1) {
                            model.type = 0;
                            
                        } else {
                            model.type++;
                        }
                    }
                    
                } else {
                    model.isSelected = YES;
                }
                
            } else {
                model.isSelected = NO;
            }
        }
        
        [self refreshUI];
        
        if (self.refreshBlock) {
            self.refreshBlock();
        }
        
    } else {
        // 点击筛选
        [self showFilterView];
    }
}

// 根据modelArray数据，刷新为对应样式
- (void)refreshUI
{
    for (int i = 0; i < self.modelArray.count; i++) {
        YDSelectViewModel *model = self.modelArray[i];
        
        UILabel *titleLabel = self.titleLabelArray[i];
        if (model.isSelected) {
            titleLabel.textColor = COLOR_MAIN_BULE;
            
        } else {
            titleLabel.textColor = ColorFromRGB(0x222222);
        }
        
        UIImageView *imgView = self.imgViewArray[i];
        if (model.imgViewArray.count) {
            NSString *imgName;
            YDSelectImgViewModel *imgViewModel = model.imgViewArray[model.type];
            
            if (model.isSelected) {
                imgName = imgViewModel.selectedImg;
                
            } else {
                imgName = imgViewModel.unselectedImg;
            }
            [imgView setImage:[UIImage imageNamed:imgName]];
            
        } else {
            [imgView setImage:nil];
        }
    }
}

// 弹出筛选弹窗
- (void)showFilterView
{
    [self.filterView showWithBlock:self.showFilterBlock];
}

@end

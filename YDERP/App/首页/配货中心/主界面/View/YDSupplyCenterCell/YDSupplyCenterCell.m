//
//  YDSupplyCenterCell.m
//  YDERP
//
//  Created by 何英健 on 2021/10/11.
//

#import "YDSupplyCenterCell.h"

#define SUPPLY_DETAIL_ROW_HEIGHT 25

@interface YDSupplyCenterCell ()

@property (nonatomic, weak) IBOutlet UILabel *clientLabel;
@property (nonatomic, weak) IBOutlet UILabel *orderNoLabel;
@property (nonatomic, weak) IBOutlet UIImageView *statusBGImgView;
@property (nonatomic, weak) IBOutlet UILabel *statusLabel;
@property (nonatomic, weak) IBOutlet UILabel *oweLabel;
@property (nonatomic, weak) IBOutlet UILabel *canAllocLabel;
@property (nonatomic, weak) IBOutlet UILabel *AlLabel;
@property (nonatomic, weak) IBOutlet UILabel *timeLabel;
@property (nonatomic, weak) IBOutlet UIImageView *canAllocImgView;

@property (nonatomic, weak) IBOutlet UIView *detailView;
@property (nonatomic, weak) IBOutlet UIImageView *detailArrowImgView;

@property (nonatomic, weak) IBOutlet UIView *skuView;
@property (nonatomic, weak) IBOutlet UIView *codeBGView;
@property (nonatomic, weak) IBOutlet UIView *colorBGView;
@property (nonatomic, weak) IBOutlet UIView *sizeBGView;
@property (nonatomic, weak) IBOutlet UIView *stockBGView;
@property (nonatomic, weak) IBOutlet UIView *oweBGView;

@property (nonatomic, weak) IBOutlet NSLayoutConstraint *statusBGImgViewW;
@property (nonatomic, weak) IBOutlet NSLayoutConstraint *skuViewH;

@end

@implementation YDSupplyCenterCell

+ (instancetype)cellWithTableView:(UITableView *)tableView
{
    static NSString *reuseID = @"supplyCenterCell";
    YDSupplyCenterCell *cell = [tableView dequeueReusableCellWithIdentifier:reuseID];
    if (!cell) {
        cell = [[NSBundle mainBundle] loadNibNamed:@"YDSupplyCenterCell" owner:nil options:nil][0];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    
    return cell;
}

+ (CGFloat)getHeightWithType:(NSInteger)type data:(YDSearcherSupplyData *)data
{
    if (type == 0 && data.isShowDetail) {
        return 162 + 40 + data.skuList.count * SUPPLY_DETAIL_ROW_HEIGHT + 17;
        
    } else {
        return 162;
    }
}

- (void)setType:(NSInteger)type
{
    _type = type;
    
    self.detailView.hidden = (type == 0)? NO : YES;
}

- (void)setData:(YDSearcherSupplyData *)data
{
    _data = data;
    
    NSString *clientName = @"临时客户";
    if (data.clientName.length) {
        clientName = data.clientName;
    }
    self.clientLabel.text = clientName;
    self.orderNoLabel.text = data.orderNo;
    
    NSString *bgImgName;
    NSString *statusName;
    UIColor *statusColor;
    switch (data.status) {
        // 待配货
        case 10:
        {
            bgImgName = @"配货中心部分配货背景";
            statusName = @"待配货";
            statusColor = ColorFromRGB(0xFF8A30);
            self.statusBGImgViewW.constant = 46;
        }
            break;
        // 重配
        case 19:
        {
            bgImgName = @"配货中心重配背景";
            statusName = @"重配";
            statusColor = ColorFromRGB(0xFF0000);
            self.statusBGImgViewW.constant = 36;
        }
            break;
        // 部分配货
        case 20:
        {
            bgImgName = @"配货中心部分配货背景";
            statusName = @"部分配货";
            statusColor = ColorFromRGB(0xFF8A30);
            self.statusBGImgViewW.constant = 55;
        }
            break;
        // 已配齐
        case 30:
        {
            bgImgName = @"配货中心已配齐背景";
            statusName = @"已配齐";
            statusColor = ColorFromRGB(0x6195FE);
            self.statusBGImgViewW.constant = 46;
        }
            break;
            
        default:
            break;
    }
    [self.statusBGImgView setImage:[UIImage imageNamed:bgImgName]];
    self.statusLabel.text = statusName;
    self.statusLabel.textColor = statusColor;
    
    self.oweLabel.text = [NSString stringWithFormat:@"%ld", data.dueItemNum];
    self.canAllocLabel.text = [NSString stringWithFormat:@"（可配货%ld件）", data.canAllocNum];
    self.canAllocImgView.hidden = (data.canAllocNum > 0)? NO : YES;
    self.AlLabel.text = [NSString stringWithFormat:@"%ld", data.allocItemNum];
    self.timeLabel.text = data.orderTime;
    
    if (self.type == 0 && data.isShowDetail) {
        self.skuView.hidden = NO;

        // 画表格
        [self refreshSkuView];

    } else {
        self.skuView.hidden = YES;
    }
}

// 画表格
- (void)refreshSkuView
{
    [self.codeBGView removeAllSubviews];
    [self.colorBGView removeAllSubviews];
    [self.sizeBGView removeAllSubviews];
    [self.stockBGView removeAllSubviews];
    [self.oweBGView removeAllSubviews];
    
    self.skuViewH.constant = 40 + self.data.skuList.count * SUPPLY_DETAIL_ROW_HEIGHT;
    [self layoutIfNeeded];
    
    CGFloat originY = 0;
    
    for (int i = 0; i < self.data.skuList.count; i++) {
        YDSearcherSkuList *sub = self.data.skuList[i];
        
        if (i != 0) {
            UIView *lineView = [[UIView alloc] initWithFrame:CGRectMake(0, 40 + originY, SCREEN_WIDTH - 10 * 2 - 13 * 2, 0.5)];
            lineView.backgroundColor = ColorFromRGB(0xE9E9E9);
            [self.skuView addSubview:lineView];
        }

        UILabel *codeLabel = [YDSupplyCenterCell createLabel];
        [codeLabel setY:originY];
        codeLabel.text = [NSString stringWithFormat:@"%@#", sub.code];
        codeLabel.font = FONT_NUMBER(13);
        codeLabel.textColor = ColorFromRGB(0xFF5630);
        [self.codeBGView addSubview:codeLabel];
        
        UILabel *colorLabel = [YDSupplyCenterCell createLabel];
        [colorLabel setY:originY];
        colorLabel.text = sub.color;
        colorLabel.font = [UIFont systemFontOfSize:13 weight:UIFontWeightSemibold];
        [self.colorBGView addSubview:colorLabel];
        
        UILabel *sizeLabel = [YDSupplyCenterCell createLabel];
        [sizeLabel setY:originY];
        sizeLabel.text = sub.size;
        sizeLabel.font = [UIFont systemFontOfSize:13 weight:UIFontWeightSemibold];
        [self.sizeBGView addSubview:sizeLabel];
        
        UILabel *stockLabel = [YDSupplyCenterCell createLabel];
        [stockLabel setY:originY];
        stockLabel.text = [NSString stringWithFormat:@"%ld", (long)sub.stockNum];
        stockLabel.font = FONT_NUMBER(13);
        [self.stockBGView addSubview:stockLabel];
        
        UILabel *oweLabel = [YDSupplyCenterCell createLabel];
        [oweLabel setY:originY];
        oweLabel.text = [NSString stringWithFormat:@"%ld", (long)sub.dueNum];
        oweLabel.font = FONT_NUMBER(13);
        [self.oweBGView addSubview:oweLabel];
        
        originY += SUPPLY_DETAIL_ROW_HEIGHT;
    }
}

// 创建标题
+ (UILabel *)createLabel
{
    UILabel *label = [[UILabel alloc] init];
    [label setHeight:SUPPLY_DETAIL_ROW_HEIGHT];
    [label setWidth:(SCREEN_WIDTH - 10 * 2 - 13 * 2) / 5];
    label.textColor = ColorFromRGB(0x222222);
    label.textAlignment = NSTextAlignmentCenter;

    return label;
}

- (void)awakeFromNib
{
    [super awakeFromNib];
    
    self.oweLabel.font = FONT_NUMBER(15);
    self.timeLabel.font = FONT_NUMBER(12);
    
    self.skuView.layer.borderColor = ColorFromRGB(0xEBECF0).CGColor;
    self.skuView.layer.borderWidth = 0.5;
    
    YDWeakSelf
    UITapGestureRecognizer *tapDetailView = [[UITapGestureRecognizer alloc] initWithActionBlock:^(id  _Nonnull sender) {
        weakSelf.data.isShowDetail = !weakSelf.data.isShowDetail;
        
        if (weakSelf.tapBlock) {
            weakSelf.tapBlock();
        }
    }];
    [self.detailView addGestureRecognizer:tapDetailView];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end

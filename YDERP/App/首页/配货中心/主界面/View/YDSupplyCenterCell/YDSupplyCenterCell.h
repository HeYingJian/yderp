//
//  YDSupplyCenterCell.h
//  YDERP
//
//  Created by 何英健 on 2021/10/11.
//

#import <UIKit/UIKit.h>
#import "YDSearcherSupplyModel.h"

typedef void(^BlockTapSupplyCenterDetail)(void);

@interface YDSupplyCenterCell : UITableViewCell

+ (instancetype)cellWithTableView:(UITableView *)tableView;

+ (CGFloat)getHeightWithType:(NSInteger)type data:(YDSearcherSupplyData *)data;

// 0-未配齐 1-已配齐
@property (nonatomic, assign) NSInteger type;

@property (nonatomic, strong) YDSearcherSupplyData *data;

@property (nonatomic, copy) BlockTapSupplyCenterDetail tapBlock;

@end

//
//  YDSupplyDetailEditModel.h
//  YDERP
//
//  Created by 何英健 on 2021/10/17.
//

#import "YDBaseModel.h"

@interface YDSupplyDetailEditData : NSObject

// 配货完成提示信息；如：配货完成，共配货50件
@property (nonatomic, copy) NSString *msg;
// 保存确认配货后生成的发货单id
@property (nonatomic, assign) NSInteger deliveryOrderId;
// 配货单是否已配齐，10：未配齐；30：已配齐
@property (nonatomic, assign) NSInteger tabStatus;

@end

@interface YDSupplyDetailEditModel : YDBaseModel

@property (nonatomic, strong) YDSupplyDetailEditData *data;

@end

//
//  YDSupplyDetailEditProductCell.h
//  YDERP
//
//  Created by 何英健 on 2021/10/14.
//

#import <UIKit/UIKit.h>
#import "YDSupplyDetailModel.h"

typedef void(^BlockSupplyEditRefresh)(void);

@interface YDSupplyDetailEditProductCell : UITableViewCell

+ (instancetype)cellWithTableView:(UITableView *)tableView;

+ (CGFloat)getHeightWithData:(YDSupplyDetailDataItem *)data;

// 用于订单发生变化后，保存旧的数据
@property (nonatomic, strong) YDSupplyDetailDataItem *oldData;

@property (nonatomic, strong) YDSupplyDetailDataItem *data;

@property (nonatomic, copy) BlockSupplyEditRefresh editRefreshBlock;

@end

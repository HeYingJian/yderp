//
//  YDSupplyDetailEditProductCell.m
//  YDERP
//
//  Created by 何英健 on 2021/10/14.
//

#import "YDSupplyDetailEditProductCell.h"

#define SUPPLY_DETAIL_ROW_HEIGHT 40

@interface YDSupplyDetailEditProductCell () <UITextFieldDelegate>

@property (nonatomic, strong) NSMutableArray<UIButton *> *minBtnArray;
@property (nonatomic, strong) NSMutableArray<UIButton *> *plusBtnArray;
@property (nonatomic, strong) NSMutableArray<YDSearcherSkuGroupData *> *oldSkuGroupArray;
@property (nonatomic, strong) NSMutableArray<YDSearcherSkuGroupData *> *skuGroupArray;
@property (nonatomic, strong) NSMutableArray<UITextField *> *selectTextFieldArray;

@property (nonatomic, weak) IBOutlet UIImageView *productImgView;
@property (nonatomic, weak) IBOutlet UILabel *productNameLabel;
@property (nonatomic, weak) IBOutlet UILabel *noticeLabel;
@property (nonatomic, weak) IBOutlet UIView *selectView;
@property (nonatomic, weak) IBOutlet UIImageView *selectImgView;

@property (nonatomic, weak) IBOutlet UIView *detailView;
@property (nonatomic, weak) IBOutlet UIView *colorBGView;
@property (nonatomic, weak) IBOutlet UIView *sizeBGView;
@property (nonatomic, weak) IBOutlet UIView *oweBGView;
@property (nonatomic, weak) IBOutlet UIView *selectBGView;

@end

@implementation YDSupplyDetailEditProductCell

#pragma mark - 懒加载

- (NSMutableArray<UIButton *> *)minBtnArray
{
    if (!_minBtnArray) {
        _minBtnArray = [NSMutableArray array];
    }
    
    return _minBtnArray;
}

- (NSMutableArray<UIButton *> *)plusBtnArray
{
    if (!_plusBtnArray) {
        _plusBtnArray = [NSMutableArray array];
    }
    
    return _plusBtnArray;
}

- (NSMutableArray<UITextField *> *)selectTextFieldArray
{
    if (!_selectTextFieldArray) {
        _selectTextFieldArray = [NSMutableArray array];
    }
    
    return _selectTextFieldArray;
}

#pragma mark - 页面加载

+ (instancetype)cellWithTableView:(UITableView *)tableView
{
    static NSString *reuseID = @"editProductCell";
    YDSupplyDetailEditProductCell *cell = [tableView dequeueReusableCellWithIdentifier:reuseID];
    if (!cell) {
        cell = [[NSBundle mainBundle] loadNibNamed:@"YDSupplyDetailEditProductCell" owner:nil options:nil][0];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    
    return cell;
}

+ (CGFloat)getHeightWithData:(YDSupplyDetailDataItem *)data
{
    // 从skuList中获取颜色和尺码
    NSMutableArray *skuGroupArray = [YDFunction getSkuGroupArrayFromSupplySkuArray:data.skuList];
    
    NSInteger rowNum = 0;
    for (YDSearcherSkuGroupData *sub in skuGroupArray) {
        rowNum += sub.skuList.count;
    }
    return 159 + rowNum * SUPPLY_DETAIL_ROW_HEIGHT + 11;
}

- (void)setOldData:(YDSupplyDetailDataItem *)oldData
{
    _oldData = oldData;
    
    // 从skuList中获取颜色和尺码，用于画表格
    self.oldSkuGroupArray = [YDFunction getSkuGroupArrayFromSupplySkuArray:oldData.skuList];
}

- (void)setData:(YDSupplyDetailDataItem *)data
{
    _data = data;
    
    if (data.picUrl.length) {
        [self.productImgView appleSetImageWithUrl:data.picUrl SprcifyOssImageStyle:@"products_list" HasHD:NO placeholderImage:@"默认商品方形图" completion:nil];
        
    } else {
        [self.productImgView setImage:[UIImage imageNamed:@"没有商品默认图"]];
    }
    
    NSString *codeStr = data.code;
    NSString *productName = data.name;
    NSString *str = [NSString stringWithFormat:@"%@#%@", codeStr, productName];
    NSMutableAttributedString *attributedStr = [[NSMutableAttributedString alloc] initWithString:str];
    [attributedStr addAttribute:NSFontAttributeName value:FONT_NUMBER(15) range:NSMakeRange(0, codeStr.length + 1)];
    [attributedStr addAttribute:NSForegroundColorAttributeName value:ColorFromRGB(0xFF5630) range:NSMakeRange(0, codeStr.length + 1)];
    [attributedStr addAttribute:NSFontAttributeName value:[UIFont systemFontOfSize:14.0f] range:NSMakeRange(codeStr.length + 1, productName.length)];
    [attributedStr addAttribute:NSForegroundColorAttributeName value:ColorFromRGB(0x212121) range:NSMakeRange(codeStr.length + 1, productName.length)];
    NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc] init];
    [paragraphStyle setLineSpacing:5];
    [attributedStr addAttribute:NSParagraphStyleAttributeName value:paragraphStyle range:NSMakeRange(0, [str length])];
    self.productNameLabel.attributedText = attributedStr;
    self.productNameLabel.lineBreakMode = NSLineBreakByTruncatingTail;
    self.noticeLabel.text = [NSString stringWithFormat:@"欠货%ld件 已配货%ld件", (long)data.dueItemNum, (long)data.allocNum];
    
    // 设置全选按钮状态
    BOOL status = ([self isAllSelected] == 1)? YES : NO;
    [self setSelectViewStatus:status];
    
    // 从skuList中获取颜色和尺码，用于画表格
    self.skuGroupArray = [YDFunction getSkuGroupArrayFromSupplySkuArray:data.skuList];
    // 画表格
    [self refreshDetailView];
}

// 画表格
- (void)refreshDetailView
{
    self.minBtnArray = nil;
    self.plusBtnArray = nil;
    self.selectTextFieldArray = nil;
    
    [self.colorBGView removeAllSubviews];
    [self.sizeBGView removeAllSubviews];
    [self.oweBGView removeAllSubviews];
    [self.selectBGView removeAllSubviews];
    
    CGFloat colorOriginY = 0;
    CGFloat sizeOriginY = 0;
    
    for (int i = 0; i < self.skuGroupArray.count; i++) {
        YDSearcherSkuGroupData *skuGroup = self.skuGroupArray[i];
        
        YDSearcherSkuGroupData *oldSkuGroup;
        for (YDSearcherSkuGroupData *sub in self.oldSkuGroupArray) {
            if ([sub.colorId isEqualToString:skuGroup.colorId]) {
                oldSkuGroup = sub;
            }
        }
        
        UILabel *colorLabel = [YDSupplyDetailEditProductCell createLabel];
        [colorLabel setY:colorOriginY];
        [colorLabel setWidth:90];
        [colorLabel setHeight:SUPPLY_DETAIL_ROW_HEIGHT * skuGroup.skuList.count];
        colorLabel.text = skuGroup.color;
        colorLabel.font = [UIFont systemFontOfSize:11 weight:UIFontWeightSemibold];
        [self.colorBGView addSubview:colorLabel];
        
        if (i < self.skuGroupArray.count - 1) {
            colorOriginY = CGRectGetMaxY(colorLabel.frame);
            
            UIView *lineView = [YDSupplyDetailEditProductCell createLineViewWithLabel:colorLabel];
            [self.colorBGView addSubview:lineView];
        }
        
        for (int m = 0; m < skuGroup.skuList.count; m++) {
            YDSearcherSkuList *sku = skuGroup.skuList[m];
            
            YDSearcherSkuList *oldSku;
            for (YDSearcherSkuList *sub in oldSkuGroup.skuList) {
                if ([sub.ID isEqualToString:sku.ID]) {
                    oldSku = sub;
                }
            }
            
            UILabel *sizeLabel = [YDSupplyDetailEditProductCell createLabel];
            [sizeLabel setY:sizeOriginY];
            [sizeLabel setWidth:70];
            [sizeLabel setHeight:SUPPLY_DETAIL_ROW_HEIGHT];
            sizeLabel.text = sku.size;
            sizeLabel.font = [UIFont systemFontOfSize:11 weight:UIFontWeightSemibold];
            [self.sizeBGView addSubview:sizeLabel];
            
            UILabel *oweLabel = [YDSupplyDetailEditProductCell createLabel];
            [oweLabel setY:sizeOriginY];
            [oweLabel setWidth:80];
            [oweLabel setHeight:SUPPLY_DETAIL_ROW_HEIGHT];
            oweLabel.text = [NSString stringWithFormat:@"%ld", (long)sku.dueNum];
            oweLabel.font = FONT_NUMBER(13);
            if (oldSku.dueNum == sku.dueNum) {
                oweLabel.textColor = ColorFromRGB(0x222222);
                
            } else {
                oweLabel.textColor = ColorFromRGB(0xFFAB00);
            }
            [self.oweBGView addSubview:oweLabel];
            
            UIView *selectView = [self createSelectViewWithSku:sku];
            [selectView setY:sizeOriginY + 5];
            [self.selectBGView addSubview:selectView];
            
            if (i < self.skuGroupArray.count - 1 || m < skuGroup.skuList.count - 1) {
                sizeOriginY = CGRectGetMaxY(sizeLabel.frame);
                
                UIView *sizeLineView = [YDSupplyDetailEditProductCell createLineViewWithLabel:sizeLabel];
                [self.sizeBGView addSubview:sizeLineView];
                
                UIView *oweLineView = [YDSupplyDetailEditProductCell createLineViewWithLabel:oweLabel];
                [self.oweBGView addSubview:oweLineView];
                
                UIView *selectLineView = [[UIView alloc] initWithFrame:CGRectMake(0, oweLineView.y, selectView.width + 15 * 2, 0.5)];
                selectLineView.backgroundColor = ColorFromRGB(0xE9E9E9);
                [self.selectBGView addSubview:selectLineView];
            }
        }
    }
}

// 创建标题
+ (UILabel *)createLabel
{
    UILabel *label = [[UILabel alloc] init];
    label.textColor = ColorFromRGB(0x222222);
    label.textAlignment = NSTextAlignmentCenter;

    return label;
}

// 创建分割线
+ (UIView *)createLineViewWithLabel:(UILabel *)label
{
    UIView *lineView = [[UIView alloc] initWithFrame:CGRectMake(0, CGRectGetMaxY(label.frame), label.width, 0.5)];
    lineView.backgroundColor = ColorFromRGB(0xE9E9E9);
    
    return lineView;
}

- (UIView *)createSelectViewWithSku:(YDSearcherSkuList *)sku
{
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(15, 0, SCREEN_WIDTH - 17 * 2 - 90 - 70 - 80 - 15 * 2, 30)];
    view.backgroundColor = [UIColor whiteColor];
    view.layer.borderColor = ColorFromRGB(0xDADADA).CGColor;
    view.layer.borderWidth = 0.5;
    
    UIButton *minBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [minBtn setFrame:CGRectMake(0, 0, view.height, view.height)];
    [minBtn setTitle:@"-" forState:UIControlStateNormal];
    [minBtn setTitleColor:ColorFromRGB(0x666666) forState:UIControlStateNormal];
    minBtn.titleLabel.font = [UIFont systemFontOfSize:20.0f];
    [minBtn setTarget:self action:@selector(tapMinBtn:) forControlEvents:UIControlEventTouchUpInside];
    [view addSubview:minBtn];
    [self.minBtnArray addObject:minBtn];
    
    UIButton *plusBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [plusBtn setFrame:CGRectMake(view.width - view.height, 0, view.height, view.height)];
    [plusBtn setTitle:@"+" forState:UIControlStateNormal];
    [plusBtn setTitleColor:ColorFromRGB(0x666666) forState:UIControlStateNormal];
    plusBtn.titleLabel.font = [UIFont systemFontOfSize:20.0f];
    [plusBtn setTarget:self action:@selector(tapPlusBtn:) forControlEvents:UIControlEventTouchUpInside];
    [view addSubview:plusBtn];
    [self.plusBtnArray addObject:plusBtn];
    
    UITextField *textField = [[UITextField alloc] initWithFrame:CGRectMake(CGRectGetMaxX(minBtn.frame), 0, view.width - 2 * view.height, view.height)];
    textField.delegate = self;
    textField.textAlignment = NSTextAlignmentCenter;
    textField.textColor = [UIColor blackColor];
    textField.font = FONT_NUMBER(14);
    textField.keyboardType = UIKeyboardTypeNumberPad;
    [textField addToolSenderWithBlock:nil];
    textField.text = [NSString stringWithFormat:@"%ld", sku.selectedNum];
    [view addSubview:textField];
    [self.selectTextFieldArray addObject:textField];
    
    return view;
}

- (void)tapMinBtn:(UIButton *)btn
{
    YDSupplyDetailDataItemSku *sku = [self getSkuWithView:btn array:self.minBtnArray];
    if (sku.selectedNum > 0) {
        sku.selectedNum--;
    }
    
    if (self.editRefreshBlock) {
        self.editRefreshBlock();
    }
}

- (void)tapPlusBtn:(UIButton *)btn
{
    YDSupplyDetailDataItemSku *sku = [self getSkuWithView:btn array:self.plusBtnArray];
    if (sku.selectedNum < sku.dueNum && sku.selectedNum < sku.stockNum) {
        sku.selectedNum++;
    }
    
    if (self.editRefreshBlock) {
        self.editRefreshBlock();
    }
}

- (YDSupplyDetailDataItemSku *)getSkuWithView:(UIView *)view array:(NSMutableArray *)array
{
    NSInteger index = [self getViewIndexFromArray:view array:array];
    YDSupplyDetailDataItemSku *sku = self.data.skuList[index];
    return sku;
}

- (NSInteger)getViewIndexFromArray:(UIView *)view array:(NSMutableArray *)array
{
    for (int i = 0; i < array.count; i++) {
        if (view == array[i]) {
            return i;
        }
    }
    
    return -1;
}

- (void)setSelectViewStatus:(BOOL)status
{
    NSString *imgName;
    if (status) {
        imgName = @"配货中心选中";
        
    } else {
        imgName = @"配货中心未选中";
    }
    [self.selectImgView setImage:[UIImage imageNamed:imgName]];
}

- (void)tapSelectView
{
    NSInteger status = [self isAllSelected];
    
    if (status == -1) {
        [MBProgressHUD showToastMessage:@"暂无充足库存可配货"];
    }
    
    for (YDSupplyDetailDataItemSku *sub in self.data.skuList) {
        if (status == 1) {
            // 已经全部配货，则清空
            sub.selectedNum = 0;
            
        } else {
            NSInteger max = sub.dueNum;
            if (max > sub.stockNum) {
                max = sub.stockNum;
            }
            sub.selectedNum = max;
        }
    }
    
    if (self.editRefreshBlock) {
        self.editRefreshBlock();
    }
}

// 是否全部配货 -1:不能配货 0:没有全部配货 1:已经全部配货
- (NSInteger)isAllSelected
{
    NSInteger totalSelected = 0;
    
    for (YDSupplyDetailDataItemSku *sub in self.data.skuList) {
        NSInteger max = sub.dueNum;
        if (max > sub.stockNum) {
            max = sub.stockNum;
        }
        
        if (sub.selectedNum < max) {
            return 0;
        }
        
        totalSelected += sub.selectedNum;
    }
    
    // 没有任何可以配货的项
    if (totalSelected == 0) {
        return -1;
    }
    
    return 1;
}

- (void)awakeFromNib
{
    [super awakeFromNib];
    
    self.detailView.layer.borderColor = ColorFromRGB(0xEBECF0).CGColor;
    self.detailView.layer.borderWidth = 0.5;
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapSelectView)];
    [self.selectView addGestureRecognizer:tap];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

#pragma mark - UITextFieldDelegate

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    textField.text = nil;
    return YES;
}

- (BOOL)textFieldShouldEndEditing:(UITextField *)textField
{
    if (textField.text.length) {
        YDSupplyDetailDataItemSku *sku = [self getSkuWithView:textField array:self.selectTextFieldArray];
        NSInteger num = [textField.text integerValue];
        NSInteger max = sku.dueNum;
        if (max > sku.stockNum) {
            max = sku.stockNum;
        }
        
        if (num > max) {
            num = max;
        }
        sku.selectedNum = num;
    }
    
    dispatch_async(dispatch_get_main_queue(),^{
        if (self.editRefreshBlock) {
            self.editRefreshBlock();
        }
    });
    
    return YES;
}

@end

//
//  YDSupplyDetailEditPhotoCell.h
//  YDERP
//
//  Created by 何英健 on 2021/10/15.
//

#import <UIKit/UIKit.h>
#import "YDSupplyDetailModel.h"

typedef void(^BlockSupplyEditRefresh)(void);

@interface YDSupplyDetailEditPhotoCell : UITableViewCell

+ (instancetype)cellWithTableView:(UITableView *)tableView;

+ (CGFloat)getHeightWithData:(YDSupplyDetailData *)data;

@property (nonatomic, strong) YDSupplyDetailData *data;

@property (nonatomic, weak) UIViewController *rootVC;

@property (nonatomic, copy) BlockSupplyEditRefresh editRefreshBlock;

@end

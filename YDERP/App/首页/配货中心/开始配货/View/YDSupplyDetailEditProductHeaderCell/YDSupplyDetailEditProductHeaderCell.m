//
//  YDSupplyDetailEditProductHeaderCell.m
//  YDERP
//
//  Created by 何英健 on 2021/10/14.
//

#import "YDSupplyDetailEditProductHeaderCell.h"

@interface YDSupplyDetailEditProductHeaderCell ()

@property (nonatomic, weak) IBOutlet UIView *selectView;
@property (nonatomic, weak) IBOutlet UIImageView *selectImgView;

@end

@implementation YDSupplyDetailEditProductHeaderCell

+ (instancetype)cellWithTableView:(UITableView *)tableView
{
    static NSString *reuseID = @"editProductHeaderCell";
    YDSupplyDetailEditProductHeaderCell *cell = [tableView dequeueReusableCellWithIdentifier:reuseID];
    if (!cell) {
        cell = [[NSBundle mainBundle] loadNibNamed:@"YDSupplyDetailEditProductHeaderCell" owner:nil options:nil][0];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    
    return cell;
}

+ (CGFloat)getHeight
{
    return 50;
}

- (void)setData:(YDSupplyDetailData *)data
{
    _data = data;
    
    // 设置全选按钮状态
    BOOL status = ([self isAllSelected] == 1)? YES : NO;
    [self setSelectViewStatus:status];
}

- (void)setSelectViewStatus:(BOOL)status
{
    NSString *imgName;
    if (status) {
        imgName = @"配货中心选中";
        
    } else {
        imgName = @"配货中心未选中";
    }
    [self.selectImgView setImage:[UIImage imageNamed:imgName]];
}

- (void)tapSelectView
{
    NSInteger status = [self isAllSelected];
    
    if (status == -1) {
        [MBProgressHUD showToastMessage:@"暂无充足库存可配货"];
    }
    
    for (YDSupplyDetailDataItem *item in self.data.itemList) {
        for (YDSupplyDetailDataItemSku *sub in item.skuList) {
            if (status == 1) {
                // 已经全部配货，则清空
                sub.selectedNum = 0;
                
            } else {
                NSInteger max = sub.dueNum;
                if (max > sub.stockNum) {
                    max = sub.stockNum;
                }
                sub.selectedNum = max;
            }
        }
    }
    
    if (self.editRefreshBlock) {
        self.editRefreshBlock();
    }
}

// 是否全部配货 -1:存在某个商品不能配货 0:没有全部配货 1:已经全部配货
- (NSInteger)isAllSelected
{
    for (YDSupplyDetailDataItem *item in self.data.itemList) {
        NSInteger totalSelected = 0;
        
        for (YDSupplyDetailDataItemSku *sub in item.skuList) {
            NSInteger max = sub.dueNum;
            if (max > sub.stockNum) {
                max = sub.stockNum;
            }
            
            if (sub.selectedNum < max) {
                return 0;
            }
            
            totalSelected += sub.selectedNum;
        }
        
        // 没有任何可以配货的项
        if (totalSelected == 0) {
            return -1;
        }
    }
    
    return 1;
}

- (void)awakeFromNib
{
    [super awakeFromNib];
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapSelectView)];
    [self.selectView addGestureRecognizer:tap];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end

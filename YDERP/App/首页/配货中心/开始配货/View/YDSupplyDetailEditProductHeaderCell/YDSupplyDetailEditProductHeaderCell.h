//
//  YDSupplyDetailEditProductHeaderCell.h
//  YDERP
//
//  Created by 何英健 on 2021/10/14.
//

#import <UIKit/UIKit.h>
#import "YDSupplyDetailModel.h"

typedef void(^BlockSupplyEditRefresh)(void);

@interface YDSupplyDetailEditProductHeaderCell : UITableViewCell

+ (instancetype)cellWithTableView:(UITableView *)tableView;

+ (CGFloat)getHeight;

@property (nonatomic, strong) YDSupplyDetailData *data;

@property (nonatomic, copy) BlockSupplyEditRefresh editRefreshBlock;

@end

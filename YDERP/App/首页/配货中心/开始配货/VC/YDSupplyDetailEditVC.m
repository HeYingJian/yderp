//
//  YDSupplyDetailEditVC.m
//  YDERP
//
//  Created by 何英健 on 2021/10/14.
//

#import "YDSupplyDetailEditVC.h"
#import "YDSupplyDetailClientInfoCell.h"
#import "YDSupplyDetailEditProductHeaderCell.h"
#import "YDSupplyDetailEditProductCell.h"
#import "YDSupplyDetailEditPhotoCell.h"
#import "YDSupplyDetailEditModel.h"
#import "YDSupplyDetailVC.h"
#import "YDDeliveryDetailVC.h"

@interface YDSupplyDetailEditVC () <UITableViewDelegate, UITableViewDataSource>

// 用于订单发生变化后，保存旧的数据
@property (nonatomic, strong) YDSupplyDetailData *oldData;

@property (nonatomic, weak) IBOutlet UITableView *tableView;
@property (nonatomic, weak) IBOutlet UILabel *countLabel;
@property (nonatomic, weak) IBOutlet UIButton *confirmBtn;

@property (nonatomic, weak) IBOutlet NSLayoutConstraint *tableViewBottom;

@end

@implementation YDSupplyDetailEditVC

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self setupModifyData];
    
    [self initUI];
    
    [self addNotification];
    
    dispatch_async(dispatch_get_main_queue(),^{
        [self.tableView setContentOffset:self.tableViewOffset];
    });
}

// 准备好修改的参数
- (void)setupModifyData
{
    // 把上次输入的本次配货数保存到新数据中
    for (YDSupplyDetailDataItem *item in self.data.itemList) {
        YDSupplyDetailDataItem *oldItem;
        for (YDSupplyDetailDataItem *sub in self.oldData.itemList) {
            if ([sub.itemId isEqualToString:item.itemId]) {
                oldItem = sub;
                break;
            }
        }
        
        for (YDSupplyDetailDataItemSku *sku in item.skuList) {
            YDSupplyDetailDataItemSku *oldSku;
            for (YDSupplyDetailDataItemSku *sub in oldItem.skuList) {
                if ([sub.skuId isEqualToString:sku.skuId]) {
                    oldSku = sub;
                    break;
                }
            }
            
            if (oldSku) {
                // 输入的本次配货数不能大于库存和欠货数
                NSInteger selectedNum = oldSku.selectedNum;
                NSInteger max = sku.dueNum;
                if (max > sku.stockNum) {
                    max = sku.stockNum;
                }
                if (selectedNum > max) {
                    selectedNum = max;
                }
                sku.selectedNum = selectedNum;
                
            } else {
                sku.selectedNum = 0;
            }
        }
    }
    
    NSMutableArray *array = [NSMutableArray array];
    for (YDSupplyDetailDataMedia *sub in self.data.medias) {
        if (sub.url.length) {
            [array addObject:sub.url];
        }
    }
    self.data.modifyMedias = array;
    self.data.modifyRemark = self.data.remark;
}

- (void)initUI
{
    self.title = @"配货详情";
    [self.navigationItem addDefaultBackButton:self actionMethod:@selector(goBack)];
    
    self.countLabel.font = FONT_NUMBER(20);
    
    [self setupTableView];
    [self setConfirmBtnStatus:NO];
}

- (void)goBack
{
    NSInteger total = [self totalSelectedNum];
    if (total > 0) {
        YDNormalSelectPopView *view = [YDNormalSelectPopView show];
        view.contentLabel.font = [UIFont systemFontOfSize:17];
        view.content = @"离开页面配货数据会消失哦！";
        view.leftBtnText = @"继续配货";
        view.rightBtnText = @"离开";
        YDWeakSelf
        view.tapRightBtnBlock = ^{
            [weakSelf exit];
        };
        
    } else {
        [self exit];
    }
}

- (void)exit
{
    if (self.getTableViewOffsetBlock) {
        self.getTableViewOffsetBlock(self.tableView.contentOffset);
    }
    [self.navigationController popViewControllerAnimated:NO];
}

- (void)setupTableView
{
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    self.tableView.showsVerticalScrollIndicator = NO;
    self.tableView.showsHorizontalScrollIndicator = NO;
    
    if (@available(iOS 11.0, *)) {
        self.tableView.contentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentNever;
        
    } else {
        self.automaticallyAdjustsScrollViewInsets = NO;
    }
}

- (void)refreshUI
{
    [self.tableView reloadData];
    
    NSInteger total = [self totalSelectedNum];
    self.countLabel.text = [NSString stringWithFormat:@"%ld", (long)total];
    
    BOOL enable = (total > 0)? YES : NO;
    [self setConfirmBtnStatus:enable];
}

- (void)setChangeData:(YDSupplyDetailData *)changeData
{
    _changeData = changeData;
    
    self.oldData = self.data;
    self.data = changeData;
    [self setupModifyData];
    
    [self.tableView reloadData];
    [self refreshUI];
}

- (NSInteger)totalSelectedNum
{
    NSInteger total = 0;
    for (YDSupplyDetailDataItem *item in self.data.itemList) {
        for (YDSupplyDetailDataItemSku *sub in item.skuList) {
            total += sub.selectedNum;
        }
    }
    
    return total;
}

- (void)setConfirmBtnStatus:(BOOL)status
{
    self.confirmBtn.userInteractionEnabled = status;
    
    if (status) {
        self.confirmBtn.backgroundColor = ColorFromRGB(0xFF5630);
        
    } else {
        self.confirmBtn.backgroundColor = ColorFromRGB(0xCCCCCC);
    }
}

- (IBAction)tapConfirmBtn:(id)sender
{
    [YDLoadingView showToSuperview:self.view];
    
    NSString *path = URL_SUPPLY_SAVE;
    NSMutableDictionary *param = [NSMutableDictionary dictionary];
    [param setObject:[NSNumber numberWithInteger:self.data.ID] forKey:@"id"];
    [param setObject:self.data.modifyRemark forKey:@"remark"];
    [param setObject:self.data.modifyMedias forKey:@"picUrlList"];
    [param setObject:self.data.version forKey:@"version"];
    
    NSMutableArray *skuArray = [NSMutableArray array];
    for (YDSupplyDetailDataItem *item in self.data.itemList) {
        for (YDSupplyDetailDataItemSku *sub in item.skuList) {
            if (sub.selectedNum > 0) {
                NSMutableDictionary *dict = [NSMutableDictionary dictionary];
                [dict setObject:sub.skuId forKey:@"skuId"];
                [dict setObject:[NSString stringWithFormat:@"%ld", (long)sub.selectedNum] forKey:@"allocNum"];
                [skuArray addObject:dict];
            }
        }
    }
    [param setObject:skuArray forKey:@"skuList"];

    YDWeakSelf
    AppleServerPostCompleteBlock postHandleBlock = ^(BOOL done, id _Nullable object, NSString * _Nullable error) {
        [YDLoadingView hideToSuperview:weakSelf.view];
        
        if (done) {
            YDSupplyDetailEditModel *resp = [YDSupplyDetailEditModel modelWithJSON:object];
            
            if (resp.code == 200) {
                // 通知配货中心更新
                NSMutableDictionary *dict = [NSMutableDictionary dictionary];
                [dict setObject:[NSNumber numberWithInteger:resp.data.tabStatus] forKey:@"tapStatus"];
                PostNotification(REFRESH_SUPPLY_CENTER, dict);
                
                YDNormalSelectPopView *view = [YDNormalSelectPopView show];
                view.contentLabel.font = [UIFont systemFontOfSize:17];
                view.content = resp.data.msg;
                view.leftBtnText = @"去发货";
                view.rightBtnText = @"完成";
                YDWeakSelf
                view.tapLeftBtnBlock = ^{
                    YDDeliveryDetailVC *vc = [[YDDeliveryDetailVC alloc] init];
                    vc.ID = [NSString stringWithFormat:@"%ld", resp.data.deliveryOrderId];
                    [weakSelf.navigationController pushViewController:vc animated:YES];
                    
                    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                        NSMutableArray *array = [NSMutableArray arrayWithArray:weakSelf.navigationController.viewControllers];
                        for (UIViewController *vc in array) {
                            if ([vc isKindOfClass:[YDSupplyDetailVC class]]) {
                                [array removeObject:vc];
                                break;
                            }
                        }
                        for (UIViewController *vc in array) {
                            if ([vc isKindOfClass:[YDSupplyDetailEditVC class]]) {
                                [array removeObject:vc];
                                break;
                            }
                        }
                        weakSelf.navigationController.viewControllers = array;
                    });
                };
                view.tapRightBtnBlock = ^{
                    [weakSelf backToLastController];
                };
                
            } else if (resp.code == 5001 || resp.code == 5002 || resp.code == 9002 || resp.code == 9000) {
                [YDNoticePopView showWithTitle:@"配货期间订单发生修改，请重新核对配货订单！" completion:^{
                    // 订单发生改变，需要刷新
                    PostNotification(REFRESH_SUPPLY_DETAIL, nil);
                }];
                
            } else {
                NSString *msg;
                if (resp.detailMessage.length) {
                    msg = resp.detailMessage;
                    
                } else if (resp.message.length) {
                    msg = resp.message;
                    
                } else {
                    msg = @"网络失败";
                }
                [MBProgressHUD showToastMessage:msg];
            }
            
        } else {
            [MBProgressHUD showToastMessage:@"网络不给力"];
        }
    };
    
    //使用AppleServer发送POST请求
    [AppleServer.shared postWithPath:path Paramters:param CompleteBlock:postHandleBlock];
}

// 返回指定的vc
- (void)backToLastController
{
    if (self.lastController) {
        [self.navigationController popToViewController:self.lastController animated:YES];
    }
}

#pragma mark - UITableView代理

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 4;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (section == 2) {
        return self.data.itemList.count;
    }
    
    return 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == 0) {
        return [YDSupplyDetailClientInfoCell getHeightWithSupplyData:self.data];
        
    } else if (indexPath.section == 1) {
        return [YDSupplyDetailEditProductHeaderCell getHeight];
        
    } else if (indexPath.section == 2) {
        YDSupplyDetailDataItem *data = self.data.itemList[indexPath.row];
        return [YDSupplyDetailEditProductCell getHeightWithData:data];
        
    } else if (indexPath.section == 3) {
        return [YDSupplyDetailEditPhotoCell getHeightWithData:self.data];
    }
    
    return 0.001;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    YDWeakSelf
    if (indexPath.section == 0) {
        YDSupplyDetailClientInfoCell *cell = [YDSupplyDetailClientInfoCell cellWithTableView:tableView];
        cell.supplyData = self.data;

        return cell;
        
    } else if (indexPath.section == 1) {
        YDSupplyDetailEditProductHeaderCell *cell = [YDSupplyDetailEditProductHeaderCell cellWithTableView:tableView];
        cell.data = self.data;
        cell.editRefreshBlock = ^{
            [weakSelf refreshUI];
        };
        
        return cell;
        
    } else if (indexPath.section == 2) {
        YDSupplyDetailEditProductCell *cell = [YDSupplyDetailEditProductCell cellWithTableView:tableView];
        
        cell.oldData = nil;
        YDSupplyDetailDataItem *data = self.data.itemList[indexPath.row];
        for (YDSupplyDetailDataItem *sub in self.oldData.itemList) {
            if ([sub.itemId isEqualToString:data.itemId]) {
                cell.oldData = sub;
            }
        }

        cell.data = data;
        cell.editRefreshBlock = ^{
            [weakSelf refreshUI];
        };

        return cell;
        
    } else if (indexPath.section == 3) {
        YDSupplyDetailEditPhotoCell *cell = [YDSupplyDetailEditPhotoCell cellWithTableView:tableView];
        cell.data = self.data;
        cell.rootVC = self;
        cell.editRefreshBlock = ^{
            [weakSelf refreshUI];
        };
        
        return cell;
    }
    
    return [[UITableViewCell alloc] init];
}

#pragma mark - 键盘高度变化

- (void)addNotification
{
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardShow:) name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardMiss:) name:UIKeyboardWillHideNotification object:nil];
}

// 弹出键盘改变控制器view
- (void)keyboardShow:(NSNotification *)noti
{
    CGFloat keyboardHeight = [noti.userInfo[UIKeyboardFrameEndUserInfoKey] CGRectValue].size.height;
    [UIView animateWithDuration:0.0f animations:^{
        self.tableViewBottom.constant = keyboardHeight - 60 - AppleBottomSafeEreaHeight;
        [self.view layoutIfNeeded];
    }];
}

//回收键盘改变控制器view
- (void)keyboardMiss:(NSNotification *)noti
{
    [UIView animateWithDuration:0.0 animations:^{
        self.tableViewBottom.constant = 0;
        [self.view layoutIfNeeded];
    }];
}

@end

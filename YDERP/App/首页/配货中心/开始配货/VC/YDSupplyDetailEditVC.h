//
//  YDSupplyDetailEditVC.h
//  YDERP
//
//  Created by 何英健 on 2021/10/14.
//

#import <UIKit/UIKit.h>
#import "YDSupplyDetailModel.h"

typedef void(^BlockGetTableViewOffset)(CGPoint tableViewOffset);

@interface YDSupplyDetailEditVC : UIViewController

@property (nonatomic, strong) YDSupplyDetailData *data;
// 用于订单发生变化后，获取到的新数据
@property (nonatomic, strong) YDSupplyDetailData *changeData;

@property (nonatomic, assign) CGPoint tableViewOffset;

@property (nonatomic, copy) BlockGetTableViewOffset getTableViewOffsetBlock;

@property (nonatomic, strong) UIViewController *lastController;

@end

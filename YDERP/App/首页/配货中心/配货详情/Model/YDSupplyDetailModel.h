//
//  YDSupplyDetailModel.h
//  YDERP
//
//  Created by 何英健 on 2021/10/13.
//

#import "YDBaseModel.h"

@interface YDSupplyDetailDataMedia : NSObject

// 实体id
@property (nonatomic, assign) NSInteger entityId;
// 实体类型，10: 商品表 20: 销售单表 30: 配货单表 40: 发货单表
@property (nonatomic, assign) NSInteger entityType;
// 文件类型：1，图片；2，视频
@property (nonatomic, assign) NSInteger fileType;
// url
@property (nonatomic, copy) NSString *url;
// 序号
@property (nonatomic, assign) NSInteger sort;
// ID
@property (nonatomic, copy) NSString *ID;
// 租户ID
@property (nonatomic, copy) NSString *tenantId;
// 创建人
@property (nonatomic, copy) NSString *createPerson;
// 创建时间
@property (nonatomic, copy) NSString *createTime;
// 修改人
@property (nonatomic, copy) NSString *updatePerson;
// 最近一次修改时间
@property (nonatomic, copy) NSString *updateTime;
// 逻辑删除标志
@property (nonatomic, assign) NSInteger dr;

@end

@interface YDSupplyDetailDataItemSku : NSObject

// ID
@property (nonatomic, copy) NSString *ID;
// 颜色
@property (nonatomic, copy) NSString *color;
// 尺寸
@property (nonatomic, copy) NSString *size;
// 颜色ID
@property (nonatomic, copy) NSString *colorId;
// 尺寸ID
@property (nonatomic, copy) NSString *sizeId;
// 编号
@property (nonatomic, copy) NSString *code;
// 可配货数量
@property (nonatomic, assign) NSInteger canAllocNum;
// 库存数量
@property (nonatomic, assign) NSInteger stockNum;
// 配货单id
@property (nonatomic, assign) NSInteger allocOrderId;
// 销售单id
@property (nonatomic, assign) NSInteger orderId;
// 店铺id
@property (nonatomic, copy) NSString *shopId;
// 客户ID
@property (nonatomic, assign) NSInteger clientId;
// 商品id
@property (nonatomic, copy) NSString *itemId;
// skuID
@property (nonatomic, copy) NSString *skuId;
// 商品数量
@property (nonatomic, assign) NSInteger itemNum;
// 欠货数量
@property (nonatomic, assign) NSInteger dueNum;
// 发货数量
@property (nonatomic, assign) NSInteger deliveryNum;
// 已配数量
@property (nonatomic, assign) NSInteger allocNum;
// 版本
@property (nonatomic, assign) NSInteger version;
// 租户ID
@property (nonatomic, copy) NSString *tenantId;
// 创建人
@property (nonatomic, copy) NSString *createPerson;
// 创建时间
@property (nonatomic, copy) NSString *createTime;
// 修改人
@property (nonatomic, copy) NSString *updatePerson;
// 最近一次修改时间
@property (nonatomic, copy) NSString *updateTime;
// 逻辑删除标志
@property (nonatomic, assign) NSInteger dr;

// 本次配货数(自行添加属性)
@property (nonatomic, assign) NSInteger selectedNum;

@end

@interface YDSupplyDetailDataItem : NSObject

// 商品id
@property (nonatomic, copy) NSString *itemId;
// 编号
@property (nonatomic, copy) NSString *code;
// 名称
@property (nonatomic, copy) NSString *name;
// 商品图url
@property (nonatomic, copy) NSString *picUrl;
// 欠货数量
@property (nonatomic, assign) NSInteger dueItemNum;
// 配货数量
@property (nonatomic, assign) NSInteger allocNum;
// sku
@property (nonatomic, strong) NSMutableArray<YDSupplyDetailDataItemSku *> *skuList;

@end

@interface YDSupplyDetailData : NSObject

@property (nonatomic, assign) NSInteger ID;
// 客户
@property (nonatomic, copy) NSString *clientName;
// 订单备注
@property (nonatomic, copy) NSString *orderRemark;
// 订单号
@property (nonatomic, copy) NSString *orderNo;
// 销售单id
@property (nonatomic, assign) NSInteger orderId;
// 店铺id
@property (nonatomic, assign) NSInteger shopId;
// 客户ID
@property (nonatomic, assign) NSInteger clientId;
// 客户地址
@property (nonatomic, copy) NSString *clientAddress;
// 配货状态  10: 待配货 19：重配 20: 部分配货 30：已配齐
@property (nonatomic, assign) NSInteger status;
// 审核时间
@property (nonatomic, copy) NSString *auditTime;
// 审核者id
@property (nonatomic, assign) NSInteger auditorId;
// 总商品数量
@property (nonatomic, assign) NSInteger totalItemNum;
// 配货商品数量
@property (nonatomic, assign) NSInteger allocItemNum;
// 欠货数量
@property (nonatomic, assign) NSInteger dueItemNum;
// 配货员id
@property (nonatomic, assign) NSInteger allocatorId;
// 订单时间
@property (nonatomic, copy) NSString *orderTime;
// 备注
@property (nonatomic, copy) NSString *remark;
// 版本
@property (nonatomic, copy) NSString *version;
// 租户ID
@property (nonatomic, assign) NSInteger tenantId;
// 创建人
@property (nonatomic, copy) NSString *createPerson;
// 创建时间
@property (nonatomic, copy) NSString *createTime;
// 修改人
@property (nonatomic, copy) NSString *updatePerson;
// 最近一次修改时间
@property (nonatomic, copy) NSString *updateTime;
// 逻辑删除标志
@property (nonatomic, assign) NSInteger dr;
// 多媒体信息
@property (nonatomic, strong) NSMutableArray<YDSupplyDetailDataMedia *> *medias;
// 配货商品详情
@property (nonatomic, strong) NSMutableArray<YDSupplyDetailDataItem *> *itemList;

// 修改备注(自行创建)
@property (nonatomic, copy) NSString *modifyRemark;
// 修改图片(自行创建)
@property (nonatomic, strong) NSMutableArray<NSString *> *modifyMedias;

@end

@interface YDSupplyDetailModel : YDBaseModel

@property (nonatomic, strong) YDSupplyDetailData *data;

@end

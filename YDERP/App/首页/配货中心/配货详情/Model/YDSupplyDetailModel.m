//
//  YDSupplyDetailModel.m
//  YDERP
//
//  Created by 何英健 on 2021/10/13.
//

#import "YDSupplyDetailModel.h"

@implementation YDSupplyDetailDataMedia

+ (NSDictionary<NSString *,id> *)modelCustomPropertyMapper
{
    return @{@"ID":@"id"};
}

@end

@implementation YDSupplyDetailDataItemSku

+ (NSDictionary<NSString *,id> *)modelCustomPropertyMapper
{
    return @{@"ID":@"id"};
}

@end

@implementation YDSupplyDetailDataItem

+ (NSDictionary<NSString *,id> *)modelContainerPropertyGenericClass{
    return @{@"skuList" : [YDSupplyDetailDataItemSku class]};
}

@end

@implementation YDSupplyDetailData

+ (NSDictionary<NSString *,id> *)modelCustomPropertyMapper
{
    return @{@"ID":@"id"};
}

+ (NSDictionary<NSString *,id> *)modelContainerPropertyGenericClass{
    return @{@"medias" : [YDSupplyDetailDataMedia class],
             @"itemList" : [YDSupplyDetailDataItem class]
            };
}

@end

@implementation YDSupplyDetailModel

@end

//
//  YDSupplyDetailVC.h
//  YDERP
//
//  Created by 何英健 on 2021/10/12.
//

#import <UIKit/UIKit.h>

@interface YDSupplyDetailVC : UIViewController

@property (nonatomic, copy) NSString *ID;

@property (nonatomic, strong) UIViewController *lastController;

@end

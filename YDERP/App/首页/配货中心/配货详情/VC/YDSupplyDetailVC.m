//
//  YDSupplyDetailVC.m
//  YDERP
//
//  Created by 何英健 on 2021/10/12.
//

#import "YDSupplyDetailVC.h"
#import "YDSupplyDetailModel.h"
#import "YDSupplyDetailClientInfoCell.h"
#import "YDSupplyDetailProductHeaderCell.h"
#import "YDSupplyDetailProductCell.h"
#import "YDSupplyDetailPhotoCell.h"
#import "YDSupplyDetailEditVC.h"

@interface YDSupplyDetailVC () <UITableViewDelegate, UITableViewDataSource>

@property (nonatomic, strong) YDSupplyDetailData *data;

@property (nonatomic, strong) YDSupplyDetailEditVC *editVC;

@property (nonatomic, weak) IBOutlet UITableView *tableView;

@property (nonatomic, weak) IBOutlet NSLayoutConstraint *bottomBtnH;

@end

@implementation YDSupplyDetailVC

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self initUI];
    
    [self addNotification];
    
    [self refreshData:YES];
}

- (void)initUI
{
    self.title = @"配货详情";
    [self.navigationItem addDefaultBackButton:self];
    
    [self setupTableView];
}

- (void)setupTableView
{
    self.tableView.hidden = YES;
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    self.tableView.showsVerticalScrollIndicator = NO;
    self.tableView.showsHorizontalScrollIndicator = NO;
    
    if (@available(iOS 11.0, *)) {
        self.tableView.contentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentNever;
        
    } else {
        self.automaticallyAdjustsScrollViewInsets = NO;
    }
}

- (void)addNotification
{
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(receiveNotiToRefreshData) name:REFRESH_SUPPLY_DETAIL object:nil];
}

- (void)receiveNotiToRefreshData
{
    [self refreshData:YES];
}

- (void)refreshData:(BOOL)showLoading
{
    [YDLoadingView showToSuperview:nil];
    
    YDWeakSelf
    NSString *path = URL_SUPPLY_DETAIL;
    
    NSMutableDictionary *param = [NSMutableDictionary dictionary];
    [param setObject:self.ID forKey:@"id"];

    AppleServerPostCompleteBlock postHandleBlock = ^(BOOL done, id _Nullable object, NSString * _Nullable error) {
        [YDLoadingView hideToSuperview:nil];
        weakSelf.tableView.hidden = NO;
        
        if (done) {
            YDSupplyDetailModel *resp = [YDSupplyDetailModel modelWithJSON:object];
            
            if (resp.code == 200) {
                weakSelf.data = resp.data;
                if (resp.data.status == 30) {
                    self.bottomBtnH.constant = 0;
                    
                } else {
                    self.bottomBtnH.constant = 65;
                }
                [weakSelf.view layoutIfNeeded];
                [weakSelf.tableView reloadData];
                
                weakSelf.editVC.changeData = resp.data;
            
            } else {
                NSString *msg;
                if (resp.detailMessage.length) {
                    msg = resp.detailMessage;
                    
                } else if (resp.message.length) {
                    msg = resp.message;
                    
                } else {
                    msg = @"网络失败";
                }
                [MBProgressHUD showToastMessage:msg];
            }
            
        } else {
            [MBProgressHUD showToastMessage:@"网络不给力"];
        }
    };
    
    //使用AppleServer发送POST请求
    [AppleServer.shared postWithPath:path Paramters:param CompleteBlock:postHandleBlock];
}

- (IBAction)tapEditBtn:(id)sender
{
    self.editVC = [[YDSupplyDetailEditVC alloc] init];
    _editVC.data = self.data;
    _editVC.tableViewOffset = self.tableView.contentOffset;
    YDWeakSelf
    _editVC.getTableViewOffsetBlock = ^(CGPoint tableViewOffset) {
        [weakSelf.tableView setContentOffset:tableViewOffset];
    };
    _editVC.lastController = self.lastController;
    [self.navigationController pushViewController:_editVC animated:NO];
}

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

#pragma mark - UITableView代理

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 4;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (section == 2) {
        return self.data.itemList.count;
    }
    
    return 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == 0) {
        return [YDSupplyDetailClientInfoCell getHeightWithSupplyData:self.data];
        
    } else if (indexPath.section == 1) {
        return [YDSupplyDetailProductHeaderCell getHeight];
        
    } else if (indexPath.section == 2) {
        YDSupplyDetailDataItem *data = self.data.itemList[indexPath.row];
        return [YDSupplyDetailProductCell getHeightWithData:data];
        
    } else if (indexPath.section == 3) {
        return [YDSupplyDetailPhotoCell getHeightWithData:self.data];
    }
    
    return 0.001;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == 0) {
        YDSupplyDetailClientInfoCell *cell = [YDSupplyDetailClientInfoCell cellWithTableView:tableView];
        cell.supplyData = self.data;

        return cell;
        
    } else if (indexPath.section == 1) {
        YDSupplyDetailProductHeaderCell *cell = [YDSupplyDetailProductHeaderCell cellWithTableView:tableView];
        cell.data = self.data;
        
        return cell;
        
    } else if (indexPath.section == 2) {
        YDSupplyDetailProductCell *cell = [YDSupplyDetailProductCell cellWithTableView:tableView];
        YDSupplyDetailDataItem *data = self.data.itemList[indexPath.row];
        cell.data = data;

        return cell;
        
    } else if (indexPath.section == 3) {
        YDSupplyDetailPhotoCell *cell = [YDSupplyDetailPhotoCell cellWithTableView:tableView];
        cell.data = self.data;
        cell.rootVC = self;
        
        return cell;
    }
    
    return [[UITableViewCell alloc] init];
}

@end

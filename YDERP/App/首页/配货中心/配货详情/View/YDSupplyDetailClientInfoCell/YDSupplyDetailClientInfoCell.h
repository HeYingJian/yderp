//
//  YDSupplyDetailClientInfoCell.h
//  YDERP
//
//  Created by 何英健 on 2021/10/13.
//

#import <UIKit/UIKit.h>
#import "YDSupplyDetailModel.h"
#import "YDDeliveryDetailModel.h"

@interface YDSupplyDetailClientInfoCell : UITableViewCell

+ (instancetype)cellWithTableView:(UITableView *)tableView;

+ (CGFloat)getHeightWithSupplyData:(YDSupplyDetailData *)supplyData;

+ (CGFloat)getHeight;

@property (nonatomic, strong) YDSupplyDetailData *supplyData;
@property (nonatomic, strong) YDDeliveryDetailData *deliveryData;

@end

//
//  YDSupplyDetailClientInfoCell.m
//  YDERP
//
//  Created by 何英健 on 2021/10/13.
//

#import "YDSupplyDetailClientInfoCell.h"

@interface YDSupplyDetailClientInfoCell ()

@property (nonatomic, weak) IBOutlet UILabel *nameLabel;
@property (nonatomic, weak) IBOutlet UILabel *addressLabel;
@property (nonatomic, weak) IBOutlet UILabel *remarkLabel;

@end

@implementation YDSupplyDetailClientInfoCell

+ (instancetype)cellWithTableView:(UITableView *)tableView
{
    static NSString *reuseID = @"clientInfoCell";
    YDSupplyDetailClientInfoCell *cell = [tableView dequeueReusableCellWithIdentifier:reuseID];
    if (!cell) {
        cell = [[NSBundle mainBundle] loadNibNamed:@"YDSupplyDetailClientInfoCell" owner:nil options:nil][0];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    
    return cell;
}

+ (CGFloat)getHeightWithSupplyData:(YDSupplyDetailData *)supplyData
{
    if (supplyData.orderRemark.length) {
        return 20 + 15 * 4 + 21 * 3;
        
    } else {
        // 隐藏订单备注
        return 20 + 15 * 3 + 21 * 2;
    }
}

+ (CGFloat)getHeight
{
    return 20 + 15 * 3 + 21 * 2;
}

- (void)setSupplyData:(YDSupplyDetailData *)supplyData
{
    _supplyData = supplyData;
    
    NSString *name = @"临时客户";
    if (supplyData.clientName.length) {
        name = supplyData.clientName;
    }
    self.nameLabel.text = name;
    
    NSString *address = @"--";
    if (supplyData.clientAddress.length) {
        address = supplyData.clientAddress;
    }
    self.addressLabel.text = address;
    
    self.remarkLabel.text = supplyData.orderRemark;
}

- (void)setDeliveryData:(YDDeliveryDetailData *)deliveryData
{
    _deliveryData = deliveryData;
    
    NSString *name = @"临时客户";
    if (deliveryData.clientName.length) {
        name = deliveryData.clientName;
    }
    self.nameLabel.text = name;
    
    NSString *address = @"--";
    if (deliveryData.clientAddress.length) {
        address = deliveryData.clientAddress;
    }
    self.addressLabel.text = address;
}

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end

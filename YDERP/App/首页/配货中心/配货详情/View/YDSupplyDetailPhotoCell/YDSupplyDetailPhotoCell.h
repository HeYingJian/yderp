//
//  YDSupplyDetailPhotoCell.h
//  YDERP
//
//  Created by 何英健 on 2021/10/13.
//

#import <UIKit/UIKit.h>
#import "YDSupplyDetailModel.h"

@interface YDSupplyDetailPhotoCell : UITableViewCell

+ (instancetype)cellWithTableView:(UITableView *)tableView;

+ (CGFloat)getHeightWithData:(YDSupplyDetailData *)data;

@property (nonatomic, strong) YDSupplyDetailData *data;

@property (nonatomic, weak) UIViewController *rootVC;

@end

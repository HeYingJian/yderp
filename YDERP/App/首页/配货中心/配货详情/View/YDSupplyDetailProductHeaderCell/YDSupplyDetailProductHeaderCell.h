//
//  YDSupplyDetailProductHeaderCell.h
//  YDERP
//
//  Created by 何英健 on 2021/10/13.
//

#import <UIKit/UIKit.h>
#import "YDSupplyDetailModel.h"

@interface YDSupplyDetailProductHeaderCell : UITableViewCell

+ (instancetype)cellWithTableView:(UITableView *)tableView;

+ (CGFloat)getHeight;

@property (nonatomic, strong) YDSupplyDetailData *data;

@end


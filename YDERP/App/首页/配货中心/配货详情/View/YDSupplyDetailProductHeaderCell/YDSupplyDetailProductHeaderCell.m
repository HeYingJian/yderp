//
//  YDSupplyDetailProductHeaderCell.m
//  YDERP
//
//  Created by 何英健 on 2021/10/13.
//

#import "YDSupplyDetailProductHeaderCell.h"

@interface YDSupplyDetailProductHeaderCell ()

@property (nonatomic, weak) IBOutlet UILabel *label;
@property (nonatomic, weak) IBOutlet UIImageView *statusImgView;

@end

@implementation YDSupplyDetailProductHeaderCell

+ (instancetype)cellWithTableView:(UITableView *)tableView
{
    static NSString *reuseID = @"productHeaderCell";
    YDSupplyDetailProductHeaderCell *cell = [tableView dequeueReusableCellWithIdentifier:reuseID];
    if (!cell) {
        cell = [[NSBundle mainBundle] loadNibNamed:@"YDSupplyDetailProductHeaderCell" owner:nil options:nil][0];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    
    return cell;
}

+ (CGFloat)getHeight
{
    return 50;
}

- (void)setData:(YDSupplyDetailData *)data
{
    _data = data;
    
    self.label.text = [NSString stringWithFormat:@"共%ld件 已配%ld件", data.totalItemNum, data.allocItemNum];
    
    NSString *imgName;
    switch (data.status) {
        // 待配货
        case 10:
            imgName = @"配货详情待配货";
            break;
        // 重配
        case 19:
            imgName = @"配货详情重配";
            break;
        // 部分配货
        case 20:
            imgName = @"配货详情部分配货";
            break;
        // 已配齐
        case 30:
            imgName = @"配货详情已配货";
            break;
            
        default:
            break;
    }
    [self.statusImgView setImage:[UIImage imageNamed:imgName]];
}

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end

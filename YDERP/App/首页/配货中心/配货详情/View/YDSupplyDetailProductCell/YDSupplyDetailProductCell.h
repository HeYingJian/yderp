//
//  YDSupplyDetailProductCell.h
//  YDERP
//
//  Created by 何英健 on 2021/10/13.
//

#import <UIKit/UIKit.h>
#import "YDSupplyDetailModel.h"

@interface YDSupplyDetailProductCell : UITableViewCell

+ (instancetype)cellWithTableView:(UITableView *)tableView;

+ (CGFloat)getHeightWithData:(YDSupplyDetailDataItem *)data;

@property (nonatomic, strong) YDSupplyDetailDataItem *data;

@end

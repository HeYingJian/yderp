//
//  YDModifyOrderVC.h
//  YDERP
//
//  Created by 何英健 on 2021/8/26.
//

#import <UIKit/UIKit.h>
#import "YDOrderDetailModel.h"

typedef void(^BlockModifyOrderDetailSucceed)(void);

@interface YDModifyOrderVC : UIViewController

@property (nonatomic, strong) YDOrderDetailData *data;

@property (nonatomic, copy) BlockModifyOrderDetailSucceed modifyBlock;

@end

//
//  YDModifyOrderVC.m
//  YDERP
//
//  Created by 何英健 on 2021/8/26.
//

#import "YDModifyOrderVC.h"
#import "YDMakeListUserCell.h"
#import "YDMakeListProductCell.h"
#import "YDSelectCustomerVC.h"
#import "YDSelectAddressVC.h"
#import "YDSearcherCustomerModel.h"
#import "YDSelectProductVC.h"
#import "YDSelectSKUPopView.h"
#import "YDCheckoutVC.h"
#import "YDRefundCheckoutVC.h"

@interface YDModifyOrderVC () <UITableViewDelegate, UITableViewDataSource>

@property (nonatomic, weak) IBOutlet UITableView *tableView;

@property (nonatomic, weak) IBOutlet UILabel *totalPriceLabel;
@property (nonatomic, weak) IBOutlet UILabel *noticeLabel;
@property (nonatomic, weak) IBOutlet UILabel *pieceLabel;
@property (nonatomic, weak) IBOutlet UIButton *accountBtn;

#pragma mark - 保存选择

// 选择客户
@property (nonatomic, strong) YDSearcherCustomerData *selectedCustomer;
// 选择地址
@property (nonatomic, strong) YDCustomerAddressData *selectedAddress;
// 选择商品
@property (nonatomic, strong) NSMutableArray<YDSearcherProductData *> *selectedProductArray;

#pragma mark - 存储初始数据，用于判断订单是否有修改

// 初始商品
@property (nonatomic, strong) NSMutableArray<YDSearcherProductData *> *oldProductArray;

@end

@implementation YDModifyOrderVC

#pragma mark - 懒加载

- (NSMutableArray<YDSearcherProductData *> *)selectedProductArray
{
    if (!_selectedProductArray) {
        _selectedProductArray = [NSMutableArray array];
    }
    
    return _selectedProductArray;
}

- (NSMutableArray<YDSearcherProductData *> *)oldProductArray
{
    if (!_oldProductArray) {
        _oldProductArray = [NSMutableArray array];
    }
    
    return _oldProductArray;
}

#pragma mark - 页面加载

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self initUI];
    
    [self addNotification];
}

- (void)initUI
{
    self.title = @"修改订单";
    [self.navigationItem addDefaultBackButton:self];
    
    self.totalPriceLabel.font = FONT_NUMBER(20);
    
    [self setupTableView];
    [self refreshCountView];
}

- (void)setupTableView
{
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    self.tableView.showsVerticalScrollIndicator = NO;
    self.tableView.showsHorizontalScrollIndicator = NO;
    
    if (@available(iOS 11.0, *)) {
        self.tableView.contentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentNever;
        
    } else {
        self.automaticallyAdjustsScrollViewInsets = NO;
    }
}

- (void)addNotification
{
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(receiveNotiToRefreshData:) name:REFRESH_MODIFY_ORDER object:nil];
}

- (void)receiveNotiToRefreshData:(NSNotification *)notification
{
    NSMutableDictionary *dict = notification.object;
    [self refreshDataWithNotice:[dict objectForKey:@"notice"]];
}

// 刷新底部统计
- (void)refreshCountView
{
    CGFloat totalPrice = 0;
    NSInteger totalPiece = 0;
    for (YDSearcherProductData *sub in self.selectedProductArray) {
        for (YDSearcherSkuGroupData *groupData in sub.skuGroupList) {
            for (YDSearcherSkuList *sku in groupData.skuList) {
                totalPrice += sku.selectedNum * sub.salesPrice;
                totalPiece += sku.selectedNum;
            }
        }
    }
    self.totalPriceLabel.text = [NSString applePrefixPriceWith:totalPrice];
    self.pieceLabel.text = [NSString stringWithFormat:@"共选中%ld件", (long)totalPiece];
    
    NSMutableAttributedString *notice;
    if ([self isCouponError]) {
        notice = [[NSMutableAttributedString alloc] initWithString:@"优惠异常" attributes:
                  @{NSForegroundColorAttributeName:ColorFromRGB(0xFF5630)}];
        
    } else {
        CGFloat resultAmount = [self getResultAmount:YES];
        if (resultAmount < 0) {
            NSString *str = [NSString stringWithFormat:@"(需退款:%@)", [NSString applePrefixPriceWith:-resultAmount]];
            notice = [[NSMutableAttributedString alloc] initWithString:str];
            [notice addAttribute:NSFontAttributeName value:FONT_NUMBER(13) range:NSMakeRange(5, str.length - 6)];
            [notice addAttribute:NSForegroundColorAttributeName value:ColorFromRGB(0xFF5630) range:NSMakeRange(5, str.length - 6)];
        }
    }
    self.noticeLabel.attributedText = notice;
    
    if (self.selectedProductArray.count) {
        self.accountBtn.userInteractionEnabled = YES;
        [self.accountBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        
    } else {
        self.accountBtn.userInteractionEnabled = NO;
        [self.accountBtn setTitleColor:ColorFromRGB(0xFFAA97) forState:UIControlStateNormal];
    }
}

// 重新请求订单数据
- (void)refreshDataWithNotice:(NSString *)notice
{
    YDWeakSelf
    [YDNoticePopView showWithTitle:notice completion:^{
        [weakSelf refreshOrderData];
    }];
}

// 重新获取订单信息
- (void)refreshOrderData
{
    [YDLoadingView showToSuperview:self.view];
    
    YDWeakSelf
    // 请求订单详情数据
    [YDFunction getOrderDetailWithType:0 orderID:[NSString stringWithFormat:@"%ld", (long)self.data.order.ID] completion:^(YDOrderDetailData *data) {
        [YDLoadingView hideToSuperview:weakSelf.view];
        
        // 刷新页面
        weakSelf.data = data;
        [weakSelf.tableView reloadData];
        [weakSelf refreshCountView];
        
        [MBProgressHUD showToastMessage:@"刷新成功，请继续修改订单"];
        
    } refreshBlock:nil faildBlock:^(NSString *msg) {
        [YDLoadingView hideToSuperview:weakSelf.view];
        [MBProgressHUD showToastMessage:msg];
    }];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [self.navigationController setNavigationBarHidden:NO animated:YES];
}

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

#pragma mark - 数据预设

- (void)setData:(YDOrderDetailData *)data
{
    _data = data;
    
    [self setSelectedCustomerWithData];
    [self setSelectedAddressWithData];
    [self setSelectedProductWithData];
}

// 预设客户信息
- (void)setSelectedCustomerWithData
{
    if (self.data.client) {
        self.selectedCustomer = [[YDSearcherCustomerData alloc] init];
        _selectedCustomer.ID = self.data.client.ID;
        _selectedCustomer.balance = self.data.client.balance;
        _selectedCustomer.name = self.data.client.name;
        _selectedCustomer.phone = self.data.client.phone;
    }
}

// 预设地址信息
- (void)setSelectedAddressWithData
{
    if (self.data.order.clientAddress.length) {
        self.selectedAddress = [[YDCustomerAddressData alloc] init];
        _selectedAddress.address = self.data.order.clientAddress;
    }
}

// 预设商品信息
- (void)setSelectedProductWithData
{
    self.selectedProductArray = [self createProductWithData];
    self.oldProductArray = [self createProductWithData];
}

- (NSMutableArray<YDSearcherProductData *> *)createProductWithData
{
    NSMutableArray<YDSearcherProductData *> *array = [NSMutableArray array];
    for (YDOrderDetailDataItems *sub in self.data.orderItems) {
        YDSearcherProductData *product = [[YDSearcherProductData alloc] init];
        product.ID = sub.itemId;
        product.code = sub.code;
        product.name = sub.itemName;
        product.salesPrice = sub.salesPrice;
        product.itemSalesPrice = sub.itemSalesPrice;
        product.wholePrice = sub.wholePrice;
        product.packagePrice = sub.packagePrice;
        YDSearcherPicList *picData = [[YDSearcherPicList alloc] init];
        picData.url = sub.itemUrl;
        product.picList = [NSMutableArray arrayWithObject:picData];
        // 从skuList中获取颜色和尺码
        product.skuGroupList = [YDFunction getSkuGroupArrayFromOrderSkuArray:sub.orderItemSkus];
        [array addObject:product];
    }
    return array;
}

#pragma mark - 订单状态判断

// 计算当前商品总价
- (double)getCurrentItemAmount
{
    double totalPrice = 0;
    for (YDSearcherProductData *sub in self.selectedProductArray) {
        for (YDSearcherSkuGroupData *groupData in sub.skuGroupList) {
            for (YDSearcherSkuList *sku in groupData.skuList) {
                totalPrice += sku.selectedNum * sub.salesPrice;
            }
        }
    }
    
    return totalPrice;
}

// 是否已经修改过订单
- (BOOL)isAlreadyModify
{
    if (self.oldProductArray.count != self.selectedProductArray.count) {
        return YES;
    }
    
    BOOL isExistProduct = NO;
    for (YDSearcherProductData *oldProduct in self.oldProductArray) {
        for (YDSearcherProductData *newProduct in self.selectedProductArray) {
            if (newProduct.ID == oldProduct.ID) {
                isExistProduct = YES;
                
                NSString *oldPrice = NSStringFormat(@"%.2f", oldProduct.salesPrice);
                NSString *newPrice = NSStringFormat(@"%.2f", newProduct.salesPrice);
                if (![oldPrice isEqualToString:newPrice]) {
                    // 商品售价修改了
                    return YES;
                }
                
                NSInteger oldSkuTotal = 0;
                NSMutableArray<YDSearcherSkuList *> *oldSkuGroup = [NSMutableArray array];
                for (YDSearcherSkuGroupData *sub in oldProduct.skuGroupList) {
                    for (YDSearcherSkuList *sku in sub.skuList) {
                        if (sku.selectedNum > 0) {
                            [oldSkuGroup addObject:sku];
                            oldSkuTotal += sku.selectedNum;
                        }
                    }
                }
                
                NSInteger newSkuTotal = 0;
                NSMutableArray<YDSearcherSkuList *> *newSkuGroup = [NSMutableArray array];
                for (YDSearcherSkuGroupData *sub in newProduct.skuGroupList) {
                    for (YDSearcherSkuList *sku in sub.skuList) {
                        if (sku.selectedNum > 0) {
                            [newSkuGroup addObject:sku];
                            newSkuTotal += sku.selectedNum;
                        }
                    }
                }
                
                if (oldSkuGroup.count != newSkuGroup.count) {
                    return YES;
                }
                
                if (oldSkuTotal != newSkuTotal) {
                    return YES;
                }
                
                BOOL isExistSku = NO;
                for (YDSearcherSkuList *oldSku in oldSkuGroup) {
                    for (YDSearcherSkuList *newSku in newSkuGroup) {
                        if ([oldSku.ID isEqualToString:newSku.ID]) {
                            isExistSku = YES;
                            
                            if (oldSku.selectedNum != newSku.selectedNum) {
                                return YES;
                            }
                        }
                    }
                    
                    if (!isExistSku) {
                        return YES;
                    }
                }
            }
        }
        
        if (!isExistProduct) {
            return YES;
        }
    }
    
    return NO;
}

// 是否优惠异常
- (BOOL)isCouponError
{
    CGFloat coupon = self.data.order.couponAmount;
    
    if (coupon >= 0.01) {
        double itemAmount = [self getCurrentItemAmount];
        
        // 商品总额 - 优惠 - 已收款
        if (itemAmount - coupon - self.data.order.payAmount < 0) {
            return YES;
        }
    }
    
    return NO;
}

// 获取订单最终价格 大于0需要补钱 小于0需要退钱 isNeedCoupon-是否计算优惠
- (CGFloat)getResultAmount:(BOOL)isNeedCoupon
{
    CGFloat itemAmount = [self getCurrentItemAmount];
    CGFloat coupon = 0;
    if (isNeedCoupon) {
        coupon = self.data.order.couponAmount;
    }
    
    // 商品总额 - 优惠 - 已收款 + 损耗扣款
    return itemAmount - coupon - self.data.order.payAmount + self.data.order.lossAmount;
}

#pragma mark - 选择页面

// 选择商品
- (void)showSelectPorudct
{
    YDSelectProductVC *vc = [[YDSelectProductVC alloc] init];
    vc.originSelectedArray = self.selectedProductArray;
    YDWeakSelf
    vc.selectedProductBlock = ^(NSMutableArray<YDSearcherProductData *> *array) {
        [weakSelf addProductToSelectedArray:array];
        
        [weakSelf refreshCountView];
        [weakSelf.tableView reloadData];
    };
    [self.navigationController pushViewController:vc animated:YES];
}

// 把新选择的商品添加到已选择数组
- (void)addProductToSelectedArray:(NSMutableArray<YDSearcherProductData *> *)productArray
{
    for (YDSearcherProductData *sub in productArray) {
        // 商品是否已选择
        BOOL isProductExist = NO;
        
        for (YDSearcherProductData *selectedProduct in self.selectedProductArray) {
            if (sub.ID == selectedProduct.ID) {
                isProductExist = YES;
                selectedProduct.salesPrice = sub.salesPrice;
                
                for (YDSearcherSkuGroupData *groupData in sub.skuGroupList) {
                    NSString *colorId = groupData.colorId;
                    // 颜色是否已选择
                    BOOL isColorExist = NO;
                    
                    for (YDSearcherSkuGroupData *selectedGroupData in selectedProduct.skuGroupList) {
                        if ([colorId isEqualToString:selectedGroupData.colorId]) {
                            isColorExist = YES;
                            
                            for (YDSearcherSkuList *sku in groupData.skuList) {
                                // 尺码是否已选择
                                BOOL isSizeExist = NO;
                                
                                for (YDSearcherSkuList *selectedSku in selectedGroupData.skuList) {
                                    if ([sku.ID isEqualToString:selectedSku.ID]) {
                                        isSizeExist = YES;
                                        selectedSku.selectedNum += sku.selectedNum;
                                        selectedSku.salesNum -= sku.selectedNum;
                                    }
                                }
                                
                                if (!isSizeExist) {
                                    // 没有加入过这个尺码
                                    [selectedGroupData.skuList addObject:sku];
                                }
                            }
                        }
                    }
                    
                    if (!isColorExist) {
                        // 没有加入过这个颜色
                        [selectedProduct.skuGroupList addObject:groupData];
                    }
                }
            }
        }
        
        if (!isProductExist) {
            // 没有添加过这个商品，则直接加入
            [self.selectedProductArray addObject:sub];
        }
    }
}

#pragma mark - 点击事件

// 点击结算
- (IBAction)tapAccountBtn:(id)sender
{
    if (![self checkFillData]) {
        return;
    }
    
    if (![self isAlreadyModify]) {
        [MBProgressHUD showToastMessage:@"还没有修改过订单"];
        return;
    }
    
    [self checkOrderVersion];
}

// 检查订单版本号
- (void)checkOrderVersion
{
    [YDLoadingView showToSuperview:self.view];
    
    YDWeakSelf
    [YDFunction checkOrderVersionWithOrderID:[NSString stringWithFormat:@"%ld", (long)self.data.order.ID] version:self.data.order.version completion:^{
        [YDLoadingView hideToSuperview:weakSelf.view];

        if ([self isCouponError]) {
            [self showCouponError];
            return;
        }

        if ([self getResultAmount:YES] < 0) {
            // 需要退款
            [self showRefundCheckout:YES];

        } else {
            // 需要补款
            if (self.selectedCustomer) {
                // 先提交订单，再走checkout
                [self modifyOrder];

            } else {
                // 直接走checkout
                [self showCollectCheckoutWithPorudct:YES isNeedCoupon:YES];
            }
        }
        
    } refreshBlock:^(NSString *msg) {
        [YDLoadingView hideToSuperview:weakSelf.view];
        [weakSelf refreshDataWithNotice:msg];
        
    } faildBlock:^(NSString *msg) {
        [YDLoadingView hideToSuperview:weakSelf.view];
    }];
}

// 修改订单
- (void)modifyOrder
{
    [YDLoadingView showToSuperview:self.view];
    
    NSString *customerID = nil;
    if (self.selectedCustomer) {
        customerID = [NSString stringWithFormat:@"%ld", (long)self.selectedCustomer.ID];
    }
    
    NSString *orderID = [NSString stringWithFormat:@"%ld", (long)self.data.order.ID];
    
    YDWeakSelf
    [YDFunction modifyOrderWithOrderID:orderID version:self.data.order.version productArray:self.selectedProductArray completion:^(YDModifyOrderData *data) {
        [YDLoadingView hideToSuperview:weakSelf.view];
        
        // 更新订单信息
        weakSelf.data.order.version = data.version;
        weakSelf.data.order.ID = data.ID;
        weakSelf.data.order.totalAmount = data.totalAmount;
        weakSelf.data.order.clientId = data.clientId;
        weakSelf.data.order.clientAddress = data.clientAddress;
        weakSelf.data.client.ID = data.clientId;
        weakSelf.data.client.balance = data.balance;
        weakSelf.data.order.orderTime = data.orderTime;
        weakSelf.data.order.payAmount = data.payAmount;
        weakSelf.data.order.couponAmount = data.couponAmount;
        weakSelf.data.order.itemAmount = data.itemAmount;
        weakSelf.data.order.clientAddress = data.clientAddress;
        weakSelf.data.order.lossAmount = data.lossAmount;
        weakSelf.data.order.clientAddress = data.clientAddress;
        weakSelf.data.order.payItemAmount = data.payItemAmount;
        
        // 订单详情刷新
        if (weakSelf.modifyBlock) {
            weakSelf.modifyBlock();
        }
        
        // 弹窗提示
        [weakSelf showOrderModifySucceedNotice];
        
    } refreshBlock:^(NSString *msg) {
        [YDLoadingView hideToSuperview:weakSelf.view];
        [weakSelf refreshDataWithNotice:msg];
        
    } faildBlock:^(NSString *msg) {
        [YDLoadingView hideToSuperview:weakSelf.view];
        [MBProgressHUD showToastMessage:msg];
    }];
}

- (void)showOrderModifySucceedNotice
{
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"订单已修改，是否马上结算" message:nil preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *confirm = [UIAlertAction actionWithTitle:@"确定" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        [self showCollectCheckoutWithPorudct:NO isNeedCoupon:YES];
        
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            NSMutableArray *array = [NSMutableArray arrayWithArray:self.navigationController.viewControllers];
            for (UIViewController *vc in array) {
                if ([vc isKindOfClass:[YDModifyOrderVC class]]) {
                    [array removeObject:vc];
                    break;
                }
            }
            self.navigationController.viewControllers = array;
        });
    }];
    UIAlertAction* cancel = [UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
        [self.navigationController popViewControllerAnimated:YES];
    }];

    [alert addAction:cancel];
    [alert addAction:confirm];

    [self presentViewController:alert animated:YES completion:nil];
}

- (void)showCouponError
{
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"有优惠异常，继续结算优惠将清零" message:nil preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *confirm = [UIAlertAction actionWithTitle:@"确定" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        if ([self getResultAmount:NO] < 0) {
            // 需要退款
            [self showRefundCheckout:NO];
            
        } else {
            // 需要补款
            [self showCollectCheckoutWithPorudct:YES isNeedCoupon:NO];
        }
    }];
    UIAlertAction* cancel = [UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleCancel handler:nil];

    [alert addAction:cancel];
    [alert addAction:confirm];

    [self presentViewController:alert animated:YES completion:nil];
}

// 显示收款checkout isNeedProduct-是否需要传商品信息 isNeedCoupon-是否需要传入优惠
- (void)showCollectCheckoutWithPorudct:(BOOL)isNeedProduct isNeedCoupon:(BOOL)isNeedCoupon
{
    YDCheckoutVC *vc = [[YDCheckoutVC alloc] init];
    vc.type = 2;
    YDWeakSelf
    vc.refreshBlock = ^{
        [weakSelf refreshOrderData];
    };
    vc.model.orderID = [NSString stringWithFormat:@"%ld", (long)self.data.order.ID];
    vc.model.orderTime = self.data.order.orderTime;
    vc.model.version = self.data.order.version;
    if (self.selectedCustomer) {
        vc.model.isCustomerInfoExist = YES;
        vc.model.balance = self.selectedCustomer.balance;
    }
    vc.model.productAmount = [YDCheckoutModel getProductAmountWithProductArray:self.selectedProductArray];
    if (isNeedCoupon) {
        vc.model.couponAmount = NSStringFormat(@"%.2f", self.data.order.couponAmount);
    }
    vc.model.payAmount = NSStringFormat(@"%.2f", self.data.order.payItemAmount);
    if (isNeedProduct) {
        vc.model.productArray = self.selectedProductArray;
    }
    [self.navigationController pushViewController:vc animated:YES];
}

// 显示退款
- (void)showRefundCheckout:(BOOL)isNeedCoupon
{
    YDRefundCheckoutVC *vc = [[YDRefundCheckoutVC alloc] init];
    YDWeakSelf
    vc.refreshBlock = ^{
        [weakSelf refreshOrderData];
    };
    vc.model.orderID = [NSString stringWithFormat:@"%ld", (long)self.data.order.ID];
    vc.model.orderTime = self.data.order.orderTime;
    vc.model.version = self.data.order.version;
    if (self.selectedCustomer) {
        vc.model.isCustomerInfoExist = YES;
    }
    vc.model.productAmount = [YDCheckoutModel getProductAmountWithProductArray:self.selectedProductArray];
    if (isNeedCoupon) {
        vc.model.couponAmount = NSStringFormat(@"%.2f", self.data.order.couponAmount);
    }
    vc.model.payAmount = NSStringFormat(@"%.2f", self.data.order.payAmount);
    vc.model.lossAmount = NSStringFormat(@"%.2f", self.data.order.lossAmount);
    vc.model.productArray = self.selectedProductArray;
    [self.navigationController pushViewController:vc animated:YES];
}

// 检查填写的数据是否完整
- (BOOL)checkFillData
{
    if (!self.selectedProductArray.count) {
        [MBProgressHUD showToastMessage:@"请先添加商品数据"];
        return NO;
    }
    
    return YES;
}

#pragma mark - UITableView代理

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 2;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    switch (section) {
        case 0:
            return 1;
            
        case 1:
            return self.selectedProductArray.count;
            
        default:
            break;
    }
    
    return 0;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == 0) {
        return [YDMakeListUserCell getHeight];
        
    } else {
        YDSearcherProductData *productData = self.selectedProductArray[indexPath.row];
        return [YDMakeListProductCell getHeightWithData:productData];
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    YDWeakSelf
    if (indexPath.section == 0) {
        YDMakeListUserCell *cell = [YDMakeListUserCell cellWithTableView:tableView];
        cell.hideEdit = YES;
        cell.selectedCustomer = self.selectedCustomer;
        cell.selectedAddress = self.selectedAddress;
        cell.showSelectProductBlock = ^{
            // 选择商品
            [weakSelf showSelectPorudct];
        };
        
        return cell;
        
    } else {
        YDMakeListProductCell *cell = [YDMakeListProductCell cellWithTableView:tableView];
        YDSearcherProductData *productData = self.selectedProductArray[indexPath.row];
        cell.data = productData;
        YDWeakSelf
        cell.deleteBlock = ^(YDSearcherProductData *data) {
            [weakSelf showDeleteProduct:data];
        };
        cell.changeDetailBlock = ^{
            [weakSelf.tableView reloadData];
        };
        
        return cell;
    }
}

- (void)showDeleteProduct:(YDSearcherProductData *)data
{
    // 是否有已配货
    for (YDSearcherSkuGroupData *sub in data.skuGroupList) {
        for (YDSearcherSkuList *sku in sub.skuList) {
            if (sku.allocNum > 0) {
                [MBProgressHUD showToastMessage:@"不能删除已配货的商品"];
                return;
            }
        }
    }
    
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"删除商品，将清掉商品的SKU数量" message:nil preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *confirm = [UIAlertAction actionWithTitle:@"确定" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        [self.selectedProductArray removeObject:data];
        [self.tableView reloadData];
        [self refreshCountView];
    }];
    UIAlertAction* cancel = [UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleCancel handler:nil];

    [alert addAction:cancel];
    [alert addAction:confirm];

    [self presentViewController:alert animated:YES completion:nil];
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (self.selectedProductArray.count <= indexPath.row) {
        return;
    }
    
    YDSearcherProductData *productData = self.selectedProductArray[indexPath.row];

    YDWeakSelf
    // 弹出sku选择框
    [YDSelectSKUPopView showWithType:YDSelectSKUTypeModifyOrder data:productData orderID:[NSString stringWithFormat:@"%ld", (long)self.data.order.ID] version:self.data.order.version completion:^(BOOL isSelected) {
        if (!isSelected) {
            // 从选择数组中删除
            [weakSelf.selectedProductArray removeObject:productData];
        }
        
        [weakSelf.tableView reloadData];
        [weakSelf refreshCountView];
    }];
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 0.01;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    return [[UIView alloc] init];
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    return 0.01;
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section
{
    return [[UIView alloc] init];
}


@end

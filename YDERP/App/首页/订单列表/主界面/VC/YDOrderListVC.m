//
//  YDOrderListVC.m
//  YDERP
//
//  Created by 何英健 on 2021/8/23.
//

#import "YDOrderListVC.h"
#import "YDSelectView.h"
#import "YDSearcher.h"
#import "YDOrderListCell.h"
#import "YDOrderDetailVC.h"

@interface YDOrderListVC () <UITextFieldDelegate, UITableViewDelegate, UITableViewDataSource>

// 搜索器
@property (nonatomic, strong) YDSearcher *searcher;

@property (nonatomic, weak) IBOutlet UITextField *searchTextField;

@property (nonatomic, weak) IBOutlet UIView *tagBGView;
@property (nonatomic, strong) YDSelectView *tagView;

@property (nonatomic, weak) IBOutlet UITableView *tableView;

@end

@implementation YDOrderListVC

- (YDSearcher *)searcher
{
    if (!_searcher) {
        _searcher = [YDSearcher createWithType:YDSearchTypeOrder];
        _searcher.pageSize = 10;
    }
    
    return _searcher;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self initUI];
    
    [self addNotification];
}

- (void)initUI
{
    self.title = @"订单";
    [self.navigationItem addDefaultBackButton:self];
    
    [self setupTagView];
    [self setupTableView];
}

- (void)setupTableView
{
    self.tableView.hidden = YES;
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    self.tableView.showsVerticalScrollIndicator = NO;
    self.tableView.showsHorizontalScrollIndicator = NO;
    
    if (@available(iOS 11.0, *)) {
        self.tableView.contentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentNever;
        
    } else {
        self.automaticallyAdjustsScrollViewInsets = NO;
    }

    self.tableView.mj_header = [YDRefreshHeader headerWithRefreshingTarget:self refreshingAction:@selector(pullingRefresh)];
    
    self.tableView.mj_footer = [YDRefreshFooter footerWithRefreshingTarget:self refreshingAction:@selector(loadMore)];
}

- (void)addNotification
{
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(receiveNotiToRefreshData) name:REFRESH_ORDER_LIST object:nil];
}

- (void)receiveNotiToRefreshData
{
    [self refreshData:NO];
}

- (IBAction)tapSearchBtn:(id)sender
{
    [self refreshData:YES];
}

- (void)dismissKeybord
{
    [self.searchTextField resignFirstResponder];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [self.navigationController setNavigationBarHidden:NO animated:YES];
}

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

#pragma mark - 数据请求

- (void)pullingRefresh
{
    [self refreshData:NO];
}

- (void)refreshData:(BOOL)showLoading
{
    [self dismissKeybord];
    
    if (showLoading) {
        [YDLoadingView showToSuperview:self.view];
    }
    
    YDWeakSelf
    if (!self.searcher.completion) {
        self.searcher.completion = ^(BaseVMRefreshType type) {
            weakSelf.tableView.hidden = NO;
            [YDLoadingView hideToSuperview:weakSelf.view];
            [weakSelf.tableView.mj_header endRefreshing];

            if (type == BaseVMRefreshTypeNoMore) {
                [weakSelf.tableView.mj_footer endRefreshingWithNoMoreData];
                
            } else {
                [weakSelf.tableView.mj_footer endRefreshing];
            }
            
            [weakSelf.tableView reloadData];
        };
    }
    
    if (!self.searcher.faildBlock) {
        self.searcher.faildBlock = ^(NSInteger code, NSString *error) {
            weakSelf.tableView.hidden = NO;
            [YDLoadingView hideToSuperview:weakSelf.view];
            [weakSelf.tableView.mj_header endRefreshing];
            [weakSelf.tableView.mj_footer endRefreshing];
            [MBProgressHUD showToastMessage:error];
        };
    }
    
    [self setSearchCondiction];
    [self.searcher refreshData];
}

- (void)loadMore
{
    [self.searcher loadMoreData];
}

#pragma mark - 搜索条件

- (void)setupTagView
{
    NSMutableArray *modelArray = [NSMutableArray array];
    
    for (int i = 0; i < 3; i++) {
        YDSelectViewModel *model = [[YDSelectViewModel alloc] init];
        model.tagWidth = SCREEN_WIDTH / 4;
        
        switch (i) {
            case 0:
            {
                model.title = @"时间";
                model.type = 0;
                [model createImgViewModelWithSelectedImg:@"筛选下箭头选中" unselectedImg:@"筛选下箭头未选中"];
                [model createImgViewModelWithSelectedImg:@"筛选上箭头选中" unselectedImg:@"筛选上箭头未选中"];
            }
                break;
                
            case 1:
            {
                model.title = @"欠款数";
                model.type = 0;
                [model createImgViewModelWithSelectedImg:@"筛选下箭头选中" unselectedImg:@"筛选下箭头未选中"];
                [model createImgViewModelWithSelectedImg:@"筛选上箭头选中" unselectedImg:@"筛选上箭头未选中"];
            }
                break;
                
            case 2:
            {
                model.title = @"欠货数";
                model.type = 0;
                [model createImgViewModelWithSelectedImg:@"筛选下箭头选中" unselectedImg:@"筛选下箭头未选中"];
                [model createImgViewModelWithSelectedImg:@"筛选上箭头选中" unselectedImg:@"筛选上箭头未选中"];
            }
                break;

            default:
                break;
        }
        [modelArray addObject:model];
    }
    
    YDWeakSelf
    self.tagView = [YDSelectView createViewWithType:YDSelectFilterTypeOrder modelArray:modelArray showFilterBlock:^{
        [weakSelf dismissKeybord];
        
    } refreshBlock:^{
        [weakSelf refreshData:YES];
    }];
    [self.tagBGView addSubview:self.tagView];
}

// 设置搜索模型，设置对应搜索条件
- (void)setSearchCondiction
{
    self.searcher.keyword = self.searchTextField.text;
    
    for (int i = 0; i < self.tagView.modelArray.count; i++) {
        YDSelectViewModel *model = self.tagView.modelArray[i];
        
        if (model.isSelected) {
            switch (i) {
                case 0:
                {
                    if (model.type == 0) {
                        self.searcher.sortType = 11;
                        
                    } else {
                        self.searcher.sortType = 10;
                    }
                }
                    break;
                    
                case 1:
                {
                    if (model.type == 0) {
                        self.searcher.sortType = 21;
                        
                    } else {
                        self.searcher.sortType = 20;
                    }
                }
                    break;
                    
                case 2:
                {
                    if (model.type == 0) {
                        self.searcher.sortType = 31;
                        
                    } else {
                        self.searcher.sortType = 30;
                    }
                }
                    break;
                    
                default:
                    break;
            }
        }
    }
    
    if (self.tagView) {
        self.searcher.createStartTime = self.tagView.selectedResultStartTime;
        self.searcher.createEndTime = self.tagView.selectedResultEndTime;
        self.searcher.status = self.tagView.selectedResultOrderStatus;
        self.searcher.arrears = self.tagView.selectedResultArrears;
        self.searcher.dueGoods = self.tagView.selectedResultDueGoods;
    }
}

#pragma mark - UITableView代理

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    NSInteger count = self.searcher.orderDataArray.count;
    if (count) {
        return count;
        
    } else {
        return 1;
    }
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (self.searcher.orderDataArray.count) {
        return [YDOrderListCell getHeight];
        
    } else {
        return [YDNullCell getHeightWithTableView:tableView];
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (self.searcher.orderDataArray.count) {
        YDOrderListCell *cell = [YDOrderListCell cellWithTableView:tableView];
        cell.data = self.searcher.orderDataArray[indexPath.section];
        
        return cell;
        
    } else {
        YDNullCell *cell = [YDNullCell cellWithTableView:tableView];
        cell.content = @"暂无数据";
        return cell;
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 10;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    return [[UIView alloc] init];
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (self.searcher.orderDataArray.count) {
        YDOrderDetailVC *vc = [[YDOrderDetailVC alloc] init];
        YDSearcherOrderData *data = self.searcher.orderDataArray[indexPath.section];
        vc.orderID = [NSString stringWithFormat:@"%ld", (long)data.orderId];
        [self.navigationController pushViewController:vc animated:YES];
    }
}

#pragma mark - UITextFieldDelegate

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    
    [self refreshData:YES];

    return YES;
}

@end

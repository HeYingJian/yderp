//
//  YDOrderListCell.m
//  YDERP
//
//  Created by 何英健 on 2021/8/23.
//

#import "YDOrderListCell.h"

@interface YDOrderListCell ()

@property (nonatomic, weak) IBOutlet UILabel *nameLabel;
@property (nonatomic, weak) IBOutlet UILabel *orderNoLabel;
@property (nonatomic, weak) IBOutlet UIImageView *statusImgView;
@property (nonatomic, weak) IBOutlet UILabel *quantityLabel;
@property (nonatomic, weak) IBOutlet UILabel *alNumLabel;
@property (nonatomic, weak) IBOutlet UILabel *dueGoodsLabel;
@property (nonatomic, weak) IBOutlet UILabel *priceLabel;
@property (nonatomic, weak) IBOutlet UILabel *arrearsLabel;
@property (nonatomic, weak) IBOutlet UILabel *salesNameLabel;
@property (nonatomic, weak) IBOutlet UILabel *timeLabel;
@property (nonatomic, weak) IBOutlet UIImageView *modifyImgView;

@end

@implementation YDOrderListCell

+ (instancetype)cellWithTableView:(UITableView *)tableView
{
    static NSString *reuseID = @"orderListCell";
    YDOrderListCell *cell = [tableView dequeueReusableCellWithIdentifier:reuseID];
    if (!cell) {
        cell = [[NSBundle mainBundle] loadNibNamed:@"YDOrderListCell" owner:nil options:nil][0];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    
    return cell;
}

+ (CGFloat)getHeight
{
    return 162;
}

- (void)setData:(YDSearcherOrderData *)data
{
    _data = data;
    
    NSString *name = @"临时客户";
    if (data.clientName.length) {
        name = data.clientName;   
    }
    if (name.length > 5) {
        name = [NSString stringWithFormat:@"%@…", [name substringToIndex:5]];
    }
    self.nameLabel.text = name;
    self.orderNoLabel.text = [NSString stringWithFormat:@"订单%@", data.orderNo];
    self.quantityLabel.text = [NSString stringWithFormat:@"%ld", (long)data.totalNum];
    
    if (data.status == 10) {
        // 待配货
        self.statusImgView.hidden = NO;
        [self.statusImgView setImage:[UIImage imageNamed:@"待配货"]];
        
    } else if (data.status == 20) {
        // 部分配货
        self.statusImgView.hidden = NO;
        [self.statusImgView setImage:[UIImage imageNamed:@"部分发货"]];
        
    } else if (data.status == 30) {
        // 已配齐
        self.statusImgView.hidden = NO;
        [self.statusImgView setImage:[UIImage imageNamed:@"已配齐"]];
        
    } else {
        self.statusImgView.hidden = YES;
    }
    
    self.alNumLabel.text = [NSString stringWithFormat:@"已配货%ld件 已发货%ld件", (long)data.allocNum, (long)data.deliveryNum];
    NSString *dueGoodsStr = @"";
    if (data.dueNum > 0) {
        dueGoodsStr = [NSString stringWithFormat:@" 欠货%ld件", (long)data.dueNum];
    }
    self.dueGoodsLabel.text = dueGoodsStr;
    
    self.priceLabel.text = [NSString applePrefixPriceWith:data.totalAmount];
    
    if (data.dueAmount >= 0.01) {
        NSString *priceStr = NSStringFormat(@"%.2f", data.dueAmount);
        NSString *str = [NSString stringWithFormat:@"（欠款%@）", priceStr];
        NSMutableAttributedString *attributedStr = [[NSMutableAttributedString alloc] initWithString:str];
        [attributedStr addAttribute:NSFontAttributeName value:FONT_NUMBER(12) range:NSMakeRange(3, priceStr.length)];
        [attributedStr addAttribute:NSForegroundColorAttributeName value:ColorFromRGB(0x2EC28B) range:NSMakeRange(1, str.length - 2)];
        self.arrearsLabel.attributedText = attributedStr;
        
    } else {
        self.arrearsLabel.attributedText = nil;
    }
    
    self.salesNameLabel.text = [NSString stringWithFormat:@"开单人-%@", data.salesName];
    self.timeLabel.text = data.orderTime;
    self.modifyImgView.hidden = (data.modify)? NO : YES;
}

- (void)awakeFromNib
{
    [super awakeFromNib];
    
    self.quantityLabel.font = FONT_NUMBER(15);
    self.priceLabel.font = FONT_NUMBER(15);
    self.timeLabel.font = FONT_NUMBER(12);
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end

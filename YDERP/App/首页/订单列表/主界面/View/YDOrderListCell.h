//
//  YDOrderListCell.h
//  YDERP
//
//  Created by 何英健 on 2021/8/23.
//

#import <UIKit/UIKit.h>

@interface YDOrderListCell : UITableViewCell

+ (instancetype)cellWithTableView:(UITableView *)tableView;

+ (CGFloat)getHeight;

@property (nonatomic, strong) YDSearcherOrderData *data;

@end

//
//  YDOrderDetailVC.h
//  YDERP
//
//  Created by 何英健 on 2021/8/23.
//

#import <UIKit/UIKit.h>

@interface YDOrderDetailVC : UIViewController

// 订单id
@property (nonatomic, copy) NSString *orderID;

@end

//
//  YDOrderDetailVC.m
//  YDERP
//
//  Created by 何英健 on 2021/8/23.
//

#import "YDOrderDetailVC.h"
#import "YDOrderDetailInfoCell.h"
#import "YDOrderDetailPayHistoriesCell.h"
#import "YDOrderDetailProductHeaderCell.h"
#import "YDOrderDetailProductCell.h"
#import "YDOrderDetailPhotoCell.h"
#import "YDCheckoutVC.h"
#import "YDCancelCheckoutVC.h"
#import "YDModifyOrderVC.h"
#import "YDSupplyDetailVC.h"
#import "YDSelectCustomerVC.h"

@interface YDOrderDetailVC () <UITableViewDelegate, UITableViewDataSource>

@property (nonatomic, strong) YDOrderDetailData *data;

@property (nonatomic, weak) IBOutlet UITableView *tableView;

@property (nonatomic, weak) IBOutlet UIButton *printBtn;
@property (nonatomic, weak) IBOutlet UIButton *modifyBtn;

@property (nonatomic, assign) BOOL isNeedRefresh;

@end

@implementation YDOrderDetailVC

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self initUI];
    
    [self refreshData:YES];
}

- (void)initUI
{
    self.title = @"订单详情";
    [self.navigationItem addDefaultBackButton:self];
    
    self.printBtn.layer.borderColor = ColorFromRGB(0xD7D9E0).CGColor;
    self.printBtn.layer.borderWidth = 1;
    self.modifyBtn.layer.borderColor = ColorFromRGB(0xD7D9E0).CGColor;
    self.modifyBtn.layer.borderWidth = 1;
    
    [self setupTableView];
}

- (void)setupTableView
{
    self.tableView.hidden = YES;
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    self.tableView.showsVerticalScrollIndicator = NO;
    self.tableView.showsHorizontalScrollIndicator = NO;
    
    if (@available(iOS 11.0, *)) {
        self.tableView.contentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentNever;
        
    } else {
        self.automaticallyAdjustsScrollViewInsets = NO;
    }
}

- (void)refreshData:(BOOL)isShowLoading
{
    if (isShowLoading) {
        [YDLoadingView showToSuperview:self.view];
    }

    YDWeakSelf
    [YDFunction getOrderDetailWithType:1 orderID:self.orderID completion:^(YDOrderDetailData *data) {
        [YDLoadingView hideToSuperview:weakSelf.view];
        weakSelf.tableView.hidden = NO;
        
        weakSelf.data = data;
        [weakSelf.tableView reloadData];
    
    } refreshBlock:nil faildBlock:^(NSString *msg) {
        [YDLoadingView hideToSuperview:weakSelf.view];
        [MBProgressHUD showToastMessage:msg];
        weakSelf.tableView.hidden = NO;
    }];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [self.navigationController setNavigationBarHidden:NO animated:YES];
    
    if (self.isNeedRefresh) {
        self.isNeedRefresh = NO;
        
        [self refreshData:YES];
    }
}

#pragma mark - 点击事件

// 打印
- (IBAction)tapPrintBtn:(id)sender
{
    [YDPrintModel checkAndPrintWithOrderID:self.orderID nav:self.navigationController];
}

// 修改订单
- (IBAction)tapModifyBtn:(id)sender
{
    self.isNeedRefresh = YES;
    [YDLoadingView showToSuperview:self.view];
    
    YDWeakSelf
    // 请求订单详情数据
    [YDFunction getOrderDetailWithType:0 orderID:self.orderID completion:^(YDOrderDetailData *data) {
        [YDLoadingView hideToSuperview:weakSelf.view];
        
        // 进入修改订单页面
        YDModifyOrderVC *vc = [[YDModifyOrderVC alloc] init];
        vc.data = data;
        vc.modifyBlock = ^{
            [weakSelf refreshData:YES];
        };
        [self.navigationController pushViewController:vc animated:YES];
        
    }  refreshBlock:nil faildBlock:^(NSString *msg) {
        [YDLoadingView hideToSuperview:weakSelf.view];
        [MBProgressHUD showToastMessage:msg];
    }];
}

// 进入配货详情
- (IBAction)tapSupplyBtn:(id)sender
{
    self.isNeedRefresh = YES;
    
    YDSupplyDetailVC *vc = [[YDSupplyDetailVC alloc] init];
    vc.ID = [NSString stringWithFormat:@"%ld", (long)self.data.order.allocOrderId];
    vc.lastController = self;
    [self.navigationController pushViewController:vc animated:YES];
}

// 显示收款
- (void)showCollectMoney
{
    self.isNeedRefresh = YES;
    [YDLoadingView showToSuperview:self.view];
    
    // 请求订单详情，获取客户余额信息
    YDWeakSelf
    [YDFunction getOrderDetailWithType:0 orderID:self.orderID completion:^(YDOrderDetailData *data) {
        [YDLoadingView hideToSuperview:weakSelf.view];
        
        // 显示结算收款
        YDCheckoutVC *vc = [[YDCheckoutVC alloc] init];
        vc.type = 1;
        vc.model.orderID = weakSelf.orderID;
        vc.model.orderTime = weakSelf.data.order.orderTime;
        vc.model.version = weakSelf.data.order.version;
        if (data.client) {
            vc.model.isCustomerInfoExist = YES;
            vc.model.balance = data.client.balance;
        }
        vc.model.productAmount = NSStringFormat(@"%.2f", weakSelf.data.order.itemAmount);
        vc.model.couponAmount = NSStringFormat(@"%.2f", weakSelf.data.order.couponAmount);
        vc.model.payAmount = NSStringFormat(@"%.2f", weakSelf.data.order.payItemAmount);
        [weakSelf.navigationController pushViewController:vc animated:YES];
        
    } refreshBlock:^(NSString *msg) {
        [YDLoadingView hideToSuperview:weakSelf.view];
        [YDNoticePopView showWithTitle:msg completion:^{
            [weakSelf refreshData:YES];
        }];
        
    } faildBlock:^(NSString *msg) {
        [YDLoadingView hideToSuperview:weakSelf.view];
        [MBProgressHUD showToastMessage:msg];
    }];
}

// 删除支付记录
- (void)cancelPayWithPayID:(NSString *)payID amount:(CGFloat)amount
{
//    // 退款金额
//    CGFloat refund = 0;
//    for (YDOrderDetailDataPayHistories *sub in self.data.payHistories) {
//        if (sub.amount < 0) {
//            refund += sub.amount;
//        }
//    }
    
    if (amount - self.data.order.payAmount > 0) {
        // 作废后会产生负的收款记录
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"本单有超收货款的情况，作废本次收款记录，会导致本单收款金额变为负数，是否要作废？" message:nil preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *confirm = [UIAlertAction actionWithTitle:@"确定" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                [self checkClientExistWithPayID:payID amount:amount];
            }];
        UIAlertAction* cancel = [UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleCancel handler:nil];
        [alert addAction:cancel];
        [alert addAction:confirm];
        [self.navigationController presentViewController:alert animated:YES completion:nil];
        
    } else {
        [self checkClientExistWithPayID:payID amount:amount];
    }
}

// 根据用户是否存在，执行删除支付记录的操作
- (void)checkClientExistWithPayID:(NSString *)payID amount:(CGFloat)amount
{
    NSString *notice;
    if (self.data.order.clientName.length) {
        // 有客户
        notice = @"作废后已收款金额将会减去作废的金额重新计算";
        
    } else {
        // 无客户
        notice = @"临时客户作废需要重新结算！";
    }
    
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:notice message:nil preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *confirm = [UIAlertAction actionWithTitle:@"确定" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        
        if (self.data.order.clientName.length) {
            // 直接请求废除接口
            [YDLoadingView showToSuperview:self.view];
            
            YDWeakSelf
            [YDFunction cancelPayHistoryWithPayID:payID completion:^{
                [YDLoadingView hideToSuperview:weakSelf.view];
                [MBProgressHUD showToastMessage:@"作废成功"];
                
                [weakSelf refreshData:YES];
  
            } refreshBlock:^(NSString *msg) {
                [YDLoadingView hideToSuperview:weakSelf.view];
                [YDNoticePopView showWithTitle:msg completion:^{
                    [weakSelf refreshData:YES];
                }];
            
            } faildBlock:^(NSString *msg) {
                [YDLoadingView hideToSuperview:weakSelf.view];
                [MBProgressHUD showToastMessage:msg];
            }];
            
        } else {
            // 进入结算界面
            YDCancelCheckoutVC *vc = [[YDCancelCheckoutVC alloc] init];
            vc.payID = payID;
            vc.model.orderID = self.orderID;
            vc.model.orderTime = self.data.order.orderTime;
            vc.model.version = self.data.order.version;
            vc.model.productAmount = NSStringFormat(@"%.2f", self.data.order.itemAmount);
            vc.model.couponAmount = NSStringFormat(@"%.2f", self.data.order.couponAmount);
            vc.model.payAmount = NSStringFormat(@"%.2f", self.data.order.payItemAmount);
            vc.model.cancelAmount = NSStringFormat(@"%.2f", amount);
            [self.navigationController pushViewController:vc animated:YES];
        }
    }];
    UIAlertAction* cancel = [UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleCancel handler:nil];
    [alert addAction:cancel];
    [alert addAction:confirm];
    [self.navigationController presentViewController:alert animated:YES completion:nil];
}

- (void)selectClientWithClientId:(NSInteger)clientID
{
    [YDLoadingView showToSuperview:self.view];
    
    YDWeakSelf
    NSString *path = URL_ORDER_DETAIL_CLIENT;
    
    NSMutableDictionary *param = [NSMutableDictionary dictionary];
    [param setObject:[NSString stringWithFormat:@"%ld", self.data.order.ID] forKey:@"orderId"];
    [param setObject:[NSString stringWithFormat:@"%ld", clientID] forKey:@"clientId"];

    AppleServerPostCompleteBlock postHandleBlock = ^(BOOL done, id _Nullable object, NSString * _Nullable error) {
        
        if (done) {
            YDSupplyDetailModel *resp = [YDSupplyDetailModel modelWithJSON:object];
            
            if (resp.code == 200) {
                [MBProgressHUD showToastMessage:@"修改成功"];
                [weakSelf refreshData:NO];
                
                // 通知订单列表刷新
                PostNotification(REFRESH_ORDER_LIST, nil);
                
            } else {
                [YDLoadingView hideToSuperview:weakSelf.view];
                
                NSString *msg;
                if (resp.detailMessage.length) {
                    msg = resp.detailMessage;
                    
                } else if (resp.message.length) {
                    msg = resp.message;
                    
                } else {
                    msg = @"网络失败";
                }
                [MBProgressHUD showToastMessage:msg];
            }
            
        } else {
            [YDLoadingView hideToSuperview:weakSelf.view];
            [MBProgressHUD showToastMessage:@"网络不给力"];
        }
    };
    
    //使用AppleServer发送POST请求
    [AppleServer.shared postWithPath:path Paramters:param CompleteBlock:postHandleBlock];
}

#pragma mark - UITableView代理

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 5;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (section == 1) {
        return self.data.payHistories.count;
        
    } else if (section == 3) {
        return self.data.orderItems.count;
    }
    
    return 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == 0) {
        return [YDOrderDetailInfoCell getHeight];
        
    } else if (indexPath.section == 1) {
        return [YDOrderDetailPayHistoriesCell getHeight];
        
    } else if (indexPath.section == 2) {
        return [YDOrderDetailProductHeaderCell getHeight];
        
    } else if (indexPath.section == 3) {
        YDOrderDetailDataItems *productData = self.data.orderItems[indexPath.row];
        return [YDOrderDetailProductCell getHeightWithData:productData];
        
    } else if (indexPath.section == 4) {
        return [YDOrderDetailPhotoCell getHeightWithData:self.data];
    }
    
    return 0.001;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    YDWeakSelf
    if (indexPath.section == 0) {
        YDOrderDetailInfoCell *cell = [YDOrderDetailInfoCell cellWithTableView:tableView];
        cell.data = self.data;
        cell.collectBlock = ^{
            [weakSelf showCollectMoney];
        };
        cell.selectClientBlock = ^{
            YDSelectCustomerVC *vc = [[YDSelectCustomerVC alloc] init];
            vc.isShowNotice = YES;
            vc.selectSucceed = ^(YDSearcherCustomerData *customer) {
                [weakSelf selectClientWithClientId:customer.ID];
            };
            [self.navigationController pushViewController:vc animated:YES];
        };
        
        return cell;
        
    } else if (indexPath.section == 1) {
        YDOrderDetailPayHistoriesCell *cell = [YDOrderDetailPayHistoriesCell cellWithTableView:tableView];
        cell.data = self.data.payHistories[indexPath.row];
        cell.cancelPay = ^(NSString *payID, CGFloat amount) {
            [weakSelf cancelPayWithPayID:payID amount:amount];
        };
        
        return cell;
        
    } else if (indexPath.section == 2) {
        YDOrderDetailProductHeaderCell *cell = [YDOrderDetailProductHeaderCell cellWithTableView:tableView];
        cell.data = self.data;
        
        return cell;
        
    } else if (indexPath.section == 3) {
        YDOrderDetailProductCell *cell = [YDOrderDetailProductCell cellWithTableView:tableView];
        YDOrderDetailDataItems *productData = self.data.orderItems[indexPath.row];
        cell.data = productData;
        
        return cell;
        
    } else if (indexPath.section == 4) {
        YDOrderDetailPhotoCell *cell = [YDOrderDetailPhotoCell cellWithTableView:tableView];
        cell.data = self.data;
        cell.rootVC = self;
        cell.modifyBlock = ^{
            [weakSelf refreshData:YES];
        };
        
        return cell;
    }
    
    return [[UITableViewCell alloc] init];
}

@end

//
//  YDOrderDetailPhotoCell.h
//  YDERP
//
//  Created by 何英健 on 2021/8/25.
//

#import <UIKit/UIKit.h>

typedef void(^BlockOrderDetailModify)(void);

@interface YDOrderDetailPhotoCell : UITableViewCell

+ (instancetype)cellWithTableView:(UITableView *)tableView;

+ (CGFloat)getHeightWithData:(YDOrderDetailData *)data;

@property (nonatomic, strong) YDOrderDetailData *data;

@property (nonatomic, weak) UIViewController *rootVC;

@property (nonatomic, copy) BlockOrderDetailModify modifyBlock;

@end

//
//  YDOrderDetailPhotoCell.m
//  YDERP
//
//  Created by 何英健 on 2021/8/25.
//

#import "YDOrderDetailPhotoCell.h"
#import "YDModifyMessagePopView.h"
#import "YDAddProductColCell.h"

#define ORDER_PHOTO_MAXCOUNT 5

@interface YDOrderDetailPhotoCell () <UITextFieldDelegate, UICollectionViewDelegate, UICollectionViewDataSource, WDImageBrowserDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate>

@property (nonatomic, weak) IBOutlet UITextField *textField;

@property (nonatomic, weak) IBOutlet UICollectionView *collectionView;

@property (nonatomic, weak) IBOutlet UILabel *timeLabel;
@property (nonatomic, weak) IBOutlet UILabel *staffLabel;

@end

@implementation YDOrderDetailPhotoCell

+ (instancetype)cellWithTableView:(UITableView *)tableView
{
    static NSString *reuseID = @"orderDetailPhotoCell";
    YDOrderDetailPhotoCell *cell = [tableView dequeueReusableCellWithIdentifier:reuseID];
    if (!cell) {
        cell = [[NSBundle mainBundle] loadNibNamed:@"YDOrderDetailPhotoCell" owner:nil options:nil][0];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        
        cell.collectionView.showsHorizontalScrollIndicator = NO;
        
        UICollectionViewFlowLayout *layout = [[UICollectionViewFlowLayout alloc] init];
        // cell宽度
        CGFloat cellWidth = [YDOrderDetailPhotoCell getCollectionViewCellBorder];
        // cell与cell之间间距
        layout.minimumLineSpacing = 9;
        layout.itemSize = CGSizeMake(cellWidth, cellWidth);
        cell.collectionView.collectionViewLayout = layout;
        
        [cell.collectionView registerClass:[YDAddProductColCell class] forCellWithReuseIdentifier:@"AddProductColCell"];
    }
    
    return cell;
}

+ (CGFloat)getHeightWithData:(YDOrderDetailData *)data
{
    NSInteger rowNum = (data.medias.count + 1) / 3;
    if (rowNum * 3 < data.medias.count + 1) {
        rowNum++;
    }
    
    CGFloat cellH = [YDOrderDetailPhotoCell getCollectionViewCellBorder];
    CGFloat colViewH = rowNum * cellH + (rowNum - 1) * 9;
    
    return 133 + colViewH + 159;
}

+ (CGFloat)getCollectionViewCellBorder
{
    return (SCREEN_WIDTH - 17 * 2 - 9 * 2) / 3 - 1;
}

- (void)setData:(YDOrderDetailData *)data
{
    _data = data;
    
    self.textField.text = data.order.remark;
    self.timeLabel.text = data.order.orderTime;
    self.staffLabel.text = data.order.salesName;
}

- (void)awakeFromNib
{
    [super awakeFromNib];
    
    NSMutableAttributedString *str = [[NSMutableAttributedString alloc] initWithString:@"请填写备注信息" attributes:
         @{NSForegroundColorAttributeName:ColorFromRGB(0xB7B7B7),
           NSFontAttributeName:[UIFont systemFontOfSize:15]}
         ];
    self.textField.attributedPlaceholder = str;
    
    self.timeLabel.font = FONT_NUMBER(15);
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

#pragma mark - 接口请求

// 保存订单备注
- (void)uploadRemarkWithString:(NSString *)string completion:(void(^)(void))completion
{
    [YDLoadingView showToSuperview:self.rootVC.view];
    
    NSString *path = URL_ORDER_SAVE_REMARK;
    NSMutableDictionary *param = [NSMutableDictionary dictionary];
    [param setObject:[NSNumber numberWithInteger:self.data.order.ID] forKey:@"orderId"];
    NSString *remark = @"";
    if (string.length) {
        remark = string;
    }
    [param setObject:remark forKey:@"remark"];

    YDWeakSelf
    AppleServerPostCompleteBlock postHandleBlock = ^(BOOL done, id _Nullable object, NSString * _Nullable error) {
        [YDLoadingView hideToSuperview:weakSelf.rootVC.view];
        
        if (done) {
            YDBaseModel *resp = [YDBaseModel modelWithJSON:object];
            
            if (resp.code == 200) {
                if (completion) {
                    completion();
                }
            
            } else {
                NSString *msg;
                if (resp.detailMessage.length) {
                    msg = resp.detailMessage;
                    
                } else if (resp.message.length) {
                    msg = resp.message;
                    
                } else {
                    msg = @"网络失败";
                }
                [MBProgressHUD showToastMessage:msg];
            }
            
        } else {
            [MBProgressHUD showToastMessage:@"网络不给力"];
        }
    };
    
    //使用AppleServer发送POST请求
    [AppleServer.shared postWithPath:path Paramters:param CompleteBlock:postHandleBlock];
}

// 订单保存图片
- (void)savePhotoWithUrlArray:(NSMutableArray<NSString *> *)urlArray
{
    if (!urlArray.count) {
        [MBProgressHUD showToastMessage:@"图片不存在"];
        return;
    }
    
    [YDLoadingView showToSuperview:self.rootVC.view];
    
    NSString *path = URL_ORDER_SAVE_PHOTO;
    NSMutableDictionary *param = [NSMutableDictionary dictionary];
    [param setObject:[NSNumber numberWithInteger:self.data.order.ID] forKey:@"orderId"];
    [param setObject:urlArray forKey:@"picUrlList"];

    YDWeakSelf
    AppleServerPostCompleteBlock postHandleBlock = ^(BOOL done, id _Nullable object, NSString * _Nullable error) {
        [YDLoadingView hideToSuperview:weakSelf.rootVC.view];
        
        if (done) {
            YDBaseModel *resp = [YDBaseModel modelWithJSON:object];
            
            if (resp.code == 200) {
                if (self.modifyBlock) {
                    self.modifyBlock();
                }
            
            } else {
                NSString *msg;
                if (resp.detailMessage.length) {
                    msg = resp.detailMessage;
                    
                } else if (resp.message.length) {
                    msg = resp.message;
                    
                } else {
                    msg = @"网络失败";
                }
                [MBProgressHUD showToastMessage:msg];
            }
            
        } else {
            [MBProgressHUD showToastMessage:@"网络不给力"];
        }
    };
    
    //使用AppleServer发送POST请求
    [AppleServer.shared postWithPath:path Paramters:param CompleteBlock:postHandleBlock];
}

// 删除图片
- (void)deletePhotoWithID:(NSString *)ID
{
    [YDLoadingView showToSuperview:self.rootVC.view];
    
    NSString *path = [NSString stringWithFormat:@"%@/%@", URL_ORDER_DEL_PHOTO, ID];
    NSMutableDictionary *param = [NSMutableDictionary dictionary];

    YDWeakSelf
    AppleServerPostCompleteBlock postHandleBlock = ^(BOOL done, id _Nullable object, NSString * _Nullable error) {
        [YDLoadingView hideToSuperview:weakSelf.rootVC.view];
        
        if (done) {
            YDBaseModel *resp = [YDBaseModel modelWithJSON:object];
            
            if (resp.code == 200) {
                if (self.modifyBlock) {
                    self.modifyBlock();
                }
            
            } else {
                NSString *msg;
                if (resp.detailMessage.length) {
                    msg = resp.detailMessage;
                    
                } else if (resp.message.length) {
                    msg = resp.message;
                    
                } else {
                    msg = @"网络失败";
                }
                [MBProgressHUD showToastMessage:msg];
            }
            
        } else {
            [MBProgressHUD showToastMessage:@"网络不给力"];
        }
    };
    
    //使用AppleServer发送POST请求
    [AppleServer.shared postWithPath:path Paramters:param CompleteBlock:postHandleBlock];
}

#pragma mark - UICollectionView 代理

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    if (self.data.medias.count >= ORDER_PHOTO_MAXCOUNT) {
        return self.data.medias.count;
        
    } else {
        return self.data.medias.count + 1;
    }
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    YDAddProductColCell *cell = [YDAddProductColCell cellWithTableView:collectionView indexPath:indexPath];
    if (indexPath.row != 0 || self.data.medias.count >= ORDER_PHOTO_MAXCOUNT) {
        NSInteger photoIndex = 0;
        if (self.data.medias.count >= ORDER_PHOTO_MAXCOUNT) {
            photoIndex = indexPath.row;
            
        } else {
            photoIndex = indexPath.row - 1;
        }
        cell.picUrl = self.data.medias[photoIndex].url;
        
        YDWeakSelf
        cell.tapDeleteBtnBlock = ^(NSString *url) {
            for (YDSearcherPicList *sub in weakSelf.data.medias) {
                if ([sub.url isEqualToString:url]) {
                    [weakSelf deletePhotoWithID:sub.ID];
                    break;
                }
            }
        };
        
        cell.showDeleteBtn = YES;
        
    } else {
        cell.showDeleteBtn = NO;
    }
    
    return cell;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row == 0 && self.data.medias.count < ORDER_PHOTO_MAXCOUNT) {
        [self addPhotos];
        
    } else {
        UICollectionViewCell *cell = [self.collectionView cellForItemAtIndexPath:indexPath];
        
        NSInteger tapIndex;
        if (self.data.medias.count < ORDER_PHOTO_MAXCOUNT) {
            tapIndex = indexPath.row - 1;
            
        } else {
            tapIndex = indexPath.row;
        }
        
        NSMutableArray *photoArray = [NSMutableArray array];
        for (YDSearcherPicList *sub in self.data.medias) {
            if (sub.url.length) {
                [photoArray addObject:sub.url];
            }
        }
        
        // 浏览图片相册
        WDImageBrowser *browser = [[WDImageBrowser alloc] init];
        [browser setupWithDelegate:self tappedIndex:tapIndex imageUrls:photoArray originView:cell];
        [browser showBrowser];
    }
}

#pragma mark - 上传图片

- (void)addPhotos
{
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:nil message:nil preferredStyle:UIAlertControllerStyleActionSheet];
    
    // 需要添加此项设置，否则ipad会崩溃
    alert.popoverPresentationController.sourceView = self.rootVC.view;
    alert.popoverPresentationController.sourceRect = CGRectMake(0, SCREEN_HEIGHT - 70, SCREEN_WIDTH, 70);
    
    //相机
    UIAlertAction *camera = [UIAlertAction actionWithTitle:@"拍照" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        [self showCarmera];
    }];
    
    //相册
    UIAlertAction* library = [UIAlertAction actionWithTitle:@"我的相册" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        [self showPhotoAlbum];
    }];
    
    //取消
    UIAlertAction* cancel = [UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleCancel handler:nil];
    
    [alert addAction:camera];
    [alert addAction:library];
    [alert addAction:cancel];
    
    [self.rootVC presentViewController:alert animated:YES completion:nil];
}

- (void)showCarmera
{
    if (![YDPhotoRight checkHaveCameraRight]) {
        return;
    }
    
    YDImagePickerController *ipvc = [[YDImagePickerController alloc] init];
    ipvc.delegate = self;
    ipvc.sourceType = UIImagePickerControllerSourceTypeCamera;
    ipvc.modalPresentationStyle = UIModalPresentationFullScreen;

    [self.rootVC showViewController:ipvc sender:nil];
}

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary<UIImagePickerControllerInfoKey,id> *)info
{
    UIImage *image = [info objectForKey:UIImagePickerControllerOriginalImage];

    if (image) {
        [self uploadImgArray:[NSArray arrayWithObject:image]];
    }
    
    [picker dismissViewControllerAnimated:YES completion:nil];
}

// 上传图片
- (void)uploadImgArray:(NSArray<UIImage *> *)imgArray
{
    [YDLoadingView showToSuperview:self.rootVC.view];
    
    YDWeakSelf
    [YDFunction uploadImageArray:imgArray type:1 group:1 groupStr:nil parameters:nil sucess:^(NSMutableArray<NSString *> *urlArray) {
        [YDLoadingView hideToSuperview:weakSelf.rootVC.view];
        
        [weakSelf savePhotoWithUrlArray:urlArray];
        
    } failure:^(NSString *msg) {
        [YDLoadingView hideToSuperview:weakSelf.rootVC.view];
        [MBProgressHUD showToastMessage:msg];
    }];
}

// 显示相册选择
- (void)showPhotoAlbum
{
    if (![YDPhotoRight checkHavePhotoLibraryRight]) {
        return;
    }
    
    YDWeakSelf
    TZImagePickerController *imagePickerVc = [[TZImagePickerController alloc] initWithMaxImagesCount:ORDER_PHOTO_MAXCOUNT - self.data.medias.count delegate:nil];
    imagePickerVc.allowPickingOriginalPhoto = NO;
    imagePickerVc.showSelectedIndex = YES;
    imagePickerVc.naviTitleColor = ColorFromRGB(0x0095F7);
    imagePickerVc.barItemTextColor = ColorFromRGB(0x0095F7);
    imagePickerVc.allowPickingVideo = NO;
    imagePickerVc.modalPresentationStyle = UIModalPresentationFullScreen;
    [imagePickerVc setDidFinishPickingPhotosHandle:^(NSArray<UIImage *> *photos, NSArray *assets, BOOL isSelectOriginalPhoto) {
        [weakSelf uploadImgArray:photos];
    }];
    [self.rootVC presentViewController:imagePickerVc animated:YES completion:nil];
}

#pragma mark - UITextFieldDelegate

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    YDWeakSelf
    [YDModifyMessagePopView showWithTitle:@"请输入备注信息" content:self.data.order.remark placeholder:@"最多可填写200字备注" completion:^(NSString *string, BlockModifyMessageStatus statusBlock) {
        if (string.length > 200) {
            [MBProgressHUD showToastMessage:@"最多可填写200字"];
            statusBlock(NO);
            return;
        }
        
        [weakSelf uploadRemarkWithString:string completion:^{
            [MBProgressHUD showToastMessage:@"修改成功"];
            
            if (weakSelf.modifyBlock) {
                weakSelf.modifyBlock();
            }
            
            statusBlock(YES);
        }];
        
    } tapCloseBlock:nil];
    
    return NO;
}

@end

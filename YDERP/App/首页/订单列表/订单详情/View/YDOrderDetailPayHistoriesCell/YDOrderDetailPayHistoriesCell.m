//
//  YDOrderDetailPayHistoriesCell.m
//  YDERP
//
//  Created by 何英健 on 2021/8/25.
//

#import "YDOrderDetailPayHistoriesCell.h"

@interface YDOrderDetailPayHistoriesCell()

@property (nonatomic, weak) IBOutlet UILabel *titleLabel;
@property (nonatomic, weak) IBOutlet UILabel *moneyLabel;
@property (nonatomic, weak) IBOutlet UILabel *timeLabel;
@property (nonatomic, weak) IBOutlet UIButton *btn;

@end

@implementation YDOrderDetailPayHistoriesCell

+ (instancetype)cellWithTableView:(UITableView *)tableView
{
    static NSString *reuseID = @"payHistoriesCell";
    YDOrderDetailPayHistoriesCell *cell = [tableView dequeueReusableCellWithIdentifier:reuseID];
    if (!cell) {
        cell = [[NSBundle mainBundle] loadNibNamed:@"YDOrderDetailPayHistoriesCell" owner:nil options:nil][0];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    
    return cell;
}

+ (CGFloat)getHeight
{
    return 40;
}

- (void)setData:(YDOrderDetailDataPayHistories *)data
{
    _data = data;
    
    self.titleLabel.text = data.payMethodName;
    self.moneyLabel.text = [NSString applePrefixPriceWith:data.amount];
    self.timeLabel.text = data.createTime;
    
    [self setUIWithStatus];
}

// 根据状态改变外观
- (void)setUIWithStatus
{
    if (self.data.status == 1) {
        if (self.data.amount <= 0) {
            // 退款
            self.titleLabel.textColor = ColorFromRGB(0xFF5630);
            self.moneyLabel.textColor = ColorFromRGB(0xFF5630);
            self.timeLabel.textColor = ColorFromRGB(0xFF5630);
            self.btn.hidden = YES;
            
        } else if (self.data.payMethod == 600) {
            // 客户余额支付
            self.titleLabel.textColor = ColorFromRGB(0x222222);
            self.moneyLabel.textColor = ColorFromRGB(0x222222);
            self.timeLabel.textColor = ColorFromRGB(0x222222);
            self.btn.hidden = YES;
            
        } else {
            // 普通支付
            self.titleLabel.textColor = ColorFromRGB(0x222222);
            self.moneyLabel.textColor = ColorFromRGB(0x222222);
            self.timeLabel.textColor = ColorFromRGB(0x222222);
            self.btn.hidden = NO;
            self.btn.userInteractionEnabled = YES;
            self.btn.selected = NO;
        }
        
    } else {
        // 已作废
        self.titleLabel.textColor = ColorFromRGB(0xC7CAD4);
        self.moneyLabel.textColor = ColorFromRGB(0xC7CAD4);
        self.timeLabel.textColor = ColorFromRGB(0xC7CAD4);
        self.btn.hidden = NO;
        self.btn.userInteractionEnabled = NO;
        self.btn.selected = YES;
    }
}

- (IBAction)tapBtn:(id)sender
{
    if (self.cancelPay) {
        self.cancelPay(self.data.ID, self.data.amount);
    }
}

- (void)awakeFromNib
{
    [super awakeFromNib];
    
    self.moneyLabel.font = FONT_NUMBER(15);
    self.timeLabel.font = FONT_NUMBER(15);
    
    [self.btn setTitle:@"作废" forState:UIControlStateNormal];
    [self.btn setTitleColor:ColorFromRGB(0x2D2D2D) forState:UIControlStateNormal];
    
    [self.btn setTitle:@"已废" forState:UIControlStateSelected];
    [self.btn setTitleColor:ColorFromRGB(0xB6B8C1) forState:UIControlStateSelected];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end

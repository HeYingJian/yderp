//
//  YDOrderDetailPayHistoriesCell.h
//  YDERP
//
//  Created by 何英健 on 2021/8/25.
//

#import <UIKit/UIKit.h>

typedef void(^BlockOrderDetailCancelPay)(NSString *payID, CGFloat amount);

@interface YDOrderDetailPayHistoriesCell : UITableViewCell

+ (instancetype)cellWithTableView:(UITableView *)tableView;

+ (CGFloat)getHeight;

@property (nonatomic, strong) YDOrderDetailDataPayHistories *data;

@property (nonatomic, copy) BlockOrderDetailCancelPay cancelPay;

@end

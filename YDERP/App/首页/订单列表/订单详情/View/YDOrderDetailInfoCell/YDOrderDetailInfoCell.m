//
//  YDOrderDetailInfoCell.m
//  YDERP
//
//  Created by 何英健 on 2021/8/25.
//

#import "YDOrderDetailInfoCell.h"

@interface YDOrderDetailInfoCell ()

@property (nonatomic, weak) IBOutlet UIView *nameBGView;
@property (nonatomic, weak) IBOutlet UILabel *nameLabel;
@property (nonatomic, weak) IBOutlet UIImageView *arrowImgView;
@property (nonatomic, weak) IBOutlet UILabel *orderNoLabel;
@property (nonatomic, weak) IBOutlet UILabel *orderMoneyLabel;
@property (nonatomic, weak) IBOutlet UILabel *couponTitleLabel;
@property (nonatomic, weak) IBOutlet UILabel *couponAmountLabel;
@property (nonatomic, weak) IBOutlet UIView *lossAmountView;
@property (nonatomic, weak) IBOutlet UILabel *lossAmountLabel;
@property (nonatomic, weak) IBOutlet UILabel *totalAmountLabel;
@property (nonatomic, weak) IBOutlet UIView *payAmountView;
@property (nonatomic, weak) IBOutlet UILabel *payAmountTitleLabel;
@property (nonatomic, weak) IBOutlet UILabel *payAmountLabel;
@property (nonatomic, weak) IBOutlet UIButton *collectMoneyBtn;
@property (nonatomic, weak) IBOutlet UIImageView *statusImgView;
@property (nonatomic, weak) IBOutlet UIButton *noticeBtn;

@property (nonatomic, weak) IBOutlet NSLayoutConstraint *couponViewLeading;
@property (nonatomic, weak) IBOutlet NSLayoutConstraint *couponTitleLeading;
@property (nonatomic, weak) IBOutlet NSLayoutConstraint *couponAmountTrailing;

@end

@implementation YDOrderDetailInfoCell

+ (instancetype)cellWithTableView:(UITableView *)tableView
{
    static NSString *reuseID = @"orderDetailInfoCell";
    YDOrderDetailInfoCell *cell = [tableView dequeueReusableCellWithIdentifier:reuseID];
    if (!cell) {
        cell = [[NSBundle mainBundle] loadNibNamed:@"YDOrderDetailInfoCell" owner:nil options:nil][0];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    
    return cell;
}

+ (CGFloat)getHeight
{
    return 227 + 13;
}

- (void)setData:(YDOrderDetailData *)data
{
    _data = data;
    
    NSString *name = @"临时客户";
    if (data.order.clientName.length) {
        name = data.order.clientName;
        self.arrowImgView.hidden = YES;
        
    } else {
        self.arrowImgView.hidden = NO;
    }
    self.nameLabel.text = name;
    self.orderNoLabel.text = data.order.orderNo;
    self.orderMoneyLabel.text = [NSString applePrefixPriceWith:data.order.itemAmount];
    
    if (data.order.couponAmount >= 0.01) {
        self.couponViewLeading.constant = 7;
        self.couponTitleLeading.constant = 6;
        self.couponAmountTrailing.constant = 6;
        self.couponTitleLabel.text = @"优惠";
        self.couponAmountLabel.text = [NSString applePrefixPriceWith:data.order.couponAmount];
        
    } else {
        self.couponViewLeading.constant = 2;
        self.couponTitleLeading.constant = 0;
        self.couponAmountTrailing.constant = 0;
        self.couponTitleLabel.text = nil;
        self.couponAmountLabel.text = nil;
    }
    
    if (data.order.lossAmount >= 0.01) {
        self.lossAmountView.hidden = NO;
        self.lossAmountLabel.text = [NSString applePrefixPriceWith:data.order.lossAmount];
        
    } else {
        self.lossAmountView.hidden = YES;
    }
    self.totalAmountLabel.text = [NSString applePrefixPriceWith:data.order.totalAmount];
    
    if (data.order.payAmount < 0) {
        self.payAmountTitleLabel.textColor = ColorFromRGB(0xFF5630);
        self.payAmountLabel.textColor = ColorFromRGB(0xFF5630);
        self.noticeBtn.hidden = NO;
        
    } else {
        self.payAmountTitleLabel.textColor = ColorFromRGB(0x222222);
        self.payAmountLabel.textColor = ColorFromRGB(0x222222);
        self.noticeBtn.hidden = YES;
    }
    self.payAmountLabel.text = [NSString applePrefixPriceWith:data.order.payAmount];
    
    if (data.order.totalAmount - data.order.payAmount > 0) {
        self.collectMoneyBtn.hidden = NO;
        
    } else {
        self.collectMoneyBtn.hidden = YES;
    }
    
    NSString *status;
    if (data.order.status == 10) {
        status = @"订单详情待配货";
        
    } else if (data.order.status == 20) {
        status = @"订单详情部分配货";
        
    } else if (data.order.status == 30) {
        status = @"订单详情已配齐";
    }
    
    if (status.length) {
        self.statusImgView.hidden = NO;
        [self.statusImgView setImage:[UIImage imageNamed:status]];
        
    } else {
        self.statusImgView.hidden = YES;
    }
}

- (IBAction)tapCopyBtn:(id)sender
{
    if (self.data.order.orderNo.length) {
        UIPasteboard *pasteboard = [UIPasteboard generalPasteboard];
        pasteboard.string = self.data.order.orderNo;
        
        [MBProgressHUD showToastMessage:@"复制成功"];
    }
}

- (IBAction)tapCollectMoney:(id)sender
{
    if (self.collectBlock) {
        self.collectBlock();
    }
}

- (IBAction)tapNoticeBtn:(id)sender
{
    [YDNoticePopView showWithTitle:@"作废的收款总金额和大于本单已收款金额；本单超收的货款已被抵扣到其他欠款单。" completion:nil];
}

- (void)tapNameBGView
{
    if (!self.data.order.clientName.length) {
        // 临时客户可以选择
        if (self.selectClientBlock) {
            self.selectClientBlock();
        }
    }
}

- (void)awakeFromNib
{
    [super awakeFromNib];
    
    self.orderNoLabel.font = FONT_NUMBER(15);
    self.orderMoneyLabel.font = FONT_NUMBER(15);
    self.totalAmountLabel.font = FONT_NUMBER(15);
    self.payAmountLabel.font = FONT_NUMBER(15);
    
    UITapGestureRecognizer *tapName = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapNameBGView)];
    [self.nameBGView addGestureRecognizer:tapName];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end

//
//  YDOrderDetailInfoCell.h
//  YDERP
//
//  Created by 何英健 on 2021/8/25.
//

#import <UIKit/UIKit.h>

typedef void(^BlockOrderDetailCollect)(void);
typedef void(^BlockOrderDetailSelectClient)(void);

@interface YDOrderDetailInfoCell : UITableViewCell

+ (instancetype)cellWithTableView:(UITableView *)tableView;

+ (CGFloat)getHeight;

@property (nonatomic, strong) YDOrderDetailData *data;

// 点击收款
@property (nonatomic, copy) BlockOrderDetailCollect collectBlock;
// 选择用户完成
@property (nonatomic, copy) BlockOrderDetailSelectClient selectClientBlock;

@end

//
//  YDOrderDetailProductCell.h
//  YDERP
//
//  Created by 何英健 on 2021/8/25.
//

#import <UIKit/UIKit.h>

@interface YDOrderDetailProductCell : UITableViewCell

+ (instancetype)cellWithTableView:(UITableView *)tableView;

+ (CGFloat)getHeightWithData:(YDOrderDetailDataItems *)data;

@property (nonatomic, strong) YDOrderDetailDataItems *data;

@end

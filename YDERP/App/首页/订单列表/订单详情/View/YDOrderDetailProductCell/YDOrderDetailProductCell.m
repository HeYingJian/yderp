//
//  YDOrderDetailProductCell.m
//  YDERP
//
//  Created by 何英健 on 2021/8/25.
//

#import "YDOrderDetailProductCell.h"

#define ORDER_DETAIL_ROW_HEIGHT 25

@interface YDOrderDetailProductCell ()

@property (nonatomic, weak) IBOutlet UIImageView *productImgView;
@property (nonatomic, weak) IBOutlet UILabel *productNameLabel;
@property (nonatomic, weak) IBOutlet UILabel *priceLabel;
@property (nonatomic, weak) IBOutlet UILabel *quantityLabel;

@property (nonatomic, weak) IBOutlet UIView *detailView;
@property (nonatomic, weak) IBOutlet UIView *colorBGView;
@property (nonatomic, weak) IBOutlet UIView *sizeBGView;
@property (nonatomic, weak) IBOutlet UIView *buyBGView;
@property (nonatomic, weak) IBOutlet UIView *oweBGView;

@end

@implementation YDOrderDetailProductCell

+ (instancetype)cellWithTableView:(UITableView *)tableView
{
    static NSString *reuseID = @"orderDetailProductCell";
    YDOrderDetailProductCell *cell = [tableView dequeueReusableCellWithIdentifier:reuseID];
    if (!cell) {
        cell = [[NSBundle mainBundle] loadNibNamed:@"YDOrderDetailProductCell" owner:nil options:nil][0];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    
    return cell;
}

+ (CGFloat)getHeightWithData:(YDOrderDetailDataItems *)data
{
    // 从skuList中获取颜色和尺码
    NSMutableArray *skuGroupArray = [YDFunction getSkuGroupArrayFromOrderSkuArray:data.orderItemSkus];
    
    NSInteger rowNum = 0;
    for (YDSearcherSkuGroupData *sub in skuGroupArray) {
        rowNum += sub.skuList.count;
    }
    return 159 + rowNum * ORDER_DETAIL_ROW_HEIGHT + 11;
}

- (void)setData:(YDOrderDetailDataItems *)data
{
    _data = data;
    
    if (data.itemUrl.length) {
        [self.productImgView appleSetImageWithUrl:data.itemUrl SprcifyOssImageStyle:@"products_list" HasHD:NO placeholderImage:@"默认商品方形图" completion:nil];
        
    } else {
        [self.productImgView setImage:[UIImage imageNamed:@"没有商品默认图"]];
    }
    
    NSString *codeStr = data.code;
    NSString *productName = data.itemName;
    NSString *str = [NSString stringWithFormat:@"%@#%@", codeStr, productName];
    NSMutableAttributedString *attributedStr = [[NSMutableAttributedString alloc] initWithString:str];
    [attributedStr addAttribute:NSFontAttributeName value:FONT_NUMBER(15) range:NSMakeRange(0, codeStr.length + 1)];
    [attributedStr addAttribute:NSForegroundColorAttributeName value:ColorFromRGB(0xFF5630) range:NSMakeRange(0, codeStr.length + 1)];
    [attributedStr addAttribute:NSFontAttributeName value:[UIFont systemFontOfSize:14.0f] range:NSMakeRange(codeStr.length + 1, productName.length)];
    [attributedStr addAttribute:NSForegroundColorAttributeName value:ColorFromRGB(0x212121) range:NSMakeRange(codeStr.length + 1, productName.length)];
    NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc] init];
    [paragraphStyle setLineSpacing:5];
    [attributedStr addAttribute:NSParagraphStyleAttributeName value:paragraphStyle range:NSMakeRange(0, [str length])];
    self.productNameLabel.attributedText = attributedStr;
    self.productNameLabel.lineBreakMode = NSLineBreakByTruncatingTail;
    
    self.priceLabel.text = [NSString applePrefixPriceWith:data.subtotalAmount];
    self.quantityLabel.text = [NSString stringWithFormat:@"%ld件*%@元", (long)data.itemNum, [NSString appleStandardPriceWith:data.salesPrice]];
    
    // 画表格
    [self refreshDetailView];
}

// 画表格
- (void)refreshDetailView
{
    [self.colorBGView removeAllSubviews];
    [self.sizeBGView removeAllSubviews];
    [self.buyBGView removeAllSubviews];
    [self.oweBGView removeAllSubviews];
    
    // 从skuList中获取颜色和尺码
    NSMutableArray *skuGroupArray = [YDFunction getSkuGroupArrayFromOrderSkuArray:self.data.orderItemSkus];
    
    CGFloat colorOriginY = 0;
    CGFloat sizeOriginY = 0;
    CGFloat colorLabelW = self.colorBGView.width;
    CGFloat sizeLabelW = self.sizeBGView.width;
    CGFloat buyLabelW = (SCREEN_WIDTH - 17 * 2 - colorLabelW - sizeLabelW) / 2;
    
    for (int i = 0; i < skuGroupArray.count; i++) {
        YDSearcherSkuGroupData *sub = skuGroupArray[i];
        
        UILabel *colorLabel = [YDOrderDetailProductCell createLabel];
        [colorLabel setY:colorOriginY];
        [colorLabel setWidth:colorLabelW];
        [colorLabel setHeight:ORDER_DETAIL_ROW_HEIGHT * sub.skuList.count];
        colorLabel.text = sub.color;
        colorLabel.font = [UIFont systemFontOfSize:11 weight:UIFontWeightSemibold];
        [self.colorBGView addSubview:colorLabel];
        
        if (i < skuGroupArray.count - 1) {
            colorOriginY = CGRectGetMaxY(colorLabel.frame);
            
            UIView *lineView = [YDOrderDetailProductCell createLineViewWithLabel:colorLabel];
            [self.colorBGView addSubview:lineView];
        }
        
        for (int m = 0; m < sub.skuList.count; m++) {
            YDSearcherSkuList *sku = sub.skuList[m];
            
            UILabel *sizeLabel = [YDOrderDetailProductCell createLabel];
            [sizeLabel setY:sizeOriginY];
            [sizeLabel setWidth:sizeLabelW];
            [sizeLabel setHeight:ORDER_DETAIL_ROW_HEIGHT];
            sizeLabel.text = sku.size;
            sizeLabel.font = [UIFont systemFontOfSize:11 weight:UIFontWeightSemibold];
            [self.sizeBGView addSubview:sizeLabel];
            
            UILabel *buyLabel = [YDOrderDetailProductCell createLabel];
            [buyLabel setY:sizeOriginY];
            [buyLabel setWidth:buyLabelW];
            [buyLabel setHeight:ORDER_DETAIL_ROW_HEIGHT];
            buyLabel.text = [NSString stringWithFormat:@"%ld", (long)sku.selectedNum];
            buyLabel.font = FONT_NUMBER(13);
            [self.buyBGView addSubview:buyLabel];
            
            UILabel *oweLabel = [YDOrderDetailProductCell createLabel];
            [oweLabel setY:sizeOriginY];
            [oweLabel setWidth:buyLabelW];
            [oweLabel setHeight:ORDER_DETAIL_ROW_HEIGHT];
            oweLabel.text = [NSString stringWithFormat:@"%ld", (long)sku.dueNum];
            oweLabel.font = FONT_NUMBER(13);
            [self.oweBGView addSubview:oweLabel];
            
            if (i < skuGroupArray.count - 1 || m < sub.skuList.count - 1) {
                sizeOriginY = CGRectGetMaxY(sizeLabel.frame);
                
                UIView *sizeLineView = [YDOrderDetailProductCell createLineViewWithLabel:sizeLabel];
                [self.sizeBGView addSubview:sizeLineView];
                
                UIView *buyLineView = [YDOrderDetailProductCell createLineViewWithLabel:buyLabel];
                [self.buyBGView addSubview:buyLineView];
                
                UIView *oweLineView = [YDOrderDetailProductCell createLineViewWithLabel:oweLabel];
                [self.oweBGView addSubview:oweLineView];
            }
        }
    }
}

// 创建标题
+ (UILabel *)createLabel
{
    UILabel *label = [[UILabel alloc] init];
    label.textColor = ColorFromRGB(0x222222);
    label.textAlignment = NSTextAlignmentCenter;

    return label;
}

// 创建分割线
+ (UIView *)createLineViewWithLabel:(UILabel *)label
{
    UIView *lineView = [[UIView alloc] initWithFrame:CGRectMake(0, CGRectGetMaxY(label.frame), label.width, 0.5)];
    lineView.backgroundColor = ColorFromRGB(0xE9E9E9);
    
    return lineView;
}

- (void)awakeFromNib
{
    [super awakeFromNib];
    
    self.priceLabel.font = FONT_NUMBER(17);
    self.detailView.layer.borderColor = ColorFromRGB(0xEBECF0).CGColor;
    self.detailView.layer.borderWidth = 0.5;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end

//
//  YDOrderDetailProductHeaderCell.h
//  YDERP
//
//  Created by 何英健 on 2021/8/25.
//

#import <UIKit/UIKit.h>

@interface YDOrderDetailProductHeaderCell : UITableViewCell

+ (instancetype)cellWithTableView:(UITableView *)tableView;

+ (CGFloat)getHeight;

@property (nonatomic, strong) YDOrderDetailData *data;

@end

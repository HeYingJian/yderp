//
//  YDOrderDetailProductHeaderCell.m
//  YDERP
//
//  Created by 何英健 on 2021/8/25.
//

#import "YDOrderDetailProductHeaderCell.h"

@interface YDOrderDetailProductHeaderCell ()

@property (nonatomic, weak) IBOutlet UILabel *totalLabel;
@property (nonatomic, weak) IBOutlet UILabel *detailLabel;

@end

@implementation YDOrderDetailProductHeaderCell

+ (instancetype)cellWithTableView:(UITableView *)tableView
{
    static NSString *reuseID = @"productHeaderCell";
    YDOrderDetailProductHeaderCell *cell = [tableView dequeueReusableCellWithIdentifier:reuseID];
    if (!cell) {
        cell = [[NSBundle mainBundle] loadNibNamed:@"YDOrderDetailProductHeaderCell" owner:nil options:nil][0];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    
    return cell;
}

+ (CGFloat)getHeight
{
    return 75;
}

- (void)setData:(YDOrderDetailData *)data
{
    _data = data;
    
    self.totalLabel.text = [NSString stringWithFormat:@"共%ld件", data.order.totalNum];
    self.detailLabel.text = [NSString stringWithFormat:@"（已配%ld件/已发货%ld件）", data.order.allocNum, data.order.deliveryNum];
}

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end

//
//  YDHomeVC.m
//  YDERP
//
//  Created by 何英健 on 2021/7/19.
//

#import "YDHomeVC.h"
#import "YDHomeCell.h"
#import "YDHomeModel.h"

@interface YDHomeVC () <UITableViewDelegate, UITableViewDataSource>

// 显示类型
@property (nonatomic, assign) YDHomeType type;

@property (nonatomic, strong) YDHomeData *data;

// 顶部状态栏颜色 0:白色 1:黑色
@property (nonatomic, assign) NSInteger statusBarType;

@property (nonatomic, weak) IBOutlet UITableView *tableView;

// mjheader再上面色块
@property (nonatomic, strong) UIView *mjHeaderTopView;

@property (nonatomic, assign) BOOL isFirstTimeLoading;

@end

@implementation YDHomeVC

#pragma mark - 页面加载

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self initUI];
    
    // 初始化蓝牙打印
    [PTDispatcher share];
}

- (void)initUI
{
    self.isFirstTimeLoading = YES;
    self.type = YDHomeTypeToday;
    
    [self setupTableView];
}

- (void)setupTableView
{
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    self.tableView.showsVerticalScrollIndicator = NO;
    self.tableView.showsHorizontalScrollIndicator = NO;
    
    if (@available(iOS 11.0, *)) {
        self.tableView.contentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentNever;
        
    } else {
        self.automaticallyAdjustsScrollViewInsets = NO;
    }
    
    YDRefreshHeader *header = [YDRefreshHeader headerWithRefreshingTarget:self refreshingAction:@selector(pullingRefreshData)];
    header.msgLabel.textColor = [UIColor whiteColor];
    header.backgroundColor = COLOR_MAIN_BULE;
    header.useWhiteIcon = YES;
    header.automaticallyChangeAlpha = NO;
    self.tableView.mj_header = header;
    
    self.mjHeaderTopView = [[UIView alloc] initWithFrame:CGRectMake(0, -SCREEN_HEIGHT, SCREEN_WIDTH, SCREEN_HEIGHT)];
    _mjHeaderTopView.backgroundColor = COLOR_MAIN_BULE;
    [_tableView insertSubview:_mjHeaderTopView atIndex:0];
}

- (void)pullingRefreshData
{
    [self refreshData:NO];
}

- (void)refreshData:(BOOL)showLoading
{
    if (self.isFirstTimeLoading || showLoading) {
        [YDLoadingView showToSuperview:self.view];
    }
    
    NSString *path = [NSString stringWithFormat:@"%@/%ld", URL_HOME_DATA, self.type];
    NSMutableDictionary *param = [NSMutableDictionary dictionary];

    YDWeakSelf
    AppleServerPostCompleteBlock postHandleBlock = ^(BOOL done, id _Nullable object, NSString * _Nullable error) {
        weakSelf.isFirstTimeLoading = NO;
        
        if (done) {
            YDHomeModel *resp = [YDHomeModel modelWithJSON:object];

            if (resp.code == 200) {
                weakSelf.data = resp.data;
                [weakSelf.tableView reloadData];
                
                // 获取用户当前权限信息
                [YDFunction loadUserLimitOnCompletion:^{
                    [YDLoadingView hideToSuperview:weakSelf.view];
                    [weakSelf.tableView.mj_header endRefreshing];
                    
                } failure:^(NSString *msg) {
                    [YDLoadingView hideToSuperview:weakSelf.view];
                    [MBProgressHUD showToastMessage:msg];
                }];
            
            } else if (resp.code == 3001) {
                [YDLoadingView hideToSuperview:weakSelf.view];
                [weakSelf.tableView.mj_header endRefreshing];
                
                // 重新登录
                [YDFunction logout];
                
                [MBProgressHUD showToastMessage:@"登录信息已失效，请重新登录"];

            } else {
                [YDLoadingView hideToSuperview:weakSelf.view];
                [weakSelf.tableView.mj_header endRefreshing];
                
                if (resp.code == 3009) {
                    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1.0f * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                        // 商户被禁用
                        [YDFunction logout];
                    });
                }
                
                NSString *msg;
                if (resp.detailMessage.length) {
                    msg = resp.detailMessage;
                    
                } else if (resp.message.length) {
                    msg = resp.message;
                    
                } else {
                    msg = @"网络失败";
                }
                [MBProgressHUD showToastMessage:msg];
            }
            
        } else {
            [YDLoadingView hideToSuperview:weakSelf.view];
            [weakSelf.tableView.mj_header endRefreshing];
            [MBProgressHUD showToastMessage:@"网络不给力"];
        }
    };
    
    [AppleServer.shared getWithUrl:path Paramters:param Callback:postHandleBlock];

    // 获取个人信息数据
    [YDFunction loadUserInfoOnCompletion:nil failure:nil];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    self.navigationController.navigationBar.translucent = YES;
    [self.navigationController setNavigationBarHidden:YES animated:NO];
    
    [self refreshData:NO];
    
    // 设置状态栏白色
    self.statusBarType = 0;
    [self setNeedsStatusBarAppearanceUpdate];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    
    // 设置状态栏黑色
    self.statusBarType = 1;
    [self setNeedsStatusBarAppearanceUpdate];
}

- (UIStatusBarStyle)preferredStatusBarStyle
{
    if (self.statusBarType == 0) {
        return UIStatusBarStyleLightContent;
    }
    
    return UIStatusBarStyleDefault;
}

#pragma mark - UITableView代理

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return [YDHomeCell getHeight];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    YDHomeCell *cell = [YDHomeCell cellWithTableView:tableView];
    cell.type = self.type;
    cell.data = self.data;
    YDWeakSelf
    cell.changeStatusBlock = ^{
        if (weakSelf.type == YDHomeTypeToday) {
            weakSelf.type = YDHomeTypeMonth;
            
        } else {
            weakSelf.type = YDHomeTypeToday;
        }
        [weakSelf refreshData:YES];
    };
    return cell;
}

@end

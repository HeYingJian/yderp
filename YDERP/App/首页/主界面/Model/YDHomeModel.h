//
//  YDHomeModel.h
//  YDERP
//
//  Created by 何英健 on 2021/8/29.
//

#import "YDBaseModel.h"

@interface YDHomeData : NSObject

// 店铺名称
@property (nonatomic, copy) NSString *shopName;
// 店铺Id
@property (nonatomic, assign) NSInteger shopId;
// 开单数
@property (nonatomic, assign) NSInteger orderNum;
// 销售额
@property (nonatomic, assign) CGFloat saleAmount;
// 欠货数
@property (nonatomic, assign) NSInteger dueNum;
// 欠款金额
@property (nonatomic, assign) CGFloat dueAmount;
// 挂单数
@property (nonatomic, assign) NSInteger hangupOrderNum;
// 待配货单数
@property (nonatomic, assign) NSInteger allocOrderNum;

@end

@interface YDHomeModel : YDBaseModel

@property (nonatomic, strong) YDHomeData *data;

@end

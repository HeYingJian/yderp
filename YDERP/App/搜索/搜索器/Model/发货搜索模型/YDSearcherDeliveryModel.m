//
//  YDSearcherDeliveryModel.m
//  YDERP
//
//  Created by 何英健 on 2021/10/19.
//

#import "YDSearcherDeliveryModel.h"

@implementation YDSearcherDeliveryData

+ (NSDictionary<NSString *,id> *)modelCustomPropertyMapper
{
    return @{@"ID":@"id"};
}

@end

@implementation YDSearcherDeliveryModelData

+ (NSDictionary<NSString *,id> *)modelContainerPropertyGenericClass{
    return @{@"list" : [YDSearcherDeliveryData class]};
}

@end

@implementation YDSearcherDeliveryModel

@end

//
//  YDSearcherDeliveryModel.h
//  YDERP
//
//  Created by 何英健 on 2021/10/19.
//

#import "YDBaseModel.h"

@interface YDSearcherDeliveryData : NSObject

@property (nonatomic, copy) NSString *ID;
// 客户姓名
@property (nonatomic, copy) NSString *clientName;
// 发货人名
@property (nonatomic, copy) NSString *deliveryUserName;
// 关联订单表的订单号
@property (nonatomic, copy) NSString *orderOrderNo;
// 订单号
@property (nonatomic, copy) NSString *orderNo;
// 订单ID
@property (nonatomic, assign) NSInteger orderId;
// 配货单id
@property (nonatomic, assign) NSInteger allocOrderId;
// 店铺id
@property (nonatomic, assign) NSInteger shopId;
// 客户ID
@property (nonatomic, assign) NSInteger clientId;
// 客户地址
@property (nonatomic, copy) NSString *clientAddress;
// 快递物流商
@property (nonatomic, copy) NSString *expressName;
// 快递单号
@property (nonatomic, copy) NSString *expressNo;
// 订单状态  10: 待发货 20: 已发货 30 驳回重配
@property (nonatomic, assign) NSInteger status;
// 发货数量
@property (nonatomic, assign) NSInteger deliveryNum;
// 发货员id，指向us_user.id
@property (nonatomic, assign) NSInteger deliveryUserId;
// 订单时间
@property (nonatomic, copy) NSString *orderTime;
// 订单备注
@property (nonatomic, copy) NSString *remark;
// 版本号
@property (nonatomic, copy) NSString *version;
// 租户ID
@property (nonatomic, assign) NSInteger tenantId;
// 创建人
@property (nonatomic, copy) NSString *createPerson;
// 创建时间
@property (nonatomic, copy) NSString *createTime;
// 修改人
@property (nonatomic, copy) NSString *updatePerson;
// 最近一次修改时间
@property (nonatomic, copy) NSString *updateTime;
// 逻辑删除标志
@property (nonatomic, assign) NSInteger dr;

@end

@interface YDSearcherDeliveryModelData : NSObject

@property (nonatomic, strong) NSMutableArray<YDSearcherDeliveryData *> *list;

@end

@interface YDSearcherDeliveryModel : YDBaseModel

@property (nonatomic, strong) YDSearcherDeliveryModelData *data;

@end

//
//  YDSearcherSupplyModel.m
//  YDERP
//
//  Created by 何英健 on 2021/10/11.
//

#import "YDSearcherSupplyModel.h"

@implementation YDSearcherSupplyData

+ (NSDictionary<NSString *,id> *)modelCustomPropertyMapper
{
    return @{@"ID":@"id"};
}

+ (NSDictionary<NSString *,id> *)modelContainerPropertyGenericClass{
    return @{@"skuList" : [YDSearcherSkuList class]};
}

@end

@implementation YDSearcherSupplyModelData

+ (NSDictionary<NSString *,id> *)modelContainerPropertyGenericClass{
    return @{@"list" : [YDSearcherSupplyData class]};
}

@end

@implementation YDSearcherSupplyModel

@end

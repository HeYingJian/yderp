//
//  YDSearcherSupplyModel.h
//  YDERP
//
//  Created by 何英健 on 2021/10/11.
//

#import "YDBaseModel.h"
#import "YDSearcherProductModel.h"

@interface YDSearcherSupplyData : NSObject

@property (nonatomic, assign) NSInteger ID;
// 可配货数量
@property (nonatomic, assign) NSInteger canAllocNum;
// 客户名
@property (nonatomic, copy) NSString *clientName;
// 订单号
@property (nonatomic, copy) NSString *orderNo;
// 销售单id
@property (nonatomic, assign) NSInteger orderId;
// 店铺ID
@property (nonatomic, assign) NSInteger shopId;
// 客户id
@property (nonatomic, assign) NSInteger clientId;
// 客户地址
@property (nonatomic, copy) NSString *clientAddress;
// 配货状态  10: 待配货 19：重配 20: 部分配货 30：已配齐
@property (nonatomic, assign) NSInteger status;
// 审核时间
@property (nonatomic, copy) NSString *auditTime;
// 审核者id
@property (nonatomic, assign) NSInteger auditorId;
// 总商品数量
@property (nonatomic, assign) NSInteger totalItemNum;
// 配货商品数量
@property (nonatomic, assign) NSInteger allocItemNum;
// 欠货数量
@property (nonatomic, assign) NSInteger dueItemNum;
// 配货员id
@property (nonatomic, assign) NSInteger allocatorId;
// 订单时间
@property (nonatomic, copy) NSString *orderTime;
// 备注
@property (nonatomic, copy) NSString *remark;
// 版本
@property (nonatomic, assign) NSInteger version;
// 租户ID
@property (nonatomic, assign) NSInteger tenantId;
// 创建人
@property (nonatomic, copy) NSString *createPerson;
// 创建时间
@property (nonatomic, copy) NSString *createTime;
// 修改人
@property (nonatomic, copy) NSString *updatePerson;
// 最近一次修改时间
@property (nonatomic, copy) NSString *updateTime;
// 逻辑删除标志
@property (nonatomic, assign) NSInteger dr;

@property (nonatomic, strong) NSMutableArray<YDSearcherSkuList *> *skuList;

// 是否展开sku详情
@property (nonatomic, assign) BOOL isShowDetail;

@end

@interface YDSearcherSupplyModelData : NSObject

@property (nonatomic, assign) NSInteger total;
@property (nonatomic, strong) NSMutableArray<YDSearcherSupplyData *> *list;

@end

@interface YDSearcherSupplyModel : YDBaseModel

@property (nonatomic, strong) YDSearcherSupplyModelData *data;

@end

//
//  YDSearcherProductModel.h
//  YDERP
//
//  Created by 何英健 on 2021/7/30.
//

#import "YDBaseModel.h"
#import "YDSearcherPropListModel.h"

@interface YDSearcherPicList : NSObject

// 主键
@property (nonatomic, copy) NSString *ID;
// 文件类型：1，图片；2，视频
@property (nonatomic, assign) NSInteger fileType;
// 地址
@property (nonatomic, copy) NSString *url;
// 序号
@property (nonatomic, assign) NSInteger sort;

@end

@interface YDSearcherVideoList : NSObject

// 主键
@property (nonatomic, copy) NSString *ID;
// 文件类型：1，图片；2，视频
@property (nonatomic, assign) NSInteger fileType;
// 地址
@property (nonatomic, copy) NSString *url;
// 序号
@property (nonatomic, assign) NSInteger sort;

@end

@interface YDSearcherSkuList : NSObject

// SKU ID
@property (nonatomic, copy) NSString *ID;
// 商品id
@property (nonatomic, copy) NSString *itemId;
// 店铺id
@property (nonatomic, copy) NSString *shopId;
// 编号
@property (nonatomic, copy) NSString *code;
// 0 停用 1启用
@property (nonatomic, assign) NSInteger status;
// 可售数量，可为负
@property (nonatomic, assign) NSInteger salesNum;
// 库存数量
@property (nonatomic, assign) NSInteger stockNum;
// 欠货数量
@property (nonatomic, assign) NSInteger dueNum;
// 销售价
@property (nonatomic, assign) double salesPrice;
// 成本价
@property (nonatomic, assign) CGFloat basePrice;
// 颜色
@property (nonatomic, copy) NSString *color;
// 尺寸
@property (nonatomic, copy) NSString *size;
// 颜色ID（来自it_prop_value）
@property (nonatomic, copy) NSString *colorId;
// 尺寸ID（来自it_prop_value）
@property (nonatomic, copy) NSString *sizeId;
// 配货单id
@property (nonatomic, assign) NSInteger deliveryOrderId;
// 发货数量
@property (nonatomic, assign) NSInteger deliveryNum;
// 租户ID
@property (nonatomic, copy) NSString *tenantId;
// 创建人
@property (nonatomic, copy) NSString *createPerson;
// 创建时间
@property (nonatomic, copy) NSString *createTime;
// 修改人
@property (nonatomic, copy) NSString *updatePerson;
// 最近一次修改时间
@property (nonatomic, copy) NSString *updateTime;
// 逻辑删除标志
@property (nonatomic, assign) NSInteger dr;
// 已配数量
@property (nonatomic, assign) NSInteger allocNum;
// 下单商品数量
@property (nonatomic, assign) NSInteger orderItemNum;

// 已选择数量(自行添加属性)
@property (nonatomic, assign) NSInteger selectedNum;
// 缓存修改后的选择数量(自行添加属性)
@property (nonatomic, assign) NSInteger cacheSelectedNum;

@end

@interface YDSearcherSkuGroupData : NSObject

// 颜色ID（来自it_prop_value）
@property (nonatomic, copy) NSString *colorId;
// 颜色
@property (nonatomic, copy) NSString *color;
// SKU
@property (nonatomic, strong) NSMutableArray<YDSearcherSkuList *> *skuList;

@end

@interface YDSearcherProductData : NSObject

// 商品ID
@property (nonatomic, assign) NSInteger ID;
// 编码(款号)
@property (nonatomic, copy) NSString *code;
// 名称
@property (nonatomic, copy) NSString *name;
// 别名
@property (nonatomic, copy) NSString *alias;
// 0 停用 1启用
@property (nonatomic, assign) NSInteger status;
// 店铺id
@property (nonatomic, assign) NSInteger shopId;
// 销售价
@property (nonatomic, assign) double salesPrice;
// 成本价
@property (nonatomic, assign) CGFloat basePrice;
// 标准销售价
@property (nonatomic, copy) NSString *itemSalesPrice;
// 批发价
@property (nonatomic, copy) NSString *wholePrice;
// 打包价
@property (nonatomic, copy) NSString *packagePrice;
// 销售量
@property (nonatomic, assign) NSInteger salesNum;
// 库存数量
@property (nonatomic, assign) NSInteger stockNum;
// 累计销售数
@property (nonatomic, assign) NSInteger totalSalesNum;
// 品牌名
@property (nonatomic, copy) NSString *brand;
// 备注
@property (nonatomic, copy) NSString *remark;
// 多媒体信息-图片
@property (nonatomic, strong) NSMutableArray<YDSearcherPicList *> *picList;
// 多媒体信息-视频
@property (nonatomic, strong) NSMutableArray<YDSearcherVideoList *> *videoList;
// 商品属性
@property (nonatomic, strong) NSMutableArray<YDSearcherPropList *> *itemPropList;
// SKU
@property (nonatomic, strong) NSMutableArray<YDSearcherSkuList *> *skuList;
// SKU聚合分组列表
@property (nonatomic, strong) NSMutableArray<YDSearcherSkuGroupData *> *skuGroupList;
// 租户ID
@property (nonatomic, copy) NSString *tenantId;
// 创建人
@property (nonatomic, copy) NSString *createPerson;
// 创建时间
@property (nonatomic, copy) NSString *createTime;
// 修改人
@property (nonatomic, copy) NSString *updatePerson;
// 最近一次修改时间
@property (nonatomic, copy) NSString *updateTime;
// 逻辑删除标志
@property (nonatomic, assign) NSInteger dr;

// 是否展开明细(自行添加属性)
@property (nonatomic, assign) BOOL isShowDetail;
// 缓存修改销售价(自行添加属性)
@property (nonatomic, assign) double cacheSalesPrice;
// 是否已经补全sku信息(针对skuGroupList，某些地方只返回了用户选择的商品sku，没选择的没有返回)
@property (nonatomic, assign) BOOL isLoadSkuGroup;

@end

@interface YDSearcherData : NSObject

@property (nonatomic, strong) NSMutableArray<YDSearcherProductData *> *list;

@end

@interface YDSearcherProductModel : YDBaseModel

@property (nonatomic, strong) YDSearcherData *data;

@end

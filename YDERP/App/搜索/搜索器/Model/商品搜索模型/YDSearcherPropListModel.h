//
//  YDSearcherPropListModel.h
//  YDERP
//
//  Created by 何英健 on 2021/8/2.
//

#import "YDBaseModel.h"

@interface YDSearcherPropValueList : NSObject

// 属性值ID
@property (nonatomic, copy) NSString *propValueId;
// 属性值
@property (nonatomic, copy) NSString *propValue;
// 默认选择标志 1-默认选项
@property (nonatomic, assign) NSInteger defaultFlag;
// 保存是否选择(自己添加)
@property (nonatomic, assign) BOOL isSelected;

@end

@interface YDSearcherPropList : NSObject

// 属性名ID（来自it_prop_name）
@property (nonatomic, copy) NSString *propNameId;
// 属性名（来自it_prop_name）
@property (nonatomic, copy) NSString *propName;
// 属性类型(基础属性显示在“更多信息”栏内) 0-基础属性 10-销售属性-颜色  11-销售属性-尺码
@property (nonatomic, assign) NSInteger type;
// 1 单选 2 多选 3 输入
@property (nonatomic, assign) NSInteger inputType;
// 0 停用 1 启用
@property (nonatomic, assign) NSInteger status;
// 商户是否可以管理对应的属性值：0-可以，1-不可以
@property (nonatomic, assign) NSInteger editAble;
// 排序
@property (nonatomic, assign) NSInteger sort;
// 前端显示用的描述
@property (nonatomic, copy) NSString *statement;
// 商品属性值
@property (nonatomic, strong) NSMutableArray<YDSearcherPropValueList *> *propValueList;
// 保存用户输入文字(自己添加)
@property (nonatomic, copy) NSString *content;

@end

@interface YDSearcherPropListModel : YDBaseModel

// 商品属性
@property (nonatomic, strong) NSMutableArray<YDSearcherPropList *> *data;

@end

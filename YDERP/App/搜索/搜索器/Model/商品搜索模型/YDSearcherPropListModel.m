//
//  YDSearcherPropListModel.m
//  YDERP
//
//  Created by 何英健 on 2021/8/2.
//

#import "YDSearcherPropListModel.h"

@implementation YDSearcherPropValueList

@end

@implementation YDSearcherPropList

+ (NSDictionary<NSString *,id> *)modelContainerPropertyGenericClass{
    return @{@"propValueList" : [YDSearcherPropValueList class]
            };
}

@end

@implementation YDSearcherPropListModel

+ (NSDictionary<NSString *,id> *)modelContainerPropertyGenericClass{
    return @{@"data" : [YDSearcherPropList class]
            };
}

@end

//
//  YDSearcherProductModel.m
//  YDERP
//
//  Created by 何英健 on 2021/7/30.
//

#import "YDSearcherProductModel.h"

@implementation YDSearcherPicList

+ (NSDictionary<NSString *,id> *)modelCustomPropertyMapper
{
    return @{@"ID":@"id"
            };
}

@end

@implementation YDSearcherVideoList

+ (NSDictionary<NSString *,id> *)modelCustomPropertyMapper
{
    return @{@"ID":@"id"
            };
}

@end

@implementation YDSearcherSkuList

+ (NSDictionary<NSString *,id> *)modelCustomPropertyMapper
{
    return @{@"ID":@"id"
            };
}

@end

@implementation YDSearcherSkuGroupData

+ (NSDictionary<NSString *,id> *)modelContainerPropertyGenericClass{
    return @{@"skuList" : [YDSearcherSkuList class]};
}

@end

@implementation YDSearcherProductData

+ (NSDictionary<NSString *,id> *)modelCustomPropertyMapper
{
    return @{@"ID":@"id"
            };
}

+ (NSDictionary<NSString *,id> *)modelContainerPropertyGenericClass{
    return @{@"picList" : [YDSearcherPicList class],
             @"videoList" : [YDSearcherVideoList class],
             @"itemPropList" : [YDSearcherPropList class],
             @"skuList" : [YDSearcherSkuList class],
             @"skuGroupList" : [YDSearcherSkuGroupData class],
            };
}

@end

@implementation YDSearcherData

+ (NSDictionary<NSString *,id> *)modelContainerPropertyGenericClass{
    return @{@"list" : [YDSearcherProductData class]
            };
}

@end

@implementation YDSearcherProductModel

@end

//
//  YDSearcherCustomerModel.h
//  YDERP
//
//  Created by 何英健 on 2021/8/11.
//

#import "YDBaseModel.h"

@interface YDSearcherCustomerDataAddress : NSObject

// 地址id
@property (nonatomic, assign) NSInteger ID;
// 地址
@property (nonatomic, copy) NSString *address;
// 是否默认地址
@property (nonatomic, assign) BOOL defaultFlag;

@end

@interface YDSearcherCustomerData : NSObject

// 客户id
@property (nonatomic, assign) NSInteger ID;
// 客户名称
@property (nonatomic, copy) NSString *name;
// 客户微信
@property (nonatomic, copy) NSString *wechat;
// 客户手机号
@property (nonatomic, copy) NSString *phone;
// 备注
@property (nonatomic, copy) NSString *remark;
// 客户状态，0-停用，1-启用
@property (nonatomic, assign) NSInteger status;
// 最近交易时间
@property (nonatomic, copy) NSString *latestTradeTime;
// 客户余额
@property (nonatomic, assign) CGFloat balance;
// 店铺欠货数
@property (nonatomic, assign) NSInteger goodsOweNum;
// 默认地址信息
@property (nonatomic, strong) YDSearcherCustomerDataAddress *defaultAddress;

@end

@interface YDSearcherCustomerModelData : NSObject

@property (nonatomic, strong) NSMutableArray<YDSearcherCustomerData *> *list;

@end

@interface YDSearcherCustomerModel : YDBaseModel

@property (nonatomic, strong) YDSearcherCustomerModelData *data;

@end

//
//  YDSearcherCustomerModel.m
//  YDERP
//
//  Created by 何英健 on 2021/8/11.
//

#import "YDSearcherCustomerModel.h"

@implementation YDSearcherCustomerDataAddress

+ (NSDictionary<NSString *,id> *)modelCustomPropertyMapper
{
    return @{@"ID":@"id"};
}

@end

@implementation YDSearcherCustomerData

+ (NSDictionary<NSString *,id> *)modelCustomPropertyMapper
{
    return @{@"ID":@"id"};
}

@end

@implementation YDSearcherCustomerModelData

+ (NSDictionary<NSString *,id> *)modelContainerPropertyGenericClass{
    return @{@"list" : [YDSearcherCustomerData class]};
}

@end

@implementation YDSearcherCustomerModel

@end

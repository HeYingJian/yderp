//
//  YDSearcherOrderModel.h
//  YDERP
//
//  Created by 何英健 on 2021/8/23.
//

#import "YDBaseModel.h"
#import "YDOrderDetailModel.h"

@interface YDSearcherOrderData : NSObject

// 订单ID
@property (nonatomic, assign) NSInteger orderId;
// 订单号
@property (nonatomic, copy) NSString *orderNo;
// 员工姓名
@property (nonatomic, copy) NSString *salesName;
// 客户姓名
@property (nonatomic, copy) NSString *clientName;
// 订单状态 0：挂单 10:待配货 20:部分配货 30：已配齐 40：已作废
@property (nonatomic, assign) NSInteger status;
// 订单状态名称
@property (nonatomic, copy) NSString *statusName;
// 总数量
@property (nonatomic, assign) NSInteger totalNum;
// 欠货数量
@property (nonatomic, assign) NSInteger dueNum;
// 应付金额
@property (nonatomic, assign) CGFloat totalAmount;
// 欠款金额
@property (nonatomic, assign) CGFloat dueAmount;
// 开单时间
@property (nonatomic, copy) NSString *orderTime;
// 已发货数量
@property (nonatomic, assign) NSInteger deliveryNum;
// 已配货数量
@property (nonatomic, assign) NSInteger allocNum;
// 是否修改过
@property (nonatomic, assign) BOOL modify;
// 商品明细
@property (nonatomic, strong) NSMutableArray<YDOrderDetailDataItems *> *orderItems;

@end

@interface YDSearcherOrderModelData : NSObject

@property (nonatomic, strong) NSMutableArray<YDSearcherOrderData *> *list;

@end

@interface YDSearcherOrderModel : YDBaseModel

@property (nonatomic, strong) YDSearcherOrderModelData *data;

@end

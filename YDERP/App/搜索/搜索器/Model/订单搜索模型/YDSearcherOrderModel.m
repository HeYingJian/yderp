//
//  YDSearcherOrderModel.m
//  YDERP
//
//  Created by 何英健 on 2021/8/23.
//

#import "YDSearcherOrderModel.h"

@implementation YDSearcherOrderData

+ (NSDictionary<NSString *,id> *)modelContainerPropertyGenericClass{
    return @{@"orderItems" : [YDOrderDetailDataItems class]};
}

@end

@implementation YDSearcherOrderModelData

+ (NSDictionary<NSString *,id> *)modelContainerPropertyGenericClass{
    return @{@"list" : [YDSearcherOrderData class]};
}

@end

@implementation YDSearcherOrderModel

@end

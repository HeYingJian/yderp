//
//  YDSearcher.m
//  YDERP
//
//  Created by 何英健 on 2021/7/27.
//

#import "YDSearcher.h"

#define SEARCH_MAX_SIZE 20

@interface YDSearcher ()

@property (nonatomic, assign) YDSearchType type;

@end

@implementation YDSearcher

#pragma mark - 懒加载

- (NSMutableArray<YDSearcherProductData *> *)productDataArray
{
    if (!_productDataArray) {
        _productDataArray = [NSMutableArray array];
    }
    
    return _productDataArray;
}

- (NSMutableArray<YDSearcherCustomerData *> *)customerDataArray
{
    if (!_customerDataArray) {
        _customerDataArray = [NSMutableArray array];
    }
    
    return _customerDataArray;
}

- (NSMutableArray<YDSearcherOrderData *> *)orderDataArray
{
    if (!_orderDataArray) {
        _orderDataArray = [NSMutableArray array];
    }
    
    return _orderDataArray;
}

- (NSMutableArray<YDSearcherSupplyData *> *)supplyDataArray
{
    if (!_supplyDataArray) {
        _supplyDataArray = [NSMutableArray array];
    }
    
    return _supplyDataArray;
}

- (NSMutableArray<YDSearcherDeliveryData *> *)deliveryDataArray
{
    if (!_deliveryDataArray) {
        _deliveryDataArray = [NSMutableArray array];
    }
    
    return _deliveryDataArray;
}

#pragma mark - 初始化

+ (YDSearcher *)createWithType:(YDSearchType)type
{
    YDSearcher *searcher = [[YDSearcher alloc] init];
    searcher.type = type;
    if (type == YDSearchTypeProduct || type == YDSearchTypeCustomer) {
        // 默认启用
        searcher.status = 1;
        
    } else if (type == YDSearchTypeOrder || type == YDSearchTypeSupply || type == YDSearchTypeDelivery) {
        // 默认不传
        searcher.status = -1;
    }
    
    // 默认按时间倒序
    if (type == YDSearchTypeProduct || type == YDSearchTypeOrder || type == YDSearchTypeDelivery) {
        searcher.sortType = 11;
        
    } else if (type == YDSearchTypeCustomer) {
        searcher.sortType = 21;
        
    } else if (type == YDSearchTypeSupply) {
        searcher.sortType = 10;
    }
    
    // 默认不传
    searcher.moneyOwe = -1;
    // 默认不传
    searcher.goodsOwe = -1;
    // 默认不传
    searcher.arrears = -1;
    // 默认不传
    searcher.dueGoods = -1;
    
    searcher.pageNum = 0;
    searcher.pageSize = 0;
    
    return searcher;
}

#pragma mark - 数据请求

- (void)refreshData
{
    self.pageNum = 0;
    self.total = 0;
    self.productDataArray = nil;
    self.customerDataArray = nil;
    self.orderDataArray = nil;
    self.supplyDataArray = nil;
    self.deliveryDataArray = nil;
    
    [self search];
}

- (void)loadMoreData
{
    [self search];
}

- (void)search
{
    NSMutableDictionary *param = [NSMutableDictionary dictionary];
    
    NSString *path;
    if (self.type == YDSearchTypeProduct) {
        path = URL_SEARCH_PRODUCT;
        
        if (self.isQuerySku) {
            [param setObject:[NSNumber numberWithBool:YES] forKey:@"isQuerySku"];
        }
        
        if (self.isQueryProp) {
            [param setObject:[NSNumber numberWithBool:YES] forKey:@"isQueryProp"];
        }
        
        if (self.condictionArray.count) {
            [param setObject:self.condictionArray forKey:@"propReqList"];
        }
        
    } else if (self.type == YDSearchTypeCustomer) {
        path = URL_SEARCH_CUSTOMER;
        
        if (self.moneyOwe >= 0) {
            if (self.moneyOwe > 0) {
                [param setObject:[NSNumber numberWithBool:YES] forKey:@"moneyOwe"];
                
            } else {
                [param setObject:[NSNumber numberWithBool:NO] forKey:@"moneyOwe"];
            }
        }
        if (self.goodsOwe >= 0) {
            if (self.goodsOwe > 0) {
                [param setObject:[NSNumber numberWithBool:YES] forKey:@"goodsOwe"];
                
            } else {
                [param setObject:[NSNumber numberWithBool:NO] forKey:@"goodsOwe"];
            }
        }
        
    } else if (self.type == YDSearchTypeOrder) {
        path = URL_SEARCH_ORDER;
        
        if (self.createStartTime.length) {
            [param setObject:self.createStartTime forKey:@"createStartTime"];
        }
        
        if (self.createEndTime.length) {
            [param setObject:self.createEndTime forKey:@"createEndTime"];
        }
        
        if (self.arrears >= 0) {
            if (self.arrears > 0) {
                [param setObject:[NSNumber numberWithBool:YES] forKey:@"arrears"];
                
            } else {
                [param setObject:[NSNumber numberWithBool:NO] forKey:@"arrears"];
            }
        }
        
        if (self.dueGoods >= 0) {
            if (self.dueGoods > 0) {
                [param setObject:[NSNumber numberWithBool:YES] forKey:@"dueGoods"];
                
            } else {
                [param setObject:[NSNumber numberWithBool:NO] forKey:@"dueGoods"];
            }
        }
        
        [param setObject:[NSNumber numberWithBool:self.includeItems] forKey:@"includeItems"];
        
        if (self.clientId.length) {
            [param setObject:self.clientId forKey:@"clientId"];
        }
        
    } else if (self.type == YDSearchTypeSupply) {
        path = URL_SEARCH_SUPPLY;
        
        [param setObject:[NSString stringWithFormat:@"%ld", (long)self.tabStatus] forKey:@"tabStatus"];
        
        if (self.createStartTime.length) {
            [param setObject:self.createStartTime forKey:@"startTime"];
        }
        
        if (self.createEndTime.length) {
            [param setObject:self.createEndTime forKey:@"endTime"];
        }
        
    } else if (self.type == YDSearchTypeDelivery) {
        path = URL_SEARCH_DELIVERY;
        
        if (self.createStartTime.length) {
            [param setObject:self.createStartTime forKey:@"startTime"];
        }
        
        if (self.createEndTime.length) {
            [param setObject:self.createEndTime forKey:@"endTime"];
        }
    }

    if (self.keyword.length) {
        [param setObject:self.keyword forKey:@"keyword"];
    }
    
    if (self.status >= 0) {
        [param setObject:[NSString stringWithFormat:@"%ld", (long)self.status] forKey:@"status"];
    }

    [param setObject:[NSString stringWithFormat:@"%ld", (long)self.sortType] forKey:@"sortType"];
    
    [param setObject:[NSString stringWithFormat:@"%ld", self.pageNum + 1] forKey:@"pageNum"];
    NSInteger maxSize = SEARCH_MAX_SIZE;
    if (self.pageSize > 0) {
        maxSize = self.pageSize;
    }
    [param setObject:[NSString stringWithFormat:@"%ld", (long)maxSize] forKey:@"pageSize"];

    YDWeakSelf
    AppleServerPostCompleteBlock postHandleBlock = ^(BOOL done, id _Nullable object, NSString * _Nullable error) {
        //接口调用成功
        if (done) {
            YDBaseModel *resp = [YDBaseModel modelWithJSON:object];
            
            if (resp.code == 200) {
                NSInteger count = 0;
                if (weakSelf.type == YDSearchTypeProduct) {
                    YDSearcherProductModel *model = [YDSearcherProductModel modelWithJSON:object];
                    // 自行设置标准商品价，用于“更多价格”显示
                    for (YDSearcherProductData *sub in model.data.list) {
                        sub.itemSalesPrice = [NSString stringWithFormat:@"%.2f", sub.salesPrice];
                    }
                    [weakSelf.productDataArray addObjectsFromArray:model.data.list];
                    count = model.data.list.count;
                    
                } else if (weakSelf.type == YDSearchTypeCustomer) {
                    YDSearcherCustomerModel *model = [YDSearcherCustomerModel modelWithJSON:object];
                    [weakSelf.customerDataArray addObjectsFromArray:model.data.list];
                    count = model.data.list.count;
                    
                } else if (weakSelf.type == YDSearchTypeOrder) {
                    YDSearcherOrderModel *model = [YDSearcherOrderModel modelWithJSON:object];
                    [weakSelf.orderDataArray addObjectsFromArray:model.data.list];
                    count = model.data.list.count;
                    
                } else if (weakSelf.type == YDSearchTypeSupply) {
                    YDSearcherSupplyModel *model = [YDSearcherSupplyModel modelWithJSON:object];
                    [weakSelf.supplyDataArray addObjectsFromArray:model.data.list];
                    weakSelf.total = model.data.total;
                    count = model.data.list.count;
                    
                } else if (weakSelf.type == YDSearchTypeDelivery) {
                    YDSearcherDeliveryModel *model = [YDSearcherDeliveryModel modelWithJSON:object];
                    [weakSelf.deliveryDataArray addObjectsFromArray:model.data.list];
                    count = model.data.list.count;
                }

                weakSelf.pageNum++;
                
                if (weakSelf.completion) {
                    if (count >= maxSize) {
                        // 还有下一页
                        weakSelf.completion(BaseVMRefreshTypeNormal);
                        
                    } else {
                        weakSelf.completion(BaseVMRefreshTypeNoMore);
                    }
                }
            
            } else {
                if (weakSelf.faildBlock) {
                    NSString *msg;
                    if (resp.detailMessage.length) {
                        msg = resp.detailMessage;
                        
                    } else if (resp.message.length) {
                        msg = resp.message;
                        
                    } else {
                        msg = @"搜索失败";
                    }
                    weakSelf.faildBlock(resp.code, msg);
                }
            }
            
        } else {
            if (weakSelf.faildBlock) {
                weakSelf.faildBlock(0, @"网络不给力");
            }
        }
    };
    
    if (self.type == YDSearchTypeProduct || self.type == YDSearchTypeSupply || self.type == YDSearchTypeDelivery) {
        [AppleServer.shared postWithPath:path Paramters:param CompleteBlock:postHandleBlock];

    } else if (self.type == YDSearchTypeCustomer || self.type == YDSearchTypeOrder) {
        [AppleServer.shared getWithUrl:path Paramters:param Callback:postHandleBlock];
    }
}

@end

//
//  YDSearcher.h
//  YDERP
//
//  Created by 何英健 on 2021/7/27.
//

#import <UIKit/UIKit.h>
#import "YDSearcherProductModel.h"
#import "YDSearcherCustomerModel.h"
#import "YDSearcherOrderModel.h"
#import "YDSearcherSupplyModel.h"
#import "YDSearcherDeliveryModel.h"

typedef enum : NSUInteger {
    YDSearchTypeProduct, // 商品搜索
    YDSearchTypeCustomer, // 客户搜索
    YDSearchTypeOrder,  // 订单搜索
    YDSearchTypeSupply, // 配货搜索
    YDSearchTypeDelivery // 发货搜索
} YDSearchType;

@interface YDSearcher : YDBaseVM

#pragma mark - 初始化
+ (YDSearcher *)createWithType:(YDSearchType)type;

#pragma mark - 公用搜索条件

// 关键字
@property (nonatomic, copy) NSString *keyword;

/*
 商品搜索&客户搜索:0-停用 1-启用(默认两个都查)
 订单搜索:10-待配货 20-部分配货 30-已配齐
 配货搜索:10-待配货 19-重配 20-部分配货
 发货搜索:10-待发货 20-已发货 30-驳回重配
 */
@property (nonatomic, assign) NSInteger status;

/*
排序条件（必传）
 商品搜索:10-创建时间升序；11-创建时间降序；20-库存升序；21-库存降序
 客户搜索:10-交易时间正序；11-交易时间倒序；20-创建时间正序；21-创建时间倒序；
 订单搜索:10-开单时间升序；11-开单时间降序；20-欠款金额升序；21-欠款金额降序；30-欠货数量升序；31-欠货数量降序；
 配货搜索:10-日期正序；11-日期倒序；20-欠货数正序；21-欠货数倒序；
 发货搜索:10-日期正序；11-日期倒序；
*/
@property (nonatomic, assign) NSInteger sortType;

// 订单起始时间（eg：2012-05-12）
@property (nonatomic, copy) NSString *createStartTime;
// 订单结束时间
@property (nonatomic, copy) NSString *createEndTime;

// 查询页码
@property (nonatomic, assign) NSInteger pageNum;
// 每页多少个
@property (nonatomic, assign) NSInteger pageSize;

#pragma mark - 商品专用搜索条件

// 是否查询SKU信息，不查询可加快查询效率（默认不需要）
@property (nonatomic, assign) BOOL isQuerySku;
// 是否查询商品属性，不查询可加快查询效率（默认不需要）
@property (nonatomic, assign) BOOL isQueryProp;
// 筛选商品属性检索 [{<propNameId>:<123>,<propValueId>:<aaa>},   {<propNameId>:<456>,<propValueId>:<bbb>}]
@property (nonatomic, strong) NSMutableArray<NSMutableDictionary *> *condictionArray;

#pragma mark - 客户专用搜索条件

// 有无欠款，1-有欠款，0-无欠款
@property (nonatomic, assign) NSInteger moneyOwe;
// 有无欠货，1-有欠货，0-无欠货
@property (nonatomic, assign) NSInteger goodsOwe;

#pragma mark - 订单列表专用搜索条件

// 是否欠款 1-有欠款 0-无欠款 -1-不传,即全部
@property (nonatomic, assign) NSInteger arrears;
// 是否欠货 1-有欠货 0-无欠货 -1-不传,即全部
@property (nonatomic, assign) NSInteger dueGoods;
// 是否包含商品明细列表
@property (nonatomic, assign) BOOL includeItems;
// 客户id
@property (nonatomic, copy) NSString *clientId;

#pragma mark - 配货列表专用搜索条件

// 配货tab状态 10-未配齐 30-已配齐
@property (nonatomic, assign) NSInteger tabStatus;

#pragma mark - 搜索结果与回调

// 商品搜索专用
@property (nonatomic, strong) NSMutableArray<YDSearcherProductData *> *productDataArray;
// 客户搜索专用
@property (nonatomic, strong) NSMutableArray<YDSearcherCustomerData *> *customerDataArray;
// 订单搜索专用
@property (nonatomic, strong) NSMutableArray<YDSearcherOrderData *> *orderDataArray;
// 配货搜索专用
@property (nonatomic, strong) NSMutableArray<YDSearcherSupplyData *> *supplyDataArray;
// 发货搜索专用
@property (nonatomic, strong) NSMutableArray<YDSearcherDeliveryData *> *deliveryDataArray;

// 总条数统计
@property (nonatomic, assign) NSInteger total;
@property (nonatomic, copy) BlockLoadPageDataDone completion;
@property (nonatomic, copy) BlockLoadDataFaild faildBlock;

#pragma mark - 搜索方法

// 从第一页开始查询
- (void)refreshData;
// 查询下一页
- (void)loadMoreData;

@end

//
//  YDSearchVC.m
//  YDERP
//
//  Created by 何英健 on 2021/7/20.
//

#import "YDSearchVC.h"
#import "YDSearchVM.h"
#import "YDOrderListCell.h"
#import "YDProductManageCell.h"
#import "YDSelectCustomerCell.h"
#import "YDOrderDetailVC.h"

@interface YDSearchVC () <UITableViewDelegate, UITableViewDataSource, UITextFieldDelegate>

@property (nonatomic, strong) YDSearchVM *vm;

@property (nonatomic, weak) IBOutlet UITextField *textField;

// 当前选中tag
@property (nonatomic, assign) NSInteger currentIndex;

@property (nonatomic, strong) UITableView *productTableView;
@property (nonatomic, strong) UITableView *orderTableView;
@property (nonatomic, strong) UITableView *customerTableView;

@property (nonatomic, weak) IBOutlet UIView *containerView;
@property (nonatomic, strong) UIScrollView *scrollView;

@property (nonatomic, weak) IBOutlet UIView *productView;
@property (nonatomic, weak) IBOutlet UIView *orderView;
@property (nonatomic, weak) IBOutlet UIView *customerView;

@property (nonatomic, weak) IBOutlet UILabel *productLabel;
@property (nonatomic, weak) IBOutlet UILabel *orderLabel;
@property (nonatomic, weak) IBOutlet UILabel *customerLabel;

@property (nonatomic, weak) IBOutlet UIView *productLineView;
@property (nonatomic, weak) IBOutlet UIView *orderLineView;
@property (nonatomic, weak) IBOutlet UIView *customerLineView;

@property (nonatomic, weak) IBOutlet NSLayoutConstraint *headerTop;

@end

@implementation YDSearchVC

#pragma mark - 懒加载

- (YDSearchVM *)vm
{
    if (!_vm) {
        _vm = [[YDSearchVM alloc] init];
    }
    
    return _vm;
}

- (UIScrollView *)scrollView
{
    if (!_scrollView) {
        _scrollView = [[UIScrollView alloc] init];
        _scrollView.pagingEnabled = YES;
        _scrollView.showsHorizontalScrollIndicator = NO;
        [self.containerView addSubview:_scrollView];
        
        [_scrollView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.mas_equalTo(0);
            make.bottom.mas_equalTo(0);
            make.leading.mas_equalTo(0);
            make.trailing.mas_equalTo(0);
        }];
        
        self.productTableView = [self createTableViewWithIndex:0];
        self.orderTableView = [self createTableViewWithIndex:1];
        self.customerTableView = [self createTableViewWithIndex:2];
    }
    
    return _scrollView;
}

#pragma mark - 页面加载

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self initUI];
    
    [self addGesture];
    
    [self setSelectedWithIndex:0];
    
    [self refreshAllData];
    
    [self.textField becomeFirstResponder];
}

- (void)initUI
{
    if (IS_PhoneXAll) {
        self.headerTop.constant = 50;
    }
}

- (UITableView *)createTableViewWithIndex:(NSInteger)index
{
    UITableView *tableView = [[UITableView alloc] initWithFrame:CGRectMake(index * SCREEN_WIDTH, 0, SCREEN_WIDTH, 0) style:UITableViewStylePlain];
    tableView.backgroundColor = ColorFromRGB(0xF8F8F8);
    tableView.dataSource = self;
    tableView.delegate = self;
    tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    tableView.estimatedRowHeight = 0.f;
    tableView.showsVerticalScrollIndicator = NO;
    tableView.showsHorizontalScrollIndicator = NO;

    if (@available(iOS 11.0, *)) {
        tableView.contentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentNever;
    } else {
        self.automaticallyAdjustsScrollViewInsets = NO;
    }
    [self.scrollView addSubview:tableView];
    
    tableView.mj_header = [YDRefreshHeader headerWithRefreshingTarget:self refreshingAction:@selector(refreshData)];
    
    tableView.mj_footer = [YDRefreshFooter footerWithRefreshingTarget:self refreshingAction:@selector(loadMore)];
    
    return tableView;
}

// 刷新所有数据
- (void)refreshAllData
{
    [YDLoadingView showToSuperview:self.view];
    
    [self refreshProductData];
    [self refreshOrderData];
    [self refreshCustomerData];
}

- (void)refreshProductData;
{
    YDWeakSelf
    if (!self.vm.getProductDataDoneBlock) {
        self.vm.getProductDataDoneBlock = ^(BaseVMRefreshType type) {
            [YDLoadingView hideToSuperview:weakSelf.view];
            [weakSelf.productTableView.mj_header endRefreshing];
            [weakSelf refreshFooterWithIndex:0 type:type];
            
            [weakSelf.productTableView reloadData];
        };
    }
    
    if (!self.vm.getProductDataFaildBlock) {
        self.vm.getProductDataFaildBlock = ^(NSInteger code, NSString *error) {
            [YDLoadingView hideToSuperview:weakSelf.view];
            [weakSelf.productTableView.mj_header endRefreshing];
            [weakSelf.productTableView.mj_footer endRefreshing];
            [MBProgressHUD showToastMessage:error];
        };
    }
    
    [self.vm refreshProductData];
}

- (void)loadMore
{
    if (self.currentIndex == 0) {
        [self loadMoreProductData];
        
    } else if (self.currentIndex == 1) {
        [self loadMoreOrderData];
        
    } else if (self.currentIndex == 2) {
        [self loadMoreCustomerData];
    }
}

- (void)loadMoreProductData
{
    [self.vm loadMoreProductData];
}

- (void)refreshOrderData
{
    YDWeakSelf
    if (!self.vm.getOrderDataDoneBlock) {
        self.vm.getOrderDataDoneBlock = ^(BaseVMRefreshType type) {
            [YDLoadingView hideToSuperview:weakSelf.view];
            [weakSelf.orderTableView.mj_header endRefreshing];
            [weakSelf refreshFooterWithIndex:1 type:type];
            
            [weakSelf.orderTableView reloadData];
        };
    }
    
    if (!self.vm.getOrderDataFaildBlock) {
        self.vm.getOrderDataFaildBlock = ^(NSInteger code, NSString *error) {
            [YDLoadingView hideToSuperview:weakSelf.view];
            [weakSelf.orderTableView.mj_header endRefreshing];
            [weakSelf.orderTableView.mj_footer endRefreshing];
            [MBProgressHUD showToastMessage:error];
        };
    }
    
    [self.vm refreshOrderData];
}

- (void)loadMoreOrderData
{
    [self.vm loadMoreOrderData];
}

- (void)refreshCustomerData
{
    YDWeakSelf
    if (!self.vm.getCustomerDataDoneBlock) {
        self.vm.getCustomerDataDoneBlock = ^(BaseVMRefreshType type) {
            [YDLoadingView hideToSuperview:weakSelf.view];
            [weakSelf.customerTableView.mj_header endRefreshing];
            [weakSelf refreshFooterWithIndex:2 type:type];
            
            [weakSelf.customerTableView reloadData];
        };
    }
    
    if (!self.vm.getCustomerDataFaildBlock) {
        self.vm.getCustomerDataFaildBlock = ^(NSInteger code, NSString *error) {
            [YDLoadingView hideToSuperview:weakSelf.view];
            [weakSelf.customerTableView.mj_header endRefreshing];
            [weakSelf.customerTableView.mj_footer endRefreshing];
            [MBProgressHUD showToastMessage:error];
        };
    }
    
    [self.vm refreshCustomerData];
}

- (void)loadMoreCustomerData
{
    [self.vm loadMoreCustomerData];
}

// 刷新footer状态
- (void)refreshFooterWithIndex:(NSInteger)index type:(BaseVMRefreshType)type
{
    UITableView *tableView;
    switch (index) {
        case 0:
        {
            tableView = self.productTableView;
        }
            break;
            
        case 1:
        {
            tableView = self.orderTableView;
        }
            break;
            
        case 2:
        {
            tableView = self.customerTableView;
        }
            break;
            
        default:
            break;
    }
    
    if (type == BaseVMRefreshTypeNoMore) {
        [tableView.mj_footer endRefreshingWithNoMoreData];
        
    } else {
        [tableView.mj_footer endRefreshing];
    }
}

- (void)addGesture
{
    UITapGestureRecognizer *tap1 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapProductView)];
    [self.productView addGestureRecognizer:tap1];
    
    UITapGestureRecognizer *tap2 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapOrderView)];
    [self.orderView addGestureRecognizer:tap2];
    
    UITapGestureRecognizer *tap3 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapCustomerView)];
    [self.customerView addGestureRecognizer:tap3];
}

- (void)dismissKeybord
{
    [self.textField resignFirstResponder];
}

- (void)viewDidLayoutSubviews
{
    [super viewDidLayoutSubviews];
    
    self.scrollView.contentSize = CGSizeMake(SCREEN_WIDTH * 3, self.scrollView.height);
    
    [self.productTableView setHeight:self.scrollView.height];
    [self.orderTableView setHeight:self.scrollView.height];
    [self.customerTableView setHeight:self.scrollView.height];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [self.navigationController setNavigationBarHidden:YES animated:NO];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    
    [self.navigationController setNavigationBarHidden:NO animated:NO];
}

#pragma mark - 点击事件

- (void)tapProductView
{
    [self setSelectedWithIndex:0];
}

- (void)tapOrderView
{
    [self setSelectedWithIndex:1];
}

- (void)tapCustomerView
{
    [self setSelectedWithIndex:2];
}

- (void)setSelectedWithIndex:(NSInteger)index
{
    self.currentIndex = index;
    self.scrollView.contentOffset = CGPointMake(SCREEN_WIDTH * index, 0);
    
    self.productLabel.textColor = ColorFromRGB(0x222222);
    self.orderLabel.textColor = ColorFromRGB(0x222222);
    self.customerLabel.textColor = ColorFromRGB(0x222222);
    
    self.productLineView.hidden = YES;
    self.orderLineView.hidden = YES;
    self.customerLineView.hidden = YES;
    
    switch (index) {
        case 0:
        {
            self.productLabel.textColor = COLOR_MAIN_BULE;
            self.productLineView.hidden = NO;
        }
            break;
            
        case 1:
        {
            self.orderLabel.textColor = COLOR_MAIN_BULE;
            self.orderLineView.hidden = NO;
        }
            break;
            
        case 2:
        {
            self.customerLabel.textColor = COLOR_MAIN_BULE;
            self.customerLineView.hidden = NO;
        }
            break;
            
        default:
            break;
    }
}

- (IBAction)tapCancelBtn:(id)sender
{
    [self.navigationController popViewControllerAnimated:NO];
}

#pragma mark - UITableView代理

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    NSMutableArray *dataArray = [self getDataArrayWithTableView:tableView];
    if (dataArray.count) {
        return dataArray.count;
        
    } else {
        return 1;
    }
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSMutableArray *dataArray = [self getDataArrayWithTableView:tableView];
    if (dataArray.count) {
        if (tableView == self.productTableView) {
            return [YDProductManageCell getHeight];
            
        } else if (tableView == self.orderTableView) {
            return [YDOrderListCell getHeight];
            
        } else if (tableView == self.customerTableView) {
            return [YDSelectCustomerCell getHeight];
        }
        
    } else {
        return [YDNullCell getHeightWithTableView:tableView];
    }
    
    return 0.01;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSMutableArray *dataArray = [self getDataArrayWithTableView:tableView];
    if (dataArray.count) {
        if (tableView == self.productTableView) {
            YDProductManageCell *cell = [YDProductManageCell cellWithTableView:tableView];
            cell.data = self.vm.productDataArray[indexPath.section];
            
            return cell;
            
        } else if (tableView == self.orderTableView) {
            YDOrderListCell *cell = [YDOrderListCell cellWithTableView:tableView];
            cell.data = self.vm.orderDataArray[indexPath.section];
            
            return cell;
            
        } else if (tableView == self.customerTableView) {
            YDSelectCustomerCell *cell = [YDSelectCustomerCell cellWithTableView:tableView];
            cell.data = self.vm.customerDataArray[indexPath.section];
            
            return cell;
        }
        
    } else {
        YDNullCell *cell = [YDNullCell cellWithTableView:tableView];
        
        return cell;
    }
    
    return [[UITableViewCell alloc] init];
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSMutableArray *dataArray = [self getDataArrayWithTableView:tableView];
    if (dataArray.count) {
        if (tableView == self.productTableView) {
            YDSearcherProductData *productData = self.vm.productDataArray[indexPath.section];
            NSString *url = [NSString stringWithFormat:@"%@/%ld", WEBURL_PRODUCT_DETAIL, (long)productData.ID];
            [YDWebViewVC showWebViewWithUrl:url isHideNav:YES];
            
        } else if (tableView == self.orderTableView) {
            YDOrderDetailVC *vc = [[YDOrderDetailVC alloc] init];
            YDSearcherOrderData *data = self.vm.orderDataArray[indexPath.section];
            vc.orderID = [NSString stringWithFormat:@"%ld", (long)data.orderId];
            [self.navigationController pushViewController:vc animated:YES];
            
        } else if (tableView == self.customerTableView) {
            YDSearcherCustomerData *productData = self.vm.customerDataArray[indexPath.section];
            NSString *url = [NSString stringWithFormat:@"%@/%ld", WEBURL_CUSTOMER_DETAIL, (long)productData.ID];
            [YDWebViewVC showWebViewWithUrl:url isHideNav:YES];
        }
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 10;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    return [[UIView alloc] init];
}

- (NSMutableArray *)getDataArrayWithTableView:(UITableView *)tableView
{
    NSMutableArray *dataArray;
    if (tableView == self.productTableView) {
        dataArray = self.vm.productDataArray;
        
    } else if (tableView == self.orderTableView) {
        dataArray = self.vm.orderDataArray;
        
    } else {
        dataArray = self.vm.customerDataArray;
    }
    return dataArray;
}

#pragma mark - UITextFieldDelegate

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    
    return YES;
}

- (BOOL)textFieldShouldEndEditing:(UITextField *)textField
{
    [self dismissKeybord];
    
    self.vm.keyword = textField.text;
    [self refreshAllData];
    
    return YES;
}

@end

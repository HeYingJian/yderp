//
//  YDSearchVM.h
//  YDERP
//
//  Created by 何英健 on 2021/7/21.
//

#import "YDBaseVM.h"

@interface YDSearchVM : YDBaseVM

// 搜索关键字
@property (nonatomic, copy) NSString *keyword;

// 刷新商品数据
- (void)refreshProductData;
// 加载商品下一页
- (void)loadMoreProductData;
@property (nonatomic, strong) NSMutableArray<YDSearcherProductData *> *productDataArray;
@property (nonatomic, copy) BlockLoadPageDataDone getProductDataDoneBlock;
@property (nonatomic, copy) BlockLoadDataFaild getProductDataFaildBlock;

// 刷新订单数据
- (void)refreshOrderData;
// 加载订单下一页
- (void)loadMoreOrderData;
@property (nonatomic, strong) NSMutableArray<YDSearcherOrderData *> *orderDataArray;
@property (nonatomic, copy) BlockLoadPageDataDone getOrderDataDoneBlock;
@property (nonatomic, copy) BlockLoadDataFaild getOrderDataFaildBlock;

// 刷新客户数据
- (void)refreshCustomerData;
// 加载客户下一页
- (void)loadMoreCustomerData;
@property (nonatomic, strong) NSMutableArray<YDSearcherCustomerData *> *customerDataArray;
@property (nonatomic, copy) BlockLoadPageDataDone getCustomerDataDoneBlock;
@property (nonatomic, copy) BlockLoadDataFaild getCustomerDataFaildBlock;

@end

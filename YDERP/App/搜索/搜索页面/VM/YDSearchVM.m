//
//  YDSearchVM.m
//  YDERP
//
//  Created by 何英健 on 2021/7/21.
//

#import "YDSearchVM.h"

@interface YDSearchVM ()

// 搜索器
@property (nonatomic, strong) YDSearcher *productSearcher;
@property (nonatomic, strong) YDSearcher *orderSearcher;
@property (nonatomic, strong) YDSearcher *customerSearcher;

@end

@implementation YDSearchVM

#pragma mark - 懒加载

- (YDSearcher *)productSearcher
{
    if (!_productSearcher) {
        _productSearcher = [YDSearcher createWithType:YDSearchTypeProduct];
    }
    
    return _productSearcher;
}

- (YDSearcher *)orderSearcher
{
    if (!_orderSearcher) {
        _orderSearcher = [YDSearcher createWithType:YDSearchTypeOrder];
    }
    
    return _orderSearcher;
}

- (YDSearcher *)customerSearcher
{
    if (!_customerSearcher) {
        _customerSearcher = [YDSearcher createWithType:YDSearchTypeCustomer];
    }
    
    return _customerSearcher;
}

- (NSMutableArray<YDSearcherProductData *> *)productDataArray
{
    return self.productSearcher.productDataArray;
}

- (NSMutableArray<YDSearcherOrderData *> *)orderDataArray
{
    return self.orderSearcher.orderDataArray;
}

- (NSMutableArray<YDSearcherCustomerData *> *)customerDataArray
{
    return self.customerSearcher.customerDataArray;
}

#pragma mark - 加载商品搜索

- (void)refreshProductData
{
    self.productSearcher.keyword = self.keyword;
    self.productSearcher.completion = self.getProductDataDoneBlock;
    self.productSearcher.faildBlock = self.getProductDataFaildBlock;
    [self.productSearcher refreshData];
}

// 加载更多商品
- (void)loadMoreProductData
{
    [self.productSearcher loadMoreData];
}

#pragma mark - 加载订单搜索

- (void)refreshOrderData
{
    self.orderSearcher.keyword = self.keyword;
    self.orderSearcher.completion = self.getOrderDataDoneBlock;
    self.orderSearcher.faildBlock = self.getOrderDataFaildBlock;
    [self.orderSearcher refreshData];
}

- (void)loadMoreOrderData
{
    [self.orderSearcher loadMoreData];
}

#pragma mark - 加载客户搜索

- (void)refreshCustomerData
{
    self.customerSearcher.keyword = self.keyword;
    self.customerSearcher.completion = self.getCustomerDataDoneBlock;
    self.customerSearcher.faildBlock = self.getCustomerDataFaildBlock;
    [self.customerSearcher refreshData];
}

- (void)loadMoreCustomerData
{
    [self.customerSearcher loadMoreData];
}

@end

//
//  YDProductVC.m
//  YDERP
//
//  Created by 何英健 on 2021/7/19.
//

#import "YDProductVC.h"
#import "YDSelectView.h"
#import "YDSearcher.h"
#import "YDProductManageCell.h"
#import "YDAddProductVC.h"

@interface YDProductVC () <UITextFieldDelegate, UITableViewDelegate, UITableViewDataSource>

// 搜索器
@property (nonatomic, strong) YDSearcher *searcher;

@property (nonatomic, weak) IBOutlet UITextField *searchTextField;

@property (nonatomic, weak) IBOutlet UIView *tagBGView;
@property (nonatomic, strong) YDSelectView *tagView;

@property (nonatomic, weak) IBOutlet UITableView *tableView;

@end

@implementation YDProductVC

- (YDSearcher *)searcher
{
    if (!_searcher) {
        _searcher = [YDSearcher createWithType:YDSearchTypeProduct];
    }
    
    return _searcher;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self initUI];
    
    [self addNotification];
}

- (void)initUI
{
    [self setupTagView];
    [self setupTableView];
}

- (void)setupTableView
{
    self.tableView.hidden = YES;
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    self.tableView.showsVerticalScrollIndicator = NO;
    self.tableView.showsHorizontalScrollIndicator = NO;
    
    if (@available(iOS 11.0, *)) {
        self.tableView.contentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentNever;
        
    } else {
        self.automaticallyAdjustsScrollViewInsets = NO;
    }
    
    UIView *tableHeaderView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, 11)];
    self.tableView.tableHeaderView = tableHeaderView;
    
    self.tableView.mj_header = [YDRefreshHeader headerWithRefreshingTarget:self refreshingAction:@selector(pullingRefresh)];
    
    self.tableView.mj_footer = [YDRefreshFooter footerWithRefreshingTarget:self refreshingAction:@selector(loadMore)];
}

- (void)addNotification
{
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(refreshDataWithNotification:) name:REFRESH_PRODUCT_MANAGE object:nil];
}

- (void)refreshDataWithNotification:(NSNotification *)notification
{
    [self refreshData:NO];
}

- (IBAction)tapAddProductBtn:(id)sender
{
    YDAddProductVC *vc = [[YDAddProductVC alloc] init];
    vc.type = YDProductTypeAdd;
    vc.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:vc animated:YES];
}

- (void)dismissKeybord
{
    [self.searchTextField resignFirstResponder];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    self.navigationController.navigationBar.translucent = YES;
    [self.navigationController setNavigationBarHidden:YES animated:NO];
}

#pragma mark - 数据请求

- (void)pullingRefresh
{
    [self refreshData:NO];
}

- (void)refreshData:(BOOL)showLoading
{
    [self dismissKeybord];
    
    if (showLoading) {
        [YDLoadingView showToSuperview:self.view];
    }
    
    YDWeakSelf
    if (!self.searcher.completion) {
        self.searcher.completion = ^(BaseVMRefreshType type) {
            weakSelf.tableView.hidden = NO;
            [YDLoadingView hideToSuperview:weakSelf.view];
            [weakSelf.tableView.mj_header endRefreshing];

            if (type == BaseVMRefreshTypeNoMore) {
                [weakSelf.tableView.mj_footer endRefreshingWithNoMoreData];
                
            } else {
                [weakSelf.tableView.mj_footer endRefreshing];
            }
            
            [weakSelf.tableView reloadData];
        };
    }
    
    if (!self.searcher.faildBlock) {
        self.searcher.faildBlock = ^(NSInteger code, NSString *error) {
            weakSelf.tableView.hidden = NO;
            [YDLoadingView hideToSuperview:weakSelf.view];
            [weakSelf.tableView.mj_header endRefreshing];
            [weakSelf.tableView.mj_footer endRefreshing];
            [MBProgressHUD showToastMessage:error];
        };
    }
    
    [self setSearchCondiction];
    [self.searcher refreshData];
}

- (void)loadMore
{
    [self.searcher loadMoreData];
}

#pragma mark - 搜索条件

- (void)setupTagView
{
    NSMutableArray *modelArray = [NSMutableArray array];
    
    for (int i = 0; i < 2; i++) {
        YDSelectViewModel *model = [[YDSelectViewModel alloc] init];
        model.tagWidth = SCREEN_WIDTH / 3;
        
        switch (i) {
            case 0:
            {
                model.title = @"默认";
            }
                break;
                
            case 1:
            {
                model.title = @"库存";
                model.type = 0;
                [model createImgViewModelWithSelectedImg:@"筛选下箭头选中" unselectedImg:@"筛选下箭头未选中"];
                [model createImgViewModelWithSelectedImg:@"筛选上箭头选中" unselectedImg:@"筛选上箭头未选中"];
            }
                break;

            default:
                break;
        }
        [modelArray addObject:model];
    }
    
    YDWeakSelf
    self.tagView = [YDSelectView createViewWithType:YDSelectFilterTypeProduct modelArray:modelArray showFilterBlock:^{
        [weakSelf dismissKeybord];
        
    } refreshBlock:^{
        [weakSelf refreshData:YES];
    }];
    [self.tagBGView addSubview:self.tagView];
}

// 设置搜索模型，设置对应搜索条件
- (void)setSearchCondiction
{
    self.searcher.keyword = self.searchTextField.text;
    
    for (int i = 0; i < self.tagView.modelArray.count; i++) {
        YDSelectViewModel *model = self.tagView.modelArray[i];
        
        if (model.isSelected) {
            switch (i) {
                case 0:
                {
                    self.searcher.sortType = 11;
                }
                    break;
                    
                case 1:
                {
                    if (model.type == 0) {
                        self.searcher.sortType = 21;
                        
                    } else {
                        self.searcher.sortType = 20;
                    }
                }
                    break;
                    
                default:
                    break;
            }
        }
    }
    
    self.searcher.condictionArray = self.tagView.selectedResultArray;
    if (self.tagView) {
        self.searcher.status = self.tagView.selectedResultStatus;
    }
}

#pragma mark - UITableView代理

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    NSInteger count = self.searcher.productDataArray.count;
    if (count) {
        return count;
        
    } else {
        return 1;
    }
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (self.searcher.productDataArray.count) {
        return [YDProductManageCell getHeight];
        
    } else {
        return [YDNullCell getHeightWithTableView:tableView] - self.tableView.tableHeaderView.height;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (self.searcher.productDataArray.count) {
        YDProductManageCell *cell = [YDProductManageCell cellWithTableView:tableView];
        cell.data = self.searcher.productDataArray[indexPath.section];
        
        return cell;
        
    } else {
        YDNullCell *cell = [YDNullCell cellWithTableView:tableView];
        cell.content = @"暂无数据";
        return cell;
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    return 10;
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section
{
    return [[UIView alloc] init];
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (self.searcher.productDataArray.count) {
        YDSearcherProductData *productData = self.searcher.productDataArray[indexPath.section];
        NSString *url = [NSString stringWithFormat:@"%@/%ld", WEBURL_PRODUCT_DETAIL, productData.ID];
        [YDWebViewVC showWebViewWithUrl:url isHideNav:YES];
    }
}

#pragma mark - UITextFieldDelegate

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    
    [self refreshData:YES];

    return YES;
}

@end

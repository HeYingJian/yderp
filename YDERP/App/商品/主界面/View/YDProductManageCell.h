//
//  YDProductManageCell.h
//  YDERP
//
//  Created by 何英健 on 2021/7/27.
//

#import <UIKit/UIKit.h>
#import "YDSearcherProductModel.h"

@interface YDProductManageCell : UITableViewCell

+ (instancetype)cellWithTableView:(UITableView *)tableView;

+ (CGFloat)getHeight;

@property (nonatomic, strong) YDSearcherProductData *data;

@end

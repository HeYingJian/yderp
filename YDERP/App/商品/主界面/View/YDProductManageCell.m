//
//  YDProductManageCell.m
//  YDERP
//
//  Created by 何英健 on 2021/7/27.
//

#import "YDProductManageCell.h"

@interface YDProductManageCell ()

@property (nonatomic, weak) IBOutlet UIImageView *imgView;
@property (nonatomic, weak) IBOutlet UILabel *productNameLabel;
@property (nonatomic, weak) IBOutlet UILabel *priceLabel;
@property (nonatomic, weak) IBOutlet UILabel *stockCountLabel;
@property (nonatomic, weak) IBOutlet UIImageView *stopImgView;

@end

@implementation YDProductManageCell

+ (instancetype)cellWithTableView:(UITableView *)tableView
{
    static NSString *reuseID = @"productManageCell";
    YDProductManageCell *cell = [tableView dequeueReusableCellWithIdentifier:reuseID];
    if (!cell) {
        cell = [[NSBundle mainBundle] loadNibNamed:@"YDProductManageCell" owner:nil options:nil][0];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    
    return cell;
}

+ (CGFloat)getHeight
{
    return 118;
}

- (void)setData:(YDSearcherProductData *)data
{
    _data = data;
    
    if (data.picList.count && data.picList[0].url.length) {
        [self.imgView appleSetImageWithUrl:data.picList[0].url SprcifyOssImageStyle:@"products_list" HasHD:NO placeholderImage:@"默认商品方形图" completion:nil];
        
    } else {
        [self.imgView setImage:[UIImage imageNamed:@"没有商品默认图"]];
    }
    
    NSString *codeStr = data.code;
    NSString *productName = data.name;
    NSString *str = [NSString stringWithFormat:@"%@#%@", codeStr, productName];
    NSMutableAttributedString *attributedStr = [[NSMutableAttributedString alloc] initWithString:str];
    [attributedStr addAttribute:NSFontAttributeName value:FONT_NUMBER(15) range:NSMakeRange(0, codeStr.length + 1)];
    [attributedStr addAttribute:NSForegroundColorAttributeName value:ColorFromRGB(0xFF5630) range:NSMakeRange(0, codeStr.length + 1)];
    [attributedStr addAttribute:NSFontAttributeName value:[UIFont systemFontOfSize:14.0f] range:NSMakeRange(codeStr.length + 1, productName.length)];
    [attributedStr addAttribute:NSForegroundColorAttributeName value:ColorFromRGB(0x212121) range:NSMakeRange(codeStr.length + 1, productName.length)];
    NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc] init];
    [paragraphStyle setLineSpacing:5];
    [attributedStr addAttribute:NSParagraphStyleAttributeName value:paragraphStyle range:NSMakeRange(0, [str length])];
    self.productNameLabel.attributedText = attributedStr;
    self.productNameLabel.lineBreakMode = NSLineBreakByTruncatingTail;
    
    self.priceLabel.text = [NSString applePrefixPriceWith:data.salesPrice];
    self.stockCountLabel.text = [NSString stringWithFormat:@"%ld", data.stockNum];
    
    self.stopImgView.hidden = (data.status == 0)? NO : YES;
}

- (void)awakeFromNib
{
    [super awakeFromNib];
    
    self.priceLabel.font = FONT_NUMBER(17);
    self.stockCountLabel.font = FONT_NUMBER(13);
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end

//
//  YDPorductDetailModel.h
//  YDERP
//
//  Created by 何英健 on 2021/8/7.
//

#import "YDBaseModel.h"
#import "YDSearcherProductModel.h"

@interface YDPorductDetailModel : YDBaseModel

@property (nonatomic, strong) YDSearcherProductData *data;

@end


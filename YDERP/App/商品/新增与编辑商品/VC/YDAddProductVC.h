//
//  YDAddProductVC.h
//  YDERP
//
//  Created by 何英健 on 2021/8/3.
//

#import <UIKit/UIKit.h>

typedef void(^BlockProductEditSucceed)(void);

typedef enum : NSUInteger {
    YDProductTypeAdd, // 新增商品
    YDProductTypeEdit // 修改商品
} YDProductType;

@interface YDAddProductVC : UIViewController

@property (nonatomic, assign) YDProductType type;

// 商品id（修改商品时传入）
@property (nonatomic, assign) NSInteger ID;

@property (nonatomic, copy) BlockProductEditSucceed editSucceedBlock;
 
@end

//
//  YDAddProductVC.m
//  YDERP
//
//  Created by 何英健 on 2021/8/3.
//

#import "YDAddProductVC.h"
#import "YDAddProductBaseInfoCell.h"
#import "YDAddProductFormCell.h"
#import "YDAddProductConfirmCell.h"
#import "YDSearcherPropListModel.h"
#import "YDPorductDetailModel.h"

@interface YDAddProductVC () <UITableViewDelegate, UITableViewDataSource,  UIImagePickerControllerDelegate, UINavigationControllerDelegate>

// 显示在“基本信息”栏的属性
@property (nonatomic, strong) NSMutableArray<YDSearcherPropList *> *baseDataArray;
// 显示在“更多信息”栏的属性
@property (nonatomic, strong) NSMutableArray<YDSearcherPropList *> *moreDataArray;

@property (nonatomic, weak) IBOutlet UITableView *tableView;

#pragma mark - 保存选择结果
// 图片
@property (nonatomic, strong) NSMutableArray<NSString *> *photoArray;
// 款号
@property (nonatomic, copy) NSString *code;
// 销售价
@property (nonatomic, copy) NSString *salesPrice;
// 批发价
@property (nonatomic, copy) NSString *wholePrice;
// 打包价
@property (nonatomic, copy) NSString *packagePrice;
// 成本价
@property (nonatomic, copy) NSString *basePrice;
// 商品名
@property (nonatomic, copy) NSString *name;
// 品牌
@property (nonatomic, copy) NSString *brand;

@end

@implementation YDAddProductVC

#pragma mark - 懒加载

- (NSMutableArray<YDSearcherPropList *> *)baseDataArray
{
    if (!_baseDataArray) {
        _baseDataArray = [NSMutableArray array];
    }
    
    return _baseDataArray;
}

- (NSMutableArray<YDSearcherPropList *> *)moreDataArray
{
    if (!_moreDataArray) {
        _moreDataArray = [NSMutableArray array];
    }
    
    return _moreDataArray;
}

- (NSMutableArray<NSString *> *)photoArray
{
    if (!_photoArray) {
        _photoArray = [NSMutableArray array];
    }
    
    return _photoArray;
}

#pragma mark - 页面加载

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self initUI];
    
    [self loadFormData];
}

- (void)initUI
{
    NSString *title;
    if (self.type == YDProductTypeAdd) {
        title = @"新增商品";
        
    } else {
        title = @"编辑商品";
    }
    self.title = title;
    [self.navigationItem addDefaultBackButton:self actionMethod:@selector(goBack)];
    
    [self setupTableView];
}

- (void)goBack
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)setupTableView
{
    self.tableView.hidden = YES;
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    self.tableView.showsVerticalScrollIndicator = NO;
    self.tableView.showsHorizontalScrollIndicator = NO;
    
    if (@available(iOS 11.0, *)) {
        self.tableView.contentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentNever;
        
    } else {
        self.automaticallyAdjustsScrollViewInsets = NO;
    }
}

// 上传前进行参数检查
- (void)checkFormData
{
    if (!self.code.length) {
        [MBProgressHUD showToastMessage:@"款号不能为空"];
        return;
    }
    
    if (self.code.length > 20) {
        [MBProgressHUD showToastMessage:@"款号限制20字以内"];
        return;
    }
    
    if (!self.salesPrice.length) {
        [MBProgressHUD showToastMessage:@"销售价不能为空"];
        return;
    }
    
    if ([self.salesPrice doubleValue] >= 1000000) {
        [MBProgressHUD showToastMessage:@"销售价必须小于100万"];
        return;
    }
    
    if ([self.wholePrice doubleValue] >= 1000000) {
        [MBProgressHUD showToastMessage:@"批发价必须小于100万"];
        return;
    }
    
    if ([self.packagePrice doubleValue] >= 1000000) {
        [MBProgressHUD showToastMessage:@"打包价必须小于100万"];
        return;
    }
    
    if ([self.basePrice doubleValue] >= 1000000) {
        [MBProgressHUD showToastMessage:@"成本价必须小于100万"];
        return;
    }
    
    if (self.name.length > 50) {
        [MBProgressHUD showToastMessage:@"商品名限制50字以内"];
        return;
    }
    
    if (self.brand.length > 20) {
        [MBProgressHUD showToastMessage:@"品牌名限制20字以内"];
        return;
    }
    
    [self uploadProductData];
}

// 刷新，相当于重新进入
- (void)refresh
{
    self.photoArray = nil;
    self.code = nil;
    self.salesPrice = nil;
    self.wholePrice = nil;
    self.packagePrice = nil;
    self.basePrice = nil;
    self.name = nil;
    self.brand = nil;
    
    for (YDSearcherPropList *sub in self.baseDataArray) {
        for (YDSearcherPropValueList *subList in sub.propValueList) {
            subList.isSelected = NO;
        }
    }
    
    for (YDSearcherPropList *sub in self.moreDataArray) {
        for (YDSearcherPropValueList *subList in sub.propValueList) {
            subList.isSelected = NO;
        }
    }
    
    [self.tableView reloadData];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    self.navigationController.navigationBar.translucent = YES;
    [self.navigationController setNavigationBarHidden:NO animated:NO];
}

#pragma mark - 添加商品

// 加载需要填写的选项数据
- (void)loadFormData
{
    [YDLoadingView showToSuperview:self.view];
    
    NSString *path = URL_ADD_PRODUCT_PROPERTY;
    NSMutableDictionary *param = [NSMutableDictionary dictionary];

    YDWeakSelf
    AppleServerPostCompleteBlock postHandleBlock = ^(BOOL done, id _Nullable object, NSString * _Nullable error) {
        [YDLoadingView hideToSuperview:weakSelf.view];
        if (done) {
            YDSearcherPropListModel *resp = [YDSearcherPropListModel modelWithJSON:object];
            
            if (resp.code == 200) {
                for (YDSearcherPropList *sub in resp.data) {
                    if (sub.type == 0) {
                        [weakSelf.moreDataArray addObject:sub];
                        
                    } else {
                        [weakSelf.baseDataArray addObject:sub];
                    }
                }
                
                if (weakSelf.type == YDProductTypeAdd) {
                    weakSelf.tableView.hidden = NO;
                    [weakSelf.tableView reloadData];
                    
                } else if (weakSelf.type == YDProductTypeEdit) {
                    // 加载当前商品原有信息
                    [weakSelf loadEditProductData];
                }
            
            } else {
                NSString *msg;
                if (resp.detailMessage.length) {
                    msg = resp.detailMessage;
                    
                } else if (resp.message.length) {
                    msg = resp.message;
                    
                } else {
                    msg = @"网络失败";
                }
                [MBProgressHUD showToastMessage:msg];
            }
            
        } else {
            [MBProgressHUD showToastMessage:@"网络不给力"];
        }
    };
    
    [AppleServer.shared getWithUrl:path Paramters:param Callback:postHandleBlock];
}

// 请求接口新增/编辑商品
- (void)uploadProductData
{
    [YDLoadingView showToSuperview:self.view];
    
    NSString *path;
    if (self.type == YDProductTypeAdd) {
        path = URL_ADD_PRODUCT;
        
    } else if (self.type == YDProductTypeEdit) {
        path = URL_EDIT_PRODUCT;
    }
    
    NSMutableDictionary *param = [NSMutableDictionary dictionary];
    if (self.type == YDProductTypeEdit) {
        [param setObject:[NSString stringWithFormat:@"%ld", (long)self.ID] forKey:@"id"];
    }
    // 名称
    if (self.name.length) {
        [param setObject:self.name forKey:@"name"];
        
    } else {
        [param setObject:@"" forKey:@"name"];
    }
    // 编码
    if (self.code.length) {
        [param setObject:self.code forKey:@"code"];
    }
    // 销售价
    if (self.salesPrice.length) {
        [param setObject:self.salesPrice forKey:@"salesPrice"];
    }
    // 批发价
    if (self.wholePrice.length) {
        [param setObject:self.wholePrice forKey:@"wholePrice"];
        
    } else {
        [param setObject:@"" forKey:@"wholePrice"];
    }
    // 打包价
    if (self.packagePrice.length) {
        [param setObject:self.packagePrice forKey:@"packagePrice"];
        
    } else {
        [param setObject:@"" forKey:@"packagePrice"];
    }
    // 成本价
    if (self.basePrice.length) {
        [param setObject:self.basePrice forKey:@"basePrice"];
        
    } else {
        [param setObject:@"" forKey:@"basePrice"];
    }
    // 品牌名
    if (self.brand.length) {
        [param setObject:self.brand forKey:@"brand"];
        
    } else {
        [param setObject:@"" forKey:@"brand"];
    }
    // 图片
    NSMutableArray *photoListArray = [self getPhotoList];
    [param setObject:photoListArray forKey:@"picList"];
    // 颜色&尺码
    NSMutableArray *skuListArray = [self getSkuList];
    [param setObject:skuListArray forKey:@"skuList"];
    // 其他属性
    NSMutableArray *itemPropListArray = [self getItemPropList];
    [param setObject:itemPropListArray forKey:@"itemPropList"];
    
    YDWeakSelf
    AppleServerPostCompleteBlock postHandleBlock = ^(BOOL done, id _Nullable object, NSString * _Nullable error) {
        [YDLoadingView hideToSuperview:weakSelf.view];
        if (done) {
            YDBaseModel *resp = [YDBaseModel modelWithJSON:object];
            
            if (resp.code == 200) {
                // 通知商品管理页刷新
                PostNotification(REFRESH_PRODUCT_MANAGE, nil);
                
                if (weakSelf.type == YDProductTypeAdd) {
                    // 新增成功
                    [weakSelf showSucceedPopView];
                    
                } else if (weakSelf.type == YDProductTypeEdit) {
                    // 编辑成功
                    [MBProgressHUD showToastMessage:@"保存成功"];
                    
                    if (weakSelf.editSucceedBlock) {
                        weakSelf.editSucceedBlock();
                    }
                    
                    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                        [self.navigationController popViewControllerAnimated:YES];
                    });
                }
            
            } else {
                NSString *msg;
                if (resp.detailMessage.length) {
                    msg = resp.detailMessage;
                    
                } else if (resp.message.length) {
                    msg = resp.message;
                    
                } else {
                    msg = @"添加失败";
                }
                [MBProgressHUD showToastMessage:msg];
            }
            
        } else {
            [MBProgressHUD showToastMessage:@"网络不给力"];
        }
    };
    
    //使用AppleServer发送POST请求
    [AppleServer.shared postWithPath:path Paramters:param CompleteBlock:postHandleBlock];
}

// 弹窗提示
- (void)showSucceedPopView
{
    YDSelectPopView *view = [YDSelectPopView show];
    view.title = @"新增成功";
    view.content = @"您可以继续新增 也可返回上一页";
    view.leftBtnText = @"返回上一页";
    view.rightBtnText = @"继续新增";
    view.tapLeftBtnBlock = ^{
        [self.navigationController popViewControllerAnimated:YES];
    };
    view.tapRightBtnBlock = ^{
        [self refresh];
        
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.3 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            [self.tableView scrollToTopAnimated:YES];
        });
    };
}

// 组装图片列表
- (NSMutableArray *)getPhotoList
{
    NSMutableArray *photoListArray = [NSMutableArray array];
    for (int i = 1; i <= self.photoArray.count; i++) {
        NSMutableDictionary *dict = [NSMutableDictionary dictionary];
        [dict setObject:self.photoArray[i - 1] forKey:@"url"];
        [dict setObject:@(i) forKey:@"sort"];
        [photoListArray addObject:dict];
    }
    
    return photoListArray;
}

// 组装sku列表
- (NSMutableArray *)getSkuList
{
    NSMutableArray<YDSearcherPropValueList *> *colorArray = [NSMutableArray array];
    NSMutableArray<YDSearcherPropValueList *> *sizeArray = [NSMutableArray array];
    
    for (YDSearcherPropList *sub in self.baseDataArray) {
        if (sub.type == 10) {
            // 这个是颜色
            YDSearcherPropValueList *defaultColor;
            for (YDSearcherPropValueList *subList in sub.propValueList) {
                if (subList.isSelected) {
                    [colorArray addObject:subList];
                }
                
                if (subList.defaultFlag == 1) {
                    defaultColor = subList;
                }
            }
            
            // 没有选中颜色，使用默认项
            if (!colorArray.count && defaultColor) {
                [colorArray addObject:defaultColor];
            }
            
        } else if (sub.type == 11) {
            // 这个是尺码
            YDSearcherPropValueList *defaultSize;
            for (YDSearcherPropValueList *subList in sub.propValueList) {
                if (subList.isSelected) {
                    [sizeArray addObject:subList];
                }
                
                if (subList.defaultFlag == 1) {
                    defaultSize = subList;
                }
            }
            
            // 没有选中尺码，使用默认项
            if (!sizeArray.count && defaultSize) {
                [sizeArray addObject:defaultSize];
            }
        }
    }
    
    // 开始组装成sku列表
    NSMutableArray *skuListArray = [NSMutableArray array];
    if (colorArray.count && sizeArray.count) {
        for (YDSearcherPropValueList *color in colorArray) {
            for (YDSearcherPropValueList *size in sizeArray) {
                NSMutableDictionary *dict = [NSMutableDictionary dictionary];
                [dict setObject:color.propValue forKey:@"color"];
                [dict setObject:size.propValue forKey:@"size"];
                [dict setObject:color.propValueId forKey:@"colorId"];
                [dict setObject:size.propValueId forKey:@"sizeId"];
                [skuListArray addObject:dict];
            }
        }
    }
    
    return skuListArray;
}

// 组装普通属性列表
- (NSMutableArray *)getItemPropList
{
    NSMutableArray *itemPropListArray = [NSMutableArray array];
    
    for (YDSearcherPropList *sub in self.moreDataArray) {
        NSMutableArray *selectPropArray = [NSMutableArray array];
        
        if (sub.inputType == 1 || sub.inputType == 2) {
            // 组装勾选数据
            NSMutableDictionary *defaultValue;
            for (YDSearcherPropValueList *subList in sub.propValueList) {
                if (subList.isSelected || subList.defaultFlag == 1) {
                    NSMutableDictionary *subListDict = [NSMutableDictionary dictionary];
                    [subListDict setObject:subList.propValueId forKey:@"propValueId"];
                    [subListDict setObject:subList.propValue forKey:@"propValue"];
                    
                    if (subList.isSelected) {
                        [selectPropArray addObject:subListDict];
                    }
                    
                    if (subList.defaultFlag == 1) {
                        defaultValue = subListDict;
                    }
                }
            }
            
            // 没有勾选项时，看看有没有默认值
            if (!selectPropArray.count && defaultValue) {
                [selectPropArray addObject:defaultValue];
            }
            
        } else if (sub.inputType == 3) {
            // 组装填写数据
            YDSearcherPropValueList *propList = [[YDSearcherPropValueList alloc] init];
            propList.propValue = sub.content;
            [selectPropArray addObject:propList];
        }
        
        // 开始组装属性列
        if (selectPropArray.count) {
            NSMutableDictionary *dict = [NSMutableDictionary dictionary];
            [dict setObject:sub.propNameId forKey:@"propNameId"];
            [dict setObject:sub.propName forKey:@"propName"];
            [dict setObject:[NSString stringWithFormat:@"%ld", (long)sub.inputType] forKey:@"inputType"];
            [dict setObject:selectPropArray forKey:@"propValueList"];
            [itemPropListArray addObject:dict];
        }
    }
    
    return itemPropListArray;
}

#pragma mark - 编辑商品

// 加载商品原有信息，为填充做准备
- (void)loadEditProductData
{
    [YDLoadingView showToSuperview:self.view];
    
    NSString *path = URL_PRODUCT_DETAIL;
    NSMutableDictionary *param = [NSMutableDictionary dictionary];
    [param setObject:[NSString stringWithFormat:@"%ld", (long)self.ID] forKey:@"id"];

    YDWeakSelf
    AppleServerPostCompleteBlock postHandleBlock = ^(BOOL done, id _Nullable object, NSString * _Nullable error) {
        [YDLoadingView hideToSuperview:weakSelf.view];
        
        if (done) {
            YDPorductDetailModel *resp = [YDPorductDetailModel modelWithJSON:object];
            
            if (resp.code == 200) {
                // 填充信息
                [weakSelf fillInWithProductData:resp.data];
                
                weakSelf.tableView.hidden = NO;
                [weakSelf.tableView reloadData];
            
            } else {
                NSString *msg;
                if (resp.detailMessage.length) {
                    msg = resp.detailMessage;
                    
                } else if (resp.message.length) {
                    msg = resp.message;
                    
                } else {
                    msg = @"网络失败";
                }
                [MBProgressHUD showToastMessage:msg];
            }
            
        } else {
            [MBProgressHUD showToastMessage:@"网络不给力"];
        }
    };
    
    //使用AppleServer发送POST请求
    [AppleServer.shared postWithPath:path Paramters:param CompleteBlock:postHandleBlock];
}

// 填充商品信息
- (void)fillInWithProductData:(YDSearcherProductData *)data
{
    // 图片信息
    for (YDSearcherPicList *sub in data.picList) {
        [self.photoArray addObject:sub.url];
    }
    // 款号
    self.code = data.code;
    // 销售价
    self.salesPrice = [NSString appleStandardPriceWith:data.salesPrice];
    // 批发价
    self.wholePrice = data.wholePrice;
    // 打包价
    self.packagePrice = data.packagePrice;
    // 成本价
    self.basePrice = [NSString appleStandardPriceWith:data.basePrice];
    // 商品名
    self.name = data.name;
    // 品牌
    self.brand = data.brand;
    
    // 从skuList中获取已选中的颜色和尺码
    NSMutableArray<NSString *> *colorIDArray = [NSMutableArray array];
    NSMutableArray<NSString *> *sizeIDArray = [NSMutableArray array];
    for (YDSearcherSkuList *sub in data.skuList) {
        BOOL exist = NO;
        for (NSString *colorID in colorIDArray) {
            if ([colorID isEqualToString:sub.colorId]) {
                exist = YES;
            }
        }
        
        if (!exist) {
            [colorIDArray addObject:sub.colorId];
        }
        
        exist = NO;
        for (NSString *sizeID in sizeIDArray) {
            if ([sizeID isEqualToString:sub.sizeId]) {
                exist = YES;
            }
        }
        
        if (!exist) {
            [sizeIDArray addObject:sub.sizeId];
        }
    }
    
    // 插入已选中颜色与尺码
    for (YDSearcherPropList *sub in self.baseDataArray) {
        if (sub.type == 10) {
            // 颜色
            for (YDSearcherPropValueList *propList in sub.propValueList) {
                for (NSString *colorID in colorIDArray) {
                    if ([propList.propValueId isEqualToString:colorID]) {
                        propList.isSelected = YES;
                    }
                }
            }
            
        } else if (sub.type == 11) {
            // 尺码
            for (YDSearcherPropValueList *propList in sub.propValueList) {
                for (NSString *sizeID in sizeIDArray) {
                    if ([propList.propValueId isEqualToString:sizeID]) {
                        propList.isSelected = YES;
                    }
                }
            }
        }
    }
    
    // 插入其他商品属性
    for (YDSearcherPropList *sub in self.moreDataArray) {
        for (YDSearcherPropList *propList in data.itemPropList) {
            if ([sub.propNameId isEqualToString:propList.propNameId]) {
                if (sub.inputType == 1 || sub.inputType == 2) {
                    // 数据类型是选择
                    NSMutableArray<NSString *> *propArray = [NSMutableArray array];
                    for (YDSearcherPropValueList *propValueList in propList.propValueList) {
                        [propArray addObject:propValueList.propValueId];
                    }
                    
                    for (YDSearcherPropValueList *propValueList in sub.propValueList) {
                        for (NSString *propID in propArray) {
                            if ([propValueList.propValueId isEqualToString:propID]) {
                                propValueList.isSelected = YES;
                            }
                        }
                    }
                    
                } else if (sub.inputType == 3) {
                    // 数据类型是填写
                    if (propList.propValueList.count) {
                        sub.content = propList.propValueList[0].propValue;
                    }
                }
            }
        }
    }
}

#pragma mark - UITableView代理

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 3;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    switch (section) {
        case 0:
            return 1 + self.baseDataArray.count;
            
        case 1:
            return 1 + self.moreDataArray.count;
            
        default:
            return 1;
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == 0 && indexPath.row == 0) {
        return [YDAddProductBaseInfoCell getHeightWithPhotoNum:self.photoArray.count];
        
    } else if (indexPath.section == 2) {
        return [YDAddProductConfirmCell getHeight];
        
    } else {
        return [YDAddProductFormCell getHeight];
    }
    
    return 100;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    YDWeakSelf
    if (indexPath.section == 0 && indexPath.row == 0) {
        YDAddProductBaseInfoCell *cell = [YDAddProductBaseInfoCell cellWithTableView:tableView];
        cell.photoArray = self.photoArray;
        cell.code = self.code;
        cell.salesPrice = self.salesPrice;
        cell.wholePrice = self.wholePrice;
        cell.packagePrice = self.packagePrice;
        cell.basePrice = self.basePrice;
        cell.name = self.name;
        
        cell.addNewImgBlock = ^{
            [weakSelf addPhotos];
        };
        cell.deleteImgBlock = ^(NSString *url) {
            [weakSelf deleteImgWithUrl:url];
        };
        cell.setCodeBlock = ^(NSString *code) {
            weakSelf.code = code;
        };
        cell.setSalesPriceBlock = ^(NSString *salesPrice) {
            weakSelf.salesPrice = salesPrice;
        };
        cell.setWholePriceBlock = ^(NSString *wholePrice) {
            weakSelf.wholePrice = wholePrice;
        };
        cell.setPackagePriceBlock = ^(NSString *packagePrice) {
            weakSelf.packagePrice = packagePrice;
        };
        cell.setBasePriceBlock = ^(NSString *basePrice) {
            weakSelf.basePrice = basePrice;
        };
        cell.setNameBlock = ^(NSString *name) {
            weakSelf.name = name;
        };
        
        return cell;
        
    } else if (indexPath.section == 2) {
        YDAddProductConfirmCell *cell = [YDAddProductConfirmCell cellWithTableView:tableView];
        cell.addProductConfirmBlock = ^{
            [weakSelf checkFormData];
        };
        return cell;
        
    } else {
        YDAddProductFormCell *cell = [YDAddProductFormCell cellWithTableView:tableView];
        
        YDWeakSelf
        if (indexPath.section == 1 && indexPath.row == 0) {
            // 品牌
            cell.isBrand = YES;
            cell.brand = self.brand;
            cell.setBrandBlock = ^(NSString *brand) {
                weakSelf.brand = brand;
            };
         
        } else {
            if (indexPath.section == 0) {
                cell.data = [self.baseDataArray objectAtIndex:indexPath.row - 1];
                
            } else {
                cell.data = [self.moreDataArray objectAtIndex:indexPath.row - 1];
            }
            cell.isBrand = NO;
        }
        
        return cell;
    }
    
    return [[UITableViewCell alloc] init];
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    if (section == 0 || section == 1) {
        return 40;
        
    } else {
        return 0.01;
    }
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UIView *view = [[UIView alloc] init];
    
    if (section == 0 || section == 1) {
        UILabel *label = [[UILabel alloc] init];
        NSString *title;
        if (section == 0) {
            title = @"基本信息";
            
        } else {
            title = @"更多信息";
        }
        label.text = title;
        label.textColor = ColorFromRGB(0x999999);
        label.font = [UIFont systemFontOfSize:13.0f weight:UIFontWeightMedium];
        [label sizeToFit];
        [label setOrigin:CGPointMake(24, 13)];
        [view addSubview:label];
    }
    
    return view;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    return 0.01;
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section
{
    return [[UIView alloc] init];
}

#pragma mark - 上传图片

- (void)addPhotos
{
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:nil message:nil preferredStyle:UIAlertControllerStyleActionSheet];
    
    // 需要添加此项设置，否则ipad会崩溃
    alert.popoverPresentationController.sourceView = self.view;
    alert.popoverPresentationController.sourceRect = CGRectMake(0, SCREEN_HEIGHT - 70, SCREEN_WIDTH, 70);
    
    //相机
    UIAlertAction *camera = [UIAlertAction actionWithTitle:@"拍照" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        [self showCarmera];
    }];
    
    //相册
    UIAlertAction* library = [UIAlertAction actionWithTitle:@"我的相册" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        [self showPhotoAlbum];
    }];
    
    //取消
    UIAlertAction* cancel = [UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleCancel handler:nil];
    
    [alert addAction:camera];
    [alert addAction:library];
    [alert addAction:cancel];
    
    [self.navigationController presentViewController:alert animated:YES completion:nil];
}

- (void)showCarmera
{
    if (![YDPhotoRight checkHaveCameraRight]) {
        return;
    }
    
    YDImagePickerController *ipvc = [[YDImagePickerController alloc] init];
    ipvc.delegate = self;
    ipvc.sourceType = UIImagePickerControllerSourceTypeCamera;
    ipvc.modalPresentationStyle = UIModalPresentationFullScreen;

    [self.navigationController showViewController:ipvc sender:nil];
}

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary<UIImagePickerControllerInfoKey,id> *)info
{
    UIImage *image = [info objectForKey:UIImagePickerControllerOriginalImage];

    if (image) {
        [self showCropVCWithImg:image];
    }
    
    [picker dismissViewControllerAnimated:YES completion:nil];
}

// 显示裁剪页面
- (void)showCropVCWithImg:(UIImage *)image
{
    EECropImgVC *vc = [[EECropImgVC alloc] init];
    vc.originImage = image;
    YDWeakSelf
    vc.cropImageSucceedBlock = ^(UIImage *image) {
        [weakSelf uploadImgArray:[NSArray arrayWithObject:image]];
    };
    [self.navigationController pushViewController:vc animated:YES];
}

// 上传图片
- (void)uploadImgArray:(NSArray<UIImage *> *)imgArray
{
    [YDLoadingView showToSuperview:self.view];
    
    YDWeakSelf
    [YDFunction uploadImageArray:imgArray type:1 group:1 groupStr:nil parameters:nil sucess:^(NSMutableArray<NSString *> *urlArray) {
        [YDLoadingView hideToSuperview:weakSelf.view];
        
        [weakSelf.photoArray addObjectsFromArray:urlArray];
        [weakSelf.tableView reloadData];
        
    } failure:^(NSString *msg) {
        [YDLoadingView hideToSuperview:weakSelf.view];
        [MBProgressHUD showToastMessage:msg];
    }];
}

// 显示相册选择
- (void)showPhotoAlbum
{
    if (![YDPhotoRight checkHavePhotoLibraryRight]) {
        return;
    }
    
    YDWeakSelf
    TZImagePickerController *imagePickerVc = [[TZImagePickerController alloc] initWithMaxImagesCount:ADD_PRODUCT_PHOTO_MAXCOUNT - self.photoArray.count delegate:nil];
    imagePickerVc.allowPickingOriginalPhoto = NO;
    imagePickerVc.showSelectedIndex = YES;
    imagePickerVc.naviTitleColor = ColorFromRGB(0x0095F7);
    imagePickerVc.barItemTextColor = ColorFromRGB(0x0095F7);
    imagePickerVc.allowPickingVideo = NO;
    imagePickerVc.modalPresentationStyle = UIModalPresentationFullScreen;
    [imagePickerVc setDidFinishPickingPhotosHandle:^(NSArray<UIImage *> *photos, NSArray *assets, BOOL isSelectOriginalPhoto) {
        [weakSelf uploadImgArray:photos];
    }];
    [self.navigationController presentViewController:imagePickerVc animated:YES completion:nil];
}

#pragma mark - 删除图片

- (void)deleteImgWithUrl:(NSString *)url
{
    for (NSString *sub in self.photoArray) {
        if ([sub isEqualToString:url]) {
            [self.photoArray removeObject:sub];
            break;
        }
    }
    
    [self.tableView reloadData];
}

@end

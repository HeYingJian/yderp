//
//  YDAddProductConfirmCell.m
//  YDERP
//
//  Created by 何英健 on 2021/8/3.
//

#import "YDAddProductConfirmCell.h"

@implementation YDAddProductConfirmCell

+ (instancetype)cellWithTableView:(UITableView *)tableView
{
    static NSString *reuseID = @"AddProductConfirmCell";
    YDAddProductConfirmCell *cell = [tableView dequeueReusableCellWithIdentifier:reuseID];
    if (!cell) {
        cell = [[NSBundle mainBundle] loadNibNamed:@"YDAddProductConfirmCell" owner:nil options:nil][0];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
    }

    return cell;
}

+ (CGFloat)getHeight
{
    return 102;
}

- (IBAction)tapConfirmBtn:(id)sender
{
    if (self.addProductConfirmBlock) {
        self.addProductConfirmBlock();
    }
}

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end

//
//  YDAddProductConfirmCell.h
//  YDERP
//
//  Created by 何英健 on 2021/8/3.
//

#import <UIKit/UIKit.h>

typedef void(^BlockAddProductConfirm)(void);

@interface YDAddProductConfirmCell : UITableViewCell

+ (instancetype)cellWithTableView:(UITableView *)tableView;

+ (CGFloat)getHeight;

@property (nonatomic, copy) BlockAddProductConfirm addProductConfirmBlock;

@end

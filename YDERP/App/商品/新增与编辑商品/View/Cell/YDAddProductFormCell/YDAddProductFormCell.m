//
//  YDAddProductFormCell.m
//  YDERP
//
//  Created by 何英健 on 2021/8/3.
//

#import "YDAddProductFormCell.h"

@interface YDAddProductFormCell () <UITextFieldDelegate>

@property (nonatomic, weak) IBOutlet UILabel *titleLabel;
// 填写用
@property (nonatomic, weak) IBOutlet UITextField *writeTextField;
@property (nonatomic, weak) IBOutlet UIView *selectBGView;
// 选择用
@property (nonatomic, weak) IBOutlet UITextField *selectTextField;

@property (nonatomic, strong) YDAddProductSelectedView *selectedView;

@end

@implementation YDAddProductFormCell

+ (instancetype)cellWithTableView:(UITableView *)tableView
{
    static NSString *reuseID = @"AddProductFormCell";
    YDAddProductFormCell *cell = [tableView dequeueReusableCellWithIdentifier:reuseID];
    if (!cell) {
        cell = [[NSBundle mainBundle] loadNibNamed:@"YDAddProductFormCell" owner:nil options:nil][0];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        
        [[NSNotificationCenter defaultCenter] addObserver:cell selector:@selector(dismissKeyboard) name:SHOW_ADD_PROPVIEW object:nil];
    }

    return cell;
}

+ (CGFloat)getHeight
{
    return 65;
}

- (void)setData:(YDSearcherPropList *)data
{
    _data = data;
    
    if (data.inputType == 3) {
        // 需要输入
        self.writeTextField.hidden = NO;
        self.selectBGView.hidden = YES;
        
        self.writeTextField.placeholder = data.statement;
        
    } else {
        // 需要选择
        self.writeTextField.hidden = YES;
        self.selectBGView.hidden = NO;
        
        self.selectTextField.placeholder = data.statement;
    }
    
    self.titleLabel.text = data.propName;
    [self refreshTextField];
}

- (void)setIsBrand:(BOOL)isBrand
{
    _isBrand = isBrand;
    
    if (isBrand) {
        self.writeTextField.hidden = NO;
        self.selectBGView.hidden = YES;
        
        self.titleLabel.text = @"品牌";
        self.writeTextField.placeholder = @"请输入品牌";
    }
}

- (void)setBrand:(NSString *)brand
{
    _brand = brand;
    
    self.writeTextField.text = brand;
}

- (void)showSelectView
{
    YDWeakSelf
    [YDAddProductSelectedView showWithData:self.data completion:^{
        [weakSelf refreshTextField];
    }];
}

- (void)refreshTextField
{
    if (self.data.inputType == 3) {
        self.writeTextField.text = self.data.content;
        
    } else {
        NSString *str;
        for (YDSearcherPropValueList *sub in self.data.propValueList) {
            if (sub.isSelected) {
                if (str.length) {
                    str = [NSString stringWithFormat:@"%@、%@", str, sub.propValue];
                    
                } else {
                    str = sub.propValue;
                }
            }
        }
        self.selectTextField.text = str;
    }
}

- (void)dismissKeyboard
{
    [self.writeTextField resignFirstResponder];
    [self.selectTextField resignFirstResponder];
}

- (void)awakeFromNib
{
    [super awakeFromNib];
    
    UITapGestureRecognizer *tapSelectView = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(showSelectView)];
    [self.selectBGView addGestureRecognizer:tapSelectView];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

#pragma mark - UITextFieldDelegate

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];

    return YES;
}

- (BOOL)textFieldShouldEndEditing:(UITextField *)textField
{
    self.writeTextField.text = textField.text;
    
    // 设置品牌
    if (self.isBrand) {
        if (self.setBrandBlock) {
            self.setBrandBlock(textField.text);
        }
        
    } else {
        self.data.content = textField.text;
    }
    
    return YES;
}

@end

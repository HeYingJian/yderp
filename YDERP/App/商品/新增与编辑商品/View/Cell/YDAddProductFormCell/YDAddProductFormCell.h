//
//  YDAddProductFormCell.h
//  YDERP
//
//  Created by 何英健 on 2021/8/3.
//

#import <UIKit/UIKit.h>
#import "YDSearcherPropListModel.h"
#import "YDAddProductSelectedView.h"

typedef void(^BlockAddProductSetBrand)(NSString *brand);

@interface YDAddProductFormCell : UITableViewCell

+ (instancetype)cellWithTableView:(UITableView *)tableView;

+ (CGFloat)getHeight;

@property (nonatomic, strong) YDSearcherPropList *data;

// 是否品牌
@property (nonatomic, assign) BOOL isBrand;
// 设置品牌
@property (nonatomic, copy) NSString *brand;
// 设置品牌回调
@property (nonatomic, copy) BlockAddProductSetBrand setBrandBlock;

@end

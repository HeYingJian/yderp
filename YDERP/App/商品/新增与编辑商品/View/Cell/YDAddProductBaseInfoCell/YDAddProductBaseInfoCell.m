//
//  YDAddProductBaseInfoCell.m
//  YDERP
//
//  Created by 何英健 on 2021/8/3.
//

#import "YDAddProductBaseInfoCell.h"
#import "YDAddProductColCell.h"

@interface YDAddProductBaseInfoCell () <UITextFieldDelegate, UICollectionViewDelegate, UICollectionViewDataSource, WDImageBrowserDelegate>

@property (nonatomic, weak) IBOutlet UICollectionView *collectionView;

// 款号
@property (nonatomic, weak) IBOutlet UITextField *codeTextField;
// 销售价
@property (nonatomic, weak) IBOutlet UITextField *salesPriceTextField;
// 批发价
@property (nonatomic, weak) IBOutlet UITextField *wholePriceTextField;
// 打包价
@property (nonatomic, weak) IBOutlet UITextField *packagePriceTextField;
// 成本价
@property (nonatomic, weak) IBOutlet UITextField *basePriceTextField;
// 商品名
@property (nonatomic, weak) IBOutlet UITextField *nameTextField;

@end

@implementation YDAddProductBaseInfoCell

+ (instancetype)cellWithTableView:(UITableView *)tableView
{
    static NSString *reuseID = @"BaseInfoCell";
    YDAddProductBaseInfoCell *cell = [tableView dequeueReusableCellWithIdentifier:reuseID];
    if (!cell) {
        cell = [[NSBundle mainBundle] loadNibNamed:@"YDAddProductBaseInfoCell" owner:nil options:nil][0];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        cell.collectionView.showsHorizontalScrollIndicator = NO;
        
        UICollectionViewFlowLayout *layout = [[UICollectionViewFlowLayout alloc] init];
        // cell宽度
        CGFloat cellWidth = [YDAddProductBaseInfoCell getCollectionViewCellBorder];
        // cell与cell之间间距
        layout.minimumLineSpacing = 9;
        layout.itemSize = CGSizeMake(cellWidth, cellWidth);
        cell.collectionView.collectionViewLayout = layout;
        
        [cell.collectionView registerClass:[YDAddProductColCell class] forCellWithReuseIdentifier:@"AddProductColCell"];
        
        [[NSNotificationCenter defaultCenter] addObserver:cell selector:@selector(dismissKeyboard) name:SHOW_ADD_PROPVIEW object:nil];
    }

    return cell;
}

+ (CGFloat)getHeightWithPhotoNum:(NSInteger)photoNum
{
    NSInteger rowNum = (photoNum + 1) / 4;
    if (rowNum * 4 < photoNum + 1) {
        rowNum++;
    }
    
    CGFloat cellH = [YDAddProductBaseInfoCell getCollectionViewCellBorder];
    CGFloat colViewH = rowNum * cellH + (rowNum - 1) * 9;
    
    return 51 + colViewH + 407;
}

+ (CGFloat)getCollectionViewCellBorder
{
    return (SCREEN_WIDTH - 24 * 2 - 9 * 3) / 4 - 1;
}

- (void)awakeFromNib
{
    [super awakeFromNib];

    self.salesPriceTextField.font = FONT_NUMBER(15);
    self.basePriceTextField.font = FONT_NUMBER(15);
    
    NSMutableAttributedString *salesStr = [[NSMutableAttributedString alloc] initWithString:@"请输入销售价" attributes:
         @{NSFontAttributeName:[UIFont systemFontOfSize:15]}];
    self.salesPriceTextField.attributedPlaceholder = salesStr;
    
    NSMutableAttributedString *wholeStr = [[NSMutableAttributedString alloc] initWithString:@"请输入批发价" attributes:
         @{NSFontAttributeName:[UIFont systemFontOfSize:15]}];
    self.wholePriceTextField.attributedPlaceholder = wholeStr;
    
    NSMutableAttributedString *packageStr = [[NSMutableAttributedString alloc] initWithString:@"请输入打包价" attributes:
         @{NSFontAttributeName:[UIFont systemFontOfSize:15]}];
    self.packagePriceTextField.attributedPlaceholder = packageStr;
    
    NSMutableAttributedString *baseStr = [[NSMutableAttributedString alloc] initWithString:@"请输入成本价" attributes:
         @{NSFontAttributeName:[UIFont systemFontOfSize:15]}];
    self.basePriceTextField.attributedPlaceholder = baseStr;
    
    [self.salesPriceTextField addToolSenderWithBlock:nil];
    [self.wholePriceTextField addToolSenderWithBlock:nil];
    [self.packagePriceTextField addToolSenderWithBlock:nil];
    [self.basePriceTextField addToolSenderWithBlock:nil];
}

- (void)dismissKeyboard
{
    [self.codeTextField resignFirstResponder];
    [self.nameTextField resignFirstResponder];
    [self.basePriceTextField resignFirstResponder];
    [self.wholePriceTextField resignFirstResponder];
    [self.packagePriceTextField resignFirstResponder];
    [self.salesPriceTextField resignFirstResponder];
}

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

#pragma mark - 设置

- (void)setCode:(NSString *)code
{
    _code = code;
    
    self.codeTextField.text = code;
}

- (void)setSalesPrice:(NSString *)salesPrice
{
    _salesPrice = salesPrice;
    
    if (salesPrice.length) {
        self.salesPriceTextField.text = [NSString stringWithFormat:@"¥%@", salesPrice];
        
    } else {
        self.salesPriceTextField.text = nil;
    }
}

- (void)setWholePrice:(NSString *)wholePrice
{
    _wholePrice = wholePrice;
    
    if (wholePrice.length) {
        self.wholePriceTextField.text = [NSString stringWithFormat:@"¥%@", wholePrice];
        
    } else {
        self.wholePriceTextField.text = nil;
    }
}

- (void)setPackagePrice:(NSString *)packagePrice
{
    _packagePrice = packagePrice;
    
    if (packagePrice.length) {
        self.packagePriceTextField.text = [NSString stringWithFormat:@"¥%@", packagePrice];
        
    } else {
        self.packagePriceTextField.text = nil;
    }
}

- (void)setBasePrice:(NSString *)basePrice
{
    _basePrice = basePrice;
    
    if (basePrice.length) {
        self.basePriceTextField.text = [NSString stringWithFormat:@"¥%@", basePrice];
        
    } else {
        self.basePriceTextField.text = nil;
    }
}

- (void)setName:(NSString *)name
{
    _name = name;
    
    self.nameTextField.text = name;
}

#pragma mark - UICollectionView 代理

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    if (self.photoArray.count >= ADD_PRODUCT_PHOTO_MAXCOUNT) {
        return self.photoArray.count;
        
    } else {
        return self.photoArray.count + 1;
    }
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    YDAddProductColCell *cell = [YDAddProductColCell cellWithTableView:collectionView indexPath:indexPath];
    if (indexPath.row != 0 || self.photoArray.count >= ADD_PRODUCT_PHOTO_MAXCOUNT) {
        NSInteger photoIndex = 0;
        if (self.photoArray.count >= ADD_PRODUCT_PHOTO_MAXCOUNT) {
            photoIndex = indexPath.row;
            
        } else {
            photoIndex = indexPath.row - 1;
        }
        cell.picUrl = self.photoArray[photoIndex];
        
        YDWeakSelf
        cell.tapDeleteBtnBlock = ^(NSString *url) {
            if (weakSelf.deleteImgBlock) {
                weakSelf.deleteImgBlock(url);
            }
        };
        
        cell.showDeleteBtn = YES;
        
    } else {
        cell.showDeleteBtn = NO;
    }
    
    return cell;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row == 0 && self.photoArray.count < ADD_PRODUCT_PHOTO_MAXCOUNT) {
        if (self.addNewImgBlock) {
            self.addNewImgBlock();
        }
        
    } else {
        UICollectionViewCell *cell = [self.collectionView cellForItemAtIndexPath:indexPath];
        
        NSInteger tapIndex;
        if (self.photoArray.count < ADD_PRODUCT_PHOTO_MAXCOUNT) {
            tapIndex = indexPath.row - 1;
            
        } else {
            tapIndex = indexPath.row;
        }
        
        // 浏览图片相册
        WDImageBrowser *browser = [[WDImageBrowser alloc] init];
        [browser setupWithDelegate:self tappedIndex:tapIndex imageUrls:self.photoArray originView:cell];
        [browser showBrowser];
    }
}

#pragma mark - UITextFieldDelegate

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];

    return YES;
}

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    if (textField == self.salesPriceTextField || textField == self.wholePriceTextField || textField == self.packagePriceTextField || textField == self.basePriceTextField) {
        textField.text = [textField.text stringByReplacingOccurrencesOfString:@"¥" withString:@""];
    }
    
    return YES;
}

- (BOOL)textFieldShouldEndEditing:(UITextField *)textField
{
    if (textField == self.codeTextField) {
        if (self.setCodeBlock) {
            self.setCodeBlock(textField.text);
        }
        
    } else if (textField == self.salesPriceTextField) {
        if (self.setSalesPriceBlock) {
            self.setSalesPriceBlock(textField.text);
        }
        
    } else if (textField == self.wholePriceTextField) {
        if (self.setWholePriceBlock) {
            self.setWholePriceBlock(textField.text);
        }
        
    } else if (textField == self.packagePriceTextField) {
        if (self.setPackagePriceBlock) {
            self.setPackagePriceBlock(textField.text);
        }
        
    } else if (textField == self.basePriceTextField) {
        if (self.setBasePriceBlock) {
            self.setBasePriceBlock(textField.text);
        }
        
    } else if (textField == self.nameTextField) {
        if (self.setNameBlock) {
            self.setNameBlock(textField.text);
        }
    }
    
    if ((textField == self.salesPriceTextField || textField == self.wholePriceTextField || textField == self.packagePriceTextField || textField == self.basePriceTextField) && textField.text.length) {
        NSString *str = [NSString appleStandardPriceWith:[textField.text doubleValue]];
        textField.text = [NSString stringWithFormat:@"¥%@", str];
    }
    
    return YES;
}

@end

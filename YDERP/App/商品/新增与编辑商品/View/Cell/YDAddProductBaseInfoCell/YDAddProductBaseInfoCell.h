//
//  YDAddProductBaseInfoCell.h
//  YDERP
//
//  Created by 何英健 on 2021/8/3.
//

#import <UIKit/UIKit.h>

#define ADD_PRODUCT_PHOTO_MAXCOUNT 30

typedef void(^BlockAddNewImg)(void);
typedef void(^BlockDeleteImg)(NSString *url);
typedef void(^BlockAddProductSetCode)(NSString *code);
typedef void(^BlockAddProductSetSalesPrice)(NSString *salesPrice);
typedef void(^BlockAddProductSetWholePrice)(NSString *wholePrice);
typedef void(^BlockAddProductSetPackagePrice)(NSString *packagePrice);
typedef void(^BlockAddProductSetBasePrice)(NSString *basePrice);
typedef void(^BlockAddProductSetName)(NSString *name);

@interface YDAddProductBaseInfoCell : UITableViewCell

+ (instancetype)cellWithTableView:(UITableView *)tableView;

+ (CGFloat)getHeightWithPhotoNum:(NSInteger)photoNum;

// 图片地址存放数组
@property (nonatomic, strong) NSMutableArray<NSString *> *photoArray;
// 添加新图片
@property (nonatomic, copy) BlockAddNewImg addNewImgBlock;
// 删除图片
@property (nonatomic, copy) BlockDeleteImg deleteImgBlock;

// 设置款号
@property (nonatomic, copy) NSString *code;
// 设置款号回调
@property (nonatomic, copy) BlockAddProductSetCode setCodeBlock;

// 设置销售价
@property (nonatomic, copy) NSString *salesPrice;
// 设置销售价回调
@property (nonatomic, copy) BlockAddProductSetSalesPrice setSalesPriceBlock;

// 设置批发价
@property (nonatomic, copy) NSString *wholePrice;
// 设置批发价回调
@property (nonatomic, copy) BlockAddProductSetWholePrice setWholePriceBlock;

// 设置打包价
@property (nonatomic, copy) NSString *packagePrice;
// 设置打包价回调
@property (nonatomic, copy) BlockAddProductSetPackagePrice setPackagePriceBlock;

// 设置成本价
@property (nonatomic, copy) NSString *basePrice;
// 设置成本价回调
@property (nonatomic, copy) BlockAddProductSetBasePrice setBasePriceBlock;

// 设置商品名
@property (nonatomic, copy) NSString *name;
// 设置商品名回调
@property (nonatomic, copy) BlockAddProductSetName setNameBlock;

@end

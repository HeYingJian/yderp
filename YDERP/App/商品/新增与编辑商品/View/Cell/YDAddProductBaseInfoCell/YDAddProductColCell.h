//
//  YDAddProductColCell.h
//  YDERP
//
//  Created by 何英健 on 2021/8/6.
//

#import <UIKit/UIKit.h>

typedef void(^BlockCellTapDeleteBtn)(NSString *url);

@interface YDAddProductColCell : UICollectionViewCell

@property (nonatomic, copy) NSString *picUrl;

@property (nonatomic, assign) BOOL showDeleteBtn;

@property (nonatomic, copy) BlockCellTapDeleteBtn tapDeleteBtnBlock;

+ (instancetype)cellWithTableView:(UICollectionView *)collectionView indexPath:(NSIndexPath *)indexPath;

@end

//
//  YDAddProductColCell.m
//  YDERP
//
//  Created by 何英健 on 2021/8/6.
//

#import "YDAddProductColCell.h"

@interface YDAddProductColCell ()

@property (nonatomic, weak) IBOutlet UIImageView *imgView;
@property (nonatomic, weak) IBOutlet UIButton *deleteBtn;

@end

@implementation YDAddProductColCell

+ (instancetype)cellWithTableView:(UICollectionView *)collectionView indexPath:(NSIndexPath *)indexPath
{
    static NSString *reuseID = @"AddProductColCell";
    YDAddProductColCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:reuseID forIndexPath:indexPath];
    
    return cell;
}

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        self = [[NSBundle mainBundle] loadNibNamed:@"YDAddProductColCell" owner:self options:nil].lastObject;
    }
    return self;
}

- (void)setPicUrl:(NSString *)picUrl
{
    _picUrl = picUrl;
    
    [self.imgView appleSetImageWithUrl:picUrl SprcifyOssImageStyle:@"products_list" HasHD:NO placeholderImage:@"默认商品方形图" completion:nil];
}

- (void)setShowDeleteBtn:(BOOL)showDeleteBtn
{
    _showDeleteBtn = showDeleteBtn;
    
    if (showDeleteBtn) {
        self.deleteBtn.hidden = NO;
        
    } else {
        [self.imgView setImage:[UIImage imageNamed:@"添加图片按钮"]];
        self.deleteBtn.hidden = YES;
    }
}

- (IBAction)tapDeleteBtn:(id)sender
{
    if (self.tapDeleteBtnBlock) {
        self.tapDeleteBtnBlock(self.picUrl);
    }
}

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

@end

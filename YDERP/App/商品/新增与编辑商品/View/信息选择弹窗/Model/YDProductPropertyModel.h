//
//  YDProductPropertyModel.h
//  YDERP
//
//  Created by 何英健 on 2021/8/5.
//

#import "YDBaseModel.h"

@interface YDProductPropertyData : NSObject

@property (nonatomic, assign) NSInteger ID;
@property (nonatomic, assign) NSInteger propNameId;
@property (nonatomic, copy) NSString *value;

@end

@interface YDProductPropertyModel : YDBaseModel

@property (nonatomic, strong) NSMutableArray<YDProductPropertyData *> *data;

@end

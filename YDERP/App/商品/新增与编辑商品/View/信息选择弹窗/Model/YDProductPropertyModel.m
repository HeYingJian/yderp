//
//  YDProductPropertyModel.m
//  YDERP
//
//  Created by 何英健 on 2021/8/5.
//

#import "YDProductPropertyModel.h"

@implementation YDProductPropertyData

+ (NSDictionary<NSString *,id> *)modelCustomPropertyMapper
{
    return @{@"ID":@"id"};
}

@end

@implementation YDProductPropertyModel

+ (NSDictionary<NSString *,id> *)modelContainerPropertyGenericClass{
    return @{@"data" : [YDProductPropertyData class]
            };
}

@end

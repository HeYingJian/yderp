//
//  YDAddProductSelectedView.h
//  YDERP
//
//  Created by 何英健 on 2021/8/4.
//

#import <UIKit/UIKit.h>
#import "YDSearcherPropListModel.h"

typedef void(^BlockAddProductSelectedComplete)(void);

@interface YDAddProductSelectedView : UIView

+ (void)showWithData:(YDSearcherPropList *)data completion:(BlockAddProductSelectedComplete)completion;

@end

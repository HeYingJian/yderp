//
//  YDAddProductSelectedView.m
//  YDERP
//
//  Created by 何英健 on 2021/8/4.
//

#import "YDAddProductSelectedView.h"
#import "YDProductPropertyModel.h"

@interface YDAddProductSelectedView () <UITextViewDelegate>

@property (nonatomic, strong) YDSearcherPropList *data;
@property (nonatomic, copy) BlockAddProductSelectedComplete completion;

// 保存按钮
@property (nonatomic, strong) NSMutableArray<UIButton *> *btnArray;
// 保存进来时候的初始选择
@property (nonatomic, strong) NSMutableArray<YDSearcherPropValueList *> *originSelectedArray;

@property (nonatomic, assign) BOOL isAddProp;

@property (nonatomic, weak) IBOutlet UIView *mainView;
@property (nonatomic, weak) IBOutlet UILabel *titleLabel;
@property (nonatomic, weak) IBOutlet UIView *selectBGView;
@property (nonatomic, strong) UIScrollView *scrollView;

@property (nonatomic, weak) IBOutlet UIView *addPropView;
@property (nonatomic, weak) IBOutlet UITextView *addPropTextView;

@property (nonatomic, weak) IBOutlet NSLayoutConstraint *mainViewTop;
@property (nonatomic, weak) IBOutlet NSLayoutConstraint *mainViewH;
@property (nonatomic, weak) IBOutlet NSLayoutConstraint *addPropViewTop;
@property (nonatomic, weak) IBOutlet NSLayoutConstraint *addPropViewH;

@end

@implementation YDAddProductSelectedView

#pragma mark - 懒加载

- (NSMutableArray<UIButton *> *)btnArray
{
    if (!_btnArray) {
        _btnArray = [NSMutableArray array];
    }
    
    return _btnArray;
}

- (NSMutableArray<YDSearcherPropValueList *> *)originSelectedArray
{
    if (!_originSelectedArray) {
        _originSelectedArray = [NSMutableArray array];
    }
    
    return _originSelectedArray;
}

#pragma mark - 页面加载

+ (void)showWithData:(YDSearcherPropList *)data completion:(BlockAddProductSelectedComplete)completion
{
    YDAddProductSelectedView *view = [[NSBundle mainBundle] loadNibNamed:@"YDAddProductSelectedView" owner:nil options:nil][0];
    view.frame = CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT);
    
    [view initUI];
    [view addNotification];
    
    view.data = data;
    view.completion = completion;
    
    // 保存刚进来时候的选择
    [view saveOriginSelected];
    
    [KeyWindow addSubview:view];
    
    PostNotification(SHOW_ADD_PROPVIEW, nil);

    dispatch_after(0, dispatch_get_main_queue(), ^{
        [UIView animateWithDuration:0.3 animations:^{
            view.mainViewTop.constant = 157;
            [view layoutIfNeeded];
        }];
    });
}

- (void)initUI
{
    self.mainView.hidden = NO;
    self.addPropView.hidden = YES;
    
    self.mainViewTop.constant = SCREEN_HEIGHT;
    self.mainViewH.constant = SCREEN_HEIGHT - 157;
    self.addPropViewTop.constant = SCREEN_HEIGHT;
    self.addPropViewH.constant = 160;
    [self layoutIfNeeded];
    
    // 添加上半部分圆角
    [self.mainView addRoundedCorners:UIRectCornerTopLeft|UIRectCornerTopRight withRadii:CGSizeMake(20, 20) viewRect:CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT - 157)];
    [self.addPropView addRoundedCorners:UIRectCornerTopLeft|UIRectCornerTopRight withRadii:CGSizeMake(20, 20) viewRect:CGRectMake(0, 0, SCREEN_WIDTH, 160)];
    
    self.scrollView = [[UIScrollView alloc] init];
    [self.selectBGView addSubview:_scrollView];
    
    [_scrollView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.leading.mas_equalTo(0);
        make.bottom.mas_equalTo(0);
        make.top.mas_equalTo(0);
        make.trailing.mas_equalTo(0);
    }];
}

- (void)setData:(YDSearcherPropList *)data
{
    _data = data;
    
    self.titleLabel.text = data.propName;
    
    [self refreshScrollView];
}

- (void)refreshScrollView
{
    [self.scrollView removeAllSubviews];
    [self.btnArray removeAllObjects];
    
    NSInteger count = self.data.propValueList.count;
    if (self.data.editAble == 0) {
        count++;
    }
    for (int i = 0; i < count; i++) {
        UIButton *btn = [self createBtnWithIndex:i];
        [self.scrollView addSubview:btn];
        
        if (i == count - 1) {
            [self.scrollView setContentSize:CGSizeMake(self.scrollView.width, CGRectGetMaxY(btn.frame))];
        }
        
        [self.btnArray addObject:btn];
    }
}

- (UIButton *)createBtnWithIndex:(NSInteger)index
{
    UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
    btn.tag = index;
    [btn setWidth:(SCREEN_WIDTH - 17 * 2 - 12 * 2) / 3];
    [btn setHeight:37];
    [btn setY:index / 3 * (37 + 12)];
    [btn setX:index%3 * (12 + btn.width)];
    btn.titleLabel.lineBreakMode = NSLineBreakByWordWrapping;
    btn.titleLabel.textAlignment = NSTextAlignmentCenter;
    NSString *str;
    if (index == 0 && self.data.editAble == 0) {
        str = @"添加";
        [btn setImage:[UIImage imageNamed:@"新增商品加号"] forState:UIControlStateNormal];
        [btn setImageEdgeInsets:UIEdgeInsetsMake(0, 0, 0, 5)];
        [btn setBackgroundImage:[UIImage imageNamed:@"新增商品添加按钮"] forState:UIControlStateNormal];
        btn.backgroundColor = [UIColor whiteColor];
        btn.titleLabel.font = [UIFont systemFontOfSize:13.0f];
        [btn setTarget:self action:@selector(tapAddBtn:) forControlEvents:UIControlEventTouchUpInside];
        
    } else {
        YDSearcherPropValueList *btnData;
        if (self.data.editAble == 0) {
            btnData = self.data.propValueList[index - 1];
            
        } else {
            btnData = self.data.propValueList[index];
        }

        NSMutableString *mutableStr = [NSMutableString stringWithString:btnData.propValue];
        if (mutableStr.length > 5) {
            [mutableStr insertString:@"\n" atIndex:5];
        }
        str = mutableStr;
        btn.layer.borderColor = ColorFromRGB(0xFF5630).CGColor;
        [btn setTarget:self action:@selector(tapSelectBtn:) forControlEvents:UIControlEventTouchUpInside];
        [self setBtnStatus:btn selected:btnData.isSelected];
    }
    [btn setTitle:str forState:UIControlStateNormal];
    btn.layer.cornerRadius = btn.height / 2;
    [btn setTitleColor:ColorFromRGB(0x222222) forState:UIControlStateNormal];
    [btn setTitleColor:ColorFromRGB(0xFF5630) forState:UIControlStateSelected];
    
    return btn;
}

// 改变按钮选择状态
- (void)setBtnStatus:(UIButton *)btn selected:(BOOL)selected
{
    YDSearcherPropValueList *btnData;
    if (self.data.editAble == 0) {
        btnData = self.data.propValueList[btn.tag - 1];
        
    } else {
        btnData = self.data.propValueList[btn.tag];
    }
    
    if (selected) {
        btn.titleLabel.font = [UIFont systemFontOfSize:13.0f weight:UIFontWeightMedium];
        btn.backgroundColor = ColorFromRGB(0xFFF6F2);
        btn.layer.borderWidth = 1;
        
    } else {
        btn.titleLabel.font = [UIFont systemFontOfSize:13.0f];
        btn.backgroundColor = ColorFromRGB(0xF4F4F4);
        btn.layer.borderWidth = 0;
    }
    
    btn.selected = selected;
    btnData.isSelected = selected;
}

// 保存刚进来时候的选择
- (void)saveOriginSelected
{
    [self.originSelectedArray removeAllObjects];
    
    for (YDSearcherPropValueList *sub in self.data.propValueList) {
        if (sub.isSelected) {
            [self.originSelectedArray addObject:sub];
        }
    }
}

// 刷新当前属性值列表
- (void)refreshData
{
    [self refreshPropList];
}

- (void)hide
{
    [UIView animateWithDuration:0.3 animations:^{
        self.mainViewTop.constant = SCREEN_HEIGHT;
        [self layoutIfNeeded];
        
    } completion:^(BOOL finished) {
        [self removeFromSuperview];
    }];
}

#pragma mark - 数据请求

- (void)uploadAddProp
{
    [YDLoadingView showToSuperview:self];
    
    NSString *path = URL_PRODUCT_ADD_PROPERTY;
    NSMutableDictionary *param = [NSMutableDictionary dictionary];
    [param setObject:[NSString stringWithFormat:@"%ld", (long)UserModel.shopId] forKey:@"shopId"];
    [param setObject:self.data.propNameId forKey:@"propNameId"];
    [param setObject:self.addPropTextView.text forKey:@"value"];

    YDWeakSelf
    AppleServerPostCompleteBlock postHandleBlock = ^(BOOL done, id _Nullable object, NSString * _Nullable error) {
        [YDLoadingView hideToSuperview:weakSelf];
        if (done) {
            YDBaseModel *resp = [YDBaseModel modelWithJSON:object];
            
            if (resp.code == 200) {
                [MBProgressHUD showToastMessage:@"添加成功"];
                
                // 通知商品首页刷新筛选里面的分类
                PostNotification(REFRESH_PRODUCT_FILTER, nil);
                
                [weakSelf hideAddPropView];
                
                [weakSelf refreshData];
            
            } else {
                NSString *msg;
                if (resp.detailMessage.length) {
                    msg = resp.detailMessage;
                    
                } else if (resp.message.length) {
                    msg = resp.message;
                    
                } else {
                    msg = @"网络失败";
                }
                [MBProgressHUD showToastMessage:msg];
            }
            
        } else {
            [MBProgressHUD showToastMessage:@"网络不给力"];
        }
    };
    
    //使用AppleServer发送POST请求
    [AppleServer.shared postWithPath:path Paramters:param CompleteBlock:postHandleBlock];
}

- (void)refreshPropList
{
    [YDLoadingView showToSuperview:self];
    
    NSString *path = URL_ADD_PRODUCT_PROPERTY;
    NSMutableDictionary *param = [NSMutableDictionary dictionary];

    YDWeakSelf
    AppleServerPostCompleteBlock postHandleBlock = ^(BOOL done, id _Nullable object, NSString * _Nullable error) {
        [YDLoadingView hideToSuperview:weakSelf];
        if (done) {
            YDSearcherPropListModel *resp = [YDSearcherPropListModel modelWithJSON:object];
            
            if (resp.code == 200) {
                for (YDSearcherPropList *sub in resp.data) {
                    if ([sub.propNameId isEqualToString:self.data.propNameId]) {
                        for (YDSearcherPropValueList *newPropData in sub.propValueList) {
                            // 把旧的勾选选项设置回去
                            for (YDSearcherPropValueList *oldPropData in weakSelf.data.propValueList) {
                                if ([newPropData.propValueId isEqualToString:oldPropData.propValueId] && oldPropData.isSelected) {
                                    newPropData.isSelected = YES;
                                }
                            }
                        }
                        
                        weakSelf.data.propValueList = sub.propValueList;
                    }
                }
                
                [weakSelf refreshScrollView];
            
            } else {
                NSString *msg;
                if (resp.detailMessage.length) {
                    msg = resp.detailMessage;
                    
                } else if (resp.message.length) {
                    msg = resp.message;
                    
                } else {
                    msg = @"网络失败";
                }
                [MBProgressHUD showToastMessage:msg];
            }
            
        } else {
            [MBProgressHUD showToastMessage:@"网络不给力"];
        }
    };
    
    [AppleServer.shared getWithUrl:path Paramters:param Callback:postHandleBlock];
}

#pragma mark - 点击事件

- (IBAction)tapCloseBtn:(id)sender
{
    // 还原为初始选择状态
    for (YDSearcherPropValueList *sub in self.data.propValueList) {
        if ([self.originSelectedArray containsObject:sub]) {
            sub.isSelected = YES;
            
        } else {
            sub.isSelected = NO;
        }
    }
    
    [self hide];
}

- (IBAction)tapAddPropCloseBtn:(id)sender
{
    [self hideAddPropView];
}

// 隐藏添加属性弹窗
- (void)hideAddPropView
{
    self.isAddProp = NO;
    [self.addPropTextView resignFirstResponder];
    
    [UIView animateWithDuration:0.3 animations:^{
        self.addPropViewTop.constant = SCREEN_HEIGHT;
        [self layoutIfNeeded];
        
    } completion:^(BOOL finished) {
        self.mainView.hidden = NO;
        self.addPropView.hidden = YES;
        
        [UIView animateWithDuration:0.3 animations:^{
            self.mainViewTop.constant = 157;
            [self layoutIfNeeded];
        }];
    }];
}

// 添加更多
- (void)tapAddBtn:(UIButton *)btn
{
    self.addPropTextView.text = nil;
    
    [UIView animateWithDuration:0.3 animations:^{
        self.mainViewTop.constant = SCREEN_HEIGHT;
        [self layoutIfNeeded];
        
    } completion:^(BOOL finished) {
        self.mainView.hidden = YES;
        self.addPropView.hidden = NO;
        self.isAddProp = YES;
        [self.addPropTextView becomeFirstResponder];
    }];
}

- (void)tapSelectBtn:(UIButton *)btn
{
    if (self.data.inputType == 1) {
        // 单选
        for (int i = 0; i < self.btnArray.count; i++) {
            if (i != 0 || self.data.editAble != 0) {
                [self setBtnStatus:self.btnArray[i] selected:NO];
            }
        }
    }
    [self setBtnStatus:btn selected:!btn.selected];
}

- (IBAction)tapConfirmBtn:(id)sender
{
    if (self.completion) {
        self.completion();
    }
    
    [self hide];
}

#pragma mark - UITextViewDelegate

- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text
{
    if ([text isEqualToString:@"\n"]) {
        if (!textView.text.length) {
            [MBProgressHUD showToastMessage:@"内容不能为空"];
            return NO;
            
        } else if (textView.text.length > 10) {
            [MBProgressHUD showToastMessage:@"不能超过10个字"];
            return NO;
        }
        
        [self uploadAddProp];
        
        return NO;
    }
    
    return YES;
}

#pragma mark - 键盘高度变化

- (void)addNotification
{
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardShow:) name:UIKeyboardWillShowNotification object:nil];
}

// 弹出键盘改变控制器view
- (void)keyboardShow:(NSNotification *)noti
{
    if (self.isAddProp) {
        CGFloat keyboardHeight = [noti.userInfo[UIKeyboardFrameEndUserInfoKey] CGRectValue].size.height;
        [UIView animateWithDuration:0.0f animations:^{
            self.addPropViewTop.constant = SCREEN_HEIGHT - 160 - keyboardHeight;
            [self layoutIfNeeded];
        }];
    }
}

@end

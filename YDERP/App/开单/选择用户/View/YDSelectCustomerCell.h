//
//  YDSelectCustomerCell.h
//  YDERP
//
//  Created by 何英健 on 2021/8/12.
//

#import <UIKit/UIKit.h>
#import "YDSearcherCustomerModel.h"

@interface YDSelectCustomerCell : UITableViewCell

+ (instancetype)cellWithTableView:(UITableView *)tableView;

+ (CGFloat)getHeight;

@property (nonatomic, strong) YDSearcherCustomerData *data;

@end

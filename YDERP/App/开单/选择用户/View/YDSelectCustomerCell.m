//
//  YDSelectCustomerCell.m
//  YDERP
//
//  Created by 何英健 on 2021/8/12.
//

#import "YDSelectCustomerCell.h"

@interface YDSelectCustomerCell ()

@property (nonatomic, weak) IBOutlet UILabel *nameLabel;
@property (nonatomic, weak) IBOutlet UILabel *phoneLabel;
@property (nonatomic, weak) IBOutlet UILabel *goodsOweNumLabel;
@property (nonatomic, weak) IBOutlet UILabel *goodsOwePackageLabel;
@property (nonatomic, weak) IBOutlet UILabel *balanceTitleLabel;
@property (nonatomic, weak) IBOutlet UILabel *balanceLabel;

@end

@implementation YDSelectCustomerCell

+ (instancetype)cellWithTableView:(UITableView *)tableView
{
    static NSString *reuseID = @"selectCustomerCell";
    YDSelectCustomerCell *cell = [tableView dequeueReusableCellWithIdentifier:reuseID];
    if (!cell) {
        cell = [[NSBundle mainBundle] loadNibNamed:@"YDSelectCustomerCell" owner:nil options:nil][0];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    
    return cell;
}

+ (CGFloat)getHeight
{
    return 88;
}

- (void)setData:(YDSearcherCustomerData *)data
{
    _data = data;
    
    if (data.name.length) {
        NSString *name = data.name;
        if (data.name.length > 5) {
            name = [NSString stringWithFormat:@"%@…", [name substringToIndex:5]];
        }
        self.nameLabel.text = name;
        
    } else {
        self.nameLabel.text = @"-";
    }
    
    if (data.phone.length) {
        self.phoneLabel.text = data.phone;
        
    } else {
        self.phoneLabel.text = @"-";
    }
    
    if (data.goodsOweNum > 0) {
        self.goodsOweNumLabel.textColor = ColorFromRGB(0x2EC28B);
        self.goodsOwePackageLabel.textColor = ColorFromRGB(0x2EC28B);
        
    } else {
        self.goodsOweNumLabel.textColor = ColorFromRGB(0x777777);
        self.goodsOwePackageLabel.textColor = ColorFromRGB(0x777777);
    }
    self.goodsOweNumLabel.text = [NSString stringWithFormat:@"%ld", data.goodsOweNum];
    
    if (data.balance > 0.001) {
        // 余额大于0
        self.balanceTitleLabel.text = @"客户余额:";
        self.balanceLabel.textColor = ColorFromRGB(0xFF5630);
        self.balanceLabel.text = [NSString applePrefixPriceWith:data.balance];
        
    } else if (data.balance > - 0.001) {
        // 余额为0
        self.balanceTitleLabel.text = @"客户余额:";
        self.balanceLabel.textColor = ColorFromRGB(0x777777);
        self.balanceLabel.text = [NSString applePrefixPriceWith:0];
        
    } else {
        //  有欠款
        self.balanceTitleLabel.text = @"客户欠款:";
        self.balanceLabel.textColor = ColorFromRGB(0x2EC28B);
        self.balanceLabel.text = [NSString applePrefixPriceWith:-data.balance];
    }
}

- (void)awakeFromNib
{
    [super awakeFromNib];
    
    self.phoneLabel.font = FONT_NUMBER(14);
    self.goodsOweNumLabel.font = FONT_NUMBER(17);
    self.balanceLabel.font = FONT_NUMBER(17);
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end

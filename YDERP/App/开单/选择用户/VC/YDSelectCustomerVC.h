//
//  YDSelectCustomerVC.h
//  YDERP
//
//  Created by 何英健 on 2021/8/12.
//

#import <UIKit/UIKit.h>

typedef void(^BlockSelectCustomerSucceed)(YDSearcherCustomerData *customer);

@interface YDSelectCustomerVC : UIViewController

// 是否弹窗提示（订单详情页专用）
@property (nonatomic, assign) BOOL isShowNotice;
// 选择成功回调
@property (nonatomic, copy) BlockSelectCustomerSucceed selectSucceed;

@end

//
//  YDSelectCustomerVC.m
//  YDERP
//
//  Created by 何英健 on 2021/8/12.
//

#import "YDSelectCustomerVC.h"
#import "YDSelectCustomerCell.h"

@interface YDSelectCustomerVC () <UITextFieldDelegate, UITableViewDelegate, UITableViewDataSource>

// 搜索器
@property (nonatomic, strong) YDSearcher *searcher;

@property (nonatomic, weak) IBOutlet UITextField *searchTextField;

@property (nonatomic, weak) IBOutlet UITableView *tableView;

@property (nonatomic, assign) BOOL isFirstTimeLoading;

@end

@implementation YDSelectCustomerVC

- (YDSearcher *)searcher
{
    if (!_searcher) {
        _searcher = [YDSearcher createWithType:YDSearchTypeCustomer];
        _searcher.pageSize = 10;
    }
    
    return _searcher;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.isFirstTimeLoading = YES;
    
    [self initUI];
}

- (void)initUI
{
    [self setupTableView];
}

- (void)setupTableView
{
    self.tableView.hidden = YES;
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    self.tableView.showsVerticalScrollIndicator = NO;
    self.tableView.showsHorizontalScrollIndicator = NO;
    
    if (@available(iOS 11.0, *)) {
        self.tableView.contentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentNever;
        
    } else {
        self.automaticallyAdjustsScrollViewInsets = NO;
    }

    self.tableView.mj_header = [YDRefreshHeader headerWithRefreshingTarget:self refreshingAction:@selector(pullingRefresh)];
    self.tableView.mj_footer = [YDRefreshFooter footerWithRefreshingTarget:self refreshingAction:@selector(loadMore)];
}

- (void)pullingRefresh
{
    [self refreshData:NO];
}

- (void)refreshData:(BOOL)showLoading
{
    [self dismissKeybord];
    
    if (showLoading || self.isFirstTimeLoading) {
        [YDLoadingView showToSuperview:self.view];
    }
    
    YDWeakSelf
    if (!self.searcher.completion) {
        self.searcher.completion = ^(BaseVMRefreshType type) {
            weakSelf.isFirstTimeLoading = NO;
            weakSelf.tableView.hidden = NO;
            [YDLoadingView hideToSuperview:weakSelf.view];

            [weakSelf.tableView.mj_header endRefreshing];
            if (type == BaseVMRefreshTypeNoMore) {
                [weakSelf.tableView.mj_footer endRefreshingWithNoMoreData];

            } else {
                [weakSelf.tableView.mj_footer endRefreshing];
            }
            
            [weakSelf.tableView reloadData];
        };
    }
    
    if (!self.searcher.faildBlock) {
        self.searcher.faildBlock = ^(NSInteger code, NSString *error) {
            weakSelf.isFirstTimeLoading = NO;
            weakSelf.tableView.hidden = NO;
            [YDLoadingView hideToSuperview:weakSelf.view];
            [weakSelf.tableView.mj_header endRefreshing];
            [weakSelf.tableView.mj_footer endRefreshing];
            [MBProgressHUD showToastMessage:error];
        };
    }
    
    self.searcher.keyword = self.searchTextField.text;
    [self.searcher refreshData];
}

- (void)loadMore
{
    [self.searcher loadMoreData];
}

- (void)dismissKeybord
{
    [self.searchTextField resignFirstResponder];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [self.navigationController setNavigationBarHidden:YES animated:NO];
    
    [self refreshData:NO];
}

#pragma mark - 点击事件

- (IBAction)tapBackBtn:(id)sender
{
    [self dismissKeybord];
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)tapAddBtn:(id)sender
{
    [YDWebViewVC showWebViewWithUrl:WEBURL_ADD_CUSTOMER isHideNav:YES nav:self.navigationController];
}

#pragma mark - UITableView代理

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    NSInteger count = self.searcher.customerDataArray.count;
    if (count) {
        return count;
        
    } else {
        return 1;
    }
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (self.searcher.customerDataArray.count) {
        return [YDSelectCustomerCell getHeight];
        
    } else {
        return [YDNullCell getHeightWithTableView:tableView];
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (self.searcher.customerDataArray.count) {
        YDSelectCustomerCell *cell = [YDSelectCustomerCell cellWithTableView:tableView];
        cell.data = self.searcher.customerDataArray[indexPath.section];
        
        return cell;
        
    } else {
        YDNullCell *cell = [YDNullCell cellWithTableView:tableView];
        cell.content = @"暂无客户哦！";
        return cell;
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 10;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    return [[UIView alloc] init];
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [self dismissKeybord];
    
    if (self.searcher.customerDataArray.count) {
        if (self.isShowNotice) {
            YDNormalSelectPopView *view = [YDNormalSelectPopView show];
            view.content = @"临时客户订单转固定客户，确定后不能再修改其他客户，是否确定选该客户？";
            view.leftBtnText = @"取消";
            view.rightBtnText = @"确定";
            view.tapLeftBtnBlock = nil;
            YDWeakSelf
            view.tapRightBtnBlock = ^{
                [weakSelf selectCustomerWithIndexPath:indexPath];
            };
            
        } else {
            [self selectCustomerWithIndexPath:indexPath];
        }
    }
}

- (void)selectCustomerWithIndexPath:(NSIndexPath *)indexPath
{
    if (self.selectSucceed) {
        YDSearcherCustomerData *customer = self.searcher.customerDataArray[indexPath.section];
        self.selectSucceed(customer);
    }
    
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - UITextFieldDelegate

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    
    return YES;
}

- (BOOL)textFieldShouldEndEditing:(UITextField *)textField
{
    [self refreshData:YES];
    
    return YES;
}

@end

//
//  YDSelectSKUPopCell.m
//  YDERP
//
//  Created by 何英健 on 2021/8/16.
//

#import "YDSelectSKUPopCell.h"

@interface YDSelectSKUPopCell () <UITextFieldDelegate>

@property (nonatomic, weak) IBOutlet UILabel *sizeLabel;
@property (nonatomic, weak) IBOutlet UILabel *stockCountLabel;
@property (nonatomic, weak) IBOutlet UILabel *allNumLabel;

@property (nonatomic, weak) IBOutlet UIView *selectView;
@property (nonatomic, weak) IBOutlet UITextField *textField;

@property (nonatomic, weak) IBOutlet UIButton *reduceBtn;

@end

@implementation YDSelectSKUPopCell

+ (instancetype)cellWithTableView:(UITableView *)tableView
{
    static NSString *reuseID = @"selectSKUPopCell";
    YDSelectSKUPopCell *cell = [tableView dequeueReusableCellWithIdentifier:reuseID];
    if (!cell) {
        cell = [[NSBundle mainBundle] loadNibNamed:@"YDSelectSKUPopCell" owner:nil options:nil][0];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    
    return cell;
}

+ (CGFloat)getHeight
{
    return 40;
}

- (void)setData:(YDSearcherSkuList *)data
{
    _data = data;
    
    self.sizeLabel.text = data.size;
    self.allNumLabel.text = [NSString stringWithFormat:@"%ld", (long)data.allocNum];
    
    [self refreshUI];
}

- (void)refreshUI
{
    NSInteger num = self.data.salesNum;

    if (num >= 0) {
        self.stockCountLabel.textColor = ColorFromRGB(0x222222);
        
    } else {
        self.stockCountLabel.textColor = ColorFromRGB(0xFFAB00);
    }
    self.stockCountLabel.text = [NSString stringWithFormat:@"%ld", (long)num];
    self.textField.text = [NSString stringWithFormat:@"%ld", (long)self.data.cacheSelectedNum];
    
    if (self.data.cacheSelectedNum > 0) {
        self.reduceBtn.enabled = YES;
        self.reduceBtn.alpha = 1;
        
    } else {
        self.reduceBtn.enabled = NO;
        self.reduceBtn.alpha = 0.4;
    }
}

- (IBAction)tapReduceBtn:(id)sender
{
    if (self.data.cacheSelectedNum <= self.data.allocNum) {
        [MBProgressHUD showToastMessage:@"修改数不能低于已配数"];
        return;
    }
    
    self.data.cacheSelectedNum--;
    self.data.salesNum++;
    
    [self refreshUI];
    [self SKUChange];
}

- (IBAction)tapPlusBtn:(id)sender
{
    self.data.cacheSelectedNum++;
    self.data.salesNum--;
    
    [self refreshUI];
    [self SKUChange];
}

- (void)SKUChange
{
    if (self.selectSKUBlock) {
        self.selectSKUBlock();
    }
}

- (void)awakeFromNib
{
    [super awakeFromNib];
    
    self.sizeLabel.font = FONT_NUMBER(13);
    self.stockCountLabel.font = FONT_NUMBER(13);
    self.allNumLabel.font = FONT_NUMBER(13);
    [self.textField addToolSenderWithBlock:nil];
    
    self.selectView.layer.borderWidth = 0.5;
    self.selectView.layer.borderColor = ColorFromRGB(0xDADADA).CGColor;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

#pragma mark - UITextFieldDelegate

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];

    return YES;
}

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    textField.text = nil;
    return YES;
}

- (BOOL)textFieldShouldEndEditing:(UITextField *)textField
{
    if (!textField.text.length) {
        textField.text = [NSString stringWithFormat:@"%ld", (long)self.data.cacheSelectedNum];
        
    } else {
        NSInteger num = [textField.text integerValue];
        
        if (num < self.data.allocNum) {
            [MBProgressHUD showToastMessage:@"修改数不能低于已配数"];
            
            num = self.data.allocNum;
            textField.text = [NSString stringWithFormat:@"%ld", (long)num];
            
        } else if (num > 9999) {
            [MBProgressHUD showToastMessage:@"最多选择9999件"];
            
            num = 9999;
            textField.text = [NSString stringWithFormat:@"%ld", (long)num];
        }
        
        self.data.cacheSelectedNum = num;
        self.data.salesNum += self.data.selectedNum - self.data.cacheSelectedNum;
        
        [self refreshUI];
        [self SKUChange];
    }
    
    return YES;
}

@end

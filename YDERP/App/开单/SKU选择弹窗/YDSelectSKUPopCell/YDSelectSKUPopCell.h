//
//  YDSelectSKUPopCell.h
//  YDERP
//
//  Created by 何英健 on 2021/8/16.
//

#import <UIKit/UIKit.h>

typedef void(^BlockSelectSKUChange)(void);

@interface YDSelectSKUPopCell : UITableViewCell

+ (instancetype)cellWithTableView:(UITableView *)tableView;

+ (CGFloat)getHeight;

@property (nonatomic, strong) YDSearcherSkuList *data;

@property (nonatomic, copy) BlockSelectSKUChange selectSKUBlock;

@end

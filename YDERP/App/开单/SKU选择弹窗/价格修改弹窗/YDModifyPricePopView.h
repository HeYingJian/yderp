//
//  YDModifyPricePopView.h
//  YDERP
//
//  Created by 何英健 on 2021/8/16.
//

#import <UIKit/UIKit.h>

typedef void(^BlockModifyPriceComplete)(double price);
typedef void(^BlockModifyPriceHide)(void);

@interface YDModifyPricePopView : UIView

+ (void)showWithCompletion:(BlockModifyPriceComplete)completion hideBlock:(BlockModifyPriceHide)hideBlock;

@end

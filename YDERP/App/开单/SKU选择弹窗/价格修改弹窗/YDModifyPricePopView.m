//
//  YDModifyPricePopView.m
//  YDERP
//
//  Created by 何英健 on 2021/8/16.
//

#import "YDModifyPricePopView.h"

@interface YDModifyPricePopView () <UITextFieldDelegate>

@property (nonatomic, copy) BlockModifyPriceComplete completion;
@property (nonatomic, copy) BlockModifyPriceHide hideBlock;

@property (nonatomic, weak) IBOutlet UITextField *textField;
@property (nonatomic, weak) IBOutlet UIButton *cancelBtn;

@end

@implementation YDModifyPricePopView

+ (void)showWithCompletion:(BlockModifyPriceComplete)completion hideBlock:(BlockModifyPriceHide)hideBlock
{
    YDModifyPricePopView *view = [[NSBundle mainBundle] loadNibNamed:@"YDModifyPricePopView" owner:nil options:nil][0];
    view.frame = CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT);
    view.completion = completion;
    view.hideBlock = hideBlock;
    
    [view initUI];
    
    [KeyWindow addSubview:view];
}

- (void)initUI
{
    [self.textField addToolSenderWithBlock:nil];
    self.textField.font = FONT_NUMBER(23);
    [self.textField becomeFirstResponder];
    
    self.cancelBtn.layer.borderColor = ColorFromRGB(0xDBDDE0).CGColor;
    self.cancelBtn.layer.borderWidth = 1;
}

- (IBAction)tapCancelBtn:(id)sender
{
    [self hide];
}

- (IBAction)tapConfirmBtn:(id)sender
{
    if (!self.textField.text.length) {
        [MBProgressHUD showToastMessage:@"开单价格不能为空"];
        return;
    }
    
    if ([self.textField.text doubleValue] - 1000000 >= 0.01) {
        [MBProgressHUD showToastMessage:@"开单价格不能大于100万"];
        return;
    }
    
    if (self.completion) {
        if ([self.textField.text isEqualToString:@"999999.99"]) {
            self.completion(999999.99);
            
        } else {
            NSNumberFormatter *formatter = [[NSNumberFormatter alloc] init];
            [formatter setNumberStyle:NSNumberFormatterDecimalStyle];
            double price = [[formatter numberFromString:self.textField.text] doubleValue];
            self.completion(price);
        }
    }
    
    [self hide];
}

- (void)hide
{
    [self.textField resignFirstResponder];
    [self removeFromSuperview];
    
    if (self.hideBlock) {
        self.hideBlock();
    }
}

#pragma mark - UITextFieldDelegate

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];

    return YES;
}

@end

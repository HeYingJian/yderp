//
//  YDSelectSKUPopView.h
//  YDERP
//
//  Created by 何英健 on 2021/8/16.
//

#import <UIKit/UIKit.h>
#import "YDSelectSKUPopCell.h"

typedef enum : NSUInteger {
    YDSelectSKUTypeMakeList, // 开单或挂单
    YDSelectSKUTypeModifyOrder, // 修改订单
    YDSelectSKUTypeSelectProduct, // 商品选择页面
} YDSelectSKUType;

typedef void(^BlockSelectSKUComplete)(BOOL isSelected);

@interface YDSelectSKUPopView : UIView

/**
 创建方法
 @param type                  入口区分
 @param data                  商品信息
 @param orderID               需要补全sku信息时才填
 @param version               当前订单版本号(用于修改sku时，确保这个订单没有被别人改动过)
 */
+ (void)showWithType:(YDSelectSKUType)type data:(YDSearcherProductData *)data orderID:(NSString *)orderID version:(NSString *)version completion:(BlockSelectSKUComplete)completion;

@end

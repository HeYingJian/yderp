//
//  YDMorePricePopView.m
//  YDERP
//
//  Created by 何英健 on 2021/10/18.
//

#import "YDMorePricePopView.h"

@interface YDMorePricePopView ()

@property (nonatomic, strong) YDSearcherProductData *data;
@property (nonatomic, copy) BlockMorePriceSelect completion;

@property (nonatomic, weak) IBOutlet UIView *view1;
@property (nonatomic, weak) IBOutlet UIView *view2;
@property (nonatomic, weak) IBOutlet UIView *view3;

@property (nonatomic, weak) IBOutlet UILabel *label1;
@property (nonatomic, weak) IBOutlet UILabel *label2;
@property (nonatomic, weak) IBOutlet UILabel *label3;

@end

@implementation YDMorePricePopView

+ (YDMorePricePopView *)showWithData:(YDSearcherProductData *)data completion:(BlockMorePriceSelect)completion
{
    YDMorePricePopView *view = [[NSBundle mainBundle] loadNibNamed:@"YDMorePricePopView" owner:nil options:nil][0];
    view.frame = CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT);
    view.data = data;
    view.completion = completion;
    
    [view initUI];
    [KeyWindow addSubview:view];
    
    return view;
}

- (void)hide
{
    [self removeFromSuperview];
}

- (void)initUI
{
    UITapGestureRecognizer *tap1 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapView1)];
    [self.view1 addGestureRecognizer:tap1];
    
    UITapGestureRecognizer *tap2 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapView2)];
    [self.view2 addGestureRecognizer:tap2];
    
    UITapGestureRecognizer *tap3 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapView3)];
    [self.view3 addGestureRecognizer:tap3];
    
    self.label1.font = FONT_NUMBER(16);
    if (self.data.itemSalesPrice.length) {
        self.label1.textColor = ColorFromRGB(0xFF5630);
        self.label1.text = [NSString stringWithFormat:@"¥%@", self.data.itemSalesPrice];
    }
    
    self.label2.font = FONT_NUMBER(16);
    if (self.data.wholePrice.length) {
        self.label2.textColor = ColorFromRGB(0xFF5630);
        self.label2.text = [NSString stringWithFormat:@"¥%@", self.data.wholePrice];
    }
    
    self.label3.font = FONT_NUMBER(16);
    if (self.data.packagePrice.length) {
        self.label3.textColor = ColorFromRGB(0xFF5630);
        self.label3.text = [NSString stringWithFormat:@"¥%@", self.data.packagePrice];
    }
}

- (IBAction)tapCloseBtn:(id)sender
{
    [self hide];
}

- (void)tapView1
{
    if (self.data.itemSalesPrice.length) {
        if (self.completion) {
            self.completion(self.data.itemSalesPrice);
        }
        
        [self hide];
        
    } else {
        [MBProgressHUD showToastMessage:@"请先设置价格"];
    }
}

- (void)tapView2
{
    if (self.data.wholePrice.length) {
        if (self.completion) {
            self.completion(self.data.wholePrice);
        }
        
        [self hide];
        
    } else {
        [MBProgressHUD showToastMessage:@"请先设置价格"];
    }
}

- (void)tapView3
{
    if (self.data.packagePrice.length) {
        if (self.completion) {
            self.completion(self.data.packagePrice);
        }
        
        [self hide];
        
    } else {
        [MBProgressHUD showToastMessage:@"请先设置价格"];
    }
}

@end

//
//  YDMorePricePopView.h
//  YDERP
//
//  Created by 何英健 on 2021/10/18.
//

#import <UIKit/UIKit.h>

typedef void(^BlockMorePriceSelect)(NSString *price);

@interface YDMorePricePopView : UIView

+ (YDMorePricePopView *)showWithData:(YDSearcherProductData *)data completion:(BlockMorePriceSelect)completion;

@end

//
//  YDSelectSKUPopView.m
//  YDERP
//
//  Created by 何英健 on 2021/8/16.
//

#import "YDSelectSKUPopView.h"
#import "YDModifyPricePopView.h"
#import "YDMorePricePopView.h"

@interface YDSelectSKUPopView () <UITableViewDelegate, UITableViewDataSource>

// 入口区分
@property (nonatomic, assign) YDSelectSKUType type;
@property (nonatomic, strong) YDSearcherProductData *data;
@property (nonatomic, copy) NSString *orderID;
@property (nonatomic, copy) NSString *version;
@property (nonatomic, copy) BlockSelectSKUComplete completion;

// 当前选中tag
@property (nonatomic, assign) NSInteger currentIndex;
@property (nonatomic, strong) NSMutableArray<UIButton *> *btnArray;
@property (nonatomic, strong) NSMutableArray<UIButton *> *redPointArray;

@property (nonatomic, weak) IBOutlet UITableView *tableView;

@property (nonatomic, weak) IBOutlet UIView *BGView;
@property (nonatomic, weak) IBOutlet UIView *mainView;
@property (nonatomic, weak) IBOutlet UILabel *totalPriceLabel;
@property (nonatomic, weak) IBOutlet UILabel *totalQuantityLabel;

@property (nonatomic, weak) IBOutlet UIImageView *productImgView;
@property (nonatomic, weak) IBOutlet UILabel *productNameLabel;
@property (nonatomic, weak) IBOutlet UIView *modifyPriceView;
@property (nonatomic, weak) IBOutlet UILabel *priceLabel;

@property (nonatomic, weak) IBOutlet UIView *scrollBGView;
@property (nonatomic, strong) UIScrollView *scrollView;
@property (nonatomic, weak) IBOutlet UILabel *quantityTitleLabel;

@property (nonatomic, weak) IBOutlet UIButton *confirmBtn;

@property (nonatomic, weak) IBOutlet NSLayoutConstraint *mainViewTop;
@property (nonatomic, weak) IBOutlet NSLayoutConstraint *mainViewH;
@property (nonatomic, weak) IBOutlet NSLayoutConstraint *countViewH;

// 是否跟随键盘出现而进行动画效果
@property (nonatomic, assign) BOOL isMoveWithKeybord;

@end

@implementation YDSelectSKUPopView

#pragma mark - 懒加载

- (NSMutableArray<UIButton *> *)btnArray
{
    if (!_btnArray) {
        _btnArray = [NSMutableArray array];
    }
    
    return _btnArray;
}

- (NSMutableArray<UIButton *> *)redPointArray
{
    if (!_redPointArray) {
        _redPointArray = [NSMutableArray array];
    }
    
    return _redPointArray;
}

#pragma mark - 对外方法

+ (void)showWithType:(YDSelectSKUType)type data:(YDSearcherProductData *)data orderID:(NSString *)orderID version:(NSString *)version completion:(BlockSelectSKUComplete)completion
{
    if (orderID.length && !data.isLoadSkuGroup) {
        // 补全sku信息(某些地方传过来的data只包含用户已选的sku)
        [YDLoadingView showToSuperview:nil];
        
        // 请求接口，补全对应的sku信息
        [YDFunction getProductSkuGroupWithOrderID:orderID productID:[NSString stringWithFormat:@"%ld", (long)data.ID] completion:^(NSMutableArray<YDSearcherSkuGroupData *> *dataArray) {
            [YDLoadingView hideToSuperview:nil];
            
            for (YDSearcherSkuGroupData *sub in dataArray) {
                BOOL isColorExist = NO;
                for (YDSearcherSkuGroupData *originGroup in data.skuGroupList) {
                    if ([sub.colorId isEqualToString:originGroup.colorId]) {
                        isColorExist = YES;
                        
                        for (YDSearcherSkuList *sku in sub.skuList) {
                            BOOL isSizeExist = NO;
                            for (YDSearcherSkuList *originSku in originGroup.skuList) {
                                if ([sku.ID isEqualToString:originSku.ID]) {
                                    isSizeExist = YES;
                                    
                                    // 赋值可售数量
                                    if (type == YDSelectSKUTypeMakeList) {
                                        // 订单没提交确认(即开单或挂单)，接口返回的salesNum实际上是没有计算选择商品的，所以需要自己算进去
                                        originSku.salesNum = sku.salesNum - originSku.selectedNum;
                                        
                                    } else {
                                        originSku.salesNum = sku.salesNum;
                                    }
                                    
                                    // 赋值库存数量
                                    originSku.stockNum = sku.stockNum;
                                    // 赋值已配数量
                                    originSku.allocNum = sku.allocNum;
                                    
                                    break;
                                }
                            }
                            
                            if (!isSizeExist) {
                                [originGroup.skuList addObject:sku];
                            }
                        }
                    }
                }
                
                if (!isColorExist) {
                    [data.skuGroupList addObject:sub];
                }
            }
            
            // 标记数据已经补全过了
            data.isLoadSkuGroup = YES;
            
            [YDSelectSKUPopView createWithType:type data:data orderID:orderID version:version completion:completion];
            
        } faildBlock:^(NSString *msg) {
            [YDLoadingView hideToSuperview:nil];
            [MBProgressHUD showToastMessage:msg];
        }];
        
    } else {
        [YDSelectSKUPopView createWithType:type data:data orderID:orderID version:version completion:completion];
    }
}

+ (void)createWithType:(YDSelectSKUType)type data:(YDSearcherProductData *)data orderID:(NSString *)orderID version:(NSString *)version completion:(BlockSelectSKUComplete)completion
{
    YDSelectSKUPopView *view = [[NSBundle mainBundle] loadNibNamed:@"YDSelectSKUPopView" owner:nil options:nil][0];
    view.frame = CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT);
    view.type = type;
    view.data = data;
    view.orderID = orderID;
    view.version = version;
    view.completion = completion;
    
    [view initUI];
    [view addNotification];
    
    [KeyWindow addSubview:view];
    
    dispatch_after(0, dispatch_get_main_queue(), ^{
        [UIView animateWithDuration:0.3 animations:^{
            view.mainViewTop.constant = SCREEN_HEIGHT - [YDSelectSKUPopView getHeightWithData:data];
            [view layoutIfNeeded];
        }];
    });
}

- (void)initUI
{
    self.isMoveWithKeybord = YES;
    self.mainViewTop.constant = SCREEN_HEIGHT;
    self.mainViewH.constant = [YDSelectSKUPopView getHeightWithData:self.data];
    if (IS_PhoneXAll) {
        self.countViewH.constant = 95;
        
    } else {
        self.countViewH.constant = 65;
    }
    [self layoutIfNeeded];
    
    // 添加上半部分圆角
    [self.mainView addRoundedCorners:UIRectCornerTopLeft|UIRectCornerTopRight withRadii:CGSizeMake(20, 20) viewRect:CGRectMake(0, 0, SCREEN_WIDTH, [YDSelectSKUPopView getHeightWithData:self.data])];
    
    UITapGestureRecognizer *tapBG = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapBGView)];
    [self.BGView addGestureRecognizer:tapBG];
    
    UITapGestureRecognizer *tapPrice = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(showModifyPrice)];
    [self.modifyPriceView addGestureRecognizer:tapPrice];
    
    self.confirmBtn.layer.borderColor = ColorFromRGB(0xE2E2E2).CGColor;
    
    self.totalPriceLabel.font = FONT_NUMBER(20);
    self.modifyPriceView.layer.borderColor = ColorFromRGB(0xDADADA).CGColor;
    self.modifyPriceView.layer.borderWidth = 0.5;
    self.priceLabel.font = FONT_NUMBER(14);
    
    [self setupScrollView];
    
    [self setupTableView];
    
    [self refreshCountView];
    
    [self setBtnSelectedWithIndex:0];
}

- (void)tapBGView
{
    // 还原可售数量
    for (YDSearcherSkuGroupData *sub in self.data.skuGroupList) {
        for (YDSearcherSkuList *sku in sub.skuList) {
            sku.salesNum += sku.cacheSelectedNum - sku.selectedNum;
        }
    }
    
    [self hide];
}

- (void)hide
{
    [UIView animateWithDuration:0.3 animations:^{
        self.mainViewTop.constant = SCREEN_HEIGHT;
        [self layoutIfNeeded];
        
    } completion:^(BOOL finished) {
        [self removeFromSuperview];
    }];
}

- (void)setData:(YDSearcherProductData *)data
{
    _data = data;
    
    // 初始化时，重置修改数量
    for (YDSearcherSkuGroupData *sub in self.data.skuGroupList) {
        for (YDSearcherSkuList *skuData in sub.skuList) {
            // 因为用户需要点击确定以后才会真实修改，所以需要设置一个暂时修改数据
            skuData.cacheSelectedNum = skuData.selectedNum;
        }
    }
    
    // 初始化销售价
    data.cacheSalesPrice = data.salesPrice;
    
    if (data.picList.count && data.picList[0].url.length) {
        [self.productImgView appleSetImageWithUrl:data.picList[0].url SprcifyOssImageStyle:@"products_list" HasHD:NO placeholderImage:@"默认商品方形图" completion:nil];
        
    } else {
        [self.productImgView setImage:[UIImage imageNamed:@"没有商品默认图"]];
    }
    
    NSString *codeStr = data.code;
    NSString *productName = data.name;
    NSString *str = [NSString stringWithFormat:@"%@#%@", codeStr, productName];
    NSMutableAttributedString *attributedStr = [[NSMutableAttributedString alloc] initWithString:str];
    [attributedStr addAttribute:NSFontAttributeName value:FONT_NUMBER(15) range:NSMakeRange(0, codeStr.length + 1)];
    [attributedStr addAttribute:NSForegroundColorAttributeName value:ColorFromRGB(0xFF5630) range:NSMakeRange(0, codeStr.length + 1)];
    [attributedStr addAttribute:NSFontAttributeName value:[UIFont systemFontOfSize:14.0f] range:NSMakeRange(codeStr.length + 1, productName.length)];
    [attributedStr addAttribute:NSForegroundColorAttributeName value:ColorFromRGB(0x212121) range:NSMakeRange(codeStr.length + 1, productName.length)];
    NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc] init];
    [paragraphStyle setLineSpacing:5];
    [attributedStr addAttribute:NSParagraphStyleAttributeName value:paragraphStyle range:NSMakeRange(0, [str length])];
    self.productNameLabel.attributedText = attributedStr;
    self.productNameLabel.lineBreakMode = NSLineBreakByTruncatingTail;
    
    NSString *priceStr = [NSString appleStandardPriceWith:data.cacheSalesPrice];
    self.priceLabel.text = [NSString stringWithFormat:@"¥%@", priceStr];
}

- (void)setupScrollView
{
    self.scrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, self.scrollBGView.height)];
    self.scrollView.showsHorizontalScrollIndicator = NO;
    [self.scrollBGView addSubview:self.scrollView];
    
    CGFloat originX = 22;
    for (int i = 0; i < self.data.skuGroupList.count; i++) {
        YDSearcherSkuGroupData *sub = self.data.skuGroupList[i];
        UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
        btn.tag = i;
        CGFloat width = [sub.color sizeWith:CGSizeMake(MAXFLOAT, 20) font:[UIFont systemFontOfSize:12 weight:UIFontWeightMedium]].width;
        btn.frame = CGRectMake(originX, 15, width + 23 * 2, 28);
        btn.layer.cornerRadius = btn.height / 2;
        [btn setTitle:sub.color forState:UIControlStateNormal];
        btn.layer.borderColor = ColorFromRGB(0xFF5630).CGColor;
        [btn addTarget:self action:@selector(tapColorBtn:) forControlEvents:UIControlEventTouchUpInside];
        [self setBtnStatus:btn isSelected:NO];
        [self.scrollView addSubview:btn];
        [self.btnArray addObject:btn];
        
        UIButton *redPointBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        [redPointBtn setY:btn.y - 5];
        [redPointBtn setHeight:12];
        [redPointBtn setBackgroundImage:[UIImage imageNamed:@"小红点标志"] forState:UIControlStateNormal];
        redPointBtn.titleLabel.font = FONT_NUMBER(10);
        [self.scrollView addSubview:redPointBtn];
        [self.redPointArray addObject:redPointBtn];
        
        if (i < self.data.skuGroupList.count - 1) {
            originX = CGRectGetMaxX(btn.frame) + 15;
            
        } else {
            [self.scrollView setContentSize:CGSizeMake(CGRectGetMaxX(btn.frame) + 22, self.scrollView.height)];
        }
    }
}

- (void)setupTableView
{
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    self.tableView.showsVerticalScrollIndicator = NO;
    self.tableView.showsHorizontalScrollIndicator = NO;
}

- (void)tapColorBtn:(UIButton *)btn
{
    [self setBtnSelectedWithIndex:btn.tag];
}

// 选中指定index的按钮
- (void)setBtnSelectedWithIndex:(NSInteger)index
{
    if (index >= self.btnArray.count) {
        return;
    }
    
    self.currentIndex = index;
    
    for (int i = 0; i < self.btnArray.count; i++) {
        UIButton *btn = self.btnArray[i];
        
        if (index == i) {
            [self setBtnStatus:btn isSelected:YES];
            
        } else {
            [self setBtnStatus:btn isSelected:NO];
        }
    }
    
    [self refreshCountView];
    [self.tableView reloadData];
}

- (void)setBtnStatus:(UIButton *)btn isSelected:(BOOL)isSelected
{
    if (isSelected) {
        [btn setBackgroundColor:ColorFromRGB(0xFFF6F2)];
        [btn setTitleColor:ColorFromRGB(0xFF5630) forState:UIControlStateNormal];
        btn.titleLabel.font = [UIFont systemFontOfSize:12 weight:UIFontWeightMedium];
        btn.layer.borderWidth = 1;
        
    } else {
        [btn setBackgroundColor:ColorFromRGB(0xF4F4F4)];
        [btn setTitleColor:ColorFromRGB(0x222222) forState:UIControlStateNormal];
        btn.titleLabel.font = [UIFont systemFontOfSize:12 weight:UIFontWeightRegular];
        btn.layer.borderWidth = 0;
    }
}

// 刷新统计
- (void)refreshCountView;
{
    CGFloat totalPrice = 0;
    NSInteger totalQuantity = 0;
    NSInteger currentQuantity = 0;
    
    for (int i = 0; i < self.data.skuGroupList.count; i++) {
        NSInteger redPointNum = 0;
        YDSearcherSkuGroupData *sub = self.data.skuGroupList[i];
        
        for (YDSearcherSkuList *sku in sub.skuList) {
            totalPrice += self.data.cacheSalesPrice * sku.cacheSelectedNum;
            totalQuantity += sku.cacheSelectedNum;
            redPointNum += sku.cacheSelectedNum;
            
            if (self.currentIndex == i) {
                currentQuantity += sku.cacheSelectedNum;
            }
        }
        
        UIButton *redPointBtn = self.redPointArray[i];
        UIButton *colorBtn = self.btnArray[i];
        if (redPointNum > 0) {
            redPointBtn.hidden = NO;
            NSString *numStr = [NSString stringWithFormat:@"%ld", (long)redPointNum];
            [redPointBtn setTitle:numStr forState:UIControlStateNormal];
            CGFloat redPointW = [numStr sizeWith:CGSizeMake(MAXFLOAT, 12) font:FONT_NUMBER(10)].width + 8;
            if (redPointW < 19) {
                redPointW = 19;
            }
            [redPointBtn setWidth:redPointW];
            [redPointBtn setX:CGRectGetMaxX(colorBtn.frame) - redPointBtn.width];
            
        } else {
            redPointBtn.hidden = YES;
        }
    }
    
    self.totalPriceLabel.text = [NSString applePrefixPriceWith:totalPrice];
    self.totalQuantityLabel.text = [NSString stringWithFormat:@"已选中%ld件", (long)totalQuantity];
    self.quantityTitleLabel.text = [NSString stringWithFormat:@"总量(%ld)", (long)currentQuantity];
}

// 计算弹窗高度
+ (CGFloat)getHeightWithData:(YDSearcherProductData *)data
{
    // 获取尺码最多有多少种
    NSInteger maxLine = 0;
    for (YDSearcherSkuGroupData *sub in data.skuGroupList) {
        if (sub.skuList.count > maxLine) {
            maxLine = sub.skuList.count;
        }
    }
    
    CGFloat countViewH = 0;
    if (IS_PhoneXAll) {
        countViewH = 95;
        
    } else {
        countViewH = 65;
    }
    
    CGFloat height = 179 + 40 + 10 + countViewH + maxLine * 40;

    CGFloat maxHeight = SCREEN_HEIGHT - 100;
    if (height > maxHeight) {
        height = maxHeight;
    }
    return height;
}

// 更多价格
- (IBAction)tapMorePriceBtn:(id)sender
{
    YDWeakSelf
    [YDMorePricePopView showWithData:self.data completion:^(NSString *price) {
        weakSelf.data.cacheSalesPrice = [price doubleValue];
        weakSelf.priceLabel.text = [NSString stringWithFormat:@"¥%@", price];
        
        [weakSelf refreshCountView];
    }];
}

// 点击确定
- (IBAction)tapConfirmBtn:(id)sender
{
    [self.window endEditing:YES];
    
    if (self.version.length) {
        // 检查订单版本号没问题以后再修改
        [self checkOrderVersion];
        
    } else {
        // 直接保存修改
        [self confirmModify];
    }
}

// 检查订单版本号
- (void)checkOrderVersion
{
    [YDLoadingView showToSuperview:nil];
    
    YDWeakSelf
    [YDFunction checkOrderVersionWithOrderID:self.orderID version:self.version completion:^{
        [YDLoadingView hideToSuperview:nil];
        
        // 保存修改
        [weakSelf confirmModify];
        
    } refreshBlock:^(NSString *msg) {
        [YDLoadingView hideToSuperview:nil];
        
        // 订单号有更改，需要重新刷新界面
        NSMutableDictionary *dict = [NSMutableDictionary dictionary];
        [dict setObject:msg forKey:@"notice"];
        PostNotification(REFRESH_MODIFY_ORDER, dict);
        [weakSelf hide];
        
    } faildBlock:^(NSString *msg) {
        [YDLoadingView hideToSuperview:nil];
        [MBProgressHUD showToastMessage:msg];
    }];
}

// 保存修改
- (void)confirmModify
{
    BOOL isSelected = NO;
    for (YDSearcherSkuGroupData *sub in self.data.skuGroupList) {
        for (YDSearcherSkuList *sku in sub.skuList) {
            // 正式确认修改数量
            sku.selectedNum = sku.cacheSelectedNum;
            
            if (sku.selectedNum > 0) {
                isSelected = YES;
            }
        }
    }
    
    // 正式确认修改价格
    self.data.salesPrice = self.data.cacheSalesPrice;
    
    if (self.completion) {
        self.completion(isSelected);
    }
    
    [self hide];
}

// 弹窗修改价格
- (void)showModifyPrice
{
    [UIView animateWithDuration:0.3 animations:^{
        self.mainViewTop.constant = SCREEN_HEIGHT - [YDSelectSKUPopView getHeightWithData:self.data];
        [self layoutIfNeeded];
    }];
    
    self.isMoveWithKeybord = NO;
    
    YDWeakSelf
    [YDModifyPricePopView showWithCompletion:^(double price) {
        weakSelf.data.cacheSalesPrice = price;
        
        NSString *priceStr = [NSString appleStandardPriceWith:price];
        weakSelf.priceLabel.text = [NSString stringWithFormat:@"¥%@", priceStr];
        
        [weakSelf refreshCountView];
        
    } hideBlock:^{
        weakSelf.isMoveWithKeybord = YES;
    }];
}

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

#pragma mark - UITableView代理

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.data.skuGroupList[self.currentIndex].skuList.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return [YDSelectSKUPopCell getHeight];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    YDSelectSKUPopCell *cell = [YDSelectSKUPopCell cellWithTableView:tableView];
    YDSearcherSkuList *data = self.data.skuGroupList[self.currentIndex].skuList[indexPath.row];
    cell.data = data;
    YDWeakSelf
    cell.selectSKUBlock = ^{
        [weakSelf refreshCountView];
    };
    
    return cell;
}

#pragma mark - 键盘高度变化

- (void)addNotification
{
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardShow:) name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardMiss:) name:UIKeyboardWillHideNotification object:nil];
}

// 弹出键盘改变控制器view
- (void)keyboardShow:(NSNotification *)noti
{
    if (self.isMoveWithKeybord) {
        CGFloat keyboardHeight = [noti.userInfo[UIKeyboardFrameEndUserInfoKey] CGRectValue].size.height;
        [UIView animateWithDuration:0.0f animations:^{
            self.mainViewTop.constant = SCREEN_HEIGHT - [YDSelectSKUPopView getHeightWithData:self.data] - keyboardHeight;
            [self layoutIfNeeded];
        }];
    }
}

//回收键盘改变控制器view
- (void)keyboardMiss:(NSNotification *)noti
{
    if (self.isMoveWithKeybord) {
        [UIView animateWithDuration:0.0 animations:^{
            self.mainViewTop.constant = SCREEN_HEIGHT - [YDSelectSKUPopView getHeightWithData:self.data];
            [self layoutIfNeeded];
        }];
    }
}

@end

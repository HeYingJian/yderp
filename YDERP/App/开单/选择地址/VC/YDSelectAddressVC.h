//
//  YDSelectAddressVC.h
//  YDERP
//
//  Created by 何英健 on 2021/8/13.
//

#import <UIKit/UIKit.h>

typedef void(^BlockSelectAddressSucceed)(YDCustomerAddressData *address);

@interface YDSelectAddressVC : UIViewController

// 客户ID
@property (nonatomic, assign) NSInteger customerID;

@property (nonatomic, copy) BlockSelectAddressSucceed selectSucceed;

@end

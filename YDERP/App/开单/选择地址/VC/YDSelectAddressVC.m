//
//  YDSelectAddressVC.m
//  YDERP
//
//  Created by 何英健 on 2021/8/13.
//

#import "YDSelectAddressVC.h"
#import "YDSelectAddressCell.h"
#import "YDModifyMessagePopView.h"

@interface YDSelectAddressVC () <UITableViewDelegate, UITableViewDataSource>

@property (nonatomic, weak) IBOutlet UITableView *tableView;

@property (nonatomic, strong) NSMutableArray<YDCustomerAddressData *> *addressArray;

@end

@implementation YDSelectAddressVC

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self initUI];
    
    [self refreshData];
}

- (void)initUI
{
    self.title = @"选择收货地址";
    [self.navigationItem addDefaultBackButton:self actionMethod:@selector(goBack)];
    
    [self setupTableView];
}

- (void)setupTableView
{
    self.tableView.hidden = YES;
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    self.tableView.showsVerticalScrollIndicator = NO;
    self.tableView.showsHorizontalScrollIndicator = NO;
    
    if (@available(iOS 11.0, *)) {
        self.tableView.contentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentNever;
        
    } else {
        self.automaticallyAdjustsScrollViewInsets = NO;
    }
}

- (void)refreshData
{
    YDWeakSelf
    [YDFunction getCustomerAddressListWithCustomerID:self.customerID completion:^(NSMutableArray<YDCustomerAddressData *> *addressArray) {
        weakSelf.addressArray = addressArray;
        weakSelf.tableView.hidden = NO;
        [weakSelf.tableView reloadData];
    }];
}

- (IBAction)tapAddBtn:(id)sender
{
    [self showEditAddressWithData:nil];
}

// 新增与编辑地址
- (void)showEditAddressWithData:(YDCustomerAddressData *)data
{
    NSString *title;
    NSString *content;
    NSString *addressID;
    if (data) {
        title = @"编辑地址";
        content = data.address;
        addressID = [NSString stringWithFormat:@"%ld", data.ID];
        
    } else {
        title = @"新建地址";
    }
    
    YDWeakSelf
    [YDModifyMessagePopView showWithTitle:title content:content placeholder:@"最多50字" completion:^(NSString *string, BlockModifyMessageStatus statusBlock) {
        if (!string.length) {
            [MBProgressHUD showToastMessage:@"内容不能为空"];
            statusBlock(NO);
            return;
        }
        
        if (string.length > 50) {
            [MBProgressHUD showToastMessage:@"最多可填写50字"];
            statusBlock(NO);
            return;
        }
        
        [YDLoadingView showToSuperview:weakSelf.view];
        
        [YDFunction modifyCustomerAddressWithCustomerID:self.customerID addressID:addressID address:string isDefaultAddress:NO completion:^{
            [YDLoadingView hideToSuperview:weakSelf.view];
            
            [weakSelf refreshData];
            
            NSString *succeedMsg;
            if (data) {
                succeedMsg = @"编辑成功";
                
            } else {
                succeedMsg = @"新增成功";
            }
            [MBProgressHUD showToastMessage:succeedMsg];
            
            statusBlock(YES);
            
        } faildBlock:^(NSString *msg) {
            [YDLoadingView hideToSuperview:weakSelf.view];
            [MBProgressHUD showToastMessage:msg];
        }];
        
    } tapCloseBlock:nil];
}

- (void)goBack
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [self.navigationController setNavigationBarHidden:NO animated:YES];
}

#pragma mark - UITableView代理

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    NSInteger count = self.addressArray.count;
    if (count) {
        return count;
        
    } else {
        return 1;
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (self.addressArray.count) {
        YDCustomerAddressData *data = self.addressArray[indexPath.row];
        return [YDSelectAddressCell getHeightWithData:data];
        
    } else {
        return [YDNullCell getHeightWithTableView:tableView];
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (self.addressArray.count) {
        YDSelectAddressCell *cell = [YDSelectAddressCell cellWithTableView:tableView];
        cell.data = self.addressArray[indexPath.row];
        YDWeakSelf
        cell.editBlock = ^(YDCustomerAddressData *data) {
            [weakSelf showEditAddressWithData:data];
        };
        
        return cell;
        
    } else {
        YDNullCell *cell = [YDNullCell cellWithTableView:tableView];
        return cell;
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (self.addressArray.count) {
        if (self.selectSucceed) {
            YDCustomerAddressData *data = self.addressArray[indexPath.row];
            self.selectSucceed(data);
        }

        [self.navigationController popViewControllerAnimated:YES];
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 0.01;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    return [[UIView alloc] init];
}

@end

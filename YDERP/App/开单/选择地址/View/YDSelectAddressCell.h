//
//  YDSelectAddressCell.h
//  YDERP
//
//  Created by 何英健 on 2021/8/13.
//

#import <UIKit/UIKit.h>

typedef void(^BlockSelectAddressEdit)(YDCustomerAddressData *data);

@interface YDSelectAddressCell : UITableViewCell

+ (instancetype)cellWithTableView:(UITableView *)tableView;

+ (CGFloat)getHeightWithData:(YDCustomerAddressData *)data;

@property (nonatomic, strong) YDCustomerAddressData *data;

@property (nonatomic, copy) BlockSelectAddressEdit editBlock;

@end

//
//  YDSelectAddressCell.m
//  YDERP
//
//  Created by 何英健 on 2021/8/13.
//

#import "YDSelectAddressCell.h"

@interface YDSelectAddressCell ()

@property (nonatomic, weak) IBOutlet YDVerticalAlignmentLabel *addressLabel;

@end

@implementation YDSelectAddressCell

+ (instancetype)cellWithTableView:(UITableView *)tableView
{
    static NSString *reuseID = @"selectAddressCell";
    YDSelectAddressCell *cell = [tableView dequeueReusableCellWithIdentifier:reuseID];
    if (!cell) {
        cell = [[NSBundle mainBundle] loadNibNamed:@"YDSelectAddressCell" owner:nil options:nil][0];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    
    return cell;
}

+ (CGFloat)getHeightWithData:(YDCustomerAddressData *)data
{
    return 38 + [YDSelectAddressCell getAttStringHeightWithData:data];
}

- (void)setData:(YDCustomerAddressData *)data
{
    _data = data;

    self.addressLabel.attributedText = [YDSelectAddressCell getAttStringWithData:data];
}

+ (NSMutableAttributedString *)getAttStringWithData:(YDCustomerAddressData *)data
{
    NSMutableAttributedString *string = [[NSMutableAttributedString alloc] initWithString:data.address];
    
    if (data.defaultFlag) {
        NSAttributedString *spaceString = [[NSAttributedString alloc] initWithString:@"  "];
        [string insertAttributedString:spaceString atIndex:0];
        
        // 创建图片图片附件
        NSTextAttachment *attach = [[NSTextAttachment alloc] init];
        attach.image = [UIImage imageNamed:@"默认地址"];
        attach.bounds = CGRectMake(0, -3, 33, 17);
        NSAttributedString *attachString = [NSAttributedString attributedStringWithAttachment:attach];
       
        //将图片插入到合适的位置
        [string insertAttributedString:attachString atIndex:0];
    }
    
    NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc] init];
    [paragraphStyle setLineSpacing:5];
    [string addAttribute:NSParagraphStyleAttributeName value:paragraphStyle range:NSMakeRange(0, [string length])];

    return string;
}

+ (CGFloat)getAttStringHeightWithData:(YDCustomerAddressData *)data
{
    NSMutableAttributedString *string = [YDSelectAddressCell getAttStringWithData:data];
    
    CGFloat width = SCREEN_WIDTH - 20 - 16 - 70;
    CGFloat height = [string boundingRectWithSize:CGSizeMake(width, MAXFLOAT) options:NSStringDrawingUsesLineFragmentOrigin | NSStringDrawingUsesFontLeading context:nil].size.height;
    
    if (height < 22) {
        return 22;
    }
    
    return height;
}

- (IBAction)tapEditBtn:(id)sender
{
    if (self.editBlock) {
        self.editBlock(self.data);
    }
}

- (void)awakeFromNib
{
    [super awakeFromNib];
    
    [self.addressLabel setVerticalAlignment:VerticalAlignmentMiddle];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end

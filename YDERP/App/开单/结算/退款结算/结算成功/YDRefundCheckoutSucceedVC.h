//
//  YDRefundCheckoutSucceedVC.h
//  YDERP
//
//  Created by 何英健 on 2021/8/28.
//

#import <UIKit/UIKit.h>

@interface YDRefundCheckoutSucceedVC : UIViewController

@property (nonatomic, strong) YDCheckoutOrderData *data;

@end

//
//  YDRefundCheckoutSucceedVC.m
//  YDERP
//
//  Created by 何英健 on 2021/8/28.
//

#import "YDRefundCheckoutSucceedVC.h"

@interface YDRefundCheckoutSucceedVC ()

@property (nonatomic, weak) IBOutlet UILabel *payMoneyLabel;
@property (nonatomic, weak) IBOutlet UILabel *customerLabel;
@property (nonatomic, weak) IBOutlet UILabel *refundMoneyLabel;
@property (nonatomic, weak) IBOutlet UILabel *lossMoneyLabel;
@property (nonatomic, weak) IBOutlet UILabel *payMethodLabel;

@end

@implementation YDRefundCheckoutSucceedVC

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self initUI];
}

- (void)initUI
{
    self.title = @"结算";
    [self.navigationItem addDefaultBackButton:self actionMethod:@selector(goBackToHome)];
    
    self.payMoneyLabel.font = FONT_NUMBER(14);
    self.refundMoneyLabel.font = FONT_NUMBER(14);
    self.lossMoneyLabel.font = FONT_NUMBER(14);
    
    self.payMoneyLabel.text = [NSString applePrefixPriceWith:self.data.payAmount];
    self.customerLabel.text = self.data.clientName;
    self.refundMoneyLabel.text = [NSString applePrefixPriceWith:self.data.payAmount];
    self.lossMoneyLabel.text = [NSString applePrefixPriceWith:self.data.lossAmount];
    
    NSString *method = @"";
    for (NSString *sub in self.data.payMethodNames) {
        if (method.length) {
            method = [NSString stringWithFormat:@"%@、%@", method, sub];
            
        } else {
            method = sub;
        }
    }
    self.payMethodLabel.text = method;
}

// 打印小票
- (IBAction)tapPrintBill:(id)sender
{
    [YDPrintModel checkAndPrintWithOrderID:[NSString stringWithFormat:@"%ld", self.data.orderId] nav:self.navigationController];
}

// 点击完成回到首页
- (IBAction)tapCompleteBtn:(id)sender
{
    [self goBackToHome];
}

// 回到首页
- (void)goBackToHome
{
    [self dismissViewControllerAnimated:NO completion:^{
        [[UINavigationController rootNavigationController] popToRootViewControllerAnimated:YES];
        [UINavigationController rootNavigationController].tabBarController.selectedIndex = 0;
    }];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [self.navigationController setNavigationBarHidden:NO animated:YES];
}

@end

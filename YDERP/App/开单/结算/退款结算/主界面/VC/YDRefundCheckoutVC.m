//
//  YDRefundCheckoutVC.m
//  YDERP
//
//  Created by 何英健 on 2021/8/18.
//

#import "YDRefundCheckoutVC.h"
#import "YDRefundCheckoutCountCell.h"
#import "YDCheckoutCollectCell.h"
#import "YDCheckoutProductCell.h"
#import "YDRefundCheckoutSucceedVC.h"

@interface YDRefundCheckoutVC () <UITableViewDelegate, UITableViewDataSource>

@property (nonatomic, weak) IBOutlet UITableView *tableView;

@property (nonatomic, assign) BOOL isShowKeybord;
@property (nonatomic, assign) BOOL isShowMark;

@property (nonatomic, weak) IBOutlet NSLayoutConstraint *bottomH;

@end

@implementation YDRefundCheckoutVC

#pragma mark - 懒加载

- (YDCheckoutModel *)model
{
    if (!_model) {
        _model = [[YDCheckoutModel alloc] init];
    }
    
    return _model;
}

#pragma mark - 页面加载

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self initUI];
    
    [self addNotification];
}

- (void)initUI
{
    self.title = @"退款结算";
    [self.navigationItem addDefaultBackButton:self];
    
    if (IS_PhoneXAll) {
        self.bottomH.constant = 75;
        [self.view layoutIfNeeded];
    }
    
    [self setupTableView];
}

- (void)setupTableView
{
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    self.tableView.showsVerticalScrollIndicator = NO;
    self.tableView.showsHorizontalScrollIndicator = NO;
    
    if (@available(iOS 11.0, *)) {
        self.tableView.contentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentNever;
        
    } else {
        self.automaticallyAdjustsScrollViewInsets = NO;
    }
}

- (IBAction)tapConfirmBtn:(id)sender
{
    if ([self checkUploadAvailable]) {
        [self uploadData];
    }
}

// 请求接口进行结算
- (void)uploadData
{
    YDWeakSelf
    [YDLoadingView showToSuperview:self.view];
    
    [YDFunction modifyCheckoutWithType:1 productArray:self.model.productArray lossAmount:self.model.lossAmount orderID:self.model.orderID couponAmount:self.model.couponAmount cashPay:self.model.cashPay cardPay:self.model.cardPay remitPay:self.model.remitPay aliPay:self.model.aliPay wechatPay:self.model.wechatPay remark:self.model.remark urlArray:self.model.photoArray version:self.model.version completion:^(YDCheckoutOrderData *data) {
        [YDLoadingView hideToSuperview:weakSelf.view];
        
        // 结算成功
        [weakSelf showCheckoutSucceedWithData:data];
        
    } refreshBlock:^(NSString *msg) {
        [YDLoadingView hideToSuperview:weakSelf.view];
        // 通知外部刷新
        YDWeakSelf
        [YDNoticePopView showWithTitle:msg completion:^{
            [weakSelf.navigationController popViewControllerAnimated:YES];
            
            if (weakSelf.refreshBlock) {
                weakSelf.refreshBlock();
            }
        }];
        
    } faildBlock:^(NSString *msg) {
        [YDLoadingView hideToSuperview:weakSelf.view];
        [MBProgressHUD showToastMessage:msg];
    }];
}

- (void)showCheckoutSucceedWithData:(YDCheckoutOrderData *)data
{
    YDRefundCheckoutSucceedVC *vc = [[YDRefundCheckoutSucceedVC alloc] init];
    vc.data = data;
    [self.navigationController pushViewController:vc animated:YES];
}

// 检查是否能提交
- (BOOL)checkUploadAvailable
{
    // 应退金额
    CGFloat totalAmount = -[self.model.totalAmount doubleValue];
    // 应退金额 - 实退金额
    CGFloat money = totalAmount - self.model.totalPayMoney;
    
    if (money < 0) {
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"提醒" message:@"实际退款金额大于需退款金额，请检查并重新填写" preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction* cancel = [UIAlertAction actionWithTitle:@"确定" style:UIAlertActionStyleCancel handler:nil];
        [alert addAction:cancel];
        [self.navigationController presentViewController:alert animated:YES completion:nil];
        
        return NO;
        
    } else if (money > 0) {
        if (self.model.isCustomerInfoExist) {
            UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"提醒" message:@"实际退款金额小于需退款金额，退款后会自动存入客户余额\n（将优先偿还客户欠款）" preferredStyle:UIAlertControllerStyleAlert];
            UIAlertAction *confirm = [UIAlertAction actionWithTitle:@"确定" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                [self uploadData];
            }];
            UIAlertAction* cancel = [UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleCancel handler:nil];

            [alert addAction:cancel];
            [alert addAction:confirm];

            [self.navigationController presentViewController:alert animated:YES completion:nil];
            
        } else {
            UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"提醒" message:@"临时客户实际退款金额必须等于退款金额" preferredStyle:UIAlertControllerStyleAlert];
            UIAlertAction* cancel = [UIAlertAction actionWithTitle:@"确定" style:UIAlertActionStyleCancel handler:nil];
            [alert addAction:cancel];
            [self.navigationController presentViewController:alert animated:YES completion:nil];
        }
        
        return NO;
    }

    return YES;
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [self.navigationController setNavigationBarHidden:NO animated:YES];
}

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

#pragma mark - UITableView代理

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 3;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    switch (indexPath.section) {
        case 0:
        {
            return [YDRefundCheckoutCountCell getHeight];
        }
            
        case 1:
        {
            return [YDCheckoutCollectCell getHeight];
        }
            
        case 2:
        {
            return [YDCheckoutProductCell getHeightWithModel:self.model];
        }
            
        default:
            return 0.01;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    YDWeakSelf
    switch (indexPath.section) {
        case 0:
        {
            YDRefundCheckoutCountCell *cell = [YDRefundCheckoutCountCell cellWithTableView:tableView];
            cell.model = self.model;
            cell.modifyBlock = ^{
                dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.01 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                    [weakSelf.tableView reloadData];
                });
            };
            cell.isShowKeybord = weakSelf.isShowKeybord;
            cell.textFieldChangeBlock = ^(BOOL isShowKeybord) {
                weakSelf.isShowKeybord = isShowKeybord;
            };
            
            return cell;
        }
            
        case 1:
        {
            YDCheckoutCollectCell *cell = [YDCheckoutCollectCell cellWithTableView:tableView];
            cell.model = self.model;
            cell.modifyBlock = ^{
                dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.01 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                    [weakSelf.tableView reloadData];
                });
            };
            cell.isShowKeybord = weakSelf.isShowKeybord;
            cell.textFieldChangeBlock = ^(BOOL isShowKeybord) {
                weakSelf.isShowKeybord = isShowKeybord;
            };
            
            return cell;
        }
            
        case 2:
        {
            YDCheckoutProductCell *cell = [YDCheckoutProductCell cellWithTableView:tableView];
            cell.model = self.model;
            cell.rootVC = self;
            cell.modifyBlock = ^{
                dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.01 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                    [weakSelf.tableView reloadData];
                });
            };
            cell.showRemarkBlock = ^(BOOL isShown) {
                weakSelf.isShowMark = isShown;
            };
            
            return cell;
        }
            
        default:
            return [[UITableViewCell alloc] init];
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 40;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UIView *view = [[UIView alloc] init];
    UILabel *titleLabel = [[UILabel alloc] init];
    titleLabel.font = [UIFont systemFontOfSize:13 weight:UIFontWeightMedium];
    titleLabel.textColor = ColorFromRGB(0x999999);
    NSString *str;
    switch (section) {
        case 0:
            str = @"金额合计信息";
            break;
            
        case 1:
            str = @"填写本次退款方式";
            break;
            
        case 2:
            str = @"备注";
            break;
            
        default:
            break;
    }
    titleLabel.text = str;
    [titleLabel sizeToFit];
    [titleLabel setX:24];
    [titleLabel setY:(40 - titleLabel.height) / 2];
    [view addSubview:titleLabel];
    
    return view;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    return 0.01;
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section
{
    return [[UIView alloc] init];
}

#pragma mark - 键盘高度变化

- (void)addNotification
{
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardShow:) name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardMiss:) name:UIKeyboardWillHideNotification object:nil];
}

// 弹出键盘改变控制器view
- (void)keyboardShow:(NSNotification *)noti
{
    if (!self.isShowMark) {
        CGFloat keyboardHeight = [noti.userInfo[UIKeyboardFrameEndUserInfoKey] CGRectValue].size.height;
        [UIView animateWithDuration:0.0f animations:^{
            CGFloat orginY = self.tableView.contentOffset.y;
            self.tableView.contentOffset = CGPointMake(0, orginY + keyboardHeight - 93);
            [self.view layoutIfNeeded];
        }];
    }
}

//回收键盘改变控制器view
- (void)keyboardMiss:(NSNotification *)noti
{
    if (!self.isShowMark) {
        CGFloat keyboardHeight = [noti.userInfo[UIKeyboardFrameEndUserInfoKey] CGRectValue].size.height;
        [UIView animateWithDuration:0.0 animations:^{
            CGFloat orginY = self.tableView.contentOffset.y;
            self.tableView.contentOffset = CGPointMake(0, orginY - keyboardHeight + 93);
            [self.view layoutIfNeeded];
        }];
    }
}

@end

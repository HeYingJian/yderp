//
//  YDRefundCheckoutVC.h
//  YDERP
//
//  Created by 何英健 on 2021/8/18.
//

#import <UIKit/UIKit.h>
#import "YDCheckoutModel.h"

@interface YDRefundCheckoutVC : UIViewController

// 结算模型
@property (nonatomic, strong) YDCheckoutModel *model;

@property (nonatomic, copy) BlockLoadDataSucceed refreshBlock;

@end

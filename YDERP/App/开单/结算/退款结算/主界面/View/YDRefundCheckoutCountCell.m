//
//  YDRefundCheckoutCountCell.m
//  YDERP
//
//  Created by 何英健 on 2021/8/28.
//

#import "YDRefundCheckoutCountCell.h"

@interface YDRefundCheckoutCountCell ()

// 商品总价
@property (nonatomic, weak) IBOutlet UITextField *textField1;
// 已收金额
@property (nonatomic, weak) IBOutlet UITextField *textField2;
// 优惠填写
@property (nonatomic, weak) IBOutlet UITextField *textField3;
// 损耗扣款
@property (nonatomic, weak) IBOutlet UITextField *textField4;
// 修改后退款
@property (nonatomic, weak) IBOutlet UITextField *textField5;

@end

@implementation YDRefundCheckoutCountCell

+ (instancetype)cellWithTableView:(UITableView *)tableView
{
    static NSString *reuseID = @"refundCheckoutCountCell";
    YDRefundCheckoutCountCell *cell = [tableView dequeueReusableCellWithIdentifier:reuseID];
    if (!cell) {
        cell = [[NSBundle mainBundle] loadNibNamed:@"YDRefundCheckoutCountCell" owner:nil options:nil][0];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    
    return cell;
}

+ (CGFloat)getHeight
{
    return 336;
}

- (void)setModel:(YDCheckoutModel *)model
{
    _model = model;
    
    if (self.model.productAmount.length) {
        self.textField1.text = [NSString stringWithFormat:@"¥%@", self.model.productAmount];
        
    } else {
        self.textField1.text = @"¥0.00";
    }
    
    if (self.model.payAmount.length) {
        self.textField2.text = [NSString stringWithFormat:@"¥%@", self.model.payAmount];
        
    } else {
        self.textField2.text = @"¥0.00";
    }
   
    if (self.model.couponAmount.length) {
        self.textField3.text = [NSString stringWithFormat:@"¥%@", self.model.couponAmount];
        
    } else {
        self.textField3.text = @"¥0.00";
    }
    
    if (self.model.lossAmount.length) {
        self.textField4.text = [NSString stringWithFormat:@"¥%@", self.model.lossAmount];
        
    } else {
        self.textField4.text = nil;
    }
    
    if (self.model.totalAmount.length) {
        CGFloat totalAmount = [self.model.totalAmount doubleValue];
        NSString *totalAmountStr = [NSString stringWithFormat:@"¥%.2f", totalAmount];
        self.textField5.text = [totalAmountStr stringByReplacingOccurrencesOfString:@"-" withString:@""];
        
    } else {
        self.textField5.text = @"¥0.00";
    }
}

- (IBAction)tapNoticeBtn:(id)sender
{
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"退款时发现货物有损耗，可收取损耗扣款，损耗扣款不能大于本次退款金额" message:nil preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *cancel = [UIAlertAction actionWithTitle:@"确定" style:UIAlertActionStyleCancel handler:nil];
    [alert addAction:cancel];
    [[UINavigationController rootNavigationController] presentViewController:alert animated:YES completion:nil];
}

- (void)awakeFromNib
{
    [super awakeFromNib];

    NSMutableAttributedString *str = [[NSMutableAttributedString alloc] initWithString:@"¥0.00" attributes:
         @{NSForegroundColorAttributeName:ColorFromRGB(0xB7B7B7),
           NSFontAttributeName:[UIFont systemFontOfSize:15]}
         ];
    self.textField4.attributedPlaceholder = str;
    
    [self.textField4 addToolSenderWithBlock:nil];
    
    self.textField1.font = FONT_NUMBER(15);
    self.textField2.font = FONT_NUMBER(15);
    self.textField3.font = FONT_NUMBER(15);
    self.textField4.font = FONT_NUMBER(15);
    self.textField5.font = FONT_NUMBER(21);
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

#pragma mark - UITextFieldDelegate

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    
    return YES;
}

- (void)setModelWithTextField:(UITextField *)textField
{
    CGFloat money = [textField.text doubleValue];
    NSString *moneyStr;
    if (money >= 1000000) {
        moneyStr = @"999999.99";
        
    } else {
        moneyStr = NSStringFormat(@"%.2f", money);
    }
    
    if (textField == self.textField4) {
        self.model.lossAmount = moneyStr;
    }
    
    if (self.modifyBlock) {
        self.modifyBlock();
    }
}

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    if (self.isShowKeybord) {
        [self.window endEditing:YES];
        return NO;
        
    } else {
        if (self.textFieldChangeBlock) {
            self.isShowKeybord = YES;
            self.textFieldChangeBlock(YES);
        }
    }
    
    textField.text = nil;
    return YES;
}

- (BOOL)textFieldShouldEndEditing:(UITextField *)textField
{
    if (self.textFieldChangeBlock) {
        self.isShowKeybord = NO;
        self.textFieldChangeBlock(NO);
    }
    
    // 没有输入任何东西，则不修改
    if (!textField.text.length) {
        if (self.modifyBlock) {
            self.modifyBlock();
        }
        
        return YES;
    }
    
    CGFloat money = [textField.text doubleValue];
    if (money >= 1000000) {
        [MBProgressHUD showToastMessage:@"金额必须小于100万"];
        
    } else if (money < 0) {
        textField.text = [textField.text stringByReplacingOccurrencesOfString:@"-" withString:@""];
    }
    
    [self setModelWithTextField:textField];
    
    return YES;
}

@end

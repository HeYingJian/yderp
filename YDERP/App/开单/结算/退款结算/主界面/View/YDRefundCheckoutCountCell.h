//
//  YDRefundCheckoutCountCell.h
//  YDERP
//
//  Created by 何英健 on 2021/8/28.
//

#import <UIKit/UIKit.h>
#import "YDCheckoutModel.h"

typedef void(^BlockTextFieldChange)(BOOL isShowKeybord);

@interface YDRefundCheckoutCountCell : UITableViewCell

+ (instancetype)cellWithTableView:(UITableView *)tableView;

+ (CGFloat)getHeight;

@property (nonatomic, strong) YDCheckoutModel *model;

@property (nonatomic, copy) BlockCheckOutModify modifyBlock;

@property (nonatomic, assign) BOOL isShowKeybord;
@property (nonatomic, copy) BlockTextFieldChange textFieldChangeBlock;

@end

//
//  YDCheckoutSucceedVC.m
//  YDERP
//
//  Created by 何英健 on 2021/8/21.
//

#import "YDCheckoutSucceedVC.h"
#import "YDSupplyDetailVC.h"

@interface YDCheckoutSucceedVC ()

@property (nonatomic, weak) IBOutlet UILabel *payMoneyLabel;
@property (nonatomic, weak) IBOutlet UILabel *customerLabel;
@property (nonatomic, weak) IBOutlet UILabel *totalMoneyLabel;
@property (nonatomic, weak) IBOutlet UILabel *couponMoneyLabel;
@property (nonatomic, weak) IBOutlet UILabel *currentPayMoneyLabel;
@property (nonatomic, weak) IBOutlet UILabel *dueMoneyLabel;
@property (nonatomic, weak) IBOutlet UILabel *payMethodLabel;
@property (nonatomic, weak) IBOutlet UILabel *orderNoLabel;

@property (nonatomic, weak) IBOutlet UIButton *completeBtn;
@property (nonatomic, weak) IBOutlet UIButton *goOnBtn;
@property (nonatomic, weak) IBOutlet UIButton *backToHomeBtn;

@end

@implementation YDCheckoutSucceedVC

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self initUI];
}

- (void)initUI
{
    self.title = @"结算";
    [self.navigationItem addDefaultBackButton:self actionMethod:@selector(goBackToHome)];
    
    if (self.type == 0) {
        self.completeBtn.hidden = YES;
        
    } else {
        self.goOnBtn.hidden = YES;
        self.backToHomeBtn.hidden = YES;
    }
    
    self.backToHomeBtn.layer.borderColor = ColorFromRGB(0xD7D9E0).CGColor;
    self.backToHomeBtn.layer.borderWidth = 1;
    
    self.payMoneyLabel.font = FONT_NUMBER(14);
    self.totalMoneyLabel.font = FONT_NUMBER(14);
    self.couponMoneyLabel.font = FONT_NUMBER(14);
    self.currentPayMoneyLabel.font = FONT_NUMBER(14);
    self.dueMoneyLabel.font = FONT_NUMBER(14);
    self.orderNoLabel.font = FONT_NUMBER(14);
    
    self.payMoneyLabel.text = [NSString applePrefixPriceWith:self.data.payAmount];
    self.customerLabel.text = self.data.clientName;
    self.totalMoneyLabel.text = [NSString applePrefixPriceWith:self.data.totalAmount];
    self.couponMoneyLabel.text = [NSString applePrefixPriceWith:self.data.couponAmount];
    self.currentPayMoneyLabel.text = [NSString applePrefixPriceWith:self.data.payAmount];
    self.dueMoneyLabel.text = [NSString applePrefixPriceWith:self.data.dueAmount];
    
    NSString *method = @"";
    for (NSString *sub in self.data.payMethodNames) {
        if (method.length) {
            method = [NSString stringWithFormat:@"%@、%@", method, sub];
            
        } else {
            method = sub;
        }
    }
    self.payMethodLabel.text = method;
    
    self.orderNoLabel.text = self.data.orderNo;
}

// 打印小票
- (IBAction)tapPrintBill:(id)sender
{
    [YDPrintModel checkAndPrintWithOrderID:[NSString stringWithFormat:@"%ld", self.data.orderId] nav:self.navigationController];
}

// 去配货
- (IBAction)tapShowSupply:(id)sender
{
    YDSupplyDetailVC *vc = [[YDSupplyDetailVC alloc] init];
    vc.ID = [NSString stringWithFormat:@"%ld", (long)self.data.allocOrderId];
    vc.lastController = self;
    [self.navigationController pushViewController:vc animated:YES];
}

// 点击完成回到首页
- (IBAction)tapCompleteBtn:(id)sender
{
    [self goBackToHome];
}

// 点击回到首页
- (IBAction)tapGoHomeBtn:(id)sender
{
    [self goBackToHome];
}

// 继续开单
- (IBAction)tapGoOnBtn:(id)sender
{
    PostNotification(MAKE_LIST_CLEAR_DATA, nil);
    [self.navigationController popToRootViewControllerAnimated:YES];
}

// 回到首页
- (void)goBackToHome
{
    [self dismissViewControllerAnimated:NO completion:^{
        [[UINavigationController rootNavigationController] popToRootViewControllerAnimated:YES];
        [UINavigationController rootNavigationController].tabBarController.selectedIndex = 0;
    }];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [self.navigationController setNavigationBarHidden:NO animated:YES];
}

@end

//
//  YDCheckoutSucceedVC.h
//  YDERP
//
//  Created by 何英健 on 2021/8/21.
//

#import <UIKit/UIKit.h>

@interface YDCheckoutSucceedVC : UIViewController

@property (nonatomic, strong) YDCheckoutOrderData *data;

// 0-新开单结算 1-订单详情收款按钮进入 2-订单详情页修改订单按钮进入
@property (nonatomic, assign) NSInteger type;

@end

//
//  YDCheckoutVC.m
//  YDERP
//
//  Created by 何英健 on 2021/8/18.
//

#import "YDCheckoutVC.h"
#import "YDCheckoutCountCell.h"
#import "YDCheckoutCollectCell.h"
#import "YDCheckoutProductCell.h"
#import "YDCheckoutSucceedVC.h"

@interface YDCheckoutVC () <UITableViewDelegate, UITableViewDataSource>

@property (nonatomic, weak) IBOutlet UITableView *tableView;

@property (nonatomic, assign) BOOL isShowKeybord;
@property (nonatomic, assign) BOOL isShowMark;

@property (nonatomic, weak) IBOutlet NSLayoutConstraint *bottomH;

@end

@implementation YDCheckoutVC

#pragma mark - 懒加载

- (YDCheckoutModel *)model
{
    if (!_model) {
        _model = [[YDCheckoutModel alloc] init];
    }
    
    return _model;
}

#pragma mark - 页面加载

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self initUI];
    
    [self addNotification];
}

- (void)initUI
{
    if (self.type == 0) {
        self.title = @"结算";
        
    } else {
        self.title = @"订单收款";
    }
    
    [self.navigationItem addDefaultBackButton:self actionMethod:@selector(goBack)];
    
    if (IS_PhoneXAll) {
        self.bottomH.constant = 75;
        [self.view layoutIfNeeded];
    }
    
    [self setupTableView];
}

- (void)setupTableView
{
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    self.tableView.showsVerticalScrollIndicator = NO;
    self.tableView.showsHorizontalScrollIndicator = NO;
    
    if (@available(iOS 11.0, *)) {
        self.tableView.contentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentNever;
        
    } else {
        self.automaticallyAdjustsScrollViewInsets = NO;
    }
}

- (IBAction)tapConfirmBtn:(id)sender
{
    if ([self checkUploadAvailable]) {
        [self uploadData];
    }
}

// 请求接口进行结算
- (void)uploadData
{
    YDWeakSelf
    [YDLoadingView showToSuperview:self.view];
    
    if (self.model.productArray.count) {
        // 有商品改动
        [YDFunction modifyCheckoutWithType:0 productArray:self.model.productArray lossAmount:nil orderID:self.model.orderID couponAmount:self.model.couponAmount cashPay:self.model.cashPay cardPay:self.model.cardPay remitPay:self.model.remitPay aliPay:self.model.aliPay wechatPay:self.model.wechatPay remark:self.model.remark urlArray:self.model.photoArray version:self.model.version completion:^(YDCheckoutOrderData *data) {
            [YDLoadingView hideToSuperview:weakSelf.view];

            // 结算成功
            [weakSelf showCheckoutSucceedWithData:data];
            
        } refreshBlock:^(NSString *msg) {
            [YDLoadingView hideToSuperview:weakSelf.view];
            // 通知外部刷新
            [YDNoticePopView showWithTitle:msg completion:^{
                [weakSelf.navigationController popViewControllerAnimated:YES];
                
                if (weakSelf.refreshBlock) {
                    weakSelf.refreshBlock();
                }
            }];
            
        } faildBlock:^(NSString *msg) {
            [YDLoadingView hideToSuperview:weakSelf.view];
            [MBProgressHUD showToastMessage:msg];
        }];
        
    } else {
        // 无商品改动
        [YDFunction checkoutWithOrderID:self.model.orderID couponAmount:self.model.couponAmount cashPay:self.model.cashPay cardPay:self.model.cardPay remitPay:self.model.remitPay aliPay:self.model.aliPay wechatPay:self.model.wechatPay remark:self.model.remark urlArray:self.model.photoArray version:self.model.version completion:^(YDCheckoutOrderData *data) {
            [YDLoadingView hideToSuperview:weakSelf.view];
            
            // 结算成功
            [weakSelf showCheckoutSucceedWithData:data];
            
        } refreshBlock:^(NSString *msg) {
            [YDLoadingView hideToSuperview:weakSelf.view];
            // 通知外部刷新
            [YDNoticePopView showWithTitle:msg completion:^{
                [weakSelf.navigationController popViewControllerAnimated:YES];
                
                if (weakSelf.refreshBlock) {
                    weakSelf.refreshBlock();
                }
            }];

        } faildBlock:^(NSString *msg) {
            [YDLoadingView hideToSuperview:weakSelf.view];
            [MBProgressHUD showToastMessage:msg];
        }];
    }
}

- (void)showCheckoutSucceedWithData:(YDCheckoutOrderData *)data
{
    YDCheckoutSucceedVC *vc = [[YDCheckoutSucceedVC alloc] init];
    vc.data = data;
    vc.type = self.type;
    [self.navigationController pushViewController:vc animated:YES];
}

// 检查是否能提交
- (BOOL)checkUploadAvailable
{
    // 应付金额
    double totalAmount = [self.model.totalAmount doubleValue];
    // 应付金额 - 实付金额
    double money = totalAmount - self.model.totalPayMoney;
    
    BOOL showNotice = NO;
    NSString *notice;
    if (self.model.isCustomerInfoExist) {
        if (money < 0) {
            UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"支付金额超过订单实收金额，超过部分将会转入客户余额是否继续结算？" message:nil preferredStyle:UIAlertControllerStyleAlert];
            UIAlertAction *confirm = [UIAlertAction actionWithTitle:@"确定" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                [self uploadData];
            }];
            UIAlertAction* cancel = [UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleCancel handler:nil];

            [alert addAction:cancel];
            [alert addAction:confirm];

            [self.navigationController presentViewController:alert animated:YES completion:nil];
            
            return NO;
        }
        
    } else {
        if (money >= 0.01) {
            showNotice = YES;
            notice = @"临时客户不允许欠款\n需一次清结算完";
            
        } else if (money <= -0.01) {
            showNotice = YES;
            notice = @"临时客户没有客户余额，不能多收款，请检查收款金额";
        }
        
        if (showNotice) {
            UIAlertController *alert = [UIAlertController alertControllerWithTitle:notice message:nil preferredStyle:UIAlertControllerStyleAlert];
            UIAlertAction* cancel = [UIAlertAction actionWithTitle:@"确定" style:UIAlertActionStyleCancel handler:nil];

            [alert addAction:cancel];

            [self.navigationController presentViewController:alert animated:YES completion:nil];
            
            return NO;
        }
    }

    return YES;
}

- (void)goBack
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [self.navigationController setNavigationBarHidden:NO animated:YES];
}

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

#pragma mark - UITableView代理

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 3;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    switch (indexPath.section) {
        case 0:
        {
            return [YDCheckoutCountCell getHeightWithModel:self.model];
        }
            
        case 1:
        {
            return [YDCheckoutCollectCell getHeight];
        }
            
        case 2:
        {
            return [YDCheckoutProductCell getHeightWithModel:self.model];
        }
            
        default:
            return 0.01;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    YDWeakSelf
    switch (indexPath.section) {
        case 0:
        {
            YDCheckoutCountCell *cell = [YDCheckoutCountCell cellWithTableView:tableView];
            cell.model = self.model;
            if (self.type == 2) {
                cell.itemAmountTitlelabel.text = @"修改后商品合计金额";
            }
            cell.modifyBlock = ^{
                dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.01 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                    [weakSelf.tableView reloadData];
                });
            };
            cell.isShowKeybord = weakSelf.isShowKeybord;
            cell.textFieldChangeBlock = ^(BOOL isShowKeybord) {
                weakSelf.isShowKeybord = isShowKeybord;
            };
            
            return cell;
        }
            
        case 1:
        {
            YDCheckoutCollectCell *cell = [YDCheckoutCollectCell cellWithTableView:tableView];
            cell.model = self.model;
            cell.modifyBlock = ^{
                dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.01 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                    [weakSelf.tableView reloadData];
                });
            };
            cell.isShowKeybord = weakSelf.isShowKeybord;
            cell.textFieldChangeBlock = ^(BOOL isShowKeybord) {
                weakSelf.isShowKeybord = isShowKeybord;
            };
            
            return cell;
        }
            
        case 2:
        {
            YDCheckoutProductCell *cell = [YDCheckoutProductCell cellWithTableView:tableView];
            cell.model = self.model;
            cell.rootVC = self;
            cell.modifyBlock = ^{
                dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.01 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                    [weakSelf.tableView reloadData];
                });
            };
            cell.showRemarkBlock = ^(BOOL isShown) {
                weakSelf.isShowMark = isShown;
            };
            
            return cell;
        }
            
        default:
            return [[UITableViewCell alloc] init];
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 40;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UIView *view = [[UIView alloc] init];
    UILabel *titleLabel = [[UILabel alloc] init];
    titleLabel.font = [UIFont systemFontOfSize:13 weight:UIFontWeightMedium];
    titleLabel.textColor = ColorFromRGB(0x999999);
    NSString *str;
    switch (section) {
        case 0:
            str = @"金额合计信息";
            break;
            
        case 1:
            str = @"填写本次收款方式";
            break;
            
        case 2:
            str = @"备注";
            break;
            
        default:
            break;
    }
    titleLabel.text = str;
    [titleLabel sizeToFit];
    [titleLabel setX:24];
    [titleLabel setY:(40 - titleLabel.height) / 2];
    [view addSubview:titleLabel];
    
    return view;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    return 0.01;
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section
{
    return [[UIView alloc] init];
}

#pragma mark - 键盘高度变化

- (void)addNotification
{
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardShow:) name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardMiss:) name:UIKeyboardWillHideNotification object:nil];
}

// 弹出键盘改变控制器view
- (void)keyboardShow:(NSNotification *)noti
{
    if (!self.isShowMark) {
        CGFloat keyboardHeight = [noti.userInfo[UIKeyboardFrameEndUserInfoKey] CGRectValue].size.height;
        [UIView animateWithDuration:0.0f animations:^{
            CGFloat orginY = self.tableView.contentOffset.y;
            self.tableView.contentOffset = CGPointMake(0, orginY + keyboardHeight - 93);
            [self.view layoutIfNeeded];
        }];
    }
}

//回收键盘改变控制器view
- (void)keyboardMiss:(NSNotification *)noti
{
    if (!self.isShowMark) {
        CGFloat keyboardHeight = [noti.userInfo[UIKeyboardFrameEndUserInfoKey] CGRectValue].size.height;
        [UIView animateWithDuration:0.0 animations:^{
            CGFloat orginY = self.tableView.contentOffset.y;
            self.tableView.contentOffset = CGPointMake(0, orginY - keyboardHeight + 93);
            [self.view layoutIfNeeded];
        }];
    }
}

@end

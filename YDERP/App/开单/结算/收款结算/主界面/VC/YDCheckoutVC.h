//
//  YDCheckoutVC.h
//  YDERP
//
//  Created by 何英健 on 2021/8/18.
//

#import <UIKit/UIKit.h>
#import "YDCheckoutModel.h"

@interface YDCheckoutVC : UIViewController

// 结算模型
@property (nonatomic, strong) YDCheckoutModel *model;

// 0-新开单结算 1-订单详情收款按钮进入 2-订单详情页修改订单按钮进入
@property (nonatomic, assign) NSInteger type;

@property (nonatomic, copy) BlockLoadDataSucceed refreshBlock;

@end

//
//  YDCheckoutProductCell.h
//  YDERP
//
//  Created by 何英健 on 2021/8/18.
//

#import <UIKit/UIKit.h>
#import "YDCheckoutModel.h"

typedef void(^BlockCheckoutProductRemarkShow)(BOOL isShown);

@interface YDCheckoutProductCell : UITableViewCell

+ (instancetype)cellWithTableView:(UITableView *)tableView;

+ (CGFloat)getHeightWithModel:(YDCheckoutModel *)model;

@property (nonatomic, strong) YDCheckoutModel *model;
@property (nonatomic, weak) UIViewController *rootVC;

@property (nonatomic, copy) BlockCheckOutModify modifyBlock;
@property (nonatomic, copy) BlockCheckoutProductRemarkShow showRemarkBlock;

@end

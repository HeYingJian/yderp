//
//  YDCheckoutProductCell.m
//  YDERP
//
//  Created by 何英健 on 2021/8/18.
//

#import "YDCheckoutProductCell.h"
#import "YDModifyMessagePopView.h"
#import "YDAddProductColCell.h"

#define CHECKOUT_PHOTO_MAXCOUNT 5

@interface YDCheckoutProductCell () <UITextFieldDelegate, UICollectionViewDelegate, UICollectionViewDataSource, WDImageBrowserDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate>

@property (nonatomic, weak) IBOutlet UITextField *textField;
@property (nonatomic, weak) IBOutlet UILabel *timeLabel;

@property (nonatomic, weak) IBOutlet UICollectionView *collectionView;

@end

@implementation YDCheckoutProductCell

+ (instancetype)cellWithTableView:(UITableView *)tableView
{
    static NSString *reuseID = @"checkoutProductCell";
    YDCheckoutProductCell *cell = [tableView dequeueReusableCellWithIdentifier:reuseID];
    if (!cell) {
        cell = [[NSBundle mainBundle] loadNibNamed:@"YDCheckoutProductCell" owner:nil options:nil][0];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        
        cell.collectionView.showsHorizontalScrollIndicator = NO;
        
        UICollectionViewFlowLayout *layout = [[UICollectionViewFlowLayout alloc] init];
        // cell宽度
        CGFloat cellWidth = [YDCheckoutProductCell getCollectionViewCellBorder];
        // cell与cell之间间距
        layout.minimumLineSpacing = 9;
        layout.itemSize = CGSizeMake(cellWidth, cellWidth);
        cell.collectionView.collectionViewLayout = layout;
        
        [cell.collectionView registerClass:[YDAddProductColCell class] forCellWithReuseIdentifier:@"AddProductColCell"];
    }
    
    return cell;
}

+ (CGFloat)getHeightWithModel:(YDCheckoutModel *)model
{
    NSInteger rowNum = (model.photoArray.count + 1) / 3;
    if (rowNum * 3 < model.photoArray.count + 1) {
        rowNum++;
    }
    
    CGFloat cellH = [YDCheckoutProductCell getCollectionViewCellBorder];
    CGFloat colViewH = rowNum * cellH + (rowNum - 1) * 9;
    
    return 119 + colViewH + 73;
}

+ (CGFloat)getCollectionViewCellBorder
{
    return (SCREEN_WIDTH - 22 * 2 - 9 * 2) / 3 - 1;
}

- (void)setModel:(YDCheckoutModel *)model
{
    _model = model;
    
    self.textField.text = model.remark;
    self.timeLabel.text = model.orderTime;
}

- (void)awakeFromNib
{
    [super awakeFromNib];
    
    NSMutableAttributedString *str = [[NSMutableAttributedString alloc] initWithString:@"请填写备注信息" attributes:
         @{NSForegroundColorAttributeName:ColorFromRGB(0xB7B7B7),
           NSFontAttributeName:[UIFont systemFontOfSize:15]}
         ];
    self.textField.attributedPlaceholder = str;
    
    self.timeLabel.font = FONT_NUMBER(15);
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

#pragma mark - UICollectionView 代理

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    if (self.model.photoArray.count >= CHECKOUT_PHOTO_MAXCOUNT) {
        return self.model.photoArray.count;
        
    } else {
        return self.model.photoArray.count + 1;
    }
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    YDAddProductColCell *cell = [YDAddProductColCell cellWithTableView:collectionView indexPath:indexPath];
    if (indexPath.row != 0 || self.model.photoArray.count >= CHECKOUT_PHOTO_MAXCOUNT) {
        NSInteger photoIndex = 0;
        if (self.model.photoArray.count >= CHECKOUT_PHOTO_MAXCOUNT) {
            photoIndex = indexPath.row;
            
        } else {
            photoIndex = indexPath.row - 1;
        }
        cell.picUrl = self.model.photoArray[photoIndex];
        
        YDWeakSelf
        cell.tapDeleteBtnBlock = ^(NSString *url) {
            for (NSString *photoUrl in weakSelf.model.photoArray) {
                if ([url isEqualToString:photoUrl]) {
                    [weakSelf.model.photoArray removeObject:photoUrl];
                    break;
                }
            }
            
            if (self.modifyBlock) {
                self.modifyBlock();
            }
        };
        
        cell.showDeleteBtn = YES;
        
    } else {
        cell.showDeleteBtn = NO;
    }
    
    return cell;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row == 0 && self.model.photoArray.count < CHECKOUT_PHOTO_MAXCOUNT) {
        [self addPhotos];
        
    } else {
        UICollectionViewCell *cell = [self.collectionView cellForItemAtIndexPath:indexPath];
        
        NSInteger tapIndex;
        if (self.model.photoArray.count < CHECKOUT_PHOTO_MAXCOUNT) {
            tapIndex = indexPath.row - 1;
            
        } else {
            tapIndex = indexPath.row;
        }
        
        // 浏览图片相册
        WDImageBrowser *browser = [[WDImageBrowser alloc] init];
        [browser setupWithDelegate:self tappedIndex:tapIndex imageUrls:self.model.photoArray originView:cell];
        [browser showBrowser];
    }
}

#pragma mark - 上传图片

- (void)addPhotos
{
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:nil message:nil preferredStyle:UIAlertControllerStyleActionSheet];
    
    // 需要添加此项设置，否则ipad会崩溃
    alert.popoverPresentationController.sourceView = self.rootVC.view;
    alert.popoverPresentationController.sourceRect = CGRectMake(0, SCREEN_HEIGHT - 70, SCREEN_WIDTH, 70);
    
    //相机
    UIAlertAction *camera = [UIAlertAction actionWithTitle:@"拍照" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        [self showCarmera];
    }];
    
    //相册
    UIAlertAction* library = [UIAlertAction actionWithTitle:@"我的相册" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        [self showPhotoAlbum];
    }];
    
    //取消
    UIAlertAction* cancel = [UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleCancel handler:nil];
    
    [alert addAction:camera];
    [alert addAction:library];
    [alert addAction:cancel];
    
    [self.rootVC presentViewController:alert animated:YES completion:nil];
}

- (void)showCarmera
{
    if (![YDPhotoRight checkHaveCameraRight]) {
        return;
    }
    
    YDImagePickerController *ipvc = [[YDImagePickerController alloc] init];
    ipvc.delegate = self;
    ipvc.sourceType = UIImagePickerControllerSourceTypeCamera;
    ipvc.modalPresentationStyle = UIModalPresentationFullScreen;

    [self.rootVC showViewController:ipvc sender:nil];
}

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary<UIImagePickerControllerInfoKey,id> *)info
{
    UIImage *image = [info objectForKey:UIImagePickerControllerOriginalImage];

    if (image) {
        [self uploadImgArray:[NSArray arrayWithObject:image]];
    }
    
    [picker dismissViewControllerAnimated:YES completion:nil];
}

// 上传图片
- (void)uploadImgArray:(NSArray<UIImage *> *)imgArray
{
    [YDLoadingView showToSuperview:self.rootVC.view];
    
    YDWeakSelf
    [YDFunction uploadImageArray:imgArray type:1 group:1 groupStr:nil parameters:nil sucess:^(NSMutableArray<NSString *> *urlArray) {
        [YDLoadingView hideToSuperview:weakSelf.rootVC.view];
        
        [weakSelf.model.photoArray addObjectsFromArray:urlArray];
        
        if (weakSelf.modifyBlock) {
            weakSelf.modifyBlock();
        }
        
    } failure:^(NSString *msg) {
        [YDLoadingView hideToSuperview:weakSelf.rootVC.view];
        [MBProgressHUD showToastMessage:msg];
    }];
}

// 显示相册选择
- (void)showPhotoAlbum
{
    if (![YDPhotoRight checkHavePhotoLibraryRight]) {
        return;
    }
    
    YDWeakSelf
    TZImagePickerController *imagePickerVc = [[TZImagePickerController alloc] initWithMaxImagesCount:CHECKOUT_PHOTO_MAXCOUNT - self.model.photoArray.count delegate:nil];
    imagePickerVc.allowPickingOriginalPhoto = NO;
    imagePickerVc.showSelectedIndex = YES;
    imagePickerVc.naviTitleColor = ColorFromRGB(0x0095F7);
    imagePickerVc.barItemTextColor = ColorFromRGB(0x0095F7);
    imagePickerVc.allowPickingVideo = NO;
    imagePickerVc.modalPresentationStyle = UIModalPresentationFullScreen;
    [imagePickerVc setDidFinishPickingPhotosHandle:^(NSArray<UIImage *> *photos, NSArray *assets, BOOL isSelectOriginalPhoto) {
        [weakSelf uploadImgArray:photos];
    }];
    [self.rootVC presentViewController:imagePickerVc animated:YES completion:nil];
}


#pragma mark - UITextFieldDelegate

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    [self.window endEditing:YES];
    
    if (self.showRemarkBlock) {
        self.showRemarkBlock(YES);
    }
    
    YDWeakSelf
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.3 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [YDModifyMessagePopView showWithTitle:@"请输入备注信息" content:self.model.remark placeholder:@"最多可填写200字备注" completion:^(NSString *string, BlockModifyMessageStatus statusBlock) {
            if (string.length > 200) {
                [MBProgressHUD showToastMessage:@"最多可填写200字"];
                statusBlock(NO);
                return;
            }
            
            weakSelf.model.remark = string;
            if (weakSelf.modifyBlock) {
                weakSelf.modifyBlock();
            }
            
            if (self.showRemarkBlock) {
                self.showRemarkBlock(NO);
            }
            
            statusBlock(YES);
            
        } tapCloseBlock:^{
            if (self.showRemarkBlock) {
                self.showRemarkBlock(NO);
            }
        }];
    });
    
    return NO;
}

@end

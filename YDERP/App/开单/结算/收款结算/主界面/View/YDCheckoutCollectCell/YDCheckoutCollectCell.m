//
//  YDCheckoutCollectCell.m
//  YDERP
//
//  Created by 何英健 on 2021/8/18.
//

#import "YDCheckoutCollectCell.h"

@interface YDCheckoutCollectCell () <UITextFieldDelegate>

@property (nonatomic, weak) IBOutlet UITextField *textField1;
@property (nonatomic, weak) IBOutlet UITextField *textField2;
@property (nonatomic, weak) IBOutlet UITextField *textField3;
@property (nonatomic, weak) IBOutlet UITextField *textField4;
@property (nonatomic, weak) IBOutlet UITextField *textField5;

@end

@implementation YDCheckoutCollectCell

+ (instancetype)cellWithTableView:(UITableView *)tableView
{
    static NSString *reuseID = @"checkoutCollectCell";
    YDCheckoutCollectCell *cell = [tableView dequeueReusableCellWithIdentifier:reuseID];
    if (!cell) {
        cell = [[NSBundle mainBundle] loadNibNamed:@"YDCheckoutCollectCell" owner:nil options:nil][0];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    
    return cell;
}

+ (CGFloat)getHeight
{
    return 325;
}

- (void)setModel:(YDCheckoutModel *)model
{
    _model = model;
    
    if (self.model.cashPay.length) {
        self.textField1.text = [NSString stringWithFormat:@"¥%@", self.model.cashPay];
        
    } else {
        self.textField1.text = nil;
    }
    
    if (self.model.cardPay.length) {
        self.textField2.text = [NSString stringWithFormat:@"¥%@", self.model.cardPay];
        
    } else {
        self.textField2.text = nil;
    }
    
    if (self.model.remitPay.length) {
        self.textField3.text = [NSString stringWithFormat:@"¥%@", self.model.remitPay];
        
    } else {
        self.textField3.text = nil;
    }
    
    if (self.model.aliPay.length) {
        self.textField4.text = [NSString stringWithFormat:@"¥%@", self.model.aliPay];
        
    } else {
        self.textField4.text = nil;
    }
    
    if (self.model.wechatPay.length) {
        self.textField5.text = [NSString stringWithFormat:@"¥%@", self.model.wechatPay];
        
    } else {
        self.textField5.text = nil;
    }
}

- (void)awakeFromNib
{
    [super awakeFromNib];
    
    NSMutableAttributedString *str = [[NSMutableAttributedString alloc] initWithString:@"¥0.00" attributes:
         @{NSForegroundColorAttributeName:ColorFromRGB(0xB7B7B7),
           NSFontAttributeName:[UIFont systemFontOfSize:15]}
         ];
    self.textField1.attributedPlaceholder = str;
    self.textField2.attributedPlaceholder = str;
    self.textField3.attributedPlaceholder = str;
    self.textField4.attributedPlaceholder = str;
    self.textField5.attributedPlaceholder = str;

    [self.textField1 addToolSenderWithBlock:nil];
    [self.textField2 addToolSenderWithBlock:nil];
    [self.textField3 addToolSenderWithBlock:nil];
    [self.textField4 addToolSenderWithBlock:nil];
    [self.textField5 addToolSenderWithBlock:nil];
    
    self.textField1.font = FONT_NUMBER(15);
    self.textField2.font = FONT_NUMBER(15);
    self.textField3.font = FONT_NUMBER(15);
    self.textField4.font = FONT_NUMBER(15);
    self.textField5.font = FONT_NUMBER(15);
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

#pragma mark - UITextFieldDelegate

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    
    return YES;
}

- (void)setModelWithTextField:(UITextField *)textField
{
    CGFloat money = [textField.text doubleValue];
    NSString *moneyStr;
    if (money >= 1000000) {
        moneyStr = @"999999.99";
        
    } else {
        moneyStr = NSStringFormat(@"%.2f", money);
    }
    
    if (textField == self.textField1) {
        self.model.cashPay = moneyStr;
        
    } else if (textField == self.textField2) {
        self.model.cardPay = moneyStr;

    } else if (textField == self.textField3) {
        self.model.remitPay = moneyStr;
        
    } else if (textField == self.textField4) {
        self.model.aliPay = moneyStr;
        
    } else if (textField == self.textField5) {
        self.model.wechatPay = moneyStr;
    }
    
    if (self.modifyBlock) {
        self.modifyBlock();
    }
}

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    if (self.isShowKeybord) {
        [self.window endEditing:YES];
        return NO;
        
    } else {
        if (self.textFieldChangeBlock) {
            self.isShowKeybord = YES;
            self.textFieldChangeBlock(YES);
        }
    }
    
    textField.text = nil;
    return YES;
}

- (BOOL)textFieldShouldEndEditing:(UITextField *)textField
{
    if (self.textFieldChangeBlock) {
        self.isShowKeybord = NO;
        self.textFieldChangeBlock(NO);
    }
    
    // 没有输入任何东西，则不修改
    if (!textField.text.length) {
        if (self.modifyBlock) {
            self.modifyBlock();
        }
        
        return YES;
    }
    
    CGFloat money = [textField.text doubleValue];
    if (money >= 1000000) {
        [MBProgressHUD showToastMessage:@"金额必须小于100万"];
        
    } else if (money < 0) {
        textField.text = [textField.text stringByReplacingOccurrencesOfString:@"-" withString:@""];
    }
    
    [self setModelWithTextField:textField];
    
    return YES;
}

@end

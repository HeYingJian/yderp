//
//  YDCheckoutCountCell.h
//  YDERP
//
//  Created by 何英健 on 2021/8/18.
//

#import <UIKit/UIKit.h>
#import "YDCheckoutModel.h"

typedef void(^BlockTextFieldChange)(BOOL isShowKeybord);

@interface YDCheckoutCountCell : UITableViewCell

+ (instancetype)cellWithTableView:(UITableView *)tableView;

+ (CGFloat)getHeightWithModel:(YDCheckoutModel *)model;

@property (nonatomic, strong) YDCheckoutModel *model;

@property (nonatomic, copy) BlockCheckOutModify modifyBlock;

@property (nonatomic, assign) BOOL isShowKeybord;
@property (nonatomic, copy) BlockTextFieldChange textFieldChangeBlock;

@property (nonatomic, weak) IBOutlet UILabel *itemAmountTitlelabel;

@end

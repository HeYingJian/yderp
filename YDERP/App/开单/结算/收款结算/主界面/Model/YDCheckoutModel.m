//
//  YDCheckoutModel.m
//  YDERP
//
//  Created by 何英健 on 2021/8/18.
//

#import "YDCheckoutModel.h"

@implementation YDCheckoutModel

- (NSMutableArray<NSString *> *)photoArray
{
    if (!_photoArray) {
        _photoArray = [NSMutableArray array];
    }
    
    return _photoArray;
}

- (double)balanceDeduct
{
    if (self.balance <= 0) {
        return 0;
    }
    
    double product = [self.productAmount doubleValue];
    double coupon = [self.couponAmount doubleValue];
    double payAmount = [self.payAmount doubleValue];
    double cancelAmount = [self.cancelAmount doubleValue];
    double lossAmount = [self.lossAmount doubleValue];
    double cash = [self.cashPay doubleValue];
    double card = [self.cardPay doubleValue];
    double remit = [self.remitPay doubleValue];
    double ali = [self.aliPay doubleValue];
    double wechat = [self.wechatPay doubleValue];
    
    // 应收金额 = 商品总额 - 优惠 - 已支付 + 撤销支付 + 损耗扣款
    double collectMoney = product - coupon - payAmount + cancelAmount + lossAmount;
    // 客户确定想要支付金额
    double wantMoney = cash + card + remit + ali + wechat;
    // 余下待支付金额
    double remainMoney = collectMoney - wantMoney;
    if (remainMoney < 0.01) {
        // 直接支付完了，不需要用客户余额
        return 0;
        
    } else {
        if (remainMoney - self.balance > 0) {
            // 资金缺口比余额大，余额全部使用
            return self.balance;
            
        } else {
            // 资金缺口比余额小，从余额扣除
            return remainMoney;
        }
    }
}

- (NSString *)totalAmount
{
    double product = [self.productAmount doubleValue];
    double coupon = [self.couponAmount doubleValue];
    double payAmount = [self.payAmount doubleValue];
    double cancelAmount = [self.cancelAmount doubleValue];
    double lossAmount = [self.lossAmount doubleValue];
    // 实收款 = 商品金额 - 优惠 - 已支付 + 撤销支付 + 损耗扣款 - 客户余额
    double money = product - coupon - payAmount + cancelAmount + lossAmount - self.balanceDeduct;
    
    return NSStringFormat(@"%.2f", money);
}

- (void)setCouponAmount:(NSString *)couponAmount
{
    double product = [self.productAmount doubleValue];
    double coupon = [couponAmount doubleValue];
    double payAmount = [self.payAmount doubleValue];
    double cancelAmount = [self.cancelAmount doubleValue];
    double lossAmount = [self.lossAmount doubleValue];
    
    // 应收款 = 商品金额 - 已支付 + 撤销支付 + 损耗扣款
    double money = product - payAmount + cancelAmount + lossAmount;
    
    // 优惠金额不能大于应收款
    if (coupon > money) {
        _couponAmount = NSStringFormat(@"%.2f", money);
        
    } else {
        _couponAmount = couponAmount;
    }
}

- (void)setLossAmount:(NSString *)lossAmount
{
    double loss = [lossAmount doubleValue];
    double product = [self.productAmount doubleValue];
    double coupon = [self.couponAmount doubleValue];
    double payAmount = [self.payAmount doubleValue];
    double cancelAmount = [self.cancelAmount doubleValue];
    // 实收款 = 商品金额 - 优惠 - 已支付 + 撤销支付 - 客户余额
    double money = product - coupon - payAmount + cancelAmount - self.balanceDeduct;
    
    if (loss + money < 0) {
        _lossAmount = lossAmount;

    } else {
        _lossAmount = NSStringFormat(@"%.2f", -money);
    }
}

- (double)totalPayMoney
{
    double cash = [self.cashPay doubleValue];
    double card = [self.cardPay doubleValue];
    double remit = [self.remitPay doubleValue];
    double ali = [self.aliPay doubleValue];
    double wechat = [self.wechatPay doubleValue];
    
    return cash + card + remit + ali + wechat;
}

+ (NSString *)getProductAmountWithProductArray:(NSMutableArray<YDSearcherProductData *> *)productArray
{
    double total = 0;
    for (YDSearcherProductData *sub in productArray) {
        for (YDSearcherSkuGroupData *group in sub.skuGroupList) {
            for (YDSearcherSkuList *sku in group.skuList) {
                if (sku.selectedNum > 0) {
                    total += sub.salesPrice * sku.selectedNum;
                }
            }
        }
    }
    
    return NSStringFormat(@"%.2f", total);
}

@end

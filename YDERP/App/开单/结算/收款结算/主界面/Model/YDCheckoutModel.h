//
//  YDCheckoutModel.h
//  YDERP
//
//  Created by 何英健 on 2021/8/18.
//

#import "YDBaseModel.h"

typedef void(^BlockCheckOutModify)(void);

@interface YDCheckoutModel : NSObject

// 订单ID
@property (nonatomic, copy) NSString *orderID;
// 订单版本号(订单传入，用于处理不同人同时操作同一个订单)
@property (nonatomic, copy) NSString *version;
// 如果传入，代表商品有修改，最终调用结算接口也会变化
@property (nonatomic, strong) NSMutableArray<YDSearcherProductData *> *productArray;

#pragma mark - 客户信息

// 是否选择有客户信息
@property (nonatomic, assign) BOOL isCustomerInfoExist;
// 客户余额
@property (nonatomic, assign) double balance;
// 客户余额抵扣
@property (nonatomic, assign) double balanceDeduct;

#pragma mark - 金额合计信息

// 商品金额
@property (nonatomic, copy) NSString *productAmount;
// 已付金额
@property (nonatomic, copy) NSString *payAmount;
// 撤销金额
@property (nonatomic, copy) NSString *cancelAmount;
// 优惠金额
@property (nonatomic, copy) NSString *couponAmount;
// 损耗扣款
@property (nonatomic, copy) NSString *lossAmount;
// 合计待收金额（即应付金额）
@property (nonatomic, copy) NSString *totalAmount;

#pragma mark - 支付方式

// 现金
@property (nonatomic, copy) NSString *cashPay;
// 刷卡
@property (nonatomic, copy) NSString *cardPay;
// 汇款
@property (nonatomic, copy) NSString *remitPay;
// 支付宝
@property (nonatomic, copy) NSString *aliPay;
// 微信
@property (nonatomic, copy) NSString *wechatPay;

#pragma mark - 备注

// 备注信息
@property (nonatomic, copy) NSString *remark;
// 图片数组
@property (nonatomic, strong) NSMutableArray<NSString *> *photoArray;
// 开单时间
@property (nonatomic, copy) NSString *orderTime;

#pragma mark - 获取数据方法

// 支付总额 = 现金 + 刷卡 + 汇款 + 支付宝 + 微信
@property (nonatomic, assign) double totalPayMoney;

+ (NSString *)getProductAmountWithProductArray:(NSMutableArray<YDSearcherProductData *> *)productArray;

@end

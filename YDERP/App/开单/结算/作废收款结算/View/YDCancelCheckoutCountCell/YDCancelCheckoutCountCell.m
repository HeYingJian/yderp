//
//  YDCancelCheckoutCountCell.m
//  YDERP
//
//  Created by 何英健 on 2021/8/26.
//

#import "YDCancelCheckoutCountCell.h"

@interface YDCancelCheckoutCountCell () <UITextFieldDelegate>

// 商品总价
@property (nonatomic, weak) IBOutlet UITextField *textField1;
// 已收金额
@property (nonatomic, weak) IBOutlet UITextField *textField2;
// 撤销金额
@property (nonatomic, weak) IBOutlet UITextField *textField3;
// 优惠填写
@property (nonatomic, weak) IBOutlet UITextField *textField4;
// 合计待收
@property (nonatomic, weak) IBOutlet UITextField *textField5;

// 优惠金额
@property (nonatomic, weak) IBOutlet UILabel *couponLabel;
// 余额抵扣
@property (nonatomic, weak) IBOutlet UILabel *balanceLabel;

@property (nonatomic, weak) IBOutlet NSLayoutConstraint *couponLabelTop;
@property (nonatomic, weak) IBOutlet NSLayoutConstraint *couponLabelH;
@property (nonatomic, weak) IBOutlet NSLayoutConstraint *balanceLabelTop;
@property (nonatomic, weak) IBOutlet NSLayoutConstraint *balanceLabelH;

@end

@implementation YDCancelCheckoutCountCell

+ (instancetype)cellWithTableView:(UITableView *)tableView
{
    static NSString *reuseID = @"checkoutCountCell";
    YDCancelCheckoutCountCell *cell = [tableView dequeueReusableCellWithIdentifier:reuseID];
    if (!cell) {
        cell = [[NSBundle mainBundle] loadNibNamed:@"YDCancelCheckoutCountCell" owner:nil options:nil][0];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    
    return cell;
}

+ (CGFloat)getHeightWithModel:(YDCheckoutModel *)model
{
    // 显示优惠金额
    CGFloat couponH = 0;
    if (model.couponAmount.length) {
        couponH = 15 + 6;
    }
    
    // 显示客户余额抵扣
    CGFloat balanceH = 0;
    if (model.balanceDeduct > 0) {
        balanceH = 15 + 1;
    }
    
    return 326 + couponH + balanceH;
}

- (void)setModel:(YDCheckoutModel *)model
{
    _model = model;
    
    if (self.model.productAmount.length) {
        self.textField1.text = [NSString stringWithFormat:@"¥%@", self.model.productAmount];
        
    } else {
        self.textField1.text = @"¥0.00";
    }
    
    if (self.model.payAmount.length) {
        self.textField2.text = [NSString stringWithFormat:@"¥%@", self.model.payAmount];
        
    } else {
        self.textField2.text = @"¥0.00";
    }
    
    if (self.model.cancelAmount.length) {
        self.textField3.text = [NSString stringWithFormat:@"¥%@", self.model.cancelAmount];
        
    } else {
        self.textField3.text = @"¥0.00";
    }
    
    if (self.model.couponAmount.length) {
        self.textField4.text = [NSString stringWithFormat:@"¥%@", self.model.couponAmount];
        
    } else {
        self.textField4.text = nil;
    }
    
    if (self.model.totalAmount.length) {
        NSString *totalAmountStr = [NSString stringWithFormat:@"¥%@", self.model.totalAmount];
        totalAmountStr = [totalAmountStr stringByReplacingOccurrencesOfString:@"-" withString:@""];
        self.textField5.text = totalAmountStr;
        
    } else {
        self.textField5.text = @"¥0.00";
    }
    
    // 显示优惠金额
    if (self.model.couponAmount.length) {
        self.couponLabel.text = [NSString stringWithFormat:@"优惠：¥%@", self.model.couponAmount];
        self.couponLabelH.constant = 15;
        self.couponLabelTop.constant = 6;
        
    } else {
        self.couponLabelH.constant = 0;
        self.couponLabelTop.constant = 0;
    }
    
    // 显示客户余额抵扣
    if (self.model.balanceDeduct > 0) {
        self.balanceLabel.text = [NSString stringWithFormat:@"客户余额抵扣：%@", [NSString applePrefixPriceWith:self.model.balanceDeduct]];
        self.balanceLabelH.constant = 15;
        self.balanceLabelTop.constant = 1;
        
    } else {
        self.balanceLabelH.constant = 0;
        self.balanceLabelTop.constant = 0;
    }
}

- (void)awakeFromNib
{
    [super awakeFromNib];

    NSMutableAttributedString *str = [[NSMutableAttributedString alloc] initWithString:@"¥0.00" attributes:
         @{NSForegroundColorAttributeName:ColorFromRGB(0xB7B7B7),
           NSFontAttributeName:[UIFont systemFontOfSize:15]}
         ];
    self.textField4.attributedPlaceholder = str;
    
    [self.textField4 addToolSenderWithBlock:nil];
    
    self.textField1.font = FONT_NUMBER(15);
    self.textField2.font = FONT_NUMBER(15);
    self.textField3.font = FONT_NUMBER(15);
    self.textField4.font = FONT_NUMBER(15);
    self.textField5.font = FONT_NUMBER(21);
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

#pragma mark - UITextFieldDelegate

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    
    return YES;
}

- (void)setModelWithTextField:(UITextField *)textField
{
    CGFloat money = [textField.text doubleValue];
    NSString *moneyStr;
    if (money >= 1000000) {
        moneyStr = @"999999.99";
        
    } else {
        moneyStr = NSStringFormat(@"%.2f", money);
    }
    
    if (textField == self.textField4) {
        self.model.couponAmount = moneyStr;
    }
    
    if (self.modifyBlock) {
        self.modifyBlock();
    }
}

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    if (self.isShowKeybord) {
        [self.window endEditing:YES];
        return NO;
        
    } else {
        if (self.textFieldChangeBlock) {
            self.isShowKeybord = YES;
            self.textFieldChangeBlock(YES);
        }
    }
    
    textField.text = nil;
    return YES;
}

- (BOOL)textFieldShouldEndEditing:(UITextField *)textField
{
    if (self.textFieldChangeBlock) {
        self.isShowKeybord = NO;
        self.textFieldChangeBlock(NO);
    }
    
    // 没有输入任何东西，则不修改
    if (!textField.text.length) {
        if (self.modifyBlock) {
            self.modifyBlock();
        }
        
        return YES;
    }
    
    CGFloat money = [textField.text doubleValue];
    if (money >= 1000000) {
        [MBProgressHUD showToastMessage:@"金额必须小于100万"];
        
    } else if (money < 0) {
        textField.text = [textField.text stringByReplacingOccurrencesOfString:@"-" withString:@""];
    }
    
    [self setModelWithTextField:textField];
    
    return YES;
}

@end

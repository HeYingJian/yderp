//
//  YDCancelCheckoutVC.h
//  YDERP
//
//  Created by 何英健 on 2021/8/18.
//

#import <UIKit/UIKit.h>
#import "YDCheckoutModel.h"

@interface YDCancelCheckoutVC : UIViewController

// 结算模型
@property (nonatomic, strong) YDCheckoutModel *model;
// 作废的收款记录id
@property (nonatomic, copy) NSString *payID;

@end

//
//  YDMakeListVC.m
//  YDERP
//
//  Created by 何英健 on 2021/7/19.
//

#import "YDMakeListVC.h"
#import "YDMakeListUserCell.h"
#import "YDMakeListProductCell.h"
#import "YDSelectCustomerVC.h"
#import "YDSelectAddressVC.h"
#import "YDSearcherCustomerModel.h"
#import "YDSelectProductVC.h"
#import "YDSelectSKUPopView.h"
#import "YDCheckoutVC.h"

@interface YDMakeListVC () <UITableViewDelegate, UITableViewDataSource>

@property (nonatomic, weak) IBOutlet UITableView *tableView;

@property (nonatomic, weak) IBOutlet UILabel *totalPriceLabel;
@property (nonatomic, weak) IBOutlet UILabel *pieceLabel;
@property (nonatomic, weak) IBOutlet UIButton *accountBtn;
@property (nonatomic, weak) IBOutlet UIButton *restingBtn;

// 保存目标订单id
@property (nonatomic, copy) NSString *orderID;

#pragma mark - 保存选择

// 选择客户
@property (nonatomic, strong) YDSearcherCustomerData *selectedCustomer;
// 选择地址
@property (nonatomic, strong) YDCustomerAddressData *selectedAddress;
// 选择商品
@property (nonatomic, strong) NSMutableArray<YDSearcherProductData *> *selectedProductArray;

@end

@implementation YDMakeListVC

#pragma mark - 懒加载

- (NSMutableArray<YDSearcherProductData *> *)selectedProductArray
{
    if (!_selectedProductArray) {
        _selectedProductArray = [NSMutableArray array];
    }
    
    return _selectedProductArray;
}

#pragma mark - 页面加载

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self initUI];
    
    [self addNotification];
}

- (void)initUI
{
    self.totalPriceLabel.font = FONT_NUMBER(20);
    self.totalPriceLabel.adjustsFontSizeToFitWidth = YES;
    self.restingBtn.layer.borderColor = ColorFromRGB(0xE2E2E2).CGColor;
    self.restingBtn.layer.borderWidth = 1.5;
    
    [self setupTableView];
    [self refreshCountView];
}

- (void)setupTableView
{
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    self.tableView.showsVerticalScrollIndicator = NO;
    self.tableView.showsHorizontalScrollIndicator = NO;
    
    if (@available(iOS 11.0, *)) {
        self.tableView.contentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentNever;
        
    } else {
        self.automaticallyAdjustsScrollViewInsets = NO;
    }
}

- (void)addNotification
{
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(clearData) name:MAKE_LIST_CLEAR_DATA object:nil];
}

- (void)clearData
{
    self.data = nil;
    self.orderID = nil;
    self.selectedCustomer = nil;
    self.selectedAddress = nil;
    self.selectedProductArray = nil;
    [self.tableView reloadData];
    [self refreshCountView];
}

// 刷新底部统计
- (void)refreshCountView
{
    CGFloat totalPrice = 0;
    NSInteger totalPiece = 0;
    for (YDSearcherProductData *sub in self.selectedProductArray) {
        for (YDSearcherSkuGroupData *groupData in sub.skuGroupList) {
            for (YDSearcherSkuList *sku in groupData.skuList) {
                totalPrice += sku.selectedNum * sub.salesPrice;
                totalPiece += sku.selectedNum;
            }
        }
    }
    self.totalPriceLabel.text = [NSString applePrefixPriceWith:totalPrice];
    self.pieceLabel.text = [NSString stringWithFormat:@"共选中%ld件", (long)totalPiece];
    
    if (self.selectedProductArray.count) {
        self.restingBtn.userInteractionEnabled = YES;
        [self.restingBtn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        
        self.accountBtn.userInteractionEnabled = YES;
        [self.accountBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        
    } else {
        self.restingBtn.userInteractionEnabled = NO;
        [self.restingBtn setTitleColor:ColorFromRGB(0xCCCCCC) forState:UIControlStateNormal];
        
        self.accountBtn.userInteractionEnabled = NO;
        [self.accountBtn setTitleColor:ColorFromRGB(0xFFAA97) forState:UIControlStateNormal];
    }
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [self.navigationController setNavigationBarHidden:YES animated:YES];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    
    [self.navigationController setNavigationBarHidden:NO animated:NO];
}

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

#pragma mark - 数据预设

- (void)setData:(YDOrderDetailData *)data
{
    _data = data;
    
    self.orderID = [NSString stringWithFormat:@"%ld", (long)data.order.ID];
    
    [self setSelectedCustomerWithData];
    [self setSelectedAddressWithData];
    [self setSelectedProductWithData];
}

// 预设客户信息
- (void)setSelectedCustomerWithData
{
    if (self.data.client) {
        YDSearcherCustomerData *customerData = [[YDSearcherCustomerData alloc] init];
        customerData.ID = self.data.client.ID;
        customerData.balance = self.data.client.balance;
        customerData.name = self.data.client.name;
        customerData.phone = self.data.client.phone;
        self.selectedCustomer = customerData;
    }
}

// 预设地址信息
- (void)setSelectedAddressWithData
{
    if (self.data.order.clientAddress.length) {
        YDCustomerAddressData *addressData = [[YDCustomerAddressData alloc] init];
        addressData.address = self.data.order.clientAddress;
        self.selectedAddress = addressData;
    }
}

// 预设商品信息
- (void)setSelectedProductWithData
{
    for (YDOrderDetailDataItems *sub in self.data.orderItems) {
        YDSearcherProductData *product = [[YDSearcherProductData alloc] init];
        product.ID = sub.itemId;
        product.code = sub.code;
        product.name = sub.itemName;
        product.salesPrice = sub.salesPrice;
        product.itemSalesPrice = sub.itemSalesPrice;
        product.wholePrice = sub.wholePrice;
        product.packagePrice = sub.packagePrice;
        YDSearcherPicList *picData = [[YDSearcherPicList alloc] init];
        picData.url = sub.itemUrl;
        product.picList = [NSMutableArray arrayWithObject:picData];
        // 从skuList中获取颜色和尺码
        product.skuGroupList = [YDFunction getSkuGroupArrayFromOrderSkuArray:sub.orderItemSkus];
        [self.selectedProductArray addObject:product];
    }
}

#pragma mark - 选择页面

// 选择客户
- (void)showSelectCustomer
{
    YDSelectCustomerVC *vc = [[YDSelectCustomerVC alloc] init];
    YDWeakSelf
    vc.selectSucceed = ^(YDSearcherCustomerData *customer) {
        weakSelf.selectedCustomer = customer;
        weakSelf.selectedAddress = nil;
        [weakSelf.tableView reloadData];
        
        // 填充默认地址
        [YDFunction getCustomerAddressListWithCustomerID:customer.ID completion:^(NSMutableArray<YDCustomerAddressData *> *addressArray) {
            for (YDCustomerAddressData *sub in addressArray) {
                if (sub.defaultFlag) {
                    weakSelf.selectedAddress = sub;
                    break;
                }
            }
            
            [weakSelf.tableView reloadData];
        }];
    };
    [self.navigationController pushViewController:vc animated:YES];
}

// 选择客户地址
- (void)showSelectAddress
{
    if (!self.selectedCustomer) {
        [MBProgressHUD showToastMessage:@"请先选择客户"];
        return;
    }
    
    YDWeakSelf
    YDSelectAddressVC *vc = [[YDSelectAddressVC alloc] init];
    vc.customerID = self.selectedCustomer.ID;
    vc.selectSucceed = ^(YDCustomerAddressData *address) {
        weakSelf.selectedAddress = address;
        [weakSelf.tableView reloadData];
    };
    [self.navigationController pushViewController:vc animated:YES];
}

// 选择商品
- (void)showSelectPorudct
{
    YDSelectProductVC *vc = [[YDSelectProductVC alloc] init];
    vc.originSelectedArray = self.selectedProductArray;
    YDWeakSelf
    vc.selectedProductBlock = ^(NSMutableArray<YDSearcherProductData *> *array) {
        [weakSelf addProductToSelectedArray:array];
        
        [weakSelf refreshCountView];
        [weakSelf.tableView reloadData];
    };
    [self.navigationController pushViewController:vc animated:YES];
}

// 把新选择的商品添加到已选择数组
- (void)addProductToSelectedArray:(NSMutableArray<YDSearcherProductData *> *)productArray
{
    for (YDSearcherProductData *sub in productArray) {
        // 商品是否已选择
        BOOL isProductExist = NO;
        
        for (YDSearcherProductData *selectedProduct in self.selectedProductArray) {
            if (sub.ID == selectedProduct.ID) {
                isProductExist = YES;
                selectedProduct.salesPrice = sub.salesPrice;
                
                for (YDSearcherSkuGroupData *groupData in sub.skuGroupList) {
                    NSString *colorId = groupData.colorId;
                    // 颜色是否已选择
                    BOOL isColorExist = NO;
                    
                    for (YDSearcherSkuGroupData *selectedGroupData in selectedProduct.skuGroupList) {
                        if ([colorId isEqualToString:selectedGroupData.colorId]) {
                            isColorExist = YES;
                            
                            for (YDSearcherSkuList *sku in groupData.skuList) {
                                // 尺码是否已选择
                                BOOL isSizeExist = NO;
                                
                                for (YDSearcherSkuList *selectedSku in selectedGroupData.skuList) {
                                    if ([sku.ID isEqualToString:selectedSku.ID]) {
                                        isSizeExist = YES;
                                        selectedSku.selectedNum += sku.selectedNum;
                                        selectedSku.salesNum -= sku.selectedNum;
                                    }
                                }
                                
                                if (!isSizeExist) {
                                    // 没有加入过这个尺码
                                    [selectedGroupData.skuList addObject:sku];
                                }
                            }
                        }
                    }
                    
                    if (!isColorExist) {
                        // 没有加入过这个颜色
                        [selectedProduct.skuGroupList addObject:groupData];
                    }
                }
            }
        }
        
        if (!isProductExist) {
            // 没有添加过这个商品，则直接加入
            [self.selectedProductArray addObject:sub];
        }
    }
}

#pragma mark - 点击事件

- (IBAction)tapCloseBtn:(id)sender
{
    if (!self.data && (self.selectedCustomer || self.selectedAddress || self.selectedProductArray.count)) {
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"关闭后订单信息会消失哦！" message:nil preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *confirm = [UIAlertAction actionWithTitle:@"确定" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            [self dismissViewControllerAnimated:YES completion:nil];
        }];
        UIAlertAction* cancel = [UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleCancel handler:nil];

        [alert addAction:cancel];
        [alert addAction:confirm];

        [self presentViewController:alert animated:YES completion:nil];
        
    } else {
        [self dismissViewControllerAnimated:YES completion:nil];
    }
}

// 点击挂单
- (IBAction)tapRestingBtn:(id)sender
{
    if (![self checkFillData]) {
        return;
    }
    
    [self submitOrderWithType:0];
}

// 点击结算
- (IBAction)tapAccountBtn:(id)sender
{
    if (![self checkFillData]) {
        return;
    }
    
    if (!self.selectedCustomer) {
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"您还没有选择客户哦！\n（继续结算将会默认为临时客户）" message:nil preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *confirm = [UIAlertAction actionWithTitle:@"继续结算" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            [self submitOrderWithType:1];
        }];
        UIAlertAction* cancel = [UIAlertAction actionWithTitle:@"去选择客户" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
            [self showSelectCustomer];
        }];

        [alert addAction:cancel];
        [alert addAction:confirm];

        [self presentViewController:alert animated:YES completion:nil];
        
    } else {
        [self submitOrderWithType:1];
    }
}

// 提交订单（挂单与结算都是提交订单）type:0-挂单 1-结算
- (void)submitOrderWithType:(NSInteger)type
{
    [YDLoadingView showToSuperview:self.view];
    
    NSString *customerID = nil;
    if (self.selectedCustomer) {
        customerID = [NSString stringWithFormat:@"%ld", (long)self.selectedCustomer.ID];
    }
    
    YDWeakSelf
    [YDFunction submitOrderWithOrderID:self.orderID customerID:customerID address:self.selectedAddress.address productArray:self.selectedProductArray completion:^(YDSubmitOrderData *orderData) {
        [YDLoadingView hideToSuperview:weakSelf.view];
        
        // 通知挂单列表发生了变化，需要刷新
        PostNotification(REFRESH_RESTING_ORDER, nil);
        
        if (type == 0) {
            if (weakSelf.data) {
                // 挂单修改
                [MBProgressHUD showToastMessage:@"保存成功"];
                [weakSelf dismissViewControllerAnimated:YES completion:nil];
                [[UINavigationController rootNavigationController] popToRootViewControllerAnimated:NO];
                
            } else {
                // 新建挂单
                [MBProgressHUD showToastMessage:@"挂单成功"];
                [weakSelf dismissViewControllerAnimated:YES completion:nil];
            }
            weakSelf.tabBarController.selectedIndex = 0;
            
        } else {
            // 保存订单id
            weakSelf.orderID = orderData.orderId;
            
            // 跳转checkout页面
            YDCheckoutVC *vc = [[YDCheckoutVC alloc] init];
            vc.type = 0;
            vc.model.orderID = orderData.orderId;
            vc.model.orderTime = orderData.orderTime;
            vc.model.version = orderData.version;
            if (weakSelf.selectedCustomer) {
                vc.model.isCustomerInfoExist = YES;
                vc.model.balance = self.selectedCustomer.balance;
            }
            vc.model.productAmount = [YDCheckoutModel getProductAmountWithProductArray:self.selectedProductArray];
            [weakSelf.navigationController pushViewController:vc animated:YES];
        }
        
    } refreshBlock:^(NSString *msg) {
        [YDLoadingView hideToSuperview:weakSelf.view];
        [YDNoticePopView showWithTitle:msg completion:^{
            if (weakSelf.refreshBlock) {
                weakSelf.refreshBlock();
            }
            [weakSelf dismissViewControllerAnimated:YES completion:nil];
        }];
        
    } faildBlock:^(NSString *msg) {
        [YDLoadingView hideToSuperview:weakSelf.view];
        [MBProgressHUD showToastMessage:msg];
    }];
}

// 检查填写的数据是否完整
- (BOOL)checkFillData
{
    if (!self.selectedProductArray.count) {
        [MBProgressHUD showToastMessage:@"请先添加商品数据"];
        return NO;
    }
    
    return YES;
}

#pragma mark - UITableView代理

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 2;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    switch (section) {
        case 0:
            return 1;
            
        case 1:
            return self.selectedProductArray.count;
            
        default:
            break;
    }
    
    return 0;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == 0) {
        return [YDMakeListUserCell getHeight];
        
    } else {
        YDSearcherProductData *productData = self.selectedProductArray[indexPath.row];
        return [YDMakeListProductCell getHeightWithData:productData];
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    YDWeakSelf
    if (indexPath.section == 0) {
        YDMakeListUserCell *cell = [YDMakeListUserCell cellWithTableView:tableView];
        cell.selectedCustomer = self.selectedCustomer;
        cell.selectedAddress = self.selectedAddress;
        cell.showSelectUserBlock = ^{
            // 选择客户
            [weakSelf showSelectCustomer];
        };
        cell.showSelectAddressBlock = ^{
            // 选择地址
            [weakSelf showSelectAddress];
        };
        cell.showSelectProductBlock = ^{
            // 选择商品
            [weakSelf showSelectPorudct];
        };
        cell.clearUserBlock = ^{
            // 清空选择
            weakSelf.selectedCustomer = nil;
            weakSelf.selectedAddress = nil;
            [weakSelf.tableView reloadData];
        };
        
        return cell;
        
    } else {
        YDMakeListProductCell *cell = [YDMakeListProductCell cellWithTableView:tableView];
        YDSearcherProductData *productData = self.selectedProductArray[indexPath.row];
        cell.data = productData;
        YDWeakSelf
        cell.deleteBlock = ^(YDSearcherProductData *data) {
            [weakSelf showDeleteProduct:data];
        };
        cell.changeDetailBlock = ^{
            [weakSelf.tableView reloadData];
        };
        
        return cell;
    }
}

- (void)showDeleteProduct:(YDSearcherProductData *)data
{
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"删除商品，将清掉商品的SKU数量" message:nil preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *confirm = [UIAlertAction actionWithTitle:@"确定" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        [self.selectedProductArray removeObject:data];
        [self.tableView reloadData];
        [self refreshCountView];
    }];
    UIAlertAction* cancel = [UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleCancel handler:nil];

    [alert addAction:cancel];
    [alert addAction:confirm];

    [self presentViewController:alert animated:YES completion:nil];
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (self.selectedProductArray.count <= indexPath.row) {
        return;
    }
    
    YDSearcherProductData *productData = self.selectedProductArray[indexPath.row];

    YDWeakSelf
    // 弹出sku选择框
    [YDSelectSKUPopView showWithType:YDSelectSKUTypeMakeList data:productData orderID:self.orderID version:nil completion:^(BOOL isSelected) {
        if (!isSelected) {
            // 从选择数组中删除
            [weakSelf.selectedProductArray removeObject:productData];
        }
        
        [weakSelf.tableView reloadData];
        [weakSelf refreshCountView];
    }];
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 0.01;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    return [[UIView alloc] init];
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    return 0.01;
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section
{
    return [[UIView alloc] init];
}

@end

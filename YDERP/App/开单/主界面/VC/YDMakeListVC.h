//
//  YDMakeListVC.h
//  YDERP
//
//  Created by 何英健 on 2021/7/19.
//

#import <UIKit/UIKit.h>
#import "YDOrderDetailModel.h"

@interface YDMakeListVC : UIViewController

// 有数据代表订单已创建
@property (nonatomic, strong) YDOrderDetailData *data;
// 数据有误，回退并刷新上一个页面数据
@property (nonatomic, copy) BlockLoadDataDone refreshBlock;

@end

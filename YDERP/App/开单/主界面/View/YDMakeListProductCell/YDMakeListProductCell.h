//
//  YDMakeListProductCell.h
//  YDERP
//
//  Created by 何英健 on 2021/8/15.
//

#import <UIKit/UIKit.h>

typedef void(^BlockMakeListProductDelete)(YDSearcherProductData *data);
typedef void(^BlockMakeListProductChangeDetail)(void);

@interface YDMakeListProductCell : UITableViewCell

+ (instancetype)cellWithTableView:(UITableView *)tableView;

+ (CGFloat)getHeightWithData:(YDSearcherProductData *)data;

@property (nonatomic, strong) YDSearcherProductData *data;

@property (nonatomic, copy) BlockMakeListProductDelete deleteBlock;

@property (nonatomic, copy) BlockMakeListProductChangeDetail changeDetailBlock;

@end

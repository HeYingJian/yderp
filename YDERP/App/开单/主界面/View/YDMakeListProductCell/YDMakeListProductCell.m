//
//  YDMakeListProductCell.m
//  YDERP
//
//  Created by 何英健 on 2021/8/15.
//

#import "YDMakeListProductCell.h"

#define ROW_HEIGHT 25

@interface YDMakeListProductCell ()

@property (nonatomic, weak) IBOutlet UIImageView *productImgView;
@property (nonatomic, weak) IBOutlet UILabel *productNameLabel;
@property (nonatomic, weak) IBOutlet UILabel *priceLabel;
@property (nonatomic, weak) IBOutlet UILabel *quantityLabel;

@property (nonatomic, weak) IBOutlet UIView *detailView;
@property (nonatomic, weak) IBOutlet UIButton *detailBtn;
@property (nonatomic, weak) IBOutlet UIView *colorBGView;
@property (nonatomic, weak) IBOutlet UIView *sizeBGView;
@property (nonatomic, weak) IBOutlet UIView *quantityBGView;

@property (nonatomic, weak) IBOutlet NSLayoutConstraint *lineViewLeading1;
@property (nonatomic, weak) IBOutlet NSLayoutConstraint *lineViewLeading2;
@property (nonatomic, weak) IBOutlet NSLayoutConstraint *detailViewBottom;

@end

@implementation YDMakeListProductCell

+ (instancetype)cellWithTableView:(UITableView *)tableView
{
    static NSString *reuseID = @"makeListProductCell";
    YDMakeListProductCell *cell = [tableView dequeueReusableCellWithIdentifier:reuseID];
    if (!cell) {
        cell = [[NSBundle mainBundle] loadNibNamed:@"YDMakeListProductCell" owner:nil options:nil][0];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    
    return cell;
}

+ (CGFloat)getHeightWithData:(YDSearcherProductData *)data
{
    CGFloat detailH = 0;
    if (data.isShowDetail) {
        detailH = [YDMakeListProductCell getDetailViewHeightWithData:data];
    }

    return 127 + detailH + 10;
}

// 计算表格高度
+ (CGFloat)getDetailViewHeightWithData:(YDSearcherProductData *)data
{
    NSInteger rowNum = 0;
    NSMutableArray *skuGroupArray = [YDMakeListProductCell getSkuGroupListWithoutUnselected:data];
    for (YDSearcherSkuGroupData *sub in skuGroupArray) {
        rowNum += sub.skuList.count;
    }
    return 40 + rowNum * ROW_HEIGHT + 15;
}

// 获取已选数量不为0的sku
+ (NSMutableArray<YDSearcherSkuGroupData *> *)getSkuGroupListWithoutUnselected:(YDSearcherProductData *)data
{
    NSMutableArray<YDSearcherSkuGroupData *> *resultArray = [NSMutableArray array];
    for (YDSearcherSkuGroupData *sub in data.skuGroupList) {
        BOOL exist = NO;
        YDSearcherSkuGroupData *groupData = [[YDSearcherSkuGroupData alloc] init];
        groupData.color = sub.color;
        
        for (YDSearcherSkuList *sku in sub.skuList) {
            if (sku.selectedNum > 0) {
                if (!exist) {
                    groupData.skuList = [NSMutableArray array];
                    [resultArray addObject:groupData];
                    exist = YES;
                }
                
                [groupData.skuList addObject:sku];
            }
        }
    }
    
    return resultArray;
}

- (void)setData:(YDSearcherProductData *)data
{
    _data = data;
    
    if (data.isShowDetail) {
        self.detailViewBottom.constant = 15;
        self.detailBtn.selected = YES;
        
        [self refreshDetailView];
        
    } else {
        self.detailViewBottom.constant = 0;
        self.detailBtn.selected = NO;
    }
    [self layoutIfNeeded];
    
    if (data.picList.count && data.picList[0].url.length) {
        [self.productImgView appleSetImageWithUrl:data.picList[0].url SprcifyOssImageStyle:@"products_list" HasHD:NO placeholderImage:@"默认商品方形图" completion:nil];
        
    } else {
        [self.productImgView setImage:[UIImage imageNamed:@"没有商品默认图"]];
    }
    
    NSString *codeStr = data.code;
    NSString *productName = data.name;
    NSString *str = [NSString stringWithFormat:@"%@#%@", codeStr, productName];
    NSMutableAttributedString *attributedStr = [[NSMutableAttributedString alloc] initWithString:str];
    [attributedStr addAttribute:NSFontAttributeName value:FONT_NUMBER(15) range:NSMakeRange(0, codeStr.length + 1)];
    [attributedStr addAttribute:NSForegroundColorAttributeName value:ColorFromRGB(0xFF5630) range:NSMakeRange(0, codeStr.length + 1)];
    [attributedStr addAttribute:NSFontAttributeName value:[UIFont systemFontOfSize:14.0f] range:NSMakeRange(codeStr.length + 1, productName.length)];
    [attributedStr addAttribute:NSForegroundColorAttributeName value:ColorFromRGB(0x212121) range:NSMakeRange(codeStr.length + 1, productName.length)];
    NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc] init];
    [paragraphStyle setLineSpacing:5];
    [attributedStr addAttribute:NSParagraphStyleAttributeName value:paragraphStyle range:NSMakeRange(0, [str length])];
    self.productNameLabel.attributedText = attributedStr;
    self.productNameLabel.lineBreakMode = NSLineBreakByTruncatingTail;
    
    CGFloat totalPrice = 0;
    NSInteger totalQuantity = 0;
    for (YDSearcherSkuGroupData *sub in self.data.skuGroupList) {
        for (YDSearcherSkuList *sku in sub.skuList) {
            totalPrice += self.data.salesPrice * sku.selectedNum;
            totalQuantity += sku.selectedNum;
        }
    }
    
    self.priceLabel.text = [NSString applePrefixPriceWith:totalPrice];
    self.quantityLabel.text = [NSString stringWithFormat:@"%ld件*%@元", (long)totalQuantity, [NSString appleStandardPriceWith:data.salesPrice]];
}

// 画表格
- (void)refreshDetailView
{
    [self.colorBGView removeAllSubviews];
    [self.sizeBGView removeAllSubviews];
    [self.quantityBGView removeAllSubviews];
    
    NSMutableArray *skuGroupArray = [YDMakeListProductCell getSkuGroupListWithoutUnselected:self.data];
    
    CGFloat colorOriginY = 0;
    CGFloat sizeOriginY = 0;
    CGFloat colorLabelW = [YDMakeListProductCell getColorLabelWidth];
    CGFloat sizeLabelW = [YDMakeListProductCell getSizeLabelWidth];
    
    for (int i = 0; i < skuGroupArray.count; i++) {
        YDSearcherSkuGroupData *sub = skuGroupArray[i];
        
        UILabel *colorLabel = [YDMakeListProductCell createLabel];
        [colorLabel setY:colorOriginY];
        [colorLabel setWidth:colorLabelW];
        [colorLabel setHeight:ROW_HEIGHT * sub.skuList.count];
        colorLabel.text = sub.color;
        colorLabel.font = [UIFont systemFontOfSize:11 weight:UIFontWeightSemibold];
        [self.colorBGView addSubview:colorLabel];
        
        if (i < skuGroupArray.count - 1) {
            colorOriginY = CGRectGetMaxY(colorLabel.frame);
            
            UIView *lineView = [YDMakeListProductCell createLineViewWithLabel:colorLabel];
            [self.colorBGView addSubview:lineView];
        }
        
        for (int m = 0; m < sub.skuList.count; m++) {
            YDSearcherSkuList *sku = sub.skuList[m];
            
            UILabel *sizeLabel = [YDMakeListProductCell createLabel];
            [sizeLabel setY:sizeOriginY];
            [sizeLabel setWidth:sizeLabelW];
            [sizeLabel setHeight:ROW_HEIGHT];
            sizeLabel.text = sku.size;
            sizeLabel.font = [UIFont systemFontOfSize:11 weight:UIFontWeightSemibold];
            [self.sizeBGView addSubview:sizeLabel];
            
            UILabel *quantityLabel = [YDMakeListProductCell createLabel];
            [quantityLabel setY:sizeOriginY];
            [quantityLabel setWidth:sizeLabelW];
            [quantityLabel setHeight:ROW_HEIGHT];
            quantityLabel.text = [NSString stringWithFormat:@"%ld", (long)sku.selectedNum];
            quantityLabel.font = FONT_NUMBER(13);
            [self.quantityBGView addSubview:quantityLabel];
            
            if (i < skuGroupArray.count - 1 || m < sub.skuList.count - 1) {
                sizeOriginY = CGRectGetMaxY(sizeLabel.frame);
                
                UIView *sizeLineView = [YDMakeListProductCell createLineViewWithLabel:sizeLabel];
                [self.sizeBGView addSubview:sizeLineView];
                
                UIView *quantityLineView = [YDMakeListProductCell createLineViewWithLabel:quantityLabel];
                [self.quantityBGView addSubview:quantityLineView];
            }
        }
    }
}

// 创建标题
+ (UILabel *)createLabel
{
    UILabel *label = [[UILabel alloc] init];
    label.textColor = ColorFromRGB(0x222222);
    label.textAlignment = NSTextAlignmentCenter;

    return label;
}

// 创建分割线
+ (UIView *)createLineViewWithLabel:(UILabel *)label
{
    UIView *lineView = [[UIView alloc] initWithFrame:CGRectMake(0, CGRectGetMaxY(label.frame), label.width, 0.5)];
    lineView.backgroundColor = ColorFromRGB(0xE9E9E9);
    
    return lineView;
}

- (IBAction)tapDeleteBtn:(id)sender
{
    if (self.deleteBlock) {
        self.deleteBlock(self.data);
    }
}

- (IBAction)tapDetailBtn:(id)sender
{
    self.data.isShowDetail = !self.data.isShowDetail;
    
    if (self.changeDetailBlock) {
        self.changeDetailBlock();
    }
}

+ (CGFloat)getColorLabelWidth
{
    return (SCREEN_WIDTH - 23 * 2) / 3 + 20;
}

+ (CGFloat)getSizeLabelWidth
{
    return (SCREEN_WIDTH - 23 * 2) / 3 - 10;
}

- (void)awakeFromNib
{
    [super awakeFromNib];
    
    self.lineViewLeading1.constant = [YDMakeListProductCell getColorLabelWidth];
    self.lineViewLeading2.constant = [YDMakeListProductCell getSizeLabelWidth];
    
    self.priceLabel.font = FONT_NUMBER(17);
    self.detailView.layer.borderColor = ColorFromRGB(0xEBECF0).CGColor;
    self.detailView.layer.borderWidth = 0.5;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end

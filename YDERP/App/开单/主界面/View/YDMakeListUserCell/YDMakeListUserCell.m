//
//  YDMakeListUserCell.m
//  YDERP
//
//  Created by 何英健 on 2021/8/12.
//

#import "YDMakeListUserCell.h"

@interface YDMakeListUserCell ()

@property (nonatomic, weak) IBOutlet UIView *userView;
@property (nonatomic, weak) IBOutlet UIView *addressView;

@property (nonatomic, weak) IBOutlet UIView *selectedUserView;
@property (nonatomic, weak) IBOutlet UIView *unselectedUserView;

@property (nonatomic, weak) IBOutlet UILabel *userLabel;
@property (nonatomic, weak) IBOutlet UILabel *priceTitleLabel;
@property (nonatomic, weak) IBOutlet UILabel *priceLabel;
@property (nonatomic, weak) IBOutlet UILabel *priceUnitLabel;
@property (nonatomic, weak) IBOutlet UILabel *addressLabel;
@property (nonatomic, weak) IBOutlet UILabel *selectNoticeLabel;
@property (nonatomic, weak) IBOutlet UILabel *unselectedUserLabel;

@property (nonatomic, weak) IBOutlet UIButton *clearBtn;
@property (nonatomic, weak) IBOutlet UIImageView *customerArrowImgView;
@property (nonatomic, weak) IBOutlet UIImageView *addressArrowImgView;

@end

@implementation YDMakeListUserCell

+ (instancetype)cellWithTableView:(UITableView *)tableView
{
    static NSString *reuseID = @"makeListUserCell";
    YDMakeListUserCell *cell = [tableView dequeueReusableCellWithIdentifier:reuseID];
    if (!cell) {
        cell = [[NSBundle mainBundle] loadNibNamed:@"YDMakeListUserCell" owner:nil options:nil][0];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        
        UITapGestureRecognizer *tapUserView = [[UITapGestureRecognizer alloc] initWithTarget:cell action:@selector(tapUserView)];
        [cell.userView addGestureRecognizer:tapUserView];
        
        UITapGestureRecognizer *tapAddressView = [[UITapGestureRecognizer alloc] initWithTarget:cell action:@selector(tapAddressView)];
        [cell.addressView addGestureRecognizer:tapAddressView];
    }
    
    return cell;
}

+ (CGFloat)getHeight
{
    return 240;
}

- (void)setSelectedCustomer:(YDSearcherCustomerData *)selectedCustomer
{
    _selectedCustomer = selectedCustomer;
    
    if (selectedCustomer) {
        self.selectedUserView.hidden = NO;
        self.unselectedUserView.hidden = YES;
        
        NSString *userNameStr = selectedCustomer.name;
        if (userNameStr.length > 5) {
            userNameStr = [NSString stringWithFormat:@"%@…", [userNameStr substringToIndex:5]];
        }
        NSString *userStr = [NSString stringWithFormat:@"%@  (%@)", userNameStr, selectedCustomer.phone];
        NSMutableAttributedString *attributedStr = [[NSMutableAttributedString alloc] initWithString:userStr];
        [attributedStr addAttribute:NSFontAttributeName value:FONT_NUMBER(15) range:NSMakeRange(userNameStr.length + 2, selectedCustomer.phone.length + 2)];
        self.userLabel.attributedText = attributedStr;
        
        if (selectedCustomer.balance > 0.001) {
            // 余额大于0
            self.priceTitleLabel.text = @"客户余额：";
            self.priceLabel.textColor = ColorFromRGB(0xFF5630);
            self.priceUnitLabel.textColor = ColorFromRGB(0xA2A2A2);
            self.priceLabel.text = NSStringFormat(@"%.2f", selectedCustomer.balance);
            
        } else if (selectedCustomer.balance > - 0.001) {
            // 余额为0
            self.priceTitleLabel.text = @"客户余额：";
            self.priceLabel.textColor = ColorFromRGB(0x777777);
            self.priceUnitLabel.textColor = ColorFromRGB(0xA2A2A2);
            self.priceLabel.text = NSStringFormat(@"%.2f", 0.0);
            
        } else {
            //  有欠款
            self.priceTitleLabel.text = @"客户欠款：";
            self.priceLabel.textColor = ColorFromRGB(0x2EC28B);
            self.priceUnitLabel.textColor = ColorFromRGB(0xA2A2A2);
            self.priceLabel.text = NSStringFormat(@"%.2f", -selectedCustomer.balance);
        }
        
    } else {
        self.selectedUserView.hidden = YES;
        self.unselectedUserView.hidden = NO;
        
        NSString *str;
        if (self.hideEdit) {
            str = @"临时客户";
            
        } else {
            str = @"客户名字";
        }
        self.unselectedUserLabel.text = str;
    }
}

- (void)setSelectedAddress:(YDCustomerAddressData *)selectedAddress
{
    _selectedAddress = selectedAddress;
    
    if (selectedAddress) {
        self.addressLabel.text = selectedAddress.address;
        
    } else {
        NSString *str;
        if (self.hideEdit) {
            str = @"无收货地址";
            
        } else {
            str = @"选择收货地址";
        }
        self.addressLabel.text = str;
    }
}

- (void)setHideEdit:(BOOL)hideEdit
{
    _hideEdit = hideEdit;
    
    if (hideEdit) {
        self.selectNoticeLabel.hidden = YES;
        self.clearBtn.hidden = YES;
        self.customerArrowImgView.hidden = YES;
        self.addressArrowImgView.hidden = YES;
    }
}

- (IBAction)tapClearBtn:(id)sender
{
    if (self.clearUserBlock) {
        self.clearUserBlock();
    }
}

- (void)tapUserView
{
    if (self.showSelectUserBlock) {
        self.showSelectUserBlock();
    }
}

- (void)tapAddressView
{
    if (self.showSelectAddressBlock) {
        self.showSelectAddressBlock();
    }
}

- (IBAction)tapAddProductBtn:(id)sender
{
    if (self.showSelectProductBlock) {
        self.showSelectProductBlock();
    }
}

- (void)awakeFromNib
{
    [super awakeFromNib];
   
    self.priceLabel.font = FONT_NUMBER(12);
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end

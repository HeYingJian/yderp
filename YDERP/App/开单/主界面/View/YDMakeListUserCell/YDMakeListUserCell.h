//
//  YDMakeListUserCell.h
//  YDERP
//
//  Created by 何英健 on 2021/8/12.
//

#import <UIKit/UIKit.h>

typedef void(^BlockMakeListShowSelectUser)(void);
typedef void(^BlockMakeListShowSelectAddress)(void);
typedef void(^BlockMakeListClearUser)(void);
typedef void(^BlockMakeListShowSelectProduct)(void);

@interface YDMakeListUserCell : UITableViewCell

+ (instancetype)cellWithTableView:(UITableView *)tableView;

+ (CGFloat)getHeight;

@property (nonatomic, copy) BlockMakeListShowSelectUser showSelectUserBlock;
@property (nonatomic, copy) BlockMakeListShowSelectAddress showSelectAddressBlock;
@property (nonatomic, copy) BlockMakeListClearUser clearUserBlock;
@property (nonatomic, copy) BlockMakeListShowSelectProduct showSelectProductBlock;

// 选择的客户
@property (nonatomic, strong) YDSearcherCustomerData *selectedCustomer;
// 选择的地址
@property (nonatomic, strong) YDCustomerAddressData *selectedAddress;

// 隐藏编辑
@property (nonatomic, assign) BOOL hideEdit;

@end

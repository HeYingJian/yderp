//
//  YDSelectProductVC.h
//  YDERP
//
//  Created by 何英健 on 2021/8/15.
//

#import <UIKit/UIKit.h>

typedef void(^BlockMakeListSelectedProduct)(NSMutableArray<YDSearcherProductData *> *array);

@interface YDSelectProductVC : UIViewController

// 原始选择商品
@property (nonatomic, strong) NSMutableArray<YDSearcherProductData *> *originSelectedArray;
// 选择结束回调
@property (nonatomic, copy) BlockMakeListSelectedProduct selectedProductBlock;

@end

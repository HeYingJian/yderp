//
//  YDSelectProductVC.m
//  YDERP
//
//  Created by 何英健 on 2021/8/15.
//

#import "YDSelectProductVC.h"
#import "YDSelectProductCell.h"
#import "YDSelectSKUPopView.h"
#import "YDAddProductVC.h"

@interface YDSelectProductVC () <UITextFieldDelegate, UITableViewDelegate, UITableViewDataSource>

// 搜索器
@property (nonatomic, strong) YDSearcher *searcher;

@property (nonatomic, weak) IBOutlet UITextField *searchTextField;

@property (nonatomic, weak) IBOutlet UITableView *tableView;

@property (nonatomic, weak) IBOutlet UILabel *totalTypeLabel;
@property (nonatomic, weak) IBOutlet UILabel *totalQuantityLabel;

// 当前选择的商品
@property (nonatomic, strong) NSMutableArray<YDSearcherProductData *> *selectedArray;

@end

@implementation YDSelectProductVC

#pragma mark - 懒加载

- (YDSearcher *)searcher
{
    if (!_searcher) {
        _searcher = [YDSearcher createWithType:YDSearchTypeProduct];
        _searcher.isQuerySku = YES;
        _searcher.pageSize = 10;
    }
    
    return _searcher;
}

- (NSMutableArray<YDSearcherProductData *> *)selectedArray
{
    if (!_selectedArray) {
        _selectedArray = [NSMutableArray array];
    }
    
    return _selectedArray;
}

#pragma mark - 页面加载

- (void)viewDidLoad
{
    [super viewDidLoad];

    [self initUI];
    
    [self addNotification];
    
    [self refreshData:YES];
}

- (void)initUI
{
    self.totalTypeLabel.font = FONT_NUMBER(20);
    self.totalQuantityLabel.font = FONT_NUMBER(20);
    [self setupTableView];
}

- (void)setupTableView
{
    self.tableView.hidden = YES;
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    self.tableView.showsVerticalScrollIndicator = NO;
    self.tableView.showsHorizontalScrollIndicator = NO;
    
    if (@available(iOS 11.0, *)) {
        self.tableView.contentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentNever;
        
    } else {
        self.automaticallyAdjustsScrollViewInsets = NO;
    }

    self.tableView.mj_header = [YDRefreshHeader headerWithRefreshingTarget:self refreshingAction:@selector(pullingRefresh)];
    self.tableView.mj_footer = [YDRefreshFooter footerWithRefreshingTarget:self refreshingAction:@selector(loadMore)];
}

- (void)addNotification
{
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(refreshDataWithNotification) name:REFRESH_PRODUCT_MANAGE object:nil];
}

- (void)pullingRefresh
{
    [self refreshData:NO];
}

- (void)refreshDataWithNotification
{
    [self refreshData:YES];
}

- (void)refreshData:(BOOL)showLoading
{
    [self dismissKeybord];
    
    if (showLoading) {
        [YDLoadingView showToSuperview:self.view];
    }
    
    YDWeakSelf
    if (!self.searcher.completion) {
        self.searcher.completion = ^(BaseVMRefreshType type) {
            weakSelf.tableView.hidden = NO;
            [YDLoadingView hideToSuperview:weakSelf.view];

            [weakSelf.tableView.mj_header endRefreshing];
            if (type == BaseVMRefreshTypeNoMore) {
                [weakSelf.tableView.mj_footer endRefreshingWithNoMoreData];

            } else {
                [weakSelf.tableView.mj_footer endRefreshing];
            }
            
            [weakSelf.tableView reloadData];
        };
    }
    
    if (!self.searcher.faildBlock) {
        self.searcher.faildBlock = ^(NSInteger code, NSString *error) {
            weakSelf.tableView.hidden = NO;
            [YDLoadingView hideToSuperview:weakSelf.view];
            [weakSelf.tableView.mj_header endRefreshing];
            [weakSelf.tableView.mj_footer endRefreshing];
            [MBProgressHUD showToastMessage:error];
        };
    }
    
    self.searcher.keyword = self.searchTextField.text;
    [self.searcher refreshData];
}

- (void)loadMore
{
    [self.searcher loadMoreData];
}

// 刷新底部统计
- (void)refreshCountView;
{
    // 总件数
    NSInteger totalQuantity = 0;
    for (YDSearcherProductData *sub in self.selectedArray) {
        for (YDSearcherSkuGroupData *groupData in sub.skuGroupList) {
            for (YDSearcherSkuList *sku in groupData.skuList) {
                totalQuantity += sku.selectedNum;
            }
        }
    }
    
    self.totalTypeLabel.text = [NSString stringWithFormat:@"%lu", (unsigned long)self.selectedArray.count];
    self.totalQuantityLabel.text = [NSString stringWithFormat:@"%ld", (long)totalQuantity];
}

// 商品是否已被选择
- (BOOL)isProductSelected:(YDSearcherProductData *)data
{
    BOOL isSelected = NO;
    for (YDSearcherProductData *sub in self.selectedArray) {
        if (sub.ID == data.ID) {
            isSelected = YES;
        }
    }
    
    return isSelected;
}

- (void)dismissKeybord
{
    [self.searchTextField resignFirstResponder];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [self.navigationController setNavigationBarHidden:YES animated:NO];
}

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

#pragma mark - 点击事件

- (IBAction)tapBackBtn:(id)sender
{
    [self dismissKeybord];
    [self.navigationController popViewControllerAnimated:YES];
}

// 点击新增商品
- (IBAction)tapAddBtn:(id)sender
{
    YDAddProductVC *vc = [[YDAddProductVC alloc] init];
    vc.type = YDProductTypeAdd;
    vc.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:vc animated:YES];
}

// 点击确定
- (IBAction)tapConfirmBtn:(id)sender
{
    if (self.selectedProductBlock) {
        self.selectedProductBlock(self.selectedArray);
    }
    
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - UITableView代理

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    NSInteger count = self.searcher.productDataArray.count;
    if (count) {
        return count;
        
    } else {
        return 1;
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (self.searcher.productDataArray.count) {
        return [YDSelectProductCell getHeight];
        
    } else {
        return [YDNullCell getHeightWithTableView:tableView];
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (self.searcher.productDataArray.count) {
        YDSelectProductCell *cell = [YDSelectProductCell cellWithTableView:tableView];
        YDSearcherProductData *data = self.searcher.productDataArray[indexPath.row];
        cell.data = data;
        
        // 先判断旧的选择中是否已经有这件商品
        BOOL isSelected = NO;
        for (YDSearcherProductData *originData in self.originSelectedArray) {
            if (originData.ID == data.ID) {
                isSelected = YES;
            }
        }
        
        if (!isSelected) {
            // 再判断当前选择中是否有这件商品
            isSelected = [self isProductSelected:data];
        }
        cell.isSelected = isSelected;
        
        return cell;
        
    } else {
        YDNullCell *cell = [YDNullCell cellWithTableView:tableView];
        cell.content = @"暂无商品哦！";
        return cell;
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [self dismissKeybord];
    
    if (self.searcher.productDataArray.count) {
        YDSearcherProductData *data = self.searcher.productDataArray[indexPath.row];
        
        YDWeakSelf
        // 弹出sku选择框
        [YDSelectSKUPopView showWithType:YDSelectSKUTypeSelectProduct data:data orderID:nil version:nil completion:^(BOOL isSelected) {
            BOOL isExist = [weakSelf isProductSelected:data];
            if (isExist && !isSelected) {
                // 从选择数组中删除
                [weakSelf.selectedArray removeObject:data];
                
            } else if (!isExist && isSelected) {
                // 加入选择数组
                [weakSelf.selectedArray addObject:data];
            }
            
            // 刷新，解决动画跳动问题
            [UIView animateWithDuration:0 animations:^{
                if (@available(iOS 11.0, *)) {
                    [weakSelf.tableView performBatchUpdates:^{
                        [weakSelf.tableView reloadRowAtIndexPath:indexPath withRowAnimation:UITableViewRowAnimationAutomatic];
                    } completion:nil];
                    
                } else {
                    [weakSelf.tableView reloadRowAtIndexPath:indexPath withRowAnimation:UITableViewRowAnimationAutomatic];
                }
            }];
            
            [weakSelf refreshCountView];
        }];
    }
}

#pragma mark - UITextFieldDelegate

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    
    return YES;
}

- (BOOL)textFieldShouldEndEditing:(UITextField *)textField
{
    [self refreshData:YES];
    
    return YES;
}

@end

//
//  YDSelectProductCell.h
//  YDERP
//
//  Created by 何英健 on 2021/8/15.
//

#import <UIKit/UIKit.h>

@interface YDSelectProductCell : UITableViewCell

+ (instancetype)cellWithTableView:(UITableView *)tableView;

+ (CGFloat)getHeight;

@property (nonatomic, strong) YDSearcherProductData *data;

// 是否已选
@property (nonatomic, assign) BOOL isSelected;

@end

//
//  YDSelectProductCell.m
//  YDERP
//
//  Created by 何英健 on 2021/8/15.
//

#import "YDSelectProductCell.h"

@interface YDSelectProductCell ()

@property (nonatomic, weak) IBOutlet UIImageView *productImgView;
@property (nonatomic, weak) IBOutlet UIImageView *selectedImgView;
@property (nonatomic, weak) IBOutlet UILabel *productNameLabel;
@property (nonatomic, weak) IBOutlet UILabel *priceLabel;
@property (nonatomic, weak) IBOutlet UILabel *stockCountLabel;
@property (nonatomic, weak) IBOutlet UILabel *colorLabel;
@property (nonatomic, weak) IBOutlet UILabel *sizeLabel;
@property (nonatomic, weak) IBOutlet UILabel *salesQuantityLabel;

@end

@implementation YDSelectProductCell

+ (instancetype)cellWithTableView:(UITableView *)tableView
{
    static NSString *reuseID = @"selectProductCell";
    YDSelectProductCell *cell = [tableView dequeueReusableCellWithIdentifier:reuseID];
    if (!cell) {
        cell = [[NSBundle mainBundle] loadNibNamed:@"YDSelectProductCell" owner:nil options:nil][0];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    
    return cell;
}

+ (CGFloat)getHeight
{
    return 225;
}

- (void)setData:(YDSearcherProductData *)data
{
    _data = data;
    
    if (data.picList.count && data.picList[0].url.length) {
        [self.productImgView appleSetImageWithUrl:data.picList[0].url SprcifyOssImageStyle:@"products_list" HasHD:NO placeholderImage:@"默认商品方形图" completion:nil];
        
    } else {
        [self.productImgView setImage:[UIImage imageNamed:@"没有商品默认图"]];
    }
    
    NSString *codeStr = data.code;
    NSString *productName = data.name;
    NSString *str = [NSString stringWithFormat:@"%@#%@", codeStr, productName];
    NSMutableAttributedString *attributedStr = [[NSMutableAttributedString alloc] initWithString:str];
    [attributedStr addAttribute:NSFontAttributeName value:FONT_NUMBER(15) range:NSMakeRange(0, codeStr.length + 1)];
    [attributedStr addAttribute:NSForegroundColorAttributeName value:ColorFromRGB(0xFF5630) range:NSMakeRange(0, codeStr.length + 1)];
    [attributedStr addAttribute:NSFontAttributeName value:[UIFont systemFontOfSize:14.0f] range:NSMakeRange(codeStr.length + 1, productName.length)];
    [attributedStr addAttribute:NSForegroundColorAttributeName value:ColorFromRGB(0x212121) range:NSMakeRange(codeStr.length + 1, productName.length)];
    NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc] init];
    [paragraphStyle setLineSpacing:5];
    [attributedStr addAttribute:NSParagraphStyleAttributeName value:paragraphStyle range:NSMakeRange(0, [str length])];
    self.productNameLabel.attributedText = attributedStr;
    self.productNameLabel.lineBreakMode = NSLineBreakByTruncatingTail;
    
    self.priceLabel.text = [NSString applePrefixPriceWith:data.salesPrice];
    
    NSInteger dueNum = 0;
    NSInteger availableNum = 0;
    for (YDSearcherSkuGroupData *sub in data.skuGroupList) {
        for (YDSearcherSkuList *sku in sub.skuList) {
            dueNum += sku.dueNum;
            availableNum += sku.salesNum + sku.cacheSelectedNum;
        }
    }
    
    UIColor *stockColor;
    if (availableNum >= 0) {
        stockColor = ColorFromRGB(0x9D9D9D);
        
    } else {
        stockColor = ColorFromRGB(0xFFAB00);
    }
    
    NSString *avaStr = [NSString stringWithFormat:@"%ld", (long)availableNum];
    NSString *stockStr = [NSString stringWithFormat:@"%@件可售 | %ld件欠货", avaStr, (long)dueNum];
    NSMutableAttributedString *stockAttributedStr = [[NSMutableAttributedString alloc] initWithString:stockStr];
    [stockAttributedStr addAttribute:NSForegroundColorAttributeName value:stockColor range:NSMakeRange(0, avaStr.length + 1)];
    self.stockCountLabel.attributedText = stockAttributedStr;
    
    // 从skuList中获取颜色和尺码
    NSMutableArray<NSString *> *colorArray = [NSMutableArray array];
    NSMutableArray<NSString *> *sizeArray = [NSMutableArray array];
    for (YDSearcherSkuList *sub in data.skuList) {
        BOOL exist = NO;
        for (NSString *color in colorArray) {
            if ([color isEqualToString:sub.color]) {
                exist = YES;
            }
        }
        
        if (!exist) {
            [colorArray addObject:sub.color];
        }
        
        exist = NO;
        for (NSString *size in sizeArray) {
            if ([size isEqualToString:sub.size]) {
                exist = YES;
            }
        }
        
        if (!exist) {
            [sizeArray addObject:sub.size];
        }
    }
    
    NSString *colorStr = @"";
    for (int i = 0; i < colorArray.count; i++) {
        NSString *color = colorArray[i];
        if (colorStr.length) {
            colorStr = [NSString stringWithFormat:@"%@/%@", colorStr, color];
            
        } else {
            colorStr = color;
        }
    }
    self.colorLabel.text = colorStr;
    
    NSString *sizeStr = @"";
    for (int i = 0; i < sizeArray.count; i++) {
        NSString *size = sizeArray[i];
        if (sizeStr.length) {
            sizeStr = [NSString stringWithFormat:@"%@、%@", sizeStr, size];
            
        } else {
            sizeStr = size;
        }
    }
    self.sizeLabel.text = sizeStr;
    
    self.salesQuantityLabel.text = [NSString stringWithFormat:@"%ld件", data.totalSalesNum];
}

- (void)setIsSelected:(BOOL)isSelected
{
    _isSelected = isSelected;
    
    self.selectedImgView.hidden = !isSelected;
}

- (void)awakeFromNib
{
    [super awakeFromNib];
    
    self.priceLabel.font = FONT_NUMBER(17);
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end

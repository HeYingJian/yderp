//
//  YDVerifyPhoneVC.h
//  YDERP
//
//  Created by 何英健 on 2021/7/29.
//

#import <UIKit/UIKit.h>
#import "YDResetPasswordVC.h"

@interface YDVerifyPhoneVC : UIViewController

// 类型
@property (nonatomic, assign) YDResetPasswordType type;

@end

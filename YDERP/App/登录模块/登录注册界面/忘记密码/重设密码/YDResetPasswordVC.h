//
//  YDResetPasswordVC.h
//  YDERP
//
//  Created by 何英健 on 2021/7/29.
//

#import <UIKit/UIKit.h>

typedef enum : NSUInteger {
    YDResetPasswordTypeLogin, // 从登录进入
    YDResetPasswordTypeSetting // 从设置进入
} YDResetPasswordType;

@interface YDResetPasswordVC : UIViewController

@property (nonatomic, copy) NSString *userName;
@property (nonatomic, copy) NSString *smsCode;
@property (nonatomic, copy) NSString *uniqueId;

// 类型
@property (nonatomic, assign) YDResetPasswordType type;

@end

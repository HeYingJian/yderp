//
//  YDResetPasswordVC.m
//  YDERP
//
//  Created by 何英健 on 2021/7/29.
//

#import "YDResetPasswordVC.h"

@interface YDResetPasswordVC () <UITextFieldDelegate>

@property (nonatomic, weak) IBOutlet UITextField *passwordField1;
@property (nonatomic, weak) IBOutlet UITextField *passwordField2;

@property (nonatomic, weak) IBOutlet UIButton *confirmBtn;

@end

@implementation YDResetPasswordVC

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self initUI];
}

- (void)initUI
{
    [self.navigationItem addDefaultBackButton:self];
    
    self.passwordField1.font = FONT_NUMBER(22);
    self.passwordField2.font = FONT_NUMBER(22);
    
    NSMutableAttributedString *passwordStr1 = [[NSMutableAttributedString alloc] initWithString:@"输入6–12位密码" attributes:
         @{NSForegroundColorAttributeName:ColorFromRGB(0xCCCCCC),
           NSFontAttributeName:[UIFont systemFontOfSize:17]}
         ];
    self.passwordField1.attributedPlaceholder = passwordStr1;
    
    NSMutableAttributedString *passwordStr2 = [[NSMutableAttributedString alloc] initWithString:@"再次确认密码" attributes:
         @{NSForegroundColorAttributeName:ColorFromRGB(0xCCCCCC),
           NSFontAttributeName:[UIFont systemFontOfSize:17]}
         ];
    self.passwordField2.attributedPlaceholder = passwordStr2;
    
    [self refreshConfirmBtn];
}

- (void)refreshConfirmBtn
{
    if (self.passwordField1.text.length && self.passwordField2.text.length) {
        [self setConfirmBtnStatus:YES];
        
    } else {
        [self setConfirmBtnStatus:NO];
    }
}

- (void)setConfirmBtnStatus:(BOOL)status
{
    self.confirmBtn.userInteractionEnabled = status;
    
    if (status) {
        self.confirmBtn.backgroundColor = COLOR_MAIN_BULE;
        
    } else {
        self.confirmBtn.backgroundColor = ColorFromRGB(0xCCCCCC);
    }
}

- (void)dismissKeybord
{
    [self.passwordField1 resignFirstResponder];
    [self.passwordField2 resignFirstResponder];
}

#pragma mark - 点击事件

// 点击确认按钮
- (IBAction)tapConfirmBtn:(id)sender
{
    [self dismissKeybord];
    
    if (self.passwordField1.text.length < 6) {
        [MBProgressHUD showToastMessage:@"密码不能小于6位数"];
        return;
    }
    
    if (self.passwordField1.text.length > 12) {
        [MBProgressHUD showToastMessage:@"密码不能大于12位数"];
        return;
    }
    
    if (![self.passwordField1.text isEqualToString:self.passwordField2.text]) {
        [MBProgressHUD showToastMessage:@"两次密码输入不一致，请重试！"];
        return;
    }
    
    [YDLoadingView showToSuperview:self.view];
    
    YDWeakSelf
    [YDLoginFunction resetPasswordWithUserName:self.userName password:self.passwordField1.text smsCode:weakSelf.smsCode uniqueId:weakSelf.uniqueId succeedBlock:^{
        [YDLoadingView hideToSuperview:weakSelf.view];
        [MBProgressHUD showToastMessage:@"修改成功"];
        
        if (weakSelf.type == YDResetPasswordTypeLogin) {
            NSMutableDictionary *dict = [NSMutableDictionary dictionary];
            [dict setObject:weakSelf.userName forKey:@"userName"];
            [dict setObject:weakSelf.passwordField1.text forKey:@"password"];
            PostNotification(NOTI_CHANGE_PASSWORD, dict);
        }
        
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            [weakSelf.navigationController popToRootViewControllerAnimated:YES];
        });
        
    } faildBlock:^(NSInteger code, NSString *error) {
        [YDLoadingView hideToSuperview:weakSelf.view];
        [MBProgressHUD showToastMessage:error];
    }];
}

#pragma mark - UITextFieldDelegate

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];

    return YES;
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    if (textField == self.passwordField1) {
        if (self.passwordField2.text.length) {
            if (string.length || self.passwordField1.text.length > 1) {
                [self setConfirmBtnStatus:YES];
                
            } else {
                [self setConfirmBtnStatus:NO];
            }
            
        } else {
            [self setConfirmBtnStatus:NO];
        }
        
    } else if (textField == self.passwordField2) {
        if (self.passwordField1.text.length) {
            if (string.length || self.passwordField2.text.length > 1) {
                [self setConfirmBtnStatus:YES];
                
            } else {
                [self setConfirmBtnStatus:NO];
            }
            
        } else {
            [self setConfirmBtnStatus:NO];
        }
    }
    
    return YES;
}

- (BOOL)textFieldShouldEndEditing:(UITextField *)textField
{
    [self refreshConfirmBtn];
    return YES;
}

@end

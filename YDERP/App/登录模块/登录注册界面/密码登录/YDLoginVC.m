//
//  YDLoginVC.m
//  YDERP
//
//  Created by 何英健 on 2021/7/19.
//

#import "YDLoginVC.h"
#import "YDRegisterVC.h"
#import "YDSMSLoginVC.h"
#import "YDVerifyPhoneVC.h"

@interface YDLoginVC () <UITextFieldDelegate>

@property (nonatomic, weak) IBOutlet UILabel *phoneHeaderLabel;
@property (nonatomic, weak) IBOutlet UITextField *phoneTextField;
@property (nonatomic, weak) IBOutlet UITextField *passwordField;

@property (nonatomic, weak) IBOutlet UIButton *loginBtn;
@property (nonatomic, weak) IBOutlet UIButton *registerBtn;

@property (nonatomic, weak) IBOutlet UIButton *agreeBtn;
@property (nonatomic, weak) IBOutlet UIView *agreeMaskView;

@property (nonatomic, weak) IBOutlet UIButton *changeEnvironmentBtn;

@end

@implementation YDLoginVC

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self initUI];
    
    [self addNotification];
}

- (void)initUI
{
    UILongPressGestureRecognizer *longPress = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(changeEnvironment:)];
    longPress.minimumPressDuration = 2;
    [self.changeEnvironmentBtn addGestureRecognizer:longPress];
    
    self.phoneHeaderLabel.font = FONT_NUMBER(22);
    self.phoneTextField.font = FONT_NUMBER(22);
    self.passwordField.font = FONT_NUMBER(22);
    self.registerBtn.layer.borderColor = ColorFromRGB(0xE6E7EC).CGColor;
    self.registerBtn.layer.borderWidth = 1;
    
    NSMutableAttributedString *phoneStr = [[NSMutableAttributedString alloc] initWithString:@"请输入手机号" attributes:
         @{NSForegroundColorAttributeName:ColorFromRGB(0xCCCCCC),
           NSFontAttributeName:[UIFont systemFontOfSize:17]}
         ];
    self.phoneTextField.attributedPlaceholder = phoneStr;
    
    NSMutableAttributedString *passwordStr = [[NSMutableAttributedString alloc] initWithString:@"请输入密码" attributes:
         @{NSForegroundColorAttributeName:ColorFromRGB(0xCCCCCC),
           NSFontAttributeName:[UIFont systemFontOfSize:17]}
         ];
    self.passwordField.attributedPlaceholder = passwordStr;
    
    [self.phoneTextField addToolSenderWithBlock:nil];
    
    UITapGestureRecognizer *tapAgree = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapAgreeBtn)];
    [self.agreeMaskView addGestureRecognizer:tapAgree];
    
    // 自动填入上次手机号码
    NSString *phone = GetUserDefaultWithKey(STORE_LAST_LOGIN_PHONE);
    if (phone.length) {
        self.phoneTextField.text = phone;
    }
    
    [self refreshLoginBtn];
}

- (void)refreshLoginBtn
{
    if (self.phoneTextField.text.length && self.passwordField.text.length) {
        [self setLoginBtnStatus:YES];
        
    } else {
        [self setLoginBtnStatus:NO];
    }
}

- (void)setLoginBtnStatus:(BOOL)status
{
    self.loginBtn.userInteractionEnabled = status;
    
    if (status) {
        self.loginBtn.backgroundColor = COLOR_MAIN_BULE;
        
    } else {
        self.loginBtn.backgroundColor = ColorFromRGB(0xCCCCCC);
    }
}

- (void)addNotification
{
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(resetPasswordSucceed:) name:NOTI_CHANGE_PASSWORD object:nil];
}

- (void)resetPasswordSucceed:(NSNotification *)notification
{
    NSMutableDictionary *dict = notification.object;
    self.phoneTextField.text = [dict objectForKey:@"userName"];
    self.passwordField.text = [dict objectForKey:@"password"];
    [self refreshLoginBtn];
}

- (void)dismissKeybord
{
    [self.phoneTextField resignFirstResponder];
    [self.passwordField resignFirstResponder];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    self.navigationController.navigationBar.translucent = YES;
    [self.navigationController setNavigationBarHidden:YES animated:NO];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    
    [self.navigationController setNavigationBarHidden:NO animated:NO];
}

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

#pragma mark - 点击事件

// 验证码登录
- (IBAction)tapSMSLoginBtn:(id)sender
{
    [self dismissKeybord];
    
    YDSMSLoginVC *vc = [[YDSMSLoginVC alloc] init];
    [self.navigationController pushViewController:vc animated:YES];
}

// 忘记密码
- (IBAction)tapForgetPasswordBtn:(id)sender
{
    [self dismissKeybord];
    
    YDVerifyPhoneVC *vc = [[YDVerifyPhoneVC alloc] init];
    vc.type = YDResetPasswordTypeLogin;
    [self.navigationController pushViewController:vc animated:YES];
}

// 点击登录按钮
- (IBAction)tapLoginBtn:(id)sender
{
    [self dismissKeybord];
    
    if (self.phoneTextField.text.length != 11) {
        [MBProgressHUD showToastMessage:@"手机号有误，请检查后重试！"];
        return;
    }
    
    if (!self.agreeBtn.selected) {
        [MBProgressHUD showToastMessage:@"请阅读并勾选下方协议"];
        return;
    }
    
    [YDLoadingView showToSuperview:self.view];
    
    YDWeakSelf
    [YDLoginFunction loginWithUserName:self.phoneTextField.text password:self.passwordField.text smsCode:nil uniqueId:nil succeedBlock:^(YDLoginData *data) {
        [YDLoadingView hideToSuperview:weakSelf.view];
        [YDFunction loginSucceedWithLoginData:data];
        
        SetUserDefaultKeyWithObject(STORE_LAST_LOGIN_PHONE, self.phoneTextField.text);
        
    } faildBlock:^(NSInteger code, NSString *error) {
        [YDLoadingView hideToSuperview:weakSelf.view];
        
        if (error.length) {
            [MBProgressHUD showToastMessage:error];
        }
    }];
}

// 点击注册按钮
- (IBAction)tapRegisterBtn:(id)sender
{
    [self dismissKeybord];
    
    YDRegisterVC *vc = [[YDRegisterVC alloc] init];
    [[UINavigationController rootNavigationController] pushViewController:vc animated:YES];
}

// 点击同意按钮
- (void)tapAgreeBtn
{
    self.agreeBtn.selected = !self.agreeBtn.selected;
}

// 点击服务协议按钮
- (IBAction)tapProtocolBtn:(id)sender
{
    [YDWebViewVC showWebViewWithUrl:WEBURL_SERVICE isHideNav:YES];
}

- (void)changeEnvironment:(UIGestureRecognizer *)res
{
    if (res.state == UIGestureRecognizerStateBegan) {
        NSString *environment = GetUserDefaultWithKey(STORE_ENVIRONMENT);
        
        if ([environment isEqualToString:@"Test"]) {
            kAppDelegate.type = YDEnvironmentTypePrepare;
            [MBProgressHUD showToastMessage:@"已切换到预生产"];
            SetUserDefaultKeyWithObject(STORE_ENVIRONMENT, @"Prepare");
            
        } else if ([environment isEqualToString:@"Prepare"]) {
            kAppDelegate.type = YDEnvironmentTypeRelease;
            [MBProgressHUD showToastMessage:@"已切换到生产"];
            SetUserDefaultKeyWithObject(STORE_ENVIRONMENT, @"Release");
            
        } else {
            kAppDelegate.type = YDEnvironmentTypeTest;
            [MBProgressHUD showToastMessage:@"已切换到测试"];
            SetUserDefaultKeyWithObject(STORE_ENVIRONMENT, @"Test");
        }
        UserDefaultSynchronize;
    }
}

#pragma mark - UITextFieldDelegate

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];

    return YES;
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    if (textField == self.phoneTextField) {
        if (self.passwordField.text.length) {
            if (string.length || self.phoneTextField.text.length > 1) {
                [self setLoginBtnStatus:YES];
                
            } else {
                [self setLoginBtnStatus:NO];
            }
            
        } else {
            [self setLoginBtnStatus:NO];
        }
        
    } else if (textField == self.passwordField) {
        if (self.phoneTextField.text.length) {
            if (string.length || self.passwordField.text.length > 1) {
                [self setLoginBtnStatus:YES];
                
            } else {
                [self setLoginBtnStatus:NO];
            }
            
        } else {
            [self setLoginBtnStatus:NO];
        }
    }
    
    return YES;
}

- (BOOL)textFieldShouldEndEditing:(UITextField *)textField
{
    [self refreshLoginBtn];
    return YES;
}

@end

//
//  YDSMSLoginVC.m
//  YDERP
//
//  Created by 何英健 on 2021/7/29.
//

#import "YDSMSLoginVC.h"

@interface YDSMSLoginVC () <UITextFieldDelegate>

// 保存验证码发送回执
@property (nonatomic, copy) NSString *uniqueId;

@property (nonatomic, weak) IBOutlet UILabel *phoneHeaderLabel;
@property (nonatomic, weak) IBOutlet UITextField *phoneTextField;
@property (nonatomic, weak) IBOutlet UITextField *smsTextField;

@property (nonatomic, weak) IBOutlet UIButton *loginBtn;
@property (nonatomic, weak) IBOutlet UIButton *sendVityerBtn;
@property (nonatomic, weak) IBOutlet UIButton *agreeBtn;
@property (nonatomic, weak) IBOutlet UIView *agreeMaskView;

@property (nonatomic, assign) NSInteger time;

@end

@implementation YDSMSLoginVC

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self initUI];
    
    self.uniqueId = GetUserDefaultWithKey(STORE_UNIQUE_ID);
}

- (void)initUI
{
    self.phoneHeaderLabel.font = FONT_NUMBER(22);
    self.phoneTextField.font = FONT_NUMBER(22);
    self.smsTextField.font = FONT_NUMBER(22);
    
    NSMutableAttributedString *phoneStr = [[NSMutableAttributedString alloc] initWithString:@"输入手机号" attributes:
         @{NSForegroundColorAttributeName:ColorFromRGB(0xCCCCCC),
           NSFontAttributeName:[UIFont systemFontOfSize:17]}
         ];
    self.phoneTextField.attributedPlaceholder = phoneStr;
    
    NSMutableAttributedString *smsStr = [[NSMutableAttributedString alloc] initWithString:@"输入验证码" attributes:
         @{NSForegroundColorAttributeName:ColorFromRGB(0xCCCCCC),
           NSFontAttributeName:[UIFont systemFontOfSize:17]}
         ];
    self.smsTextField.attributedPlaceholder = smsStr;
    
    [self.phoneTextField addToolSenderWithBlock:nil];
    [self.smsTextField addToolSenderWithBlock:nil];
    
    UITapGestureRecognizer *tapAgree = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapAgreeBtn)];
    [self.agreeMaskView addGestureRecognizer:tapAgree];
    
    // 自动填入上次手机号码
    NSString *phone = GetUserDefaultWithKey(STORE_LAST_LOGIN_PHONE);
    if (phone.length) {
        self.phoneTextField.text = phone;
    }
    
    [self refreshUI];
}

- (void)refreshUI
{
    [self refreshLoginBtn];
    [self refreshSendVityerBtn];
}

- (void)refreshLoginBtn
{
    if (self.phoneTextField.text.length && self.smsTextField.text.length) {
        [self setLoginBtnStatus:YES];
        
    } else {
        [self setLoginBtnStatus:NO];
    }
}

- (void)setLoginBtnStatus:(BOOL)status
{
    self.loginBtn.userInteractionEnabled = status;
    
    if (status) {
        self.loginBtn.backgroundColor = COLOR_MAIN_BULE;
        
    } else {
        self.loginBtn.backgroundColor = ColorFromRGB(0xCCCCCC);
    }
}

- (void)refreshSendVityerBtn
{
    if (self.phoneTextField.text.length) {
        [self setVityerBtnStatus:YES];
        
    } else {
        [self setVityerBtnStatus:NO];
    }
}

- (void)setVityerBtnStatus:(BOOL)enable
{
    self.sendVityerBtn.userInteractionEnabled = enable;
    
    if (enable) {
        [self.sendVityerBtn setTitleColor:COLOR_MAIN_BULE forState:UIControlStateNormal];
        [self.sendVityerBtn setBackgroundColor:ColorFromRGB(0xE3EAFC)];
        
    } else {
        [self.sendVityerBtn setTitleColor:ColorFromRGB(0xCCCCCC) forState:UIControlStateNormal];
        [self.sendVityerBtn setBackgroundColor:ColorFromRGB(0xF1F2F6)];
    }
}

// 检查
- (BOOL)checkAvailable
{
    if (self.phoneTextField.text.length != 11) {
        [MBProgressHUD showToastMessage:@"请填写11位电话号码"];
        return NO;
    }
    
    return YES;
}

- (void)dismissKeybord
{
    [self.phoneTextField resignFirstResponder];
    [self.smsTextField resignFirstResponder];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [self.navigationController setNavigationBarHidden:YES animated:NO];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    
    [self.navigationController setNavigationBarHidden:NO animated:NO];
}

#pragma mark - 点击事件

- (IBAction)tapBackBtn:(id)sender
{
    [self dismissKeybord];
    [self.navigationController popViewControllerAnimated:YES];
}

// 点击登录按钮
- (IBAction)tapLoginBtn:(id)sender
{
    [self dismissKeybord];
    
    if (self.phoneTextField.text.length != 11) {
        [MBProgressHUD showToastMessage:@"手机号有误，请检查后重试！"];
        return;
    }
    
    if (!self.agreeBtn.selected) {
        [MBProgressHUD showToastMessage:@"请阅读并勾选下方协议"];
        return;
    }
    
    [YDLoadingView showToSuperview:self.view];
    
    YDWeakSelf
    [YDLoginFunction loginWithUserName:self.phoneTextField.text password:nil smsCode:self.smsTextField.text uniqueId:self.uniqueId succeedBlock:^(YDLoginData *data) {
        [YDLoadingView hideToSuperview:weakSelf.view];
        [YDFunction loginSucceedWithLoginData:data];
        
        SetUserDefaultKeyWithObject(STORE_LAST_LOGIN_PHONE, self.phoneTextField.text);
        
    } faildBlock:^(NSInteger code, NSString *error) {
        [YDLoadingView hideToSuperview:weakSelf.view];
        
        if (error.length) {
            [MBProgressHUD showToastMessage:error];
        }
    }];
}

// 点击同意按钮
- (void)tapAgreeBtn
{
    self.agreeBtn.selected = !self.agreeBtn.selected;
}

// 点击服务协议按钮
- (IBAction)tapProtocolBtn:(id)sender
{
    [YDWebViewVC showWebViewWithUrl:WEBURL_SERVICE isHideNav:YES];
}

#pragma mark - 发送验证码

// 发送验证码
- (IBAction)tapSendVityerBtn:(id)sender
{
    [self dismissKeybord];

    if (!self.phoneTextField.text.length) {
        [MBProgressHUD showToastMessage:@"请先输入手机号码"];
        return;
    }
    
    if (self.phoneTextField.text.length != 11) {
        [MBProgressHUD showToastMessage:@"手机号有误，请检查后重试！"];
        return;
    }
    
    [self.smsTextField becomeFirstResponder];
    
    [YDLoadingView showToSuperview:self.view];
    
    YDWeakSelf
    [YDLoginFunction sendSMSWithUserName:self.phoneTextField.text succeedBlock:^(NSString *uniqueId) {
        [YDLoadingView hideToSuperview:weakSelf.view];
        
        [MBProgressHUD showSuccessMessage:@"发送成功"];
        
        weakSelf.time = 60;
        weakSelf.uniqueId = uniqueId;
        SetUserDefaultKeyWithObject(STORE_UNIQUE_ID, uniqueId);
        
        [weakSelf openCountdownWith:sender];
        
    } faildBlock:^(NSInteger code, NSString *error) {
        [YDLoadingView hideToSuperview:weakSelf.view];
        [MBProgressHUD showToastMessage:error];
    }];
}

// 开启倒计时效果
- (void)openCountdownWith:(UIButton *)sender
{
    YDWeakSelf
    dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
    dispatch_source_t _timer = dispatch_source_create(DISPATCH_SOURCE_TYPE_TIMER, 0, 0, queue);
    
    dispatch_source_set_timer(_timer,dispatch_walltime(NULL, 0), 1.0 * NSEC_PER_SEC, 0); //每秒执行
    
    dispatch_source_set_event_handler(_timer, ^{
        if (self.time <= 0) {
            //倒计时结束，关闭
            dispatch_source_cancel(_timer);
            dispatch_async(dispatch_get_main_queue(), ^{
                //设置按钮的样式
                [sender setTitle:@"获取验证码" forState:UIControlStateNormal];
                [weakSelf setVityerBtnStatus:YES];
            });
            
        } else {
            dispatch_async(dispatch_get_main_queue(), ^{
                //设置按钮显示读秒效果
                if (self.time != 0) {
                    [sender setTitle:[NSString stringWithFormat:@"%.2ldS", (long)self.time] forState:UIControlStateNormal];
                    
                } else {
                    [sender setTitle:@"0S" forState:UIControlStateNormal];
                }
                [weakSelf setVityerBtnStatus:NO];
            });
            self.time--;
        }
    });
    dispatch_resume(_timer);
}

#pragma mark - UITextFieldDelegate

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];

    return YES;
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    if (textField == self.phoneTextField) {
        if (self.smsTextField.text.length) {
            if (string.length || self.phoneTextField.text.length > 1) {
                [self setLoginBtnStatus:YES];
                
            } else {
                [self setLoginBtnStatus:NO];
            }
            
        } else {
            [self setLoginBtnStatus:NO];
        }
        
        if (string.length || self.phoneTextField.text.length > 1) {
            [self setVityerBtnStatus:YES];
            
        } else {
            [self setVityerBtnStatus:NO];
        }
        
    } else if (textField == self.smsTextField) {
        if (self.phoneTextField.text.length) {
            if (string.length || self.smsTextField.text.length > 1) {
                [self setLoginBtnStatus:YES];
                
            } else {
                [self setLoginBtnStatus:NO];
            }
            
        } else {
            [self setLoginBtnStatus:NO];
        }
    }
    
    return YES;
}

- (BOOL)textFieldShouldEndEditing:(UITextField *)textField
{
    [self refreshLoginBtn];
    return YES;
}

@end

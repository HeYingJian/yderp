//
//  YDInputShopNameVC.h
//  YDERP
//
//  Created by 何英健 on 2021/7/28.
//

#import <UIKit/UIKit.h>

@interface YDInputShopNameVC : UIViewController

@property (nonatomic, copy) NSString *userName;
@property (nonatomic, copy) NSString *password;
@property (nonatomic, copy) NSString *smsCode;
@property (nonatomic, copy) NSString *uniqueId;

@end

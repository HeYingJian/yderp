//
//  YDInputShopNameVC.m
//  YDERP
//
//  Created by 何英健 on 2021/7/28.
//

#import "YDInputShopNameVC.h"

@interface YDInputShopNameVC () <UITextFieldDelegate>

@property (nonatomic, weak) IBOutlet UITextField *textField;
@property (nonatomic, weak) IBOutlet UIButton *confirmBtn;

@end

@implementation YDInputShopNameVC

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self initUI];
}

- (void)initUI
{
    [self refreshConfirmBtn];
}

- (void)refreshConfirmBtn
{
    if (self.textField.text.length) {
        [self setConfirmBtnStatus:YES];
        
    } else {
        [self setConfirmBtnStatus:NO];
    }
}

- (void)setConfirmBtnStatus:(BOOL)status
{
    self.confirmBtn.userInteractionEnabled = status;
    
    if (status) {
        self.confirmBtn.backgroundColor = COLOR_MAIN_BULE;
        
    } else {
        self.confirmBtn.backgroundColor = ColorFromRGB(0xCCCCCC);
    }
}

- (void)dismissKeybord
{
    [self.textField resignFirstResponder];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [self.navigationController setNavigationBarHidden:YES animated:NO];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    
    [self.navigationController setNavigationBarHidden:NO animated:NO];
}

#pragma mark - 点击事件

- (IBAction)tapBackBtn:(id)sender
{
    [self dismissKeybord];
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)tapConfirmBtn:(id)sender
{
    [self dismissKeybord];
    
    if (self.textField.text.length < 2) {
        [MBProgressHUD showToastMessage:@"店铺名长度不能小于2位"];
        return;
    }
    
    if (self.textField.text.length > 12) {
        [MBProgressHUD showToastMessage:@"店铺名长度不能超过12位"];
        return;
    }
    
    [YDLoadingView showToSuperview:self.view];
    
    YDWeakSelf
    [YDLoginFunction registerWithUserName:self.userName password:self.password smsCode:self.smsCode uniqueId:self.uniqueId shopName:self.textField.text succeedBlock:^(YDLoginData *data) {
        [YDLoadingView hideToSuperview:weakSelf.view];
        [YDFunction loginSucceedWithLoginData:data];
        
        SetUserDefaultKeyWithObject(STORE_LAST_LOGIN_PHONE, self.userName);
        
    } faildBlock:^(NSInteger code, NSString *error) {
        [YDLoadingView hideToSuperview:weakSelf.view];
        [MBProgressHUD showToastMessage:error];
    }];
}

#pragma mark - UITextFieldDelegate

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];

    return YES;
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    if (string.length || self.textField.text.length > 1) {
        [self setConfirmBtnStatus:YES];
        
    } else {
        [self setConfirmBtnStatus:NO];
    }
    
    return YES;
}

- (BOOL)textFieldShouldEndEditing:(UITextField *)textField
{
    [self refreshConfirmBtn];
    return YES;
}

@end

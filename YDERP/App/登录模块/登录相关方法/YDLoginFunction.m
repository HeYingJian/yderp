//
//  YDLoginFunction.m
//  YDERP
//
//  Created by 何英健 on 2021/7/27.
//

#import "YDLoginFunction.h"

@implementation YDLoginFunction

+ (void)loginWithUserName:(NSString *)userName password:(NSString *)password smsCode:(NSString *)smsCode uniqueId:(NSString *)uniqueId succeedBlock:(BlockLoginSucceed)succeedBlock faildBlock:(BlockLoginFaild)faildBlock
{
    if (!userName.length) {
        [MBProgressHUD showToastMessage:@"请输入手机号"];
        return;
    }
    
    NSString *path = URL_LOGIN;
    
    NSMutableDictionary *param = [NSMutableDictionary dictionary];
    [param setObject:userName forKey:@"userName"];
    
    if (password.length) {
        [param setObject:[[password sha256String] base64EncodedString] forKey:@"password"];
    }
    
    if (smsCode.length) {
        [param setObject:smsCode forKey:@"smsCode"];
    }
    
    if (uniqueId.length) {
        [param setObject:uniqueId forKey:@"uniqueId"];
    }
    
    //请求回调Block
    AppleServerPostCompleteBlock postHandleBlock = ^(BOOL done, id _Nullable object, NSString * _Nullable error) {
        //接口调用成功
        if (done) {
            YDLoginModel *resp = [YDLoginModel modelWithJSON:object];
            
            if (resp.code == 200) {
                if (succeedBlock) {
                    succeedBlock(resp.data);
                }
            
            } else if (resp.code == 3009) {
                [YDAccountPackagePopView showWithType:YDAccountPopType3];
                
                if (faildBlock) {
                    faildBlock(resp.code, nil);
                }
                
            } else {
                if (faildBlock) {
                    NSString *msg;
                    if (resp.detailMessage.length) {
                        msg = resp.detailMessage;
                        
                    } else if (resp.message.length) {
                        msg = resp.message;
                        
                    } else {
                        msg = @"登录失败";
                    }
                    faildBlock(resp.code, msg);
                }
            }
            
        } else {
            if (faildBlock) {
                faildBlock(0, @"网络不给力");
            }
        }
    };
    
    //使用AppleServer发送POST请求
    [AppleServer.shared postWithPath:path Paramters:param CompleteBlock:postHandleBlock];
}

+ (void)logoutWithSucceedBlock:(BlockLoginFunctionSucceed)succeedBlock faildBlock:(BlockLoginFaild)faildBlock
{
    NSString *path = URL_LOGOUT;
    NSMutableDictionary *param = [NSMutableDictionary dictionary];
    
    //请求回调Block
    AppleServerPostCompleteBlock postHandleBlock = ^(BOOL done, id _Nullable object, NSString * _Nullable error) {
        //接口调用成功
        if (done) {
            YDBaseModel *resp = [YDBaseModel modelWithJSON:object];
            
            if (resp.code == 200) {
                if (succeedBlock) {
                    succeedBlock();
                }
            
            } else {
                if (faildBlock) {
                    NSString *msg;
                    if (resp.detailMessage.length) {
                        msg = resp.detailMessage;
                        
                    } else if (resp.message.length) {
                        msg = resp.message;
                        
                    } else {
                        msg = @"退出失败";
                    }
                    faildBlock(resp.code, msg);
                }
            }
            
        } else {
            if (faildBlock) {
                faildBlock(0, @"网络不给力");
            }
        }
    };
    
    //使用AppleServer发送POST请求
    [AppleServer.shared postWithPath:path Paramters:param CompleteBlock:postHandleBlock];
}

+ (void)verifyRegisterInfoWithUserName:(NSString *)userName password:(NSString *)password smsCode:(NSString *)smsCode uniqueId:(NSString *)uniqueId succeedBlock:(BlockLoginFunctionSucceed)succeedBlock faildBlock:(BlockLoginFaild)faildBlock
{
    if (!userName.length) {
        [MBProgressHUD showToastMessage:@"请输入手机号"];
        return;
    }
    
    if (!password.length) {
        [MBProgressHUD showToastMessage:@"请输入密码"];
        return;
    }
    
    if (!smsCode.length) {
        [MBProgressHUD showToastMessage:@"请输入短信验证码"];
        return;
    }

    NSString *path = URL_REGISTER_VERIFY;
    
    NSMutableDictionary *param = [NSMutableDictionary dictionary];
    [param setObject:userName forKey:@"userName"];
    [param setObject:smsCode forKey:@"smsCode"];
    [param setObject:[password base64EncodedString] forKey:@"password"];
    
    if (uniqueId.length) {
        [param setObject:uniqueId forKey:@"uniqueId"];
    }

    //请求回调Block
    AppleServerPostCompleteBlock postHandleBlock = ^(BOOL done, id _Nullable object, NSString * _Nullable error) {
        //接口调用成功
        if (done) {
            YDBaseModel *resp = [YDBaseModel modelWithJSON:object];
            
            if (resp.code == 200) {
                if (succeedBlock) {
                    succeedBlock();
                }
            
            } else {
                if (faildBlock) {
                    NSString *msg;
                    if (resp.detailMessage.length) {
                        msg = resp.detailMessage;
                        
                    } else if (resp.message.length) {
                        msg = resp.message;
                        
                    } else {
                        msg = @"注册失败";
                    }
                    faildBlock(resp.code, msg);
                }
            }
            
        } else {
            if (faildBlock) {
                faildBlock(0, @"网络不给力");
            }
        }
    };
    
    //使用AppleServer发送POST请求
    [AppleServer.shared postWithPath:path Paramters:param CompleteBlock:postHandleBlock];
}

+ (void)registerWithUserName:(NSString *)userName password:(NSString *)password smsCode:(NSString *)smsCode uniqueId:(NSString *)uniqueId shopName:(NSString *)shopName succeedBlock:(BlockLoginSucceed)succeedBlock faildBlock:(BlockLoginFaild)faildBlock
{
    if (!userName.length) {
        [MBProgressHUD showToastMessage:@"请输入手机号"];
        return;
    }
    
    if (!smsCode.length) {
        [MBProgressHUD showToastMessage:@"请输入短信验证码"];
        return;
    }
    
    if (!password.length) {
        [MBProgressHUD showToastMessage:@"请输入密码"];
        return;
    }
    
    if (!shopName.length) {
        [MBProgressHUD showToastMessage:@"请输入店铺名"];
        return;
    }
    
    NSString *path = URL_REGISTER_SAVE;
    
    NSMutableDictionary *param = [NSMutableDictionary dictionary];
    [param setObject:userName forKey:@"userName"];
    [param setObject:smsCode forKey:@"smsCode"];
    [param setObject:[[password sha256String] base64EncodedString] forKey:@"password"];
    [param setObject:shopName forKey:@"shopName"];
    
    if (uniqueId.length) {
        [param setObject:uniqueId forKey:@"uniqueId"];
    }
    
    //请求回调Block
    AppleServerPostCompleteBlock postHandleBlock = ^(BOOL done, id _Nullable object, NSString * _Nullable error) {
        //接口调用成功
        if (done) {
            YDLoginModel *resp = [YDLoginModel modelWithJSON:object];
            
            if (resp.code == 200) {
                if (succeedBlock) {
                    succeedBlock(resp.data);
                }
            
            } else {
                if (faildBlock) {
                    NSString *msg;
                    if (resp.detailMessage.length) {
                        msg = resp.detailMessage;
                        
                    } else if (resp.message.length) {
                        msg = resp.message;
                        
                    } else {
                        msg = @"注册失败";
                    }
                    faildBlock(resp.code, msg);
                }
            }
            
        } else {
            if (faildBlock) {
                faildBlock(0, @"网络不给力");
            }
        }
    };
    
    //使用AppleServer发送POST请求
    [AppleServer.shared postWithPath:path Paramters:param CompleteBlock:postHandleBlock];
}

+ (void)verifyWithUserName:(NSString *)userName smsCode:(NSString *)smsCode uniqueId:(NSString *)uniqueId succeedBlock:(BlockLoginFunctionSucceed)succeedBlock faildBlock:(BlockLoginFaild)faildBlock
{
    if (!userName.length) {
        [MBProgressHUD showToastMessage:@"请输入手机号"];
        return;
    }
    
    NSString *path = URL_VERIFY_PHONE;
    
    NSMutableDictionary *param = [NSMutableDictionary dictionary];
    [param setObject:userName forKey:@"userName"];
    
    if (smsCode.length) {
        [param setObject:smsCode forKey:@"smsCode"];
    }
    
    if (uniqueId.length) {
        [param setObject:uniqueId forKey:@"uniqueId"];
    }
    
    //请求回调Block
    AppleServerPostCompleteBlock postHandleBlock = ^(BOOL done, id _Nullable object, NSString * _Nullable error) {
        //接口调用成功
        if (done) {
            YDBaseModel *resp = [YDBaseModel modelWithJSON:object];
            
            if (resp.code == 200) {
                if (succeedBlock) {
                    succeedBlock();
                }
            
            } else {
                if (faildBlock) {
                    NSString *msg;
                    if (resp.detailMessage.length) {
                        msg = resp.detailMessage;
                        
                    } else if (resp.message.length) {
                        msg = resp.message;
                        
                    } else {
                        msg = @"验证失败";
                    }
                    faildBlock(resp.code, msg);
                }
            }
            
        } else {
            if (faildBlock) {
                faildBlock(0, @"网络不给力");
            }
        }
    };
    
    //使用AppleServer发送POST请求
    [AppleServer.shared postWithPath:path Paramters:param CompleteBlock:postHandleBlock];
}

+ (void)resetPasswordWithUserName:(NSString *)userName password:(NSString *)password smsCode:(NSString *)smsCode uniqueId:(NSString *)uniqueId succeedBlock:(BlockLoginFunctionSucceed)succeedBlock faildBlock:(BlockLoginFaild)faildBlock
{
    if (!userName.length) {
        [MBProgressHUD showToastMessage:@"请输入手机号"];
        return;
    }
    
    if (!password.length) {
        [MBProgressHUD showToastMessage:@"请输入密码"];
        return;
    }
    
    NSString *path = URL_RESET_PASSWORD;
    
    NSMutableDictionary *param = [NSMutableDictionary dictionary];
    [param setObject:userName forKey:@"userName"];
    [param setObject:[[password sha256String] base64EncodedString] forKey:@"password"];
    
    if (smsCode.length) {
        [param setObject:smsCode forKey:@"smsCode"];
    }
    
    if (uniqueId.length) {
        [param setObject:uniqueId forKey:@"uniqueId"];
    }
    
    //请求回调Block
    AppleServerPostCompleteBlock postHandleBlock = ^(BOOL done, id _Nullable object, NSString * _Nullable error) {
        //接口调用成功
        if (done) {
            YDBaseModel *resp = [YDBaseModel modelWithJSON:object];
            
            if (resp.code == 200) {
                if (succeedBlock) {
                    succeedBlock();
                }
            
            } else {
                if (faildBlock) {
                    NSString *msg;
                    if (resp.detailMessage.length) {
                        msg = resp.detailMessage;
                        
                    } else if (resp.message.length) {
                        msg = resp.message;
                        
                    } else {
                        msg = @"重设密码失败";
                    }
                    faildBlock(resp.code, msg);
                }
            }
            
        } else {
            if (faildBlock) {
                faildBlock(0, @"网络不给力");
            }
        }
    };
    
    //使用AppleServer发送POST请求
    [AppleServer.shared postWithPath:path Paramters:param CompleteBlock:postHandleBlock];
}

+ (void)sendSMSWithUserName:(NSString *)userName succeedBlock:(BlockLoginSendSMSSucceed)succeedBlock faildBlock:(BlockLoginFaild)faildBlock
{
    if (!userName.length) {
        [MBProgressHUD showToastMessage:@"请输入手机号"];
        return;
    }
   
    NSString *path = URL_SEND_SMS;
    
    NSMutableDictionary *param = [NSMutableDictionary dictionary];
    [param setObject:userName forKey:@"userName"];
    
    //请求回调Block
    AppleServerPostCompleteBlock postHandleBlock = ^(BOOL done, id _Nullable object, NSString * _Nullable error) {
        //接口调用成功
        if (done) {
            YDLoginSendSMSModel *resp = [YDLoginSendSMSModel modelWithJSON:object];
            
            if (resp.code == 200) {
                if (succeedBlock) {
                    succeedBlock(resp.data);
                }
            
            } else {
                if (faildBlock) {
                    NSString *msg;
                    if (resp.detailMessage.length) {
                        msg = resp.detailMessage;
                        
                    } else if (resp.message.length) {
                        msg = resp.message;
                        
                    } else {
                        msg = @"短信发送失败";
                    }
                    faildBlock(resp.code, msg);
                }
            }
            
        } else {
            if (faildBlock) {
                faildBlock(0, @"网络不给力");
            }
        }
    };
    
    //使用AppleServer发送POST请求
    [AppleServer.shared getWithUrl:path Paramters:param Callback:postHandleBlock];
}

+ (void)validateSMSWithUserName:(NSString *)userName smsCode:(NSString *)smsCode uniqueId:(NSString *)uniqueId succeedBlock:(BlockLoginFunctionSucceed)succeedBlock faildBlock:(BlockLoginFaild)faildBlock
{
    if (!userName.length) {
        [MBProgressHUD showToastMessage:@"请输入手机号"];
        return;
    }
    
    if (!smsCode.length || !uniqueId) {
        [MBProgressHUD showToastMessage:@"请输入验证码"];
        return;
    }
    
    NSString *path = URL_VALIDATE_SMS;
    
    NSMutableDictionary *param = [NSMutableDictionary dictionary];
    [param setObject:userName forKey:@"userName"];
    [param setObject:smsCode forKey:@"smsCode"];
    [param setObject:uniqueId forKey:@"uniqueId"];
    
    //请求回调Block
    AppleServerPostCompleteBlock postHandleBlock = ^(BOOL done, id _Nullable object, NSString * _Nullable error) {
        //接口调用成功
        if (done) {
            YDBaseModel *resp = [YDBaseModel modelWithJSON:object];
            
            if (resp.code == 200) {
                if (succeedBlock) {
                    succeedBlock();
                }
            
            } else {
                if (faildBlock) {
                    NSString *msg;
                    if (resp.detailMessage.length) {
                        msg = resp.detailMessage;
                        
                    } else if (resp.message.length) {
                        msg = resp.message;
                        
                    } else {
                        msg = @"校验验证码失败";
                    }
                    faildBlock(resp.code, msg);
                }
            }
            
        } else {
            if (faildBlock) {
                faildBlock(0, @"网络不给力");
            }
        }
    };
    
    //使用AppleServer发送POST请求
    [AppleServer.shared postWithPath:path Paramters:param CompleteBlock:postHandleBlock];
}

@end

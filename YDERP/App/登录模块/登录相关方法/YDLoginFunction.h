//
//  YDLoginFunction.h
//  YDERP
//
//  Created by 何英健 on 2021/7/27.
//

#import <Foundation/Foundation.h>
#import "YDLoginModel.h"
#import "YDLoginSendSMSModel.h"

typedef void(^BlockLoginFunctionSucceed)(void);
typedef void(^BlockLoginSendSMSSucceed)(NSString *uniqueId);
typedef void(^BlockLoginSucceed)(YDLoginData *data);
typedef void(^BlockLoginFaild)(NSInteger code, NSString *error);

@interface YDLoginFunction : NSObject

/**
 登录
 @param userName 用户名/电话号码
 @param password 用户密码
 @param smsCode 收到的短信
 @param uniqueId 发短信时返回的加密字符串
 */
+ (void)loginWithUserName:(NSString *)userName password:(NSString *)password smsCode:(NSString *)smsCode uniqueId:(NSString *)uniqueId succeedBlock:(BlockLoginSucceed)succeedBlock faildBlock:(BlockLoginFaild)faildBlock;

/**
 退出登录
 */
+ (void)logoutWithSucceedBlock:(BlockLoginFunctionSucceed)succeedBlock faildBlock:(BlockLoginFaild)faildBlock;

/**
 验证手机号/验证码/密码合法性(注册前先进行验证),因为下一步会跳转去输入店铺名
 @param userName 用户名/电话号码
 @param password 用户密码
 @param smsCode 收到的短信
 @param uniqueId 发短信时返回的加密字符串
 */
+ (void)verifyRegisterInfoWithUserName:(NSString *)userName password:(NSString *)password smsCode:(NSString *)smsCode uniqueId:(NSString *)uniqueId succeedBlock:(BlockLoginFunctionSucceed)succeedBlock faildBlock:(BlockLoginFaild)faildBlock;

/**
 注册
 @param userName 用户名/电话号码
 @param password 用户密码
 @param smsCode 收到的短信
 @param uniqueId 发短信时返回的加密字符串
 @param shopName 店铺名称
 */
+ (void)registerWithUserName:(NSString *)userName password:(NSString *)password smsCode:(NSString *)smsCode uniqueId:(NSString *)uniqueId shopName:(NSString *)shopName succeedBlock:(BlockLoginSucceed)succeedBlock faildBlock:(BlockLoginFaild)faildBlock;

/**
 验证用户名/手机号（忘记密码流程第一步）
 @param userName 用户名/电话号码
 @param smsCode 收到的短信
 @param uniqueId 发短信时返回的加密字符串
 */
+ (void)verifyWithUserName:(NSString *)userName smsCode:(NSString *)smsCode uniqueId:(NSString *)uniqueId succeedBlock:(BlockLoginFunctionSucceed)succeedBlock faildBlock:(BlockLoginFaild)faildBlock;

/**
 重置密码（忘记密码流程第二步）
 @param userName 用户名/电话号码
 @param password 用户密码
 @param smsCode 收到的短信
 @param uniqueId 发短信时返回的加密字符串
 */
+ (void)resetPasswordWithUserName:(NSString *)userName password:(NSString *)password smsCode:(NSString *)smsCode uniqueId:(NSString *)uniqueId succeedBlock:(BlockLoginFunctionSucceed)succeedBlock faildBlock:(BlockLoginFaild)faildBlock;

/**
 发送短信验证码
 @param userName 用户名/电话号码
 */
+ (void)sendSMSWithUserName:(NSString *)userName succeedBlock:(BlockLoginSendSMSSucceed)succeedBlock faildBlock:(BlockLoginFaild)faildBlock;

/**
 校验验证码
 @param userName 用户名/电话号码
 @param smsCode 收到的短信
 @param uniqueId 发短信时返回的加密字符串
 */
+ (void)validateSMSWithUserName:(NSString *)userName smsCode:(NSString *)smsCode uniqueId:(NSString *)uniqueId succeedBlock:(BlockLoginFunctionSucceed)succeedBlock faildBlock:(BlockLoginFaild)faildBlock;

@end

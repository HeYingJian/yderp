//
//  YDLoginModel.m
//  YDERP
//
//  Created by 何英健 on 2021/7/27.
//

#import "YDLoginModel.h"

@implementation YDLoginData

+ (NSDictionary<NSString *,id> *)modelCustomPropertyMapper
{
    return @{@"ID":@"id"};
}

@end

@implementation YDLoginModel

@end

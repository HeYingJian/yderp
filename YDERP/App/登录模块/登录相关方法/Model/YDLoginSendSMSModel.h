//
//  YDLoginSendSMSModel.h
//  YDERP
//
//  Created by 何英健 on 2021/7/28.
//

#import "YDBaseModel.h"

@interface YDLoginSendSMSModel : YDBaseModel

@property (nonatomic, copy) NSString *data;

@end


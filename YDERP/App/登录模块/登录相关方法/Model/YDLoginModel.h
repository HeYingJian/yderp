//
//  YDLoginModel.h
//  YDERP
//
//  Created by 何英健 on 2021/7/27.
//

#import "YDBaseModel.h"

@interface YDLoginData : NSObject

@property (nonatomic, assign) NSInteger ID;
@property (nonatomic, copy) NSString *userName;
@property (nonatomic, copy) NSString *name;
@property (nonatomic, copy) NSString *headImg;
@property (nonatomic, copy) NSString *phone;
@property (nonatomic, copy) NSString *roleCode;
@property (nonatomic, assign) NSInteger bossId;
@property (nonatomic, assign) NSInteger shopId;
@property (nonatomic, copy) NSString *shopName;
@property (nonatomic, copy) NSString *token;
@property (nonatomic, copy) NSString *roleName;
@property (nonatomic, assign) NSInteger roleId;

@end

@interface YDLoginModel : YDBaseModel

@property (nonatomic, strong) YDLoginData *data;

@end

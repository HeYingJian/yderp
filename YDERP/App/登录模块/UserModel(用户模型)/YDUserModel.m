//
//  TEUserModel.m
//  terasure
//
//  Created by 何英健 on 2019/11/23.
//  Copyright © 2019 何英健. All rights reserved.
//

#import "YDUserModel.h"

@implementation YDUserModel

+ (YDUserModel *)defaultUserModel
{
    static dispatch_once_t once;
    static YDUserModel *instance;
    dispatch_once(&once, ^{
        instance = [[YDUserModel alloc] init];
        instance.loginToken = GetUserDefaultWithKey(STORE_LOGIN_TOKEN);
        instance.ID = GetUserDefaultIntegerWithKey(STORE_USER_ID);
        instance.userName = GetUserDefaultWithKey(STORE_USER_NAME);
        instance.name = GetUserDefaultWithKey(STORE_USER_NICKNAME);
        instance.headImg = GetUserDefaultWithKey(STORE_USER_HEADIMG);
        instance.phone = GetUserDefaultWithKey(STORE_USER_PHONE);
        instance.roleCode = GetUserDefaultWithKey(STORE_USER_ROLECODE);
        instance.roleName = GetUserDefaultWithKey(STORE_USER_ROLENAME);
        instance.roleId = GetUserDefaultIntegerWithKey(STORE_USER_ROLEID);
        instance.bossId = GetUserDefaultIntegerWithKey(STORE_USER_BOSSID);
        instance.shopId = GetUserDefaultIntegerWithKey(STORE_USER_SHOPID);
        instance.shopName = GetUserDefaultWithKey(STORE_USER_SHOPNAME);
        instance.feePlanExpire = GetUserDefaultIntegerWithKey(STORE_USER_FEEPLANEXPIRE);
    });
    return instance;
}

+ (void)setUserModelWithLoginData:(YDLoginData *)loginData
{
    UserModel.loginToken = loginData.token;
    UserModel.ID = loginData.ID;
    UserModel.userName = loginData.userName;
    UserModel.name = loginData.name;
    UserModel.headImg = loginData.headImg;
    UserModel.phone = loginData.phone;
    UserModel.roleCode = loginData.roleCode;
    UserModel.roleName = loginData.roleName;
    UserModel.roleId = loginData.roleId;
    UserModel.bossId = loginData.bossId;
    UserModel.shopId = loginData.shopId;
    UserModel.shopName = loginData.shopName;
}

+ (void)clear
{
    UserModel.loginToken = @"";
    UserModel.ID = 0;
    UserModel.userName = @"";
    UserModel.name = @"";
    UserModel.headImg = @"";
    UserModel.phone = @"";
    UserModel.roleCode = @"";
    UserModel.roleName = @"";
    UserModel.roleId = 0;
    UserModel.bossId = 0;
    UserModel.shopId = 0;
    UserModel.shopName = @"";
    UserModel.feePlanExpire = 1;
}

#pragma mark - 属性设置

- (BOOL)userIsLogin
{
    if (self.loginToken.length) {
        return YES;
        
    } else {
        return NO;
    }
}

- (void)setLoginToken:(NSString *)loginToken
{
    _loginToken = loginToken;
    
    SetUserDefaultKeyWithObject(STORE_LOGIN_TOKEN, loginToken);
    UserDefaultSynchronize;
}

- (void)setID:(NSInteger)ID
{
    _ID = ID;
    
    SetUserIntegerKeyWithObject(STORE_USER_ID, ID);
    UserDefaultSynchronize;
}

- (void)setUserName:(NSString *)userName
{
    _userName = userName;
    
    SetUserDefaultKeyWithObject(STORE_USER_NAME, userName);
    UserDefaultSynchronize;
}

- (void)setName:(NSString *)name
{
    _name = name;
    
    SetUserDefaultKeyWithObject(STORE_USER_NICKNAME, name);
    UserDefaultSynchronize;
}

- (void)setHeadImg:(NSString *)headImg
{
    _headImg = headImg;
    
    SetUserDefaultKeyWithObject(STORE_USER_HEADIMG, headImg);
    UserDefaultSynchronize;
}

- (void)setPhone:(NSString *)phone
{
    _phone = phone;
    
    SetUserDefaultKeyWithObject(STORE_USER_PHONE, phone);
    UserDefaultSynchronize;
}

- (void)setRoleCode:(NSString *)roleCode
{
    _roleCode = roleCode;
    
    SetUserDefaultKeyWithObject(STORE_USER_ROLECODE, roleCode);
    UserDefaultSynchronize;
}

- (void)setRoleName:(NSString *)roleName
{
    _roleName = roleName;
    
    SetUserDefaultKeyWithObject(STORE_USER_ROLENAME, roleName);
    UserDefaultSynchronize;
}

- (void)setRoleId:(NSInteger)roleId
{
    _roleId = roleId;
    
    SetUserIntegerKeyWithObject(STORE_USER_ROLEID, roleId);
    UserDefaultSynchronize;
}

- (void)setBossId:(NSInteger)bossId
{
    _bossId = bossId;
    
    SetUserIntegerKeyWithObject(STORE_USER_BOSSID, bossId);
    UserDefaultSynchronize;
}

- (void)setShopId:(NSInteger)shopId
{
    _shopId = shopId;
    
    SetUserIntegerKeyWithObject(STORE_USER_SHOPID, shopId);
    UserDefaultSynchronize;
}

- (void)setShopName:(NSString *)shopName
{
    _shopName = shopName;
    
    SetUserDefaultKeyWithObject(STORE_USER_SHOPNAME, shopName);
    UserDefaultSynchronize;
}

- (void)setFeePlanExpire:(NSInteger)feePlanExpire
{
    _feePlanExpire = feePlanExpire;
    
    SetUserIntegerKeyWithObject(STORE_USER_FEEPLANEXPIRE, feePlanExpire);
    UserDefaultSynchronize;
}
                
@end

//
//  YDUserModel.h
//  terasure
//
//  Created by 何英健 on 2019/11/23.
//  Copyright © 2019 何英健. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "YDLoginModel.h"

@interface YDUserModel : NSObject

// 是否已登录
@property (nonatomic, assign) BOOL userIsLogin;

@property (nonatomic, copy) NSString *loginToken;
@property (nonatomic, assign) NSInteger ID;
// 登录名（手机号）
@property (nonatomic, copy) NSString *userName;
// 名称
@property (nonatomic, copy) NSString *name;
@property (nonatomic, copy) NSString *headImg;
// 联系电话（非登录时填写的电话）
@property (nonatomic, copy) NSString *phone;
// 角色代码 roleCode = Boss时代表老板
@property (nonatomic, copy) NSString *roleCode;
// 角色名称
@property (nonatomic, copy) NSString *roleName;
// 角色id
@property (nonatomic, assign) NSInteger roleId;
// 老板id，如果值大于0，则表明该用户从属于某个老板
@property (nonatomic, assign) NSInteger bossId;
@property (nonatomic, assign) NSInteger shopId;
@property (nonatomic, copy) NSString *shopName;
// 套餐是否过期，0：未过期；1：已过期
@property (nonatomic, assign) NSInteger feePlanExpire;


// 单例
+ (YDUserModel *)defaultUserModel;

// 从登录模型中写入数据
+ (void)setUserModelWithLoginData:(YDLoginData *)loginData;

// 清空
+ (void)clear;

@end

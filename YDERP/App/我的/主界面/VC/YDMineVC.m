//
//  YDMineVC.m
//  YDERP
//
//  Created by 何英健 on 2021/7/19.
//

#import "YDMineVC.h"
#import "YDMineCell.h"

@interface YDMineVC () <UITableViewDelegate, UITableViewDataSource>

@property (nonatomic, weak) IBOutlet UITableView *tableView;

@property (nonatomic, assign) BOOL isFirstTimeLoading;

@end

@implementation YDMineVC 

#pragma mark - 页面加载

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.isFirstTimeLoading = YES;
    
    [self initUI];
}

- (void)initUI
{
    [self setupTableView];
}

- (void)setupTableView
{
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    self.tableView.showsVerticalScrollIndicator = NO;
    self.tableView.showsHorizontalScrollIndicator = NO;
    
    if (@available(iOS 11.0, *)) {
        self.tableView.contentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentNever;
        
    } else {
        self.automaticallyAdjustsScrollViewInsets = NO;
    }
}

- (void)refreshData
{
    if (self.isFirstTimeLoading) {
        [YDLoadingView showToSuperview:self.view];
    }
    
    YDWeakSelf
    [YDFunction loadUserInfoOnCompletion:^(YDUserInfoData *data) {
        weakSelf.isFirstTimeLoading = NO;
        [YDLoadingView hideToSuperview:weakSelf.view];
        [weakSelf.tableView reloadData];
        
    } failure:^(NSString *msg) {
        weakSelf.isFirstTimeLoading = NO;
        [YDLoadingView hideToSuperview:weakSelf.view];
        [MBProgressHUD showToastMessage:msg];
    }];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    self.navigationController.navigationBar.translucent = YES;
    [self.navigationController setNavigationBarHidden:YES animated:NO];
    
    [self refreshData];
}

#pragma mark - UITableView代理

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return [YDMineCell getHeight];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    YDMineCell *cell = [YDMineCell cellWithTableView:tableView];
    
    return cell;
}

@end

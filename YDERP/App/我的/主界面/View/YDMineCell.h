//
//  YDMineCell.h
//  YDERP
//
//  Created by 何英健 on 2021/7/22.
//

#import <UIKit/UIKit.h>

@interface YDMineCell : UITableViewCell

+ (instancetype)cellWithTableView:(UITableView *)tableView;

+ (CGFloat)getHeight;

@end

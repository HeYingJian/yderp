//
//  YDMineCell.m
//  YDERP
//
//  Created by 何英健 on 2021/7/22.
//

#import "YDMineCell.h"
#import "YDSettingVC.h"
#import "YDPrintSettingVC.h"
#import "YDAccountPackageVC.h"

@interface YDMineCell ()

@property (nonatomic, weak) IBOutlet UIView *BGView;
@property (nonatomic, weak) IBOutlet UIImageView *avatarImgView;
@property (nonatomic, weak) IBOutlet UIImageView *tagImgView;
@property (nonatomic, weak) IBOutlet UILabel *nameLabel;
@property (nonatomic, weak) IBOutlet UILabel *shopLabel;

@property (nonatomic, weak) IBOutlet UIView *staffManagementView;
@property (nonatomic, weak) IBOutlet UIView *rightsManagementView;
@property (nonatomic, weak) IBOutlet UIView *printSettingView;
@property (nonatomic, weak) IBOutlet UIView *settingView;
@property (nonatomic, weak) IBOutlet UIView *accountPackageView;

@property (nonatomic, weak) IBOutlet NSLayoutConstraint *BGViewH;

@end

@implementation YDMineCell

+ (instancetype)cellWithTableView:(UITableView *)tableView
{
    static NSString *reuseID = @"mineCell";
    YDMineCell *cell = [tableView dequeueReusableCellWithIdentifier:reuseID];
    if (!cell) {
        cell = [[NSBundle mainBundle] loadNibNamed:@"YDMineCell" owner:nil options:nil][0];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    
    [cell setupData];
    
    return cell;
}

+ (CGFloat)getHeight
{
    return 600;
}

- (void)awakeFromNib
{
    [super awakeFromNib];
    
    if (IS_PhoneXAll) {
        self.BGViewH.constant = 261;
    }
    
    [self addGesture];
}

- (void)addGesture
{
    UITapGestureRecognizer *tap1 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(showUserSetting)];
    [self.BGView addGestureRecognizer:tap1];
    
    UITapGestureRecognizer *tap2 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(showStaffManagement)];
    [self.staffManagementView addGestureRecognizer:tap2];
    
    UITapGestureRecognizer *tap3 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(showRightsManagement)];
    [self.rightsManagementView addGestureRecognizer:tap3];
    
    UITapGestureRecognizer *tap4 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(showPrintSetting)];
    [self.printSettingView addGestureRecognizer:tap4];
    
    UITapGestureRecognizer *tap5 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(showSetting)];
    [self.settingView addGestureRecognizer:tap5];
    
    UITapGestureRecognizer *tap6 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(showAccountPackage)];
    [self.accountPackageView addGestureRecognizer:tap6];
}

- (void)setupData
{
    [self.avatarImgView appleSetImageWithUrl:UserModel.headImg SprcifyOssImageStyle:@"head_pic" HasHD:NO placeholderImage:@"默认头像" completion:nil];
    self.nameLabel.text = UserModel.name;
    self.shopLabel.text = UserModel.shopName;
    self.tagImgView.hidden = ([UserModel.roleCode isEqualToString:@"BOSS"])? NO: YES;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

#pragma mark - 点击事件

// 编辑个人信息
- (void)showUserSetting
{
    [YDWebViewVC showWebViewWithUrl:WEBURL_USER_INFO isHideNav:YES];
}

// 打开员工管理
- (void)showStaffManagement
{
    if ([YDLimit limitSystem]) {
        [YDWebViewVC showWebViewWithUrl:WEBURL_STAFF_MANAGE isHideNav:YES];
    }
}

// 打开权限管理
- (void)showRightsManagement
{
    if ([YDLimit limitSystem]) {
        [YDWebViewVC showWebViewWithUrl:WEBURL_RIGHTS_MANAGE isHideNav:YES];
    }
}

// 进入打印设置
- (void)showPrintSetting
{
    YDPrintSettingVC *vc = [[YDPrintSettingVC alloc] init];
    vc.hidesBottomBarWhenPushed = YES;
    [[UINavigationController rootNavigationController] pushViewController:vc animated:YES];
}

// 进入设置
- (void)showSetting
{
    YDSettingVC *vc = [[YDSettingVC alloc] init];
    vc.hidesBottomBarWhenPushed = YES;
    [[UINavigationController rootNavigationController] pushViewController:vc animated:YES];
}

// 进入账户套餐
- (void)showAccountPackage
{
    YDAccountPackageVC *vc = [[YDAccountPackageVC alloc] init];
    vc.hidesBottomBarWhenPushed = YES;
    [[UINavigationController rootNavigationController] pushViewController:vc animated:YES];
}

@end

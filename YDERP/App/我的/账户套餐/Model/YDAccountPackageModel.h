//
//  YDAccountPackageModel.h
//  YDERP
//
//  Created by 何英健 on 2021/9/23.
//

#import "YDBaseModel.h"

@interface YDAccountPackageData : NSObject

// 套餐到期时间，yyyy-MM-dd 格式
@property (nonatomic, copy) NSString *feeExpireDate;
// 使用时长剩余天数，如果值小于等于0，前端自行显示『已到期』字样
@property (nonatomic, assign) NSInteger remainDay;
// 付费套餐状态 0-停用 1-启用，如果为停用状态，则 隐藏【到期时间】
@property (nonatomic, assign) NSInteger feeStatus;

@end

@interface YDAccountPackageModel : YDBaseModel

@property (nonatomic, strong) YDAccountPackageData *data;

@end

//
//  YDAccountPackagePopView.h
//  YDERP
//
//  Created by 何英健 on 2021/9/22.
//

#import <UIKit/UIKit.h>

typedef enum : NSUInteger {
    YDAccountPopType1, // 服务咨询
    YDAccountPopType2, // 套餐已到期
    YDAccountPopType3 // 商户已被禁用
} YDAccountPopType;

@interface YDAccountPackagePopView : UIView

+ (void)showWithType:(YDAccountPopType)type;

@end

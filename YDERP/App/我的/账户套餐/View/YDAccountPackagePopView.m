//
//  YDAccountPackagePopView.m
//  YDERP
//
//  Created by 何英健 on 2021/9/22.
//

#import "YDAccountPackagePopView.h"

@interface YDAccountPackagePopView ()

@property (nonatomic, weak) IBOutlet UIView *view1;
@property (nonatomic, weak) IBOutlet UILabel *cellphone;
@property (nonatomic, weak) IBOutlet UILabel *telephone;

@property (nonatomic, weak) IBOutlet UIView *view2;
@property (nonatomic, weak) IBOutlet UIButton *knowBtn;

@property (nonatomic, weak) IBOutlet UIView *view3;
@property (nonatomic, weak) IBOutlet UIButton *confirmBtn;

@end

@implementation YDAccountPackagePopView

+ (void)showWithType:(YDAccountPopType)type
{
    YDAccountPackagePopView *view = [[NSBundle mainBundle] loadNibNamed:@"YDAccountPackagePopView" owner:nil options:nil][0];
    view.frame = CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT);
    
    [view initUI];
    
    switch (type) {
        case YDAccountPopType1:
        {
            view.view2.hidden = YES;
            view.view3.hidden = YES;
        }
            break;
            
        case YDAccountPopType2:
        {
            view.view1.hidden = YES;
            view.view3.hidden = YES;
        }
            break;
            
        case YDAccountPopType3:
        {
            view.view1.hidden = YES;
            view.view2.hidden = YES;
        }
            break;
            
        default:
            break;
    }

    [KeyWindow addSubview:view];
}

- (void)initUI
{
    self.cellphone.font = FONT_NUMBER(17);
    self.telephone.font = FONT_NUMBER(17);
    self.knowBtn.layer.borderColor = ColorFromRGB(0xDBDDE0).CGColor;
    self.knowBtn.layer.borderWidth = 1;
    self.confirmBtn.layer.borderColor = ColorFromRGB(0xDBDDE0).CGColor;
    self.confirmBtn.layer.borderWidth = 1;
}

- (IBAction)tapCloseBtn:(id)sender
{
    [self removeFromSuperview];
}

- (IBAction)tapCellphoneBtn:(id)sender
{
    NSString *phone = [NSString stringWithFormat:@"telprompt://%@", self.cellphone.text];
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:phone] options:@{} completionHandler:nil];
}

- (IBAction)tapTelephoneBtn:(id)sender
{
    NSString *phone = [NSString stringWithFormat:@"telprompt://%@", self.telephone.text];
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:phone] options:@{} completionHandler:nil];
}

- (IBAction)tapKnowBtn:(id)sender
{
    [self removeFromSuperview];
}

- (IBAction)tapAddTimeBtn:(id)sender
{
    self.view1.hidden = NO;
    self.view2.hidden = YES;
    self.view3.hidden = YES;
}

- (IBAction)tapConfirmBtn:(id)sender
{
    [self removeFromSuperview];
}

@end

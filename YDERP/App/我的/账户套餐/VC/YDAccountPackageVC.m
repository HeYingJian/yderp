//
//  YDAccountPackageVC.m
//  YDERP
//
//  Created by 何英健 on 2021/9/22.
//

#import "YDAccountPackageVC.h"
#import "YDAccountPackageModel.h"

@interface YDAccountPackageVC ()

@property (nonatomic, weak) IBOutlet UILabel *expirationLabel;
@property (nonatomic, weak) IBOutlet UILabel *remainDayLabel;

@property (nonatomic, weak) IBOutlet NSLayoutConstraint *mainViewH;
@property (nonatomic, weak) IBOutlet NSLayoutConstraint *expirationLabelH;
@property (nonatomic, weak) IBOutlet NSLayoutConstraint *expirationLabelBottom;

@end

@implementation YDAccountPackageVC

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self initUI];
    
    [self refreshData];
}

- (void)initUI
{
    self.title = @"账户套餐";
    [self.navigationItem addDefaultBackButton:self actionMethod:@selector(goBack)];
    
    self.expirationLabel.font = FONT_NUMBER(18);
    self.remainDayLabel.font = FONT_NUMBER(18);
}

- (void)goBack
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)refreshData
{
    NSString *path = URL_ACCOUNT_PACKAGE;
    NSMutableDictionary *param = [NSMutableDictionary dictionary];

    YDWeakSelf
    AppleServerPostCompleteBlock postHandleBlock = ^(BOOL done, id _Nullable object, NSString * _Nullable error) {
        if (done) {
            YDAccountPackageModel *resp = [YDAccountPackageModel modelWithJSON:object];

            if (resp.code == 200) {
                if (resp.data.feeStatus == 0) {
                    weakSelf.expirationLabel.hidden = YES;
                    weakSelf.expirationLabelH.constant = 0;
                    weakSelf.expirationLabelBottom.constant = 0;
                    weakSelf.mainViewH.constant = 154;
                    [weakSelf.view layoutIfNeeded];
                    
                } else {
                    weakSelf.expirationLabel.text = resp.data.feeExpireDate;
                }
                
                NSString *remainDayStr;
                if (resp.data.remainDay > 0) {
                    remainDayStr = [NSString stringWithFormat:@"%ld天", resp.data.remainDay];
                    weakSelf.remainDayLabel.textColor = ColorFromRGB(0x222222);
                    
                } else {
                    remainDayStr = @"已到期";
                    weakSelf.remainDayLabel.textColor = ColorFromRGB(0xFF5630);
                }
                weakSelf.remainDayLabel.text = remainDayStr;

            } else {
                NSString *msg;
                if (resp.detailMessage.length) {
                    msg = resp.detailMessage;
                    
                } else if (resp.message.length) {
                    msg = resp.message;
                    
                } else {
                    msg = @"网络失败";
                }
                [MBProgressHUD showToastMessage:msg];
            }
            
        } else {
            [MBProgressHUD showToastMessage:@"网络不给力"];
        }
    };
    
    [AppleServer.shared getWithUrl:path Paramters:param Callback:postHandleBlock];
}

- (IBAction)tapRenewBtn:(id)sender
{
    [YDAccountPackagePopView showWithType:YDAccountPopType1];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [self.navigationController setNavigationBarHidden:NO animated:YES];
}

@end

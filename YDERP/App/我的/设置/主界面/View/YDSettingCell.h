//
//  YDSettingCell.h
//  YDERP
//
//  Created by 何英健 on 2021/7/22.
//

#import <UIKit/UIKit.h>

@interface YDSettingCell : UITableViewCell

@property (nonatomic, assign) BOOL showLogout;
@property (nonatomic, copy) NSString *title;
@property (nonatomic, copy) NSString *content;

+ (instancetype)cellWithTableView:(UITableView *)tableView index:(NSInteger)index;

+ (CGFloat)getHeight;

@end

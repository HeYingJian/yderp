//
//  YDSettingCell.m
//  YDERP
//
//  Created by 何英健 on 2021/7/22.
//

#import "YDSettingCell.h"

@interface YDSettingCell ()

@property (nonatomic, weak) IBOutlet UILabel *titleLabel;
@property (nonatomic, weak) IBOutlet UIImageView *arrowImgView;
@property (nonatomic, weak) IBOutlet UILabel *contentLabel;
@property (nonatomic, weak) IBOutlet UILabel *logoutLabel;
@property (nonatomic, weak) IBOutlet UIView *lineView;

@end

@implementation YDSettingCell

+ (instancetype)cellWithTableView:(UITableView *)tableView index:(NSInteger)index
{
    static NSString *reuseID = @"settingCell";
    YDSettingCell *cell = [tableView dequeueReusableCellWithIdentifier:reuseID];
    if (!cell) {
        cell = [[NSBundle mainBundle] loadNibNamed:@"YDSettingCell" owner:nil options:nil][0];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    
    cell.lineView.hidden = (index == 0)? YES : NO;
    
    return cell;
}

+ (CGFloat)getHeight
{
    return 70;
}

- (void)setShowLogout:(BOOL)showLogout
{
    _showLogout = showLogout;
    
    self.logoutLabel.hidden = !showLogout;
    self.titleLabel.hidden = showLogout;
    self.arrowImgView.hidden = showLogout;
    self.contentLabel.hidden = showLogout;
}

- (void)setTitle:(NSString *)title
{
    _title = title;
    
    self.logoutLabel.hidden = YES;
    self.contentLabel.hidden = YES;
    self.arrowImgView.hidden = NO;
    self.titleLabel.hidden = NO;
    self.titleLabel.text = title;
}

- (void)setContent:(NSString *)content
{
    _content = content;
    
    self.arrowImgView.hidden = YES;
    self.contentLabel.hidden = NO;
    self.contentLabel.text = content;
}

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end

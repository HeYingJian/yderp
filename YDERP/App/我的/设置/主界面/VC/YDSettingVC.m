//
//  YDSettingVC.m
//  YDERP
//
//  Created by 何英健 on 2021/7/22.
//

#import "YDSettingVC.h"
#import "YDSettingCell.h"
#import "YDClearCacheTool.h"
#import "YDAccountSecurityVC.h"

@interface YDSettingVC () <UITableViewDelegate, UITableViewDataSource>

@property (nonatomic, weak) IBOutlet UITableView *tableView;

@end

@implementation YDSettingVC

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self initUI];
}

- (void)initUI
{
    self.title = @"设置";
    [self.navigationItem addDefaultBackButton:self];
    
    [self setupTableView];
}

- (void)setupTableView
{
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    self.tableView.showsVerticalScrollIndicator = NO;
    self.tableView.showsHorizontalScrollIndicator = NO;
    
    if (@available(iOS 11.0, *)) {
        self.tableView.contentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentNever;
        
    } else {
        self.automaticallyAdjustsScrollViewInsets = NO;
    }
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [self.navigationController setNavigationBarHidden:NO animated:YES];
}

#pragma mark - UITableView代理

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 3;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    switch (section) {
        case 0:
            return 4;
        case 1:
            return 2;
            
        default:
            break;
    }
    
    return 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return [YDSettingCell getHeight];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    YDSettingCell *cell = [YDSettingCell cellWithTableView:tableView index:indexPath.row];
    
    if (indexPath.section == 0) {
        switch (indexPath.row) {
            case 0:
            {
                cell.title = @"账号安全";
            }
                break;
                
            case 1:
            {
                cell.title = @"隐私政策";
            }
                break;
                
            case 2:
            {
                cell.title = @"用户协议";
            }
                break;
                
            case 3:
            {
                cell.title = @"隐私设置";
            }
                break;
                
            default:
                break;
        }
        
    } else if (indexPath.section == 1) {
        if (indexPath.row == 0) {
            cell.title = @"当前版本";
            NSString *key = @"CFBundleShortVersionString";
            NSString *currentVersion = [NSBundle mainBundle].infoDictionary[key];
            cell.content = [NSString stringWithFormat:@"版本%@", currentVersion];
            
        } else {
            cell.title = @"清空缓存";
            NSString *cachesPath = NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES)[0];
            cell.content = [NSString stringWithFormat:@"%@", [YDClearCacheTool getCacheSizeWithFilePath:cachesPath]];
        }
        
    } else {
        cell.showLogout = YES;
    }
    
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 10;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    return [[UIView alloc] init];
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == 0) {
        switch (indexPath.row) {
            // 账号安全
            case 0:
            {
                [self showAccountSecurity];
            }
                break;
            // 隐私政策
            case 1:
            {
                [self showPrivacyPolicy];
            }
                break;
            // 用户协议
            case 2:
            {
                [self showuserAgreement];
            }
                break;
            // 隐私设置
            case 3:
            {
                [self showPrivacySetting];
            }
                break;
                
            default:
                break;
        }
        
    } else if (indexPath.section == 1) {
        if (indexPath.row == 1) {
            [self clearCache];
        }
        
    } else {
        // 退出登录
        [self logout];
    }
}

// 账号安全
- (void)showAccountSecurity
{
    YDAccountSecurityVC *vc = [[YDAccountSecurityVC alloc] init];
    [[UINavigationController rootNavigationController] pushViewController:vc animated:YES];
}

// 隐私政策
- (void)showPrivacyPolicy
{
    [YDWebViewVC showWebViewWithUrl:WEBURL_PRIVACY isHideNav:YES];
}

// 用户协议
- (void)showuserAgreement
{
    [YDWebViewVC showWebViewWithUrl:WEBURL_SERVICE isHideNav:YES];
}

// 隐私设置
- (void)showPrivacySetting
{
    NSURL *url = [NSURL URLWithString:UIApplicationOpenSettingsURLString];
    if ([[UIApplication sharedApplication] canOpenURL:url]) {
        NSURL *url = [NSURL URLWithString:UIApplicationOpenSettingsURLString];
        [[UIApplication sharedApplication] openURL:url options:@{} completionHandler:nil];
    }
}

// 清理缓存
- (void)clearCache
{
    NSString *cachesPath = NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES)[0];
    [YDClearCacheTool clearCache:cachesPath completion:^{
        [MBProgressHUD showToastMessage:@"清空缓存成功"];
        [self.tableView reloadData];
    }];
}

- (void)logout
{
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"确认退出登录吗？" message:nil preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *confirm = [UIAlertAction actionWithTitle:@"确定" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        [YDFunction logout];
    }];
    UIAlertAction* cancel = [UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleCancel handler:nil];

    [alert addAction:cancel];
    [alert addAction:confirm];

    [self.navigationController presentViewController:alert animated:YES completion:nil];
}

@end

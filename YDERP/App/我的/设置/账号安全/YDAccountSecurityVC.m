//
//  YDAccountSecurityVC.m
//  YDERP
//
//  Created by 何英健 on 2021/7/26.
//

#import "YDAccountSecurityVC.h"
#import "YDSettingCell.h"
#import "YDVerifyPhoneVC.h"

@interface YDAccountSecurityVC () <UITableViewDelegate, UITableViewDataSource>

@property (nonatomic, weak) IBOutlet UITableView *tableView;

@end

@implementation YDAccountSecurityVC

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self initUI];
}

- (void)initUI
{
    self.title = @"账户安全";
    [self.navigationItem addDefaultBackButton:self];
    
    [self setupTableView];
}

- (void)setupTableView
{
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    self.tableView.showsVerticalScrollIndicator = NO;
    self.tableView.showsHorizontalScrollIndicator = NO;
    
    if (@available(iOS 11.0, *)) {
        self.tableView.contentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentNever;
        
    } else {
        self.automaticallyAdjustsScrollViewInsets = NO;
    }
}

#pragma mark - UITableView代理

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 2;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return [YDSettingCell getHeight];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    YDSettingCell *cell = [YDSettingCell cellWithTableView:tableView index:indexPath.row];
    
    switch (indexPath.row) {
        case 0:
        {
            cell.title = @"修改密码";
        }
            break;
            
        case 1:
        {
            cell.title = @"注销服务";
        }
            break;
        
        default:
            break;
    }
    
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 10;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    return [[UIView alloc] init];
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    switch (indexPath.row) {
        // 修改密码
        case 0:
        {
            [self changePassword];
        }
            break;
        // 注销服务
        case 1:
        {
            [self showWithdrawNotice];
        }
            break;
        default:
            break;
    }
}

- (void)changePassword
{
    YDVerifyPhoneVC *vc = [[YDVerifyPhoneVC alloc] init];
    vc.type = YDResetPasswordTypeSetting;
    [self.navigationController pushViewController:vc animated:YES];
}

- (void)showWithdrawNotice
{
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:nil message:@"如需注销您的账号，请您联系客服，客服电话020-37738777" preferredStyle:UIAlertControllerStyleAlert];
    
        UIAlertAction *otherAction = [UIAlertAction actionWithTitle:@"知道了" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            
        }];
    
        [alertController addAction:otherAction];

        [self presentViewController:alertController animated:YES completion:nil];
}

@end

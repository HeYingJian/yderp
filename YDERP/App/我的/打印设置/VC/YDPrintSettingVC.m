//
//  YDPrintSettingVC.m
//  YDERP
//
//  Created by 何英健 on 2021/8/29.
//

#import "YDPrintSettingVC.h"
#import "YDPrintMouldCell.h"
#import "YDPrintDeviceCell.h"
#import "YDPrintBluetoothCell.h"

@interface YDPrintSettingVC () <UITableViewDelegate, UITableViewDataSource>

@property (nonatomic, assign) YDPrintSettingType type;
@property (nonatomic, strong) NSArray<PTPrinter *> *printerArray;

@property (nonatomic, weak) IBOutlet UITableView *tableView;

@end

@implementation YDPrintSettingVC

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self initUI];
    
    [self addNotification];
}

- (void)initUI
{
    self.title = @"打印设置";
    [self.navigationItem addDefaultBackButton:self actionMethod:@selector(goBack)];
    
    [self setupTableView];
}

- (void)goBack
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)setupTableView
{
    self.tableView.hidden = YES;
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    self.tableView.showsVerticalScrollIndicator = NO;
    self.tableView.showsHorizontalScrollIndicator = NO;
    
    if (@available(iOS 11.0, *)) {
        self.tableView.contentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentNever;
        
    } else {
        self.automaticallyAdjustsScrollViewInsets = NO;
    }
}

- (void)addNotification
{
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(refreshTableView) name:PRINTER_CONNECT_REFRESH object:nil];
}

- (void)refreshUI
{
    if ([[PTDispatcher share] getBluetoothStatus] == PTBluetoothStatePoweredOn) {
        self.type = YDPrintSettingTypeNormal;
        
    } else if ([[PTDispatcher share] getBluetoothStatus] == PTBluetoothStatePoweredOff) {
        self.type = YDPrintSettingTypeBluetoothClosed;
        
    } else {
        self.type = YDPrintSettingTypeBluetoothUnauthorized;
    }
    
    if (self.type == YDPrintSettingTypeNormal) {
        YDWeakSelf
        [YDPrintModel getDeviceOnCompletion:^(NSArray<PTPrinter *> *printerArray) {
            weakSelf.printerArray = printerArray;
            [weakSelf refreshTableView];
        }];
        
    } else {
        self.printerArray = nil;
        [self refreshTableView];
    }
}

- (void)refreshTableView
{
    self.tableView.hidden = NO;
    [self.tableView reloadData];
    [self refreshFooterView];
}

- (void)refreshFooterView
{
    if (self.type == YDPrintSettingTypeNormal) {
        UIView *footerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, 110)];
        
        UILabel *titleLabel = [[UILabel alloc] init];
        titleLabel.font = [UIFont systemFontOfSize:13.0f];
        titleLabel.textColor = ColorFromRGB(0x999999);
        titleLabel.text = @"操作说明";
        [titleLabel sizeToFit];
        [titleLabel setOrigin:CGPointMake(24, 20)];
        [footerView addSubview:titleLabel];
        
        UILabel *contentLabel = [[UILabel alloc] init];
        contentLabel.font = [UIFont systemFontOfSize:12.0f];
        contentLabel.textColor = ColorFromRGB(0xB7B7B7);
        contentLabel.numberOfLines = 0;
        [contentLabel setOrigin:CGPointMake(24, CGRectGetMaxY(titleLabel.frame) + 6)];
        [contentLabel setWidth:SCREEN_WIDTH - 24 * 2];
        [contentLabel setHeight:90];
        NSString *str = @"1、点击“搜索打印机”获取附近的蓝牙设备信息\n2、点击“未连接”的设备,完成连接\n3、点击打印后,如未能正常打印,请等待10s后再次进行打印\n4、有疑问可拨打020-37738777";
        NSMutableAttributedString *attributedStr = [[NSMutableAttributedString alloc] initWithString:str];
        NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc] init];
        [paragraphStyle setLineSpacing:5];
        [attributedStr addAttribute:NSParagraphStyleAttributeName value:paragraphStyle range:NSMakeRange(0, [str length])];
        contentLabel.attributedText = attributedStr;
        contentLabel.lineBreakMode = NSLineBreakByTruncatingTail;
        [footerView addSubview:contentLabel];
        
        self.tableView.tableFooterView = footerView;
        
    } else {
        self.tableView.tableFooterView = nil;
    }
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [self.navigationController setNavigationBarHidden:NO animated:YES];
    
    [self refreshUI];
}

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

#pragma mark - UITableView代理

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 2;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (section == 1 && self.type == YDPrintSettingTypeNormal) {
        return self.printerArray.count;
    }
    
    return 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == 0) {
        return [YDPrintMouldCell getHeight];
        
    } else if (indexPath.section == 1 && self.type == YDPrintSettingTypeNormal) {
        return [YDPrintDeviceCell getHeight];
        
    } else {
        return [YDPrintBluetoothCell getHeightWithTableView:tableView];
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == 0) {
        YDPrintMouldCell *cell = [YDPrintMouldCell cellWithTableView:tableView];
        return cell;
        
    } else if (indexPath.section == 1 && self.type == YDPrintSettingTypeNormal) {
        YDPrintDeviceCell *cell = [YDPrintDeviceCell cellWithTableView:tableView index:indexPath.row];
        cell.printer = self.printerArray[indexPath.row];
        return cell;
        
    } else {
        YDPrintBluetoothCell *cell = [YDPrintBluetoothCell cellWithTableView:tableView];
        cell.type = self.type;
        YDWeakSelf
        cell.refreshBlock = ^{
            [weakSelf refreshUI];
        };
        return cell;
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 40;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UIView *view = [[UIView alloc] init];
    
    UILabel *titleLabel = [[UILabel alloc] init];
    titleLabel.font = [UIFont systemFontOfSize:13 weight:UIFontWeightMedium];
    titleLabel.textColor = ColorFromRGB(0x999999);
    NSString *str;
    switch (section) {
        case 0:
            str = @"选择小票宽度";
            break;
            
        case 1:
            str = @"搜索设备";
            break;

        default:
            break;
    }
    titleLabel.text = str;
    [titleLabel sizeToFit];
    [titleLabel setX:24];
    [titleLabel setY:(40 - titleLabel.height) / 2];
    [view addSubview:titleLabel];
    
    return view;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    return 0.01;
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section
{
    return [[UIView alloc] init];
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == 1 && self.type == YDPrintSettingTypeNormal) {
        // 连接
        [YDPrintModel connectPrinter:self.printerArray[indexPath.row] completion:^{
            [MBProgressHUD showToastMessage:@"连接成功"];
        }];
    }
}

@end

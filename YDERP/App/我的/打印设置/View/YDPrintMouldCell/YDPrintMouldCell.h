//
//  YDPrintMouldCell.h
//  YDERP
//
//  Created by 何英健 on 2021/8/30.
//

#import <UIKit/UIKit.h>

@interface YDPrintMouldCell : UITableViewCell

+ (instancetype)cellWithTableView:(UITableView *)tableView;

+ (CGFloat)getHeight;

@end

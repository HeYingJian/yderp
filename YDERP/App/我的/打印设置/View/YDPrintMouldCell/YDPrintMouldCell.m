//
//  YDPrintMouldCell.m
//  YDERP
//
//  Created by 何英健 on 2021/8/30.
//

#import "YDPrintMouldCell.h"

@interface YDPrintMouldCell () <WDImageBrowserDelegate>

@end

@implementation YDPrintMouldCell

+ (instancetype)cellWithTableView:(UITableView *)tableView
{
    static NSString *reuseID = @"printMouldCell";
    YDPrintMouldCell *cell = [tableView dequeueReusableCellWithIdentifier:reuseID];
    if (!cell) {
        cell = [[NSBundle mainBundle] loadNibNamed:@"YDPrintMouldCell" owner:nil options:nil][0];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
    }

    return cell;
}

+ (CGFloat)getHeight
{
    return 66;
}

- (IBAction)tapMouldBtn:(id)sender
{
    // 浏览图片相册
    WDImageBrowser *browser = [[WDImageBrowser alloc] init];
    [browser setupWithDelegate:self tappedIndex:0 images:[NSMutableArray arrayWithObject:[UIImage imageNamed:@"打印模版"]] originView:(UIButton *)sender];
    [browser showBrowser];
}

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end

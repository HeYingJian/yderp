//
//  YDPrintBluetoothCell.h
//  YDERP
//
//  Created by 何英健 on 2021/8/30.
//

#import <UIKit/UIKit.h>

typedef enum : NSUInteger {
    YDPrintSettingTypeNormal, //正常使用状态
    YDPrintSettingTypeBluetoothUnauthorized, // 蓝牙未授权
    YDPrintSettingTypeBluetoothClosed // 蓝牙未打开
} YDPrintSettingType;

typedef void(^BlockPrinterRefresh)(void);

@interface YDPrintBluetoothCell : UITableViewCell

+ (instancetype)cellWithTableView:(UITableView *)tableView;

+ (CGFloat)getHeightWithTableView:(UITableView *)tableView;

@property (nonatomic, assign) YDPrintSettingType type;
@property (nonatomic, copy) BlockPrinterRefresh refreshBlock;

@end


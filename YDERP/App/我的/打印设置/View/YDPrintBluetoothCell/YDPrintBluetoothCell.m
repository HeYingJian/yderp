//
//  YDPrintBluetoothCell.m
//  YDERP
//
//  Created by 何英健 on 2021/8/30.
//

#import "YDPrintBluetoothCell.h"

@interface YDPrintBluetoothCell ()

@property (nonatomic, weak) IBOutlet UILabel *contentLabel;
@property (nonatomic, weak) IBOutlet UIButton *btn;

@end

@implementation YDPrintBluetoothCell

+ (instancetype)cellWithTableView:(UITableView *)tableView
{
    static NSString *reuseID = @"printBluetoothCell";
    YDPrintBluetoothCell *cell = [tableView dequeueReusableCellWithIdentifier:reuseID];
    if (!cell) {
        cell = [[NSBundle mainBundle] loadNibNamed:@"YDPrintBluetoothCell" owner:nil options:nil][0];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
    }

    return cell;
}

+ (CGFloat)getHeightWithTableView:(UITableView *)tableView
{
    return tableView.height - 146;
}

- (void)setType:(YDPrintSettingType)type
{
    _type = type;
    
    if (type == YDPrintSettingTypeBluetoothClosed) {
        self.contentLabel.text = @"请打开手机蓝牙";
        [self.btn setTitle:@"我已开启蓝牙" forState:UIControlStateNormal];
        
    } else if (type == YDPrintSettingTypeBluetoothUnauthorized) {
        self.contentLabel.text = @"请打开APP蓝牙权限";
        [self.btn setTitle:@"打开蓝牙设置" forState:UIControlStateNormal];
    }
}

- (IBAction)tapJumpBtn:(id)sender
{
    if (self.type == YDPrintSettingTypeBluetoothClosed) {
        [YDLoadingView showToSuperview:nil];
        
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            [YDLoadingView hideToSuperview:nil];
            
            if (self.refreshBlock) {
                self.refreshBlock();
            }
        });
        
    } else if (self.type == YDPrintSettingTypeBluetoothUnauthorized) {
        NSURL *url = [NSURL URLWithString:UIApplicationOpenSettingsURLString];
        if ([[UIApplication sharedApplication] canOpenURL:url]) {
            NSURL *url = [NSURL URLWithString:UIApplicationOpenSettingsURLString];
            [[UIApplication sharedApplication] openURL:url options:@{} completionHandler:nil];
        }
    }
}

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end

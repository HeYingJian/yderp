//
//  YDPrintDeviceCell.h
//  YDERP
//
//  Created by 何英健 on 2021/8/30.
//

#import <UIKit/UIKit.h>

@interface YDPrintDeviceCell : UITableViewCell

+ (instancetype)cellWithTableView:(UITableView *)tableView index:(NSInteger)index;

+ (CGFloat)getHeight;

@property (nonatomic, strong) PTPrinter *printer;

@end

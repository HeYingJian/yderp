//
//  YDPrintDeviceCell.m
//  YDERP
//
//  Created by 何英健 on 2021/8/30.
//

#import "YDPrintDeviceCell.h"

@interface YDPrintDeviceCell ()

@property (nonatomic, weak) IBOutlet UILabel *nameLabel;
@property (nonatomic, weak) IBOutlet UILabel *statusLabel;
@property (nonatomic, weak) IBOutlet UIView *lineView;

@end

@implementation YDPrintDeviceCell

+ (instancetype)cellWithTableView:(UITableView *)tableView index:(NSInteger)index
{
    static NSString *reuseID = @"printDeviceCell";
    YDPrintDeviceCell *cell = [tableView dequeueReusableCellWithIdentifier:reuseID];
    if (!cell) {
        cell = [[NSBundle mainBundle] loadNibNamed:@"YDPrintDeviceCell" owner:nil options:nil][0];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    
    cell.lineView.hidden = (index == 0)? YES : NO;

    return cell;
}

+ (CGFloat)getHeight
{
    return 75;
}

- (void)setPrinter:(PTPrinter *)printer
{
    _printer = printer;
    
    self.nameLabel.text = printer.name;
    PTPrinter *connectedPrinter = [YDPrintModel getCurrentPrinter];
    self.statusLabel.hidden = ([printer.name isEqualToString:connectedPrinter.name])? NO : YES;
}

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end

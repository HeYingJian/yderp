//
//  YDCustomerVC.m
//  YDERP
//
//  Created by 何英健 on 2021/7/19.
//

#import "YDCustomerVC.h"
#import "YDSelectView.h"
#import "YDSearcher.h"
#import "YDCustomerManageCell.h"

@interface YDCustomerVC () <UITextFieldDelegate, UITableViewDelegate, UITableViewDataSource>

// 搜索器
@property (nonatomic, strong) YDSearcher *searcher;

@property (nonatomic, weak) IBOutlet UITextField *searchTextField;

@property (nonatomic, weak) IBOutlet UIView *tagBGView;
@property (nonatomic, strong) YDSelectView *tagView;

@property (nonatomic, weak) IBOutlet UITableView *tableView;

// 刷新标记
@property (nonatomic, assign) BOOL isNeedRefresh;

@end

@implementation YDCustomerVC

- (YDSearcher *)searcher
{
    if (!_searcher) {
        _searcher = [YDSearcher createWithType:YDSearchTypeCustomer];
    }
    
    return _searcher;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self initUI];
}

- (void)initUI
{
    [self setupTagView];
    [self setupTableView];
}

- (void)setupTableView
{
    self.tableView.hidden = YES;
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    self.tableView.showsVerticalScrollIndicator = NO;
    self.tableView.showsHorizontalScrollIndicator = NO;
    
    if (@available(iOS 11.0, *)) {
        self.tableView.contentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentNever;
        
    } else {
        self.automaticallyAdjustsScrollViewInsets = NO;
    }
    
    UIView *tableHeaderView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, 11)];
    self.tableView.tableHeaderView = tableHeaderView;
    
    self.tableView.mj_header = [YDRefreshHeader headerWithRefreshingTarget:self refreshingAction:@selector(pullingRefresh)];
    
    self.tableView.mj_footer = [YDRefreshFooter footerWithRefreshingTarget:self refreshingAction:@selector(loadMore)];
}

- (void)refreshDataWithNotification:(NSNotification *)notification
{
    [self refreshData:NO];
}

- (IBAction)tapAddCustomerBtn:(id)sender
{
    self.isNeedRefresh = YES;
    [YDWebViewVC showWebViewWithUrl:WEBURL_ADD_CUSTOMER isHideNav:YES];
}

- (void)dismissKeybord
{
    [self.searchTextField resignFirstResponder];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    self.navigationController.navigationBar.translucent = YES;
    [self.navigationController setNavigationBarHidden:YES animated:NO];
    
    if (self.isNeedRefresh) {
        self.isNeedRefresh = NO;
        [self refreshData:YES];
    }
}

#pragma mark - 数据请求

- (void)pullingRefresh
{
    [self refreshData:NO];
}

- (void)refreshData:(BOOL)showLoading
{
    [self dismissKeybord];
    
    if (showLoading) {
        [YDLoadingView showToSuperview:self.view];
    }
    
    YDWeakSelf
    if (!self.searcher.completion) {
        self.searcher.completion = ^(BaseVMRefreshType type) {
            weakSelf.tableView.hidden = NO;
            [YDLoadingView hideToSuperview:weakSelf.view];
            [weakSelf.tableView.mj_header endRefreshing];

            if (type == BaseVMRefreshTypeNoMore) {
                [weakSelf.tableView.mj_footer endRefreshingWithNoMoreData];
                
            } else {
                [weakSelf.tableView.mj_footer endRefreshing];
            }
            
            [weakSelf.tableView reloadData];
        };
    }
    
    if (!self.searcher.faildBlock) {
        self.searcher.faildBlock = ^(NSInteger code, NSString *error) {
            weakSelf.tableView.hidden = NO;
            [YDLoadingView hideToSuperview:weakSelf.view];
            [weakSelf.tableView.mj_header endRefreshing];
            [weakSelf.tableView.mj_footer endRefreshing];
            [MBProgressHUD showToastMessage:error];
        };
    }
    
    [self setSearchCondiction];
    [self.searcher refreshData];
}

- (void)loadMore
{
    [self.searcher loadMoreData];
}

#pragma mark - 搜索条件

- (void)setupTagView
{
    NSMutableArray *modelArray = [NSMutableArray array];
    
    for (int i = 0; i < 2; i++) {
        YDSelectViewModel *model = [[YDSelectViewModel alloc] init];
        model.tagWidth = SCREEN_WIDTH / 3;
        
        switch (i) {
            case 0:
            {
                model.title = @"默认";
            }
                break;
                
            case 1:
            {
                model.title = @"最近交易";
                model.type = 0;
                [model createImgViewModelWithSelectedImg:@"筛选下箭头选中" unselectedImg:@"筛选下箭头未选中"];
                [model createImgViewModelWithSelectedImg:@"筛选上箭头选中" unselectedImg:@"筛选上箭头未选中"];
            }
                break;

            default:
                break;
        }
        [modelArray addObject:model];
    }
    
    YDWeakSelf
    self.tagView = [YDSelectView createViewWithType:YDSelectFilterTypeCustomer modelArray:modelArray showFilterBlock:^{
        [weakSelf dismissKeybord];
        
    } refreshBlock:^{
        [weakSelf refreshData:YES];
    }];
    [self.tagBGView addSubview:self.tagView];
}

// 设置搜索模型，设置对应搜索条件
- (void)setSearchCondiction
{
    self.searcher.keyword = self.searchTextField.text;
    
    for (int i = 0; i < self.tagView.modelArray.count; i++) {
        YDSelectViewModel *model = self.tagView.modelArray[i];
        
        if (model.isSelected) {
            switch (i) {
                case 0:
                {
                    self.searcher.sortType = 21;
                }
                    break;
                    
                case 1:
                {
                    if (model.type == 0) {
                        self.searcher.sortType = 11;
                        
                    } else {
                        self.searcher.sortType = 10;
                    }
                }
                    break;
                    
                default:
                    break;
            }
        }
    }
    
    if (self.tagView) {
        self.searcher.status = self.tagView.selectedResultStatus;
        self.searcher.moneyOwe = self.tagView.selectedResultMoneyOwe;
        self.searcher.goodsOwe = self.tagView.selectedResultGoodsOwe;
    }
}

#pragma mark - UITableView代理

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    NSInteger count = self.searcher.customerDataArray.count;
    if (count) {
        return count;
        
    } else {
        return 1;
    }
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (self.searcher.customerDataArray.count) {
        return [YDCustomerManageCell getHeight];
        
    } else {
        return [YDNullCell getHeightWithTableView:tableView] - self.tableView.tableHeaderView.height;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (self.searcher.customerDataArray.count) {
        YDCustomerManageCell *cell = [YDCustomerManageCell cellWithTableView:tableView];
        cell.data = self.searcher.customerDataArray[indexPath.section];
        
        return cell;
        
    } else {
        YDNullCell *cell = [YDNullCell cellWithTableView:tableView];
        return cell;
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    return 10;
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section
{
    return [[UIView alloc] init];
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (self.searcher.customerDataArray.count) {
        YDSearcherCustomerData *productData = self.searcher.customerDataArray[indexPath.section];
        NSString *url = [NSString stringWithFormat:@"%@/%ld", WEBURL_CUSTOMER_DETAIL, (long)productData.ID];
        [YDWebViewVC showWebViewWithUrl:url isHideNav:YES];
    }
}

#pragma mark - UITextFieldDelegate

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    
    [self refreshData:YES];

    return YES;
}

@end

//
//  YDPrintModel.m
//  YDERP
//
//  Created by 何英健 on 2021/8/30.
//

#import "YDPrintModel.h"
#import "YDOrderPrintView.h"
#import "YDPrintSettingVC.h"

// 二维码宽高
#define CODE_PHOTO_BORDER 180

typedef void(^BlockPrintModelCreateImg)(UIImage *image);

@interface YDPrintModel ()

@property (nonatomic, strong) YDOrderPrintData *data;

@property (nonatomic, strong) YDOrderPrintView *printView;

@property (nonatomic, assign) BOOL isPrinting;

@property (nonatomic, assign) BOOL aliPaySucceed;
@property (nonatomic, assign) BOOL wechatPaySucceed;
@property (nonatomic, assign) BOOL otherPaySucceed;

@property (nonatomic, strong) UIImage *aliPayImg;
@property (nonatomic, strong) UIImage *wechatPayImg;
@property (nonatomic, strong) UIImage *otherPayImg;

@end

@implementation YDPrintModel

+ (YDPrintModel *)defaultPrintModel
{
    static dispatch_once_t once;
    static YDPrintModel *instance;
    dispatch_once(&once, ^{
        instance = [[YDPrintModel alloc] init];
        
        [[NSNotificationCenter defaultCenter] addObserver:instance selector:@selector(appWillResignActiveNotification) name:UIApplicationWillResignActiveNotification object:nil];
        
        [[NSNotificationCenter defaultCenter] addObserver:instance
                                                 selector:@selector(appDidBecomeActive)
                                                     name:UIApplicationDidBecomeActiveNotification
                                                   object:nil];
    });
    return instance;
}

- (void)appWillResignActiveNotification
{
    if (self.isPrinting) {
        [[PTDispatcher share] pauseWriteData];
    }
}

- (void)appDidBecomeActive
{
    if (self.isPrinting) {
        [[PTDispatcher share] resumeWriteData];
    }
}

+ (BOOL)checkBluetoothStatus
{
    if ([[PTDispatcher share] getBluetoothStatus] == PTBluetoothStatePoweredOn) {
        return YES;
        
    } else if ([[PTDispatcher share] getBluetoothStatus] == PTBluetoothStatePoweredOff) {
        [MBProgressHUD showToastMessage:@"请打开蓝牙"];
        
        return NO;
        
    } else {
        [MBProgressHUD showToastMessage:@"蓝牙未授权，请前往系统设置找到APP授权"];

        return NO;
    }
}

+ (PTPrinter *)getCurrentPrinter
{
    return [PTDispatcher share].printerConnected;
}

+ (void)getDeviceOnCompletion:(BlockGetBluetoothPrinter)completion
{
    [[PTDispatcher share] scanBluetooth];
    
    [[PTDispatcher share] whenFindAllBluetooth:^(NSMutableArray<PTPrinter *> *printerArray) {
        if (completion) {
            // 把当前连接的打印机加入扫描结果中（扫描结果不会找到已经被连接的打印机）
            PTPrinter *currentPrinter = [YDPrintModel getCurrentPrinter];
            if (currentPrinter) {
                BOOL isExist = NO;
                for (PTPrinter *sub in printerArray) {
                    if ([sub.name isEqualToString:currentPrinter.name]) {
                        isExist = YES;
                    }
                }
                
                if (!isExist) {
                    [printerArray insertObject:currentPrinter atIndex:0];
                }
            }
            
            // 按照距离排序
            NSArray *array = [printerArray sortedArrayUsingComparator:^NSComparisonResult(PTPrinter*  _Nonnull obj1, PTPrinter*  _Nonnull obj2) {
                return obj1.distance.floatValue > obj2.distance.floatValue;
            }];
            
            completion(array);
        }
    }];
}

+ (void)connectPrinter:(PTPrinter *)printer completion:(void(^)(void))completion
{
    PTPrinter *currentPrinter = [YDPrintModel getCurrentPrinter];
    if ([printer.name isEqualToString:currentPrinter.name]) {
        // 当前已连接
        return;
    }
    
    // 连接
    [YDLoadingView showToSuperview:nil];
    
    [[PTDispatcher share] connectPrinter:printer];
    
    [[PTDispatcher share] whenConnectSuccess:^{
        [YDLoadingView hideToSuperview:nil];
        
        // 保存最近连接的打印机
        SetUserDefaultKeyWithObject(CURRENT_PRINTER, printer.name);
        PostNotification(PRINTER_CONNECT_REFRESH, nil);
        
        if (completion) {
            completion();
        }
    }];
    
    /// 异步回调
    [[PTDispatcher share] whenConnectFailureWithErrorBlock:^(PTConnectError error) {
        [YDLoadingView hideToSuperview:nil];
        PostNotification(PRINTER_CONNECT_REFRESH, nil);
        
        NSString *errorString = @"";
        switch (error) {
        case PTConnectErrorBleSystem:
            errorString = @"系统错误";
            break;
        case PTConnectErrorBleTimeout:
            errorString = @"连接超时";
            break;
        case PTConnectErrorBleValidateFail:
            errorString = @"验证失败";
            break;
        case PTConnectErrorBleUnknownDevice:
            errorString = @"未知设备";
            break;
        case PTConnectErrorBleValidateTimeout:
            errorString = @"验证超时";
            break;
        case PTConnectErrorBleDisvocerServiceTimeout:
            errorString = @"获取不到蓝牙服务";
            break;
        default:
            break;
        }
        [MBProgressHUD showToastMessage:errorString];
    }];
}

+ (void)checkAndPrintWithOrderID:(NSString *)orderID nav:(UINavigationController *)nav
{
    // 检查蓝牙连接
    if ([YDPrintModel checkBluetoothStatus]) {
        // 检查打印机连接
        if ([YDPrintModel getCurrentPrinter]) {
            [YDPrintModel printWithOrderID:orderID];
            
        } else {
            // 查找上次连接记录
            NSString *currentPrinter = GetUserDefaultWithKey(CURRENT_PRINTER);
            
            if (currentPrinter.length) {
                // 查找可用蓝牙设备，连接上次的设备
                [YDLoadingView showToSuperview:nil];
                
                __block BOOL isExist = NO;
                
                [YDPrintModel getDeviceOnCompletion:^(NSArray<PTPrinter *> *printerArray) {
                    // 这个block会多次调用，因此需要加入isShown进行控制
                    for (PTPrinter *sub in printerArray) {
                        if ([sub.name isEqualToString:currentPrinter]) {
                            [YDLoadingView hideToSuperview:nil];
                            isExist = YES;
                            
                            [YDPrintModel connectPrinter:sub completion:^{
                                // 连接成功，开始打印
                                [YDPrintModel printWithOrderID:orderID];
                            }];
                            break;
                        }
                    }
                }];
                
                dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(10.0f * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                    if (!isExist) {
                        [YDLoadingView hideToSuperview:nil];
                        [YDPrintModel showPrintSettingWithNav:nav];
                    }
                });

            } else {
                // 没有连接过打印机
                [YDPrintModel showPrintSettingWithNav:nav];
            }
        }
    }
}

+ (void)showPrintSettingWithNav:(UINavigationController *)nav
{
    [MBProgressHUD showToastMessage:@"请选择打印机"];
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        YDPrintSettingVC *vc = [[YDPrintSettingVC alloc] init];
        [nav pushViewController:vc animated:YES];
    });
}

+ (void)printWithOrderID:(NSString *)orderID
{
    // 获取订单打印信息
    [YDLoadingView showToSuperview:nil];
    
    [YDFunction getPrintInfoWithOrderID:orderID completion:^(YDOrderPrintData *data) {
        [YDLoadingView hideToSuperview:nil];
        
        [YDPrintModel printWithData:data];
//        [YDPrintModel printImageWithData:data];
        
    } faildBlock:^(NSString *msg) {
        [YDLoadingView hideToSuperview:nil];
        [MBProgressHUD showToastMessage:msg];
    }];
}

#pragma mark - 直接打印

+ (void)printWithData:(YDOrderPrintData *)data
{
    [YDPrintModel clear];
    [YDPrintModel defaultPrintModel].data = data;
    [YDPrintModel downloadCodePayImg];
}

+ (void)clear
{
    YDPrintModel *model = [YDPrintModel defaultPrintModel];
    model.data = nil;
    model.aliPaySucceed = NO;
    model.wechatPaySucceed = NO;
    model.otherPaySucceed = NO;
    model.aliPayImg = nil;
    model.wechatPayImg = nil;
    model.otherPayImg = nil;
}

// 下载二维码
+ (void)downloadCodePayImg
{
    YDPrintModel *model = [YDPrintModel defaultPrintModel];
    
    // 依次获取支付宝/微信/收款码
    YDCollectMoneyData *aliPay;
    YDCollectMoneyData *wechatPay;
    YDCollectMoneyData *otherPay;
    for (YDCollectMoneyData *sub in model.data.shop.paySettings) {
        if ([sub.type isEqualToString:@"alipay"]) {
            aliPay = sub;
            
        } else if ([sub.type isEqualToString:@"wechat"]) {
            wechatPay = sub;
            
        } else if ([sub.type isEqualToString:@"other"]) {
            otherPay = sub;
        }
    }
    
    if (!aliPay.imgUrl.length && !wechatPay.imgUrl.length && !otherPay.imgUrl.length) {
        [YDPrintModel startToPrint];
        
    } else {
        if (!aliPay.imgUrl.length) {
            NSLog(@"客户没有支付宝图片");
            model.aliPaySucceed = YES;
        }
        
        if (!wechatPay.imgUrl.length) {
            NSLog(@"客户没有微信图片");
            model.wechatPaySucceed = YES;
        }
        
        if (!otherPay.imgUrl.length) {
            NSLog(@"客户没有个人支付码图片");
            model.otherPaySucceed = YES;
        }
        
        if (aliPay.imgUrl.length) {
            [SDWebImageDownloader.sharedDownloader downloadImageWithURL:[NSURL URLWithString:aliPay.imgUrl] options:SDWebImageDownloaderHighPriority progress:nil completed:^(UIImage * _Nullable image, NSData * _Nullable data, NSError * _Nullable error, BOOL finished) {
                model.aliPaySucceed = YES;
                
                if (image) {
                    NSLog(@"下载支付宝图片完成");
                    model.aliPayImg = [image scaleToSize:CGSizeMake(CODE_PHOTO_BORDER, CODE_PHOTO_BORDER)];
 
                } else {
                    NSLog(@"下载支付宝图片失败");
                }
                [YDPrintModel checkPayImgDownloadSucceed];
            }];
        }
        
        if (wechatPay.imgUrl.length) {
            [SDWebImageDownloader.sharedDownloader downloadImageWithURL:[NSURL URLWithString:wechatPay.imgUrl] options:SDWebImageDownloaderHighPriority progress:nil completed:^(UIImage * _Nullable image, NSData * _Nullable data, NSError * _Nullable error, BOOL finished) {
                model.wechatPaySucceed = YES;
                
                if (image) {
                    NSLog(@"下载微信图片完成");
                    model.wechatPayImg = [image scaleToSize:CGSizeMake(CODE_PHOTO_BORDER, CODE_PHOTO_BORDER)];
 
                } else {
                    NSLog(@"下载微信图片失败");
                }
                [YDPrintModel checkPayImgDownloadSucceed];
            }];
        }
        
        if (otherPay.imgUrl.length) {
            [SDWebImageDownloader.sharedDownloader downloadImageWithURL:[NSURL URLWithString:otherPay.imgUrl] options:SDWebImageDownloaderHighPriority progress:nil completed:^(UIImage * _Nullable image, NSData * _Nullable data, NSError * _Nullable error, BOOL finished) {
                model.otherPaySucceed = YES;
                
                if (image) {
                    NSLog(@"下载个人支付码图片完成");
                    model.otherPayImg = [image scaleToSize:CGSizeMake(CODE_PHOTO_BORDER, CODE_PHOTO_BORDER)];
 
                } else {
                    NSLog(@"下载个人支付码图片失败");
                }
                [YDPrintModel checkPayImgDownloadSucceed];
            }];
        }
    }
}

+ (void)checkPayImgDownloadSucceed
{
    YDPrintModel *model = [YDPrintModel defaultPrintModel];
    if (model.aliPaySucceed && model.wechatPaySucceed && model.otherPaySucceed) {
        NSLog(@"打印单所有图片下载完成");
        [YDPrintModel startToPrint];
    }
}

+ (void)startToPrint
{
    YDPrintModel *model = [YDPrintModel defaultPrintModel];
    model.isPrinting = YES;
    YDOrderPrintData *data = model.data;
    
    CGFloat originX = 0;
    CGFloat originY = 0;
    CGFloat width = 576;
    
    PTCommandCPCL *cmd = [[PTCommandCPCL alloc] init];
    // 商品单高度
    CGFloat productListH = 32 * data.orderItems.count;
    // 备注高度
    CGFloat remarkH = [YDPrintModel getLabelHeightWithContent:data.order.remark];
    // 店铺地址高度
    CGFloat addressH = [YDPrintModel getLabelHeightWithContent:data.shop.shopAddr];
    // 银行卡高度
    CGFloat cardPayH = 0;
    NSMutableArray *cardArray = [YDPrintModel getCardWithData:data];
    if (cardArray.count) {
        cardPayH = 8 + 32 * cardArray.count;
    }
    // 二维码高度
    CGFloat codePayH = 0;
    NSMutableArray *codeArray = [YDPrintModel getCodeWithData:data];
    if (codeArray.count) {
        codePayH = CODE_PHOTO_BORDER + 50;
    }
    // 小票总高度
    CGFloat pageH = 780 + productListH + remarkH + addressH + cardPayH + codePayH + 50;
    
    /// 这条是必需要调用的，表示打印多长
    [cmd cpclLabelWithOffset:0 hRes:PTCPCLLabelResolution200 vRes:PTCPCLLabelResolution200 height:pageH quantity:1];
    /// 设置打印宽度
    [cmd cpclPageWidth:width];
    
    [cmd cpclCenter];
    [cmd cpclTextWithRotate:PTCPCLStyleRotation0 font:PTCPCLTextFont4 fontSize:PTCPCLTextFontSize0 x:originX y:originY text:[NSString stringWithFormat:@"%@订单", data.shop.shopName]];
    originY += 80;
    
    [cmd cpclLeft];
    NSString *clientName = data.order.clientName;
    if (!clientName.length) {
        clientName = @"临时客户";
    }
    [cmd cpclTextWithRotate:PTCPCLStyleRotation0 font:PTCPCLTextFont8 fontSize:PTCPCLTextFontSize0 x:originX y:originY text:[NSString stringWithFormat:@"客户：%@", clientName]];
    [cmd cpclTextWithRotate:PTCPCLStyleRotation0 font:PTCPCLTextFont8 fontSize:PTCPCLTextFontSize0 x:originX y:originY + 35 text:[NSString stringWithFormat:@"订单：%@", data.order.orderNo]];
    [cmd cpclTextWithRotate:PTCPCLStyleRotation0 font:PTCPCLTextFont8 fontSize:PTCPCLTextFontSize0 x:originX y:originY + 70 text:[NSString stringWithFormat:@"开单日期：%@", data.order.orderTime]];
    [cmd cpclTextWithRotate:PTCPCLStyleRotation0 font:PTCPCLTextFont8 fontSize:PTCPCLTextFontSize0 x:originX y:originY + 105 text:[NSString stringWithFormat:@"开单人：%@", data.order.salesName]];
    
    originY += 145;
    [cmd cpclLineWithXPos:originX yPos:originY xEnd:width - 2 * originX yEnd:originY thickness:2];
    
    [cmd cpclConcatStartWithXPos:originX yPos:originY + 8];
    NSString *title1 = @"款号/名称";
    NSString *title2 = @" 颜色 ";
    NSString *title3 = @" 尺码 ";
    NSString *title4 = @"  数量  ";
    NSString *title5 = @"  单价  ";
    NSString *title6 = @"   小计   ";
    [cmd cpclConcatTextWithFont:PTCPCLTextFont8 fontSize:PTCPCLTextFontSize0 offset:0 text:title1];
    [cmd cpclConcatTextWithFont:PTCPCLTextFont8 fontSize:PTCPCLTextFontSize0 offset:0 text:title2];
    [cmd cpclConcatTextWithFont:PTCPCLTextFont8 fontSize:PTCPCLTextFontSize0 offset:0 text:title3];
    [cmd cpclConcatTextWithFont:PTCPCLTextFont8 fontSize:PTCPCLTextFontSize0 offset:0 text:title4];
    [cmd cpclConcatTextWithFont:PTCPCLTextFont8 fontSize:PTCPCLTextFontSize0 offset:0 text:title5];
    [cmd cpclConcatTextWithFont:PTCPCLTextFont8 fontSize:PTCPCLTextFontSize0 offset:0 text:title6];
    [cmd cpclConcatEnd];
    
    originY += 40;
    [cmd cpclLineWithXPos:originX yPos:originY xEnd:width - 2 * originX yEnd:originY thickness:2];
    
    originY += 8;
    for (YDOrderPrintDataItem *sub in data.orderItems) {
        [cmd cpclConcatStartWithXPos:originX yPos:originY];
        
        // 款号
        NSString *codeStr = @"";
        NSString *nameStr = @"";
        if (sub.itemCode.length) {
            codeStr = sub.itemCode;
        }
        if (sub.itemName.length) {
            nameStr = sub.itemName;
        }
        [cmd cpclConcatTextWithFont:PTCPCLTextFont8 fontSize:PTCPCLTextFontSize0 offset:0 text:[YDPrintModel getStringWithTitleLength:9 content:[NSString stringWithFormat:@"%@/%@", codeStr, nameStr]]];
        [cmd cpclConcatTextWithFont:PTCPCLTextFont8 fontSize:PTCPCLTextFontSize0 offset:0 text:[YDPrintModel getStringWithTitleLength:6 content:sub.color]];
        [cmd cpclConcatTextWithFont:PTCPCLTextFont8 fontSize:PTCPCLTextFontSize0 offset:0 text:[YDPrintModel getStringWithTitleLength:6 content:sub.size]];
        [cmd cpclConcatTextWithFont:PTCPCLTextFont8 fontSize:PTCPCLTextFontSize0 offset:0 text:[YDPrintModel getStringWithTitleLength:8 content:[NSString stringWithFormat:@"%ld", (long)sub.itemNum]]];
        [cmd cpclConcatTextWithFont:PTCPCLTextFont8 fontSize:PTCPCLTextFontSize0 offset:0 text:[YDPrintModel getStringWithTitleLength:8 content:[NSString stringWithFormat:@"%.2f", sub.salesPrice]]];
        [cmd cpclConcatTextWithFont:PTCPCLTextFont8 fontSize:PTCPCLTextFontSize0 offset:0 text:[YDPrintModel getStringWithTitleLength:10 content:[NSString stringWithFormat:@"%.2f", sub.subtotalAmount]]];
        [cmd cpclConcatEnd];
        
        originY += 32;
    }

    [cmd cpclLineWithXPos:originX yPos:originY xEnd:width - 2 * originX yEnd:originY thickness:2];
    
    originY += 8;
    [cmd cpclTextWithRotate:PTCPCLStyleRotation0 font:PTCPCLTextFont8 fontSize:PTCPCLTextFontSize0 x:originX y:originY text:@"总 数 量（件）"];
    [cmd cpclTextWithRotate:PTCPCLStyleRotation0 font:PTCPCLTextFont8 fontSize:PTCPCLTextFontSize0 x:originX y:originY + 32 text:@"商品金额（元）"];
    [cmd cpclTextWithRotate:PTCPCLStyleRotation0 font:PTCPCLTextFont8 fontSize:PTCPCLTextFontSize0 x:originX y:originY + 32 * 2 text:@"优惠金额（元）"];
    [cmd cpclRight];
    [cmd cpclTextWithRotate:PTCPCLStyleRotation0 font:PTCPCLTextFont8 fontSize:PTCPCLTextFontSize0 x:originX y:originY text:[NSString stringWithFormat:@"%ld", (long)data.order.totalNum]];
    [cmd cpclTextWithRotate:PTCPCLStyleRotation0 font:PTCPCLTextFont8 fontSize:PTCPCLTextFontSize0 x:originX y:originY + 32 text:[NSString stringWithFormat:@"%.2f", data.order.itemAmount]];
    [cmd cpclTextWithRotate:PTCPCLStyleRotation0 font:PTCPCLTextFont8 fontSize:PTCPCLTextFontSize0 x:originX y:originY + 32 * 2 text:[NSString stringWithFormat:@"%.2f", data.order.couponAmount]];
    
    originY += 32 * 3;
    [cmd cpclLineWithXPos:originX yPos:originY xEnd:width - 2 * originX yEnd:originY thickness:2];
    
    originY += 8;
    [cmd cpclTextWithRotate:PTCPCLStyleRotation0 font:PTCPCLTextFont8 fontSize:PTCPCLTextFontSize0 x:originX y:originY text:[NSString stringWithFormat:@"合计   %.2f", data.order.itemAmount - data.order.couponAmount]];
    
    originY += 32;
    [cmd cpclLineWithXPos:originX yPos:originY xEnd:width - 2 * originX yEnd:originY thickness:2];
    
    originY += 8;
    [cmd cpclLeft];
    [cmd cpclTextWithRotate:PTCPCLStyleRotation0 font:PTCPCLTextFont8 fontSize:PTCPCLTextFontSize0 x:originX y:originY text:[NSString stringWithFormat:@"已收货款   %.2f", data.order.payItemAmount]];
    [cmd cpclCenter];
    [cmd cpclTextWithRotate:PTCPCLStyleRotation0 font:PTCPCLTextFont8 fontSize:PTCPCLTextFontSize0 x:originX y:originY text:@"|"];
    [cmd cpclRight];
    [cmd cpclTextWithRotate:PTCPCLStyleRotation0 font:PTCPCLTextFont8 fontSize:PTCPCLTextFontSize0 x:originX y:originY text:[NSString stringWithFormat:@"累计欠款   %.2f", data.order.currDueAmount]];
    
    originY += 32;
    [cmd cpclLineWithXPos:originX yPos:originY xEnd:width - 2 * originX yEnd:originY thickness:2];
    
    [cmd cpclLeft];
    originY += 8;
    [cmd cpclTextWithRotate:PTCPCLStyleRotation0 font:PTCPCLTextFont8 fontSize:PTCPCLTextFontSize0 x:originX y:originY text:@"备   注"];
    [cmd cpclTextWithRotate:PTCPCLStyleRotation0 font:PTCPCLTextFont8 fontSize:PTCPCLTextFontSize0 x:133 y:originY text:data.order.remark];
    
    originY += [YDPrintModel getLabelHeightWithContent:data.order.remark] + 8;
    [cmd cpclLineWithXPos:originX yPos:originY xEnd:width - 2 * originX yEnd:originY thickness:2];
    
    originY += 8;
    [cmd cpclTextWithRotate:PTCPCLStyleRotation0 font:PTCPCLTextFont8 fontSize:PTCPCLTextFontSize0 x:originX y:originY text:@"店铺名称"];
    [cmd cpclTextWithRotate:PTCPCLStyleRotation0 font:PTCPCLTextFont8 fontSize:PTCPCLTextFontSize0 x:133 y:originY text:data.shop.shopName];
    originY += 32;
    [cmd cpclTextWithRotate:PTCPCLStyleRotation0 font:PTCPCLTextFont8 fontSize:PTCPCLTextFontSize0 x:originX y:originY text:@"店铺地址"];
    [cmd cpclTextWithRotate:PTCPCLStyleRotation0 font:PTCPCLTextFont8 fontSize:PTCPCLTextFontSize0 x:133 y:originY text:data.shop.shopAddr];
    originY += [YDPrintModel getLabelHeightWithContent:data.shop.shopAddr] + 8;
    [cmd cpclTextWithRotate:PTCPCLStyleRotation0 font:PTCPCLTextFont8 fontSize:PTCPCLTextFontSize0 x:originX y:originY text:@"联系方式"];
    [cmd cpclTextWithRotate:PTCPCLStyleRotation0 font:PTCPCLTextFont8 fontSize:PTCPCLTextFontSize0 x:133 y:originY text:data.shop.shopMobile];
    
    originY += 32;
    [cmd cpclLineWithXPos:originX yPos:originY xEnd:width - 2 * originX yEnd:originY thickness:2];
    
    if (cardArray.count) {
        originY += 8;
        for (YDCollectMoneyData *sub in cardArray) {
            [cmd cpclLeft];
            NSString *cardBank = [YDPrintModel getStringWithTitleLength:4 * 2 content:sub.cardBank];
            [cmd cpclTextWithRotate:PTCPCLStyleRotation0 font:PTCPCLTextFont8 fontSize:PTCPCLTextFontSize0 x:originX y:originY text:cardBank];
            [cmd cpclTextWithRotate:PTCPCLStyleRotation0 font:PTCPCLTextFont8 fontSize:PTCPCLTextFontSize0 x:133 y:originY text:sub.cardNo];
            
            [cmd cpclRight];
            NSString *cardName = [YDPrintModel getStringWithTitleLength:3 * 2 content:sub.cardName];
            [cmd cpclTextWithRotate:PTCPCLStyleRotation0 font:PTCPCLTextFont8 fontSize:PTCPCLTextFontSize0 x:originX y:originY text:[NSString stringWithFormat:@"账户名 %@", cardName]];
            
            originY += 32;
        }
        
        [cmd cpclLineWithXPos:originX yPos:originY xEnd:width - 2 * originX yEnd:originY thickness:2];
    }

    originY += 8;
    [cmd cpclTextWithRotate:PTCPCLStyleRotation0 font:PTCPCLTextFont3 fontSize:PTCPCLTextFontSize0 x:originX y:originY text:@"* 提醒：本店不调换款，可调色调码，残次在收到货一周内调换"];

    originY += 50;
    if (codeArray.count) {
        for (int i = 0; i < codeArray.count; i++) {
            NSDictionary *dict = codeArray[i];
            NSString *name = [dict objectForKey:@"name"];
            UIImage *img = [dict objectForKey:@"img"];
            CGFloat photoX;
            CGFloat codeTitleX;
            
            if (i == 0) {
                photoX = originX;
                
            } else if (i == 1) {
                photoX = originX + CODE_PHOTO_BORDER + 15;
                
            } else {
                photoX = originX + (CODE_PHOTO_BORDER + 15) * 2;
            }
            
            if (name.length < 3) {
                codeTitleX = photoX + (CODE_PHOTO_BORDER - 50) / 2;
                
            } else {
                codeTitleX = photoX + (CODE_PHOTO_BORDER - 70) / 2;
            }
            
            /// 图片效果 有二值 、抖动(模拟灰度效果)
            /// 如果支持压缩选PTBitmapCompressModeLZO  不支持的话PTBitmapCompressModeNone
            [cmd cpclPrintBitmapWithXPos:photoX yPos:originY image:img.CGImage bitmapMode:PTBitmapModeDithering compress:PTBitmapCompressModeNone isPackage:NO];
            
            [cmd cpclTextWithRotate:PTCPCLStyleRotation0 font:PTCPCLTextFont8 fontSize:PTCPCLTextFontSize0 x:codeTitleX y:originY + CODE_PHOTO_BORDER text:name];
        }
         
        originY += CODE_PHOTO_BORDER + 50;
    }
    
    originY += 100;
    [cmd cpclCenter];
    [cmd cpclTextWithRotate:PTCPCLStyleRotation0 font:PTCPCLTextFont8 fontSize:PTCPCLTextFontSize0 x:originX y:originY text:@"欢迎您再次光临！"];
    
    /// 若是标签纸，加上这条指令可以定位到缝标
    [cmd cpclForm];
    /// 打印
    [cmd cpclPrint];
    
    [[PTDispatcher share] sendData:cmd.cmdData];
    
    [[PTDispatcher share] whenSendSuccess:^(int64_t dataCount, double time) {
        [MBProgressHUD showToastMessage:@"打印成功"];
        [SVProgressHUD dismiss];
        [YDPrintModel defaultPrintModel].isPrinting = NO;
    }];
    
    [[PTDispatcher share] whenSendProgressUpdate:^(NSNumber *number) {
        [SVProgressHUD showProgress:number.floatValue];
    }];
    
    /// 实现不处理
    [[PTDispatcher share] whenReceiveData:^(NSData *data) {
    }];
    
    [[PTDispatcher share] whenUpdatePrintState:^(PTPrintState state) {
    }];
    
    [[PTDispatcher share] whenSendFailure:^{
        [MBProgressHUD showToastMessage:@"打印失败"];
        [SVProgressHUD dismiss];
        [YDPrintModel defaultPrintModel].isPrinting = NO;
    }];
}

// 限制或补全文案长度
+ (NSString *)getStringWithTitleLength:(NSInteger)titleLength content:(NSString *)content
{
    // 从右面开始补全空格
    NSInteger type = 1;
    NSInteger contentLength = [content charactorNumber];

    while (contentLength < titleLength) {
        if (type == 1) {
            type = 0;
            content = [NSString stringWithFormat:@"%@ ", content];
            
        } else {
            type = 1;
            content = [NSString stringWithFormat:@" %@", content];
        }
        contentLength = [content charactorNumber];
    }
    
    while (contentLength > titleLength) {
        content = [content substringToIndex:content.length - 1];
        contentLength = [content charactorNumber];
    }
    return content;
}

// 计算content占的总高度
+ (CGFloat)getLabelHeightWithContent:(NSString *)content
{
    // 单行字节数
    NSInteger oneLineLength = 18 * 2;
    NSInteger contentLength = [content charactorNumber];
    NSInteger row = contentLength / oneLineLength + 1;
    return row * 24;
}

// 获取所有银行卡
+ (NSMutableArray<YDCollectMoneyData *> *)getCardWithData:(YDOrderPrintData *)data
{
    NSMutableArray<YDCollectMoneyData *> *cardArray = [NSMutableArray array];
    for (YDCollectMoneyData *sub in data.shop.paySettings) {
        if ([sub.type isEqualToString:@"card"]) {
            [cardArray addObject:sub];
        }
    }
    
    return cardArray;
}

// 按顺序获取支付宝/微信支付/个人二维码
+ (NSMutableArray<NSDictionary *> *)getCodeWithData:(YDOrderPrintData *)data
{
    YDPrintModel *model = [YDPrintModel defaultPrintModel];
    YDCollectMoneyData *aliPay;
    YDCollectMoneyData *wechatPay;
    YDCollectMoneyData *otherPay;
    
    for (YDCollectMoneyData *sub in data.shop.paySettings) {
        if ([sub.type isEqualToString:@"alipay"]) {
            aliPay = sub;
            
        } else if ([sub.type isEqualToString:@"wechat"]) {
            wechatPay = sub;
            
        } else if ([sub.type isEqualToString:@"other"]) {
            otherPay = sub;
        }
    }
    
    NSMutableArray<NSDictionary *> *codeArray = [NSMutableArray array];
    if (aliPay && model.aliPayImg) {
        [codeArray addObject:[NSDictionary dictionaryWithObjectsAndKeys:@"支付宝", @"name", model.aliPayImg, @"img", nil]];
    }
    if (wechatPay && model.wechatPayImg) {
        [codeArray addObject:[NSDictionary dictionaryWithObjectsAndKeys:@"微信", @"name", model.wechatPayImg, @"img", nil]];
    }
    if (otherPay && model.otherPayImg) {
        [codeArray addObject:[NSDictionary dictionaryWithObjectsAndKeys:@"收款码", @"name", model.otherPayImg, @"img", nil]];
    }
    
    return codeArray;
}

#pragma mark - 转成图片打印

// 采用图片方式打印
+ (void)printImageWithData:(YDOrderPrintData *)data
{
    [YDPrintModel createImgWithData:data completion:^(UIImage *image) {
        // 开始打印
        if (image == nil) {
            NSLog(@"打印图片为空");
            return;
        }
        
        NSLog(@"开始进入图片打印");
        [YDPrintModel defaultPrintModel].isPrinting = YES;
        
        PTCommandCPCL *cmd = [[PTCommandCPCL alloc] init];
        /// 这条是必需要调用的，表示打印多长
        [cmd cpclLabelWithOffset:0 hRes:PTCPCLLabelResolution200 vRes:PTCPCLLabelResolution200 height:image.size.height quantity:1];
        /// 设置打印宽度
        [cmd cpclPageWidth:576];
        
        /// 图片效果 有二值 、抖动(模拟灰度效果)
        /// 如果支持压缩选PTBitmapCompressModeLZO  不支持的话PTBitmapCompressModeNone
        [cmd cpclPrintBitmapWithXPos:0 yPos:0 image:image.CGImage bitmapMode:PTBitmapModeDithering compress:PTBitmapCompressModeNone isPackage:NO];
        
        [cmd cpclPrint];
        
        [[PTDispatcher share] sendData:cmd.cmdData];
        
        [[PTDispatcher share] whenSendSuccess:^(int64_t dataCount, double time) {
            [MBProgressHUD showToastMessage:@"打印成功"];
            [SVProgressHUD dismiss];
            [YDPrintModel defaultPrintModel].isPrinting = NO;
        }];
        
        [[PTDispatcher share] whenSendProgressUpdate:^(NSNumber *number) {
            [SVProgressHUD showProgress:number.floatValue];
        }];
        
        /// 实现不处理
        [[PTDispatcher share] whenReceiveData:^(NSData *data) {
        }];
        
        [[PTDispatcher share] whenUpdatePrintState:^(PTPrintState state) {
        }];
        
        [[PTDispatcher share] whenSendFailure:^{
            [MBProgressHUD showToastMessage:@"打印失败"];
            [SVProgressHUD dismiss];
            [YDPrintModel defaultPrintModel].isPrinting = NO;
        }];
    }];
}

+ (void)createImgWithData:(YDOrderPrintData *)data completion:(BlockPrintModelCreateImg)completion
{
    // 创建打印单
    [YDPrintModel defaultPrintModel].printView = [YDOrderPrintView createWithData:data completion:^(UIView *view) {
        if (completion) {
            UIImage *image = [UIView changeViewToPhoto:view];

        //    NSData *newData = [PTBitmap getImageData:image.CGImage mode:PTBitmapModeBinary compress:PTBitmapCompressModeNone package:NO inversion:NO];
            UIImage *newImg = [PTBitmap generateRenderingWithImage:image.CGImage mode:PTBitmapModeBinary];
            NSLog(@"aaaaaa view:%f newImg:%f", view.height, newImg.size.height);
            completion(newImg);
        }
    }];
}

@end

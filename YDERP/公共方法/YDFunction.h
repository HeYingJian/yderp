//
//  YDFunction.h
//  YDERP
//
//  Created by 何英健 on 2021/7/27.
//

#import <Foundation/Foundation.h>
#import "YDLoginModel.h"
#import "YDUploadOnePhotoModel.h"
#import "YDUploadMorePhotoModel.h"
#import "YDCustomerAddressModel.h"
#import "YDSubmitOrderModel.h"
#import "YDOrderDetailModel.h"
#import "YDProductSkuGroupModel.h"
#import "YDCheckoutOrderModel.h"
#import "YDSubmitOrderModel.h"
#import "YDUserInfoModel.h"
#import "YDModifyOrderModel.h"
#import "YDOrderPrintModel.h"
#import "YDSupplyDetailModel.h"

typedef void(^BlockLoadDataSucceed)(void);
typedef void(^BlockUploadImgSucceed)(NSMutableArray<NSString *> *urlArray);
typedef void(^BlockGetAddressSucceed)(NSMutableArray<YDCustomerAddressData *> *addressArray);
typedef void(^BlockSubmitOrderSucceed)(YDSubmitOrderData *orderData);
typedef void(^BlockLoadOrderDetailSucceed)(YDOrderDetailData *data);
typedef void(^BlockLoadOrderPrintSucceed)(YDOrderPrintData *data);
typedef void(^BlockModifyOrderSucceed)(YDModifyOrderData *data);
typedef void(^BlockProductSkuGroupSucceed)(NSMutableArray<YDSearcherSkuGroupData *> *dataArray);
typedef void(^BlockCheckoutOrder)(YDCheckoutOrderData *data);
typedef void(^BlockLoadUserInfo)(YDUserInfoData *data);

typedef void(^BlockUploadImgFaild)(NSString *msg);

@interface YDFunction : NSObject

// 打开客服页面
+ (void)showCustomer;

// 登录成功保存数据
+ (void)loginSucceedWithLoginData:(YDLoginData *)data;
// 退出登录
+ (void)logout;

// 获取当前用户信息
+ (void)loadUserInfoOnCompletion:(BlockLoadUserInfo)completion failure:(BlockUploadImgFaild)failure;
// 获取当前用户权限
+ (void)loadUserLimitOnCompletion:(BlockLoadDataSucceed)completion failure:(BlockUploadImgFaild)failure;

/**
 上传图片
 @param imageArray   图片数组
 @param type       上传文件类型 1-图片(image) 2-视频(video)
 @param group      文件分组存储，可为空 1-商品图片(item) 2-销售单图片(sales) 3-配货单图片(alloc) 4-发货单图片(delivery) 5-收款图片(payment) 6-用户(user)
 @param groupStr   自己填写文件分组存储
 @param parameters 额外参数
 */
+ (void)uploadImageArray:(NSArray<UIImage *> *)imageArray type:(NSInteger)type group:(NSInteger)group groupStr:(NSString *)groupStr parameters:(id)parameters sucess:(BlockUploadImgSucceed)sucess failure:(BlockUploadImgFaild)failure;

// 把订单数据中sku列表格式化为skuGroup(颜色+尺码)
+ (NSMutableArray<YDSearcherSkuGroupData *> *)getSkuGroupArrayFromOrderSkuArray:(NSMutableArray<YDOrderDetailDataItemSku *> *)orderSkuArray;

// 把配货详情数据中sku列表格式化为skuGroup(颜色+尺码)
+ (NSMutableArray<YDSearcherSkuGroupData *> *)getSkuGroupArrayFromSupplySkuArray:(NSMutableArray<YDSupplyDetailDataItemSku *> *)orderSkuArray;

/**
 获取某个客户的地址列表
 @param customerID   客户ID
 */
+ (void)getCustomerAddressListWithCustomerID:(NSInteger)customerID completion:(BlockGetAddressSucceed)completion;

/**
 新增/编辑某个客户的地址
 @param customerID          客户ID
 @param addressID           原地址ID(编辑用到)
 @param address             新地址
 @param isDefaultAddress    是否设置为默认地址
 */
+ (void)modifyCustomerAddressWithCustomerID:(NSInteger)customerID addressID:(NSString *)addressID address:(NSString *)address isDefaultAddress:(BOOL)isDefaultAddress completion:(BlockLoadDataSucceed)completion faildBlock:(BlockUploadImgFaild)faildBlock;

/**
 提交订单
 @param orderID         旧的orderID(修改订单用到)
 @param customerID      客户ID
 @param address         地址
 @param productArray    商品列
 @param refreshBlock    需要外部刷新页面
 */
+ (void)submitOrderWithOrderID:(NSString *)orderID customerID:(NSString *)customerID address:(NSString *)address productArray:(NSMutableArray<YDSearcherProductData *> *)productArray completion:(BlockSubmitOrderSucceed)completion refreshBlock:(BlockUploadImgFaild)refreshBlock faildBlock:(BlockUploadImgFaild)faildBlock;

/**
 检查订单版本号是否有变更(接口返回错误码5002表示订单发生修改，需要刷新页面)
 @param orderID         订单ID
 @param version         旧的版本号
 @param refreshBlock    需要外部刷新页面
 */
+ (void)checkOrderVersionWithOrderID:(NSString *)orderID version:(NSString *)version completion:(BlockLoadDataSucceed)completion refreshBlock:(BlockUploadImgFaild)refreshBlock faildBlock:(BlockUploadImgFaild)faildBlock;

/**
 获取订单详情
 @param type            0-有返回客户信息，但没有支付流水 1-没有返回客户信息，但有支付流水
 @param orderID         订单id
 @param refreshBlock    需要外部刷新页面
 */
+ (void)getOrderDetailWithType:(NSInteger)type orderID:(NSString *)orderID completion:(BlockLoadOrderDetailSucceed)completion refreshBlock:(BlockUploadImgFaild)refreshBlock faildBlock:(BlockUploadImgFaild)faildBlock;

/**
 修改订单
 @param orderID         订单id
 @param version         旧的订单版本号
 @param productArray    商品
 @param refreshBlock    需要外部刷新页面
 */
+ (void)modifyOrderWithOrderID:(NSString *)orderID version:(NSString *)version productArray:(NSMutableArray<YDSearcherProductData *> *)productArray completion:(BlockModifyOrderSucceed)completion refreshBlock:(BlockUploadImgFaild)refreshBlock faildBlock:(BlockUploadImgFaild)faildBlock;

/**
 获取订单打印信息
 @param orderID         订单id
 */
+ (void)getPrintInfoWithOrderID:(NSString *)orderID completion:(BlockLoadOrderPrintSucceed)completion faildBlock:(BlockUploadImgFaild)faildBlock;

/**
 获取指定订单底下指定商品的颜色尺码组合(包括下单数，已配数)
 @param orderID     订单id
 @param productID   商品id
 */
+ (void)getProductSkuGroupWithOrderID:(NSString *)orderID productID:(NSString *)productID completion:(BlockProductSkuGroupSucceed)completion faildBlock:(BlockUploadImgFaild)faildBlock;

/**
 作废收款记录
 @param payID           收款项id
 @param refreshBlock    需要外部刷新页面
 */
+ (void)cancelPayHistoryWithPayID:(NSString *)payID completion:(BlockLoadDataSucceed)completion refreshBlock:(BlockUploadImgFaild)refreshBlock faildBlock:(BlockUploadImgFaild)faildBlock;

/**
 结算（商品不改变）
 @param orderID           订单id
 @param couponAmount      优惠金额
 @param cashPay           现金支付金额
 @param cardPay           刷卡支付金额
 @param remitPay          汇款支付金额
 @param aliPay            支付宝支付金额
 @param wechatPay         微信支付金额
 @param remark            备注
 @param urlArray          图片数组
 @param version           回传从订单详情接口中拿到的version，如果发生修改，返回错误码5002
 @param refreshBlock      需要外部刷新页面
 */
+ (void)checkoutWithOrderID:(NSString *)orderID couponAmount:(NSString *)couponAmount cashPay:(NSString *)cashPay cardPay:(NSString *)cardPay remitPay:(NSString *)remitPay aliPay:(NSString *)aliPay wechatPay:(NSString *)wechatPay remark:(NSString *)remark urlArray:(NSMutableArray<NSString *> *)urlArray version:(NSString *)version completion:(BlockCheckoutOrder)completion refreshBlock:(BlockUploadImgFaild)refreshBlock faildBlock:(BlockUploadImgFaild)faildBlock;

/**
 改单后收款结算（商品有改变）
 @param type              0-改单后收款结算 1-改单后退款结算
 @param productArray      商品数组
 @param lossAmount        损耗扣款金额
 @param orderID           订单id
 @param couponAmount      优惠金额
 @param cashPay           现金支付金额
 @param cardPay           刷卡支付金额
 @param remitPay          汇款支付金额
 @param aliPay            支付宝支付金额
 @param wechatPay         微信支付金额
 @param remark            备注
 @param urlArray          图片数组
 @param version           回传从订单详情接口中拿到的version，如果发生修改，返回错误码5002
 @param refreshBlock      需要外部刷新页面
 */
+ (void)modifyCheckoutWithType:(NSInteger)type productArray:(NSMutableArray<YDSearcherProductData *> *)productArray lossAmount:(NSString *)lossAmount orderID:(NSString *)orderID couponAmount:(NSString *)couponAmount cashPay:(NSString *)cashPay cardPay:(NSString *)cardPay remitPay:(NSString *)remitPay aliPay:(NSString *)aliPay wechatPay:(NSString *)wechatPay remark:(NSString *)remark urlArray:(NSMutableArray *)urlArray version:(NSString *)version completion:(BlockCheckoutOrder)completion refreshBlock:(BlockUploadImgFaild)refreshBlock faildBlock:(BlockUploadImgFaild)faildBlock;

/**
 废除支付记录并结算
 @param orderID           订单id
 @param payID             废弃的支付记录
 @param couponAmount      优惠金额
 @param cashPay           现金支付金额
 @param cardPay           刷卡支付金额
 @param remitPay          汇款支付金额
 @param aliPay            支付宝支付金额
 @param wechatPay         微信支付金额
 @param remark            备注
 @param urlArray          图片数组
 @param version           回传从订单详情接口中拿到的version，如果发生修改，返回错误码5002
 */
+ (void)deletePayAndCheckoutWithOrderID:(NSString *)orderID payID:(NSString *)payID couponAmount:(NSString *)couponAmount cashPay:(NSString *)cashPay cardPay:(NSString *)cardPay remitPay:(NSString *)remitPay aliPay:(NSString *)aliPay wechatPay:(NSString *)wechatPay remark:(NSString *)remark urlArray:(NSMutableArray<NSString *> *)urlArray version:(NSString *)version completion:(BlockCheckoutOrder)completion faildBlock:(BlockUploadImgFaild)faildBlock;

@end

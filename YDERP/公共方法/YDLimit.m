//
//  YDLimit.m
//  YDERP
//
//  Created by 何英健 on 2021/8/28.
//

#import "YDLimit.h"

@implementation YDLimit

+ (YDLimit *)defaultLimit
{
    static dispatch_once_t once;
    static YDLimit *instance;
    dispatch_once(&once, ^{
        instance = [[YDLimit alloc] init];
        instance.data = [NSKeyedUnarchiver unarchiveObjectWithFile:[YDLimit getPath]];
    });
    return instance;
}

- (void)setData:(NSMutableArray<YDLimitData *> *)data
{
    _data = data;
    
    if (data) {
        [NSKeyedArchiver archiveRootObject:data toFile:[YDLimit getPath]];
    }
}

+ (NSString *)getPath
{
    return [NSString stringWithFormat:@"%@/documents/LimitFile", NSHomeDirectory()];
}

+ (BOOL)limitCustomer
{
    return [YDLimit getLimitWithCode:@"M_CLIENT"];
}

+ (BOOL)limitProduct
{
    return [YDLimit getLimitWithCode:@"M_ITEM"];
}

+ (BOOL)limitOrder
{
    return [YDLimit getLimitWithCode:@"M_ORDER"];
}

+ (BOOL)limitStock
{
    return [YDLimit getLimitWithCode:@"M_STOCK"];
}

+ (BOOL)limitAllocation
{
    return [YDLimit getLimitWithCode:@"M_ALLOCATION"];
}

+ (BOOL)limitDelivery
{
    return [YDLimit getLimitWithCode:@"M_DELIVERY"];
}

+ (BOOL)limitSystem
{
    return [YDLimit getLimitWithCode:@"M_SYSTEM"];
}

// 根据code判断权限
+ (BOOL)getLimitWithCode:(NSString *)code
{
    for (YDLimitData *sub in UserLimit.data) {
        if ([sub.code isEqualToString:code]) {
            if (!sub.selected) {
                [MBProgressHUD showToastMessage:[NSString stringWithFormat:@"没有%@权限", sub.name]];
            }
            
            return sub.selected;
        }
    }
    
    return NO;
}

@end

//
//  YDPrintModel.h
//  YDERP
//
//  Created by 何英健 on 2021/8/30.
//

#import <Foundation/Foundation.h>

typedef void(^BlockGetBluetoothPrinter)(NSArray<PTPrinter *> *printerArray);
typedef void(^BlockGetPrintImg)(UIImage *image);

@interface YDPrintModel : NSObject

// 检查蓝牙状态
+ (BOOL)checkBluetoothStatus;
// 获取当前连接的打印机
+ (PTPrinter *)getCurrentPrinter;
// 获取蓝牙设备列表
+ (void)getDeviceOnCompletion:(BlockGetBluetoothPrinter)completion;
// 连接打印机(本身有其他连接的话，断开连接，然后再重新连接新的)
+ (void)connectPrinter:(PTPrinter *)printer completion:(void(^)(void))completion;
// 打印订单
+ (void)printWithOrderID:(NSString *)orderID;
// 打印订单(包含检查蓝牙状态/自动连接)
+ (void)checkAndPrintWithOrderID:(NSString *)orderID nav:(UINavigationController *)nav;

@end

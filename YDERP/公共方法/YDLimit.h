//
//  YDLimit.h
//  YDERP
//
//  Created by 何英健 on 2021/8/28.
//

#import <Foundation/Foundation.h>
#import "YDLimitModel.h"

@interface YDLimit : NSObject

// 单例
+ (YDLimit *)defaultLimit;

@property (nonatomic, strong) NSMutableArray<YDLimitData *> *data;

// 是否有客户权限
+ (BOOL)limitCustomer;
// 是否有商品权限
+ (BOOL)limitProduct;
// 是否有订单权限
+ (BOOL)limitOrder;
// 是否有库存权限
+ (BOOL)limitStock;
// 是否有配货权限
+ (BOOL)limitAllocation;
// 是否有发货权限
+ (BOOL)limitDelivery;
// 是否有系统权限(包括用户管理、权限分配)
+ (BOOL)limitSystem;

@end

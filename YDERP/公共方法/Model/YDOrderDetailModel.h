//
//  YDOrderDetailModel.h
//  YDERP
//
//  Created by 何英健 on 2021/8/18.
//

#import "YDBaseModel.h"

@interface YDOrderDetailDataClient : NSObject

// 客户ID
@property (nonatomic, assign) NSInteger ID;
// 姓名
@property (nonatomic, copy) NSString *name;
// 联系方式
@property (nonatomic, copy) NSString *phone;
// 余额
@property (nonatomic, assign) CGFloat balance;

@end

@interface YDOrderDetailDataInfo : NSObject

// 订单ID
@property (nonatomic, assign) NSInteger ID;
// 订单号
@property (nonatomic, copy) NSString *orderNo;
// 员工姓名
@property (nonatomic, copy) NSString *salesName;
// 客户ID
@property (nonatomic, assign) NSInteger clientId;
// 客户姓名
@property (nonatomic, copy) NSString *clientName;
// 订单状态 0：挂单 10:待配货 20:部分配货 30：已配齐 40：已作废
@property (nonatomic, assign) NSInteger status;
// 订单状态名称
@property (nonatomic, copy) NSString *statusName;
// 总数量
@property (nonatomic, assign) NSInteger totalNum;
// 损耗金额
@property (nonatomic, assign) CGFloat lossAmount;
// 应付金额(合计)
@property (nonatomic, assign) double totalAmount;
// 商品总金额(订单总额)
@property (nonatomic, assign) CGFloat itemAmount;
// 已付金额
@property (nonatomic, assign) double payAmount;
// 已付货款金额
@property (nonatomic, assign) CGFloat payItemAmount;
// 优惠金额
@property (nonatomic, assign) CGFloat couponAmount;
// 开单时间
@property (nonatomic, copy) NSString *orderTime;
// 订单备注
@property (nonatomic, copy) NSString *remark;
// 发货数量
@property (nonatomic, assign) NSInteger deliveryNum;
// 配货数量
@property (nonatomic, assign) NSInteger allocNum;
// 客户地址
@property (nonatomic, copy) NSString *clientAddress;
// 配货单id
@property (nonatomic, assign) NSInteger allocOrderId;
// 版本号
@property (nonatomic, copy) NSString *version;

@end

@interface YDOrderDetailDataPayHistories : NSObject

// 支付流水ID
@property (nonatomic, copy) NSString *ID;
// 100: 现金 200：刷卡 300: 汇款 400：支付宝 500: 微信 600: 余额
@property (nonatomic, assign) NSInteger payMethod;
// 支付方法名称
@property (nonatomic, copy) NSString *payMethodName;
// 支付金额
@property (nonatomic, assign) CGFloat amount;
// 创建时间
@property (nonatomic, copy) NSString *createTime;
// 状态 0：作废，1：启用
@property (nonatomic, assign) NSInteger status;
// 状态名称
@property (nonatomic, copy) NSString *statusName;

@end

@interface YDOrderDetailDataItemSku : NSObject

// 订单项ID
@property (nonatomic, assign) NSInteger orderItemId;
// skuID
@property (nonatomic, assign) NSInteger skuId;
// 颜色
@property (nonatomic, copy) NSString *color;
// 尺码
@property (nonatomic, copy) NSString *size;
// 颜色ID
@property (nonatomic, copy) NSString *colorId;
// 尺码ID
@property (nonatomic, copy) NSString *sizeId;
// 欠货数量
@property (nonatomic, assign) NSInteger dueNum;
// 商品数量
@property (nonatomic, assign) NSInteger itemNum;
// 已配货数量
@property (nonatomic, assign) NSInteger allocNum;

@end

@interface YDOrderDetailDataItems : NSObject

// 商品ID
@property (nonatomic, assign) NSInteger itemId;
// 商品款号
@property (nonatomic, copy) NSString *code;
// 商品名称
@property (nonatomic, copy) NSString *itemName;
// 商品数量
@property (nonatomic, assign) NSInteger itemNum;
// 销售价格
@property (nonatomic, assign) double salesPrice;
// 标准销售价
@property (nonatomic, copy) NSString *itemSalesPrice;
// 批发价
@property (nonatomic, copy) NSString *wholePrice;
// 打包价
@property (nonatomic, copy) NSString *packagePrice;
// 金额小计(销售单价*商品数量)
@property (nonatomic, assign) CGFloat subtotalAmount;
// 商品图片
@property (nonatomic, copy) NSString *itemUrl;
// sku信息
@property (nonatomic, strong) NSMutableArray<YDOrderDetailDataItemSku *> *orderItemSkus;

@end

@interface YDOrderDetailData : NSObject

// 客户信息
@property (nonatomic, strong) YDOrderDetailDataClient *client;
// 订单详细信息
@property (nonatomic, strong) YDOrderDetailDataInfo *order;
// 支付流水
@property (nonatomic, strong) NSMutableArray<YDOrderDetailDataPayHistories *> *payHistories;
// 订单商品
@property (nonatomic, strong) NSMutableArray<YDOrderDetailDataItems *> *orderItems;
// 多媒体信息
@property (nonatomic, strong) NSMutableArray<YDSearcherPicList *> *medias;

@end

@interface YDOrderDetailModel : YDBaseModel

@property (nonatomic, strong) YDOrderDetailData *data;

@end

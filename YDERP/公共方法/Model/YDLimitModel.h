//
//  YDLimitModel.h
//  YDERP
//
//  Created by 何英健 on 2021/8/29.
//

#import "YDBaseModel.h"

@interface YDLimitData : NSObject

@property (nonatomic, assign) NSInteger ID;

@property (nonatomic, copy) NSString *code;

@property (nonatomic, copy) NSString *name;

@property (nonatomic, assign) BOOL selected;

@end

@interface YDLimitModel : YDBaseModel

@property (nonatomic, strong) NSMutableArray<YDLimitData *> *data;

@end

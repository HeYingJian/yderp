//
//  YDCustomerAddressModel.m
//  YDERP
//
//  Created by 何英健 on 2021/8/13.
//

#import "YDCustomerAddressModel.h"

@implementation YDCustomerAddressData

+ (NSDictionary<NSString *,id> *)modelCustomPropertyMapper
{
    return @{@"ID":@"id"};
}

@end

@implementation YDCustomerAddressModel

+ (NSDictionary<NSString *,id> *)modelContainerPropertyGenericClass{
    return @{@"data" : [YDCustomerAddressData class]};
}

@end

//
//  YDCheckoutOrderModel.h
//  YDERP
//
//  Created by 何英健 on 2021/8/19.
//

#import "YDBaseModel.h"

@interface YDCheckoutOrderData : YDBaseModel

// 客户姓名
@property (nonatomic, copy) NSString *clientName;
// 总金额
@property (nonatomic, assign) CGFloat totalAmount;
// 总优惠金额
@property (nonatomic, assign) CGFloat couponAmount;
// 支付金额
@property (nonatomic, assign) CGFloat payAmount;
// 耗损金额
@property (nonatomic, assign) CGFloat lossAmount;
// 总欠款金额
@property (nonatomic, assign) CGFloat dueAmount;
// 订单号
@property (nonatomic, copy) NSString *orderNo;
// 配货单ID
@property (nonatomic, assign) NSInteger allocOrderId;
// 订单id，用于打印小票时获取打印信息
@property (nonatomic, assign) NSInteger orderId;
// 支付方式名称
@property (nonatomic, strong) NSMutableArray<NSString *> *payMethodNames;

@end

@interface YDCheckoutOrderModel : YDBaseModel

@property (nonatomic, strong) YDCheckoutOrderData *data;

@end

//
//  YDLimitModel.m
//  YDERP
//
//  Created by 何英健 on 2021/8/29.
//

#import "YDLimitModel.h"

@implementation YDLimitData

+ (NSDictionary<NSString *,id> *)modelCustomPropertyMapper
{
    return @{@"ID":@"id"};
}

@end

@implementation YDLimitModel

+ (NSDictionary<NSString *,id> *)modelContainerPropertyGenericClass{
    return @{@"data" : [YDLimitData class]};
}

@end

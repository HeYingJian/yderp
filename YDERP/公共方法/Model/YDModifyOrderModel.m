//
//  YDModifyOrderModel.m
//  YDERP
//
//  Created by 何英健 on 2021/8/27.
//

#import "YDModifyOrderModel.h"

@implementation YDModifyOrderData

+ (NSDictionary<NSString *,id> *)modelCustomPropertyMapper
{
    return @{@"ID":@"id"};
}

@end

@implementation YDModifyOrderModel

@end

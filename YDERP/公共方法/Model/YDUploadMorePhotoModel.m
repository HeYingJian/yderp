//
//  YDUploadMorePhotoModel.m
//  YDERP
//
//  Created by 何英健 on 2021/8/6.
//

#import "YDUploadMorePhotoModel.h"

@implementation YDUploadMorePhotoModel

+ (NSDictionary<NSString *,id> *)modelContainerPropertyGenericClass{
    return @{@"data" : [NSString class]};
}

@end

//
//  YDCustomerAddressModel.h
//  YDERP
//
//  Created by 何英健 on 2021/8/13.
//

#import "YDBaseModel.h"

@interface YDCustomerAddressData : NSObject

@property (nonatomic, assign) NSInteger ID;
@property (nonatomic, copy) NSString *address;
// 是否默认地址
@property (nonatomic, assign) BOOL defaultFlag;

@end

@interface YDCustomerAddressModel : YDBaseModel

@property (nonatomic, strong) NSMutableArray<YDCustomerAddressData *> *data;

@end

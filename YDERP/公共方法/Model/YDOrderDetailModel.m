//
//  YDOrderDetailModel.m
//  YDERP
//
//  Created by 何英健 on 2021/8/18.
//

#import "YDOrderDetailModel.h"

@implementation YDOrderDetailDataClient

+ (NSDictionary<NSString *,id> *)modelCustomPropertyMapper
{
    return @{@"ID":@"id"};
}

@end

@implementation YDOrderDetailDataInfo

+ (NSDictionary<NSString *,id> *)modelCustomPropertyMapper
{
    return @{@"ID":@"id"};
}

@end

@implementation YDOrderDetailDataPayHistories

+ (NSDictionary<NSString *,id> *)modelCustomPropertyMapper
{
    return @{@"ID":@"id"};
}

@end

@implementation YDOrderDetailDataItemSku

@end

@implementation YDOrderDetailDataItems

+ (NSDictionary<NSString *,id> *)modelContainerPropertyGenericClass{
    return @{@"orderItemSkus" : [YDOrderDetailDataItemSku class]};
}

@end

@implementation YDOrderDetailData

+ (NSDictionary<NSString *,id> *)modelContainerPropertyGenericClass{
    return @{@"payHistories" : [YDOrderDetailDataPayHistories class],
             @"orderItems" : [YDOrderDetailDataItems class],
             @"medias" : [YDSearcherPicList class]
            };
}

@end

@implementation YDOrderDetailModel

@end

//
//  YDUploadOnePhotoModel.h
//  YDERP
//
//  Created by 何英健 on 2021/8/6.
//

#import "YDBaseModel.h"

@interface YDUploadOnePhotoModel : YDBaseModel

@property (nonatomic, copy) NSString *data;

@end

//
//  YDUserInfoModel.h
//  YDERP
//
//  Created by 何英健 on 2021/8/25.
//

#import "YDBaseModel.h"

@interface YDUserInfoData : NSObject

@property (nonatomic, assign) NSInteger ID;

// 角色id
@property (nonatomic, assign) NSInteger roleId;
// 角色名称
@property (nonatomic, copy) NSString *roleName;
// 角色代码
@property (nonatomic, copy) NSString *roleCode;
// 店铺id
@property (nonatomic, assign) NSInteger shopId;
// 店铺名称
@property (nonatomic, copy) NSString *shopName;
// 用户名
@property (nonatomic, copy) NSString *userName;
// 名称
@property (nonatomic, copy) NSString *name;
// 老板id，如果值大于0，则表明该用户从属于某个老板
@property (nonatomic, assign) NSInteger bossId;
// 联系方式
@property (nonatomic, copy) NSString *phone;
// 密码
@property (nonatomic, copy) NSString *password;
// 是否管理员, 0：否，1：是
@property (nonatomic, assign) NSInteger adminInd;
// 头像图片url
@property (nonatomic, copy) NSString *headImg;
// 状态 0：停用 1：启用
@property (nonatomic, assign) NSInteger status;
// 备注
@property (nonatomic, copy) NSString *remark;
// 租户ID
@property (nonatomic, assign) NSInteger tenantId;
// 创建人
@property (nonatomic, copy) NSString *createPerson;
// 创建时间
@property (nonatomic, copy) NSString *createTime;
// 修改人
@property (nonatomic, copy) NSString *updatePerson;
// 最近一次修改时间
@property (nonatomic, copy) NSString *updateTime;
// 套餐是否过期，0：未过期；1：已过期
@property (nonatomic, assign) NSInteger feePlanExpire;
// 逻辑删除标志
@property (nonatomic, assign) NSInteger dr;

@end

@interface YDUserInfoModel : YDBaseModel

@property (nonatomic, strong) YDUserInfoData *data;

@end

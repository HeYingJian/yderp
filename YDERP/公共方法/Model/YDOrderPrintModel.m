//
//  YDOrderPrintModel.m
//  YDERP
//
//  Created by 何英健 on 2021/9/1.
//

#import "YDOrderPrintModel.h"

@implementation YDOrderPrintDataShop

+ (NSDictionary<NSString *,id> *)modelContainerPropertyGenericClass{
    return @{@"paySettings" : [YDCollectMoneyData class]};
}

@end

@implementation YDOrderPrintDataItem

@end

@implementation YDOrderPrintDataInfo

@end

@implementation YDOrderPrintData

+ (NSDictionary<NSString *,id> *)modelContainerPropertyGenericClass{
    return @{@"orderItems" : [YDOrderPrintDataItem class]};
}

@end

@implementation YDOrderPrintModel

@end

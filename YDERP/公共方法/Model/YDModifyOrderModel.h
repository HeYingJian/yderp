//
//  YDModifyOrderModel.h
//  YDERP
//
//  Created by 何英健 on 2021/8/27.
//

#import "YDBaseModel.h"

@interface YDModifyOrderData : NSObject

// 订单ID
@property (nonatomic, assign) NSInteger ID;
// 订单号
@property (nonatomic, copy) NSString *orderNo;
// 店铺id
@property (nonatomic, assign) NSInteger shopId;
// 客户ID
@property (nonatomic, assign) NSInteger clientId;
// 客户地址
@property (nonatomic, copy) NSString *clientAddress;
// 当前订单客户的账户余额
@property (nonatomic, assign) CGFloat balance;
// 订单状态 0：挂单 10:待配货 20:部分配货 30：已配齐 40：已作废
@property (nonatomic, assign) NSInteger status;
// 挂单开始时间
@property (nonatomic, copy) NSString *pendingStartTime;
// 挂单结束时间
@property (nonatomic, copy) NSString *pendingEndTime;
// 已付货款金额
@property (nonatomic, assign) CGFloat payItemAmount;
// 实付金额
@property (nonatomic, assign) CGFloat payAmount;
// 欠款金额
@property (nonatomic, assign) CGFloat dueAmount;
// 优惠金额
@property (nonatomic, assign) CGFloat couponAmount;
// 应付金额
@property (nonatomic, assign) CGFloat totalAmount;
// 商品总金额
@property (nonatomic, assign) CGFloat itemAmount;
// 总商品数量
@property (nonatomic, assign) NSInteger totalItemNum;
// 损耗扣款金额
@property (nonatomic, assign) CGFloat lossAmount;
// 销售员id
@property (nonatomic, assign) NSInteger salesId;
// 订单时间
@property (nonatomic, copy) NSString *orderTime;
// 订单备注
@property (nonatomic, copy) NSString *remark;
// 版本号
@property (nonatomic, copy) NSString *version;
// 是否有改单 0:否；1:是
@property (nonatomic, assign) NSInteger isModify;
// 租户ID
@property (nonatomic, assign) NSInteger tenantId;
// 创建人
@property (nonatomic, copy) NSString *createPerson;
// 创建时间
@property (nonatomic, copy) NSString *createTime;
// 修改人
@property (nonatomic, copy) NSString *updatePerson;
// 最近一次修改时间
@property (nonatomic, copy) NSString *updateTime;
// 逻辑删除标志
@property (nonatomic, assign) NSInteger dr;

@end

@interface YDModifyOrderModel : YDBaseModel

@property (nonatomic, strong) YDModifyOrderData *data;

@end

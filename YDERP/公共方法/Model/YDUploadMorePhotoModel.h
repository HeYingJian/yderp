//
//  YDUploadMorePhotoModel.h
//  YDERP
//
//  Created by 何英健 on 2021/8/6.
//

#import "YDBaseModel.h"

@interface YDUploadMorePhotoModel : YDBaseModel

@property (nonatomic, strong) NSMutableArray<NSString *> *data;

@end

//
//  YDProductSkuGroupModel.m
//  YDERP
//
//  Created by 何英健 on 2021/8/19.
//

#import "YDProductSkuGroupModel.h"

@implementation YDProductSkuGroupModel

+ (NSDictionary<NSString *,id> *)modelContainerPropertyGenericClass{
    return @{@"data" : [YDSearcherSkuGroupData class]};
}

@end

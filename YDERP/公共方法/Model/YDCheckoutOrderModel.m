//
//  YDCheckoutOrderModel.m
//  YDERP
//
//  Created by 何英健 on 2021/8/19.
//

#import "YDCheckoutOrderModel.h"

@implementation YDCheckoutOrderData

+ (NSDictionary<NSString *,id> *)modelContainerPropertyGenericClass{
    return @{@"payMethodNames" : [NSString class]};
}

@end

@implementation YDCheckoutOrderModel

@end

//
//  YDSubmitOrderModel.h
//  YDERP
//
//  Created by 何英健 on 2021/8/18.
//

#import "YDBaseModel.h"

@interface YDSubmitOrderData : NSObject

// 订单id
@property (nonatomic, copy) NSString *orderId;
// 订单创建时间
@property (nonatomic, copy) NSString *orderTime;
// 版本号
@property (nonatomic, copy) NSString *version;

@end

@interface YDSubmitOrderModel : YDBaseModel

@property (nonatomic, strong) YDSubmitOrderData *data;

@end

//
//  YDUserInfoModel.m
//  YDERP
//
//  Created by 何英健 on 2021/8/25.
//

#import "YDUserInfoModel.h"

@implementation YDUserInfoData

+ (NSDictionary<NSString *,id> *)modelCustomPropertyMapper
{
    return @{@"ID":@"id"};
}

@end

@implementation YDUserInfoModel

@end

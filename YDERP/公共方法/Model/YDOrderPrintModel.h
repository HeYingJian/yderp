//
//  YDOrderPrintModel.h
//  YDERP
//
//  Created by 何英健 on 2021/9/1.
//

#import "YDBaseModel.h"
#import "YDCollectMoneyModel.h"

@interface YDOrderPrintDataShop : NSObject

// 店铺名称
@property (nonatomic, copy) NSString *shopName;
// 店铺地址
@property (nonatomic, copy) NSString *shopAddr;
// 联系方式
@property (nonatomic, copy) NSString *shopMobile;
// 实付设置
@property (nonatomic, strong) NSMutableArray<YDCollectMoneyData *> *paySettings;

@end

@interface YDOrderPrintDataItem : NSObject

// 商品名称
@property (nonatomic, copy) NSString *itemName;
// 商品款号
@property (nonatomic, copy) NSString *itemCode;
// 颜色
@property (nonatomic, copy) NSString *color;
// 尺码
@property (nonatomic, copy) NSString *size;
// 商品数量
@property (nonatomic, assign) NSInteger itemNum;
// 销售价格
@property (nonatomic, assign) double salesPrice;
// 金额小计(销售单价*商品数量)
@property (nonatomic, assign) CGFloat subtotalAmount;

@end

@interface YDOrderPrintDataInfo : NSObject

// 订单号
@property (nonatomic, copy) NSString *orderNo;
// 员工姓名
@property (nonatomic, copy) NSString *salesName;
// 客户姓名
@property (nonatomic, copy) NSString *clientName;
// 客户电话
@property (nonatomic, copy) NSString *clientPhone;
// 总数量
@property (nonatomic, assign) NSInteger totalNum;
// 商品金额
@property (nonatomic, assign) CGFloat itemAmount;
// 应付金额
@property (nonatomic, assign) CGFloat totalAmount;
// 已付金额
@property (nonatomic, assign) CGFloat payAmount;
// 优惠金额
@property (nonatomic, assign) CGFloat couponAmount;
// 开单时间
@property (nonatomic, copy) NSString *orderTime;
// 订单备注
@property (nonatomic, copy) NSString *remark;
// 当前订单欠款
@property (nonatomic, assign) CGFloat currDueAmount;
// 已付货款金额
@property (nonatomic, assign) CGFloat payItemAmount;

@end

@interface YDOrderPrintData : NSObject

@property (nonatomic, strong) YDOrderPrintDataInfo *order;
@property (nonatomic, strong) NSMutableArray<YDOrderPrintDataItem *> *orderItems;
@property (nonatomic, strong) YDOrderPrintDataShop *shop;

@end

@interface YDOrderPrintModel : YDBaseModel

@property (nonatomic, strong) YDOrderPrintData *data;

@end

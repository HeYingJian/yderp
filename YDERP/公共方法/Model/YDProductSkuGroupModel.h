//
//  YDProductSkuGroupModel.h
//  YDERP
//
//  Created by 何英健 on 2021/8/19.
//

#import "YDBaseModel.h"

@interface YDProductSkuGroupModel : YDBaseModel

@property (nonatomic, strong) NSMutableArray<YDSearcherSkuGroupData *> *data;

@end

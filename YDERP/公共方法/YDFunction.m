//
//  YDFunction.m
//  YDERP
//
//  Created by 何英健 on 2021/7/27.
//

#import "YDFunction.h"

@implementation YDFunction

+ (void)showCustomer
{
    [YDWebViewVC showWebViewWithUrl:@"https://rfgmc.udesk.cn/im_client/?web_plugin_id=175061&agent_id=238021" isHideNav:NO];
}

+ (void)loginSucceedWithLoginData:(YDLoginData *)data
{
    [YDUserModel setUserModelWithLoginData:data];
    [kAppDelegate initRootVC];
}

+ (void)logout
{
    [YDLoginFunction logoutWithSucceedBlock:nil faildBlock:nil];
    [YDUserModel clear];
    [kAppDelegate initRootVC];
}

+ (void)loadUserInfoOnCompletion:(BlockLoadUserInfo)completion failure:(BlockUploadImgFaild)failure
{
    NSString *path = [NSString stringWithFormat:@"%@/%ld", URL_USER_INFO, (long)UserModel.ID];
    NSMutableDictionary *param = [NSMutableDictionary dictionary];

    AppleServerPostCompleteBlock postHandleBlock = ^(BOOL done, id _Nullable object, NSString * _Nullable error) {
        if (done) {
            YDUserInfoModel *resp = [YDUserInfoModel modelWithJSON:object];
            
            if (resp.code == 200) {
                UserModel.roleId = resp.data.roleId;
                UserModel.roleName = resp.data.roleName;
                UserModel.roleCode = resp.data.roleCode;
                UserModel.shopId = resp.data.shopId;
                UserModel.shopName = resp.data.shopName;
                UserModel.userName = resp.data.userName;
                UserModel.name = resp.data.name;
                UserModel.bossId = resp.data.bossId;
                UserModel.phone = resp.data.phone;
                UserModel.headImg = resp.data.headImg;
                UserModel.feePlanExpire = resp.data.feePlanExpire;
                
                if (completion) {
                    completion(resp.data);
                }
            
            } else if (resp.code == 3001) {
                // 重新登录
                [YDFunction logout];
                
                if (failure) {
                    failure(@"登录信息已失效，请重新登录");
                }
                
            } else {
                if (resp.code == 3009) {
                    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1.0f * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                        // 商户被禁用
                        [YDFunction logout];
                    });
                }
                
                if (failure) {
                    NSString *msg;
                    if (resp.detailMessage.length) {
                        msg = resp.detailMessage;
                        
                    } else if (resp.message.length) {
                        msg = resp.message;
                        
                    } else {
                        msg = @"网络失败";
                    }
                    
                    failure(msg);
                }
            }
            
        } else {
            if (failure) {
                failure(@"网络不给力");
            }
        }
    };
    
    [AppleServer.shared getWithUrl:path Paramters:param Callback:postHandleBlock];
}

+ (void)loadUserLimitOnCompletion:(BlockLoadDataSucceed)completion failure:(BlockUploadImgFaild)failure
{
    NSString *path = URL_RESOURCE_LIST;
    NSMutableDictionary *param = [NSMutableDictionary dictionary];
    [param setObject:[NSNumber numberWithInteger:UserModel.roleId] forKey:@"roleId"];

    AppleServerPostCompleteBlock postHandleBlock = ^(BOOL done, id _Nullable object, NSString * _Nullable error) {
        if (done) {
            YDLimitModel *resp = [YDLimitModel modelWithJSON:object];
            if (resp.code == 200) {
                UserLimit.data = resp.data;

                if (completion) {
                    completion();
                }

            } else {
                NSString *msg;
                if (resp.detailMessage.length) {
                    msg = resp.detailMessage;
                    
                } else if (resp.message.length) {
                    msg = resp.message;
                    
                } else {
                    msg = @"网络失败";
                }
                if (failure) {
                    failure(msg);
                }
            }
            
        } else {
            if (failure) {
                failure(@"网络不给力");
            }
        }
    };
    
    [AppleServer.shared getWithUrl:path Paramters:param Callback:postHandleBlock];
}

+ (void)uploadImageArray:(NSArray<UIImage *> *)imageArray type:(NSInteger)type group:(NSInteger)group groupStr:(NSString *)groupStr parameters:(id)parameters sucess:(BlockUploadImgSucceed)sucess failure:(BlockUploadImgFaild)failure
{
    NSString *contentTypeName;
    NSString *path;
    if (imageArray.count == 1) {
        path = @"media/upload";
        contentTypeName = @"file";
        
    } else {
        path = @"media/upload-batch";
        contentTypeName = @"files";
    }
    path = [NSString stringWithFormat:@"%@/%@", API_URL, path];
    
    NSString *typeStr;
    if (type == 2) {
        typeStr = @"video";
        
    } else {
        typeStr = @"image";
    }
    path = [NSString stringWithFormat:@"%@/%@", path, typeStr];
    
    NSString *str;
    if (groupStr.length) {
        str = groupStr;
        
    } else {
        switch (group) {
            case 1:
                str = @"item";
                break;
            case 2:
                str = @"sales";
                break;
            case 3:
                str = @"alloc";
                break;
            case 4:
                str = @"delivery";
                break;
            case 5:
                str = @"payment";
                break;
            case 6:
                str = @"user";
                break;
                
            default:
                break;
        }
    }
    path = [NSString stringWithFormat:@"%@/%@", path, str];
    
    dispatch_async(dispatch_get_global_queue(0, 0), ^{
        // 处理耗时操作的代码块...
        NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:path]];
        [AppleServer setUpRequest:request];
        
        request.HTTPMethod = @"POST";
        //设置请求实体
        NSMutableData *body = [NSMutableData data];
        if (parameters != nil) {
            for (NSString *key in parameters) {
                //循环参数按照部分1、2、3那样循环构建每部分数据
                NSString *pair = [NSString stringWithFormat:@"--%@\r\nContent-Disposition: form-data; name=\"%@\"\r\n\r\n",@"BOUNDARY",key];
                [body appendData:[pair dataUsingEncoding:NSUTF8StringEncoding]];
                
                id value = [parameters objectForKey:key];
                if ([value isKindOfClass:[NSString class]]) {
                    [body appendData:[value dataUsingEncoding:NSUTF8StringEncoding]];
                }else if ([value isKindOfClass:[NSData class]]){
                    [body appendData:value];
                }
                [body appendData:[@"\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
            }
        }
        
        for (int i = 0; i < imageArray.count; i++) {
            ///文件参数
            UIImage *img = imageArray[i];
            NSData *imageData = [NSData data];
            NSString *imageFormat = @"";
            if (UIImagePNGRepresentation(img) != nil) {
                imageFormat = @"Content-Type: image/png \r\n";
                
            } else {
                imageFormat = @"Content-Type: image/jpeg \r\n";
            }
            imageData = [UIImage compressImageToData:img maxLength:128 * 1000];
            
            [body appendData:[self getDataWithString:@"--BOUNDARY\r\n" ]];
            NSString *disposition = [NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@\"; filename=\"%d.jpg\"\r\n", contentTypeName, i + 1];
            [body appendData:[self getDataWithString:disposition]];
            [body appendData:[self getDataWithString:imageFormat]];
            [body appendData:[self getDataWithString:@"\r\n"]];
            [body appendData:imageData];
            [body appendData:[self getDataWithString:@"\r\n"]];
            //普通参数
            [body appendData:[self getDataWithString:@"--BOUNDARY\r\n" ]];
            NSString *dispositions = [NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@\"\r\n",@"key"];
            [body appendData:[self getDataWithString:dispositions ]];
            [body appendData:[self getDataWithString:@"\r\n"]];
            [body appendData:[self getDataWithString:[NSString stringWithFormat:@"%d", i + 1]]];//filename
            [body appendData:[self getDataWithString:@"\r\n"]];
        }
        
        //参数结束
        [body appendData:[self getDataWithString:@"--BOUNDARY--\r\n"]];
        request.HTTPBody = body;
        //设置请求体长度
        NSInteger length = [body length];
        [request setValue:[NSString stringWithFormat:@"%ld",(long)length] forHTTPHeaderField:@"Content-Length"];
        //设置 POST请求文件上传
        [request setValue:@"multipart/form-data; boundary=BOUNDARY" forHTTPHeaderField:@"Content-Type"];
        
        NSURLSession *session = [NSURLSession sharedSession];
        NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:request completionHandler:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error) {
            dispatch_async(dispatch_get_main_queue(), ^{
                YDBaseModel *resp = [YDBaseModel modelWithJSON:data];

                if (resp != nil) {
                    if (resp.code == 200) {
                        if (sucess) {
                            if (imageArray.count > 1) {
                                YDUploadMorePhotoModel *model = [YDUploadMorePhotoModel modelWithJSON:data];
                                sucess(model.data);
                                
                            } else {
                                YDUploadOnePhotoModel *model = [YDUploadOnePhotoModel modelWithJSON:data];
                                NSMutableArray *array = [NSMutableArray arrayWithObject:model.data];
                                sucess(array);
                            }
                        }
                        
                    } else {
                        if (failure) {
                            NSString *msg;
                            if (resp.detailMessage.length) {
                                msg = resp.detailMessage;
                                
                            } else if (resp.message.length) {
                                msg = resp.message;
                            }
                            failure(msg);
                        }
                    }
                    
                } else {
                    if (failure) {
                        if (error) {
                            failure(error.description);
                            
                        } else {
                            failure(@"网络不给力");
                        }
                    }
                }
            });
        }];
        //开始任务
        [dataTask resume];
    });
}

+ (NSMutableArray<YDSearcherSkuGroupData *> *)getSkuGroupArrayFromOrderSkuArray:(NSMutableArray<YDOrderDetailDataItemSku *> *)orderSkuArray
{
    // 从skuList中获取颜色和尺码
    NSMutableArray<YDSearcherSkuGroupData *> *colorArray = [NSMutableArray array];
    
    for (YDOrderDetailDataItemSku *sku in orderSkuArray) {
        YDSearcherSkuList *skuData = [[YDSearcherSkuList alloc] init];
        skuData.ID = [NSString stringWithFormat:@"%ld", (long)sku.skuId];
        skuData.color = sku.color;
        skuData.colorId = sku.colorId;
        skuData.size = sku.size;
        skuData.sizeId = sku.sizeId;
        skuData.dueNum = sku.dueNum;
        skuData.allocNum = sku.allocNum;
        skuData.selectedNum = sku.itemNum;
        
        BOOL exist = NO;
        for (YDSearcherSkuGroupData *groupData in colorArray) {
            if ([groupData.colorId isEqualToString:sku.colorId]) {
                exist = YES;
                // 本身有对应颜色的object
                [groupData.skuList addObject:skuData];
            }
        }
        
        if (!exist) {
            // 没有对应颜色的object
            YDSearcherSkuGroupData *colorObj = [[YDSearcherSkuGroupData alloc] init];
            colorObj.color = sku.color;
            colorObj.colorId = sku.colorId;
            colorObj.skuList = [NSMutableArray arrayWithObject:skuData];
            [colorArray addObject:colorObj];
        }
    }
    
    return colorArray;
}

+ (NSMutableArray<YDSearcherSkuGroupData *> *)getSkuGroupArrayFromSupplySkuArray:(NSMutableArray<YDSupplyDetailDataItemSku *> *)orderSkuArray
{
    // 从skuList中获取颜色和尺码
    NSMutableArray<YDSearcherSkuGroupData *> *colorArray = [NSMutableArray array];
    
    for (YDSupplyDetailDataItemSku *sku in orderSkuArray) {
        YDSearcherSkuList *skuData = [[YDSearcherSkuList alloc] init];
        skuData.ID = [NSString stringWithFormat:@"%ld", (long)sku.skuId];
        skuData.color = sku.color;
        skuData.colorId = sku.colorId;
        skuData.size = sku.size;
        skuData.sizeId = sku.sizeId;
        skuData.dueNum = sku.dueNum;
        skuData.deliveryNum = sku.deliveryNum;
        skuData.stockNum = sku.stockNum;
        skuData.selectedNum = sku.selectedNum;
        
        BOOL exist = NO;
        for (YDSearcherSkuGroupData *groupData in colorArray) {
            if ([groupData.colorId isEqualToString:sku.colorId]) {
                exist = YES;
                // 本身有对应颜色的object
                [groupData.skuList addObject:skuData];
            }
        }
        
        if (!exist) {
            // 没有对应颜色的object
            YDSearcherSkuGroupData *colorObj = [[YDSearcherSkuGroupData alloc] init];
            colorObj.color = sku.color;
            colorObj.colorId = sku.colorId;
            colorObj.skuList = [NSMutableArray arrayWithObject:skuData];
            [colorArray addObject:colorObj];
        }
    }
    
    return colorArray;
}

+ (void)getCustomerAddressListWithCustomerID:(NSInteger)customerID completion:(BlockGetAddressSucceed)completion
{
    [YDLoadingView showToSuperview:nil];
    
    NSString *path = URL_CLIENT_ADDRESS;
    path = [NSString stringWithFormat:@"%@/%ld/list", path, (long)customerID];
    NSMutableDictionary *param = [NSMutableDictionary dictionary];

    AppleServerPostCompleteBlock postHandleBlock = ^(BOOL done, id _Nullable object, NSString * _Nullable error) {
        [YDLoadingView hideToSuperview:nil];
        
        if (done) {
            YDCustomerAddressModel *resp = [YDCustomerAddressModel modelWithJSON:object];
            
            if (resp.code == 200) {
                if (completion) {
                    completion(resp.data);
                }
            
            } else {
                NSString *msg;
                if (resp.detailMessage.length) {
                    msg = resp.detailMessage;
                    
                } else if (resp.message.length) {
                    msg = resp.message;
                    
                } else {
                    msg = @"网络失败";
                }
                [MBProgressHUD showToastMessage:msg];
            }
            
        } else {
            [MBProgressHUD showToastMessage:@"网络不给力"];
        }
    };
    
    [AppleServer.shared getWithUrl:path Paramters:param Callback:postHandleBlock];
}

+ (void)modifyCustomerAddressWithCustomerID:(NSInteger)customerID addressID:(NSString *)addressID address:(NSString *)address isDefaultAddress:(BOOL)isDefaultAddress completion:(BlockLoadDataSucceed)completion faildBlock:(BlockUploadImgFaild)faildBlock
{
    if (!address.length) {
        faildBlock(@"地址为空");
        return;
    }

    NSString *path = URL_CLIENT_ADDRESS;
    path = [NSString stringWithFormat:@"%@/%ld/save", path, (long)customerID];
    
    NSMutableDictionary *param = [NSMutableDictionary dictionary];
    if (addressID.length) {
        [param setObject:addressID forKey:@"id"];
    }
    
    [param setObject:address forKey:@"address"];
    
    if (isDefaultAddress) {
        [param setObject:[NSNumber numberWithBool:isDefaultAddress] forKey:@"defaultFlag"];
    }

    AppleServerPostCompleteBlock postHandleBlock = ^(BOOL done, id _Nullable object, NSString * _Nullable error) {
        if (done) {
            YDBaseModel *resp = [YDBaseModel modelWithJSON:object];
            
            if (resp.code == 200) {
                if (completion) {
                    completion();
                }
            
            } else {
                if (faildBlock) {
                    NSString *msg;
                    if (resp.detailMessage.length) {
                        msg = resp.detailMessage;
                        
                    } else if (resp.message.length) {
                        msg = resp.message;
                        
                    } else {
                        msg = @"网络失败";
                    }
                    faildBlock(msg);
                }
            }
            
        } else {
            if (faildBlock) {
                faildBlock(@"网络不给力");
            }
        }
    };
    
    //使用AppleServer发送POST请求
    [AppleServer.shared postWithPath:path Paramters:param CompleteBlock:postHandleBlock];
}

+ (void)submitOrderWithOrderID:(NSString *)orderID customerID:(NSString *)customerID address:(NSString *)address productArray:(NSMutableArray<YDSearcherProductData *> *)productArray completion:(BlockSubmitOrderSucceed)completion refreshBlock:(BlockUploadImgFaild)refreshBlock faildBlock:(BlockUploadImgFaild)faildBlock
{
    if (!productArray.count) {
        faildBlock(@"商品信息为空");
        return;
    }
    
    NSString *path = URL_SUBMIT_ORDER;
    
    NSMutableDictionary *param = [NSMutableDictionary dictionary];
    if (orderID.length) {
        [param setObject:orderID forKey:@"oldOrderId"];
    }
    if (customerID.length) {
        [param setObject:customerID forKey:@"clientId"];
    }
    if (address.length) {
        [param setObject:address forKey:@"clientAddress"];
    }
    
    NSMutableArray *array = [NSMutableArray array];
    for (YDSearcherProductData *data in productArray) {
        for (YDSearcherSkuGroupData *sub in data.skuGroupList) {
            for (YDSearcherSkuList *sku in sub.skuList) {
                if (sku.selectedNum > 0) {
                    NSMutableDictionary *dict = [NSMutableDictionary dictionary];
                    [dict setObject:sku.ID forKey:@"skuId"];
                    [dict setObject:[NSNumber numberWithInteger:sku.selectedNum] forKey:@"itemNum"];
                    [dict setObject:[NSString appleStandardPriceWith:data.salesPrice] forKey:@"salesPrice"];
                    [array addObject:dict];
                }
            }
        }
    }
    [param setObject:array forKey:@"orderItems"];

    AppleServerPostCompleteBlock postHandleBlock = ^(BOOL done, id _Nullable object, NSString * _Nullable error) {
        if (done) {
            YDSubmitOrderModel *resp = [YDSubmitOrderModel modelWithJSON:object];
            
            if (resp.code == 200) {
                if (completion) {
                    completion(resp.data);
                }
            
            } else {
                NSString *msg;
                if (resp.detailMessage.length) {
                    msg = resp.detailMessage;
                    
                } else if (resp.message.length) {
                    msg = resp.message;
                    
                } else {
                    msg = @"网络失败";
                }
                
                if (resp.code == 5002 || resp.code == 9002 || resp.code == 9000) {
                    if (refreshBlock) {
                        refreshBlock(msg);
                    }
                    
                } else {
                    if (faildBlock) {
                        faildBlock(msg);
                    }
                }
            }
            
        } else {
            if (faildBlock) {
                faildBlock(@"网络不给力");
            }
        }
    };
    
    //使用AppleServer发送POST请求
    [AppleServer.shared postWithPath:path Paramters:param CompleteBlock:postHandleBlock];
}

+ (void)checkOrderVersionWithOrderID:(NSString *)orderID version:(NSString *)version completion:(BlockLoadDataSucceed)completion refreshBlock:(BlockUploadImgFaild)refreshBlock faildBlock:(BlockUploadImgFaild)faildBlock
{
    NSString *path = [NSString stringWithFormat:@"%@/%@/%@", URL_ORDER_CHECK_VERSION, orderID, version];
    NSMutableDictionary *param = [NSMutableDictionary dictionary];

    AppleServerPostCompleteBlock postHandleBlock = ^(BOOL done, id _Nullable object, NSString * _Nullable error) {
        if (done) {
            YDOrderDetailModel *resp = [YDOrderDetailModel modelWithJSON:object];

            if (resp.code == 200) {
                if (completion) {
                    completion();
                }
                
            } else {
                NSString *msg;
                if (resp.detailMessage.length) {
                    msg = resp.detailMessage;
                    
                } else if (resp.message.length) {
                    msg = resp.message;
                    
                } else {
                    msg = @"网络失败";
                }
                
                if (resp.code == 5002 || resp.code == 9002 || resp.code == 9000) {
                    if (refreshBlock) {
                        refreshBlock(msg);
                    }
                    
                } else {
                    if (faildBlock) {
                        faildBlock(msg);
                    }
                }
            }
            
        } else {
            if (faildBlock) {
                faildBlock(@"网络不给力");
            }
        }
    };
    
    [AppleServer.shared getWithUrl:path Paramters:param Callback:postHandleBlock];
}

+ (void)getOrderDetailWithType:(NSInteger)type orderID:(NSString *)orderID completion:(BlockLoadOrderDetailSucceed)completion refreshBlock:(BlockUploadImgFaild)refreshBlock faildBlock:(BlockUploadImgFaild)faildBlock
{
    NSString *path;
    if (type == 0) {
        path = URL_ORDER_DETAIL;
        
    } else {
        path = URL_ORDER_DETAIL_OLD;
    }
    path = [NSString stringWithFormat:@"%@/%@", path, orderID];
    NSMutableDictionary *param = [NSMutableDictionary dictionary];

    AppleServerPostCompleteBlock postHandleBlock = ^(BOOL done, id _Nullable object, NSString * _Nullable error) {
        if (done) {
            YDOrderDetailModel *resp = [YDOrderDetailModel modelWithJSON:object];
            
            if (resp.code == 200) {
                if (completion) {
                    completion(resp.data);
                }
            
            } else {
                NSString *msg;
                if (resp.detailMessage.length) {
                    msg = resp.detailMessage;
                    
                } else if (resp.message.length) {
                    msg = resp.message;
                    
                } else {
                    msg = @"网络失败";
                }
                
                if (resp.code == 5002 || resp.code == 9002 || resp.code == 9000) {
                    if (refreshBlock) {
                        refreshBlock(msg);
                    }
                    
                } else {
                    if (faildBlock) {
                        faildBlock(msg);
                    }
                }
            }
            
        } else {
            if (faildBlock) {
                faildBlock(@"网络不给力");
            }
        }
    };
    
    [AppleServer.shared getWithUrl:path Paramters:param Callback:postHandleBlock];
}

+ (void)modifyOrderWithOrderID:(NSString *)orderID version:(NSString *)version productArray:(NSMutableArray<YDSearcherProductData *> *)productArray completion:(BlockModifyOrderSucceed)completion refreshBlock:(BlockUploadImgFaild)refreshBlock faildBlock:(BlockUploadImgFaild)faildBlock
{
    if (!productArray.count) {
        faildBlock(@"商品信息为空");
        return;
    }
    
    NSString *path = URL_ORDER_DETAIL;
    
    NSMutableDictionary *param = [NSMutableDictionary dictionary];
    if (orderID.length) {
        [param setObject:orderID forKey:@"orderId"];
    }
    if (version.length) {
        [param setObject:version forKey:@"version"];
    }
    
    NSMutableArray *array = [NSMutableArray array];
    for (YDSearcherProductData *data in productArray) {
        for (YDSearcherSkuGroupData *sub in data.skuGroupList) {
            for (YDSearcherSkuList *sku in sub.skuList) {
                if (sku.selectedNum > 0) {
                    NSMutableDictionary *dict = [NSMutableDictionary dictionary];
                    [dict setObject:sku.ID forKey:@"skuId"];
                    [dict setObject:[NSNumber numberWithInteger:sku.selectedNum] forKey:@"itemNum"];
                    [dict setObject:[NSString appleStandardPriceWith:data.salesPrice] forKey:@"salesPrice"];
                    [array addObject:dict];
                }
            }
        }
    }
    [param setObject:array forKey:@"orderItems"];

    AppleServerPostCompleteBlock postHandleBlock = ^(BOOL done, id _Nullable object, NSString * _Nullable error) {
        if (done) {
            YDModifyOrderModel *resp = [YDModifyOrderModel modelWithJSON:object];
            
            if (resp.code == 200) {
                if (completion) {
                    completion(resp.data);
                }
            
            } else {
                NSString *msg;
                if (resp.detailMessage.length) {
                    msg = resp.detailMessage;
                    
                } else if (resp.message.length) {
                    msg = resp.message;
                    
                } else {
                    msg = @"网络失败";
                }
                
                if (resp.code == 5002 || resp.code == 9002 || resp.code == 9000) {
                    if (refreshBlock) {
                        refreshBlock(msg);
                    }
                    
                } else {
                    if (faildBlock) {
                        faildBlock(msg);
                    }
                }
            }
            
        } else {
            if (faildBlock) {
                faildBlock(@"网络不给力");
            }
        }
    };
    
    //使用AppleServer发送POST请求
    [AppleServer.shared postWithPath:path Paramters:param CompleteBlock:postHandleBlock];
}

+ (void)getPrintInfoWithOrderID:(NSString *)orderID completion:(BlockLoadOrderPrintSucceed)completion faildBlock:(BlockUploadImgFaild)faildBlock
{
    if (!orderID.length) {
        faildBlock(@"订单id不存在");
        return;
    }
    
    NSString *path = [NSString stringWithFormat:@"%@/%@", URL_ORDER_PRINT, orderID];

    NSMutableDictionary *param = [NSMutableDictionary dictionary];

    AppleServerPostCompleteBlock postHandleBlock = ^(BOOL done, id _Nullable object, NSString * _Nullable error) {
        if (done) {
            YDOrderPrintModel *resp = [YDOrderPrintModel modelWithJSON:object];
            
            if (resp.code == 200) {
                if (completion) {
                    completion(resp.data);
                }
            
            } else {
                NSString *msg;
                if (resp.detailMessage.length) {
                    msg = resp.detailMessage;
                    
                } else if (resp.message.length) {
                    msg = resp.message;
                    
                } else {
                    msg = @"网络失败";
                }
                faildBlock(msg);
            }
            
        } else {
            faildBlock(@"网络不给力");
        }
    };
    
    [AppleServer.shared getWithUrl:path Paramters:param Callback:postHandleBlock];
}

+ (void)getProductSkuGroupWithOrderID:(NSString *)orderID productID:(NSString *)productID completion:(BlockProductSkuGroupSucceed)completion faildBlock:(BlockUploadImgFaild)faildBlock
{
    if (!orderID.length || !productID.length) {
        faildBlock(@"订单id或商品id不存在");
        return;
    }
    
    NSString *path = URL_ORDER_SKU_GROUP;

    NSMutableDictionary *param = [NSMutableDictionary dictionary];
    [param setObject:orderID forKey:@"orderId"];
    [param setObject:productID forKey:@"id"];

    AppleServerPostCompleteBlock postHandleBlock = ^(BOOL done, id _Nullable object, NSString * _Nullable error) {
        if (done) {
            YDProductSkuGroupModel *resp = [YDProductSkuGroupModel modelWithJSON:object];
            
            if (resp.code == 200) {
                if (completion) {
                    completion(resp.data);
                }
            
            } else {
                NSString *msg;
                if (resp.detailMessage.length) {
                    msg = resp.detailMessage;
                    
                } else if (resp.message.length) {
                    msg = resp.message;
                    
                } else {
                    msg = @"网络失败";
                }
                faildBlock(msg);
            }
            
        } else {
            faildBlock(@"网络不给力");
        }
    };
    
    [AppleServer.shared getWithUrl:path Paramters:param Callback:postHandleBlock];
}

+ (void)cancelPayHistoryWithPayID:(NSString *)payID completion:(BlockLoadDataSucceed)completion refreshBlock:(BlockUploadImgFaild)refreshBlock faildBlock:(BlockUploadImgFaild)faildBlock
{
    NSString *path = URL_CANCEL_PAY_HISTORY;
    path = [NSString stringWithFormat:@"%@/%@", path, payID];
    
    NSMutableDictionary *param = [NSMutableDictionary dictionary];

    AppleServerPostCompleteBlock postHandleBlock = ^(BOOL done, id _Nullable object, NSString * _Nullable error) {
        if (done) {
            YDBaseModel *resp = [YDBaseModel modelWithJSON:object];
            
            if (resp.code == 200) {
                if (completion) {
                    completion();
                }
            
            } else {
                NSString *msg;
                if (resp.detailMessage.length) {
                    msg = resp.detailMessage;
                    
                } else if (resp.message.length) {
                    msg = resp.message;
                    
                } else {
                    msg = @"网络失败";
                }
                
                if (resp.code == 5002 || resp.code == 9002 || resp.code == 9000) {
                    if (refreshBlock) {
                        refreshBlock(msg);
                    }
                    
                } else {
                    if (faildBlock) {
                        faildBlock(msg);
                    }
                }
            }
            
        } else {
            if (faildBlock) {
                faildBlock(@"网络不给力");
            }
        }
    };
    
    //使用AppleServer发送POST请求
    [AppleServer.shared postWithPath:path Paramters:param CompleteBlock:postHandleBlock];
}

+ (void)checkoutWithOrderID:(NSString *)orderID couponAmount:(NSString *)couponAmount cashPay:(NSString *)cashPay cardPay:(NSString *)cardPay remitPay:(NSString *)remitPay aliPay:(NSString *)aliPay wechatPay:(NSString *)wechatPay remark:(NSString *)remark urlArray:(NSMutableArray<NSString *> *)urlArray version:(NSString *)version completion:(BlockCheckoutOrder)completion refreshBlock:(BlockUploadImgFaild)refreshBlock faildBlock:(BlockUploadImgFaild)faildBlock
{
    if (!orderID.length) {
        faildBlock(@"订单id为空");
        return;
    }
    
    NSString *path = URL_ORDER_CHECKOUT;
    
    NSMutableDictionary *param = [NSMutableDictionary dictionary];
    [param setObject:orderID forKey:@"orderId"];
    if (couponAmount.length) {
        [param setObject:couponAmount forKey:@"couponAmount"];
    }
    if (remark.length) {
        [param setObject:remark forKey:@"remark"];
    }
    if (urlArray.count) {
        [param setObject:urlArray forKey:@"url"];
    }
    if (version.length) {
        [param setObject:version forKey:@"version"];
    }
    
    // 支付方式
    NSMutableArray *payArray = [NSMutableArray array];
    if (cashPay.length) {
        [payArray addObject:[YDFunction createDictWithType:0 money:cashPay]];
    }
    if (cardPay.length) {
        [payArray addObject:[YDFunction createDictWithType:1 money:cardPay]];
    }
    if (remitPay.length) {
        [payArray addObject:[YDFunction createDictWithType:2 money:remitPay]];
    }
    if (aliPay.length) {
        [payArray addObject:[YDFunction createDictWithType:3 money:aliPay]];
    }
    if (wechatPay.length) {
        [payArray addObject:[YDFunction createDictWithType:4 money:wechatPay]];
    }
    if (payArray.count) {
        [param setObject:payArray forKey:@"paymentFlows"];
    }

    AppleServerPostCompleteBlock postHandleBlock = ^(BOOL done, id _Nullable object, NSString * _Nullable error) {
        if (done) {
            YDCheckoutOrderModel *resp = [YDCheckoutOrderModel modelWithJSON:object];
            
            if (resp.code == 200) {
                if (completion) {
                    completion(resp.data);
                }
            
            } else {
                NSString *msg;
                if (resp.detailMessage.length) {
                    msg = resp.detailMessage;
                    
                } else if (resp.message.length) {
                    msg = resp.message;
                    
                } else {
                    msg = @"网络失败";
                }
                
                if (resp.code == 5002 || resp.code == 9002 || resp.code == 9000) {
                    if (refreshBlock) {
                        refreshBlock(msg);
                    }
                    
                } else {
                    if (faildBlock) {
                        faildBlock(msg);
                    }
                }
            }
            
        } else {
            if (faildBlock) {
                faildBlock(@"网络不给力");
            }
        }
    };
    
    //使用AppleServer发送POST请求
    [AppleServer.shared postWithPath:path Paramters:param CompleteBlock:postHandleBlock];
}

+ (void)modifyCheckoutWithType:(NSInteger)type productArray:(NSMutableArray<YDSearcherProductData *> *)productArray lossAmount:(NSString *)lossAmount orderID:(NSString *)orderID couponAmount:(NSString *)couponAmount cashPay:(NSString *)cashPay cardPay:(NSString *)cardPay remitPay:(NSString *)remitPay aliPay:(NSString *)aliPay wechatPay:(NSString *)wechatPay remark:(NSString *)remark urlArray:(NSMutableArray *)urlArray version:(NSString *)version completion:(BlockCheckoutOrder)completion refreshBlock:(BlockUploadImgFaild)refreshBlock faildBlock:(BlockUploadImgFaild)faildBlock
{
    if (!orderID.length) {
        faildBlock(@"订单id为空");
        return;
    }
    
    NSString *path;
    if (type == 0) {
        path = URL_CHECKOUT_COLLECT;
        
    } else {
        path = URL_CHECKOUT_REFUND;
    }
    
    NSMutableDictionary *param = [NSMutableDictionary dictionary];
    [param setObject:orderID forKey:@"orderId"];
    if (lossAmount.length) {
        [param setObject:lossAmount forKey:@"lossAmount"];
    }
    if (couponAmount.length) {
        [param setObject:couponAmount forKey:@"couponAmount"];
    }
    if (remark.length) {
        [param setObject:remark forKey:@"remark"];
    }
    if (urlArray.count) {
        [param setObject:urlArray forKey:@"url"];
    }
    if (version.length) {
        [param setObject:version forKey:@"version"];
    }
    
    if (productArray.count) {
        [param setObject:[YDFunction createProductArrayWithArray:productArray] forKey:@"orderItems"];
    }
    
    // 支付方式
    NSMutableArray *payArray = [NSMutableArray array];
    if (cashPay.length) {
        [payArray addObject:[YDFunction createDictWithType:0 money:cashPay]];
    }
    if (cardPay.length) {
        [payArray addObject:[YDFunction createDictWithType:1 money:cardPay]];
    }
    if (remitPay.length) {
        [payArray addObject:[YDFunction createDictWithType:2 money:remitPay]];
    }
    if (aliPay.length) {
        [payArray addObject:[YDFunction createDictWithType:3 money:aliPay]];
    }
    if (wechatPay.length) {
        [payArray addObject:[YDFunction createDictWithType:4 money:wechatPay]];
    }
    if (payArray.count) {
        [param setObject:payArray forKey:@"paymentFlows"];
    }

    AppleServerPostCompleteBlock postHandleBlock = ^(BOOL done, id _Nullable object, NSString * _Nullable error) {
        if (done) {
            YDCheckoutOrderModel *resp = [YDCheckoutOrderModel modelWithJSON:object];
            
            if (resp.code == 200) {
                if (completion) {
                    completion(resp.data);
                }
            
            } else {
                NSString *msg;
                if (resp.detailMessage.length) {
                    msg = resp.detailMessage;
                    
                } else if (resp.message.length) {
                    msg = resp.message;
                    
                } else {
                    msg = @"网络失败";
                }
                
                if (resp.code == 5002 || resp.code == 9002 || resp.code == 9000) {
                    if (refreshBlock) {
                        refreshBlock(msg);
                    }
                    
                } else {
                    if (faildBlock) {
                        faildBlock(msg);
                    }
                }
            }
            
        } else {
            if (faildBlock) {
                faildBlock(@"网络不给力");
            }
        }
    };
    
    //使用AppleServer发送POST请求
    [AppleServer.shared postWithPath:path Paramters:param CompleteBlock:postHandleBlock];
}

// 把商品数组转化成接口可接收格式
+ (NSMutableArray *)createProductArrayWithArray:(NSMutableArray<YDSearcherProductData *> *)array
{
    NSMutableArray *resultArray = [NSMutableArray array];
    
    for (YDSearcherProductData *sub in array) {
        for (YDSearcherSkuGroupData *group in sub.skuGroupList) {
            for (YDSearcherSkuList *sku in group.skuList) {
                if (sku.selectedNum > 0) {
                    NSMutableDictionary *dict = [NSMutableDictionary dictionary];
                    [dict setObject:sku.ID forKey:@"skuId"];
                    [dict setObject:[NSNumber numberWithInteger:sku.selectedNum] forKey:@"itemNum"];
                    [dict setObject:[NSString stringWithFormat:@"%.2f", sub.salesPrice] forKey:@"salesPrice"];
                    [resultArray addObject:dict];
                }
            }
        }
    }
    
    return resultArray;
}

+ (NSMutableDictionary *)createDictWithType:(NSInteger)type money:(NSString *)money
{
    NSMutableDictionary *dict = [NSMutableDictionary dictionary];
    NSString *method;
    switch (type) {
        case 0:
            // 现金
            method = @"100";
            break;
            
        case 1:
            // 刷卡
            method = @"200";
            break;
            
        case 2:
            // 汇款
            method = @"300";
            break;
            
        case 3:
            // 支付宝
            method = @"400";
            break;
            
        case 4:
            // 微信
            method = @"500";
            break;
            
        default:
            break;
    }
    [dict setObject:method forKey:@"payMethod"];
    [dict setObject:money forKey:@"amount"];
    
    return dict;
}

+ (void)deletePayAndCheckoutWithOrderID:(NSString *)orderID payID:(NSString *)payID couponAmount:(NSString *)couponAmount cashPay:(NSString *)cashPay cardPay:(NSString *)cardPay remitPay:(NSString *)remitPay aliPay:(NSString *)aliPay wechatPay:(NSString *)wechatPay remark:(NSString *)remark urlArray:(NSMutableArray<NSString *> *)urlArray version:(NSString *)version completion:(BlockCheckoutOrder)completion faildBlock:(BlockUploadImgFaild)faildBlock
{
    if (!orderID.length) {
        faildBlock(@"订单id为空");
        return;
    }
    
    NSString *path = URL_CANCEL_CHECKOUT;
    
    NSMutableDictionary *param = [NSMutableDictionary dictionary];
    [param setObject:orderID forKey:@"orderId"];
    [param setObject:payID forKey:@"payId"];
    if (couponAmount.length) {
        [param setObject:couponAmount forKey:@"couponAmount"];
    }
    if (remark.length) {
        [param setObject:remark forKey:@"remark"];
    }
    if (urlArray.count) {
        [param setObject:urlArray forKey:@"url"];
    }
    if (version.length) {
        [param setObject:version forKey:@"version"];
    }
    
    // 支付方式
    NSMutableArray *payArray = [NSMutableArray array];
    if (cashPay.length) {
        [payArray addObject:[YDFunction createDictWithType:0 money:cashPay]];
    }
    if (cardPay.length) {
        [payArray addObject:[YDFunction createDictWithType:1 money:cardPay]];
    }
    if (remitPay.length) {
        [payArray addObject:[YDFunction createDictWithType:2 money:remitPay]];
    }
    if (aliPay.length) {
        [payArray addObject:[YDFunction createDictWithType:3 money:aliPay]];
    }
    if (wechatPay.length) {
        [payArray addObject:[YDFunction createDictWithType:4 money:wechatPay]];
    }
    if (payArray.count) {
        [param setObject:payArray forKey:@"paymentFlows"];
    }

    AppleServerPostCompleteBlock postHandleBlock = ^(BOOL done, id _Nullable object, NSString * _Nullable error) {
        if (done) {
            YDCheckoutOrderModel *resp = [YDCheckoutOrderModel modelWithJSON:object];
            
            if (resp.code == 200) {
                if (completion) {
                    completion(resp.data);
                }
            
            } else {
                if (faildBlock) {
                    NSString *msg;
                    if (resp.detailMessage.length) {
                        msg = resp.detailMessage;
                        
                    } else if (resp.message.length) {
                        msg = resp.message;
                        
                    } else {
                        msg = @"网络失败";
                    }
                    faildBlock(msg);
                }
            }
            
        } else {
            if (faildBlock) {
                faildBlock(@"网络不给力");
            }
        }
    };
    
    //使用AppleServer发送POST请求
    [AppleServer.shared postWithPath:path Paramters:param CompleteBlock:postHandleBlock];
}

+ (NSData *)getDataWithString:(NSString *)string
{
    NSData *data = [string dataUsingEncoding:NSUTF8StringEncoding];
    
    return data;
}

@end

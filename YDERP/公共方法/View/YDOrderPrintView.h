//
//  YDOrderPrintView.h
//  YDERP
//
//  Created by 何英健 on 2021/9/1.
//

#import <UIKit/UIKit.h>

typedef void(^BlockPrintOrderViewSucceed)(UIView *view);

@interface YDOrderPrintView : UIView

+ (YDOrderPrintView *)createWithData:(YDOrderPrintData *)data completion:(BlockPrintOrderViewSucceed)completion;

@end

//
//  YDOrderPrintView.m
//  YDERP
//
//  Created by 何英健 on 2021/9/1.
//

#import "YDOrderPrintView.h"

@interface YDOrderPrintView ()

@property (nonatomic, strong) YDOrderPrintData *data;
@property (nonatomic, copy) BlockPrintOrderViewSucceed completion;

@property (nonatomic, assign) BOOL aliPaySucceed;
@property (nonatomic, assign) BOOL wechatPaySucceed;
@property (nonatomic, assign) BOOL otherPaySucceed;

@property (nonatomic, weak) IBOutlet UILabel *titleLabel;
@property (nonatomic, weak) IBOutlet UILabel *customerLabel;
@property (nonatomic, weak) IBOutlet UILabel *orderNoLabel;
@property (nonatomic, weak) IBOutlet UILabel *orderTimeLabel;
@property (nonatomic, weak) IBOutlet UILabel *salesNameLabel;

@property (nonatomic, weak) IBOutlet UIView *lineView1;
@property (nonatomic, weak) IBOutlet UIView *lineView2;
@property (nonatomic, weak) IBOutlet UIView *lineView3;
@property (nonatomic, weak) IBOutlet UIView *lineView4;
@property (nonatomic, weak) IBOutlet UIView *lineView5;
@property (nonatomic, weak) IBOutlet UIView *lineView6;
@property (nonatomic, weak) IBOutlet UIView *lineView7;
@property (nonatomic, weak) IBOutlet UIView *lineView8;
@property (nonatomic, weak) IBOutlet UIView *lineView9;
@property (nonatomic, weak) IBOutlet UIView *lineView10;

@property (nonatomic, weak) IBOutlet UILabel *titleNameLabel;
@property (nonatomic, weak) IBOutlet UILabel *titleColorLabel;
@property (nonatomic, weak) IBOutlet UILabel *titleSizeLabel;
@property (nonatomic, weak) IBOutlet UILabel *titleQuantityLabel;
@property (nonatomic, weak) IBOutlet UILabel *titlePriceLabel;
@property (nonatomic, weak) IBOutlet UILabel *titleTotalPriceLabel;

@property (nonatomic, weak) IBOutlet UIView *nameBGView;
@property (nonatomic, weak) IBOutlet UIView *colorBGView;
@property (nonatomic, weak) IBOutlet UIView *sizeBGView;
@property (nonatomic, weak) IBOutlet UIView *quantityBGView;
@property (nonatomic, weak) IBOutlet UIView *priceBGView;
@property (nonatomic, weak) IBOutlet UIView *totalPriceBGView;

@property (nonatomic, weak) IBOutlet UILabel *totalNumerTitleLabel;
@property (nonatomic, weak) IBOutlet UILabel *totalPriceTitleLabel;
@property (nonatomic, weak) IBOutlet UILabel *couponTitleLabel;
@property (nonatomic, weak) IBOutlet UILabel *totalNumerLabel;
@property (nonatomic, weak) IBOutlet UILabel *totalPriceLabel;
@property (nonatomic, weak) IBOutlet UILabel *couponLabel;

@property (nonatomic, weak) IBOutlet UILabel *totalAmountTitleLabel;
@property (nonatomic, weak) IBOutlet UILabel *totalAmountLabel;
@property (nonatomic, weak) IBOutlet UILabel *collectMoneyTitleLabel;
@property (nonatomic, weak) IBOutlet UILabel *collectMoneyLabel;
@property (nonatomic, weak) IBOutlet UILabel *totalOweMoneyTitleLabel;
@property (nonatomic, weak) IBOutlet UILabel *totalOweMoneyLabel;

@property (nonatomic, weak) IBOutlet UILabel *commonTitleLabel;
@property (nonatomic, weak) IBOutlet UILabel *commonLabel;

@property (nonatomic, weak) IBOutlet UILabel *shopNameTitleLabel;
@property (nonatomic, weak) IBOutlet UILabel *shopNameLabel;
@property (nonatomic, weak) IBOutlet UILabel *shopAddressTitleLabel;
@property (nonatomic, weak) IBOutlet UILabel *shopAddressLabel;
@property (nonatomic, weak) IBOutlet UILabel *phoneTitleLabel;
@property (nonatomic, weak) IBOutlet UILabel *phoneLabel;

@property (nonatomic, weak) IBOutlet UIView *cardPayView;
@property (nonatomic, weak) IBOutlet UILabel *noticeLabel;
@property (nonatomic, weak) IBOutlet UILabel *welcomeLabel;

@property (nonatomic, weak) IBOutlet UIImageView *aliPayImgView;
@property (nonatomic, weak) IBOutlet UIImageView *wechatPayImgView;
@property (nonatomic, weak) IBOutlet UIImageView *otherPayImgView;

@property (nonatomic, weak) IBOutlet NSLayoutConstraint *skuBGViewH;
@property (nonatomic, weak) IBOutlet NSLayoutConstraint *commonLabelH;
@property (nonatomic, weak) IBOutlet NSLayoutConstraint *shopAddressLabelH;
@property (nonatomic, weak) IBOutlet NSLayoutConstraint *cardPayViewH;
@property (nonatomic, weak) IBOutlet NSLayoutConstraint *codePayViewH;
@property (nonatomic, weak) IBOutlet NSLayoutConstraint *codePayViewTop;
@property (nonatomic, weak) IBOutlet NSLayoutConstraint *aliPayViewW;
@property (nonatomic, weak) IBOutlet NSLayoutConstraint *aliPayViewTrailing;
@property (nonatomic, weak) IBOutlet NSLayoutConstraint *wechatPayViewW;
@property (nonatomic, weak) IBOutlet NSLayoutConstraint *wechatPayViewTrailing;
@property (nonatomic, weak) IBOutlet NSLayoutConstraint *otherPayViewW;

@end

@implementation YDOrderPrintView

+ (YDOrderPrintView *)createWithData:(YDOrderPrintData *)data completion:(BlockPrintOrderViewSucceed)completion
{
    YDOrderPrintView *view = [[NSBundle mainBundle] loadNibNamed:@"YDOrderPrintView" owner:nil options:nil][0];
    view.frame = CGRectMake(0, 0, 576, 0);
    [view initUI];
    view.completion = completion;
    view.data = data;
    
    return view;
}

- (void)setData:(YDOrderPrintData *)data
{
    _data = data;
    
    self.titleLabel.text = [NSString stringWithFormat:@"%@订单", data.shop.shopName];
    NSString *clientName = data.order.clientName;
    if (!clientName.length) {
        clientName = @"临时客户";
    }
    self.customerLabel.text = [NSString stringWithFormat:@"客户：%@", clientName];
    self.orderNoLabel.text = [NSString stringWithFormat:@"订单：%@", data.order.orderNo];
    self.orderTimeLabel.text = [NSString stringWithFormat:@"开单日期：%@", data.order.orderTime];
    self.salesNameLabel.text = [NSString stringWithFormat:@"开单人：%@", data.order.salesName];
    
    [self setupSkuView];
    
    self.totalNumerLabel.text = [NSString stringWithFormat:@"%ld", (long)data.order.totalNum];
    self.totalPriceLabel.text = [NSString stringWithFormat:@"%.2f", data.order.itemAmount];
    self.couponLabel.text = [NSString stringWithFormat:@"%.2f", data.order.couponAmount];
    
    self.totalAmountLabel.text = [NSString stringWithFormat:@"%.2f", data.order.itemAmount - data.order.couponAmount];
    self.collectMoneyLabel.text = [NSString stringWithFormat:@"%.2f", data.order.payItemAmount];
    self.totalOweMoneyLabel.text = [NSString stringWithFormat:@"%.2f", data.order.currDueAmount];
    NSString *remark = data.order.remark;
    self.commonLabel.text = remark;
    self.commonLabelH.constant = [remark sizeWith:CGSizeMake(self.commonLabel.width, MAXFLOAT) font:FONT_ORDER(21)].height + 2;
    
    self.shopNameLabel.text = data.shop.shopName;
    NSString *address = data.shop.shopAddr;
    self.shopAddressLabel.text = address;
    self.shopAddressLabelH.constant = [address sizeWith:CGSizeMake(self.shopAddressLabel.width, MAXFLOAT) font:FONT_ORDER(22)].height + 2;
    
    self.phoneLabel.text = data.shop.shopMobile;
    
    [self setupCardPayView];
    [self setupCodePayView];
}

- (void)initUI
{
    [self setupFont];
    [self setupLineView];
}

- (void)setupFont
{
    self.titleLabel.font = FONT_ORDER(40);
    self.customerLabel.font = FONT_ORDER(22);
    self.orderNoLabel.font = FONT_ORDER(22);
    self.orderTimeLabel.font = FONT_ORDER(22);
    self.salesNameLabel.font = FONT_ORDER(22);
    self.titleNameLabel.font = FONT_ORDER(21);
    self.titleColorLabel.font = FONT_ORDER(21);
    self.titleSizeLabel.font = FONT_ORDER(21);
    self.titleQuantityLabel.font = FONT_ORDER(21);
    self.titlePriceLabel.font = FONT_ORDER(21);
    self.titleTotalPriceLabel.font = FONT_ORDER(21);
    self.totalNumerTitleLabel.font = FONT_ORDER(22);
    self.totalPriceTitleLabel.font = FONT_ORDER(22);
    self.couponTitleLabel.font = FONT_ORDER(22);
    self.totalNumerLabel.font = FONT_ORDER(22);
    self.totalPriceLabel.font = FONT_ORDER(22);
    self.couponLabel.font = FONT_ORDER(22);
    self.totalAmountTitleLabel.font = FONT_ORDER(32);
    self.totalAmountLabel.font = FONT_ORDER(32);
    self.collectMoneyLabel.font = FONT_ORDER(24);
    self.collectMoneyTitleLabel.font = FONT_ORDER(24);
    self.totalOweMoneyLabel.font = FONT_ORDER(24);
    self.totalOweMoneyTitleLabel.font = FONT_ORDER(24);
    self.commonLabel.font = FONT_ORDER(21);
    self.shopNameTitleLabel.font = FONT_ORDER(22);
    self.shopNameLabel.font = FONT_ORDER(22);
    self.shopAddressTitleLabel.font = FONT_ORDER(22);
    self.shopAddressLabel.font = FONT_ORDER(22);
    self.phoneTitleLabel.font = FONT_ORDER(22);
    self.phoneLabel.font = FONT_ORDER(22);
    self.noticeLabel.font = FONT_ORDER(21);
    self.welcomeLabel.font = FONT_ORDER(21);
}

- (void)setupLineView
{
    [self drawLineOfDashByCAShapeLayer:self.lineView1 lineLength:5 lineSpacing:2 lineColor:[UIColor blackColor] lineDirection:YES];
    [self drawLineOfDashByCAShapeLayer:self.lineView2 lineLength:2 lineSpacing:1 lineColor:[UIColor blackColor] lineDirection:YES];
    [self drawLineOfDashByCAShapeLayer:self.lineView3 lineLength:2 lineSpacing:1 lineColor:[UIColor blackColor] lineDirection:YES];
    [self drawLineOfDashByCAShapeLayer:self.lineView4 lineLength:2 lineSpacing:1 lineColor:[UIColor blackColor] lineDirection:YES];
    [self drawLineOfDashByCAShapeLayer:self.lineView5 lineLength:2 lineSpacing:1 lineColor:[UIColor blackColor] lineDirection:YES];
    [self drawLineOfDashByCAShapeLayer:self.lineView6 lineLength:2 lineSpacing:1 lineColor:[UIColor blackColor] lineDirection:NO];
    [self drawLineOfDashByCAShapeLayer:self.lineView7 lineLength:2 lineSpacing:1 lineColor:[UIColor blackColor] lineDirection:YES];
    [self drawLineOfDashByCAShapeLayer:self.lineView8 lineLength:5 lineSpacing:2 lineColor:[UIColor blackColor] lineDirection:YES];
    [self drawLineOfDashByCAShapeLayer:self.lineView9 lineLength:2 lineSpacing:1 lineColor:[UIColor blackColor] lineDirection:YES];
    [self drawLineOfDashByCAShapeLayer:self.lineView10 lineLength:5 lineSpacing:2 lineColor:[UIColor blackColor] lineDirection:YES];
}

- (void)setupSkuView
{
    self.skuBGViewH.constant = 37 + 35 * self.data.orderItems.count;

    for (NSInteger i = 0; i < self.data.orderItems.count; i++) {
        YDOrderPrintDataItem *item = self.data.orderItems[i];
        
        // 款号
        NSString *nameStr = @"";
        if (item.itemCode.length) {
            nameStr = item.itemCode;
            
            if (item.itemName.length) {
                nameStr = [NSString stringWithFormat:@"%@/%@", nameStr, item.itemName];
            }
            
        } else if (item.itemName.length) {
            nameStr = item.itemName;
        }
        [YDOrderPrintView createLabelWithSection:i text:nameStr targetView:self.nameBGView];
        // 颜色
        [YDOrderPrintView createLabelWithSection:i text:item.color targetView:self.colorBGView];
        // 尺码
        [YDOrderPrintView createLabelWithSection:i text:item.size targetView:self.sizeBGView];
        // 数量
        [YDOrderPrintView createLabelWithSection:i text:[NSString stringWithFormat:@"%ld", item.itemNum] targetView:self.quantityBGView];
        // 单价
        [YDOrderPrintView createLabelWithSection:i text:[NSString stringWithFormat:@"%.2f", item.salesPrice] targetView:self.priceBGView];
        // 小计
        [YDOrderPrintView createLabelWithSection:i text:[NSString stringWithFormat:@"%.2f", item.subtotalAmount] targetView:self.totalPriceBGView];
    }
}

- (void)setupCardPayView
{
    // 获取所有银行卡
    NSMutableArray<YDCollectMoneyData *> *cardArray = [NSMutableArray array];
    for (YDCollectMoneyData *sub in self.data.shop.paySettings) {
        if ([sub.type isEqualToString:@"card"]) {
            [cardArray addObject:sub];
        }
    }
    
    self.cardPayViewH.constant = 35 * cardArray.count;
    
    for (int i = 0; i < cardArray.count; i++) {
        YDCollectMoneyData *sub = cardArray[i];
        UILabel *bankLabel = [YDOrderPrintView createLabelWithText:sub.cardBank targetView:self.cardPayView];
        [bankLabel setY:6 + i * 35];
        [bankLabel setWidth:85];
        
        UILabel *cardNoLabel = [YDOrderPrintView createLabelWithText:sub.cardNo targetView:self.cardPayView];
        [cardNoLabel setX:CGRectGetMaxX(bankLabel.frame) + 30];
        [cardNoLabel setY:bankLabel.y];
        [cardNoLabel setWidth:230];
        
        NSString *cardName = sub.cardName;
        if (cardName.length > 5) {
            cardName = [cardName substringToIndex:5];
        }
        UILabel *cardNameLabel = [YDOrderPrintView createLabelWithText:cardName targetView:self.cardPayView];
        CGFloat nameW = [cardName sizeWith:CGSizeMake(MAXFLOAT, 29) font:FONT_ORDER(21)].width + 2;
        [cardNameLabel setWidth:nameW];
        [cardNameLabel setX:self.cardPayView.width - cardNameLabel.width];
        [cardNameLabel setY:bankLabel.y];
        
        UILabel *cardNameTitleLabel = [YDOrderPrintView createLabelWithText:@"账户名" targetView:self.cardPayView];
        [cardNameTitleLabel setWidth:65];
        [cardNameTitleLabel setX:cardNameLabel.x - 15 - cardNameTitleLabel.width];
        [cardNameTitleLabel setY:bankLabel.y];
    }
}

- (void)setupCodePayView
{
    // 依次获取支付宝/微信/收款码
    YDCollectMoneyData *aliPay;
    YDCollectMoneyData *wechatPay;
    YDCollectMoneyData *otherPay;
    for (YDCollectMoneyData *sub in self.data.shop.paySettings) {
        if ([sub.type isEqualToString:@"alipay"]) {
            aliPay = sub;
            
        } else if ([sub.type isEqualToString:@"wechat"]) {
            wechatPay = sub;
            
        } else if ([sub.type isEqualToString:@"other"]) {
            otherPay = sub;
        }
    }
    
    if (!aliPay.imgUrl.length && !wechatPay.imgUrl.length && !otherPay.imgUrl.length) {
        self.codePayViewH.constant = 0;
        self.codePayViewTop.constant = 0;
        [self createSucceed];
        
    } else {
        if (!aliPay.imgUrl.length) {
            NSLog(@"客户没有支付宝图片");
            self.aliPaySucceed = YES;
            self.aliPayViewW.constant = 0;
            self.aliPayViewTrailing.constant = 0;
        }
        
        if (!wechatPay.imgUrl.length) {
            NSLog(@"客户没有微信图片");
            self.wechatPaySucceed = YES;
            self.wechatPayViewW.constant = 0;
            self.wechatPayViewTrailing.constant = 0;
        }
        
        if (!otherPay.imgUrl.length) {
            NSLog(@"客户没有个人支付码图片");
            self.otherPaySucceed = YES;
            self.otherPayViewW.constant = 0;
        }
        
        YDWeakSelf
        if (aliPay.imgUrl.length) {
            [self.aliPayImgView appleSetImageWithUrl:aliPay.imgUrl SprcifyOssImageStyle:@"products_list" HasHD:NO placeholderImage:@"默认商品方形图" completion:^(UIImage * _Nullable image, NSError * _Nullable error, SDImageCacheType cacheType, NSURL * _Nullable imageURL) {
                NSLog(@"下载支付宝图片完成");
                weakSelf.aliPaySucceed = YES;
                [weakSelf checkPayImgDownloadSucceed];
            }];
        }
        
        if (wechatPay.imgUrl.length) {
            [self.wechatPayImgView appleSetImageWithUrl:wechatPay.imgUrl SprcifyOssImageStyle:@"products_list" HasHD:NO placeholderImage:@"默认商品方形图" completion:^(UIImage * _Nullable image, NSError * _Nullable error, SDImageCacheType cacheType, NSURL * _Nullable imageURL) {
                NSLog(@"下载微信图片完成");
                weakSelf.wechatPaySucceed = YES;
                [weakSelf checkPayImgDownloadSucceed];
            }];
        }
        
        if (otherPay.imgUrl.length) {
            [self.otherPayImgView appleSetImageWithUrl:otherPay.imgUrl SprcifyOssImageStyle:@"products_list" HasHD:NO placeholderImage:@"默认商品方形图" completion:^(UIImage * _Nullable image, NSError * _Nullable error, SDImageCacheType cacheType, NSURL * _Nullable imageURL) {
                NSLog(@"下载个人支付码图片完成");
                weakSelf.otherPaySucceed = YES;
                [weakSelf checkPayImgDownloadSucceed];
            }];
        }
    }
}

- (void)createSucceed
{
    [self layoutIfNeeded];
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.3 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        self.height = CGRectGetMaxY(self.welcomeLabel.frame) + 150;
        
        if (self.completion) {
            self.completion(self);
        }
    });
}

- (void)checkPayImgDownloadSucceed
{
    if (self.aliPaySucceed && self.wechatPaySucceed && self.otherPaySucceed) {
        NSLog(@"打印单所有图片下载完成");
        [self createSucceed];
    }
}

+ (UILabel *)createLabelWithText:(NSString *)text targetView:(UIView *)targetView;
{
    UILabel *label = [[UILabel alloc] init];
    [label setHeight:29];
    label.textColor = [UIColor blackColor];
    label.font = FONT_ORDER(21);
    label.text = text;
    label.lineBreakMode = NSLineBreakByCharWrapping;
    [targetView addSubview:label];
    
    return label;
}

+ (void)createLabelWithSection:(NSInteger)section text:(NSString *)text targetView:(UIView *)targetView;
{
    UILabel *label = [YDOrderPrintView createLabelWithText:text targetView:targetView];
    [label setWidth:targetView.width];
    [label setY:6 + section * (label.height + 6)];
}

/**
 *  通过 CAShapeLayer 方式绘制虚线
 *
 *  param lineView:       需要绘制成虚线的view
 *  param lineLength:     虚线的宽度
 *  param lineSpacing:    虚线的间距
 *  param lineColor:      虚线的颜色
 *  param lineDirection   虚线的方向  YES 为水平方向， NO 为垂直方向
 **/
- (void)drawLineOfDashByCAShapeLayer:(UIView *)lineView lineLength:(int)lineLength lineSpacing:(int)lineSpacing lineColor:(UIColor *)lineColor lineDirection:(BOOL)isHorizonal {
 
    CAShapeLayer *shapeLayer = [CAShapeLayer layer];
 
    [shapeLayer setBounds:lineView.bounds];
 
    if (isHorizonal) {
 
        [shapeLayer setPosition:CGPointMake(CGRectGetWidth(lineView.frame) / 2, CGRectGetHeight(lineView.frame))];
 
    } else{
        [shapeLayer setPosition:CGPointMake(CGRectGetWidth(lineView.frame) / 2, CGRectGetHeight(lineView.frame)/2)];
    }
 
    [shapeLayer setFillColor:[UIColor clearColor].CGColor];
    //  设置虚线颜色为blackColor
    [shapeLayer setStrokeColor:lineColor.CGColor];
    //  设置虚线宽度
    if (isHorizonal) {
        [shapeLayer setLineWidth:CGRectGetHeight(lineView.frame)];
    } else {
 
        [shapeLayer setLineWidth:CGRectGetWidth(lineView.frame)];
    }
    [shapeLayer setLineJoin:kCALineJoinRound];
    //  设置线宽，线间距
    [shapeLayer setLineDashPattern:[NSArray arrayWithObjects:[NSNumber numberWithInt:lineLength], [NSNumber numberWithInt:lineSpacing], nil]];
    //  设置路径
    CGMutablePathRef path = CGPathCreateMutable();
    CGPathMoveToPoint(path, NULL, 0, 0);
 
    if (isHorizonal) {
        CGPathAddLineToPoint(path, NULL,CGRectGetWidth(lineView.frame), 0);
    } else {
        CGPathAddLineToPoint(path, NULL, 0, CGRectGetHeight(lineView.frame));
    }
 
    [shapeLayer setPath:path];
    CGPathRelease(path);
    //  把绘制好的虚线添加上来
    [lineView.layer addSublayer:shapeLayer];
}

@end
